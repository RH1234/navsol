﻿using System;

namespace NSC_DI.SAP
{
	public static class Effect 
    {
		public static void CreateEffect(string Code, string Name)
		{
			SAPbobsCOM.GeneralService	oGeneralService = null;
			SAPbobsCOM.GeneralData		oGeneralData	= null;

			try
			{
				SAPbobsCOM.CompanyService oCompService = Globals.oCompany.GetCompanyService();

				oGeneralService = oCompService.GetGeneralService(Globals.tEffect + "_C");

				oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

				oGeneralData.SetProperty("Code", Code);

				oGeneralData.SetProperty("U_Name", Name);

				oGeneralService.Add(oGeneralData);
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(oGeneralService);
				UTIL.Misc.KillObject(oGeneralData);

				GC.Collect();
			}
		}
    }
}
