﻿using System;

namespace NSC_DI.SAP
{
	public static class GreenbookTaskNote 
    {
		public static void CreateNewGreenbookTaskNote(string CreatedBy, string TaskID, string Note)
        {
			SAPbobsCOM.GeneralService	oGeneralService = null;
			SAPbobsCOM.GeneralData		oGeneralData	= null;

            try
            {
				SAPbobsCOM.CompanyService oCompService = Globals.oCompany.GetCompanyService();
				oGeneralService = oCompService.GetGeneralService(Globals.tGBTaskNote + "_C");
				oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

				var NextCode = UTIL.SQL.GetNextAvailableCodeForItem(Globals.tGBTaskNote, "Code");
				//Setting Data to Master Data Table Fields         
                oGeneralData.SetProperty("Code", NextCode);
				oGeneralData.SetProperty("UserSign",	Globals.oCompany.UserSignature);
				oGeneralData.SetProperty("CreateDate",	DateTime.Now);
				oGeneralData.SetProperty("U_CreatedBy", CreatedBy);
                oGeneralData.SetProperty("U_CreatedOn", DateTime.Now);
                oGeneralData.SetProperty("U_TaskID",	TaskID);
                oGeneralData.SetProperty("U_Note",		Note);

                // Attempt to add
                oGeneralService.Add(oGeneralData);
            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
            // Garbage collection
            finally
            {
				UTIL.Misc.KillObject(oGeneralService);
				UTIL.Misc.KillObject(oGeneralData);

				GC.Collect();
            }
        }
    }
}
