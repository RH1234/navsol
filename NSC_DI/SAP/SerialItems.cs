﻿using System;
using System.Collections.Generic;

namespace NSC_DI.SAP
{
    public static class SerialItems
    {
        public static string GetNextAvailableSerialID(string ItemCode, string Prefix)
        {
            // Prepare the SQL statement to get a unique serialId.
            string SQL_NewID = @"SELECT ISNULL((MAX(CONVERT(int,[OSRI].[SysSerial]))),1) FROM [OSRI] WHERE [OSRI].[ItemCode] = '" + ItemCode + "'";

            // Get the results from SQL
            SAPbobsCOM.Fields ResultFromSQL = UTIL.SQL.GetFieldsFromSQLQuery(SQL_NewID);

            if (ResultFromSQL.Count <= 0) return null;

            // ItemCode-Prefx-SQLResult
            return string.Format("{0}-{1}-{2}", ItemCode, Prefix, ResultFromSQL.Item(0).Value.ToString());
        }

        public static List<string> GetNextAvailableSerialID(string ItemCode, string Prefix, double numberOfPlants)
        {
            List<string> results = new List<string>();
            for (int i = 0; i < numberOfPlants; i++)
            {
                // Prepare the SQL statement to get the next availble plant ID
                string SQL_NewID = @"SELECT ISNULL((MAX(CONVERT(int,[OSRI].[SysSerial]))),1) FROM [OSRI] WHERE [OSRI].[ItemCode] = '" + ItemCode + "'";

                // Get the results from SQL
                SAPbobsCOM.Fields ResultFromSQL = UTIL.SQL.GetFieldsFromSQLQuery(SQL_NewID);

                // If the results from SQL came back successfully
                if (ResultFromSQL.Count > 0)
                {
                    string nextSerialId = string.Format("{0}-{1}-{2}", ItemCode, Prefix, (Convert.ToInt32(ResultFromSQL.Item(0).Value.ToString()) + i).ToString());
                    results.Add(nextSerialId);
                }
            }
            if (results.Count > 0)
            {
                return results;
            }
            return null;
        }

        public static void SetUDF(string pDistNumber, string pItemCode = null, string pField = "", string pValue = "")
        {
            SAPbobsCOM.SerialNumberDetail oDetails = null;

            SAPbobsCOM.CompanyService oCoService = null;
            SAPbobsCOM.SerialNumberDetailsService oService = null;
            SAPbobsCOM.SerialNumberDetailParams oParams = null;

            try
            {
                string sql = $"SELECT AbsEntry FROM OSRN WHERE DistNumber = '{pDistNumber}'";
                if (!string.IsNullOrEmpty(pItemCode?.Trim())) sql += $" AND ItemCode = '{pItemCode}'";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pItemCode) == false)

                int DocEntry = UTIL.SQL.GetValue<int>(sql, 0);

                oCoService = Globals.oCompany.GetCompanyService() as SAPbobsCOM.CompanyService;
                oService = oCoService.GetBusinessService(SAPbobsCOM.ServiceTypes.SerialNumberDetailsService);
                oParams = oService.GetDataInterface(SAPbobsCOM.SerialNumberDetailsServiceDataInterfaces.sndsSerialNumberDetailParams);

                oParams.DocEntry = DocEntry;
                oDetails = oService.Get(oParams);
                oDetails.UserFields.Item(pField).Value = pValue;
                oService.Update(oDetails);

            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(oDetails);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oParams);
            }

        }

        public static void SetStateID(string pDistNumber, string pItemCode = null, string pValue = "")
        {
            SAPbobsCOM.SerialNumberDetail oDetails = null;

            SAPbobsCOM.CompanyService oCoService = null;
            SAPbobsCOM.SerialNumberDetailsService oService = null;
            SAPbobsCOM.SerialNumberDetailParams oParams = null;

            try
            {
                string strSQL = $"SELECT AbsEntry FROM OSRN WHERE DistNumber = '{pDistNumber}'";
                if (!string.IsNullOrEmpty(pItemCode?.Trim())) strSQL += $" AND ItemCode = '{pItemCode}'";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pItemCode) == false)

                int DocEntry = UTIL.SQL.GetValue<int>(strSQL);

                oCoService   = Globals.oCompany.GetCompanyService() as SAPbobsCOM.CompanyService;
                oService     = oCoService.GetBusinessService(SAPbobsCOM.ServiceTypes.SerialNumberDetailsService);
                oParams      = oService.GetDataInterface(SAPbobsCOM.SerialNumberDetailsServiceDataInterfaces.sndsSerialNumberDetailParams);

                oParams.DocEntry     = DocEntry;
                oDetails             = oService.Get(oParams);
                oDetails.MfrSerialNo = pValue;
                oService.Update(oDetails);

            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(oDetails);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oParams);
            }

        }

        public static void SetWetWeight(string pPlantID, double pWetWeight)
        {
            try
            {
                string qry = $"UPDATE OSRN SET U_NSC_WetWgt = {pWetWeight} WHERE DistNumber = '{pPlantID}'";
                UTIL.SQL.RunSQLQuery(qry);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static void SetHarvestInfo(string[] pPlantIDs, int pHarvestPdO, string pHarvestDate)
        {
            try
            {
                string PlantsToUpdate = "(";
                for (int i=0; i< pPlantIDs.Length;i++)
                {
                    if(i==0)
                    {
                        PlantsToUpdate += "'"+ pPlantIDs[i].ToString() + "'";
                    }
                    else
                    {
                        PlantsToUpdate += ",'" + pPlantIDs[i].ToString() + "'";
                    }
                }
                PlantsToUpdate += ")";
                string qry = $"UPDATE OSRN SET U_NSC_HarvestPdO = {pHarvestPdO}, U_NSC_HarvestDate = '{pHarvestDate}' WHERE DistNumber  in {PlantsToUpdate} ";
                UTIL.SQL.RunSQLQuery(qry);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static string CurrentWH(string pSN)
        {
            try
            {
                if (string.IsNullOrEmpty(pSN?.Trim())) return null;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pSN))
                var sql = "";
                sql += $"SELECT TOP (1) OSBQ.WhsCode FROM OSRN" + Environment.NewLine;
                sql += $" INNER JOIN OSBQ ON OSRN.SysNumber = OSBQ.SnBMDAbs" + Environment.NewLine;
                sql += $" WHERE OSRN.DistNumber = '{pSN}'" + Environment.NewLine;
                sql += $" ORDER BY OSBQ.AbsEntry DESC";
                return NSC_DI.UTIL.SQL.GetValue<string>(sql, 0);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static SAPbobsCOM.SerialNumberDetail GetInfo(string pSerialNumber)
        {
            // retrurn the batch number object 

            if (pSerialNumber.Trim() == "") return null;

            SAPbobsCOM.CompanyService             oCoService     = null;
            SAPbobsCOM.SerialNumberDetailParams   oParams        = null;
            SAPbobsCOM.SerialNumberDetailsService oSerialService = null;

            try
            {
                oCoService       = Globals.oCompany.GetCompanyService() as SAPbobsCOM.CompanyService;
                oSerialService   = oCoService.GetBusinessService(SAPbobsCOM.ServiceTypes.SerialNumberDetailsService);
                oParams          = oSerialService.GetDataInterface(SAPbobsCOM.SerialNumberDetailsServiceDataInterfaces.sndsSerialNumberDetailParams);

                string strSQL    = $"SELECT AbsEntry FROM [OSRN] WHERE DistNumber='{pSerialNumber}'";
                oParams.DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                return oSerialService.Get(oParams);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oCoService);
                NSC_DI.UTIL.Misc.KillObject(oParams);
                NSC_DI.UTIL.Misc.KillObject(oSerialService);
                GC.Collect();
            }
        }
    }
}
