﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Runtime.InteropServices;

namespace NSC_DI.SAP
{
    public class B1Object
    {
        internal static XmlDocument ToXmlDoc(dynamic o)
        {
            try
            {
                if (!Marshal.IsComObject(o)) throw new Exception("Cannot copy object");

                Globals.oCompany.XmlExportType = SAPbobsCOM.BoXmlExportTypes.xet_ExportImportMode;
                var xmlStr = o.GetAsXML() as string;
                var doc = new XmlDocument();
                doc.LoadXml(xmlStr);
                return doc;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
        }

        internal static dynamic FromXmlDoc(XmlDocument pDoc)
        {
            var dest = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            pDoc.Save(dest);
            dynamic b = Globals.oCompany.GetBusinessObjectFromXML(dest, 0);
            File.Delete(dest);
            return b;
        }

        public static int GetLastAdded()
        {
            try
            {
                string newCode = null;

                // Get the production order code
                Globals.oCompany.GetNewObjectCode(out newCode);
                var Key = 0;
                if (newCode != null) Key = Convert.ToInt32(newCode);
                return Key;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
            }
        }

        public static int GetKey(string pObjectKey, string pXpathToNode = "//DocEntry")
        {
            try
            {
                XmlDocument doc = new XmlDocument();

                doc.LoadXml(pObjectKey);

                XmlNode node = doc.SelectSingleNode(pXpathToNode);

                var keyStr = node?.InnerText;

                if (string.IsNullOrEmpty(keyStr?.Trim())) throw new KeyNotFoundException("given xml and xpath expression resulted in no key found");// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(keyStr))

                if (!int.TryParse(keyStr, out int key)) throw new InvalidCastException($"Given key '{keyStr}' cannot be parsed to an integer");

                return key;
            }
            catch (Exception ex)
            {

                throw new Exception(UTIL.Message.Format(ex));
            }
        }
    }
}
