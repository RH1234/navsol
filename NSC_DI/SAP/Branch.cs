﻿using System;

namespace NSC_DI.SAP
{
    public static class Branch
    {
        public static int Get(string pWH)
        {
            try
            {
                // get the branch to the Warehouse's branch
                var Branch = NSC_DI.SAP.Warehouse.GetBranch(pWH);
                if (Branch > 0)  Assigned(Globals.oCompany.UserSignature, Branch); // see if the user has been assigned to this branch
                return Branch;
            }
            catch (System.ComponentModel.WarningException ex)
            {
                throw new System.ComponentModel.WarningException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
        public static string[] GetAll_User_Ary(int pDefaultBr)
        {
            try
            {
                if (pDefaultBr < 0) return new string[0];    // -1 is no branches, 0 is no default branch
                var userCode = Globals.oCompany.UserName;

                //if () brList = branch.ToString();             // if a default is specified, then use it

                // get the branch to the Warehouse's branch
                var sql = $"SELECT USR6.BPLId FROM USR6 INNER JOIN OBPL ON USR6.BPLId = OBPL.BPLId WHERE UserCode = '{userCode}' AND OBPL.Disabled = 'N'";
                if (pDefaultBr > 0) sql += $" AND OBPL.BPLId = {pDefaultBr.ToString()}";

                var oRS = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(sql);
                if (oRS == null) throw new Exception($"You have not been assigned any branches.");

                string[] ret = new string[oRS.RecordCount];

                for (var i = 0; i < oRS.RecordCount; i++)
                {
                    if (i > 0) oRS.MoveNext();
                    ret[i] = oRS.Fields.Item(0).Value.ToString();
                }
                NSC_DI.UTIL.Misc.KillObject(oRS);
                return ret;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static string GetAll_User_Str(int pDefaultBr)
        {
            // -1 is no branches, 0 is no default branch
            try
            {
                if (pDefaultBr < 0) return null;    
                var vals = GetAll_User_Ary(pDefaultBr);
                return string.Join(",", vals);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void Assigned(int pUserID, dynamic pBranch)
        {
            try
            {
                // see if the user has been assigned to this Branch
                var User = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT USER_CODE FROM OUSR WHERE INTERNAL_K = {pUserID}", null);
                var sql = $"SELECT BPLId FROM USR6 WHERE BPLId = {pBranch} AND UserCode = '{User}'";
                var ret = NSC_DI.UTIL.SQL.GetValue<string>(sql, null);
                if (ret == null) throw new System.ComponentModel.WarningException($"You have not been assigned to branch {pBranch}.");
            }
            catch (System.ComponentModel.WarningException ex)
            {
                throw new System.ComponentModel.WarningException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
