﻿using System;
using System.Collections.Generic;

namespace NSC_DI.SAP
{
    public class GoodsReceipt
    {
	    /// <summary>
	    /// Contains the ID or Code for the object that was recently created.
	    /// </summary>
	    private static int Key = 0;

        public static int Create(DateTime documentDate, List<GoodsReceipt_Lines> ListOfGoodsReceiptLines, string pRemarks = null, Dictionary<string, string> pUserDefinedFields = null)
	    {
		    // TODO: Write in support for "Service" type of Goods Receipt doucment  

            
		    try
		    {
			    // Grab the next available code from the SQL database
			    int NextDocNumber = Convert.ToInt32(UTIL.SQL.GetNextAvailableCodeForItem("OIGN", "DocNum"));

			    // Prepare a Goods Receipt document
			    SAPbobsCOM.Documents goodsReceipt = (SAPbobsCOM.Documents) Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);
			    goodsReceipt.DocDate    = documentDate;
			    goodsReceipt.DocType    = SAPbobsCOM.BoDocumentTypes.dDocument_Items;
			    goodsReceipt.DocNum     = NextDocNumber;
                goodsReceipt.Comments   = pRemarks;
                
                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null)
                    goodsReceipt.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                if (pUserDefinedFields != null)
                {
                    foreach (var key in pUserDefinedFields.Keys)
                    {                        
                        goodsReceipt.UserFields.Fields.Item(key).Value = pUserDefinedFields[key].ToString();                        
                    }
                }

                int br = -1;
                // For each item passed through the list of items
                foreach (GoodsReceipt_Lines GI_Line in ListOfGoodsReceiptLines)
			    {
                    if (br == -1)
                    {
                        // only want to check the 1st row
                        br = NSC_DI.SAP.Branch.Get(GI_Line.WarehouseCode);
                        if(br > 0) goodsReceipt.BPL_IDAssignedToInvoice = br;
                    }

                    goodsReceipt.Lines.ItemCode      = GI_Line.ItemCode;
				    goodsReceipt.Lines.Quantity      = GI_Line.Quantity;
                    goodsReceipt.Lines.Price         = GI_Line.Price;
                    goodsReceipt.Lines.WarehouseCode = GI_Line.WarehouseCode;
                    NSC_DI.SAP.Document.SetDefaultDistributionRules(goodsReceipt.Lines);

                    if (GI_Line.CropFinPRoj!=null && GI_Line.CropFinPRoj.ToString().Length > 0)
                    {
                        goodsReceipt.Lines.ProjectCode = GI_Line.CropFinPRoj;
                    }
                    
                    if (GI_Line.ListOfBatchLineItems != null && GI_Line.ListOfBatchLineItems.Count > 0)
				    {
					    foreach (var batchLineItem in GI_Line.ListOfBatchLineItems)
					    {
						    goodsReceipt.Lines.BatchNumbers.BatchNumber              = batchLineItem.BatchNumber;
						    goodsReceipt.Lines.BatchNumbers.Quantity                 = batchLineItem.Quantity;
                            goodsReceipt.Lines.BatchNumbers.ManufacturerSerialNumber = batchLineItem.ManufacturerSerialNumber;                            

                            if (batchLineItem.UserDefinedFields != null)
						    {
							    foreach (var key in batchLineItem.UserDefinedFields.Keys)
							    {
                                    //12404 was trying to convert a null to a string and getting an object reference error, created the if/else and ?. to handle that contingency
                                    var test = batchLineItem.UserDefinedFields[key]?.ToString();
                                    var testTwo = goodsReceipt.Lines.BatchNumbers.UserFields.Fields.Item(key);
                                    if (string.IsNullOrWhiteSpace(batchLineItem.UserDefinedFields[key]?.ToString()))// chose whitespace instead of empty because its going to be set to empty string                                        
                                            goodsReceipt.Lines.BatchNumbers.UserFields.Fields.Item(key).Value = "";
                                    else
                                        goodsReceipt.Lines.BatchNumbers.UserFields.Fields.Item(key).Value = batchLineItem.UserDefinedFields[key]?.ToString();
							    }
						    }
                            goodsReceipt.Lines.BatchNumbers.Add();
					    }
				    }

				    if (GI_Line.SerialNumbers != null)
				    {
					    foreach (var serialNumber in GI_Line.SerialNumbers)
					    {
						    goodsReceipt.Lines.SerialNumbers.InternalSerialNumber = serialNumber;
						    goodsReceipt.Lines.SerialNumbers.Quantity             = GI_Line.Quantity;
						    if (GI_Line.UserDefinedFields != null)
						    {
							    foreach (var key in GI_Line.UserDefinedFields.Keys)
							    {
                                    //12404 was trying to convert a null to a string and getting an object reference error, created the if/else and ?. to handle that contingency
                                    if (string.IsNullOrWhiteSpace(GI_Line.UserDefinedFields[key]?.ToString()))// chose whitespace instead of empty because its going to be set to empty string
                                        goodsReceipt.Lines.SerialNumbers.UserFields.Fields.Item(key).Value = "";
                                    else
                                        goodsReceipt.Lines.SerialNumbers.UserFields.Fields.Item(key).Value = GI_Line.UserDefinedFields[key]?.ToString();
							    }
						    }
						    goodsReceipt.Lines.SerialNumbers.Add();
					    }
				    }

				    goodsReceipt.Lines.Add();
			    }

			    // Attempt to add the new item to the database
			    if(goodsReceipt.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

			    string newCode = null;

			    // Get the newly created object's code
			    Globals.oCompany.GetNewObjectCode(out newCode);

			    if (newCode != null)
			    {
				    Key = Convert.ToInt32(newCode);
			    }
			    return Key;
		    }
		    catch (Exception ex)
		    {
			    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    GC.Collect();
		    }
	    }

	    public class GoodsReceipt_Lines
        {
            public string ItemCode { get; set; }
            public double Quantity { get; set; }
            public double Price { get; set; }
            public string WarehouseCode { get; set; }
            public List<BatchItem> ListOfBatchLineItems { get; set; }
            public IEnumerable<string> SerialNumbers { get; set; }
            public Dictionary<string, string> UserDefinedFields { get; set; }
            public string CropID { get; set; }
            public string StageID { get; set; }
            public string CropFinPRoj { get; set; }

            public class BatchItem
            {
                public string BatchNumber { get; set; }
                public double Quantity { get; set; }
                public string ManufacturerSerialNumber { get; set; }
                public string LotNumber { get; set; }
                public Dictionary<string, string> UserDefinedFields { get; set; }
                public string BatchNotes { get; set; }
                public DateTime ExpDate { get; set; }               
            }
        }
    }
}
