﻿using System.Collections.Generic;
using System.Data.SqlClient;
using SAPbobsCOM;
using System.Linq;
using System;

namespace NSC_DI.SAP
{
	public static class BillOfMaterials
    {
        public class BillOfMaterialLineItem
        {
	        public string ItemCode { get; set; }

	        public double Quantity { get; set; }

            public string WarehouseCode { get; set; }
        }


        /// <summary>
        /// Returns a list of item codes that are components to the item code that was passed.
        /// </summary>
        /// <param name="itemCode">The item code you want to know the components for.</param>
        /// <returns>A list of strings.  Each string represents a unique item component.</returns>
		public static List<BillOfMaterialLineItem> ListOfComponentsFromAnItemCode(string itemCode)
        {
            // Prepare a list of strings
            List<BillOfMaterialLineItem> listOfComponentsFromAnItemCode = new List<BillOfMaterialLineItem>();

            // Prepare the SQL statement for finding the product tree lines
            string SQL = @"
SELECT 
 [ITT1].[Code]
,  [ITT1].[Quantity]
, [ITT1].[Warehouse]
FROM
[ITT1]
WHERE
 [ITT1].[Father] = '" + (new SqlParameter("Father", itemCode)).Value.ToString() + @"'";

            // TODO-SAP-DIAPI: Replace the SQL method with BoBCOM interactions

            // Get the results from the SQL query
			Recordset ResultsFromSQL = UTIL.SQL.GetRecordSetFromSQLQuery(SQL);

            // Make sure results were returned from SQL
            if (ResultsFromSQL != null)
            {
                ResultsFromSQL.MoveFirst();
                // For each component found in the SQL results
                for (int i = 0; i < ResultsFromSQL.RecordCount; i++)
                {
                    listOfComponentsFromAnItemCode.Add(
                        new BillOfMaterialLineItem()
                        {
                            ItemCode = ResultsFromSQL.Fields.Item("Code").Value,
                            Quantity = ResultsFromSQL.Fields.Item("Quantity").Value,
                            WarehouseCode = ResultsFromSQL.Fields.Item("Warehouse").Value,
                        }
                    );
                    ResultsFromSQL.MoveNext();
                }
            }

            return listOfComponentsFromAnItemCode;
        }

        /// <summary>
        /// Returns a list of item codes that are components to the item code that was passed.
        /// </summary>
        /// <param name="itemCode">The item code you want to know the components for.</param>
        /// <returns>A list of strings.  Each string represents a unique item component.</returns>
		public static List<BillOfMaterialLineItem> ListOfBackflushComponentsFromAnItemCode(string itemCode)
        {
            // Prepare a list of strings
            List<BillOfMaterialLineItem> listOfComponentsFromAnItemCode = new List<BillOfMaterialLineItem>();

            // Prepare the SQL statement for finding the product tree lines
            string SQL = @"
SELECT 
 [ITT1].[Code]
,  [ITT1].[Quantity]
, [ITT1].[Warehouse]
FROM
[ITT1]
WHERE
 [ITT1].[Father] = '" + (new SqlParameter("Father", itemCode)).Value.ToString() + @"'
 AND [ITT1].[IssueMthd] = 'B'
";

            // TODO-SAP-DIAPI: Replace the SQL method with BoBCOM interactions

            // Get the results from the SQL query
			Recordset ResultsFromSQL = UTIL.SQL.GetRecordSetFromSQLQuery(SQL);

            // Make sure results were returned from SQL
            if (ResultsFromSQL != null)
            {
                ResultsFromSQL.MoveFirst();
                // For each component found in the SQL results
                for (int i = 0; i < ResultsFromSQL.RecordCount; i++)
                {
                    listOfComponentsFromAnItemCode.Add(
                        new BillOfMaterialLineItem()
                        {
                            ItemCode = ResultsFromSQL.Fields.Item("Code").Value,
                            Quantity = ResultsFromSQL.Fields.Item("Quantity").Value,
                            WarehouseCode = ResultsFromSQL.Fields.Item("Warehouse").Value,
                        }
                    );
                    ResultsFromSQL.MoveNext();
                }
            }

            return listOfComponentsFromAnItemCode;
        }

        public static IEnumerable<string> GetByProductNames(string pParentItemCode, bool allowFrozen = false)
        {
            try
            {
                var fzCount = UTIL.SQL.GetValue<int>("SELECT COUNT(*) FROM ITT1 AS G JOIN OITM AS I ON I.ItemCode = G.Father WHERE I.QryGroup49 = 'Y' AND I.QryGroup50 = 'Y'", null);

                if (fzCount > 0 && !allowFrozen) throw new B1Exception("Cannot have frozen for curing");

                var tipField = Items.Property.GetFieldName("Template");

                var sb = new System.Text.StringBuilder();

                sb.Append($"SELECT REPLACE(ItemName, '~', '') AS NAME FROM OITM WHERE {tipField} = 'Y'");

                sb.Append(" AND ");

                sb.Append($"ItemCode IN (SELECT code FROM ITT1 AS G WHERE LEFT(Code,1) = '~' AND Father = '{pParentItemCode}' AND G.Quantity < 0)");

                var qry = sb.ToString();

                var dt = UTIL.SQL.DataTable(qry);

                return dt.Select().Select(n => n["NAME"] as string);
            }
            catch (B1Exception ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static int GetLineNumberFromChildName(string pParentItemCode, string pChildName, bool addTemplateIdentifier = false)
        {
            try
            {
                if (addTemplateIdentifier) pChildName = "~" + pChildName;
                var qry = $"SELECT ChildNum FROM ITT1 AS G JOIN OITM AS I ON I.ItemCode = G.Code WHERE G.Father = '{pParentItemCode}' AND I.ItemName = '{pChildName}'";
                return UTIL.SQL.GetValue<int>(qry, -1);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static IEnumerable<string> GetTemplateCodes(bool pNoLock = true)
        {
            var nl = pNoLock ? "WITH (NOLOCK) " : string.Empty;
            var dt = UTIL.SQL.DataTable($"SELECT T.Code FROM OITT AS T {nl}JOIN OITM AS M ON T.Code = M.ItemCode WHERE M.QryGroup64 = 'Y'");
            return dt.Select().Select(n => n["Code"] as string);
        }

        public static void CopyTemplate(string pTemplateCode, Dictionary<string, UTIL.AutoStrain.AutoItemInfo> pItemMappings)
        {
            ProductTrees oBom = null;
            try
            {
                GetByKey(out oBom, pTemplateCode);

                // 9.3 has a bug where the DI does not replace the Item Name with the one from the Item Master for the parent item.
                //if (pTemplateCode.StartsWith("~")) oBom.ProductDescription = pItemMappings[pTemplateCode].ItemName;

                for (var i = 0; i < oBom.Items.Count; i++)
                {
                    oBom.Items.SetCurrentLine(i);
                    
                    if (oBom.Items.ItemCode.StartsWith("~"))
                    {
                        var qry = $"SELECT 1 FROM [@{Globals.tAutoItem}] WHERE U_ItemCode = '{oBom.Items.ItemCode}' AND (U_ManageBatch = 'Y' OR U_ManageSN = 'Y')";

                        var hasBatchSN = UTIL.SQL.DataTable(qry).Rows.Count > 0;

                        oBom.Items.IssueMethod = IsBOM(oBom.Items.ItemCode) || hasBatchSN ? BoIssueMethod.im_Manual : BoIssueMethod.im_Backflush;

                        oBom.Items.ItemCode = pItemMappings[oBom.Items.ItemCode].ItemCode;
                    }
                }

                var xmlDoc = B1Object.ToXmlDoc(oBom);

                UTIL.Misc.KillObject(oBom);

                var oCode = pItemMappings[pTemplateCode].ItemCode;

                xmlDoc.SelectSingleNode("//TreeCode").InnerText = oCode;

                foreach (System.Xml.XmlNode xn in xmlDoc.SelectNodes("//ParentItem"))
                {
                    xn.InnerText = oCode;
                }

                oBom = B1Object.FromXmlDoc(xmlDoc);

                oBom.TreeType = BoItemTreeTypes.iProductionTree;

                if (oBom.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
            }
            catch (Exception ex)
            {
                throw new Exception($"Cannot copy BOM{Environment.NewLine}Source: {pTemplateCode}{Environment.NewLine}{UTIL.Message.Format(ex)}");
            }
            finally
            {
                UTIL.Misc.KillObject(oBom);
                GC.Collect();
            }
        }
        public static bool IsBOM(string sItem)
        {
            try
            {
                if (string.IsNullOrEmpty(sItem)) throw new System.ArgumentNullException("sItem", "Not a valid BOM.");

                var SQL = "SELECT 1 FROM OITT WITH (NOLOCK) WHERE Code = '" + NSC_DI.UTIL.SQL.FixSQL(sItem) + "'";

                return NSC_DI.UTIL.SQL.DataTable(SQL).Rows.Count > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
        public static bool HasKey(string pCode, bool pNoLock = true)
        {
            try
            {
                var nl = pNoLock ? "WITH (NOLOCK) " : string.Empty;
                var dt = UTIL.SQL.DataTable($"SELECT 1 FROM OITT {nl}WHERE Code = '{pCode}'");
                return dt.Rows.Count == 1;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static void GetByKey(out SAPbobsCOM.ProductTrees pBom, string pCode)
        {
            try
            {
                pBom = Globals.oCompany.GetBusinessObject(BoObjectTypes.oProductTrees);
                GetByKey(pBom, pCode);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(ex.Message, ex);
            }
            catch (Exception ex)
            {

                throw new Exception(UTIL.Message.Format(ex));
            }
        }
        public static void GetByKey(SAPbobsCOM.ProductTrees pBom, string pCode)
        {
            try
            {
                var res = pBom.GetByKey(pCode);
                if (!res) throw new KeyNotFoundException(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }
    }
}
