﻿using System;

namespace NSC_DI.SAP
{
	public static class Disease 
    {
		public static void CreateDisease(string Code, string Name)
        {
            SAPbobsCOM.GeneralService	oGeneralService = null;
			SAPbobsCOM.GeneralData		oGeneralData	= null;

            // TODO: Check to make sure disease doesn't exist by name already.
            try
            {
				SAPbobsCOM.CompanyService oCompService = Globals.oCompany.GetCompanyService();

				oGeneralService = oCompService.GetGeneralService(Globals.tDisease + "_C");

                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

                oGeneralData.SetProperty("Code", Code);

                oGeneralData.SetProperty("U_Name", Name);

                oGeneralService.Add(oGeneralData);
            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
			}
        }

        /// <summary>
        /// Create a Relation between Compound and Effect.
        /// </summary>
        /// <param name="Code">The code of the new row</param>
        /// <param name="DiseaseCode">Disease Code</param>
        /// <param name="EffectCode">Effect Code</param>
		public static void CreateCompoundEffectRelation(string Code, string DiseaseCode, string EffectCode)
        {
			SAPbobsCOM.GeneralService	oGeneralService = null;
			SAPbobsCOM.GeneralData		oGeneralData	= null;
			
			try
            {
				SAPbobsCOM.CompanyService oCompService = Globals.oCompany.GetCompanyService();

				oGeneralService = oCompService.GetGeneralService(Globals.tDisease + "_C");

                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

                oGeneralData.SetProperty("Code", Code);

                oGeneralData.SetProperty("U_DiseaseCode", DiseaseCode);

                oGeneralData.SetProperty("U_EffectCode", EffectCode);

                oGeneralService.Add(oGeneralData);
            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
        }

		public static void RemoveDiseaseEffectRelation(string EffectCode, string DiseaseCode)
        {
            // Using a record set to delete a relation table record.
			SAPbobsCOM.Recordset oRecordSet = null;
			try
            {
                // Prepare to run a SQL statement.
				Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                // TODO-SAP-DIAPI: Replace the SQL method with BoBCOM interactions
                string sqlQuery = string.Format(@"
DELETE [@" + Globals.tDisease + @"]
FROM [@" + Globals.tDisease + @"]
WHERE [@" + Globals.tDisease + @"].[U_EffectCode] = {0}
AND [@" + Globals.tDisease + @"].[U_DiseaseCode] = {1}", EffectCode, DiseaseCode);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);
            }
            catch (Exception ex)
            {
                //TODO-LOG: Log error
				throw new Exception(UTIL.Message.Format(ex));
			}
        }
    }
}
