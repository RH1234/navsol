﻿using System;

namespace NSC_DI.SAP
{
    public static class BatchSerialTracking
    {
        public static string Load(string pItemCode, string pBatchSN, int pLevel = 0)
        {
            // load the Batch Serial Tracking Table

            SAPbobsCOM.UserTable oUDT = null;
            SAPbobsCOM.Recordset oRS = null;

            try
            {
                var sql = "";
                var managedBy = NSC_DI.SAP.Items.ManagedBy(pItemCode);

                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tBatSerTrace);

                if (managedBy == "B") sql = $"SELECT Name FROM {NSC_DI.UTIL.SQL.SetTableName(NSC_DI.Globals.tBatSerTrace)} WHERE U_ItemCode = '{pItemCode}' AND U_Batch = '{pBatchSN}'";
                if (managedBy == "S") sql = $"SELECT Name FROM {NSC_DI.UTIL.SQL.SetTableName(NSC_DI.Globals.tBatSerTrace)} WHERE U_ItemCode = '{pItemCode}' AND U_Serial = '{pBatchSN}'";
                var name = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
                sql = $"SELECT * FROM {NSC_DI.UTIL.SQL.SetTableName(NSC_DI.Globals.tBatSerTrace)} WHERE Name = '{name}' AND U_Level >= {pLevel}";
                oRS = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(sql);

                // delete the current records associated with the itemcode as a parent
                for (var i = 0; i < oRS.RecordCount; i++)
                {
                    if (oUDT.GetByKey(oRS.Fields.Item("Code").Value) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                    if (oUDT.Remove() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                    oRS.MoveNext();
                }

                // add the new records
                name = Guid.NewGuid().ToString();
                oUDT.Name = name;
                oUDT.UserFields.Fields.Item("U_Level").Value = pLevel;

                if (managedBy == "B")
                {
                    pBatchSN = BatchRec(oRS, oUDT, pBatchSN);
                    GRPO(oRS, oUDT, pBatchSN);
                }

                if (NSC_DI.SAP.Items.ManagedBy(pItemCode) == "S")
                {
                    // get the Production Order associated with Issue for the SN
                    sql = $@"
SELECT OSRI.BaseEntry AS PdO
  FROM SRI1
 INNER JOIN OSRI ON SRI1.SysSerial = OSRI.SysSerial AND SRI1.ItemCode = OSRI.ItemCode
 WHERE SRI1.BsDocType = 202 AND SRI1.BaseType = 60 AND OSRI.IntrSerial = N'{pBatchSN}'";
                    var PdO = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
                    //if(PdO == "") throw new Exception(NSC_DI.UTIL.Message.Format($"Serial Number {pBatchSN} not found."));
                    SerialRec(oRS, oUDT, PdO, pBatchSN);
                }

                return name;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oRS);
                NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        private static string BatchRec(SAPbobsCOM.Recordset oRS, SAPbobsCOM.UserTable oUDT, string pBatch)
        {
            // load the Batch - keep track of the last Batch Num throught the return value

            var retVal = pBatch;

            try
            {
                var sql = $@"
SELECT IBT1.ItemCode, IBT1.ItemName, IBT1.BatchNum, '' AS SerialNumber, IBT1.BaseNum AS [Receipt], IBT1.BsDocEntry AS [PdO], 1 AS [Level], T1.BatchNum AS [Parent]
  FROM IBT1
 INNER JOIN IBT1 T1 ON T1.BsDocEntry = IBT1.BsDocEntry AND T1.BaseType = 60 AND T1.BsDocType = 202
 WHERE IBT1.BaseType = 59 AND IBT1.BsDocType = 202 AND IBT1.BatchNum = N'{pBatch}'";
                oRS = UTIL.SQL.GetRecordSetFromSQLQuery(sql);
                if (oRS.RecordCount < 1)
                {
                    // no results for the previous query, so move onto the next query
                    // this gets the last batch number in the current trail section. there may be SNs next.
                    sql = $@"
SELECT IBT1.ItemCode, IBT1.ItemName, IBT1.BatchNum, '' AS SerialNumber, IBT1.BaseNum AS [Receipt], IBT1.BsDocEntry AS[PdO], 4 AS[Level], '' AS[Parent]
 FROM IBT1
WHERE IBT1.BaseType = 59 AND IBT1.BsDocType = 202 AND IBT1.BatchNum = N'{pBatch}'";
                    oRS = UTIL.SQL.GetRecordSetFromSQLQuery(sql);

                    if (oRS.RecordCount < 1)
                    {
                        sql = $@"SELECT BsDocEntry FROM IBT1 WHERE BaseType = 59 AND BaseEntry = {oRS.Fields.Item("Receipt").Value}";
                        var PdO = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
                        if (PdO != "") SerialRec(oRS, oUDT, PdO);
                        return retVal;
                    }
                }

                int level = oUDT.UserFields.Fields.Item("U_Level").Value + 1;

                oUDT.UserFields.Fields.Item("U_ItemCode").Value = oRS.Fields.Item("ItemCode").Value;
                oUDT.UserFields.Fields.Item("U_ItemName").Value = oRS.Fields.Item("ItemName").Value;
                oUDT.UserFields.Fields.Item("U_Batch").Value    = oRS.Fields.Item("BatchNum").Value;
                oUDT.UserFields.Fields.Item("U_Serial").Value   = oRS.Fields.Item("SerialNumber").Value;
                oUDT.UserFields.Fields.Item("U_Receipt").Value  = oRS.Fields.Item("Receipt").Value;
                oUDT.UserFields.Fields.Item("U_PDO").Value      = oRS.Fields.Item("PDO").Value;
                oUDT.UserFields.Fields.Item("U_Level").Value    = level;

                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                var nextBatch = oRS.Fields.Item("Parent").Value;
                if (nextBatch == oRS.Fields.Item("BatchNum").Value) return retVal; // end of the trip
                if (nextBatch == "")                          
                {
                    // end of the current trail section.
                    // there may be SNs next.
                    sql = $@"SELECT BsDocEntry FROM IBT1 WHERE BaseType = 59 AND BaseEntry = {oRS.Fields.Item("Receipt").Value}";
                    var PdO = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
                    if (PdO != "") SerialRec(oRS, oUDT, PdO);
                    return retVal;                                  // end of the trip
                }

                BatchRec(oRS, oUDT, nextBatch);

                return retVal;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void SerialRec(SAPbobsCOM.Recordset oRS, SAPbobsCOM.UserTable oUDT, string pPdO, string pSN = "")
        {
            // load the Serial 

            try
            {
                var sql = $@"
SELECT SRI1.ItemCode, SRI1.ItemName, '' AS BatchNum, OSRI.IntrSerial AS SerialNumber, T1.BaseEntry AS [Receipt], T1.BsDocEntry AS [PdO], 5 AS [Level], '' AS [Parent]
  FROM SRI1 
 INNER JOIN SRI1 T1 ON SRI1.SysSerial = T1.SysSerial AND SRI1.ItemCode = T1.ItemCode  AND T1.BaseType = 59
 INNER JOIN OSRI ON SRI1.SysSerial = OSRI.SysSerial AND SRI1.ItemCode = OSRI.ItemCode
 WHERE SRI1.BsDocType = 202 AND SRI1.BsDocEntry = '{pPdO}'
 ORDER BY SRI1.SysSerial";
                oRS = UTIL.SQL.GetRecordSetFromSQLQuery(sql);
                if (oRS.RecordCount < 1)
                {
                    // no results for the query - see if there is a goods receipt
                    sql = $@"
SELECT SRI1.ItemCode, SRI1.ItemName, '' AS BatchNum, OSRI.IntrSerial AS SerialNumber, T1.BaseEntry AS [Receipt], T1.BsDocEntry AS [PdO], 0 AS [Level], '' AS [Parent]
  FROM SRI1 
 INNER JOIN SRI1 T1 ON SRI1.SysSerial = T1.SysSerial AND SRI1.ItemCode = T1.ItemCode -- AND T1.BaseType = 59
 INNER JOIN OSRI ON SRI1.SysSerial = OSRI.SysSerial AND SRI1.ItemCode = OSRI.ItemCode
 WHERE SRI1.BsDocType = 202 AND OSRI.IntrSerial = '{pSN}' AND T1.BaseType = 59 
 ORDER BY SRI1.SysSerial";
                    oRS = UTIL.SQL.GetRecordSetFromSQLQuery(sql);
                    //return;
                }

                int level = oUDT.UserFields.Fields.Item("U_Level").Value + 1;

                for (var i = 0; i < oRS.RecordCount; i++)
                {
                    if (i > 0) oRS.MoveNext();
                    oUDT.UserFields.Fields.Item("U_ItemCode").Value = oRS.Fields.Item("ItemCode").Value;
                    oUDT.UserFields.Fields.Item("U_ItemName").Value = oRS.Fields.Item("ItemName").Value;
                    oUDT.UserFields.Fields.Item("U_Batch").Value    = oRS.Fields.Item("BatchNum").Value;
                    oUDT.UserFields.Fields.Item("U_Serial").Value   = oRS.Fields.Item("SerialNumber").Value;
                    oUDT.UserFields.Fields.Item("U_Receipt").Value  = oRS.Fields.Item("Receipt").Value;
                    oUDT.UserFields.Fields.Item("U_PDO").Value      = oRS.Fields.Item("PdO").Value;
                    oUDT.UserFields.Fields.Item("U_Level").Value    = level;

                    if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }
                // get the batch to re-start the batch query trail
                sql = $@"SELECT BatchNum FROM IBT1 WHERE BaseType = 59 AND BaseEntry = {oRS.Fields.Item("Receipt").Value}";
                var nextBatch = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
                //if (nextBatch == oRS.Fields.Item("BatchNum").Value) return ""; // end of the trip
                //if (nextBatch == "") return pBatch;                         // this gets the last batch number in the current batch trail. there may be SNs next.

                BatchRec(oRS, oUDT, nextBatch);
                return;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oRS);
                NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        private static void GRPO(SAPbobsCOM.Recordset oRS, SAPbobsCOM.UserTable oUDT, string pBatch)
        {
            // load the Goods Receipt Purchase Order 

            try
            {
                var sql = $@"
SELECT OIBT.ItemCode, OIBT.ItemName, OIBT.BatchNum, '' AS SerialNumber, OIBT.BaseNum AS [Receipt], 0 AS [PdO], '' AS [Parent]
  FROM OIBT
 WHERE OIBT.BaseType = 20 AND OIBT.BatchNum = N'{pBatch}'";
                oRS = UTIL.SQL.GetRecordSetFromSQLQuery(sql);
                if (oRS.RecordCount < 1) return;

                int level = oUDT.UserFields.Fields.Item("U_Level").Value + 1;

                oUDT.UserFields.Fields.Item("U_ItemCode").Value = oRS.Fields.Item("ItemCode").Value;
                oUDT.UserFields.Fields.Item("U_ItemName").Value = oRS.Fields.Item("ItemName").Value;
                oUDT.UserFields.Fields.Item("U_Batch").Value    = oRS.Fields.Item("BatchNum").Value;
                oUDT.UserFields.Fields.Item("U_Serial").Value   = oRS.Fields.Item("SerialNumber").Value;
                oUDT.UserFields.Fields.Item("U_Receipt").Value  = oRS.Fields.Item("Receipt").Value;
                oUDT.UserFields.Fields.Item("U_PDO").Value      = oRS.Fields.Item("PDO").Value;
                oUDT.UserFields.Fields.Item("U_Level").Value    = level;

                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                return;                                  // end of the trip
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
