﻿using System;
using System.Collections.Generic;
using SAPbobsCOM;

namespace NSC_DI.SAP
{
	public class Test 
    {
		public static void CreateTest(string Code, string Name)
        {
			GeneralService oGeneralService = null;
			GeneralData oGeneralData = null;

            try
            {
                var oCompService = Globals.oCompany.GetCompanyService();

                oGeneralService = oCompService.GetGeneralService(Globals.tTests + "_C");

                oGeneralData = oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);

                oGeneralData.SetProperty("Code", Code);

                oGeneralData.SetProperty("U_Name", Name);

                oGeneralService.Add(oGeneralData);
            }
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(oGeneralService);
				UTIL.Misc.KillObject(oGeneralData);

				GC.Collect();
			}
		}

		public static bool ValidateTestExists(string code)
		{
			UserTable sboTable = null;
            try
            {
				sboTable = Globals.oCompany.UserTables.Item(Globals.tTests);
                if(sboTable.GetByKey(code) == false) return false;

                string validCheck = sboTable.UserFields.Fields.Item("U_Name").Value.ToString();
                return true;
            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(sboTable);
				GC.Collect();
			}
		}
		
		public static void MapTests(ref API.TestResults.StateTestResults StateTestResults)
				{
/*	// COMPLIANCE
 * StateTestResults.DefaultTestsMap = new TestResults.StateTestResults.DefaultTestPopulateRequest();
					StateTestResults.DefaultTestsMap.ControllerTest = this;

					var testId = FindTestByName("Moisture", "WAState");
					if (!string.IsNullOrEmpty(testId))
					{
						StateTestResults.DefaultTestsMap.MoistureTableMap = new TestResults.StateTestResults.DefaultTestPopulateRequest.MoistureTestUpdate()
						{
							MoistureTestId = testId,
							MoistureAmountFieldId = ""
						};

						foreach (var testColumn in GetTestColumnList(testId))
						{
							if(testColumn.Name.Contains("Moisture"))
							{
								StateTestResults.DefaultTestsMap.MoistureTableMap.MoistureAmountFieldId = testColumn.Code;
							}
						}
					}

					testId = FindTestByName("Potency", "WAState");
					if (!string.IsNullOrEmpty(testId))
					{
						StateTestResults.DefaultTestsMap.PotencyTableMap = new TestResults.StateTestResults.DefaultTestPopulateRequest.PotencyTestUpdate()
						{
							PotencyTestId = testId,
							PotencyFieldId_THCA = "",
							PotencyFieldId_THC = "",
							PotencyFieldId_CDB = "",
							PotencyFieldId_TotalTHC = ""
						};

						foreach (var testColumn in GetTestColumnList(testId))
						{
							if (testColumn.Name.Contains("THCA"))
							{
								StateTestResults.DefaultTestsMap.PotencyTableMap.PotencyFieldId_THCA = testColumn.Code;
							}
							else if (testColumn.Name.Contains("Total"))
							{
								StateTestResults.DefaultTestsMap.PotencyTableMap.PotencyFieldId_TotalTHC = testColumn.Code;
							}
							else if (testColumn.Name.Contains("THC"))
							{
								StateTestResults.DefaultTestsMap.PotencyTableMap.PotencyFieldId_THC = testColumn.Code;
							}
							else if (testColumn.Name.Contains("CBD"))
							{
								StateTestResults.DefaultTestsMap.PotencyTableMap.PotencyFieldId_CDB = testColumn.Code;
							}
						}   
					}

					testId = FindTestByName("Foreign", "WAState");
					if (!string.IsNullOrEmpty(testId))
					{
						StateTestResults.DefaultTestsMap.ForeignMatterTableMap = new TestResults.StateTestResults.DefaultTestPopulateRequest.ForeignMatterTestUpdate()
						{
							ForeignMatterTestId = testId,
							ForeignMatterFieldId_Stems = "",
							ForeignMatterFieldId_Other = ""
						};

						foreach (var testColumn in GetTestColumnList(testId))
						{
							if (testColumn.Name.Contains("Stem"))
							{
								StateTestResults.DefaultTestsMap.ForeignMatterTableMap.ForeignMatterFieldId_Stems = testColumn.Code;
							}
							else if (testColumn.Name.Contains("Other"))
							{
								StateTestResults.DefaultTestsMap.ForeignMatterTableMap.ForeignMatterFieldId_Other = testColumn.Code;
							}
						}   
					}

					testId = FindTestByName("Micro", "WAState");
					if (!string.IsNullOrEmpty(testId))
					{
						StateTestResults.DefaultTestsMap.MicrobiologicalTableMap = new TestResults.StateTestResults.DefaultTestPopulateRequest.MicrobiologicalTestUpdate()
						{
							MicrobiologicalTestId = testId,
							MicrobiologicalFieldId_AerobicBacteria = "",
							MicrobiologicalFieldId_BileTolerant = "",
							MicrobiologicalFieldId_Coliforms = "",
							MicrobiologicalFieldId_EColiAndSalonella = "",
							MicrobiologicalFieldId_YeastAndMold = ""
						};

						foreach (var testColumn in GetTestColumnList(testId))
						{
							if (testColumn.Name.Contains("Aerobic"))
							{
								StateTestResults.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_AerobicBacteria = testColumn.Code;
							}
							else if (testColumn.Name.Contains("Yeast"))
							{
								StateTestResults.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_YeastAndMold = testColumn.Code;
							}
							else if (testColumn.Name.Contains("Coliform"))
							{
								StateTestResults.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_Coliforms = testColumn.Code;
							}
							else if (testColumn.Name.Contains("Bile"))
							{
								StateTestResults.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_BileTolerant = testColumn.Code;
							}
							else if (testColumn.Name.Contains("Ecoli"))
							{
								StateTestResults.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_EColiAndSalonella = testColumn.Code;
							}
						}  
					}

					testId = FindTestByName("Residual", "WAState");
					if (!string.IsNullOrEmpty(testId))
					{
						StateTestResults.DefaultTestsMap.ResidualSolventTableMap = new TestResults.StateTestResults.DefaultTestPopulateRequest.ResidualSolventTestUpdate()
						{
							ResidualSolventTestId = testId,
							ResidualSolventFieldId_ResidualSolvent = ""                    
						};

						foreach (var testColumn in GetTestColumnList(testId))
						{
							if (testColumn.Name.Contains("Residual"))
							{
								StateTestResults.DefaultTestsMap.ResidualSolventTableMap.ResidualSolventFieldId_ResidualSolvent = testColumn.Code;
							}
						}  
					}
*/
				}
		
		public static string FindTestByName(string nameOfTest, string testVendor)
        {
            var result = "";

            var sqlQuery = string.Format(@"
SELECT [@VSC_TEST].[Code] AS [TestId]
	, [@VSC_TEST].[U_VSC_Name] AS [TestName] 
	, [@VSC_TEST].[U_VSC_Vendor]
FROM [@VSC_TEST]
WHERE [@VSC_TEST].[U_VSC_Name] LIKE '%{0}%'
	AND  [@VSC_TEST].[U_VSC_Vendor] = '{1}'", nameOfTest, testVendor);


			var dbCommunication = (IRecordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            try
            {
                dbCommunication.DoQuery(sqlQuery);
                dbCommunication.MoveFirst();
                if(dbCommunication.RecordCount >= 1)
                {
                    result = dbCommunication.Fields.Item("TestId").Value.ToString();
                }
            }
            catch(Exception ex)
            {

            }
            return result;
        }

		public static List<TestColumnResult> GetTestColumnList(string testCodeNumber)
        {
            var result = new List<TestColumnResult>();

            var sqlQuery = string.Format(@"
SELECT [@VSC_COMPOUND].[U_VSC_Name] AS [Name], [@VSC_COMPOUND].[Code] AS [Code]
FROM [@VSC_TEST_COMPOUND]
JOIN [@VSC_COMPOUND] ON [@VSC_COMPOUND].Code = [@VSC_TEST_COMPOUND].[U_VSC_CompoundCode]
WHERE [@VSC_TEST_COMPOUND].[U_VSC_TestCode] = '{0}'

UNION ALL

SELECT [@VSC_TESTFIELD].[U_VSC_Name] AS [Name] , [@VSC_TESTFIELD].[Code] AS [Code]
FROM [@VSC_TEST_TESTFIELD]
JOIN [@VSC_TESTFIELD] ON [@VSC_TESTFIELD].Code = [@VSC_TEST_TESTFIELD].[U_VSC_TestFieldCode]
WHERE [@VSC_TEST_TESTFIELD].[U_VSC_TestCode] = '{0}'", testCodeNumber);

			var dbCommunication = (IRecordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            try
            {
                dbCommunication.DoQuery(sqlQuery);
                dbCommunication.MoveFirst();
                for(var i = 0; i <= dbCommunication.RecordCount; i++)
                {
                    var newColumn = new TestColumnResult()
                    {
                        Code = dbCommunication.Fields.Item("Code").Value.ToString(), 
                        Name = dbCommunication.Fields.Item("Name").Value.ToString()
                    };
                    result.Add(newColumn);
                    dbCommunication.MoveNext();
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public class TestColumnResult
        {
            public string Name { get; set; }
            public string Code { get; set; }
        }

        public class TestResultUpdate
        {
            public string BatchId { get; set; }
            public string SerialId { get; set; }

            public bool QAPassed { get; set; }
        }

        #region Test - Test Field Relation Actions

        /// <summary>
        /// Create a Relation between Test and TestField
        /// </summary>
        /// <param name="Code">The code of the new row</param>
        /// <param name="TestCode">Test Code</param>
        /// <param name="TestFieldCode">Test Field Code</param>
		public static void CreateTestFieldRelation(string Code, string TestCode, string TestFieldCode, string Group)
        {
			GeneralService oGeneralService = null;
			GeneralData oGeneralData = null;

            try
            {
                var oCompService = Globals.oCompany.GetCompanyService();
				oGeneralService = oCompService.GetGeneralService(Globals.tTestTestField + "_C");
                oGeneralData = oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);

                oGeneralData.SetProperty("Code", Code);
                oGeneralData.SetProperty("U_TestCode", TestCode);
                oGeneralData.SetProperty("U_TestFieldCode", TestFieldCode);
                oGeneralData.SetProperty("U_Group", Group);
                oGeneralService.Add(oGeneralData);
            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(oGeneralService);
				UTIL.Misc.KillObject(oGeneralData);
				GC.Collect();
			}
		}

        /// <summary>
        /// Removes an Existing Relation between a Test and a Test Field
        /// </summary>
        /// <param name="TestCode">Test Code Identifier</param>
        /// <param name="CompoundCode">Test Field Codde Identifier</param>
		public static void RemoveTestFieldRelation(string TestCode, string TestFieldCode, string Group)
        {
            // Using a record set to delete a relation table record.
	        Recordset oRecordSet = null;
            try
            {
                // Prepare to run a SQL statement.
				oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                var sqlQuery = $"DELETE FROM [@{Globals.tTestTestField}] WHERE U_TestCode = '{TestCode}' AND U_TestFieldCode = '{TestFieldCode}' AND U_Group = '{Group}'";

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);
            }
            catch (Exception ex)
            {
                //TODO-LOG: Log error
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(oRecordSet);
				GC.Collect();
			}
		}
        #endregion

        #region Test - Compound Relation Actions
        /// <summary>
        /// Create a Relation between Test and Compound
        /// </summary>
        /// <param name="Code">The code of the new row</param>
        /// <param name="TestCode">Test Code</param>
        /// <param name="CompoundCode">Compound Code</param>
		public static void CreateCompoundRelation(string Code, string TestCode, string CompoundCode)
        {
			GeneralService oGeneralService = null;
			GeneralData oGeneralData = null;

            try
            {
                var oCompService = Globals.oCompany.GetCompanyService();
				oGeneralService = oCompService.GetGeneralService(Globals.tTestComp + "_C");
                oGeneralData = oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);

                oGeneralData.SetProperty("Code", Code);
                oGeneralData.SetProperty("U_TestCode", TestCode);
                oGeneralData.SetProperty("U_CompoundCode", CompoundCode);

                oGeneralService.Add(oGeneralData);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex) + Environment.NewLine + "CODE = " + Code);
			}
			finally
			{
				UTIL.Misc.KillObject(oGeneralService);
				UTIL.Misc.KillObject(oGeneralData);

				GC.Collect();
			}
		}

        /// <summary>
        /// Removes an Existing Relation between a Test and a Compound
        /// </summary>
        /// <param name="TestCode">Test Code Identifier</param>
        /// <param name="CompoundCode">Compound Codde Identifier</param>
		public static void RemoveCompoundRelation(string TestCode, string CompoundCode)
        {
            // Using a record set to delete a relation table record.
            try
            {
                // Prepare to run a SQL statement.
				var oRecordSet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                var sqlQuery = string.Format(@"
        DELETE [@" + Globals.tTestComp + @"]
        FROM [@" + Globals.tTestComp + @"]
        WHERE [@" + Globals.tTestComp + @"].[U_TestCode] = '{0}'
        AND [@" + Globals.tTestComp + @"].[U_CompoundCode] = '{1}'", TestCode, CompoundCode);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);
            }
            catch (Exception ex)
            {
                //TODO-LOG: Log error
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				//UTIL.Misc.KillObject(oRecordSet);
				GC.Collect();
			}
		}
        #endregion

        #region Test - Sample Relation Actions
        /// <summary>
        /// Create a Relation between Test and a Sample
        /// </summary>
        /// <param name="Code">The code of the new row</param>
        /// <param name="TestCode">Test Code</param>
        /// <param name="SampleCode">SampleId - Batch Number</param>
		public static void CreateSampleRelation(string Code, string TestCode, string SampleId, string pRecipientType = null, string pRecipientCode = null, string pSampleItem = null, string pSampledItem = null, string pSampledBatch = null)
        {
			GeneralService oGeneralService = null;
			GeneralData oGeneralData = null;

            try
            {
                var oCompService = Globals.oCompany.GetCompanyService();

				oGeneralService = oCompService.GetGeneralService(Globals.tTestSamp + "_C");

                oGeneralData = oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);

                oGeneralData.SetProperty("Code", Code);
                oGeneralData.SetProperty("U_TestCode", TestCode);
                oGeneralData.SetProperty("U_SampleCode", SampleId);
                if(!NSC_DI.UTIL.Strings.Empty(pSampleItem)) oGeneralData.SetProperty("U_SampleItem", pSampleItem);
                if(!NSC_DI.UTIL.Strings.Empty(pSampledItem)) oGeneralData.SetProperty("U_SampledItem", pSampledItem);
                if(!NSC_DI.UTIL.Strings.Empty(pSampledBatch)) oGeneralData.SetProperty("U_SampledBatch", pSampledBatch);
                if (pRecipientType != null && pRecipientCode != null)
                {
                    oGeneralData.SetProperty("U_RecipientType", pRecipientType);
                    oGeneralData.SetProperty("U_RecipientCode", pRecipientCode);
                }
                oGeneralService.Add(oGeneralData);
            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(oGeneralService);
				UTIL.Misc.KillObject(oGeneralData);

				GC.Collect();
			}
		}

        /// <summary>
        /// Removes an Existing Relation between a Test and a Sample
        /// </summary>
        /// <param name="TestCode">Test Code Identifier</param>
        /// <param name="SampleCode">Sample Codde Identifier</param>
		public static void RemoveSampleRelation(string TestCode, string SampleCode)
        {
            // Using a record set to delete a relation table record.
            try
            {
                // Prepare to run a SQL statement.
				Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                var sqlQuery = string.Format(@"
DELETE [@" + Globals.tTestSamp + @"]
FROM [@" + Globals.tTestSamp + @"]
WHERE [@" + Globals.tTestSamp + @"].[U_TestCode] = '{0}'
AND [@" + Globals.tTestSamp + @"].[U_SampleCode] = '{1}'", TestCode, SampleCode);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);
            }
            catch (Exception ex)
            {
                //TODO-LOG: Log error
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				//UTIL.Misc.KillObject(oRecordSet);
				GC.Collect();
			}
		}

        public static void UpdateQC_onBatch(string BatchID, string QC_State, string pLabID)
        {
            CompanyService oCoService = null;
            BatchNumberDetailsService oBatchDetailService = null;
            BatchNumberDetailParams oBatchParam = null;
            BatchNumberDetail oBatchDetails = null;
            SAPbobsCOM.Recordset oRecordSet = null;
            int intDocEntry = 0;

            try
            {
                oCoService = Globals.oCompany.GetCompanyService();
                oBatchDetailService = oCoService.GetBusinessService(ServiceTypes.BatchNumberDetailsService);
                oBatchParam = oBatchDetailService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);

                oRecordSet = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordSet = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery("Select AbsEntry from [OBTN] where U_NSC_LabTestID='"+ pLabID + "'");
                if(oRecordSet.RecordCount==0)
                {
                    return;
                }
                else
                {
                    for(int i=0;i< oRecordSet.RecordCount;i++)
                    {
                        if(i==0)
                        {
                            oRecordSet.MoveFirst();
                        }
                        else
                        {
                            oRecordSet.MoveNext();
                        }

                        oBatchParam.DocEntry = oRecordSet.Fields.Item("AbsEntry").Value;
                        oBatchDetails = oBatchDetailService.Get(oBatchParam);
                        oBatchDetails.UserFields.Item("U_NSC_PassedQA").Value = QC_State;
                        oBatchDetailService.Update(oBatchDetails);
                    }

                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(oBatchDetails);
                UTIL.Misc.KillObject(oBatchDetailService);
                UTIL.Misc.KillObject(oBatchParam);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oRecordSet);

                GC.Collect();
            }
        }
		public static void UpdateCompletionStatus(string TestCode, string SampleCode, string completeState)
        {
            try
            {

                // Prepare to run a SQL statement.
                Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                var sqlQuery = string.Format("UPDATE [@" + Globals.tTestSamp + "] SET U_IsComplete = '" + completeState + "' FROM [@" + Globals.tTestSamp + "] WHERE U_TestCode = '{0}' AND U_SampleCode = '{1}'", TestCode, SampleCode);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				//UTIL.Misc.KillObject(oRecordSet);
				GC.Collect();
			}
		}
        #endregion

        #region Test - Test Sample Compound Result Relation Actions

		public static string GetNextTestCompoundResultCode()
        {
            var result = "";
			IRecordset dbCommunication = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            try
            {
                // Count how many current records exist within the database.
	            dbCommunication.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + Globals.tTCResult + "]");

                // Set the textbox for the ID to the pre-determined number.
                result = dbCommunication.Fields.Item(0).Value.ToString();
            }
            catch (Exception ex)
            {
                result = "";
            }

            return result;
        }

		public static bool ValidateTestCompoundResultExists(string TestSampleCode, string TestFieldCode)
        {
            var result = false;

            var sqlQuery = string.Format("SELECT TOP 1 U_CompoundCode FROM [@" + Globals.tTCResult + @"] WHERE U_TestSampleCode = '{0}' AND U_CompoundCode = '{1}'", TestSampleCode, TestFieldCode);

			var dbCommunication = (IRecordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            try
            {
                dbCommunication.DoQuery(sqlQuery);
                dbCommunication.MoveFirst();
                if (dbCommunication.RecordCount > 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Create a TestSample Compound Result relation.
        /// </summary>
        /// <param name="Code">The code of the new row</param>
        /// <param name="TestSampleCode">Test Sample Code</param>
        /// <param name="CompoundCode">Compound Code</param>
		public static void CreateTestCompoundResult(string Code, string TestSampleCode, string CompoundCode, string Value = "")
        {
			GeneralService oGeneralService = null;
			GeneralData oGeneralData = null;

            try
            {
                var oCompService = Globals.oCompany.GetCompanyService();

				oGeneralService = oCompService.GetGeneralService(Globals.tTCResult + "_C");

                oGeneralData = oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);

                oGeneralData.SetProperty("Code", Code);

                oGeneralData.SetProperty("U_TestSampleCode", TestSampleCode);

                oGeneralData.SetProperty("U_CompoundCode", CompoundCode);

                if (!string.IsNullOrEmpty(Value))
                {
                    oGeneralData.SetProperty("U_Value", Value);
                }

                oGeneralService.Add(oGeneralData);
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(oGeneralService);
				UTIL.Misc.KillObject(oGeneralData);

				GC.Collect();
			}
		}

		public static void UpdateTestCompoundResult(string TestSampleCode, string CompoundCode, string Value)
        {
            // Using a record set to Update our relation table record.
            try
            {
                // Prepare to run a SQL statement.
				Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                var sqlQuery = string.Format(@"
        UPDATE [@" + Globals.tTCResult + @"]
        SET U_Value = '{0}'
        FROM [@" + Globals.tTCResult + @"]
        WHERE [@" + Globals.tTCResult + @"].U_TestSampleCode = '{1}'
        AND [@" + Globals.tTCResult + @"].U_CompoundCode = '{2}'", Value, TestSampleCode, CompoundCode);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				//UTIL.Misc.KillObject(oRecordSet);
				GC.Collect();
			}
		}

        /// <summary>
        /// Removes an Existing Relation between a Test Compound Result.
        /// </summary>
        /// <param name="TestSampleCode">Test Sample Code</param>
        /// <param name="SampleCode">Compound Code</param>
		public static void RemoveTestCompoundResult(string TestSampleCode, string CompoundCode)
        {
            // Using a record set to delete a relation table record.
            try
            {
                // Prepare to run a SQL statement.
				Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                var sqlQuery = string.Format(@"
        DELETE [@" + Globals.tTCResult + @"
        FROM [@" + Globals.tTCResult + @"
         WHERE [@" + Globals.tTCResult + @".U_TestSampleCode = '{0}'
        AND U_CompoundCode = '{1}'", TestSampleCode, CompoundCode);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				//UTIL.Misc.KillObject(oRecordSet);
				GC.Collect();
			}
		}
        #endregion

        #region Test - Test Sample Test Field Result Relation Actions

		public static string GetNextTestFieldResultCode()
        {
            var result = "";
            IRecordset dbCommunication = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            try
            {
                // Count how many current records exist within the database.
	            dbCommunication.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + Globals.tTFResult + "]");

                // Set the textbox for the ID to the pre-determined number.
                result = dbCommunication.Fields.Item(0).Value.ToString();
            }
            catch (Exception ex)
            {
                result = "";
            }

            return result;
        }

		public static bool ValidateTestFieldResultExists(string TestSampleCode, string TestFieldCode)
        {
            var result = false;

            var sqlQuery = string.Format("SELECT TOP 1 U_FieldCode FROM [@" + Globals.tTFResult + "] WHERE U_TestSampleCode = '{0}' AND U_FieldCode] = '{1}'", TestSampleCode, TestFieldCode);

            IRecordset dbCommunication = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            try
            {
                dbCommunication.DoQuery(sqlQuery);
                dbCommunication.MoveFirst();
                if (dbCommunication.RecordCount > 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Create a TestSample TestField Result relation.
        /// </summary>
        /// <param name="Code">The code of the new row</param>
        /// <param name="TestSampleCode">Test Sample Code</param>
        /// <param name="TestFieldCode">Test Field Code</param>
		public static void CreateTestFieldResult(string Code, string TestSampleCode, string TestFieldCode, string Value = "")
        {
			GeneralService oGeneralService = null;
			GeneralData oGeneralData = null;

            try
            {
                var oCompService = Globals.oCompany.GetCompanyService();
				oGeneralService = oCompService.GetGeneralService(Globals.tTFResult + "_C");
                oGeneralData = oGeneralService.GetDataInterface(GeneralServiceDataInterfaces.gsGeneralData);

                oGeneralData.SetProperty("Code", Code);
                oGeneralData.SetProperty("U_TestSampleCode", TestSampleCode);
                oGeneralData.SetProperty("U_FieldCode", TestFieldCode);

                if (!string.IsNullOrEmpty(Value))
                {
                    oGeneralData.SetProperty("U_Value", Value);
                }

                oGeneralService.Add(oGeneralData);
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(oGeneralService);
				UTIL.Misc.KillObject(oGeneralData);

				GC.Collect();
			}
		}

		public static void UpdateTestFieldResult(string TestSampleCode, string TestFieldCode, string Value)
        {
            // Using a record set to Update our relation table record.
            try
            {
                // Prepare to run a SQL statement.
                Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

				var sqlQuery = string.Format("UPDATE [@" + Globals.tTFResult + "] SET U_Value = '{0}' FROM [@" + Globals.tTFResult + "] WHERE U_TestSampleCode = '{1}' AND U_FieldCode = '{2}'", Value, TestSampleCode, TestFieldCode);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				//UTIL.Misc.KillObject(oRecordSet);
				GC.Collect();
			}
		}

        /// <summary>
        /// Removes an Existing Relation between a Test TestField Result.
        /// </summary>
        /// <param name="TestSampleCode">Test Sample Code</param>
        /// <param name="TestFieldCode">Test Field Code</param>
		public static void RemoveTestFieldResult(string TestSampleCode, string TestFieldCode)
        {
            // Using a record set to delete a relation table record.
            try
            {
                // Prepare to run a SQL statement.
                Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                var sqlQuery = string.Format("DELETE [@" + Globals.tTFResult + "] FROM [@" + Globals.tTFResult + "] WHERE U_TestSampleCode = '{0}' AND U_FieldCode = '{1}'", TestSampleCode, TestFieldCode);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				//UTIL.Misc.KillObject(oRecordSet);
				GC.Collect();
			}
		}
        #endregion
    }
}
