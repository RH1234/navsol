﻿using System;
using System.Data.SqlClient;

namespace NSC_DI.SAP
{
	public static class ItemGroups 
    {
		public static T GetCode<T>(string pName)
		{
			var sql = "";

			try
			{
				sql = "";
				sql += "SELECT ItmsGrpCod FROM OITB WHERE ItmsGrpNam = '" + pName + "'";

				var ItmGrpCod = NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);
				if (ItmGrpCod == 0) throw new Exception(UTIL.Message.Format("Item Group Name not found. " + pName));

				return (T)Convert.ChangeType(ItmGrpCod, typeof(T));
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
		}

		public static Globals.ItemGroupTypes? ItemGroupFromNumber(int Number)
        {
            switch (Number)
            {
                case 1:
                    return Globals.ItemGroupTypes.CannabisSeed;
                case 2:
                    return Globals.ItemGroupTypes.CloneClipping;
                case 3:
                    return Globals.ItemGroupTypes.CannabisPlant;
                case 4:
                    return Globals.ItemGroupTypes.WetCannabis;
                case 5:
                    return Globals.ItemGroupTypes.DriedCannabis;
                case 6:
                    return Globals.ItemGroupTypes.ManicuredTrim;
                case 7:
                    return Globals.ItemGroupTypes.ManicuredBud;
                case 8:
                    return Globals.ItemGroupTypes.PlantWaste;
                case 9:
                    return Globals.ItemGroupTypes.PackagedCannabis;
                case 10:
                    return Globals.ItemGroupTypes.Concentrate;
                case 11:
                    return Globals.ItemGroupTypes.Additives;
                case 12:
                    return Globals.ItemGroupTypes.Pesticides;
                case 13:
                    return Globals.ItemGroupTypes.Sample;
                case 14:
                    return Globals.ItemGroupTypes.QABatch;
            }
            return null;
        }

        public static Int32? GetNumberFromItemGroup(Globals.ItemGroupTypes ItemGroup)
        {
            switch (ItemGroup)
            {
                case Globals.ItemGroupTypes.CannabisSeed:
                    return 1;
                case Globals.ItemGroupTypes.CloneClipping:
                    return 2;
                case Globals.ItemGroupTypes.CannabisPlant:
                    return 3;
                case Globals.ItemGroupTypes.WetCannabis:
                    return 4;
                case Globals.ItemGroupTypes.DriedCannabis:
                    return 5;
                case Globals.ItemGroupTypes.ManicuredTrim:
                    return 6;
                case Globals.ItemGroupTypes.ManicuredBud:
                    return 7;
                 case Globals.ItemGroupTypes.PlantWaste:
                    return 8;
                case Globals.ItemGroupTypes.PackagedCannabis:
                    return 9;
                case Globals.ItemGroupTypes.Concentrate:
                    return 10;
                case Globals.ItemGroupTypes.Additives:
                    return 11;
                case Globals.ItemGroupTypes.Nutrients:
                    return 12;
                case Globals.ItemGroupTypes.Pesticides:
                    return 12;
                case Globals.ItemGroupTypes.Fungicides:
                    return 13;
                case Globals.ItemGroupTypes.Sample:
                    return 14;
                case Globals.ItemGroupTypes.QABatch:
                    return 15;
            }
            return null;
        }

        public static string GetNameFromItemGroup(Globals.ItemGroupTypes ItemGroup)
        {
            switch (ItemGroup)
            {
                case Globals.ItemGroupTypes.CannabisSeed:
                    return "Seed";
                case Globals.ItemGroupTypes.CloneClipping:
                    return "Clone";
                case Globals.ItemGroupTypes.CannabisPlant:
                    return "Mature Plant";
                case Globals.ItemGroupTypes.WetCannabis:
                    return "Wet Cannabis";
                case Globals.ItemGroupTypes.DriedCannabis:
                    return "Dried Cannabis";
                case Globals.ItemGroupTypes.ManicuredTrim:
                    return "Manicured Trim";
                case Globals.ItemGroupTypes.ManicuredBud:
                    return "Manicured Bud";
                case Globals.ItemGroupTypes.PlantWaste:
                    return "Plant Waste";
                case Globals.ItemGroupTypes.PackagedCannabis:
                    return "Packaged Cannabis";
                case Globals.ItemGroupTypes.Concentrate:
                    return "Concentrate";
                case Globals.ItemGroupTypes.Additives:
                    return "Additives";
                case Globals.ItemGroupTypes.Nutrients:
                    return "Nutrients";
                case Globals.ItemGroupTypes.Pesticides:
                    return "Pesticides";
                case Globals.ItemGroupTypes.Fungicides:
                    return "Fungicides";
                case Globals.ItemGroupTypes.Sample:
                    return "Wet Cannabis Sample";
                case Globals.ItemGroupTypes.QABatch:
                    return "QA Batch";
            }
            return null;
        }

        public static string GetSAPItemGroupCodeFromGroupType(Globals.ItemGroupTypes ItemGroupType)
        {
            try
            {
                var sql = "SELECT ItmsGrpCod FROM OITB WHERE ItmsGrpNam = '{0}'";
                return NSC_DI.UTIL.SQL.GetValue<string>(string.Format(sql, GetNameFromItemGroup(ItemGroupType)));
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        /// <summary>
        /// Will return the state ID for the item group type for a particular item when passed an item code.
        /// </summary>
        /// <param name="ItemCode">The code for the item you want the state ID of the item group type for.</param>
        /// <returns>Returns the state ID for an item group.</returns>
		public static string GetStateIDFromItemCode(string ItemCode)
        {
            // TODO-SAP-DIAPI: Replace the SQL method with BoBCOM interactions

            // Prepare a SQL statement that will return the states ID for the item group that an item is attached to.
            string SQL_GetStateIDFromAnItemCode = @"
SELECT [OITB].[U_NSC_StateID] FROM [OITB] JOIN [OITM] ON [OITB].[ItmsGrpCod] = [OITM].[ItmsGrpCod] WHERE [OITM].[ItemCode] = '" + (new SqlParameter("ItemCode", ItemCode)).Value.ToString() + "'";

			return UTIL.SQL.GetFieldsFromSQLQuery(SQL_GetStateIDFromAnItemCode).Item("U_NSC_StateID").Value.ToString();
        }

        public static string GetComplCatUOM(string ItemCode)
        {
            // Prepare a SQL statement that will return the states ID for the item group that an item is attached to.
            string sql = $"SELECT [OITB].[U_NSC_CmplCatUOM] FROM [OITB] JOIN [OITM] ON [OITB].[ItmsGrpCod] = [OITM].[ItmsGrpCod] WHERE [OITM].[ItemCode] = '" + (new SqlParameter("ItemCode", ItemCode)).Value.ToString() + "'";
            return NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
        }
    }
}
