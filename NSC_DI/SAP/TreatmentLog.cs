﻿using System;

namespace NSC_DI.SAP
{
    public class TreatmentLog 
    {
	    /// <summary>
	    /// Creates a new Treament log of for a specific plant.
	    /// </summary>
	    /// <param name="PlantID">The unique identifier of a plant.</param>
	    /// <param name="AmountOfAdditiveApplied">Amount of pesticide to apply to a plant.</param>
	    /// <param name="WarehouseCode">Unique identifier of the warehouse.</param>
	    /// <param name="PPM">Parts Per Million.</param>
	    /// <param name="TDS">Total Dissolved Solids</param>
	    /// <param name="EC">Electrical Conductivity.</param>
	    public static void AddTreatmentLog(string PlantID, string ItemCode, double AmountOfAdditiveApplied, string WarehouseCode, string PPM, string TDS, string EC)
	    {
		    // Grab the next available code from the SQL database
		    string NextCode = UTIL.SQL.GetNextAvailableCodeForItem(Globals.tTreatLog, "Code");

			SAPbobsCOM.UserTable sboTable = null;

		    try
		    {

			    // Prepare a connection to the user table
			    sboTable = Globals.oCompany.UserTables.Item(Globals.tTreatLog);

			    // Set the values for the newly created item
			    //sboTable.Code = NextCode;
			    sboTable.Name = "";
			    sboTable.UserFields.Fields.Item("U_PlantID").Value = PlantID;
			    sboTable.UserFields.Fields.Item("U_ItemCode").Value = ItemCode;
			    sboTable.UserFields.Fields.Item("U_DateCreated").Value = DateTime.Now;
			    sboTable.UserFields.Fields.Item("U_Amount").Value = AmountOfAdditiveApplied;
			    sboTable.UserFields.Fields.Item("U_PSPPM").Value = PPM;
			    sboTable.UserFields.Fields.Item("U_PSTDS").Value = TDS;
			    sboTable.UserFields.Fields.Item("U_PSEC").Value = EC;
                sboTable.UserFields.Fields.Item("U_GUID").Value = "TREAT";

                // Attempt to add the new item to the database         
                if (sboTable.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

			    // Attempt to add a new plant journal entry
			    PlantJournal.AddPlantJournalEntry(PlantID, Globals.JournalEntryType.Treat, "Treated plant with " + AmountOfAdditiveApplied + " of " + ItemCode);
		    }
		    catch (Exception ex)
		    {
			    throw new Exception(UTIL.Message.Format(ex));
		    }
		    finally
		    {
				UTIL.Misc.KillObject(sboTable);
			    GC.Collect();
		    }
	    }

	    /// <summary>
        /// Returns the number of milliliters given a additive amount.
        /// </summary>
        /// <param name="Measurement">The type of liquid measurment</param>
        /// <param name="Amount">The amount of the type of liquid measurement</param>
        /// <returns>Returns a decimal number of milliliters in a given liquid amount</returns>
		public static double GetMillilitersFromAmountOfLiquid(Globals.LiquidMeasurements Measurement, double Amount)
        {
            // Convert to milliliters from various liquid measurements
            switch (Measurement)
            {
                case Globals.LiquidMeasurements.gal:
                    return Amount * 3785.41;

                case Globals.LiquidMeasurements.liters:
                    return Amount * 1000.00;

                case Globals.LiquidMeasurements.qt:
                    return Amount * 946.353;

                case Globals.LiquidMeasurements.pt:
                    return Amount * 473.176;

                case Globals.LiquidMeasurements.cups:
                    return Amount * 236.588;

                case Globals.LiquidMeasurements.ml:
                    return Amount;

                case Globals.LiquidMeasurements.floz:
                    return Amount * 29.5735;
            }

            return Amount;
        }

        /// <summary>
        /// Returns a specified amount given milliliter amount of additive.
        /// </summary>
        /// <param name="Measurement">The liquid measurement type to return</param>
        /// <param name="MilliliterAmount">The number of milliliters of water</param>
        /// <returns>Returns a decimal number of the specific liquid measurement type given milliliters of water.</returns>
		public static double GetAmountFromMilliliters(Globals.LiquidMeasurements Measurement, double MilliliterAmount)
        {
            // Convert to milliliters from various liquid measurements
            switch (Measurement)
            {
                case Globals.LiquidMeasurements.gal:
                    return MilliliterAmount / 3785.41;

                case Globals.LiquidMeasurements.liters:
                    return MilliliterAmount / 1000.00;

                case Globals.LiquidMeasurements.qt:
                    return MilliliterAmount / 946.353;

                case Globals.LiquidMeasurements.pt:
                    return MilliliterAmount / 473.176;

                case Globals.LiquidMeasurements.cups:
                    return MilliliterAmount / 236.588;

                case Globals.LiquidMeasurements.ml:
                    return MilliliterAmount;

                case Globals.LiquidMeasurements.floz:
                    return MilliliterAmount / 29.5735;
            }

            return MilliliterAmount;
        }
    }
}
