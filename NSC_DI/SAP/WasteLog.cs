﻿using System;

namespace NSC_DI.SAP
{
	public class WasteLog
	{
		/// <summary>
		/// Removes waste from a plant and adds the waste as inventory to SAP Business One
		/// </summary>
		/// <param name="PlantID"></param>
		/// <param name="AmountOfWasteRemoved"></param>
		/// <param name="UnitOfMeasurement"></param>
		/// <param name="Reason"></param>
		/// <param name="Details"></param>
		public static void GetWasteFromPlant(string PlantID, double AmountOfWasteRemoved, string UnitOfMeasurement, string Reason, string Details)
		{
			// Grab the next available code from the SQL database
			string NextCode = UTIL.SQL.GetNextAvailableCodeForItem(Globals.tWasteLog, "Code");

			SAPbobsCOM.UserTable sboTable = null;

			try
			{

				// Prepare a connection to the user table
				sboTable = Globals.oCompany.UserTables.Item(Globals.tWasteLog);

				// Set the values for the newly created item
				//sboTable.Code = NextCode;
				sboTable.Name = NextCode;
				sboTable.UserFields.Fields.Item("U_PlantID").Value = PlantID;
				sboTable.UserFields.Fields.Item("U_DateCreated").Value = DateTime.Now;
				sboTable.UserFields.Fields.Item("U_Amount").Value = AmountOfWasteRemoved;
				sboTable.UserFields.Fields.Item("U_Measure").Value = UnitOfMeasurement;
				sboTable.UserFields.Fields.Item("U_Reason").Value = Reason.ToString();
				sboTable.UserFields.Fields.Item("U_Details").Value = Details.ToString();
                sboTable.UserFields.Fields.Item("U_GUID").Value ="PRUNE";

                // Attempt to add the new item to the database
                if (sboTable.Add() != 0) B1Exception.RaiseException();

				// TODO: Add waste item to inventory

				// Add a plant journal entry
				PlantJournal.AddPlantJournalEntry(PlantID, Globals.JournalEntryType.Waste, "Waste removed " + AmountOfWasteRemoved + " " + UnitOfMeasurement					);

/*		// RHH. - Compliance
				// Prepare a Compliance Controller
				NavSol.Controllers.Compliance VirSci_Controllers_Compliance = new Compliance(SAPBusinessOne_Application: Globals.oApp,
					SAPBusinessOne_Company: Globals.oCompany);

				// API-SAP-TODO: Build Real API Call For Plant Waste (removed unworkable compliance code, needs to be moved to btAPI)
				//BioTrack.Plant.Waste_Weigh()
*/
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(sboTable);
				GC.Collect();
			}
		}
	}
}
