﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace NSC_DI.SAP
{
    public static class Phase
    {
        public class AutoPdoPhase
        {
            public string PhaseName { get; set; }

            public string CurrentPhase { get; set; }

            public string NextPhase { get; set; }

            public bool CanWater { get; set; }
            public bool CanFeed { get; set; }
            public bool CanTreat { get; set; }
            public bool CanPrune { get; set; }
            public bool CanQuarantine { get; set; }

            //FIXME: load from CSV if this phase can change mother state, until then, all phases can
            //public bool CanChangeMother { get; set; }
        }

	    public static List<AutoPdoPhase> GetPhases()
	    {
		    try
		    {
			    var sqlQuery = "SELECT * FROM [@" + Globals.tPDOPhase + @"] ORDER BY [Code]";
			    DataTable dt_Phase = UTIL.SQL.DataTable(sqlQuery, "Phase");
			    return (from DataRow dr in dt_Phase.Rows
				    select new AutoPdoPhase()
				    {
					    PhaseName = dr["Name"] as string,
					    CurrentPhase = dr["U_CurrentPhase"] as string,
					    NextPhase = dr["U_NextPhase"] as string,
					    CanWater = string.Equals(dr["U_CanWater"].ToString(), "Y", StringComparison.InvariantCulture),
					    CanFeed = string.Equals(dr["U_CanFeed"].ToString(), "Y", StringComparison.InvariantCulture),
					    CanTreat = string.Equals(dr["U_CanTreat"].ToString(), "Y", StringComparison.InvariantCulture),
					    CanPrune = string.Equals(dr["U_CanPrune"].ToString(), "Y", StringComparison.InvariantCulture),
					    CanQuarantine = string.Equals(dr["U_CanQuarantine"].ToString(), "Y", StringComparison.InvariantCulture)
				    }).ToList();
		    }
		    catch (Exception ex)
		    {
			    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
		    }
	    }
        public static string GetNext(string pCurrentStage)
        {
            try
            {
                var sqlQuery = $"SELECT U_NextPhase FROM [@{Globals.tPDOPhase}] WHERE U_CurrentPhase = '{pCurrentStage}'";
                return NSC_DI.UTIL.SQL.GetValue<string>(sqlQuery, "");
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                //UTIL.Misc.KillObject(sboTransfer);
            }
        }

        public static bool IsSerialize(string pStage)
        {
            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && 
                    NSC_DI.UTIL.Settings.Value.Get("Serialize Stage")  == pStage)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                //UTIL.Misc.KillObject(sboTransfer);
            }
        }
    }
}
