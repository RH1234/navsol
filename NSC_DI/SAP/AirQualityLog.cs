﻿using System;
using SAPbobsCOM;

namespace NSC_DI.SAP
{
	public static class AirQualityLog
	{
		public static void CreateNewAirQualityLog(string CreatedBy, string CreatedOn, string CO2, string Humidity, string Temperature, string DegreeType, string Warehouse)
		{

			//// Grab the next available code from the SQL database
			//string NextCode = (new SAP_BusinessOne.Helpers.UDO(_SBO_Application, _SBO_Company)).
			//  GetNextAvailableCodeForItem(DatabaseTableName: "@" + SAP_BusinessOne.Global.SAP_PartnerCode + "_" + VirSci_SAP.Models.AirQuality.TableName, CodeColumnName: "Code");

			// Prepare a connection to the user table
			UserTable sboTable = Globals.oCompany.UserTables.Item(Globals.tAirLog);

			try
			{
				//Setting Data to Master Data Table Fields  
				//sboTable.Code = NextCode;
				sboTable.Name = "";
				sboTable.UserFields.Fields.Item("U_CreatedBy").Value = CreatedBy;
				sboTable.UserFields.Fields.Item("U_CreatedOn").Value = DateTime.Now;
				sboTable.UserFields.Fields.Item("U_CO2").Value = CO2;
				sboTable.UserFields.Fields.Item("U_Humidity").Value = Humidity;
				sboTable.UserFields.Fields.Item("U_Temperature").Value = Temperature;
				sboTable.UserFields.Fields.Item("U_DegreeType").Value = DegreeType;
				sboTable.UserFields.Fields.Item("U_Warehouse").Value = Warehouse;

				// Attempt to add the new item to the database
				if (sboTable.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
		}
	}
}
