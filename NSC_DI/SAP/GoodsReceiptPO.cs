﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace NSC_DI.SAP
{
    public static class GoodsReceiptPO 
    {
        public class GoodsReciept_Lines
        {
            public double Price { get; set; }
            public string ItemCode { get; set; }
            public double Quantity { get; set; }
            public string UnitOfMeasure { get; set; }
            public string IsMedical { get; set; }
            public string Serial_StateID { get; set; }
            public string WarehouseCode { get; set; }
            public string FinProject { get; set; }
            public int BaseDoc { get; set; }
            public int BaseLine { get; set; }


            public IEnumerable<GoodsReceipt_Lines_BinAlloc> BinAllocations { get; set; }

            public IEnumerable<string> SerialNumbers { get; set; }

            public List<GoodsReciept_Lines_Batches> BatchLineItems { get; set; }

            public Dictionary<string, string> UserDefinedFields { get; set; }
        }

        public class GoodsReciept_Lines_Batches
        {
            public string BatchNumber { get; set; }
            public double Quantity { get; set; }
            public string StateID { get; set; }
            public string CropID { get; set; }
            public string CropStageID { get; set; }
        }

        public class GoodsReceipt_Lines_BinAlloc
        {
           public double Quantity { get; set; }

           public string BinsAbs { get; set; }

           public string BatchNumber { get; set; }

           public string SerialNumber { get; set; }
        }

        public static string AddGoodsReceiptPO(string BusinessPartnerCode, DateTime DueDate, List<GoodsReciept_Lines> LineItems, string pVendRefNo = "", string pComments = null, string pFromPOEntry = null)
        {
            // Grab the next available code from the SQL database
			//int NextDocNumber = Convert.ToInt32(UTIL.SQL.GetNextAvailableCodeForItem("OIGN", "DocNum"));

            // Create a new "Goods Receipt - Purchase Order" document
			SAPbobsCOM.Documents oGoodsReceiptPurchaseOrder = (SAPbobsCOM.Documents) Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseDeliveryNotes);
            SAPbobsCOM.Documents oFromPurchaseOrder = null;// (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);            

            // Set the document to use items
            oGoodsReceiptPurchaseOrder.DocType      = SAPbobsCOM.BoDocumentTypes.dDocument_Items;
            oGoodsReceiptPurchaseOrder.CardCode     = BusinessPartnerCode;
            oGoodsReceiptPurchaseOrder.DocDueDate   = DueDate;
            oGoodsReceiptPurchaseOrder.NumAtCard    = pVendRefNo;
            //oGoodsReceiptPurchaseOrder.DocNum       = NextDocNumber;

            try
            {
                // For each line item we are creating
                int line = 0;
                if (!NSC_DI.UTIL.Strings.Empty(pFromPOEntry))
                {                   
                    NSC_DI.SAP.Document.GetByKey(out oFromPurchaseOrder, SAPbobsCOM.BoObjectTypes.oPurchaseOrders,  int.Parse(pFromPOEntry));
                    if (oFromPurchaseOrder.Expenses.Count > 0)
                        NSC_DI.SAP.Document.Freight(oFromPurchaseOrder, oGoodsReceiptPurchaseOrder);
                }

                foreach (GoodsReciept_Lines LineItem in LineItems)
                {
                    if (line == 0)
                    {
                        // only want to check the 1st row
                        var br = NSC_DI.SAP.Branch.Get(LineItem.WarehouseCode);
                        if (br > 0) oGoodsReceiptPurchaseOrder.BPL_IDAssignedToInvoice = br;
                    }

                    if (line > 0) oGoodsReceiptPurchaseOrder.Lines.Add();

                    oGoodsReceiptPurchaseOrder.Lines.ItemCode       = LineItem.ItemCode;
                    oGoodsReceiptPurchaseOrder.Lines.Quantity       = LineItem.Quantity;
                    if (LineItem.UnitOfMeasure != null && LineItem.UnitOfMeasure.Length > 0) {oGoodsReceiptPurchaseOrder.Lines.MeasureUnit = LineItem.UnitOfMeasure;}
                    oGoodsReceiptPurchaseOrder.Lines.Price          = LineItem.Price;
                    oGoodsReceiptPurchaseOrder.Lines.UnitPrice      = LineItem.Price;
                    oGoodsReceiptPurchaseOrder.Lines.WarehouseCode  = LineItem.WarehouseCode;
                    oGoodsReceiptPurchaseOrder.Lines.ProjectCode    = LineItem.FinProject;
                    //oGoodsReceiptPurchaseOrder.Lines.MeasureUnit = LineItem.UnitOfMeasure;
                    //oGoodsReceiptPurchaseOrder.Lines.TaxCode = "EX";
                    //oGoodsReceiptPurchaseOrder.Lines.MeasureUnit = "GR";
                    if (LineItem.BaseDoc > 0) oGoodsReceiptPurchaseOrder.Lines.BaseEntry = LineItem.BaseDoc;
                    if (LineItem.BaseLine >= 0) oGoodsReceiptPurchaseOrder.Lines.BaseLine  = LineItem.BaseLine;
                    if (LineItem.BaseDoc > 0) oGoodsReceiptPurchaseOrder.Lines.BaseType  = (int)SAPbobsCOM.BoObjectTypes.oPurchaseOrders;

                    NSC_DI.SAP.Document.SetDefaultDistributionRules(oGoodsReceiptPurchaseOrder.Lines);

                    // Grab the item we are attempting to add from the database
                    SAPbobsCOM.Items oItems = (SAPbobsCOM.Items)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                    oItems.GetByKey(LineItem.ItemCode);

                    
                    int serialAndBatchNumbersLine = 0;

                    if (LineItem.SerialNumbers != null) // Handle Serial Items
                    {
                        // expand the bins - if there are any
                        var snCount = LineItem.SerialNumbers.Count();
                        string[] binsExpanded = new string[snCount];
                        if (LineItem.BinAllocations != null)
                        {
                            int indx = 0;

                            foreach (NSC_DI.SAP.GoodsReceiptPO.GoodsReceipt_Lines_BinAlloc binRec in LineItem.BinAllocations)
                            {
                                for (int i = 0; i < binRec.Quantity; i++)
                                {
                                    binsExpanded[indx++] = binRec.BinsAbs;
                                }
                            }
                        }
                        foreach (string serial in LineItem.SerialNumbers)
                        {
                            if (serialAndBatchNumbersLine > 0) oGoodsReceiptPurchaseOrder.Lines.SerialNumbers.Add();
                            oGoodsReceiptPurchaseOrder.Lines.SerialNumbers.InternalSerialNumber = serial;
                            oGoodsReceiptPurchaseOrder.Lines.SerialNumbers.Quantity = 1;
                            if(LineItem.Serial_StateID != "") oGoodsReceiptPurchaseOrder.Lines.SerialNumbers.ManufacturerSerialNumber = LineItem.Serial_StateID;

                            if (LineItem.UserDefinedFields != null)
                            {
                                foreach (var key in LineItem.UserDefinedFields.Keys)
                                {
                                    oGoodsReceiptPurchaseOrder.Lines.SerialNumbers.UserFields.Fields.Item(key).Value = LineItem.UserDefinedFields[key].ToString();
                                }
                            }

                            // Bin Allocations - if there are any
                            if (LineItem.BinAllocations != null)
                            {

                                if (LineItem.BinAllocations?.Any(n => n.SerialNumber == serial) == true)
                                {
                                    // SERIAL NUMBER CORRISPONDS TO A BIN
                                    var bin = LineItem.BinAllocations.First(n => n.SerialNumber == serial);
                                    if (bin.BinsAbs != null)
                                    {
                                        if (serialAndBatchNumbersLine > 0) oGoodsReceiptPurchaseOrder.Lines.BinAllocations.Add();
                                        oGoodsReceiptPurchaseOrder.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = serialAndBatchNumbersLine;
                                        oGoodsReceiptPurchaseOrder.Lines.BinAllocations.Quantity = 1;
                                        oGoodsReceiptPurchaseOrder.Lines.BinAllocations.BinAbsEntry = int.Parse(bin.BinsAbs);
                                    }
                                }
                                else
                                {
                                    // SERIAL NUMBERS AND BINS HAVE 2 DIFFERENT LISTS.
                                    // COULD BE 20 SNs AND 3 BINS BUT THE BINS HAVE A QUUANTITY TOTALING 20.
                                    if (serialAndBatchNumbersLine > 0) oGoodsReceiptPurchaseOrder.Lines.BinAllocations.Add();
                                    oGoodsReceiptPurchaseOrder.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = serialAndBatchNumbersLine;
                                    oGoodsReceiptPurchaseOrder.Lines.BinAllocations.Quantity = 1;
                                    oGoodsReceiptPurchaseOrder.Lines.BinAllocations.BinAbsEntry = int.Parse(binsExpanded[serialAndBatchNumbersLine]);
                                }
                            }
                            serialAndBatchNumbersLine++;
                        }
                    }
                    else if (LineItem.BatchLineItems != null) // handle batch items
                    {
                        foreach (GoodsReciept_Lines_Batches batchItem in LineItem.BatchLineItems)
                        {
                            if (serialAndBatchNumbersLine > 0) oGoodsReceiptPurchaseOrder.Lines.BatchNumbers.Add();
                            oGoodsReceiptPurchaseOrder.Lines.BatchNumbers.BatchNumber = batchItem.BatchNumber;
                            //Check if the item has purchase unit of measure value (NumInBuy) if so multiple this by the quanitity to get you inventory amount if different. 
                            double dblQuantityAdj = 0.0;
                            string treeTypeSQL = NSC_DI.UTIL.SQL.GetValue<string>($"Select T0.TreeType from OITM T0 where ItemCode = '" + LineItem.ItemCode + "' ");                            
                            if (treeTypeSQL == "T")
                            {
                                throw new System.ComponentModel.WarningException($"Item {LineItem.ItemCode} has a template BOM Setup, check the BOM Type.");                                
                            }
                            dblQuantityAdj = NSC_DI.UTIL.SQL.GetValue<double>($"Select T0.NumInBuy from OITM T0 where ItemCode = '" + LineItem.ItemCode + "' ");
                            if (dblQuantityAdj > 1)
                            {
                                batchItem.Quantity = batchItem.Quantity * dblQuantityAdj;
                            }
                            oGoodsReceiptPurchaseOrder.Lines.BatchNumbers.Quantity = batchItem.Quantity;
                            oGoodsReceiptPurchaseOrder.Lines.BatchNumbers.ManufacturerSerialNumber = batchItem.StateID;

                            if (LineItem.UserDefinedFields != null)
                            {
                                foreach (var key in LineItem.UserDefinedFields.Keys)
                                {
                                    oGoodsReceiptPurchaseOrder.Lines.BatchNumbers.UserFields.Fields.Item(key).Value = LineItem.UserDefinedFields[key].ToString();

                                }
                            }

                            // Bin Allocations

                            //if (LineItem.BinAllocations?.Any(n => n.BatchNumber == batchItem.BatchNumber) == true)
                            //{
                            //    if (serialAndBatchNumbersLine > 0) oGoodsReceiptPurchaseOrder.Lines.BinAllocations.Add();

                            //    var bin = LineItem.BinAllocations.First(n => n.BatchNumber == batchItem.BatchNumber);
                            //    oGoodsReceiptPurchaseOrder.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = serialAndBatchNumbersLine;
                            //    oGoodsReceiptPurchaseOrder.Lines.BinAllocations.Quantity = batchItem.Quantity;
                            //    oGoodsReceiptPurchaseOrder.Lines.BinAllocations.BinAbsEntry = int.Parse(bin.BinsAbs);
                            //}
                            if (LineItem.BinAllocations != null)
                            {
                                var newRec = false;
                                foreach (NSC_DI.SAP.GoodsReceiptPO.GoodsReceipt_Lines_BinAlloc binRec in LineItem.BinAllocations)
                                {
                                    if (newRec) oGoodsReceiptPurchaseOrder.Lines.BinAllocations.Add();
                                    oGoodsReceiptPurchaseOrder.Lines.BinAllocations.SerialAndBatchNumbersBaseLine   = serialAndBatchNumbersLine;
                                    oGoodsReceiptPurchaseOrder.Lines.BinAllocations.Quantity                        = binRec.Quantity;
                                    oGoodsReceiptPurchaseOrder.Lines.BinAllocations.BinAbsEntry                     = int.Parse(binRec.BinsAbs);
                                    newRec = true;
                                }
                            }
                            serialAndBatchNumbersLine++;
                        }
                    }
                    else // Line item
                    {
                        if (LineItem.BinAllocations?.Any() == true)
                        {
                            var bin = LineItem.BinAllocations.First();
                            oGoodsReceiptPurchaseOrder.Lines.BinAllocations.BaseLineNumber = line;
                            oGoodsReceiptPurchaseOrder.Lines.BinAllocations.Quantity = LineItem.Quantity;
                            oGoodsReceiptPurchaseOrder.Lines.BinAllocations.BinAbsEntry = int.Parse(bin.BinsAbs);
                        }
                    }

                    line++;
                }
                oGoodsReceiptPurchaseOrder.Comments = pComments; // add the remarks from the intake wizard if they exist. OP-1156
                // Attempt to add the Goods Receipt Purchase Order to the database

                int retCode = oGoodsReceiptPurchaseOrder.Add();                
                if (retCode != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                string strNewDocEntry = Globals.oCompany.GetNewObjectKey();

                return strNewDocEntry;

            }
            catch (System.ComponentModel.WarningException ex)
            {
                throw new System.ComponentModel.WarningException(ex.Message);
            }
            catch (Exception ex)
            {
                //TODO-LOG: Error creating a "Goods Receipt PO" SAP document.
				throw new Exception(UTIL.Message.Format(ex));
			}
		}
    }
}
