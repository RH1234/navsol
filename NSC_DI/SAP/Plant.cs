﻿using System;
using System.Collections.Generic;
using SAPbobsCOM;
using System.Linq;

namespace NSC_DI.SAP
{
	public static class Plant
	{
		//public static string GetNextAvailablePlantSerialID(string ItemCodeOfPlant, int IncrementToSerialNumber = 0)
		//{
		//	// Prepare the SQL statement to get the next availble plant ID
		//	string SQL_NewID = "SELECT ISNULL((MAX(CONVERT(int,[OSRI].[SysSerial]))),1) FROM [OSRI] WHERE [OSRI].[ItemCode] = '" + ItemCodeOfPlant + "'";

		//	// Get the results from SQL
		//	Fields ResultFromSQL = UTIL.SQL.GetFieldsFromSQLQuery(SQL_NewID);

		//	if (ResultFromSQL == null) return null;
		//	if (ResultFromSQL.Count < 1) return null;

		//	return ItemCodeOfPlant + "-P-" + (ResultFromSQL.Item(0).Value + IncrementToSerialNumber).ToString();
		//}

		public static IEnumerable<string> GetNextAvailablePlantSerialID(string ItemCodeOfPlant, int numberOfPlants, string pInterCoSubCode = "")
		{
            //Inter-Company Check
            ItemCodeOfPlant = (NSC_DI.Globals.InterCoCode == "") ? ItemCodeOfPlant : NSC_DI.Globals.InterCoCode + "-" + ItemCodeOfPlant;
            //ItemCodeOfPlant = NSC_DI.UTIL.Settings.concatBatchNum(pInterCoSubCode, ItemCodeOfPlant);
            //try
            //{

            //    string InterCo = UTIL.SQL.GetValue<string>("select T0.U_Value from dbo.[@NSC_SETTINGS] T0 where T0.Name = 'Inter-Company'");
            //    if (InterCo == "Y" && pInterCoSubCode.Trim().Length > 0)
            //    { 
            //        ItemCodeOfPlant = pInterCoSubCode + "-" + ItemCodeOfPlant;
            //    }
            //}
            //catch
            //{ 
            //    throw new Exception(UTIL.Message.Format("Inter-Company Error: Please check your settings (Y or N) and make sure you have a subsidiary code on your warehouse."));
            //}

            IEnumerable<string> results = NSC_DI.UTIL.AutoStrain.NextSN(ItemCodeOfPlant, quantity: numberOfPlants);


   //         for (int i = 0; i < numberOfPlants; i++)
			//{
			//	// Prepare the SQL statement to get the next availble plant ID
			//	string SQL_NewID = "SELECT ISNULL(MAX(CONVERT(int,[OSRI].[SysSerial])),1) FROM [OSRI] WHERE [OSRI].[ItemCode] = '" + ItemCodeOfPlant + "'";

			//	// Get the results from SQL
			//	Fields ResultFromSQL = UTIL.SQL.GetFieldsFromSQLQuery(SQL_NewID);

			//	if (ResultFromSQL == null) return null;
			//	if (ResultFromSQL.Count < 1) return null;

			//	results.Add(ItemCodeOfPlant + "-P-" + (Convert.ToInt32(ResultFromSQL.Item(0).Value.ToString()) + i).ToString());
			//}
			return results;
		}

        /// <summary>
        /// Attempts to change the "Is Mother Plant" state on a serialzed item within SAP Business One.
        /// </summary>
        /// <param name="ItemCode"></param>
        /// <param name="DistNumber"></param>
        /// <param name="NewPlantMotherState">Y: is a mother, N: is not a mother</param>
        public static void ChangeMotherStatus(string ItemCode, string DistNumber, string NewPlantMotherState)
		{
			//check that NewPlantMotherState is a valid value
			if (!NewPlantMotherState.Equals("Y") && !NewPlantMotherState.Equals("N"))
			{
				throw new Exception(UTIL.Message.Format("NewPlantMotherState must be 'Y' or 'N'"));
			}

			// TODO-SAP-DIAPI: Replace the SQL method with BoBCOM interactions

			// Get DocEntry from ItemCode/SysNumber
			string sql = "SELECT [OSRN].[AbsEntry] as [DocNumber] ,[OSRN].[U_NSC_StateID] as [U_NSC_StateID],[OSRN].[DistNumber] FROM [OSRN] WHERE [ItemCode] = '" + ItemCode +
			             "' AND [DistNumber] = '" + DistNumber + "'";

			int DocEntry = 0;

			Recordset oRecordSet = (Recordset) Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
			oRecordSet.DoQuery(sql);
			oRecordSet.MoveFirst();

            string MotherPlantID = oRecordSet.Fields.Item("DistNumber").Value.ToString(); 
            // Attempt to get the DocEntry number from our SQL results
            if (Int32.TryParse(oRecordSet.Fields.Item("DocNumber").Value.ToString(), out DocEntry))
			{
				// Prepare a Serial Number Details Service
				SerialNumberDetailsService oSerialNumberDetailsService =
					(Globals.oCompany.GetCompanyService()).GetBusinessService(ServiceTypes.SerialNumberDetailsService) as SerialNumberDetailsService;

				// Prepare the parameters for the serial number details
				SerialNumberDetailParams oSerialNumberDetailParams =
					oSerialNumberDetailsService.GetDataInterface(SerialNumberDetailsServiceDataInterfaces.sndsSerialNumberDetailParams) as
						SerialNumberDetailParams;

				// Set the DocEntry to the number we found through SQL
				oSerialNumberDetailParams.DocEntry = DocEntry;

				// Locate the record we are attempting to edit
				SerialNumberDetail oSerialNumberDetail = oSerialNumberDetailsService.Get(oSerialNumberDetailParams);

                // Set the new plant state
                string strIsMother = null;
                if(NewPlantMotherState =="Y")
                {
                    strIsMother = "Y";
                }
                else
                {
                    strIsMother = "N";
                }
				oSerialNumberDetail.UserFields.Item("U_" + Globals.SAP_PartnerCode + "_IsMother").Value = strIsMother;
                oSerialNumberDetail.UserFields.Item("U_" + Globals.SAP_PartnerCode + "_MotherID").Value = MotherPlantID;
                oSerialNumberDetail.UserFields.Item("U_" + Globals.SAP_PartnerCode + "_MotherStatusChg").Value = "Y";
                // Attempt to update the serialized items information
                oSerialNumberDetailsService.Update(oSerialNumberDetail);

                /*	// RHH. - Compliance
                                                // Prepare a connection to the state API
                                                var TraceCon = new NavSol.Controllers.TraceabilityAPI(SAPBusinessOne_Application: Globals.oApp,
                                                    SAPBusinessOne_Company: Globals.oCompany,
                                                    SAPBusinessOne_Form: _SBO_Form);

                                                BioTrack.API btAPI = TraceCon.new_API_obj();

                                                string StateID = oRecordSet.Fields.Item("U_VSC_StateID").Value.ToString();

                                                BioTrack.Plant.Modify(ref btAPI,StateID,Mother: NewPlantMotherState.Equals("1"));

                                                string compID = Compliance.CreateComplianceLineItem(
                                                                                Reason: Models.Compliance.ComplianceReasons.Plant_Modify
                                                                                , API_Call: btAPI.XmlApiRequest.ToString()
                                                                            );

                                                btAPI.PostToApi();

                                                if (btAPI.WasSuccesful)
                                                {
                                                    Compliance.UpdateComliancy(compID, btAPI.xDocFromResponse.ToString(), Globals.ComplianceStatus.Success);
                                                }
                                                else
                                                {
                                                    Compliance.UpdateComliancy(compID, btAPI.xDocFromResponse.ToString(), Globals.ComplianceStatus.Failed);
                                                    //this.WasSuccessful = true; //TODO: do we want to say not successful when we can't post to state?
                                                }
                */

                NSC_DI.UTIL.Misc.KillObject(oRecordSet); 

                return;
			}
			throw new Exception(UTIL.Message.Format("Could not find DocEntry for item."));
		}

        public static void ChangeQuarantineStatus(string ItemCode, string SysNumber, Globals.QuarantineStates NewPlantQuarantineState)
        {
            ChangeQuarantineStatus(ItemCode, SysNumber, NewPlantQuarantineState.ToString());
        }

        /// <summary>
        /// Attempts to change the "Plant Status" on a serialzed item within SAP Business One.
        /// </summary>
        /// <param name="ItemCode"></param>
        /// <param name="SysNumber"></param>
        /// <param name="NewPlantQuarantineState"></param>
        public static void ChangeQuarantineStatus(string ItemCode, string SysNumber, string NewPlantQuarantineState)
		{
			// TODO-SAP-DIAPI: Replace the SQL method with BoBCOM interactions

			// Get DocEntry from ItemCode/SysNumber
			string sql = "SELECT [AbsEntry] FROM [OSRN] WHERE [ItemCode] = '" + ItemCode + "' AND [SysNumber] = '" + SysNumber + "'";

			int DocEntry = 0;

			// Attempt to get the DocEntry number from our SQL results
			if (!Int32.TryParse(UTIL.SQL.GetFieldsFromSQLQuery(sql).Item(0).Value.ToString(), out DocEntry))
				throw new Exception(UTIL.Message.Format("Could not find DocEntry for item."));
			
			// Prepare a Serial Number Details Service
			SerialNumberDetailsService oSerialNumberDetailsService =
				(Globals.oCompany.GetCompanyService()).GetBusinessService(ServiceTypes.SerialNumberDetailsService) as SerialNumberDetailsService;

			// Prepare the parameters for the serial number details
			SerialNumberDetailParams oSerialNumberDetailParams =
				oSerialNumberDetailsService.GetDataInterface(SerialNumberDetailsServiceDataInterfaces.sndsSerialNumberDetailParams) as
					SerialNumberDetailParams;

			// Set the DocEntry to the number we found through SQL
			oSerialNumberDetailParams.DocEntry = DocEntry;

			// Locate the record we are attempting to edit
			SerialNumberDetail oSerialNumberDetail = oSerialNumberDetailsService.Get(oSerialNumberDetailParams);

			// Set the new plant state
			oSerialNumberDetail.UserFields.Item("U_" + Globals.SAP_PartnerCode + "_QuarState").Value = NewPlantQuarantineState.ToString();

			// Set our Quarantine Timer.
			string timeStamp = UTIL.Dates.ToTimeStamp(DateTime.Now).ToString();
			oSerialNumberDetail.UserFields.Item("U_" + Globals.SAP_PartnerCode + "_TimerQuar").Value = timeStamp;

			// Attempt to update the serialized items information
			oSerialNumberDetailsService.Update(oSerialNumberDetail);
		}

		public static void TransferPlants(IEnumerable<string> ListOfSelectedPlants, string DestinationWarehouseCode, string pFromWarehouseName, 
            string NextPlantPhase = null, System.Data.DataTable pBins = null, string pXFerType = null, string pXFerReason = null)
		{
			StockTransfer sboTransfer = null;
			
			try
			{
				// Prepare an SAP Business One Stock Transfer
				sboTransfer = Globals.oCompany.GetBusinessObject(BoObjectTypes.oStockTransfer) as StockTransfer;

				// Assign all needed variables for a stock transfer
				sboTransfer.DocDate = DateTime.Now;
				sboTransfer.TaxDate = DateTime.Now;
                string strSQL = string.Format(@"Select [WhsCode] from [OWHS] where [WhsName]='{0}'", pFromWarehouseName);
                string strFromWarehouseID = NSC_DI.UTIL.SQL.GetValue<string>(strSQL);
				sboTransfer.FromWarehouse = strFromWarehouseID;
                sboTransfer.ToWarehouse = DestinationWarehouseCode;
                strSQL = string.Format(@"Select [WhsName] from [OWHS] where [WhsCode]='{0}'", DestinationWarehouseCode);
                string strDestinationWarehouseName = NSC_DI.UTIL.SQL.GetValue<string>(strSQL);

                sboTransfer.Comments = "Transfer from production view";

                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null)
                    sboTransfer.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                if (pXFerType != null) sboTransfer.UserFields.Fields.Item("U_NSC_InvXFerType").Value = pXFerType;
                if (pXFerReason != null) sboTransfer.UserFields.Fields.Item("U_NSC_InvXFerResn").Value = pXFerReason;


                List<string> BarcodeIDs = new List<string>();

				// For each plant we are transferring
				foreach (string PlantID in ListOfSelectedPlants)
				{
                    var ItemCode = Items.SNs.GetItemCode(PlantID);

                    //// Grab SysNumber
                    //string SysNumber = SplitPlantID[2];

                    //**** This will have to be adjusted when Randy gets the Serialized Item code to generate properly
                    int intIndexHolder = PlantID.IndexOf('-');
                    //string strPlantID = PlantID.Remove(0, intIndexHolder + 1);
                    //intIndexHolder = strPlantID.IndexOf('-');
                    //strPlantID = strPlantID.Remove(0, intIndexHolder + 1);
                    
                    //Get SerialSys aka DocEntry
                    strSQL = string.Format(@"SELECT SysSerial FROM [OSRI] WHERE [ItemCode] = '{0}' AND [IntrSerial] = '{1}'", ItemCode, PlantID);
                    int iSysNo = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                    // Add a line item to the stock transfer
                    sboTransfer.Lines.ItemCode                          = ItemCode;
					sboTransfer.Lines.WarehouseCode                     = DestinationWarehouseCode;
					sboTransfer.Lines.Quantity                          = 1;
                    sboTransfer.Lines.SerialNumbers.SystemSerialNumber  = iSysNo;
                    sboTransfer.Lines.FromWarehouseCode                 = strFromWarehouseID;

                    int AbsEntry = 0;

                    // have to get the last entry, but then make sure there is a qty there.
                    AbsEntry = NSC_DI.SAP.Bins.GetAbsFromSN(PlantID);
                    var fromBin = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT OSBQ.BinAbs FROM OSBQ WHERE AbsEntry = {AbsEntry} AND OnHandQty > 0.0", 0);
                    var toBin = NSC_DI.SAP.Bins.GetIdFromDiffWH(fromBin, DestinationWarehouseCode);
                    if (pBins != null)
                    {
                        var sn = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT DistNumber FROM OSRN WHERE ItemCode = '{ItemCode}' AND SysNumber = {iSysNo}","");
                        var rows = pBins.Select($"[Serial\\Batch] = '{sn}'");
                        toBin = Int32.Parse(rows[0]["BinAbs"].ToString());
                    }
                    // don't need to specify a bin on the from WH for serialized items
                    //if (fromBin > 0)    // may be comming from a non-bin WH
                    //{
                    //    sboTransfer.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = 0;
                    //    sboTransfer.Lines.BinAllocations.BinAbsEntry = fromBin;
                    //    sboTransfer.Lines.BinAllocations.BinActionType = BinActionTypeEnum.batFromWarehouse;
                    //    sboTransfer.Lines.BinAllocations.Quantity = 1;
                    //}

                    if (toBin > 0)
                    {
                        if(sboTransfer.Lines.BinAllocations.BinActionType == BinActionTypeEnum.batFromWarehouse) sboTransfer.Lines.BinAllocations.Add();
                        sboTransfer.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = 0;
                        sboTransfer.Lines.BinAllocations.BinAbsEntry    = toBin;
                        sboTransfer.Lines.BinAllocations.BinActionType  = BinActionTypeEnum.batToWarehouse;
                        sboTransfer.Lines.BinAllocations.Quantity       = 1;
                    }

                    NSC_DI.SAP.Document.SetDefaultDistributionRules(sboTransfer.Lines);

                    sboTransfer.Lines.Add();

					string SQL_Query_BarcodeID = string.Format(@"SELECT SuppSerial FROM [OSRI] WHERE [ItemCode] = '{0}' AND [IntrSerial] = '{1}'",
						ItemCode, PlantID);

					Fields ResultFromSQLQuery = UTIL.SQL.GetFieldsFromSQLQuery(SQL_Query_BarcodeID);

					// Get real state identifier for plant
					BarcodeIDs.Add(ResultFromSQLQuery.Item(0).Value.ToString());

                    // Create plant journal entries for newly moved plant
                    PlantJournal.AddPlantJournalEntry(PlantID, Globals.JournalEntryType.Transfer,
						"Plant moved from warehouse #" + pFromWarehouseName + " to warehouse #" + strDestinationWarehouseName);
				}

				// Attempt to add the Stock Transfer
				if (sboTransfer.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(sboTransfer);
			}
		}
        public static void TransferPlantsBatch(Dictionary<string, Dictionary<string,double>> PlantBatchWhrsQty, string DestinationWarehouseCode, 
                                                string NextPlantPhase = null, System.Data.DataTable pBins = null)
        {
            StockTransfer sboTransfer = null;

            try
            {
                // Prepare an SAP Business One Stock Transfer
                sboTransfer = Globals.oCompany.GetBusinessObject(BoObjectTypes.oStockTransfer) as StockTransfer;

                // Assign all needed variables for a stock transfer
                sboTransfer.DocDate = DateTime.Now;
                sboTransfer.TaxDate = DateTime.Now;

                sboTransfer.FromWarehouse = (PlantBatchWhrsQty.First().Value).First().Key;
                sboTransfer.ToWarehouse = DestinationWarehouseCode;

                string strSQL = string.Format(@"Select [WhsName] from [OWHS] where [WhsCode]='{0}'", DestinationWarehouseCode);
                string strDestinationWarehouseName = NSC_DI.UTIL.SQL.GetValue<string>(strSQL);

                sboTransfer.Comments = "Transfer from production view";

                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null)
                   sboTransfer.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                List<string> BarcodeIDs = new List<string>();

                // For each plant we are transferring
                foreach (string PlantID in PlantBatchWhrsQty.Keys)
                {
                    var ItemCode = Items.Batches.GetItemCode(PlantID);

                    // Add a line item to the stock transfer
                    sboTransfer.Lines.ItemCode = ItemCode;
                    sboTransfer.Lines.WarehouseCode = DestinationWarehouseCode;                    

                    Dictionary <string, double> NewDic  = new Dictionary<string, double>();
                    PlantBatchWhrsQty.TryGetValue(PlantID,out NewDic);
                    sboTransfer.Lines.Quantity = NewDic.OrderBy(kvp => kvp.Key).First().Value;
                    sboTransfer.Lines.FromWarehouseCode = NewDic.OrderBy(kvp => kvp.Key).First().Key;
                    sboTransfer.Lines.BatchNumbers.BatchNumber = PlantID;
                    sboTransfer.Lines.BatchNumbers.Quantity = NewDic.OrderBy(kvp => kvp.Key).First().Value;

                    NSC_DI.SAP.Document.SetDefaultDistributionRules(sboTransfer.Lines);

                    if (pBins != null)
                    {
                        //Get SerialSys aka DocEntry
                        strSQL = string.Format(@"SELECT SysSerial FROM [OIBT] WHERE [ItemCode] = '{0}' AND [IntrSerial] = '{1}'", ItemCode, PlantID);
                        int iSysNo = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                        int AbsEntry = 0;
                        // have to get the last entry, but then make sure there is a qty there.
                        AbsEntry = NSC_DI.SAP.Bins.GetAbsFromSN(PlantID);
                        var fromBin = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT OSBQ.BinAbs FROM OSBQ WHERE AbsEntry = {AbsEntry} AND OnHandQty > 0.0", 0);
                        var toBin = NSC_DI.SAP.Bins.GetIdFromDiffWH(fromBin, DestinationWarehouseCode);

                        var sn = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT DistNumber FROM OSRN WHERE ItemCode = '{ItemCode}' AND SysNumber = {iSysNo}", "");
                        var rows = pBins.Select($"[Serial Number] = '{sn}'");
                        toBin = Int32.Parse(rows[0]["BinAbs"].ToString());

                        // don't need to specify a bin on the from WH for serialized items
                        //if (fromBin > 0)    // may be comming from a non-bin WH
                        //{
                        //    sboTransfer.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = 0;
                        //    sboTransfer.Lines.BinAllocations.BinAbsEntry = fromBin;
                        //    sboTransfer.Lines.BinAllocations.BinActionType = BinActionTypeEnum.batFromWarehouse;
                        //    sboTransfer.Lines.BinAllocations.Quantity = 1;
                        //}

                        if (toBin > 0)
                        {
                            if (sboTransfer.Lines.BinAllocations.BinActionType == BinActionTypeEnum.batFromWarehouse) sboTransfer.Lines.BinAllocations.Add();
                            sboTransfer.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = 0;
                            sboTransfer.Lines.BinAllocations.BinAbsEntry = toBin;
                            sboTransfer.Lines.BinAllocations.BinActionType = BinActionTypeEnum.batToWarehouse;
                            sboTransfer.Lines.BinAllocations.Quantity = 1;
                        }
                    }

                    sboTransfer.Lines.Add();

                    string SQL_Query_BarcodeID = string.Format(
                        @" SELECT [OBTN].MnfSerial FROM [OBTN] WHERE [ItemCode] = '{0}' AND [OBTN].DistNumber = '{1}'",
                        ItemCode, PlantID);

                    Fields ResultFromSQLQuery = UTIL.SQL.GetFieldsFromSQLQuery(SQL_Query_BarcodeID);

                    // Get real state identifier for plant
                    if(ResultFromSQLQuery.Item(0).Value.ToString()!=null) BarcodeIDs.Add(ResultFromSQLQuery.Item(0).Value.ToString());

                    // Create plant journal entries for newly moved plant
                    PlantJournal.AddPlantJournalEntry(PlantID, Globals.JournalEntryType.Transfer,
                        "Plant moved from warehouse #" + NewDic.OrderBy(kvp => kvp.Key).First().Key + " to warehouse #" + strDestinationWarehouseName);
                }

                // Attempt to add the Stock Transfer
                if (sboTransfer.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(sboTransfer);
            }
        }


        public static void Transfer(System.Data.DataTable pPlants = null)
        {
            // datatable definition is located in: F_BinSelect.dtCreateOut

            if (pPlants == null)        throw new Exception(UTIL.Message.Format("DataTable is empty.")); 
            if (pPlants.Rows.Count < 1) throw new Exception(UTIL.Message.Format("DataTable is empty."));

            StockTransfer oXfer = null;

            try
            {
                // Prepare a SAP Business One Stock Transfer
                oXfer = Globals.oCompany.GetBusinessObject(BoObjectTypes.oStockTransfer) as StockTransfer;

                // Assign all needed variables for a stock transfer
                oXfer.DocDate = DateTime.Now;
                oXfer.TaxDate = DateTime.Now;

                oXfer.Comments = "Transfer from Production View";

                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null)
                    oXfer.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                List<string> BarcodeIDs = new List<string>();

                // For each plant we are transferring - first pass just get the simple transfers
                for (var i = 0; i < pPlants.Rows.Count; i++)
                {
                    var row      = pPlants.Rows[i];

                    var ToBinAbs = (string.IsNullOrEmpty(row["BinAbs"].ToString()))? 0: Convert.ToInt32(row["BinAbs"].ToString());
                    var Qty      = Convert.ToDouble(row["Quantity"].ToString());
                    var ItemCode = row["ItemCode"].ToString();
                    var PlantID  = row["Serial\\Batch"].ToString();
                    var DestWH   = row["DestWH"].ToString();
                    var FromWH   = row["FromWH"].ToString();
                    var FromBin  = row["FromBin"].ToString();
                    var MangBy   = row["ManBySBN"].ToString();

                    var CurrentPlantPhase   = NSC_DI.SAP.Warehouse.GetType(FromWH);
                    var NextPlantPhase      = NSC_DI.SAP.Phase.GetNext(CurrentPlantPhase);

                    // if this entry requires seialization, then skip it for now
                    if (MangBy != "S" && NSC_DI.SAP.Phase.IsSerialize(NextPlantPhase)) continue;

                    if (oXfer.Lines.ItemCode != "") oXfer.Lines.Add();

                    //Get SerialSys
                    var sql = $"SELECT SysSerial FROM [OSRI] WHERE [ItemCode] = '{ItemCode}' AND [IntrSerial] = '{PlantID}'";
                    int iSysNo = NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);

                    // Add a line item to the stock transfer
                    oXfer.Lines.ItemCode            = ItemCode;
                    oXfer.Lines.WarehouseCode       = DestWH;
                    oXfer.Lines.FromWarehouseCode   = FromWH;
                    oXfer.Lines.Quantity            = Qty;
                    if (MangBy == "S") oXfer.Lines.SerialNumbers.SystemSerialNumber = iSysNo;
                    if (MangBy == "B")
                    {
                        oXfer.Lines.BatchNumbers.BatchNumber = PlantID;
                        oXfer.Lines.BatchNumbers.Quantity    = Qty;
                    }

                    NSC_DI.SAP.Document.SetDefaultDistributionRules(oXfer.Lines);

                    //// have to get the last entry, but then make sure there is a qty there.
                    //int AbsEntry = 0;
                    ////AbsEntry = NSC_DI.SAP.Bins.GetAbsFromSN(PlantID);
                    ////var fromBin = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT OSBQ.BinAbs FROM OSBQ WHERE AbsEntry = {AbsEntry} AND OnHandQty > 0.0", 0);
                    ////if (ToBinAbs != 0)
                    ////{
                    ////    var sn = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT DistNumber FROM OSRN WHERE ItemCode = '{ItemCode}' AND SysNumber = {iSysNo}", "");
                    ////}
                    //AbsEntry = NSC_DI.SAP.Bins.GetAbsFromCode(FromBin);
                    ////don't need to specify a bin on the from WH for serialized items
                    //if (AbsEntry > 0)    // may be comming from a non-bin WH
                    //{
                    //    oXfer.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = 0;
                    //    oXfer.Lines.BinAllocations.BinAbsEntry                   = AbsEntry;
                    //    oXfer.Lines.BinAllocations.BinActionType                 = BinActionTypeEnum.batFromWarehouse;
                    //    oXfer.Lines.BinAllocations.Quantity                      = Qty;
                    //}

                    if (ToBinAbs > 0)
                    {
                        if (oXfer.Lines.BinAllocations.BinActionType == BinActionTypeEnum.batFromWarehouse) oXfer.Lines.BinAllocations.Add();
                        oXfer.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = 0;
                        oXfer.Lines.BinAllocations.BinAbsEntry                   = ToBinAbs;
                        oXfer.Lines.BinAllocations.BinActionType                 = BinActionTypeEnum.batToWarehouse;
                        oXfer.Lines.BinAllocations.Quantity                      = Qty;
                    }


                    //string SQL_Query_BarcodeID = string.Format(@"SELECT SuppSerial FROM [OSRI] WHERE [ItemCode] = '{0}' AND [IntrSerial] = '{1}'",
                    //    ItemCode, PlantID);

                    //Fields ResultFromSQLQuery = UTIL.SQL.GetFieldsFromSQLQuery(SQL_Query_BarcodeID);

                    //// Get real state identifier for plant
                    //BarcodeIDs.Add(ResultFromSQLQuery.Item(0).Value.ToString());

                    // For each plant we are transferring - first pass just get the simple transfers
                    // Create plant journal entries for newly moved plant
                    PlantJournal.AddPlantJournalEntry(PlantID, Globals.JournalEntryType.Transfer,
                        "Plant moved from warehouse #" + FromWH + " to warehouse #" + DestWH);

                    // mark the current entry from the data table
                    //row["Serial\\Batch"]
                    pPlants.Rows[i]["ManBySBN"] = "X";
                }

                // Attempt to add the Stock Transfer
                if (oXfer.Lines.ItemCode != "" && oXfer.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                //------------------------------------
                // second pass - have to serialized plants 
                pPlants.Select("ManBySBN='X'").AsEnumerable().ToList().ForEach(x => x.Delete()); // remove the processed rows
                foreach (System.Data.DataRow row in pPlants.Rows)
                {
                    var BinAbs   = row["BinAbs"].ToString();
                    var Qty      = Convert.ToDouble(row["Quantity"].ToString());
                    var ItemCode = row["ItemCode"].ToString();
                    var PlantID  = row["Serial\\Batch"].ToString();
                    var DestWH   = row["DestWH"].ToString();
                    var FromWH   = row["FromWH"].ToString();
                    var MangBy   = row["ManBySBN"].ToString();

                    var CurrentPlantPhase   = NSC_DI.SAP.Warehouse.GetType(FromWH);
                    var NextPlantPhase      = NSC_DI.SAP.Phase.GetNext(CurrentPlantPhase);

                    // create PdO. Set the batch number and line WH
                    var PdO = NSC_DI.SAP.ProductionOrder.CreateFromBOM("F-" + ItemCode , Qty, DateTime.Now, DestWH, false, "Y");
                    NSC_DI.SAP.ProductionOrder.UpdateRowWH(PdO, FromWH, ItemCode); 
                    NSC_DI.SAP.ProductionOrder.AddBatchSN(PdO, ItemCode, -1, "", PlantID, Qty); // can't do this if PdO is released
                    NSC_DI.SAP.ProductionOrder.Release(PdO);

                    // Issue
                    var PdOI = NSC_DI.SAP.ProductionOrder.Issue(PdO, "Cannabis Plant", Qty, PlantID, null);

                    // Receipt
                    var SNs = NSC_DI.UTIL.AutoStrain.NextSN("F-" + ItemCode, "Serial Plant", Convert.ToInt32(Qty));
                    NSC_DI.SAP.ProductionOrder.ReceiveParent(PdO, DateTime.Today, "", SNs);
                }
            }
            catch (B1Exception ex) { throw ex; }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(oXfer);
            }
        }

        private static void CreateIndividualProductionOrders(Dictionary<string, Dictionary<string, double>> PlantLots, string DestinationWarehouseCode = null)
        {
            //List<int> listPdoKeys = new List<int>();

            //try
            //{
            //    //CloneTransactionData cloneTransDetails = PullCloneDetailsFromForm();
            //    //if (cloneTransDetails == null) return;

            //    bool bUpdatedProdO = false;

            //    foreach (var PlantID in PlantLots.Keys)
            //    {
            //        //Globals.oCompany.StartTransaction();
            //        var PlantBatchItemCode = NSC_DI.SAP.Items.Batches.GetItemCode(PlantID);

            //        //Serialized Flower Item - 
            //        //ToDo:Base this off of BOM or Properties or combination of both 
            //        string ItemCode = PlantBatchItemCode + "-F";

            //        Dictionary<string, double> NewDic = new Dictionary<string, double>();
            //        PlantLots.TryGetValue(PlantID, out NewDic);

            //        //--------------------------------------------
            //        // PRODUCTION ORDER
            //        int PdOKey = NSC_DI.SAP.ProductionOrder.CreateFromBOM(
            //            ItemCode, NewDic.OrderBy(kvp => kvp.Key).First().Value, DateTime.Now, true, "","");
            //        if (PdOKey == 0)
            //        {
            //            Globals.oApp.MessageBox("Failed to create Production Order for batch " + PlantID + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
            //            continue;
            //        }
            //        else
            //        {
            //            //add to list
            //            listPdoKeys.Add(PdOKey);
            //        }

            //        //Check the warehouse for the Plant(s) item(s) on tbe Production BOM and if needed adjust accordingly
            //        //Get Plant Warehouses for selected items on the Dictionaty by batchnumber 
            //        SAPbobsCOM.ProductionOrders oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
            //        oPdO.GetByKey(PdOKey);
            //        SAPbobsCOM.ProductionOrders_Lines oLines = oPdO.Lines;

            //        //Update Destination Warehouse.
            //        if (DestinationWarehouseCode != oPdO.Warehouse)
            //        {
            //            oPdO.Warehouse = DestinationWarehouseCode; 
            //            bUpdatedProdO = true;
            //        }
            //        //Update line Item warehouse
            //        for (int i = 0; i <= oLines.Count - 1; i++)
            //        {
            //            oLines.SetCurrentLine(i);
            //            if (PlantBatchItemCode == oLines.ItemNo)
            //            {
            //                string strWhsSourceHolder = NewDic.OrderBy(kvp => kvp.Key).First().Key;

            //                if (strWhsSourceHolder != oLines.Warehouse && oLines.PlannedQuantity > 0)
            //                {
            //                    //Update warehouse
            //                    oLines.Warehouse = strWhsSourceHolder;
            //                    bUpdatedProdO = true;
            //                }
            //            }
            //        }
            //        if (bUpdatedProdO == true)
            //        {
            //            oPdO.Update();
            //        }


            //        //--------------------------------------------
            //        // ISSUE TO PRODUCTION ORDER
            //        var PdOIssueKey = NSC_DI.SAP.ProductionOrder.Issue(PdOKey, "Cannabis Plant", NewDic.OrderBy(kvp => kvp.Key).First().Value, PlantID, null);
            //        if (PdOIssueKey == 0)
            //        {
            //            Globals.oApp.MessageBox("Failed to Issue to Production for batch " + PlantID + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
            //            continue;
            //        }

            //        //Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
            //    }

            //    Globals.oApp.StatusBar.SetText("Complete.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
            //}
            //catch (Exception ex)
            //{
            //   // if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
            //    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            //}
           
            //return listPdoKeys;
            
        }

        private static void ReceivePlants()
        {
            //            #region Get Information and Create Controllers


            //            // Grab the destination warehouse code from the Production Order
            //            string DestinationWarehouseCode = pDestinationWarehouseCode;

            //            // Create a List to hold items
            //            //List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> listOfGoodsReceiptLines = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();

            //            #endregion

            //            //Create an array of strings to hold newly generated SN numbers for plants being created. 
            //            // Attempt to create a "Receipt from Production" document
            //            // SERIAL NUMBER

            //            try
            //            {
            //                foreach (int PrdOrd in pPrdOrdList)
            //                {
            //                    // Grab the warehouse code from the Matrix in the form UI
            //                    string SourceWarehouseCode = NSC_DI.UTIL.SQL.GetValue<string>(@"
            //            Select T0.WhsCode
            // from[IGE1] T0
            //join[OITM] T1 on T0.ItemCode = T1.ItemCode
            // join[OITB] T2 on T2.ItmsGrpCod = T1.ItmsGrpCod
            // where T0.[BaseRef] = "+ PrdOrd+ @" and T2.ItmsGrpNam ='Cannabis Plant'");

            //                    // Grab the Item Code of the Plant
            //                    string ItemCodeOfPlant = NSC_DI.UTIL.SQL.GetValue<string>(@"Select T0.ItemCode from OWOR T0 where T0.DocNum = " + PrdOrd.ToString()); ;
            //                    string ItemCode = ItemCodeOfPlant;

            //                    string strSQL = @"Select T0.Quantity
            //from[IGE1] T0
            //join[OITM] T1 on T0.ItemCode = T1.ItemCode
            // join[OITB] T2 on T2.ItmsGrpCod = T1.ItmsGrpCod
            // where T0.[BaseRef] = " + PrdOrd + @" and T2.ItmsGrpNam ='Cannabis Plant'";

            //                    int Qty = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);
            //                        //Convert.ToInt16(QuantityToAdd);
            //                    SAPbouiCOM.Form pForm = null;
            //                    int prodOrder = PrdOrd;
            //                    //System.Data.DataTable dtBins = null;
            //                    pForm = Globals.oApp.Forms.Item(_SBO_Form.UniqueID) as Form;

            //                    SAPbouiCOM.DataTable dtBinsS = _SBO_Form.DataSources.DataTables.Item("dtBinSrc");    // NavSol.Forms.F_BinSelect.CreateDT();
            //                    NavSol.Forms.F_BinSelect.dtCreateIn(dtBinsS);
            //                    var j = 0;
            //                    var sql = $@"
            //SELECT DISTINCT 
            //       OITM.ItemCode  AS [ItemCode], OITM.ItemName AS [Name], 
            //	   WOR1.warehouse AS [SrcWhCode], (SELECT WhsName FROM OWHS WHERE WhsCode = WOR1.wareHouse) AS [From WH],
            //	   OWOR.Warehouse AS [DstWhCode], (SELECT WhsName FROM OWHS WHERE WhsCode = OWOR.Warehouse) AS [To WH],
            //	   '' AS [Serial Number], '' AS [BinCode], 
            //       '' AS [LevelID1], '' AS [LevelID2], '' AS [LevelID3], '' AS [LevelID4], 
            //	   0.0 AS [XFer Qty], {Convert.ToDecimal(Qty)}.00 AS [Bin Qty],
            //       WOR1.IssuedQty AS [Old Bin Qty], -1 AS [LinkRecNo]
            //  FROM OWOR
            //  JOIN OITM ON OITM.ItemCode  = OWOR.ItemCode
            //  JOIN WOR1 ON WOR1.DocEntry  = OWOR.DocNum
            //  JOIN OWHS ON WOR1.wareHouse = OWHS.WhsCode
            // WHERE OWOR.DocNum = {prodOrder} 
            //   AND OITM.QryGroup39 = 'Y' AND OWHS.U_NSC_WhrsType='CLO' AND WOR1.IssuedQty > 0";

            //                    // skip the BIN mangement form if the destination warehouse is NOT bin managed
            //                    var wh = NSC_DI.UTIL.SQL.GetValue<string>(string.Format(@"Select warehouse from OWOR where DocNum = {0}", prodOrder), "");
            //                    //if (NSC_DI.SAP.Bins.IsManaged(wh))
            //                    //{
            //                    //    NavSol.Forms.F_BinSelect.FormCreate(_SBO_Form.UniqueID, sql, Qty, wh, "", true);
            //                    //    F_BinSelect.BinsCB = delegate (string callingFormUid, System.Data.DataTable dtBins)
            //                    //    {
            //                    //        Process(pForm, prodOrder, ItemCode, Qty, dtBins);
            //                    //    };
            //                    //}
            //                    //else Process(pForm, prodOrder, ItemCode, Qty, null);
            //                    Process(pForm, prodOrder, ItemCode, Qty, null);
            //                }
            //            }
            //            catch (NSC_DI.SAP.B1Exception ex)
            //            {
            //                Globals.oApp.StatusBar.SetText(ex.Message);
            //            }
        }
    }
}
