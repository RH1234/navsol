﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace NSC_DI.SAP
{
	public class Employee
	{
		public static void CreateRole(string pName, string pDesc)
		{
			if (pName.Trim() == "" | pDesc.Trim() == "") return;

			SAPbobsCOM.EmployeeRolesSetupService oRoleSrv = null;
			SAPbobsCOM.EmployeeRoleSetup addLine = null;

			try
			{
				// see if it already exists
				var ss = UTIL.SQL.GetValue<string>("SELECT [name] FROM OHTY WHERE [name] = '" + pName + "'");
				if (UTIL.SQL.GetValue<string>("SELECT [name] FROM OHTY WHERE [name] = '" + pName + "'") != null) return;

				oRoleSrv = Globals.oCompService.GetBusinessService(SAPbobsCOM.ServiceTypes.EmployeeRolesSetupService);
				addLine = oRoleSrv.GetDataInterface(SAPbobsCOM.EmployeeRolesSetupServiceDataInterfaces.erssEmployeeRoleSetup);

				addLine.Name = pName;	// 20 chars
				addLine.Description = pDesc;	// memo
				oRoleSrv.AddEmployeeRoleSetup(addLine);
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(oRoleSrv);
                UTIL.Misc.KillObject(addLine);
				GC.Collect();
			}
		}
        public static string GetEmpID_User()
        {
            // get the employee ID from the current user

            var sql = "";

            try
            {
                // get the user ID
                sql = $"SELECT USERID FROM OUSR WHERE USER_CODE = '{Globals.oCompany.UserName}'";
                var userID = UTIL.SQL.GetValue<string>(sql, "");

                // get the employee
                sql = $"SELECT empID FROM OHEM WHERE userID = '{userID}'";
                var empID = UTIL.SQL.GetValue<string>(sql, null);

                return empID;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static IEnumerable<string> GetRoleUsers(string pRole)
		{
            // get the list of employees assigned to a role  

            if (string.IsNullOrEmpty(pRole?.Trim())) throw new ArgumentNullException("pRole", "Parameter cannot be null or white space");// Whitespace-Change NSC_DI.UTIL.Strings.Empty(pRole)

            try
			{
				var SQL = "";
				SQL = "SELECT OUSR.USER_CODE FROM HEM6 INNER JOIN OHTY ON HEM6.roleID = OHTY.typeID"
					+ " INNER JOIN OHEM ON HEM6.empID = OHEM.empID INNER JOIN OUSR ON OHEM.userId = OUSR.USERID WHERE OHTY.name = N'" + pRole + "'";

                System.Data.DataTable dt = UTIL.SQL.DataTable(SQL);
                var res = from System.Data.DataRow dr in dt.Rows select dr["USER_CODE"] as string;
                return res;
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
		}
	}

}
