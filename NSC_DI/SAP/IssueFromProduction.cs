﻿using System;
using System.Collections.Generic;

namespace NSC_DI.SAP
{
    public static class IssueFromProduction
    {
        /// <summary>
        /// Contains the ID or Code for the object that was recently created.
        /// </summary>
        public static int NewlyCreatedKey { get; set; }

        public static int Create(int productionOrderKey, DateTime documentDate, List<GoodsIssued.GoodsIssued_Lines> listOfGoodsIssuedLines, string CallingForm = null, string groupName = null)
        {
            try
            {
                // Grab the next available code from the SQL database
                //int NextDocNumber = Convert.ToInt32(UTIL.SQL.GetNextAvailableCodeForItem("OIGE", "DocNum"));

                // Prepare a Goods Issued document
                SAPbobsCOM.Documents goodsIssuedProductionOrder = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
                goodsIssuedProductionOrder.DocDate = documentDate;
                goodsIssuedProductionOrder.DocDueDate = documentDate;
                goodsIssuedProductionOrder.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items;
                //goodsIssuedProductionOrder.DocNum       = NextDocNumber;
                goodsIssuedProductionOrder.JournalMemo = "Issue for Production";
                
                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null)
                    goodsIssuedProductionOrder.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                // For each item passed through the list of items
                foreach (GoodsIssued.GoodsIssued_Lines GI_Line in listOfGoodsIssuedLines)
                {
                    // Add information about the production order
                    goodsIssuedProductionOrder.Lines.BaseEntry = productionOrderKey;
                    goodsIssuedProductionOrder.Lines.BaseType = 202;
                    NSC_DI.SAP.Document.SetDefaultDistributionRules(goodsIssuedProductionOrder.Lines);

                    if (GI_Line.BaseLine >= 0)
                    {
                        goodsIssuedProductionOrder.Lines.BaseLine = GI_Line.BaseLine;
                    }
                    else
                    {
                        //Get row number from production order 
                        int intLineNum = NSC_DI.UTIL.SQL.GetValue<int>("select LineNum from WOR1 where WOR1.DocEntry = " + productionOrderKey + " and WOR1.ItemCode = '" + GI_Line.ItemCode + "'");//??12121
                        if (intLineNum >= 0)
                        {
                            goodsIssuedProductionOrder.Lines.BaseLine = intLineNum;
                        }
                        else
                        {
                            goodsIssuedProductionOrder.Lines.ItemCode = GI_Line.ItemCode;
                        }


                    }



                    // Add information about the item
                    //goodsIssuedProductionOrder.Lines.Price           = 0;
                    //goodsIssuedProductionOrder.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                    goodsIssuedProductionOrder.Lines.Quantity = GI_Line.Quantity;
                    goodsIssuedProductionOrder.Lines.WarehouseCode = GI_Line.WarehouseCode;


                    if (GI_Line.SysSerialNumbers != null)
                    {
                        foreach (var serialNumber in GI_Line.SysSerialNumbers)
                        {
                            goodsIssuedProductionOrder.Lines.SerialNumbers.SystemSerialNumber = serialNumber;
                            goodsIssuedProductionOrder.Lines.SerialNumbers.Quantity = GI_Line.Quantity;
                            goodsIssuedProductionOrder.Lines.SerialNumbers.Add();
                        }
                    }

                    if (GI_Line.BatchLineItems != null)
                    {
                        foreach (GoodsIssued.GoodsIssued_Lines_Batches BatchLineItem in GI_Line.BatchLineItems)
                        {
                            goodsIssuedProductionOrder.Lines.BatchNumbers.BatchNumber = BatchLineItem.BatchNumber;
                            goodsIssuedProductionOrder.Lines.BatchNumbers.Quantity = BatchLineItem.Quantity;
                            goodsIssuedProductionOrder.Lines.BatchNumbers.Add();
                        }
                    }

                    // set the Branch
                    var br = NSC_DI.SAP.Branch.Get(goodsIssuedProductionOrder.Lines.WarehouseCode);
                    if (br > 0) goodsIssuedProductionOrder.BPL_IDAssignedToInvoice = br;

                    goodsIssuedProductionOrder.Lines.Add();
                }

                // Attempt to add the new item to the database
                int ResponseCode = goodsIssuedProductionOrder.Add();

                // Attempt to add the goods issued document
                if (ResponseCode != 0)
                {
                    throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }

                string newCode = null;

                // Get the newly created object's code
                Globals.oCompany.GetNewObjectCode(out newCode);

                if (newCode != null)
                {
                    NewlyCreatedKey = Convert.ToInt32(newCode);

                    // Someone on SAP Developer Network says you can pass the "202" object type, but you can NOT define the components item codes, the DI-API/SDK will automatically assign the item codes.
                    // TODO-SAP-DIAPI: Replace the SQL method with BoBCOM interactions

                    #region DELETE ME SOME DAY!

                    // Update through SQL 
                    string sql = @"
UPDATE
[IGE1]
SET
[BaseRef] = '" + productionOrderKey + @"'
, [BaseType] = '202'
, [BaseEntry] = '" + productionOrderKey + @"'
, [TreeType] = 'N'

WHERE [DocEntry] = '" + NewlyCreatedKey + @"'
";

                    UTIL.SQL.RunSQLQuery(sql);

                    #endregion
                }
                return NewlyCreatedKey;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oBar);
                GC.Collect();
            }
        }

    }

}
