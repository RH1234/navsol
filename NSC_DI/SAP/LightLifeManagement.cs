﻿using System;

namespace NSC_DI.SAP
{
	public static class LightLifeManagement 
    {
		public static void CreateNewLightLifeLog(string UserName, string LightItem, string Quantity, string ReplaceDate, string Warehouse)
        {
            // Prepare a connection to the user table
			SAPbobsCOM.UserTable sboTable = null;

            try
            {
				sboTable = Globals.oCompany.UserTables.Item(Globals.tLightLife);
				//Setting Data to Master Data Table Fields  
                sboTable.UserFields.Fields.Item("U_LightItem").Value	= LightItem;
                sboTable.UserFields.Fields.Item("U_Quantity").Value		= Quantity;
                sboTable.UserFields.Fields.Item("U_ReplaceDate").Value	= DateTime.ParseExact(ReplaceDate,"yyyyMMdd",System.Globalization.CultureInfo.InvariantCulture);
                sboTable.UserFields.Fields.Item("U_Warehouse").Value	= Warehouse;
                sboTable.UserFields.Fields.Item("U_CreatedOn").Value	= DateTime.Now;
				sboTable.UserFields.Fields.Item("U_CreatedBy").Value	= UserName;

                // Attempt to add the new item to the database
				if (sboTable.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(sboTable);
			}
		}
	}
}
