﻿using System;
using System.Collections.Generic;
using SAPbobsCOM;

namespace NSC_DI.SAP
{
	public static class GoodsIssued 
    {
        /// <summary>
        /// Contains the ID or Code for the object that was recently created.
        /// </summary>
		public static int NewlyCreatedKey { get; set; }

		public static int Create(DateTime documentDate, List<GoodsIssued_Lines> ListOfGoodsIssuedLines)
        {
            // TODO: Write in support for "Service" type of Goods Issued doucment  

            // Grab the next available code from the SQL database
			int NextDocNumber = Convert.ToInt32(UTIL.SQL.GetNextAvailableCodeForItem("OIGE", "DocNum"));

            // Prepare a Goods Issued document
			Documents goodsIssue = (Documents) Globals.oCompany.GetBusinessObject(BoObjectTypes.oInventoryGenExit);
            goodsIssue.DocDate = documentDate;
            goodsIssue.DocType = BoDocumentTypes.dDocument_Items;
            goodsIssue.DocNum = NextDocNumber;
            goodsIssue.UserFields.Fields.Item("U_NSC_StateSyncStatus").Value = "R";
          
            // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
            var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
            if (getEmpID != null)
                goodsIssue.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

            int i = 0;
            // For each item passed through the list of items
            foreach (GoodsIssued_Lines GI_Line in ListOfGoodsIssuedLines)
            {
                if (i == 0)
                {
                    // only want to check the 1st row
                    var br = NSC_DI.SAP.Branch.Get(GI_Line.WarehouseCode);
                    if (br > 0) goodsIssue.BPL_IDAssignedToInvoice = br;
                }               

                goodsIssue.Lines.ItemCode = GI_Line.ItemCode;
                goodsIssue.Lines.Quantity = GI_Line.Quantity;
                double wasteWeight = GI_Line.WasteWeight;
                var testone = goodsIssue.Lines.SerialNumbers.UserFields.Fields.Count;
                //goodsIssue.Lines.UnitsOfMeasurment = GI_Line.UoMPer;


                goodsIssue.Lines.WarehouseCode = GI_Line.WarehouseCode;
                NSC_DI.SAP.Document.SetDefaultDistributionRules(goodsIssue.Lines);

                if (GI_Line.BatchLineItems != null)
                {                   
                    foreach (var batch_line_item in GI_Line.BatchLineItems)
                    {
                        goodsIssue.Lines.BatchNumbers.BatchNumber = batch_line_item.BatchNumber;
                        goodsIssue.Lines.BatchNumbers.Quantity = batch_line_item.Quantity;
                        goodsIssue.Lines.BatchNumbers.Add();
                        //if (wasteWeight > 0)
                        //    NSC_DI.SAP.BatchItems.SetUDF(GI_Line.ItemCode, batch_line_item.BatchNumber, "U_NSC_WasteWeight", wasteWeight.ToString());// NO NEED TO SET OSRN WASTEWEIGHT RIGHT NOW
                    }
                }
                   
            
                if (GI_Line.SysSerialNumbers != null)
                {
                    if (wasteWeight > 0)                   
                        NSC_DI.SAP.SerialItems.SetUDF(GI_Line.DistNum, GI_Line.ItemCode, "U_NSC_WasteWeight", wasteWeight.ToString());// 13816 sets wasteweight udf osrn used in plant quarantine view
                    foreach (var serialNumber in GI_Line.SysSerialNumbers)
                    {
                        goodsIssue.Lines.SerialNumbers.SystemSerialNumber = serialNumber;
                        goodsIssue.Lines.SerialNumbers.Quantity = GI_Line.Quantity;
                        goodsIssue.Lines.SerialNumbers.Add();
                    }
                }             
                   

                if(GI_Line.UserDefinedFields != null) // 12903 added in the ability to handle udfs in the goodsissue
                    foreach (var key in GI_Line.UserDefinedFields.Keys)
                    {
                        var test = key;
                        goodsIssue.UserFields.Fields.Item(key).Value = GI_Line.UserDefinedFields[key].ToString();
                    }

                if (i==0)
                {
                    //do nothing except increase i
                    i++;
                    if (ListOfGoodsIssuedLines.Count > 1)
                    {
                        goodsIssue.Lines.Add();
                    }
                }
                else
                {
                    goodsIssue.Lines.Add();
                    i++;
                }                
            }

            if (goodsIssue.Add() < 0) B1Exception.RaiseException();

            // Get the newly created object's code
			Globals.oCompany.GetNewObjectCode(out string newCode);

            if (string.IsNullOrEmpty(newCode)) B1Exception.RaiseException();

            return Convert.ToInt32(newCode);
        }

        public class GoodsIssued_Lines
        {
            public string ItemCode { get; set; }
            public double Quantity { get; set; }
            public string WarehouseCode { get; set; }
            public int BaseLine { get; set; } = -1;
            public double UoMPer { get; set; }
            public double WasteWeight { get; set; } = 0.00;
            public string DistNum { get; set; }

            public List<int> SysSerialNumbers { get; set; }

            public List<GoodsIssued_Lines_Batches> BatchLineItems { get; set; }

            public Dictionary<string, string> UserDefinedFields { get; set; }
        }

        public class GoodsIssued_Lines_Batches
        {
            public string BatchNumber { get; set; }
            public double Quantity { get; set; }
        }
    }
}
