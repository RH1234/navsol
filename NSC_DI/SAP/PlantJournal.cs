﻿using System;

namespace NSC_DI.SAP
{
	public static class PlantJournal
	{
		public static void AddPlantJournalEntry(string PlantID, Globals.JournalEntryType Type, string Note, double quantity = 0.0, string serial = null)
		{
            // update the table with direct SQL

            string CodeHolder = null;
            var sql = "";
            SAPbobsCOM.Recordset oRS = null;

            try
            {
                oRS = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                // Prepare a connection to the user table
                if (Type.ToString() == "Remarks")
                {
                    //Check to see if there is existing remarks we only want one record to hold the remarks.  
                    sql = $"SELECT Code FROM [@{NSC_DI.Globals.tPlantJournal}] WHERE [U_PlantID] = '{PlantID}' AND U_EntryType = 'Remarks'";
                    CodeHolder = NSC_DI.UTIL.SQL.GetValue<string>(sql, null);
                }

                var _date = NSC_DI.UTIL.Dates.GetForUI(DateTime.Now); ;
                if (CodeHolder == null)
                {
                    sql = $@"EXEC('
INSERT INTO [@{NSC_DI.Globals.tPlantJournal}] (Name, U_PlantID, U_DateCreated, U_EntryType, U_Note, U_GUID, U_Quantity)
VALUES ('''', ''{PlantID}'', ''{_date}'', ''{Type.ToString()}'', ''{Note}'', ''{Guid.NewGuid().ToString()}'', {quantity.ToString()})
 ')";
                }
                else
                {
                    sql = $@"EXEC('
UPDATE [@{NSC_DI.Globals.tPlantJournal}]
   SET Name = '''', U_PlantID = ''{PlantID}'', U_DateCreated = ''{_date}'', U_EntryType = ''{Type.ToString()}'', U_Note = ''{Note}'', 
        U_GUID = ''{Guid.NewGuid().ToString()}'', U_Quantity = {quantity.ToString()}
 WHERE Code = {CodeHolder}
 ')";
                }

                NSC_DI.UTIL.SQL.RunSQLQuery(sql);

            }
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(oRS);
                GC.Collect();
            }
        }
        public static void AddPlantJournalEntry_OLD(string PlantID, Globals.JournalEntryType Type, string Note, double quantity = 0.0, string serial = null)
        {
            SAPbobsCOM.UserTable sboTable = null;
            string CodeHolder = null;

            try
            {
                // Prepare a connection to the user table
                sboTable = Globals.oCompany.UserTables.Item(Globals.tPlantJournal);
                if (Type.ToString() == "Remarks")
                {
                    //Check to see if there is existing remarks we only want one record to hold the remarks.                     
                    CodeHolder = NSC_DI.UTIL.SQL.GetValue<string>(@"Select Code from [@NSC_PLANTJOURNAL] WHERE [U_PlantID] = '" + PlantID + "' and U_EntryType = 'Remarks'");
                    if (CodeHolder != null)
                    {
                        sboTable.GetByKey(CodeHolder);
                    }
                }
                // Set the values for the newly created item
                sboTable.Name = "";
                sboTable.UserFields.Fields.Item("U_PlantID").Value = PlantID;
                sboTable.UserFields.Fields.Item("U_DateCreated").Value = DateTime.Now;
                sboTable.UserFields.Fields.Item("U_EntryType").Value = Type.ToString();
                sboTable.UserFields.Fields.Item("U_Note").Value = Note;
                sboTable.UserFields.Fields.Item("U_GUID").Value = Guid.NewGuid().ToString();

                // optional parms
                if (quantity > 0) sboTable.UserFields.Fields.Item("U_Quantity").Value = quantity;
                if (!string.IsNullOrEmpty(serial)) sboTable.UserFields.Fields.Item("U_Serial").Value = serial;
                // Attempt to add the new item to the database
                if (CodeHolder != null)
                {
                    sboTable.Update();

                }
                else if (sboTable.Add() != 0) B1Exception.RaiseException();

            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(sboTable);
            }
        }
    }
}
