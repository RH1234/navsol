﻿using System;

namespace NSC_DI.SAP
{
	public static class Compound 
    {
		public static void CreateCompound(string Code, string Name, string Type)
        {
			SAPbobsCOM.GeneralService	oGeneralService = null;
			SAPbobsCOM.GeneralData		oGeneralData	= null;

            try
            {
				SAPbobsCOM.CompanyService oCompService = Globals.oCompany.GetCompanyService();
				oGeneralService = oCompService.GetGeneralService(Globals.tCompounds + "_C");
                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

                oGeneralData.SetProperty("Code", Code);
				oGeneralData.SetProperty("U_Name", Name);
                oGeneralData.SetProperty("U_Type", Type);

                oGeneralService.Add(oGeneralData);
            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(oGeneralService);
				UTIL.Misc.KillObject(oGeneralData);

				GC.Collect();
			}
		}

        /// <summary>
        /// Create a Relation between Compound and Effect.
        /// </summary>
        /// <param name="Code">The code of the new row</param>
        /// <param name="CompoundCode">Compound Code</param>
        /// <param name="EffectCode">Effect Code</param>
		public static void CreateCompoundEffectRelation(string Code, string CompoundCode, string EffectCode)
        {
            SAPbobsCOM.GeneralService	oGeneralService = null;
			SAPbobsCOM.GeneralData		oGeneralData	= null;

            try
            {
				var oCompService = Globals.oCompany.GetCompanyService();

				oGeneralService = oCompService.GetGeneralService(Globals.tCompEffect + "_C");

                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

                oGeneralData.SetProperty("Code", Code);

                oGeneralData.SetProperty("U_CompoundCode", CompoundCode);

                oGeneralData.SetProperty("U_EffectCode", EffectCode);

                oGeneralService.Add(oGeneralData);

            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(oGeneralService);
				UTIL.Misc.KillObject(oGeneralData);

				GC.Collect();
			}
		}

		public static void RemoveCompoundEffectRelation(string EffectCode, string CompoundCode)
        {
            // Using a record set to delete a relation table record.
			SAPbobsCOM.Recordset oRecordSet = null;
            //SAPbobsCOM.GeneralService oGeneralService = null;
            //SAPbobsCOM.GeneralDataParams oGeneralDataParams = null;
            try
            {
                //SAPbobsCOM.CompanyService oCompService = Globals.oCompany.GetCompanyService();

                //oGeneralService = oCompService.GetGeneralService(Globals.tCompEffect + "_C");

                //oGeneralDataParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams);

                //oGeneralDataParams.SetProperty("U_EffectCode", EffectCode);
                //oGeneralDataParams.SetProperty("U_CompoundCode", CompoundCode);

                //oGeneralService.Delete(oGeneralDataParams);

                oRecordSet = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                var sqlQuery = $"DELETE FROM [@{Globals.tCompEffect}] WHERE U_EffectCode = '{EffectCode}' AND U_CompoundCode = '{CompoundCode}'";

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);
            }
            catch(Exception)
            {
                //TODO-LOG: Log error
				throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
			}
			finally
			{
				UTIL.Misc.KillObject(oRecordSet);

				GC.Collect();
			}
		}
    }
}
