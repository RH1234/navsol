﻿using System;
using System.Linq;

namespace NSC_DI.SAP
{
    public static class Bins
    {
        public static bool IsManaged(string pWH_Code)
        {
            // get the current Bin ID (AbsEntry) from a Bin Code
            try
            {
                if (string.IsNullOrEmpty(pWH_Code?.Trim())) return false;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pWH_Code))
                var sql = "";
                sql += $"SELECT BinActivat FROM OWHS WHERE WhsCode = '{pWH_Code}'";
                return (NSC_DI.UTIL.SQL.GetValue<string>(sql, "N") == "Y");
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static bool IsSystem(string pBinCode)
        {
            // check if a system bin
            try
            {
                if (string.IsNullOrEmpty(pBinCode?.Trim())) return false;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pBinCode))
                var sql = "";
                sql += $"SELECT SysBin FROM OBIN WHERE BinCode = '{pBinCode}'";
                return (NSC_DI.UTIL.SQL.GetValue<string>(sql, "N") == "Y");
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static int GetAbsFromCode(string pCode)
        {
            // get the current Bin ID (AbsEntry) from a Bin Code
            try
            {
                if (string.IsNullOrEmpty(pCode?.Trim())) return 0;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pCode))
                var sql = "";
                sql += $"SELECT OBIN.AbsEntry FROM OBIN WHERE OBIN.BinCode = '{pCode}'";
                return NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static int GetAbsFromSN(string pSN, string pItemCode = null)
        {
            // get the current Bin ID (AbsEntry) from a Serial Number    OSRQ ON OSRN.ItemCode = OSRQ.ItemCode AND OSRN.SysNumber = OSRQ.SysNumber
            try
            {
                if (string.IsNullOrEmpty(pSN?.Trim())) return 0;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pSN))
                var sql = "";
                sql += $"SELECT TOP (1) OSBQ.BinAbs FROM OSRN" + Environment.NewLine;
                sql += $" INNER JOIN OSBQ ON OSRN.AbsEntry  = OSBQ.SnBMDAbs" + Environment.NewLine;
                sql += $" WHERE OSRN.DistNumber = '{pSN}'" + Environment.NewLine;
                //if(pItemCode != null) sql += $"   AND ItemCode = '{pItemCode}'" + Environment.NewLine;
                sql += $" ORDER BY OSBQ.AbsEntry DESC";
                return NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static string GetCodeFromSN(string pSN)
        {
            // get the current Bin ID  from a Serial Number    OSRQ ON OSRN.ItemCode = OSRQ.ItemCode AND OSRN.SysNumber = OSRQ.SysNumber
            try
            {
                if (string.IsNullOrEmpty(pSN?.Trim())) return "";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pSN))
                var sql = "";
                sql += $"SELECT TOP (1) OBIN.BinCode FROM OSRN" + Environment.NewLine;
                sql += $" INNER JOIN OSBQ ON OSRN.AbsEntry  = OSBQ.SnBMDAbs" + Environment.NewLine;
                sql += $" INNER JOIN OBIN ON OBIN.AbsEntry  = OSBQ.BinAbs" + Environment.NewLine;
                sql += $" WHERE OSRN.DistNumber = '{pSN}'" + Environment.NewLine;
                sql += $" ORDER BY OSBQ.AbsEntry DESC";
                return NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static string GetCodeFromBatch(string pBatch)
        {
            // get the current Bin ID  from a Serial Number    OSRQ ON OSRN.ItemCode = OSRQ.ItemCode AND OSRN.SysNumber = OSRQ.SysNumber
            try
            {
                if (string.IsNullOrEmpty(pBatch?.Trim())) return "";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pBatch))
                var sql = "";
                sql += $"SELECT TOP (1) OBIN.BinCode FROM OBTN" + Environment.NewLine;
                sql += $" INNER JOIN OBBQ ON OBTN.AbsEntry  = OBBQ.SnBMDAbs" + Environment.NewLine;
                sql += $" INNER JOIN OBIN ON OBIN.AbsEntry  = OBBQ.BinAbs" + Environment.NewLine;
                sql += $" WHERE OBTN.DistNumber = '{pBatch}'" + Environment.NewLine;
                sql += $" ORDER BY OBBQ.AbsEntry DESC";
                return NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static string GetWH(string pBin)
        {
            // get the warehouse from the Bin ID
            try
            {
                if (string.IsNullOrEmpty(pBin?.Trim())) return "";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pBin))
                var sql = "";
                sql += $"SELECT TOP (1) OBIN.WhsCode FROM OBIN" + Environment.NewLine;
                sql += $" WHERE OBIN.BinCode = '{pBin}'" + Environment.NewLine;
                return NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static int GetBinCount(string pWH)
        {
            // get the bin count
            try
            {
                var sql = $"SELECT COUNT(*) FROM OBIN WHERE WhsCode = '{pWH}'";
                return NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static string GetLevelCode(string pBinCode, int pLevel)
        {
            // get the level code 
            try
            {
                if (string.IsNullOrEmpty(pBinCode?.Trim())) return "";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pBinCode))
                var level = pLevel.ToString();
                var sql = "";
                sql += $"SELECT OBIN.SL{level}Code FROM OBIN WHERE OBIN.BinCode = '{pBinCode}'";
                return NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static int GetIdFromDiffWH(int pBinID, string pWH)
        {
            // get the corrisponding Bin ID (AbsEntry) for a different warehouse
            try
            {
                if (pBinID < 0 || string.IsNullOrEmpty(pWH?.Trim())) return 0;// Whitespace-Change NSC_DI.UTIL.Strings.Empty(pWH))

                // get each level code for the Bin
                var l1 = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT SL1Code FROM OBIN WHERE AbsEntry ={pBinID.ToString()}", "");
                var l2 = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT SL2Code FROM OBIN WHERE AbsEntry ={pBinID.ToString()}", "");
                var l3 = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT SL3Code FROM OBIN WHERE AbsEntry ={pBinID.ToString()}", "");
                var l4 = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT SL4Code FROM OBIN WHERE AbsEntry ={pBinID.ToString()}", "");

                // get the Bin code for the new warehouse
                var sql = "";
                sql += $"SELECT AbsEntry FROM OBIN " + Environment.NewLine;
                sql += $" WHERE WhsCode = '{pWH}'" + Environment.NewLine;
                if (l1 != "") sql += $"   AND SL1Code = '{l1}'";
                if (l2 != "") sql += $"   AND SL2Code = '{l2}'";
                if (l3 != "") sql += $"   AND SL3Code = '{l3}'";
                if (l4 != "") sql += $"   AND SL4Code = '{l4}'";
                return NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static int GetLevelCount()
        {
            // get the level count
            try
            {
                var sql = "SELECT COUNT(*) FROM OBFC WHERE Activated = 'Y' AND FldType = 'S'";
                return NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static int GetLevelCount(string pWH)
        {
            // get the level count for a Warehouse
            try
            {
                var sql = $"SELECT TOP 1 IIF(SL1Abs IS NULL,0,1) + IIF(SL2Abs IS NULL,0,1) + IIF(SL3Abs IS NULL,0,1) + IIF(SL4Abs IS NULL,0,1) FROM OBIN WHERE SysBin = 'N' AND WhsCode = '{pWH}'";
                return NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static string[] GetLevelNames()
        {
            // get the level names
            try
            {
                var dt = NSC_DI.UTIL.SQL.DataTable("SELECT CASE WHEN Activated = 'Y' THEN DispName ELSE '' END AS DispName FROM OBFC WHERE FldType = 'S'");
                string[] BinLevNames = dt.Select().Select(n => n["DispName"] as string).ToArray();
                return BinLevNames;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static string[] GetLevelNames(string pWH)
        {
            // get the level names for a warehouse
            try
            {
                var sql = $@" 
SELECT 1, ISNULL((SELECT OBFC.DispName FROM OBSL INNER JOIN OBFC ON OBSL.FldAbs = OBFC.AbsEntry WHERE OBSL.AbsEntry = (SELECT TOP 1 OBIN.SL1Abs FROM OBIN WHERE SysBin = 'N' AND WhsCode = '{pWH}')), '') AS DispName UNION
SELECT 2, ISNULL((SELECT OBFC.DispName FROM OBSL INNER JOIN OBFC ON OBSL.FldAbs = OBFC.AbsEntry WHERE OBSL.AbsEntry = (SELECT TOP 1 OBIN.SL2Abs FROM OBIN WHERE SysBin = 'N' AND WhsCode = '{pWH}')), '') AS DispName UNION
SELECT 3, ISNULL((SELECT OBFC.DispName FROM OBSL INNER JOIN OBFC ON OBSL.FldAbs = OBFC.AbsEntry WHERE OBSL.AbsEntry = (SELECT TOP 1 OBIN.SL3Abs FROM OBIN WHERE SysBin = 'N' AND WhsCode = '{pWH}')), '') AS DispName UNION
SELECT 4, ISNULL((SELECT OBFC.DispName FROM OBSL INNER JOIN OBFC ON OBSL.FldAbs = OBFC.AbsEntry WHERE OBSL.AbsEntry = (SELECT TOP 1 OBIN.SL4Abs FROM OBIN WHERE SysBin = 'N' AND WhsCode = '{pWH}')), '') AS DispName";
                var dt = NSC_DI.UTIL.SQL.DataTable(sql);
                string[] BinLevNames = dt.Select().Select(n => n["DispName"] as string).ToArray();
                return BinLevNames;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static double GetMaxQty(string pWH, string pLv1="", string pLv2 = "", string pLv3 = "", string pLv4 = "")
        {
            // get the Max qty for a bin
            try
            {
                if (string.IsNullOrEmpty(pWH?.Trim())) return 0;// Whitespace-Change NSC_DI.UTIL.Strings.Empty(pWH)
                var sql = "";
                sql += $"SELECT TOP (1) MaxLevel FROM OBIN" + Environment.NewLine;
                sql += $" WHERE WhsCode = '{pWH}'" + Environment.NewLine;
                sql += (pLv1 == "" || pLv1 == null) ? "" : $" AND SL1Code = '{pLv1}'";
                sql += (pLv2 == "" || pLv2 == null) ? "" : $" AND SL2Code = '{pLv2}'";
                sql += (pLv3 == "" || pLv3 == null) ? "" : $" AND SL3Code = '{pLv3}'";
                sql += (pLv4 == "" || pLv4 == null) ? "" : $" AND SL4Code = '{pLv4}'";
                var qty = NSC_DI.UTIL.SQL.GetValue<double>(sql, 0.0d);
                if (qty > 0.0d) return qty;
                sql = "";
                sql += $"SELECT TOP (1) MaxLevel FROM OBIN" + Environment.NewLine;
                sql += $" WHERE WhsCode = '{pWH}'" + Environment.NewLine;
                return NSC_DI.UTIL.SQL.GetValue<int>(sql, 0.0d);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static double GetQOH(string pItemCode, string pBinCode)
        {
            // get the Current qty for a bin
            try
            {
                var sql = "";
                sql += $"SELECT OnHandQty FROM OIBQ" + Environment.NewLine;
                sql += $" INNER JOIN OBIN ON OIBQ.AbsEntry = OBIN.AbsEntry" + Environment.NewLine;
                sql += $" WHERE OIBQ.ItemCode = '{pItemCode}'" + Environment.NewLine;
                sql += $"   AND OBIN.pBinCode  = '{pBinCode}'";
                return NSC_DI.UTIL.SQL.GetValue<int>(sql, 0.0);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static double GetQOH(string pItemCode, string pWH, string pLv1 = "", string pLv2 = "", string pLv3 = "", string pLv4 = "")
        {
            // get the Current qty for a bin
            try
            {
                if (string.IsNullOrEmpty(pWH?.Trim())) return 0;// Whitespace-Change NSC_DI.UTIL.Strings.Empty(pWH)
                var sql = "";
                sql += $"SELECT OnHandQty FROM OIBQ" + Environment.NewLine;
                sql += $" INNER JOIN OBIN ON OIBQ.BinAbs = OBIN.AbsEntry" + Environment.NewLine;
                sql += $" WHERE OIBQ.ItemCode = '{pItemCode}'" + Environment.NewLine;
                sql += $"   AND OIBQ.WhsCode  = '{pWH}'" + Environment.NewLine;
                sql += (pLv1 == "" || pLv1 == null) ? "" : $" AND OBIN.SL1Code = '{pLv1}'";
                sql += (pLv2 == "" || pLv2 == null) ? "" : $" AND OBIN.SL2Code = '{pLv2}'";
                sql += (pLv3 == "" || pLv3 == null) ? "" : $" AND OBIN.SL3Code = '{pLv3}'";
                sql += (pLv4 == "" || pLv4 == null) ? "" : $" AND OBIN.SL4Code = '{pLv4}'";
                return NSC_DI.UTIL.SQL.GetValue<double>(sql, 0.0);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static double GetFreeQty(string pItemCode, string pWH, string pLv1 = "", string pLv2 = "", string pLv3 = "", string pLv4 = "")
        {
            // get the Free qty (max - QOH) for a bin
            try
            {
                return GetMaxQty(pWH, pLv1, pLv2, pLv3, pLv4) - GetQOH(pItemCode, pWH, pLv1, pLv2, pLv3, pLv4);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static string GetCodePull(string pItemCode, string pWH, double pQty, string pLv1 = "", string pLv2 = "", string pLv3 = "", string pLv4 = "")
        {
            // get the bin code for the item based on the location and available quantity to pull from

            if (string.IsNullOrEmpty(pItemCode?.Trim())) return "";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pItemCode)
            if (string.IsNullOrEmpty(pWH?.Trim())) return "";// Whitespace-Change NSC_DI.UTIL.Strings.Empty(pWH)

            try
            {
                var sql = $@"
SELECT TOP 1 OBIN.BinCode 
  FROM OBIN
  LEFT JOIN OIBQ ON OBIN.AbsEntry = OIBQ.BinAbs
 WHERE OBIN.WhsCode = '{pWH}' AND OIBQ.OnHandQty >= {pQty} 
   AND OIBQ.ItemCode = '{pItemCode}'
   AND ISNULL(OBIN.SL1Code, '') >= '{pLv1}'
   AND ISNULL(OBIN.SL2Code, '') >= '{pLv2}'
   AND ISNULL(OBIN.SL3Code, '') >= '{pLv3}'
   AND ISNULL(OBIN.SL4Code, '') >= '{pLv4}'
";
                return NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static SAPbobsCOM.Recordset GetCodePush(string pWH, double pQty, string pLv1 = "", string pLv2 = "", string pLv3 = "", string pLv4 = "")
        {
            // get the bin code based on the location and available quantity to push to - **** NOT COMPLETED YET

            SAPbobsCOM.Recordset oRS = null;

            try
            {
                var sql = $@"
SELECT OBIN.BinCode, OBIN.MaxLevel, ISNULL(OIBQ.OnHandQty, 0.0) AS [QOH], OBIN.MaxLevel - ISNULL(OIBQ.OnHandQty, 0.0) AS [FreeQty]
  FROM OBIN
  LEFT JOIN OIBQ ON OBIN.AbsEntry = OIBQ.BinAbs
 WHERE OBIN.SysBin = 'N' AND OBIN.WhsCode = '{pWH}'
   AND ISNULL(OBIN.SL1Code, '') >= '{pLv1}'
   AND ISNULL(OBIN.SL2Code, '') >= '{pLv2}'
   AND ISNULL(OBIN.SL3Code, '') >= '{pLv3}'
   AND ISNULL(OBIN.SL4Code, '') >= '{pLv4}'
";
                oRS = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRS.DoQuery(sql);
                return oRS;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static SAPbobsCOM.Recordset GetBinQtys(string pWH, string pLv1 = "", string pLv2 = "", string pLv3 = "", string pLv4 = "", string pSysBin = "N")
        {
            // get the list of bins and their qtys starting with specific WH \ level

            SAPbobsCOM.Recordset oRS = null;

            try
            {
                var sql = $@"
SELECT OBIN.BinCode, OBIN.MaxLevel,
       ISNULL((SELECT SUM(OIBQ.OnHandQty) FROM OIBQ WHERE OIBQ.BinAbs = OBIN.AbsEntry AND OIBQ.WhsCode = OBIN.WhsCode), 0.0) AS [QOH],
	   OBIN.MaxLevel - 
	   ISNULL((SELECT SUM(OIBQ.OnHandQty) FROM OIBQ WHERE OIBQ.BinAbs = OBIN.AbsEntry AND OIBQ.WhsCode = OBIN.WhsCode), 0.0) AS [FreeQty]
  FROM OBIN
 WHERE OBIN.SysBin = '{pSysBin}' AND OBIN.WhsCode = '{pWH}'
   AND ISNULL(OBIN.SL1Code, '') >= '{pLv1}'
   AND ISNULL(OBIN.SL2Code, '') >= '{pLv2}'
   AND ISNULL(OBIN.SL3Code, '') >= '{pLv3}'
   AND ISNULL(OBIN.SL4Code, '') >= '{pLv4}'
";
                oRS = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRS.DoQuery(sql);

                if(oRS.RecordCount > 0) return oRS;

                // nothing found, just search using the WH
                sql = $@"
SELECT OBIN.BinCode, OBIN.MaxLevel,
       ISNULL((SELECT SUM(OIBQ.OnHandQty) FROM OIBQ WHERE OIBQ.BinAbs = OBIN.AbsEntry AND OIBQ.WhsCode = OBIN.WhsCode), 0.0) AS [QOH],
	   OBIN.MaxLevel - 
	   ISNULL((SELECT SUM(OIBQ.OnHandQty) FROM OIBQ WHERE OIBQ.BinAbs = OBIN.AbsEntry AND OIBQ.WhsCode = OBIN.WhsCode), 0.0) AS [FreeQty]
  FROM OBIN
 WHERE OBIN.SysBin = '{pSysBin}' AND OBIN.WhsCode = '{pWH}'";

                oRS.DoQuery(sql);
                return oRS;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }
    }
}
