﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSC_DI.SAP
{
    public class BusinessPartners
    {
        public static T GetField<T>(string CardCode, string field)
        {
            try
            {
                var qry = $"SELECT {field} FROM OCRD WHERE CardCode = '{CardCode}'";//??12121
                return NSC_DI.UTIL.SQL.GetValue<T>(qry);
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }
        public static string GetGroupName(string CardCode)
        {
            try
            {
                var qry = $"SELECT GroupName FROM OCRG INNER JOIN OCRD ON OCRG.GroupCode = OCRD.GroupCode WHERE CardCode = '{CardCode}'";
                return NSC_DI.UTIL.SQL.GetValue<string>(qry, "");
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }
        
        public static string GetPartnerCode(string pBusPartnerName)
        {
            try
            {
                string busPartnerCode = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT CardCode FROM OCRD WHERE CardName = '{pBusPartnerName}'");
                return busPartnerCode;
            }
            catch(Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }           
        }
    }
}
