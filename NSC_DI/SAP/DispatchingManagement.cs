﻿using System;
using System.Collections.Generic;

namespace NSC_DI.SAP
{
	public static class DispatchingManagement 
    {
		private static SAPbobsCOM.GeneralService oGeneralService = null;
		private static SAPbobsCOM.GeneralData	 oGeneralData	 = null;

		/// <summary>
        /// Creates a new Route
        /// </summary>
        /// <param name="newRouteToCreate">Data transfer object to set all required fields.</param>
		public static string CreateNewRoute(CreateRoute newRouteToCreate)
        {
			try
            {
                // Grab the next available code from the SQL database
                var NextCode = UTIL.SQL.GetNextAvailableCodeForItem(Globals.tRouteDetls, "Code");

                var oCompService = Globals.oCompany.GetCompanyService();

				oGeneralService = oCompService.GetGeneralService(Globals.tRouteDetls + "_C");

                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

				oGeneralData.SetProperty("Code", NextCode);

                oGeneralData.SetProperty("Name", newRouteToCreate.RouteName);

                oGeneralData.SetProperty("U_LicenseType", newRouteToCreate.LicenseType);

                // Set it to true|false... with "T" | "F"
                if (newRouteToCreate.IsActive)
                {
                    oGeneralData.SetProperty("U_Active", "Y");
                }
                else
                {
                    oGeneralData.SetProperty("U_Active", "N");
                }

                // Attempt to add the new item to the database
                oGeneralService.Add(oGeneralData);

                // Associate Deliveries to our newly created Route.
                AssociateDeliveriesToRoute(newRouteToCreate.DeliveryCodes, NextCode);
                return NextCode;
            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
        }

        /// <summary>
        /// Update existing route details
        /// </summary>
        /// <param name="routeUpdate">Route update request object.</param>
		public static void UpdateRouteDetails(UpdateRoute routeUpdate)
        {
            // Prepare a connection to the user table
			SAPbobsCOM.UserTable sboTable = (SAPbobsCOM.UserTable)Globals.oCompany.UserTables.Item(Globals.tRouteDetls);
            try
            {
                sboTable.GetByKey(routeUpdate.RouteId);

                sboTable.UserFields.Fields.Item("U_Map").Value		 = routeUpdate.Map;
                sboTable.UserFields.Fields.Item("U_EmpID").Value	 = routeUpdate.EmployeeId;
                sboTable.UserFields.Fields.Item("U_VehicleID").Value = routeUpdate.VehicleId;

                sboTable.UserFields.Fields.Item("U_Vehicle").Value	 = routeUpdate.Vehicle;
                sboTable.UserFields.Fields.Item("U_Driver").Value	 = routeUpdate.Driver;

                sboTable.UserFields.Fields.Item("U_Room").Value      = routeUpdate.Room;
                sboTable.UserFields.Fields.Item("U_EmpID2").Value    = routeUpdate.EmployeeId2;
                sboTable.UserFields.Fields.Item("U_Driver2").Value   = routeUpdate.Driver2 ?? "";
                sboTable.UserFields.Fields.Item("U_SchdOutDate").Value = routeUpdate.DepartureDate;
                sboTable.UserFields.Fields.Item("U_SchdOutTime").Value = routeUpdate.DepartureTime;

                var isActiveInSAPDB = "Y";
                if (!routeUpdate.IsActive)
                {
                    isActiveInSAPDB = "N";
                }
                sboTable.UserFields.Fields.Item("U_Active").Value	 = isActiveInSAPDB;

				int response = sboTable.Update();	//TODO-ERROR MESSAGE ?
            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
        }
        public static void UpdateRouteStops(string pCode, List<UpdateRouteStop> pStops)
        {
            SAPbobsCOM.GeneralService        oGS     = null;
            SAPbobsCOM.GeneralData           oHeader = null;
            SAPbobsCOM.GeneralDataParams     oParams = null;
            SAPbobsCOM.GeneralData           oRow    = null;
            SAPbobsCOM.GeneralDataCollection oGDC    = null;

            try
            {
                int line = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT MAX(LineId) FROM [@{NSC_DI.Globals.tRouteStops}] WHERE Code = {pCode}", 0);

                // Get a handle to the UDO
                oGS = Globals.oCompService.GetGeneralService("NSC_ROUTE_DETAILS_C");

                //Get UDO record
                oParams = oGS.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams);
                oParams.SetProperty("Code", pCode);
                oHeader = oGS.GetByParams(oParams);

                foreach (UpdateRouteStop lRow in pStops)
                {
                    oGDC = oHeader.Child("NSC_ROUTE_STOPS");
                    if (lRow.Line < 0)                  // new record
                    {
                        oRow = oGDC.Add();
                        oRow.SetProperty("U_DocType", lRow.DocType);
                        oRow.SetProperty("U_DocEntry", lRow.DocEntry);
                        oRow.SetProperty("U_DocNum", lRow.DocNum);
                    }
                    else oRow = oGDC.Item(lRow.Line - 1);    // existing record

                    oRow.SetProperty("U_DestSeqNum", lRow.Order);
                    oRow.SetProperty("U_ArriveDate", lRow.ArriveDate);
                    oRow.SetProperty("U_ArriveTime", lRow.ArriveTime);
                    oRow.SetProperty("U_RouteText",  (lRow.RouteText)?? "");    // null is not a valid value.
                    oRow.SetProperty("U_QRCode",     (lRow.QRCode) ?? "");      // null is not a valid value.
                }

                oGS.Update(oHeader);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGS);
                NSC_DI.UTIL.Misc.KillObject(oHeader);
                NSC_DI.UTIL.Misc.KillObject(oParams);
                NSC_DI.UTIL.Misc.KillObject(oRow);
                NSC_DI.UTIL.Misc.KillObject(oGDC);
                GC.Collect();
            }
        }

        /// <summary>
        /// Associates a List of Deliveries to a Route.
        /// </summary>
        /// <param name="deliveryCodes">List of distinct deliveries</param>
        /// <param name="RouteID">Unique identifier of the route</param>
        /// <returns></returns>
		public static void AssociateDeliveriesToRoute(List<int> deliveryCodes, string RouteID)
        {
			SAPbobsCOM.Documents deliverNote = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes);
            List<NSC_DI.SAP.DispatchingManagement.UpdateRouteStop> oStops = new List<NSC_DI.SAP.DispatchingManagement.UpdateRouteStop>();
            try
            {

                // All Deliveries in the system need to be updated to contain the new RouteCode in the UDF -> U_VSC_RouteID
                foreach (int deliveryCode in deliveryCodes)
            {
                NSC_DI.SAP.DispatchingManagement.UpdateRouteStop oStop = new NSC_DI.SAP.DispatchingManagement.UpdateRouteStop();
                    deliverNote.GetByKey(deliveryCode);
					deliverNote.UserFields.Fields.Item("U_NSC_RouteID").Value = Convert.ToInt16(RouteID);
					int response = deliverNote.Update();	//TODO-ERROR MESSAGE ?

                oStop.Line       = -1;
                oStop.ArriveDate = deliverNote.DocDueDate;
                oStop.Order      =  0;
                oStop.DocType    = (int)deliverNote.DocObjectCode;
                oStop.DocEntry   = deliverNote.DocEntry;
                oStop.DocNum     = deliverNote.DocNum;
                oStops.Add(oStop);
            }

            NSC_DI.SAP.DispatchingManagement.UpdateRouteStops(RouteID, oStops);

            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static bool DriverAndVehicleAreSet(string RouteID)
        {
			SAPbobsCOM.Recordset dbCommunication = (SAPbobsCOM.Recordset)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            // Query to get all the Inventory items that are being delivered in this route.
            string sqlQuery = string.Format(@"
SELECT 
DISTINCT
 [DLN1].[Quantity] AS                                   [Quantity]
, [DLN1].[Price] AS                                     [Price]
, [DLN1].[LineTotal] AS                                 [LineTotal]
, [@" + Globals.tRouteDetls + @"].[U_EmpID] AS          [EmployeeId]
, [@" + Globals.tRouteDetls + @"].[U_VehicleID] AS      [VehicleId]
, [@" + Globals.tRouteDetls + @"].[U_ManifestID] AS     [ManifestId]
, [DLN1].[U_NSC_StateID] AS                             [StateId]
, [OCRD].[U_NSC_LocUBI] AS							    [StateLocationLicenseNumber]

FROM [DLN1]
JOIN [OCRD] ON [OCRD].[CardCode] = [DLN1].[BaseCard]
JOIN [ODLN] ON [ODLN].[DocEntry] = [DLN1].[DocEntry]
JOIN [@" + Globals.tRouteDetls + @"] ON [@" + Globals.tRouteDetls + @"].Code = [ODLN].[U_NSC_RouteID]
	AND [@" + Globals.tRouteDetls + @"].Code = '{0}'", RouteID);

            dbCommunication.DoQuery(sqlQuery);

            // We are guessing that the Documenation is meaning Location License Number instead of the StateUBI. BioTrack API documenation is ambiguous.
            string vendorLicense = dbCommunication.Fields.Item(SQLDeliveryResult.LocationLicenseNumber).Value.ToString();

            // Get required information for the API call from the Query.
            string vehicleId = dbCommunication.Fields.Item(SQLDeliveryResult.VehicleId).Value;
            string employeeId = dbCommunication.Fields.Item(SQLDeliveryResult.EmployeeId).Value.ToString();

            bool exists = true;
            if (string.IsNullOrEmpty(vehicleId) || string.IsNullOrEmpty(vehicleId))
            {
                exists = false;
            }

            return exists;
        }

        private enum SQLDeliveryResult { Quantity, Price, LineTotal, EmployeeId, VehicleId, ManifestId, InventoryStateId, LocationLicenseNumber }
        /// <summary>
        /// Starts a Route of Deliveries 24 hour wait timer.
        /// </summary>
        /// <param name="RouteID">Unique indentifier of the route</param>
		public static void StartRouteDeliveriesTimer(string RouteID)
        {
            #region Creating required controllers for the API call
/*		// COMPLIANCE
            // Prepare a connection to the state API
            var TraceCon = new NavSol.Controllers.TraceabilityAPI(SAPBusinessOne_Application: Globals.oApp,
                SAPBusinessOne_Company: Globals.oCompany);

            // Create an API call -> inventory_manifest
            BioTrack.API bioTrackApi = TraceCon.new_API_obj();

            // Get our Settings controller for the State Information for Complain calls.
            NavSol.Controllers.Settings controllerSettings = new Controllers.Settings(
                SAPBusinessOne_Application: Globals.oApp,
                SAPBusinessOne_Company: Globals.oCompany,
                SAPBusinessOne_Form: null);

            // Add the API Call into our compliance monitor.
            Controllers.Compliance controllerCompliance = new Controllers.Compliance(SAPBusinessOne_Application: Globals.oApp, SAPBusinessOne_Company: Globals.oCompany);
*/
            #endregion Creating required controllers for the API call

            // Prepare a connection to the user table
			SAPbobsCOM.UserTable sboTable = (SAPbobsCOM.UserTable)Globals.oCompany.UserTables.Item(Globals.tRouteDetls);
            try
            {
                sboTable.GetByKey(RouteID);

                sboTable.UserFields.Fields.Item("U_CNT_DOWN_DATE").Value = DateTime.Now;
                sboTable.UserFields.Fields.Item("U_CNT_DOWN_ON").Value = DateTime.Now.Ticks.ToString();

	            if (sboTable.Update() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));	// RHH added throw

				var dbCommunication = (SAPbobsCOM.Recordset)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                // Query to get all the Inventory items that are being delivered in this route.
                string sqlQuery = string.Format(@"
SELECT 
DISTINCT
 [DLN1].[Quantity] AS                                   [Quantity]
, [DLN1].[Price] AS                                     [Price]
, [DLN1].[LineTotal] AS                                 [LineTotal]
, [@" + Globals.tRouteDetls + @"].[U_EmpID] AS          [EmployeeId]
, [@" + Globals.tRouteDetls + @"].[U_VehicleID] AS      [VehicleId]
, [@" + Globals.tRouteDetls + @"].[U_ManifestID] AS     [ManifestId]
, [DLN1].[U_NSC_StateID] AS                             [StateId]
, [OCRD].[U_NSC_LocUBI] AS							    [StateLocationLicenseNumber]

FROM [DLN1]
JOIN [OCRD] ON [OCRD].[CardCode] = [DLN1].[BaseCard]
JOIN [ODLN] ON [ODLN].[DocEntry] = [DLN1].[DocEntry]
JOIN [@" + Globals.tRouteDetls + @"] ON [@" + Globals.tRouteDetls + @"].Code = [ODLN].[U_NSC_RouteID]
	AND [@" + Globals.tRouteDetls + @"].Code = '{0}'", RouteID);

                dbCommunication.DoQuery(sqlQuery);

                // We are guessing that the Documenation is meaning Location License Number instead of the StateUBI. BioTrack API documenation is ambiguous.
                string vendorLicense = dbCommunication.Fields.Item(SQLDeliveryResult.LocationLicenseNumber).Value.ToString();

                // Get required information for the API call from the Query.
                int vehicleId = Int32.Parse(dbCommunication.Fields.Item(SQLDeliveryResult.VehicleId).Value);
                string employeeId = dbCommunication.Fields.Item(SQLDeliveryResult.EmployeeId).Value.ToString();

/*		// COMPLIANCE
                List<BioTrack.VehicleStop> destinationList = new List<BioTrack.VehicleStop>();
                List<string> transferInventoryStateIds = new List<string>();

                // Create our list of barcodes.
                for (int i = 0; i < dbCommunication.RecordCount; i++)
                {
                    transferInventoryStateIds.Add(dbCommunication.Fields.Item(SQLDeliveryResult.InventoryStateId).Value.ToString());
                    if (!dbCommunication.EoF)
                    {
                        dbCommunication.MoveNext();
                    }
                }
                // Move back to our first element.
                dbCommunication.MoveFirst();

                // Currently just assuming our Routes have no real order..
                int stopNumber = 0;

                // Approximate Route is not required by Washington State regulations.. All Approximate Routes will be marked as the following.
                string approximateRoute = "Not required to have approx route as per WAC § 314-55-085";

                // Chase said this approximation is alright since you only have 24 hours after the 24 hour wait timer is up to complete the delivery.
				int approximateDeparture = Compliance.DateTimeToTimeStamp(DateTime.Now.AddHours(24).AddMinutes(30));
				int approximateArrival	 = Compliance.DateTimeToTimeStamp(DateTime.Now.AddHours(30));

                destinationList.Add(new BioTrack.VehicleStop()
                {
                    BarcodeID = transferInventoryStateIds.ToArray(),
                    ApproximateArrival = approximateArrival,
                    ApproximateDeparture = approximateDeparture,
                    ApproximateRoute = approximateRoute,
                    StopNumber = stopNumber,
                    VendorLicense = vendorLicense
                });

                // License_Number of this Facility
                string locationId = controllerSettings.GetValueOfSetting("StateLocLic");

                BioTrack.Inventory.Manifest(
                    BioTrackAPI: ref bioTrackApi,
                    EmployeeID: employeeId,
                    VehicleID: vehicleId,
                    Location: locationId,
                    StopsOverview: destinationList.ToArray());

                string compID = controllerCompliance.CreateComplianceLineItem(
                    Reason: NavSol.Models.Compliance.ComplianceReasons.Inventory_Manifest,
                    API_Call: bioTrackApi.XmlApiRequest.ToString());

                // Attempt to post to the BioCrap API
                bioTrackApi.PostToApi();

                if (bioTrackApi.WasSuccesful)
                {
                    System.Xml.XmlNodeList responseBarcodesXML = bioTrackApi.xmlDocFromResponse.SelectNodes("/xml/barcode_id");

                    string resultManifestId = responseBarcodesXML[0].InnerText;

                    if (string.IsNullOrEmpty(resultManifestId))
                    {
                        // Some reason the xml didn't return a response.
                        Globals.oApp.StatusBar.SetText(
                            Text: "API was successful but the ManifestId was not returned!",
                            Seconds: SAPbouiCOM.BoMessageTime.bmt_Medium,
                            Type: SAPbouiCOM.BoStatusBarMessageType.smt_Error
                        );

                        // Update the Compliance monitor with a failure
                        controllerCompliance.UpdateComliancy(
                            ComplianceID: compID,
                            ResponseXML: "Missing ManifestID: " + bioTrackApi.xDocFromResponse.ToString(),
                            Status: Controllers.Compliance.ComplianceStatus.Failed);

                        return;
                    }

                    sboTable.UserFields.Fields.Item("U_" + Globals.SAP_PartnerCode + "_" + "ManifestID").Value = resultManifestId;

                    response = sboTable.Update();

                    // Update the Compliance monitor with a success
                    controllerCompliance.UpdateComliancy(
                        ComplianceID: compID,
                        ResponseXML: bioTrackApi.xDocFromResponse.ToString(),
                        Status: Controllers.Compliance.ComplianceStatus.Success);

                    Globals.oApp.StatusBar.SetText(
                        Text: "Successfully generated Manifest!",
                        Seconds: SAPbouiCOM.BoMessageTime.bmt_Medium,
                        Type: SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                }
                else
                {
                    // Update the Compliance monitor with a failure
                    controllerCompliance.UpdateComliancy(
                        ComplianceID: compID,
                        ResponseXML: bioTrackApi.xDocFromResponse.ToString(),
                        Status: Controllers.Compliance.ComplianceStatus.Failed);

                    Globals.oApp.StatusBar.SetText(
                        Text: "Failed to generate Manifest!",
                        Seconds: SAPbouiCOM.BoMessageTime.bmt_Medium,
                        Type: SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                }
				*/
            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
        }

        /// <summary>
        /// Check to see if the current route wait timer has complete.
        /// </summary>
        /// <param name="RouteID">Unique indentifier of the route</param>
		public static bool IsWaitTimerComplete(string RouteID)
        {
            bool timerIsComplete = false;
            try
            {
                string sqlQuery = string.Format(@"
SELECT 
[U_CNT_DOWN_ON]
F ROM [@" + Globals.tRouteDetls + @"]
WHERE [@" + Globals.tRouteDetls + @"].Code = '{0}'
  AND [@" + Globals.tRouteDetls + @"].[U_Active] = 'Y'
  AND [@" + Globals.tRouteDetls + @"].[U_OutTime] IS NULL
  AND [@" + Globals.tRouteDetls + @"].[U_CNT_DOWN_ON] IS NOT NULL", RouteID);

				SAPbobsCOM.Fields resultOfQuery = UTIL.SQL.GetFieldsFromSQLQuery(sqlQuery);

                long parsedStartTimeTicks = 0;

                // Parse the ticks out of the database.
                if (!long.TryParse(resultOfQuery.Item(0).Value.ToString, out parsedStartTimeTicks))
                {
                    throw new Exception(UTIL.Message.Format("Failed to parse sql query results!"));
                }

                // Get the Date and Time of when the timer was started based on the ticks stored in the database.
                DateTime timerDateTime = new DateTime(parsedStartTimeTicks);

                // If the timer was started over a day ago the timer is complete.
                if (DateTime.Now >= timerDateTime.AddDays(1))
                {
                    timerIsComplete = true;
                }

				return timerIsComplete;
			}
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
        }

        /// <summary>
        /// Sets a Route of Deliveries as Out
        /// </summary>
        /// <param name="RouteID">Unique indentifier of the route</param>
		public static void MarkRouteDeliveriesAsOut(string RouteID)
        {
			/*		// COMPLIANCE
            // Prepare a connection to the state API
            var TraceCon = new NavSol.Controllers.TraceabilityAPI(SAPBusinessOne_Application: Globals.oApp,
                SAPBusinessOne_Company: Globals.oCompany);

            // Create an API call -> inventory_manifest
            BioTrack.API bioTrackApi = TraceCon.new_API_obj();
			*/

			// RHH - code is never used
            // Get our Settings controller for the State Information for Complain calls.
			//NavSol.Controllers.Settings controllerSettings = new Controllers.Settings(
			//	SAPBusinessOne_Application: Globals.oApp,
			//	SAPBusinessOne_Company: Globals.oCompany,
			//	SAPBusinessOne_Form: null);

			if (RouteID == "") throw new Exception(UTIL.Message.Format("Route ID is empty."));	

            try
            {
				// Prepare a connection to the user table
				SAPbobsCOM.UserTable sboTable = Globals.oCompany.UserTables.Item(Globals.tRouteDetls);

				sboTable.GetByKey(RouteID);

                // Update our Route Details to have an Out date and dime of now.
                sboTable.UserFields.Fields.Item("U_OutDate").Value = DateTime.Now;
                sboTable.UserFields.Fields.Item("U_OutTime").Value  = DateTime.Now;
                sboTable.UserFields.Fields.Item("U_StateSyncStatus").Value = "R";

                if (sboTable.Update() != 0) throw new Exception(UTIL.Message.Format("Route ID was not specified."));

                // Prepare a sql connection for our rediculous query..
				SAPbobsCOM.Recordset dbCommunication = (SAPbobsCOM.Recordset)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                string sqlQuery = string.Format(@"
SELECT 
 [DLN1].[Quantity] AS                                   [Quantity]
, [DLN1].[Price] AS                                     [Price]
, [DLN1].[LineTotal] AS                                 [LineTotal]
, [@" + Globals.tRouteDetls + @"].[U_EmpID] AS          [EmployeeId]
, [@" + Globals.tRouteDetls + @"].[U_VehicleID] AS      [VehicleId]
, [@" + Globals.tRouteDetls + @"].[U_ManifestID] AS     [ManifestId]
, [DLN1].[U_NSC_StateID] AS                             [StateId]
FROM [DLN1]
JOIN [ODLN] ON [ODLN].[DocEntry] = [DLN1].[DocEntry]
JOIN [@" + Globals.tRouteDetls + @"] ON [@" + Globals.tRouteDetls + @"].Code = [ODLN].[U_NSC_RouteID]
 AND [@" + Globals.tRouteDetls + @"].Code = '{0}'", RouteID);

                dbCommunication.DoQuery(sqlQuery);

                string manifestId = dbCommunication.Fields.Item(SQLDeliveryResult.ManifestId).Value.ToString();

				/*		// COMPLIANCE
					// Create an API call -> Inventory_Transfer_OutBound
					List<BioTrack.Inventory.InvTransferOut> inventoryGoingOut = new List<BioTrack.Inventory.InvTransferOut>();

					for (int i = 0; i < dbCommunication.RecordCount; i++)
					{
						double totalPrice = double.Parse(dbCommunication.Fields.Item(SQLDeliveryResult.LineTotal).Value.ToString());
						inventoryGoingOut.Add(new BioTrack.Inventory.InvTransferOut()
						{
							BarcodeID = dbCommunication.Fields.Item(SQLDeliveryResult.InventoryStateId).Value.ToString(),
							Price = totalPrice
						});

						// Move to the next row.
						if (!dbCommunication.EoF)
						{
							dbCommunication.MoveNext();
						}
					}

					// Create the TrasferOutbound API call.
					BioTrack.Inventory.TransferOutbound(
						BioTrackAPI: ref bioTrackApi,
						ManifestID: manifestId,
						data: inventoryGoingOut.ToArray());

					// Log our API call.
					string compID = controllerCompliance.CreateComplianceLineItem(
					   Reason: NavSol.Models.Compliance.ComplianceReasons.Inventory_Transfer_Outbound,
					   API_Call: bioTrackApi.XmlApiRequest.ToString());

					// Post to the state.
					bioTrackApi.PostToApi();

					if (bioTrackApi.WasSuccesful)
					{
						// Update the Compliance monitor with a success
						controllerCompliance.UpdateComliancy(
							ComplianceID: compID,
							ResponseXML: bioTrackApi.xDocFromResponse.ToString(),
							Status: Controllers.Compliance.ComplianceStatus.Success);
					}
					else
					{
						// Update the Compliance monitor with a failure
						controllerCompliance.UpdateComliancy(
							ComplianceID: compID,
							ResponseXML: bioTrackApi.xDocFromResponse.ToString(),
							Status: Controllers.Compliance.ComplianceStatus.Failed);
					}
					*/
            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
        }

        /// <summary>
        /// Sets a Route of Deliveries as in and sets the route to inactive.
        /// </summary>
        /// <param name="RouteID">Unique indentifier of the route</param>
		public static void MarkRouteDeliveriesAsIn(string RouteID)
        {
            // Prepare a connection to the user table
			var sboTable = Globals.oCompany.UserTables.Item(Globals.tRouteDetls);

            try
            {
                sboTable.GetByKey(RouteID);

                sboTable.UserFields.Fields.Item("U_InDate").Value = DateTime.Now;
                sboTable.UserFields.Fields.Item("U_InTime").Value = DateTime.Now.Ticks.ToString();

                // Set it to false... with "N"
                sboTable.UserFields.Fields.Item("U_Active").Value = "N";

				if (sboTable.Update() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));	// RHH - added throw
			}
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
        }

        /// <summary>
        /// Void the Route. Doing this will create a Manifest Void and set the route as inactive.
        /// </summary>
        /// <param name="RouteID"></param>
		public static void VoidRouteOfDeliveries(string RouteID)
        {
			/*		// COMPLIANCE

			// Prepare a connection to the state API
            var TraceCon = new NavSol.Controllers.TraceabilityAPI(SAPBusinessOne_Application: Globals.oApp,
                SAPBusinessOne_Company: Globals.oCompany);

            // Create an API call -> inventory_manifest
            BioTrack.API bioTrackApi = TraceCon.new_API_obj();
                SAPBusinessOne_Form: null);
			*/

            // Get all details of the current route.
            RouteDetailsView detailsOfCurrentRoute = GetRouteDetailsByRouteId(RouteID);

            // Create an Update Object.
            UpdateRoute updateRoute = new UpdateRoute()
            {
                EmployeeId = detailsOfCurrentRoute.EmployeeId,
                RouteId = RouteID,
                Vehicle = detailsOfCurrentRoute.VehicleName,
                VehicleId = detailsOfCurrentRoute.VehicleId,
                Driver = detailsOfCurrentRoute.DriverName,
                IsActive = false,
                Map = ""
            };

            UpdateRouteDetails(updateRoute);
            
            // Prepare a sql connection for our rediculous query..
			SAPbobsCOM.Recordset dbCommunication = (SAPbobsCOM.Recordset)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            string deliveryDocEntriesQuery = string.Format(@"
SELECT [ODLN].DocEntry
FROM [ODLN]
JOIN [@" + Globals.tRouteDetls + @" ON [@" + Globals.tRouteDetls + @"].Code = [ODLN].[U_NSC_RouteID]
WHERE [@" + Globals.tRouteDetls + @"].Code = '{0}'", RouteID);

            dbCommunication.DoQuery(deliveryDocEntriesQuery);
            dbCommunication.MoveFirst();

            List<string> deliveryDocEntries = new List<string>();
            for (int i = 0; i < dbCommunication.RecordCount; i++)
            {
                string deliveryDocEntry = dbCommunication.Fields.Item(0).Value.ToString();
                deliveryDocEntries.Add(deliveryDocEntry);

                dbCommunication.MoveNext();
            }

            RemoveDeliveriesFromRoute(deliveryDocEntries);

            string sqlQuery = string.Format(@"
SELECT 
[@" + Globals.tRouteDetls + @"].[U_ManifestID]
FROM [@" + Globals.tRouteDetls + @"]
WHERE [@" + Globals.tRouteDetls + @"].Code = '{0}'", RouteID);

            dbCommunication.DoQuery(sqlQuery);

            string manifestId = dbCommunication.Fields.Item(0).Value.ToString();

			/*		// COMPLIANCE
            // Create the Manifest Void Call.
            BioTrack.Inventory.ManifestVoid(BioTrackAPI: ref bioTrackApi, ManifestID: manifestId);

            // Log our API call.
            string compID = controllerCompliance.CreateComplianceLineItem(
               Reason: NavSol.Models.Compliance.ComplianceReasons.Inventory_Manifest_Void,
               API_Call: bioTrackApi.XmlApiRequest.ToString());

            // Post to the state.
            bioTrackApi.PostToApi();

            if (bioTrackApi.WasSuccesful)
            {
                // Update the Compliance monitor with a success
                controllerCompliance.UpdateComliancy(
                    ComplianceID: compID,
                    ResponseXML: bioTrackApi.xDocFromResponse.ToString(),
                    Status: Controllers.Compliance.ComplianceStatus.Success);

                this.WasSuccessful = true;
            }
            else
            {
                // Update the Compliance monitor with a failure
                controllerCompliance.UpdateComliancy(
                    ComplianceID: compID,
                    ResponseXML: bioTrackApi.xDocFromResponse.ToString(),
                    Status: Controllers.Compliance.ComplianceStatus.Failed);
            }
			*/
        }

		private static bool RemoveDeliveriesFromRoute(List<string> deliveryDocEntries)
        {
            bool allSucceeded = true;
            foreach (string docEntry in deliveryDocEntries)
            {
				SAPbobsCOM.Documents deliveryDocument = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes);
                if (deliveryDocument.GetByKey(Convert.ToInt32(docEntry)))
                {
                    deliveryDocument.UserFields.Fields.Item("U_NSC_RouteID").Value = -1;
                    deliveryDocument.Lines.UserFields.Fields.Item("U_NSC_RouteID").Value = -1;
                    int result = deliveryDocument.Update();
                    if (result != 0)
                    {
                        allSucceeded = false;
                    }
                }
                else
                {
                    allSucceeded = false;
                }
            }

            return allSucceeded;
        }

        /// <summary>
        /// Retrieves the route details for the specified id
        /// </summary>
        /// <param name="RouteID">Unique identifier for a route</param>
		public static RouteDetailsView GetRouteDetailsByRouteId(string RouteID)
        {
            RouteDetailsView routeDetails = new RouteDetailsView();
            try
            {

                var sqlQuery = $@"
SELECT Name AS [RouteName], [U_Driver] AS [DriverName], [U_Vehicle] AS [VehicleName], [U_EmpID] AS [EmployeeId]
	 , [U_VehicleID] AS [VehicleId], [U_Active] AS [IsActive], [U_Map] AS [Map], [U_OutTime]AS [OutTimeInTicks]
     , [U_CNT_DOWN_ON] AS[TimerStartedTicks]
     , U_Driver2, U_EmpID2, U_Room, U_ManifestID, U_SchdOutDate, U_SchdOutTime, U_Map, U_LicenseType
 FROM [@{Globals.tRouteDetls}]
 WHERE Code = '{RouteID}'";

				SAPbobsCOM.Fields resultOfQuery = UTIL.SQL.GetFieldsFromSQLQuery(sqlQuery);

                routeDetails.RouteName	    = resultOfQuery.Item(0).Value;
                routeDetails.DriverName	    = resultOfQuery.Item(1).Value;
                routeDetails.VehicleName    = resultOfQuery.Item(2).Value;
                routeDetails.EmployeeId	    = resultOfQuery.Item(3).Value;
                routeDetails.VehicleId	    = resultOfQuery.Item(4).Value;
                routeDetails.Map            = resultOfQuery.Item(6).Value;
                routeDetails.DriverName2    = resultOfQuery.Item(9).Value;
                routeDetails.EmployeeId2    = resultOfQuery.Item(10).Value;
                routeDetails.Room           = resultOfQuery.Item(11).Value;
                routeDetails.ManifestID     = resultOfQuery.Item(12).Value;
                routeDetails.DepartureDate  = resultOfQuery.Item(13).Value;
                routeDetails.DepartureTime  = resultOfQuery.Item(14).Value;
                routeDetails.LicenseType    = resultOfQuery.Item(16).Value;

                if (!string.IsNullOrEmpty(resultOfQuery.Item(5).Value) && resultOfQuery.Item(5).Value == "Y")
                {
                    routeDetails.IsActive = true;
                }

                routeDetails.Map = resultOfQuery.Item(6).Value;

                routeDetails.AllowEditing = true;

                // If the Delivery is already set to out.. we cannot edit things now.
                //if (!string.IsNullOrEmpty(resultOfQuery.Item(7).Value))
                if (resultOfQuery.Item(7).Value > 0)
                {
                    routeDetails.AllowEditing = false;
                }

                routeDetails.EnableVoidManifest = false;

                // If the Delivery is already set to out.. we cannot edit things now.
                if (!string.IsNullOrEmpty(resultOfQuery.Item(8).Value) && routeDetails.AllowEditing)
                {
                    routeDetails.EnableVoidManifest = true;
                }

            }
            catch (Exception ex)
            {
				throw new Exception(UTIL.Message.Format(ex));
			}
            return routeDetails;
        }

        /// <summary>
        /// Wrapper class for create a new route
        /// </summary>
        public class CreateRoute
        {
            public bool IsActive { get; set; }
            public string RouteName { get; set; }
            public string LicenseType { get; set; }
            public List<int> DeliveryCodes { get; set; }
        }

        /// <summary>
        /// Wrapper class for updating route details
        /// </summary>
        public class UpdateRoute
        {
            public string RouteId { get; set; }
            public bool IsActive { get; set; }
            public string Driver { get; set; }
            public string Vehicle { get; set; }
            public string Map { get; set; }
            public string EmployeeId { get; set; }
            public string VehicleId { get; set; }
            public string EmployeeId2 { get; set; }
            public string Driver2 { get; set; }
            public string Room { get; set; }
            public DateTime DepartureDate { get; set; }
            public DateTime DepartureTime { get; set; }
        }
        public class UpdateRouteStop
        {
            public string RouteId { get; set; }
            public int Line { get; set; }
            public int Order { get; set; }
            public int DocType { get; set; }
            public int DocEntry { get; set; }
            public int DocNum { get; set; }
            public DateTime ArriveDate { get; set; }
            public DateTime ArriveTime { get; set; }
            public string RouteText { get; set; }
            public string QRCode { get; set; }
        }

        public class RouteDetailsView
        {
            public string RouteName { get; set; }
            public string DriverName { get; set; }
            public string EmployeeId { get; set; }
            public string VehicleName { get; set; }
            public string VehicleId { get; set; }
            public bool IsActive { get; set; }
            public string Map { get; set; }
            public string DriverName2 { get; set; }
            public string EmployeeId2 { get; set; }
            public string Room { get; set; }
            public DateTime DepartureDate { get; set; }
            public int DepartureTime { get; set; }
            public string ManifestID { get; set; }
            public string LicenseType { get; set; }

            // Flag to see if the user can still edit portions of the Route Details
            public bool AllowEditing { get; set; }

            public bool EnableVoidManifest { get; set; }
        }
    }
}
