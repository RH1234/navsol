﻿using System;
using System.Collections.Generic;

namespace NSC_DI.SAP
{
    public static class ReceiptFromProduction
    {
        /// <summary>
        /// Contains the ID or Code for the object that was recently created.
        /// </summary>
        public static int NewlyCreatedKey { get; set; }

        public static int Create(int productionOrderKey, DateTime documentDate, List<GoodsReceipt.GoodsReceipt_Lines> listOfGoodsReceiptLines, string CallingForm = null, string groupName = null)
        {
            try
            {
                // Grab the next available code from the SQL database
                int NextDocNumber = Convert.ToInt32(UTIL.SQL.GetNextAvailableCodeForItem("OIGN", "DocNum"));

                // Prepare a Goods Receipt document
                SAPbobsCOM.Documents goodsReceiptProductionOrder = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                // Add items to the Goods Receipt document
                goodsReceiptProductionOrder.DocDate     = documentDate;
                goodsReceiptProductionOrder.DocDueDate  = documentDate;
                goodsReceiptProductionOrder.DocType     = SAPbobsCOM.BoDocumentTypes.dDocument_Items;
                goodsReceiptProductionOrder.DocNum      = NextDocNumber;
                goodsReceiptProductionOrder.JournalMemo = "Receipt from Production";

                // For each item passed through the list of items
                foreach (GoodsReceipt.GoodsReceipt_Lines GI_Line in listOfGoodsReceiptLines)
                {
                    // Add information about the production order
                    goodsReceiptProductionOrder.Lines.BaseType  = 0;
                    goodsReceiptProductionOrder.Lines.BaseEntry = productionOrderKey;


                    //goodsReceiptProductionOrder.Lines.ItemCode          = GI_Line.ItemCode;
                    //goodsReceiptProductionOrder.Lines.Price             = 0;
                    goodsReceiptProductionOrder.Lines.TransactionType   = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                    goodsReceiptProductionOrder.Lines.Quantity          = GI_Line.Quantity;
                    goodsReceiptProductionOrder.Lines.WarehouseCode     = GI_Line.WarehouseCode;

                    NSC_DI.SAP.Document.SetDefaultDistributionRules(goodsReceiptProductionOrder.Lines);

                    // Determine if item is batch managed
                    if (GI_Line.ListOfBatchLineItems != null && GI_Line.ListOfBatchLineItems.Count > 0)
                    {
                        foreach (var batchLineItem in GI_Line.ListOfBatchLineItems)
                        {
                            goodsReceiptProductionOrder.Lines.BatchNumbers.BatchNumber  = batchLineItem.BatchNumber;
                            goodsReceiptProductionOrder.Lines.BatchNumbers.Quantity     = batchLineItem.Quantity;
                            if (batchLineItem.ManufacturerSerialNumber != null && batchLineItem.ManufacturerSerialNumber.Length > 0) goodsReceiptProductionOrder.Lines.BatchNumbers.ManufacturerSerialNumber = batchLineItem.ManufacturerSerialNumber;
                            if (batchLineItem.BatchNotes != null && batchLineItem.BatchNotes.Length > 0) goodsReceiptProductionOrder.Lines.BatchNumbers.Notes = batchLineItem.BatchNotes;


                            // If user defined fields were passed to the line item
                            if (batchLineItem.UserDefinedFields != null)
                            {
                                // Add each user defined field to the line item of the "Receipt from Production" document.
                                foreach (string key in batchLineItem.UserDefinedFields.Keys)
                                {
                                    goodsReceiptProductionOrder.Lines.BatchNumbers.UserFields.Fields.Item(key).Value = batchLineItem.UserDefinedFields[key].ToString();
                                }
                            }
                            goodsReceiptProductionOrder.Lines.BatchNumbers.Add();
                        }
                    }



                    if (GI_Line.SerialNumbers != null)
                    {
                        foreach (var serialNumber in GI_Line.SerialNumbers)
                        {
                            goodsReceiptProductionOrder.Lines.SerialNumbers.InternalSerialNumber = serialNumber;
                            goodsReceiptProductionOrder.Lines.SerialNumbers.Quantity             = 1;


                            // If user defined fields were passed to the line item
                            if (GI_Line.UserDefinedFields != null)
                            {
                                // Add each user defined field to the line item of the "Receipt from Production" document.
                                foreach (string key in GI_Line.UserDefinedFields.Keys)
                                {
                                    goodsReceiptProductionOrder.Lines.SerialNumbers.UserFields.Fields.Item(key).Value = GI_Line.UserDefinedFields[key].ToString();
                                }
                            }

                            goodsReceiptProductionOrder.Lines.SerialNumbers.Add();
                        }
                    }

                    // set the Branch
                    var br = NSC_DI.SAP.Branch.Get(goodsReceiptProductionOrder.Lines.WarehouseCode);
                    if (br > 0) goodsReceiptProductionOrder.BPL_IDAssignedToInvoice = br;

                    goodsReceiptProductionOrder.Lines.Add();
                }

                // Attempt to add the new item to the database
                int ResponseCode = goodsReceiptProductionOrder.Add();

                // Attempt to add the goods issued document
                if (ResponseCode != 0)
                {
                    throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }

                string newCode = null;

                // Get the newly created object's code
                Globals.oCompany.GetNewObjectCode(out newCode);

                if (newCode != null)
                {
                    NewlyCreatedKey = Convert.ToInt32(newCode);

                    // Someone on SAP Developer Network says you can pass the "202" object type, but you can NOT define the components item codes, the DI-API/SDK will automatically assign the item codes.
                    // TODO-SAP-DIAPI: Replace the SQL method with BoBCOM interactions

                    #region DELETE ME SOME DAY!

                    // Update through SQL 
                    string sql = @"
UPDATE
[IGN1]
SET
[BaseRef] = '" + productionOrderKey + @"'
, [BaseType] = '202'
, [BaseEntry] = '" + productionOrderKey + @"'
, [TreeType] = 'N'

WHERE [DocEntry] = '" + NewlyCreatedKey + @"'
";

                    UTIL.SQL.RunSQLQuery(sql);

                    #endregion
                }
                return NewlyCreatedKey;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oForm);
            }
        }
    }
}
