﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NSC_DI.SAP
{
    public static class Items 
    {
        /// <summary>
        /// Returns the item's name when given an item's code
        /// </summary>
        /// <param name="ItemCode">The item code you want the name for</param>
        /// <returns>Returns a string of the item's name</returns>
		public static string GetItemNameFromItemCode(string ItemCode)
        {
            // TODO-SAP-DIAPI: Replace the SQL method with BoBCOM interactions

            // Prepare the SQL statement to get the next availble plant ID
            var sql = "SELECT [ItemName] FROM [OITM] WHERE [OITM].[ItemCode] = '" + ItemCode + "'";

	        return UTIL.SQL.GetValue<string>(sql, "");
        }

        public static IEnumerable<string> GetItemNames(params string [] pItemPropertyNames)
        {
            var tipField = Property.GetFieldName("Template");

            var sb = new System.Text.StringBuilder();

            sb.Append($"SELECT REPLACE(ItemName, '~', '') AS Name FROM OITM WHERE {tipField} = 'Y'");

            // Match any of the specified properties

            if (pItemPropertyNames.Any())
            {
                sb.Append(" AND (");

                var ipFields = pItemPropertyNames.Select(n => Property.GetFieldName(n) + " = 'Y'");

                sb.Append(string.Join(" OR ", ipFields));

                sb.Append(")");
            }

            var qry = sb.ToString();

            var dt = UTIL.SQL.DataTable(qry);

            return dt.Select().Select(n => n["Name"] as string);
        }

        public static decimal GetPrice(string sItemCode, int iListnum)
        {
            // routine to get an Item price based on the Price List Number

            if (iListnum < 1) throw new ArgumentException("must be a valid list", "iListnum");

            if (string.IsNullOrEmpty(sItemCode)) throw new ArgumentNullException("sItemCode");

            try
            {
                var SQL = $"SELECT Price FROM ITM1 WHERE ItemCode = '{sItemCode}' AND PriceList ='{iListnum.ToString()}'";
                var sTmp = UTIL.SQL.GetValue<decimal>(SQL, decimal.MinusOne);
                if (sTmp == decimal.MinusOne) throw new Exception("Price list yielded no price");

                return sTmp;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
        }

        public static decimal GetPrice(string sItemCode)
        {
            var sql = $"select case When OITM.PricingPrc > 0 THen OITM.PricingPrc else OITM.LastPurPrc end as prc from OITM WHERE (OITM.PricingPrc > 0 OR OITM.LastPurPrc > 0) AND ItemCode = '{sItemCode}'";

            var sTmp = UTIL.SQL.GetValue<decimal>(sql, decimal.MinusOne);
            if (sTmp == decimal.MinusOne)
            { 
                sql = $"SELECT TOP 1 Price FROM ITM1 WHERE ItemCode = '{sItemCode}' AND Price Is Not null AndAlso Price > 0";

                sTmp = UTIL.SQL.GetValue<decimal>(sql, decimal.MinusOne);
                if (sTmp == decimal.MinusOne) throw new Exception("Price list yielded no price");
            }

            return sTmp;
        }

        public static string GetStrainID(string pItemCode)
        {
            var qry = $"SELECT U_{Globals.SAP_PartnerCode}_StrainID FROM OITM WHERE ItemCode = '{pItemCode}'";
            return UTIL.SQL.GetValue<string>(qry, String.Empty);
        }

        public static string GetStrainName(string pItemCode)
        {
            var qry = $"SELECT S.Name FROM OITM JOIN [@{NSC_DI.Globals.tStrains}] AS S on S.Code = U_{Globals.SAP_PartnerCode}_StrainID WHERE ItemCode = '{pItemCode}'";
            return UTIL.SQL.GetValue<string>(qry, String.Empty);
        }

		public static T GetField<T>(string pItemCode, string pField)
		{
			// routine to get an Item field
			if (string.IsNullOrEmpty(pField)) throw new Exception("Item Code not specified");

			try
			{
				var sql = "SELECT " + pField + " FROM OITM WHERE ItemCode ='" + pItemCode + "'";
				var rtn = UTIL.SQL.GetValue<string>(sql);
				if(string.IsNullOrEmpty(rtn)) throw new Exception("Item Code does not exist.");
				return (T)Convert.ChangeType(rtn, typeof(T));
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

        public static void CopyTemplate(string pTemplateItemCode, string pDestItemCode, string pDestItemName, string strainID)
        {
            SAPbobsCOM.Items oItems = null;
            try
            {
                //GetByKey(out oItems, pTemplateItemCode);
                oItems = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                var res = oItems.GetByKey(pTemplateItemCode);
                if (!res) throw new System.Collections.Generic.KeyNotFoundException(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oItems.IssueMethod = BillOfMaterials.IsBOM(oItems.ItemCode) ? SAPbobsCOM.BoIssueMethod.im_Manual : SAPbobsCOM.BoIssueMethod.im_Backflush;

                var xmlDoc = B1Object.ToXmlDoc(oItems);

                UTIL.Misc.KillObject(oItems);
                GC.Collect();

                foreach (System.Xml.XmlNode node in xmlDoc.SelectNodes("//ItemCode"))
                {
                    node.InnerText = pDestItemCode;
                }

                oItems = B1Object.FromXmlDoc(xmlDoc);

                // Add item attributes

                var qry = $"SELECT U_PurItem, U_InvItem, U_SalItem, U_ManageBatch, U_ManageSN FROM [@{Globals.tAutoItem}] WHERE U_ItemCode = '{pTemplateItemCode}'";

                var dt = NSC_DI.UTIL.SQL.DataTable(qry);

                if (dt.Rows.Count == 1)
                {
                    var dr = dt.Rows[0];

                    Func<string, SAPbobsCOM.BoYesNoEnum> YN = UTIL.Misc.ToSAPYesNo;

                    oItems.PurchaseItem =           YN(dr["U_PurItem"] as string);
                    oItems.InventoryItem =          YN(dr["U_InvItem"] as string);
                    oItems.SalesItem =              YN(dr["U_SalItem"] as string);
                    oItems.ManageBatchNumbers =     YN(dr["U_ManageBatch"] as string);
                    oItems.ManageSerialNumbers =    YN(dr["U_ManageSN"] as string);

                    if ((oItems.ManageBatchNumbers | oItems.ManageSerialNumbers) == SAPbobsCOM.BoYesNoEnum.tYES)
                        oItems.IssueMethod = SAPbobsCOM.BoIssueMethod.im_Manual;
                }

                Item.SetItemProperty(oItems, "Template", SAPbobsCOM.BoYesNoEnum.tNO);

                oItems.ItemName = pDestItemName;

                // Set item to Active
                oItems.Valid = SAPbobsCOM.BoYesNoEnum.tYES;
                oItems.Frozen = SAPbobsCOM.BoYesNoEnum.tNO;

                oItems.UserFields.Fields.Item($"U_{Globals.SAP_PartnerCode}_StrainID").Value = strainID;
                oItems.UserFields.Fields.Item($"U_{Globals.SAP_PartnerCode}_Template").Value = pTemplateItemCode;

                if (oItems.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(oItems);
                GC.Collect();
            }
        }

        public static bool HasKey(string pItemCode, bool pNoLock = true)
        {
            try
            {
                var nl = pNoLock ? "WITH (NOLOCK)" : string.Empty;
                var dt = UTIL.SQL.DataTable($"SELECT 1 FROM OITM {nl}WHERE ItemCode = '{pItemCode}'");
                return dt.Rows.Count == 1;
            }
            catch (Exception ex)
            {

                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static void GetByKey(out SAPbobsCOM.Items pItems, string pItemCode)
        {
            try
            {
                pItems = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                GetByKey(pItems, pItemCode);
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static void GetByKey(SAPbobsCOM.Items pItems, string pItemCode)
        {
            try
            {
                var res = pItems.GetByKey(pItemCode);
                if (!res) throw new System.Collections.Generic.KeyNotFoundException(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static bool IsPlant(string pItemCode)
        {
            return !String.IsNullOrEmpty(pItemCode) && GetStrainID(pItemCode) == pItemCode;
        }

        public static string ManagedBy(string pItemCode)
        {
            var sql = $"SELECT CASE WHEN OITM.ManSerNum = 'Y' THEN 'S' WHEN OITM.ManBtchNum = 'Y' THEN 'B' ELSE 'N' END FROM OITM WHERE ItemCode = '{pItemCode}'";
            return UTIL.SQL.GetValue<string>(sql, "N");
        }

        public static string ManagedBySB(string pID)
        {
            // get the Managed By using the Serial Number or Batch ID

            var ret = "N";
            var sB = Batches.GetItemCode(pID);
            var sS = SNs.GetItemCode(pID);
            if (sB != "" && sS == "") ret = "B";
            if (sB == "" && sS != "") ret = "S";
            return ret;
        }

        public static string GetItemCode(string pID)
        {
            // get the Item Code using the Serial Number or Batch ID

            var ret = "";
            var sB = Batches.GetItemCode(pID);
            var sS = SNs.GetItemCode(pID);
            if (sB != "" && sS == "") ret = sB;
            if (sB == "" && sS != "") ret = sS;
            return ret;
        }

        public class Batches
        {
            public static string GetStrainID(string pBatchID)
            {
                var qry = $"SELECT OITM.U_{Globals.SAP_PartnerCode}_StrainID FROM OITM JOIN OBTN ON OBTN.ItemCode = OITM.ItemCode WHERE OBTN.DistNumber = '{pBatchID}'";
                return UTIL.SQL.GetValue<string>(qry, String.Empty);
            }

            public static string GetItemCode(string pBatchID)
            {
                var qry = $"SELECT OITM.ItemCode FROM OITM JOIN OBTN ON OBTN.ItemCode = OITM.ItemCode WHERE OBTN.DistNumber = '{pBatchID}'";
                return UTIL.SQL.GetValue<string>(qry, String.Empty);
            }

            public static string GetNo(string pItemCode, string pWH = "", int pBinAbs = -1, double pQty =-1)
            {
                // get the Batch Number - BIN is not implemented (use OIBQ)

                var bin = "";
                if (NSC_DI.SAP.Bins.GetLevelCount(pWH) > 0)
                {
                    bin = NSC_DI.SAP.Bins.GetCodePull(pItemCode, pWH, pQty);
                }
                else
                {
                    var qry = "";
                    qry += $"SELECT TOP 1 OBTN.DistNumber FROM OBTQ INNER JOIN OBTN ON OBTQ.MdAbsEntry = OBTN.AbsEntry " + Environment.NewLine;
                    qry += $" WHERE OBTQ.ItemCode = '{pItemCode}' AND OBTQ.WhsCode = '{pWH}' AND OBTQ.Quantity >= {pQty.ToString()}";
                    bin = UTIL.SQL.GetValue<string>(qry, String.Empty);
                }

                return bin;
            }
        }

        public class SNs
        {
            public static String GetStrainID(string pSN)
            {
                var qry = $"SELECT OITM.U_{Globals.SAP_PartnerCode}_StrainID FROM OITM JOIN OSRN ON OSRN.ItemCode = OITM.ItemCode WHERE OSRN.DistNumber = '{pSN}'";
                return UTIL.SQL.GetValue<string>(qry, String.Empty);
            }
            public static String GetItemCode(string pSN)
            {
                var qry = $"SELECT ItemCode FROM  OSRN WHERE DistNumber = '{pSN}'";
                return UTIL.SQL.GetValue<string>(qry, String.Empty);
            }
            public static string GetSysNumber(string pSN)
            {
                var qry = $"SELECT SysNumber FROM OSRN WHERE DistNumber = '{pSN}'";
                return UTIL.SQL.GetValue<string>(qry, String.Empty);
            }
        }

        public static class Property
		{
			public static string GetFieldName(string pName)
			{

				// get the Item Property (QryGroup) field by Name

				if (string.IsNullOrEmpty(pName)) return "";

				try
				{
					// get the property number from the settings table
					var num = UTIL.Settings.Value.Get(Globals.SettingsItemPropPrefix + pName);
					if (string.IsNullOrEmpty(num)) throw new Exception(UTIL.Message.Format("Settings entry not found: " + Globals.SettingsItemPropPrefix + pName));

					return "QryGroup" + num;
				}
				catch (Exception ex)
				{
					throw new Exception(UTIL.Message.Format(ex));
				}
				finally
				{
					GC.Collect();
				}
			}

			public static string Get(string pItemCode, string pName)
			{

				// get the Item Property (QryGroup)  by Name

				if (string.IsNullOrEmpty(pItemCode) || string.IsNullOrEmpty(pName)) return "";

				try
				{
					// get the property number from the settings table
					var num = UTIL.Settings.Value.Get(Globals.SettingsItemPropPrefix + pName);
					if(string.IsNullOrEmpty(num)) throw new Exception(UTIL.Message.Format("Settings entry not found: " + Globals.SettingsItemPropPrefix + pName));

					return Get(pItemCode, Convert.ToInt32(num));
				}
				catch (Exception ex)
				{
					throw new Exception(UTIL.Message.Format(ex));
				}
				finally
				{
					GC.Collect();
				}
			}

			public static string Get(string pItemCode, int pProp)
			{

				// get the Item Property (QryGroup)  by number

				if (string.IsNullOrEmpty(pItemCode)) return "";
				if (pProp < 1 | pProp > 64) return "";

				try
				{
					var sql = "SELECT [QryGroup" + pProp.ToString() + "] FROM OITM WITH (NOLOCK) WHERE [ItemCode] ='" + UTIL.SQL.FixSQL(pItemCode) + "'";
					return UTIL.SQL.GetValue<string>(sql);
				}
				catch (Exception ex)
				{
					throw new Exception(UTIL.Message.Format(ex));
				}
				finally
				{
					GC.Collect();
				}
			}

			public static void Set(string pItem, int pProp, string pValue)
			{
				// set the Item Property (QryGroup)  

				if (pProp < 1 | pProp > 64) return;

				try
				{
					if (pValue.ToUpper() == "Y" || pValue.ToUpper() == "YES") Set(pItem, pProp, SAPbobsCOM.BoYesNoEnum.tYES);
					if (pValue.ToUpper() == "N" || pValue.ToUpper() == "NO" ) Set(pItem, pProp, SAPbobsCOM.BoYesNoEnum.tNO);
				}
				catch (Exception ex)
				{
					throw new Exception(UTIL.Message.Format(ex));
				}
				finally
				{
					//GC.Collect();
				}
			}

			public static void Set(string pItem, int pProp, SAPbobsCOM.BoYesNoEnum pValue)
			{
				// set the Item Property (QryGroup)  

				SAPbobsCOM.Items oITM = null;

				if (pProp < 1 | pProp > 64) return;

				try
				{
					if (oITM.GetByKey(pItem) == false) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

					// update 
					oITM.Properties[pProp] = pValue;
					if (oITM.Update() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
				}
				catch (Exception ex)
				{
					throw new Exception(UTIL.Message.Format(ex));
				}
				finally
				{
					UTIL.Misc.KillObject(oITM);
					GC.Collect();
				}
			}

			public static void Create(int pProp, string pName)
			{
				// set the Item Property (QryGroup)  

				SAPbobsCOM.ItemProperties oITG = null;

				if (pProp < 1 | pProp > 64) return;

				try
				{
					oITG = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItemProperties);

					if (oITG.GetByKey(pProp) == false) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                    // check to see if it equals defualt name
                    if (oITG.PropertyName.ToUpper() != $"ITEMS PROPERTY {pProp}")
                        return;

					// update 
					oITG.PropertyName = pName;
					if (oITG.Update() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
				}
				catch (Exception ex)
				{
					throw new Exception(UTIL.Message.Format(ex));
				}
				finally
				{
					UTIL.Misc.KillObject(oITG);
					GC.Collect();
				}
			}
            /// <summary>
            /// Checks to see if any of the item property names are enabled
            /// </summary>
            /// <param name="pItemCode">the item code</param>
            /// <param name="pNames">the item additional property names</param>
            /// <returns>true if any of the specified item property names are enabled</returns>
            public static bool AnyYes(string pItemCode, params string[] pNames)
            {
                if (string.IsNullOrEmpty(pItemCode?.Trim())) throw new ArgumentNullException("pItemCode");// Whitespace-Change NSC_DI.UTIL.Strings.Empty(pItemCode))

                if (pNames?.Length == 0) throw new ArgumentException("Parameter 'pNames' must have at least one value");

                foreach (var oName in pNames)
                {
                    var ynVal = Get(pItemCode, oName);
                    if (ynVal == "Y") return true;
                }

                return false;
            }
		}
	}
}