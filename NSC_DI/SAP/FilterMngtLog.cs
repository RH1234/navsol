﻿using System;

namespace NSC_DI.SAP
{
	public static class FilterMngtLog 
    {
		public static void CreateNewFilterLog(string UserName, string FilterItem, string Quantity, string ReplaceDate, string Warehouse)
        {
            // Prepare a connection to the user table
			SAPbobsCOM.UserTable sboTable = null;

            try
            {
				sboTable = Globals.oCompany.UserTables.Item(Globals.tFilterLog);
				//Setting Data to Master Data Table Fields  
                sboTable.Name = "";
                sboTable.UserFields.Fields.Item("U_ItemCode").Value  = FilterItem;
                sboTable.UserFields.Fields.Item("U_Quantity").Value  = Quantity;
                sboTable.UserFields.Fields.Item("U_ReDate").Value	 = DateTime.Parse(ReplaceDate);
                sboTable.UserFields.Fields.Item("U_Warehouse").Value = Warehouse;
                sboTable.UserFields.Fields.Item("U_CreatedOn").Value = DateTime.Now;
				sboTable.UserFields.Fields.Item("U_CreatedBy").Value = UserName;

                // Attempt to add the new item to the database
				if (sboTable.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(sboTable);
			}
		}
	}
}
