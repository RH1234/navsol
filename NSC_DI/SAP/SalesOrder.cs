﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace NSC_DI.SAP
{
	public class SalesOrder
	{
		public static double GetPickQty(dynamic pDocType, dynamic pDocEntry, dynamic pLineNum)
		{
			//if (pDocType.ToString.Trim() == "" || pDocEntry.ToString.Trim() == "") return;

			try
			{
                // get the allocation qty
                var sql = $"SELECT Quantity FROM B1_SnBOpenQtyViewAlloc WHERE DocType = {pDocType.ToString()} AND  DocEntry = {pDocEntry.ToString()} AND DocLine = {pLineNum.ToString()}";

                var qty = UTIL.SQL.GetValue<double>(sql, 0.0);
                return qty;
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}
	}

}
