﻿using System;
using System.Collections.Generic;
using SAPbobsCOM;
using System.Linq;

namespace NSC_DI.SAP
{
	public static class Strain 
    {
		public static void CreateStrain(string Code, string Name, string Type)
        {
            UserTable sboTable = null;
            try
            {
                sboTable = Globals.oCompany.UserTables.Item(Globals.tStrains);
                sboTable.Code = Code;
                sboTable.Name = Name;
                sboTable.UserFields.Fields.Item("U_Type").Value = Type;
                sboTable.UserFields.Fields.Item("U_CreateDate").Value = DateTime.Now.ToString();
                if (sboTable.Add() != 0)
                    throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
			}
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(sboTable);
            }
        }

        public static void AddBOMsToStrain(string strainID, string strainName)
        {
			var components = "";
            SAPbobsCOM.ProductTrees newBom = null;
            try
            {
				var dtAutoItemBOMs = UTIL.SQL.DataTable($"SELECT B.U_ParentItem, B.U_ItemCode, B.U_Quantity, B.U_IssueMethod,B.U_SourceWH FROM [@{Globals.tAutoItemBOM}] B JOIN [@{Globals.tAutoItem}] A ON B.U_ParentItem = A.U_ItemCode WHERE {UTIL.SQL.QueryClauses.MicroVertical} AND {UTIL.SQL.QueryClauses.WetCannabis}");
				//var dtAutoItemBOMs = UTIL.SQL.DataTable($"SELECT B.U_ParentItem, B.U_ItemCode, B.U_Quantity, B.U_IssueMethod FROM [@{Globals.tAutoItemBOM}] B");

				if (!dtAutoItemBOMs.Select().Any()) return; // no BOMs

                var itemNamesDict = NSC_DI.UTIL.AutoStrain.GetAutoItemsInfo(strainID, strainName).ToDictionary(n => n.AutoItemCode);

                foreach (var dr in dtAutoItemBOMs.Select())
                {
                    if (newBom == null || newBom.TreeCode != itemNamesDict[dr["U_ParentItem"] as string].ItemCode)
                    {
						components = "";
						if (newBom != null && !string.IsNullOrEmpty(newBom.TreeCode) && (newBom.Add() < 0)) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
						newBom = (SAPbobsCOM.ProductTrees)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductTrees);
						newBom.TreeCode = itemNamesDict[dr["U_ParentItem"] as string].ItemCode;
                        newBom.TreeType = BoItemTreeTypes.iProductionTree;
                    }
                    else newBom.Items.Add();
                    var autoItemCode = dr["U_ItemCode"] as string;

					components += autoItemCode + ", ";

					newBom.Items.ItemCode = autoItemCode.StartsWith("~") ? itemNamesDict[autoItemCode].ItemCode : autoItemCode;
                    newBom.Items.Quantity = dr["U_Quantity"] != null ? Convert.ToDouble(dr["U_Quantity"].ToString()) : 1;
                    newBom.Items.IssueMethod = string.Equals(dr["U_IssueMethod"] as string, "Manual", StringComparison.InvariantCultureIgnoreCase) ? BoIssueMethod.im_Manual : BoIssueMethod.im_Backflush;
                    newBom.Items.Warehouse = dr["U_SourceWH"] != null ? Convert.ToString(dr["U_SourceWH"].ToString()):"";
                }
            }
            catch (Exception ex)
            {
				throw new Exception(Environment.NewLine + $"Parent: {newBom.TreeCode}" + Environment.NewLine + $"Component: {components}" + Environment.NewLine + UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(newBom);
                GC.Collect();
            }
        }

        public static void AddItemsToStrain(string StrainID, string StrainName)
		{
            SAPbobsCOM.Items newItem = null;

            try
            {
                newItem = (SAPbobsCOM.Items)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);

                var itemNamesDict = NSC_DI.UTIL.AutoStrain.GetAutoItemsInfo(StrainID, StrainName).ToDictionary(n => n.AutoItemCode);

				var qry = $"SELECT * FROM [@{Globals.tAutoItem}] WHERE LEFT(U_ItemCode, 1) = '~' AND {UTIL.SQL.QueryClauses.MicroVertical} AND {UTIL.SQL.QueryClauses.WetCannabis}";
				//var qry = $"SELECT * FROM [@{Globals.tAutoItem}] WHERE LEFT(U_ItemCode, 1) = '~'";
                var dtAutoItems = UTIL.SQL.DataTable(qry);
                
                foreach (var line in dtAutoItems.Select())
                {
                    newItem = (SAPbobsCOM.Items)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                    newItem.ItemCode = itemNamesDict[line["U_ItemCode"] as string].ItemCode;
                    newItem.ItemName = itemNamesDict[line["U_ItemCode"] as string].ItemName;
                    newItem.ItemsGroupCode = Convert.ToInt32(line["U_ItemGroupCode"].ToString());

                    Func<string, SAPbobsCOM.BoYesNoEnum> YN = NSC_DI.UTIL.Misc.ToSAPYesNo;

                    newItem.ManageSerialNumbers = YN(line["U_ManageSN"].ToString());
					newItem.ManageBatchNumbers	= YN(line["U_ManageBatch"].ToString());
					newItem.InventoryItem		= YN(line["U_InvItem"].ToString());
					newItem.PurchaseItem		= YN(line["U_PurItem"].ToString());
					newItem.SalesItem	    	= YN(line["U_SalItem"].ToString());
                    newItem.AvgStdPrice         = Convert.ToDouble(line["U_UnitPrice"].ToString());
                    newItem.MaterialType = SAPbobsCOM.BoMaterialTypes.mt_FinishedGoods;
                    newItem.ManageStockByWarehouse = SAPbobsCOM.BoYesNoEnum.tYES;

                    SetUDF("StrainID", StrainID, newItem);
                    SetUDF("Sampleable", line, newItem);
                    SetUDF("Batchable", line, newItem);
                    SetUDF("Processable", line, newItem);
 
                    var propertyToSet = line["U_PropertyName"] as string;
                    if (!string.IsNullOrEmpty(propertyToSet)) Item.SetItemProperty(newItem, propertyToSet);

                    var DefaultWarehouse = line["U_DefaultWH"] as string;
                    if (!string.IsNullOrEmpty(DefaultWarehouse)) newItem.DefaultWarehouse = DefaultWarehouse;

                    if (newItem.Add() != 0) throw new Exception(Globals.oCompany.GetLastErrorDescription());
                }
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(newItem);
                GC.Collect();
			}
		}

        private static void SetUDF(string fieldName, System.Data.DataRow dr, SAPbobsCOM.Items newItem)
        {
            if (string.Equals(dr["U_" + fieldName] as string, "Y", StringComparison.InvariantCultureIgnoreCase))
            {
                SetUDF(fieldName, "Y", newItem);
            }
        }
        private static void SetUDF(string fieldName, string fieldValue, SAPbobsCOM.Items newItem)
        {
            newItem.UserFields.Fields.Item($"U_{Globals.SAP_PartnerCode}_{fieldName}").Value = fieldValue;
        }

        public class CSV_Record_Item
        {
            public string ItemCode { get; set; }
            public string ItemName { get; set; }
            public string ItemGroupCode { get; set; }
            public string ManageSerialNumbers { get; set; }
            public string ManageBatchNumbers { get; set; }
            public string HasBillOfMaterials { get; set; }
            public string InventoryItem { get; set; }
            public string PurchaseItem { get; set; }
            public string SalesItem { get; set; }
            public string UnitPrice { get; set; }
            public string DefaultWarehouse { get; set; }
            public string Sampleable { get; set; }
            public string Batchable { get; set; }
            public string Processable { get; set; }
        }

        public class CSV_Record_BillOfMaterial
        {
            public string ParentItemCode { get; set; }
            public string ItemCode { get; set; }
            public string Quantity { get; set; }
            public string IssueMethod { get; set; }
            public string SourceWarehouseCode { get; set; }
        }
    }
}
