﻿using System;
using System.Collections.Generic;
using SAPbobsCOM;
using System.Linq;

namespace NSC_DI.SAP
{
    public static class BatchItems
    {

        public static bool BatchExists(string pItemCode, string pBatchCode)
        {
            try
            {
                // check if batch exists.
                var sql = $"SELECT CASE WHEN EXISTS(SELECT AbsEntry FROM OBTN WHERE ItemCode = '{pItemCode}' AND DistNumber = '{pBatchCode}') THEN 1 ELSE 0 END";
                return NSC_DI.UTIL.SQL.GetValue<bool>(sql, false);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                GC.Collect();
            }
        }

        public static string Get_OBTN_AbsEntry()
        {
            // just posting code for now - 
            // uses Issue for Production
            var sql = $@"
SELECT OWOR.DocEntry, OWOR.DocNum, OITM.ItemName, OWOR.ItemCode, OWOR.PlannedQty, OWOR.CmpltQty, OWOR.CreateDate, OWOR.Warehouse, 0.0 AS WasteQty, 
       WOR1.wareHouse AS destinationWarehouse,
	   OBTN.U_NSC_MotherID AS [MotherID], OBTN.U_NSC_GroupNum AS [GroupNum], OBTN.MnfSerial AS [StateID], OBTN.DistNumber AS [BatchID]
  FROM OWOR
  JOIN OITM ON OITM.ItemCode   = OWOR.ItemCode
  JOIN OITB ON OITB.ItmsGrpCod = OITM.ItmsGrpCod
  JOIN WOR1 ON OWOR.DocEntry   = WOR1.DocEntry AND WOR1.BaseQty > 0
  JOIN IGE1 ON OWOR.DocNum     = IGE1.BaseRef
  JOIN OITL ON IGE1.DocEntry   = OITL.DocEntry AND OITL.ApplyType = 60
  JOIN ITL1 ON OITL.LogEntry   = ITL1.LogEntry
  JOIN OBTN ON OBTN.ItemCode   = ITL1.ItemCode AND OBTN.SysNumber = ITL1.SysNumber
 WHERE OWOR.Status = 'R' AND OITB.ItmsGrpNam IN('Dry Cannabis')
 ORDER BY CONVERT(int, OWOR.DocNum) DESC";

            // this is the main part
            //          JOIN OITL ON IGE1.DocEntry = OITL.DocEntry AND OITL.ApplyType = 60
            //JOIN ITL1 ON OITL.LogEntry = ITL1.LogEntry
            //JOIN OBTN ON OBTN.ItemCode = ITL1.ItemCode AND OBTN.SysNumber = ITL1.SysNumber

            return "";
        }

        //public static string GetNextAvailableBatchID(string ItemCode, string batchCode = "B")//, string pInterCoCode = "")
        //{
        //    return GetNextAvailableBatchID(ItemCode, 1, batchCode).Single(); //, pInterCoCode
        //}

        //public static IEnumerable<string> GetNextAvailableBatchID(string ItemCode, int quantity, string batchCode = "B", string pInterCoCode = "")
        //{
        //    //// can access in foreach or .ElementAt(i); and using System.Linq;
        //    //if (string.IsNullOrEmpty(pInterCoCode))
        //    //    pInterCoCode = NSC_DI.Globals.InterCoCode;
        //    //else
        //    //    pInterCoCode = (NSC_DI.Globals.InterCoCode == "") ? "" : pInterCoCode;
        //    //var prefix = NSC_DI.UTIL.Settings.concatBatchNum(pInterCoCode, "");
        //    IEnumerable<string> batches = UTIL.AutoStrain.NextBatch(ItemCode, batchCode, quantity, 0, pInterCoCode);
        //    //batches = ( IEnumerable)prefix + batches;
        //    return batches;
        //}

        //public static string GetNextAvailableBatchID(string ItemCode, string batchCode = "B", string pInterCoSubCode = "", int counter = 0)
        //{
        //    try
        //    {
        //        // var InterCo = NSC_DI.UTIL.SQL.GetValue<string>("select T0.U_Value from dbo.[@NSC_SETTINGS] T0 where T0.Name = 'Inter-Company'"); // DELETE 2020/12/30
        //        string newBatchNumber;
        //        //pInterCoSubCode = (string.IsNullOrEmpty(pInterCoSubCode)) ? NSC_DI.Globals.InterCoCode : pInterCoSubCode;
        //        //if (pInterCoSubCode != "" && NSC_DI.Globals.InterCoCode != "") // if globals interco is empty they are not using inter co
        //        //{
        //            newBatchNumber = NSC_DI.UTIL.AutoStrain.NextBatch(ItemCode, batchCode, 1, counter, pInterCoSubCode).First();
        //        //    newBatchNumber = NSC_DI.UTIL.Settings.concatBatchNum(pInterCoSubCode, newBatchNumber);
        //        //}
        //        //else
        //        //    newBatchNumber = GetNextAvailableBatchID(ItemCode, 1, batchCode).Single();

        //        return newBatchNumber;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(NSC_DI.UTIL.Message.Format(ex));
        //    }
        //    finally
        //    {
        //        //NSC_DI.UTIL.Misc.KillObject(ButtonCombo_Action);
        //        GC.Collect();
        //    }
        //}

        public static string NextBatch(string pItemCode, string pBatchCode = "B", string pInterCoCode = "")
        {
            try
            {
                return NextBatch(pItemCode, 1, pBatchCode, 0, pInterCoCode).First();
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                GC.Collect();
            }
        }

        public static IEnumerable<string> NextBatch(string pItemCode, int pQuantity, string pBatchCode = "B", int pCounter = 0, string pInterCoCode = "")
        {
            // Will get the Batch based off of the item master. Nearly identical to NextSN.
            try
            {
                // get the Inter Company code.
                // if NSC_DI.Globals.InterCoCode is empty, then not using Inter-Company
                if (string.IsNullOrEmpty(pInterCoCode))
                    pInterCoCode = NSC_DI.Globals.InterCoCode;
                else
                    pInterCoCode = (NSC_DI.Globals.InterCoCode == "") ? "" : pInterCoCode;

                var interCoCode = (pInterCoCode == "") ? "" : pInterCoCode + "-";

                var fmt = NSC_DI.UTIL.AutoStrain.BuildBatchFormat();
                // Counter deals with what happens if several items with the same item number are selected
                int lastNum = pCounter + GetLastNum(pItemCode, pBatchCode, pInterCoCode);
                return NSC_DI.UTIL.AutoStrain.Next(pItemCode, pQuantity, lastNum).Select(n => interCoCode + String.Format(fmt, pItemCode, n, pBatchCode));
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                GC.Collect();
            }
        }

        private static int GetLastNum(string pItemCode, string pPrefix = "B", string pInterCoCode = "")
        {
            try
            {
                string sql;
                if (pInterCoCode.Trim() != "")
                {
                    // for inter company
                    sql = $@"
SELECT TOP 1 DistNumber FROM OBTN 
 WHERE ItemCode = '{pItemCode}' AND DistNumber LIKE '{pInterCoCode}-{pItemCode}-{pPrefix}-%' AND DistNumber NOT LIKE '{pInterCoCode}-{pItemCode}-{pPrefix}-%-%'
 ORDER BY AbsEntry DESC";
                }
                else
                {
                    // for standard batches
                    sql = $@"
SELECT TOP 1 DistNumber FROM OBTN 
 WHERE ItemCode = '{pItemCode}' AND DistNumber LIKE '{pItemCode}-{pPrefix}-%' AND DistNumber NOT LIKE '{pItemCode}-{pPrefix}-%-%'
 ORDER BY AbsEntry DESC";
                }

                var id = UTIL.SQL.GetValue<string>(sql, null);
                if (id == null)
                {
                    // For opening balances
                    string OBsql = $@"
SELECT TOP 1 DistNumber FROM OBTN 
 WHERE ItemCode = '{pItemCode}' AND DistNumber LIKE 'OB-{pItemCode}-{pPrefix}-%'
 ORDER BY AbsEntry DESC";
                    id = UTIL.SQL.GetValue<string>(OBsql, null);

                    if (id == null)
                        return 0;
                }
                if (id.Length < 1) return 0;

                var indx = id.LastIndexOf("-") + 1;
                var val = id.Substring(indx);
                return int.Parse(val);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void ChangeQuarantineStatus(string batchNumber, Globals.QuarantineStates newItemQuarantineState)
        {
            // TODO-SAP-DIAPI: Replace the SQL method with BoBCOM interactions

            try
            {
                // Get DocEntry from ItemCode/SysNumber
                string sql = $"SELECT [AbsEntry] FROM [OBTN] WHERE [OBTN].DistNumber = '{batchNumber}'";

                int DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);

                // Attempt to get the DocEntry number from our SQL results
                if (DocEntry < 1) throw new Exception(UTIL.Message.Format($"Could not find DocEntry for Batch: {batchNumber}"));

                BatchNumberDetailsService batchDetailsService = (Globals.oCompany.GetCompanyService()).GetBusinessService(ServiceTypes.BatchNumberDetailsService) as BatchNumberDetailsService;

                // Prepare the parameters for the serial number details
                BatchNumberDetailParams batchNumberDetailParams = batchDetailsService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams) as BatchNumberDetailParams;

                // Set the DocEntry to the number we found through SQL
                batchNumberDetailParams.DocEntry = DocEntry;

                // Locate the record we are attempting to edit
                BatchNumberDetail batchNumberDetail = batchDetailsService.Get(batchNumberDetailParams);

                // Set the new state
                batchNumberDetail.UserFields.Item("U_" + Globals.SAP_PartnerCode + "_QuarState").Value = newItemQuarantineState.ToString();

                // Set our Quarantine Timer.
                string timeStamp = NSC_DI.UTIL.Dates.ToTimeStamp(DateTime.Now).ToString();
                batchNumberDetail.UserFields.Item("U_" + Globals.SAP_PartnerCode + "_TimerQuar").Value = timeStamp;

                // Attempt to update the serialized items information
                batchDetailsService.Update(batchNumberDetail);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(ButtonCombo_Action);
                GC.Collect();
            }
        }

        public static void UpdateQATestResults(string pItemCode, string pBatchID, string pStatus, string pNewTestID = "")
        {
            // Get DocEntry from ItemCode/SysNumber
            string sql = $@"SELECT [AbsEntry] FROM [OBTN] WHERE DistNumber = '{pBatchID}'" + (string.IsNullOrEmpty(pItemCode?.Trim()) ? "" : $" AND ItemCode = '{pItemCode}'");// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pItemCode)

            int DocEntry = 0;

            try
            {
                DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(sql, -1);
                if (DocEntry < -1) throw new Exception(UTIL.Message.Format("Could not find DocEntry for item."));

                BatchNumberDetailsService batchDetailsService =
                   (Globals.oCompany.GetCompanyService()).GetBusinessService(ServiceTypes.BatchNumberDetailsService) as BatchNumberDetailsService;

                // Prepare the parameters for the serial number details
                BatchNumberDetailParams batchNumberDetailParams =
                    batchDetailsService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams) as
                        BatchNumberDetailParams;

                // Set the DocEntry to the number we found through SQL
                batchNumberDetailParams.DocEntry = DocEntry;

                // Locate the record we are attempting to edit
                BatchNumberDetail batchNumberDetail = batchDetailsService.Get(batchNumberDetailParams);

                // Set the new state -- We only care about the Integer Value.
                batchNumberDetail.UserFields.Item("U_" + Globals.SAP_PartnerCode + "_PassedQA").Value = pStatus;

                // update the Test ID, if there is one
                if(NSC_DI.UTIL.Strings.Empty(pNewTestID) == false) batchNumberDetail.UserFields.Item("U_" + Globals.SAP_PartnerCode + "_LabTestID").Value = pNewTestID;


                // Attempt to update the serialized items information
                batchDetailsService.Update(batchNumberDetail);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void UpdateUDF(string pItemCode, string pBatchID, string udfName, dynamic udfValue)
        {
            // Get DocEntry from ItemCode/SysNumber
            string sql = $@"SELECT [AbsEntry] FROM [OBTN] WHERE DistNumber = '{pBatchID}'" + (string.IsNullOrEmpty(pItemCode?.Trim()) ? "" : $" AND ItemCode = '{pItemCode}'");// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pItemCode)

            int DocEntry = 0;

            try
            {
                DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(sql, -1);
                if (DocEntry < -1) throw new Exception(UTIL.Message.Format("Could not find Batch."));

                BatchNumberDetailsService batchDetailsService =
                   (Globals.oCompany.GetCompanyService()).GetBusinessService(ServiceTypes.BatchNumberDetailsService) as BatchNumberDetailsService;

                // Prepare the parameters for the batch number details
                BatchNumberDetailParams batchNumberDetailParams =
                    batchDetailsService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams) as
                        BatchNumberDetailParams;

                // Set the DocEntry to the number we found through SQL
                batchNumberDetailParams.DocEntry = DocEntry;

                // Locate the record we are attempting to edit
                BatchNumberDetail batchNumberDetail = batchDetailsService.Get(batchNumberDetailParams);

                // Set the new UDF
                batchNumberDetail.UserFields.Item("U_" + Globals.SAP_PartnerCode + "_" + udfName).Value = udfValue;

                // Attempt to update the serialized items information
                batchDetailsService.Update(batchNumberDetail);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void UpdateUDF(string pBatchID, string udfName, string udfValue)
        {
            // Get DocEntry from ItemCode/SysNumber
            string SQL_Query_GetDocEntryFromItemCodeAndSysNum = string.Format(@"
SELECT
[AbsEntry]
FROM [OBTN]
WHERE [OBTN].DistNumber = '{0}'", pBatchID);


            int DocEntry = 0;
            try
            {

                // Attempt to get the DocEntry number from our SQL results
                if (Int32.TryParse(UTIL.SQL.GetFieldsFromSQLQuery(SQL_Query_GetDocEntryFromItemCodeAndSysNum).Item(0).Value.ToString(), out DocEntry))
                {
                    BatchNumberDetailsService batchDetailsService = (Globals.oCompany.GetCompanyService()).GetBusinessService(ServiceTypes.BatchNumberDetailsService) as BatchNumberDetailsService;

                    // Prepare the parameters for the serial number details
                    BatchNumberDetailParams batchNumberDetailParams = batchDetailsService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams) as BatchNumberDetailParams;

                    // Set the DocEntry to the number we found through SQL
                    batchNumberDetailParams.DocEntry = DocEntry;

                    // Locate the record we are attempting to edit
                    BatchNumberDetail batchNumberDetail = batchDetailsService.Get(batchNumberDetailParams);

                    // Set the new state
                    batchNumberDetail.UserFields.Item("U_" + Globals.SAP_PartnerCode + "_" + udfName).Value = udfValue;


                    // Attempt to update the serialized items information
                    batchDetailsService.Update(batchNumberDetail);

                    return;
                }

                throw new Exception(UTIL.Message.Format("Could not find DocEntry for item."));
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void CopyStateID(string newBatchNumber, string batchNumber, string itemCode)
        {
            try
            {
                string strSql = $"SELECT MnfSerial FROM OBTN WHERE DistNumber = '{batchNumber}' AND ItemCode = '{itemCode}'";
                string stateId = UTIL.SQL.GetValue<string>(strSql);
                SetStateID(newBatchNumber, itemCode, stateId);

            }
            catch (Exception ex)
            {

                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static void SetStateID(string batchNumber, string itemCode = null, string value = "")
        {
            SAPbobsCOM.BatchNumberDetail oBatchDetails = null;
            CompanyService oCoService = null;
            BatchNumberDetailsService oBatchService = null;
            BatchNumberDetailParams oParams = null;
            
            try
            { 
                string strSQL = $"SELECT AbsEntry FROM OBTN WHERE DistNumber = '{batchNumber}'";
                if (string.IsNullOrEmpty(itemCode?.Trim()) == false) strSQL += $" AND ItemCode = '{itemCode}'";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(itemCode)

                int DocEntry = UTIL.SQL.GetValue<int>(strSQL);

                oCoService = Globals.oCompany.GetCompanyService() as CompanyService;
                oBatchService = oCoService.GetBusinessService(ServiceTypes.BatchNumberDetailsService);
                oParams = oBatchService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);

                oParams.DocEntry = DocEntry;
                oBatchDetails = oBatchService.Get(oParams);
                oBatchDetails.BatchAttribute1 = value;
                oBatchService.Update(oBatchDetails);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(oBatchDetails);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oParams);
            }

        }

        public static void setAttribute1(string pBatchNumber, string pItemCode = null, string pValue = "")
        {
            SAPbobsCOM.BatchNumberDetail oBatchDetails = null;
            CompanyService oCoService = null;
            BatchNumberDetailsService oBatchService = null;
            BatchNumberDetailParams oParams = null;

            try
            {
                string strSQL = $"SELECT AbsEntry FROM OBTN WHERE DistNumber = '{pBatchNumber}'";
                if (string.IsNullOrEmpty(pItemCode?.Trim()) == false) strSQL += $" AND ItemCode = '{pItemCode}'";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pItemCode)

                int DocEntry = UTIL.SQL.GetValue<int>(strSQL);

                oCoService = Globals.oCompany.GetCompanyService() as CompanyService;
                oBatchService = oCoService.GetBusinessService(ServiceTypes.BatchNumberDetailsService);
                oParams = oBatchService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);

                oParams.DocEntry = DocEntry;
                oBatchDetails = oBatchService.Get(oParams);
                oBatchDetails.BatchAttribute1 = pValue;
                oBatchService.Update(oBatchDetails);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(oBatchDetails);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oParams);
            }
        }

        public static void setAttribute2(string pBatchNumber, string pItemCode = null, string pValue = "")
        {
            SAPbobsCOM.BatchNumberDetail oBatchDetails = null;
            CompanyService oCoService = null;
            BatchNumberDetailsService oBatchService = null;
            BatchNumberDetailParams oParams = null;

            try
            {
                string strSQL = $"SELECT AbsEntry FROM OBTN WHERE DistNumber = '{pBatchNumber}'";
                if (string.IsNullOrEmpty(pItemCode.Trim()) == false) strSQL += $" AND ItemCode = '{pItemCode}'";// Whitespace-Change

                int DocEntry = UTIL.SQL.GetValue<int>(strSQL);

                oCoService = Globals.oCompany.GetCompanyService() as CompanyService;
                oBatchService = oCoService.GetBusinessService(ServiceTypes.BatchNumberDetailsService);
                oParams = oBatchService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);

                oParams.DocEntry = DocEntry;
                oBatchDetails = oBatchService.Get(oParams);
                oBatchDetails.BatchAttribute2 = pValue;
                oBatchService.Update(oBatchDetails);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(oBatchDetails);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oParams);
            }
        }

        public static void SetCropStageID(string batchNumber, string itemCode, string value)
        {
            SAPbobsCOM.BatchNumberDetail oBatchDetails = null;
            CompanyService oCoService = null;
            BatchNumberDetailsService oBatchService = null;
            BatchNumberDetailParams oParams = null;

            try
            {
                string strSQL = $"SELECT AbsEntry FROM OBTN WHERE DistNumber = '{batchNumber}' AND ItemCode = '{itemCode}'";

                int DocEntry = UTIL.SQL.GetValue<int>(strSQL);

                oCoService = Globals.oCompany.GetCompanyService() as CompanyService;
                oBatchService = oCoService.GetBusinessService(ServiceTypes.BatchNumberDetailsService);
                oParams = oBatchService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);

                oParams.DocEntry = DocEntry;
                oBatchDetails = oBatchService.Get(oParams);
                oBatchDetails.UserFields.Item("U_NSC_CropStageID").Value = value;
                oBatchService.Update(oBatchDetails);

            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(oBatchDetails);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oCoService);
                UTIL.Misc.KillObject(oParams);
            }

        }

        public static void CreateStockTransfer(StockTransferCreate StockTransfer)
        {
            // Prepare an SAP Business One Stock Transfer
			StockTransfer inventoryTransfer = Globals.oCompany.GetBusinessObject(BoObjectTypes.oStockTransfer) as StockTransfer;
            // add try catch
            // Assign all needed variables for a stock transfer
            inventoryTransfer.DocDate = DateTime.Now;
            inventoryTransfer.TaxDate = DateTime.Now;
            inventoryTransfer.FromWarehouse = StockTransfer.FromWarehouse;
            inventoryTransfer.ToWarehouse = StockTransfer.FromWarehouse;
            inventoryTransfer.Comments = StockTransfer.Comment;

            foreach (StockTransferLine line in StockTransfer.StockTransferLines)
            {
                if (line.DestinationWarehouseCode != StockTransfer.FromWarehouse)   // if both Whs are the same, can't transfer
                {
                    if (inventoryTransfer.Lines.ItemCode != "") inventoryTransfer.Lines.Add();

                    inventoryTransfer.Lines.ItemCode = line.ItemCode;
                    inventoryTransfer.Lines.WarehouseCode = line.DestinationWarehouseCode;
                    inventoryTransfer.Lines.Quantity = line.Quantity;
                    NSC_DI.SAP.Document.SetDefaultDistributionRules(inventoryTransfer.Lines);

                    if (!string.IsNullOrEmpty(line.BatchNumber))
                    {
                        inventoryTransfer.Lines.BatchNumbers.BatchNumber = line.BatchNumber;
                        inventoryTransfer.Lines.BatchNumbers.Quantity = line.Quantity;
                        inventoryTransfer.Lines.BatchNumbers.Add();
                    }
                    else
                    {
                        inventoryTransfer.Lines.SerialNumbers.SystemSerialNumber = line.SysNumber;
                        inventoryTransfer.Lines.SerialNumbers.Add();
                    }
                }
            }

            if (inventoryTransfer.Lines.ItemCode == "") return; // make sure that there is something to transfer.
            if (inventoryTransfer.Add() != 0) B1Exception.RaiseException();
        }

        public static SAPbobsCOM.BatchNumberDetail GetInfoFromDoc(SAPbobsCOM.BoObjectTypes pDocType, dynamic pDocEntry, int pDocLine = 0)
        {
            // retrurn the batch number object based on a Document

            try
            {
                //if (pDocEntry.GetType() is string && string.IsNullOrWhiteSpace(pDocEntry)) return null;
                var DocEntry = pDocEntry.ToString();
                if (string.IsNullOrEmpty(DocEntry?.Trim()) || DocEntry == "0") return null;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(DocEntry)

                string sql = $"SELECT BatchNum FROM IBT1 WHERE BaseType = {(int)pDocType} AND BaseEntry = {DocEntry} AND BaseLinNum = {pDocLine}";    // -- ItemCode ='CL-I-1'
                var BatchNum = NSC_DI.UTIL.SQL.GetValue<string>(sql);

                return GetInfo(BatchNum);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static string GetBatchNumFromDoc(string pDocType, dynamic pDocEntry, int pDocLine = 0)
        {
            // retrurn the batch number object based on a Document

            try
            {
                //if (pDocEntry.GetType() is string && string.IsNullOrWhiteSpace(pDocEntry)) return null;
                var DocEntry = pDocEntry.ToString();
                if (string.IsNullOrEmpty(DocEntry?.Trim()) || DocEntry == "0") return null;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(DocEntry)

                string sql = $"SELECT BatchNum FROM IBT1 WHERE BaseType = {pDocType} AND BaseEntry = {DocEntry} AND BaseLinNum = {pDocLine}";    // -- ItemCode ='CL-I-1'
                string BatchNum = NSC_DI.UTIL.SQL.GetValue<string>(sql);

                return BatchNum;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
        public static SAPbobsCOM.BatchNumberDetail GetInfoFromDocBs(SAPbobsCOM.BoObjectTypes pBsDocType, dynamic pBsDocEntry, int pBsDocLine = 0)
        {
            // retrurn the batch number object based on a Base Document (BsDoc fields)

            try
            {
                //if (pDocEntry.GetType() is string && string.IsNullOrWhiteSpace(pDocEntry)) return null;
                var DocEntry = pBsDocEntry.ToString();
                if (string.IsNullOrEmpty(DocEntry?.Trim()) || DocEntry == "0") return null;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(DocEntry)

                string sql = $"SELECT BatchNum FROM IBT1 WHERE BaseType = {pBsDocType.ToString()} AND BaseEntry = {DocEntry} AND BaseLinNum = {pBsDocLine}";   //  -- ItemCode ='CL-I-1'
                var BatchNum = NSC_DI.UTIL.SQL.GetValue<string>(sql);

                return GetInfo(BatchNum);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
        public static void SetWetWeight(string pPlantID, double pWetWeight)
        {
            try
            {
                string qry = $"UPDATE OBTN SET U_NSC_WetWgt = {pWetWeight} WHERE DistNumber = '{pPlantID}'";
                UTIL.SQL.RunSQLQuery(qry);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static void SetHarvestInfo(string[] pPlantIDs, int pHarvestPdO, string pHarvestDate)
        {
            try
            {
                string PlantsToUpdate = "(";
                for (int i = 0; i < pPlantIDs.Length; i++)
                {
                    if (i == 0)
                    {
                        PlantsToUpdate += "'" + pPlantIDs[i].ToString() + "'";
                    }
                    else
                    {
                        PlantsToUpdate += ",'" + pPlantIDs[i].ToString() + "'";
                    }
                }
                PlantsToUpdate += ")";
                string qry = $"UPDATE OBTN SET U_NSC_HarvestPdO = {pHarvestPdO}, U_NSC_HarvestDate = '{pHarvestDate}' WHERE DistNumber  in {PlantsToUpdate} ";
                UTIL.SQL.RunSQLQuery(qry);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }

        public static SAPbobsCOM.BatchNumberDetail GetInfo(string pDistNumber, string pItemCode = null)
        {
            // retrurn the batch number object 

            if (pDistNumber.Trim() == "") return null;

            SAPbobsCOM.CompanyService            oCoService     = null;
            SAPbobsCOM.BatchNumberDetailParams   oParams        = null;
            SAPbobsCOM.BatchNumberDetailsService oBatchService  = null;

            try
            {
                oCoService      = Globals.oCompany.GetCompanyService() as SAPbobsCOM.CompanyService;
                oBatchService   = oCoService.GetBusinessService(SAPbobsCOM.ServiceTypes.BatchNumberDetailsService);
                oParams         = oBatchService.GetDataInterface(SAPbobsCOM.BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);

                string strSQL = $"SELECT AbsEntry FROM [OBTN] WHERE DistNumber='{pDistNumber}'";
                if (string.IsNullOrEmpty(pItemCode?.Trim()) == false) strSQL += $" AND ItemCode = '{pItemCode}'";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pItemCode)

                oParams.DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL, 0);
                
                return oBatchService.Get(oParams);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oCoService);
                NSC_DI.UTIL.Misc.KillObject(oParams);
                //NSC_DI.UTIL.Misc.KillObject(oBatchService);
                GC.Collect();
            }
        }

        public static void SetUDF(string pItemCode, string pBatchID, string pField, dynamic pValue)
        {
            if (string.IsNullOrEmpty(pItemCode?.Trim()) || string.IsNullOrEmpty(pBatchID?.Trim()) || string.IsNullOrEmpty(pField?.Trim())) return;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pItemCode) || NSC_DI.UTIL.Strings.Empty(pBatchID) || NSC_DI.UTIL.Strings.Empty(pField))

            SAPbobsCOM.CompanyService            oCoService     = null;
            SAPbobsCOM.BatchNumberDetailParams   oParams        = null;
            SAPbobsCOM.BatchNumberDetailsService oBatchService  = null;

            try
            {
                var oFrom = NSC_DI.SAP.BatchItems.GetInfo(pBatchID);

                oCoService = Globals.oCompany.GetCompanyService() as SAPbobsCOM.CompanyService;
                oBatchService = oCoService.GetBusinessService(SAPbobsCOM.ServiceTypes.BatchNumberDetailsService);
                oParams = oBatchService.GetDataInterface(SAPbobsCOM.BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);
                string strSQL = $"SELECT AbsEntry FROM [OBTN] WHERE ItemCode = '{pItemCode}' AND DistNumber = '{pBatchID}'";
                oParams.DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL, 0);
                if (oParams.DocEntry < 1) return;

                var oBatch = oBatchService.Get(oParams);
                oBatch.UserFields.Item(pField).Value = pValue;
                oBatchService.Update(oBatch);
                NSC_DI.UTIL.Misc.KillObject(oCoService);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oCoService);
                NSC_DI.UTIL.Misc.KillObject(oParams);
                NSC_DI.UTIL.Misc.KillObject(oBatchService);
                GC.Collect();
            }
        }

        public static void CopyUDFs(string pToBatchID, string pFromBatchID, string[] pSkip = null)
        {
            // copy the UDFs from object to object

            // USAGE:                         
            // NSC_DI.UTIL.UDO.CopyUDFs(idT, idF, new string[1] { "U_NSC_StateID" });

            if (string.IsNullOrEmpty(pToBatchID?.Trim()) || string.IsNullOrEmpty(pFromBatchID?.Trim())) return;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pToBatchID) || NSC_DI.UTIL.Strings.Empty(pFromBatchID))

            SAPbobsCOM.CompanyService            oCoService     = null;
            SAPbobsCOM.BatchNumberDetailParams   oParams        = null;
            SAPbobsCOM.BatchNumberDetailsService oBatchService  = null;

            try
            {
                var oFrom = NSC_DI.SAP.BatchItems.GetInfo(pFromBatchID);

                oCoService       = Globals.oCompany.GetCompanyService() as SAPbobsCOM.CompanyService;
                oBatchService    = oCoService.GetBusinessService(SAPbobsCOM.ServiceTypes.BatchNumberDetailsService);
                oParams          = oBatchService.GetDataInterface(SAPbobsCOM.BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);
                string strSQL    = $"SELECT AbsEntry FROM [OBTN] WHERE DistNumber='{pToBatchID}'";
                oParams.DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL, 0);
                if (oParams.DocEntry < 1) return;

                var oTo          = oBatchService.Get(oParams);

                for (int j = 0; j < oFrom.UserFields.Count; j++)
                {
                    try
                    {
                        var s = oFrom.UserFields.Item(j).Name;
                        if (pSkip == null || Array.IndexOf(pSkip, s) < 0) oTo.UserFields.Item(s).Value = oFrom.UserFields.Item(j).Value;
                    }
                    catch { }
                }
                
                oBatchService.Update(oTo);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oCoService);
                NSC_DI.UTIL.Misc.KillObject(oParams);
                NSC_DI.UTIL.Misc.KillObject(oBatchService);
                GC.Collect();
            }
        }

        public static void UpdateUDFsFromObj(string pToBatchID, dynamic pFrom, string[] pSkip = null)
        {
            // copy the UDFs from an object to a specified Batch ID

            // USAGE:                         
            // NSC_DI.UTIL.UDO.CopyUDFs(oIss.Lines.BatchNumbers, BatchNumber, new string[1] { "U_NSC_StateID" });

            if (pToBatchID.Trim() == "" || pFrom == null) return;

            SAPbobsCOM.CompanyService            oCoService     = null;
            SAPbobsCOM.BatchNumberDetailParams   oParams        = null;
            SAPbobsCOM.BatchNumberDetailsService oBatchService  = null;

            try
            {
                oCoService       = Globals.oCompany.GetCompanyService() as SAPbobsCOM.CompanyService;
                oBatchService    = oCoService.GetBusinessService(SAPbobsCOM.ServiceTypes.BatchNumberDetailsService);
                oParams          = oBatchService.GetDataInterface(SAPbobsCOM.BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);
                string strSQL    = $"SELECT AbsEntry FROM [OBTN] WHERE DistNumber='{pToBatchID}'";
                oParams.DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL, 0);
                if (oParams.DocEntry == 0) return;
                var oTo          = oBatchService.Get(oParams);

                for (int j = 0; j < pFrom.UserFields.Count; j++)
                {
                    try
                    {
                        var s = pFrom.UserFields.Item(j).Name;
                        if (pSkip == null || Array.IndexOf(pSkip, s) < 0)
                        {
                            var ValueHolder =  pFrom.UserFields.Item(j).Value;
                            if (s != "U_NSC_HarvestDate")
                            {
                                oTo.UserFields.Item(s).Value = ValueHolder;
                            }
                            else
                            {
                                //Adjust string for date
                                DateTime HarvestDate = Convert.ToDateTime(ValueHolder);
                                oTo.UserFields.Item(s).Value = HarvestDate;
                            }
                        }
                    }
                    catch { }
                }

                oBatchService.Update(oTo);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oCoService);
                NSC_DI.UTIL.Misc.KillObject(oParams);
                NSC_DI.UTIL.Misc.KillObject(oBatchService);
                GC.Collect();
            }
        }

        public static void UpdateUDFsToObj(dynamic pTo, string pFromBatchID, string[] pSkip = null, bool pUpdate = true)
        {
            // copy the UDFs from a specified Batch ID to object - NOT YET FUNCTIONAL ***********************************************

            // USAGE:                         
            // NSC_DI.UTIL.UDO.CopyUDFs(oIss.Lines.BatchNumbers, BatchNumber, new string[1] { "U_NSC_StateID" });

            if (string.IsNullOrEmpty(pFromBatchID?.Trim()) || pTo == null) return;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pFromBatchID)

            SAPbobsCOM.CompanyService            oCoService     = null;
            SAPbobsCOM.BatchNumberDetailParams   oParams        = null;
            SAPbobsCOM.BatchNumberDetailsService oBatchService  = null;

            try
            {
                oCoService       = Globals.oCompany.GetCompanyService() as SAPbobsCOM.CompanyService;
                oBatchService    = oCoService.GetBusinessService(SAPbobsCOM.ServiceTypes.BatchNumberDetailsService);
                oParams          = oBatchService.GetDataInterface(SAPbobsCOM.BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);
                string strSQL    = $"SELECT AbsEntry FROM [OBTN] WHERE DistNumber='{pFromBatchID}'";
                oParams.DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);
                var oFrom = oBatchService.Get(oParams);

                for (int j = 0; j < oFrom.UserFields.Count; j++)
                {
                    try
                    {
                        var s = oFrom.UserFields.Item(j).Name;
                        if (pSkip == null || Array.IndexOf(pSkip, s) < 0) pTo.UserFields.Item(s).Value = oFrom.UserFields.Item(s).Value;
                    }
                    catch { }
                }

                if(pUpdate) oBatchService.Update(pTo);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oCoService);
                NSC_DI.UTIL.Misc.KillObject(oParams);
                NSC_DI.UTIL.Misc.KillObject(oBatchService);
                GC.Collect();
            }
        }

        public class StockTransferCreate
        {
			public DateTime DocDate { get; set; }
            public DateTime TaxDate { get; set; }
            public string FromWarehouse { get; set; }
            public string Comment { get; set; }
            public List<StockTransferLine> StockTransferLines { get; set; }

            public StockTransferCreate(string SourceWarehouse, string Comment)
            {
                DocDate = DateTime.Now;
                TaxDate = DateTime.Now;
                FromWarehouse = SourceWarehouse;
                this.Comment = Comment;
                StockTransferLines = new List<StockTransferLine>();
            }
        }

        public class StockTransferLine
        {
            public string ItemCode { get; set; }
            public string DestinationWarehouseCode { get; set; }
            public double Quantity { get; set; }

            public int	  SysNumber { get; set; }
            public string BatchNumber { get; set; }
        }

		public class SubBatch
		{
            public static void Create(List<BatchRec> pItemList, string pIssueTypeCode = null)
            {
                // create the sub batches using a goods issue and receipt

                if (pItemList.Count < 1) return;
                SAPbobsCOM.Documents oDoc = null;

                try
                {
                    var commit = !Globals.oCompany.InTransaction;
                    if (commit && NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                    //----------------------------
                    //----------------------------
                    // create a Goods Issue
                    oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
                    oDoc.DocDate = DateTime.Now;
                    oDoc.TaxDate = DateTime.Now;

                    // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                    var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                    if (getEmpID != null)
                        oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID; // 10062

                    if (pIssueTypeCode != null) oDoc.UserFields.Fields.Item("U_NSC_ComplianceType").Value = pIssueTypeCode;

                    foreach (var rec in pItemList)
                    {
                        // 10823 - 4 - set the doc branch if this is the 1st time through loop.
                        if(oDoc.Lines.ItemCode == "")
                        {
                            var branch = NSC_DI.UTIL.SQL.GetValue<int>($"Select BPLId FROM OWHS WHERE WHSCODE = '{rec.FromWarehouse}'", -1);
                            if (branch >= 0) oDoc.BPL_IDAssignedToInvoice = branch;
                        }

                        oDoc.Lines.ItemCode      = rec.Item;
                        oDoc.Lines.Quantity      = rec.Quantity;
                        oDoc.Lines.WarehouseCode = rec.FromWarehouse;
                        //oDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_NSC_StateID {sqlWhere}", null);

                        if(rec.OldBatch != "")
                        {
                            oDoc.Lines.BatchNumbers.BatchNumber = rec.OldBatch.ToString();
                            oDoc.Lines.BatchNumbers.Quantity    = rec.Quantity;
                        }

                        if (rec.Bin != 0)
                        {
                            //oDoc.Lines.BinAllocations.BaseLineNumber              = pPL.Lines.BinAllocations.BaseLineNumber;    // this causes an error (if the PL is modified or something)
                            oDoc.Lines.BinAllocations.BinAbsEntry                   = rec.Bin;
                            oDoc.Lines.BinAllocations.Quantity                      = rec.Quantity;
                            oDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = 0;
                        }
                    }

                    NSC_DI.SAP.Document.Add(oDoc);

                    //----------------------------
                    //----------------------------
                    // create the Goods Receipt
                    oDoc = null;
                    oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);
                    oDoc.DocDate = DateTime.Now;
                    oDoc.TaxDate = DateTime.Now;

                    // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                    if (getEmpID != null)
                        oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID; //10062

                    int line = -1;

                    foreach (var rec in pItemList)
                    {
                        // 10823 - 4 - set the doc branch if this is the 1st time through loop.
                        if (oDoc.Lines.ItemCode == "")
                        {
                            var branch = NSC_DI.UTIL.SQL.GetValue<int>($"Select BPLId FROM OWHS WHERE WHSCODE = '{rec.FromWarehouse}'", -1);
                            if (branch >= 0) oDoc.BPL_IDAssignedToInvoice = branch;
                        }

                        line++;
                        if (oDoc.Lines.ItemCode != "") oDoc.Lines.Add();

                        oDoc.Lines.ItemCode = rec.Item;
                        oDoc.Lines.Quantity = rec.Quantity;
                        oDoc.Lines.WarehouseCode = rec.ToWarehouse;
                        if (pIssueTypeCode != null) oDoc.UserFields.Fields.Item("U_NSC_ComplianceType").Value = pIssueTypeCode;
                        //oDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value = oOriginalDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value;

                        if (rec.OldBatch != "")
                        {
                            oDoc.Lines.BatchNumbers.BatchNumber = NSC_DI.UTIL.AutoStrain.NextSubBatch(rec.Item, rec.OldBatch.ToString());
                            oDoc.Lines.BatchNumbers.Quantity    = rec.Quantity;
                            //oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = oOriginalDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value;
                            oDoc.Lines.BatchNumbers.Location    = rec.OldBatch.ToString();    // put the source batch
                            oDoc.Lines.BatchNumbers.Notes       = rec.OldBatch.ToString();    // put the source batch
                            

                            var oFrom = NSC_DI.SAP.BatchItems.GetInfo(oDoc.Lines.BatchNumbers.Location);
                            NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.BatchNumbers, oFrom, new string[1] { "U_NSC_StateID" });
                        }
                        //  Set attr 1 here
                        if (string.IsNullOrEmpty(rec.MnfSerial?.Trim()))// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(rec.MnfSerial))
                            oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT MnfSerial FROM OBTN WHERE DistNumber = '{rec.OldBatch}'");
                        else
                            oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = rec.MnfSerial;

                        //check to see if whse is destruction and set the whse flag here.
                        string whsType = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_NSC_WhrsType FROM OWHS WHERE WhsCode = '{rec.ToWarehouse}'");
                        if(whsType == "QND")
                            oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_QuarState").Value = "DEST";
                        if(whsType == "QNX")
                            oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_QuarState").Value = "SICK";

                        if (rec.Bin != 0)
                        {
                            oDoc.Lines.BinAllocations.BaseLineNumber                = line;
                            oDoc.Lines.BinAllocations.BinAbsEntry                   = rec.Bin;
                            oDoc.Lines.BinAllocations.Quantity                      = rec.Quantity;
                            oDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = 0;
                        }
                    }

                    NSC_DI.SAP.Document.Add(oDoc);

                    if (commit && Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                }
                catch (Exception ex)
                {
                    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }
                finally
                {
                    NSC_DI.UTIL.Misc.KillObject(oDoc);
                    GC.Collect();
                }
            }

            public static void Create(SAPbobsCOM.Documents oOriginalDoc, System.Data.DataTable pdtPLs = null, string pIssueTypeCode = null)
			{
                // create the sub batches using a goods issue and receipt

                if (oOriginalDoc.Lines.Count < 1) return;
				SAPbobsCOM.Documents oDoc = null;
                SAPbobsCOM.PickLists oPL = null;

                var baseDocRow = -1;

                try
                {
                    var commit = !Globals.oCompany.InTransaction;
                    if (commit && NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                    //----------------------------
                    //----------------------------
                    // create a Goods Issue
                    oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
                    oDoc.DocDate = DateTime.Now;
                    oDoc.TaxDate = DateTime.Now;

                    // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                    var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                    if (getEmpID != null) oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;
                    
                    if (pIssueTypeCode != null) oDoc.UserFields.Fields.Item("U_NSC_ComplianceType").Value = pIssueTypeCode;

                    // possible for the allocation to occur in the SO, but a PL is still created
                    oPL = Globals.oCompany.GetBusinessObject(BoObjectTypes.oPickLists);
                    var lastPlId = 0;
                    for (var i = 0; i < oOriginalDoc.Lines.Count; i++)
                    {
                        oOriginalDoc.Lines.SetCurrentLine(i);

                        // ------  BRANCH
                        // 10823 - 4 - set the doc branch if this is the 1st time through loop.
                        if (i == 0)
                        {
                            var branch = NSC_DI.SAP.Branch.Get(oOriginalDoc.Lines.WarehouseCode);
                            if (branch >= 0) oDoc.BPL_IDAssignedToInvoice = branch;
                        }

                        // get the corrisponding PL entry and see if there is a batch specified
                        //                        var sql = $@"
                        //SELECT AbsEntry
                        //  FROM RDR1 
                        // INNER JOIN PKL1 ON RDR1.DocEntry = PKL1.OrderEntry AND RDR1.LineNum = PKL1.OrderLine AND PKL1.BaseObject = 17 
                        // INNER JOIN OITM ON RDR1.ItemCode = OITM.ItemCode AND OITM.ManBtchNum = 'Y'
                        // WHERE RDR1.DocEntry = {oOriginalDoc.DocEntry.ToString()} AND RDR1.LineNum = {oOriginalDoc.Lines.LineNum.ToString()} AND PKL1.PickStatus = 'Y'";    // canceled delivery change
                        //  //WHERE RDR1.DocEntry = {oOriginalDoc.DocEntry.ToString()} AND RDR1.LineNum = {oOriginalDoc.Lines.LineNum.ToString()}";
                        var sql = $@"
SELECT RDR1.PickIdNo
  FROM RDR1 
 INNER JOIN OITM ON RDR1.ItemCode = OITM.ItemCode AND OITM.ManBtchNum = 'Y'
 WHERE RDR1.DocEntry = {oOriginalDoc.DocEntry.ToString()} AND RDR1.LineNum = {oOriginalDoc.Lines.LineNum.ToString()} AND RDR1.PickStatus = 'Y'";

                        var plId = NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);
                        if (lastPlId != plId && plId != 0 && oPL.GetByKey(plId) == false) throw new Exception(Globals.oCompany.GetLastErrorDescription());
                        lastPlId = plId;

                        // get the PL line num
                        if (plId > 0)
                        {
                            // items can be deleted from the SO/PL.
                            // the "PickEntry" column is not the actual line number.
                            // need to get the count starting from zero.
                            // used <= and not < so that can check for a no entry if needed in the future.
                            sql = $"SELECT COUNT(PickEntry) FROM PKL1 WHERE AbsEntry = {plId.ToString()} AND OrderLine <= {oOriginalDoc.Lines.LineNum.ToString()}";
                            var plLine = NSC_DI.UTIL.SQL.GetValue<int>(sql, null) - 1;  
                            oPL.Lines.SetCurrentLine(plLine);
                            if (oPL.Lines.BatchNumbers.BatchNumber == "") plId = 0;     // no batch ID, so use the SO
                        }

                        if (plId == 0)
                        {       // USE THE DOCUMENT AS THE SOURCE
                            baseDocRow = oOriginalDoc.Lines.LineNum;

                            if (oDoc.Lines.ItemCode != "") oDoc.Lines.Add();

                            oDoc.Lines.ItemCode      = oOriginalDoc.Lines.ItemCode;
                            oDoc.Lines.Quantity      = oOriginalDoc.Lines.Quantity;
                            oDoc.Lines.LineTotal     = oOriginalDoc.Lines.LineTotal;
                            oDoc.Lines.WarehouseCode = oOriginalDoc.Lines.WarehouseCode;
                            oDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value = oOriginalDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value;

                            for (var j = 0; j < oOriginalDoc.Lines.BatchNumbers.Count; j++)
                            {
                                oOriginalDoc.Lines.BatchNumbers.SetCurrentLine(j);
                                if (j > 0) oDoc.Lines.BatchNumbers.Add();

                                //oDoc.Lines.BatchNumbers.InternalSerialNumber = oOriginalDoc.Lines.BatchNumbers.InternalSerialNumber;
                                oDoc.Lines.BatchNumbers.BatchNumber              = oOriginalDoc.Lines.BatchNumbers.BatchNumber;
                                oDoc.Lines.BatchNumbers.Quantity                 = oOriginalDoc.Lines.BatchNumbers.Quantity;
                                oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = oOriginalDoc.Lines.BatchNumbers.ManufacturerSerialNumber;
                                oDoc.Lines.BatchNumbers.Location                 = oOriginalDoc.Lines.BatchNumbers.Location;
                                oDoc.Lines.BatchNumbers.Notes                    = oOriginalDoc.Lines.BatchNumbers.Notes;

                                // set the bins - even if the batches were set in the SO, the bins are always in the PL.
                                if (pdtPLs != null)
                                {
                                    for (int x = 0; x < pdtPLs.Rows.Count; x++) // We will want to DRY this set of code out and set it into a new class with properties and methods EB 6/16/21
                                    {
                                        if (int.Parse(pdtPLs.Rows[x]["OrderLine"].ToString()) != baseDocRow)
                                            continue;
                                        if (oDoc.Lines.BinAllocations.BinAbsEntry != 0) oDoc.Lines.BinAllocations.Add();
                                        oDoc.Lines.BinAllocations.BinAbsEntry = int.Parse(pdtPLs.Rows[x]["BinAbs"].ToString());
                                        oDoc.Lines.BinAllocations.Quantity = double.Parse(pdtPLs.Rows[x]["PickQtty"].ToString());
                                        oDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = j;
                                    }
                                    //System.Data.DataRow[] dtRow = pdtPLs.Select("OrderLine = " + baseDocRow.ToString());
                                    //foreach (var dr in dtRow)
                                    //{
                                    //    if (oDoc.Lines.BinAllocations.BinAbsEntry != 0) oDoc.Lines.BinAllocations.Add();
                                    //    oDoc.Lines.BinAllocations.BinAbsEntry = int.Parse(dr["BinAbs"].ToString());
                                    //    oDoc.Lines.BinAllocations.Quantity = double.Parse(dr["PickQtty"].ToString());
                                    //    oDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = j;
                                    //}
                                }
                            }

                            // THERE ARE NO BIN ALLOCATIONS ON THE SO
                            //for (var j = 0; j < oDoc.Lines.BinAllocations.Count; j++)
                            //{
                            //    oOriginalDoc.Lines.BinAllocations.SetCurrentLine(j);
                            //    if (j > 0) oDoc.Lines.Add();

                            //    oDoc.Lines.BinAllocations.BaseLineNumber                = oOriginalDoc.Lines.BinAllocations.BaseLineNumber;
                            //    oDoc.Lines.BinAllocations.BinAbsEntry                   = oOriginalDoc.Lines.BinAllocations.BinAbsEntry;
                            //    oDoc.Lines.BinAllocations.Quantity                      = oOriginalDoc.Lines.BinAllocations.Quantity;
                            //    oDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = oOriginalDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine;
                            //}

                        }
                        else    // USE PICK LIST AS THE SOURCE
                        {
                            baseDocRow = oPL.Lines.LineNumber;

                            if (oDoc.Lines.ItemCode != "") oDoc.Lines.Add();

                            var sqlWhere = $" FROM RDR1 WHERE DocEntry = {oOriginalDoc.DocEntry.ToString()} AND LineNum = {oPL.Lines.OrderRowID.ToString()}";
                            oDoc.Lines.ItemCode = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT ItemCode" + sqlWhere, null);
                            oDoc.Lines.Quantity = oPL.Lines.PickedQuantity;
                            //oDoc.Lines.LineTotal									 = NSC_DI.UTIL.SQL.GetValue<double>($"SELECT LineTotal {sqlWhere}", null);
                            oDoc.Lines.WarehouseCode = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT WhsCode {sqlWhere}", null);
                            oDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_NSC_StateID {sqlWhere}", null);

                            for (var j = 0; j < oPL.Lines.BatchNumbers.Count; j++)
                            {
                                oPL.Lines.BatchNumbers.SetCurrentLine(j);
                                if (j > 0) oDoc.Lines.BatchNumbers.Add();

                                oDoc.Lines.BatchNumbers.BatchNumber              = oPL.Lines.BatchNumbers.BatchNumber;
                                oDoc.Lines.BatchNumbers.Quantity                 = oPL.Lines.BatchNumbers.Quantity;
                                oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = oPL.Lines.BatchNumbers.ManufacturerSerialNumber;
                                oDoc.Lines.BatchNumbers.Location                 = oPL.Lines.BatchNumbers.Location;
                                oDoc.Lines.BatchNumbers.Notes                    = oPL.Lines.BatchNumbers.Notes;

                                // set the bins - even if the batches were set in the SO, the bins are always in the PL.
                                if (pdtPLs != null)// We will want to DRY this set of code out and set it into a new class with properties and methods EB 6/16/21
                                {
                                    for (int x = 0; x < pdtPLs.Rows.Count; x++)
                                    {
                                        if (int.Parse(pdtPLs.Rows[x]["OrderLine"].ToString()) != baseDocRow)
                                            continue;
                                        if (oDoc.Lines.BinAllocations.BinAbsEntry != 0) oDoc.Lines.BinAllocations.Add();
                                        oDoc.Lines.BinAllocations.BinAbsEntry = int.Parse(pdtPLs.Rows[x]["BinAbs"].ToString());
                                        oDoc.Lines.BinAllocations.Quantity = double.Parse(pdtPLs.Rows[x]["PickQtty"].ToString());
                                        oDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = j;
                                    }
                                }
                            }
                        }
                    }

                    NSC_DI.SAP.Document.Add(oDoc);

                    //----------------------------
                    //----------------------------
                    // create the Goods Receipt
                    oDoc = null;
					oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);
					oDoc.DocDate = DateTime.Now;
					oDoc.TaxDate = DateTime.Now;

                    // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                    //var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                    if (getEmpID != null)
                        oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;
                   
                    if (pIssueTypeCode != null) oDoc.UserFields.Fields.Item("U_NSC_ComplianceType").Value = pIssueTypeCode;

                    for (var i = 0; i < oOriginalDoc.Lines.Count; i++)
                    {
                        oOriginalDoc.Lines.SetCurrentLine(i);
                        if (oDoc.Lines.ItemCode != "") oDoc.Lines.Add();

                        // ------  BRANCH
                        // 10823 - 4 - set the doc branch if this is the 1st time through loop.
                        if (i == 0)
                        {
                            var branch = NSC_DI.SAP.Branch.Get(oOriginalDoc.Lines.WarehouseCode);
                            if (branch >= 0) oDoc.BPL_IDAssignedToInvoice = branch;
                        }

                        baseDocRow = oOriginalDoc.Lines.LineNum;

                        oDoc.Lines.ItemCode										 = oOriginalDoc.Lines.ItemCode;
						oDoc.Lines.Quantity										 = oOriginalDoc.Lines.Quantity;
						//oDoc.Lines.LineTotal									 = oOriginalDoc.Lines.LineTotal;
						oDoc.Lines.WarehouseCode								 = oOriginalDoc.Lines.WarehouseCode;
                        oDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value = oOriginalDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value;

                        for (var j = 0; j < oOriginalDoc.Lines.BatchNumbers.Count; j++)
                        {
                            oOriginalDoc.Lines.BatchNumbers.SetCurrentLine(j);
                            if (oOriginalDoc.Lines.BatchNumbers.BatchNumber == "") continue;    // skip non-batched items
                            if (oDoc.Lines.BatchNumbers.BatchNumber != "") oDoc.Lines.BatchNumbers.Add();

                            oDoc.Lines.BatchNumbers.BatchNumber              = NSC_DI.UTIL.AutoStrain.NextSubBatch(oOriginalDoc.Lines.ItemCode, oOriginalDoc.Lines.BatchNumbers.BatchNumber);
                            oDoc.Lines.BatchNumbers.Quantity                 = oOriginalDoc.Lines.BatchNumbers.Quantity;
                            oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = oOriginalDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value;
                            oDoc.Lines.BatchNumbers.Location                 = oOriginalDoc.Lines.BatchNumbers.BatchNumber; // put the source batch
                            oDoc.Lines.BatchNumbers.Notes                    = oOriginalDoc.Lines.BatchNumbers.BatchNumber; // put the source batch

                            // save the new sub batch info into the original document
                            oOriginalDoc.Lines.BatchNumbers.ManufacturerSerialNumber = oDoc.Lines.BatchNumbers.ManufacturerSerialNumber;
                            oOriginalDoc.Lines.BatchNumbers.BatchNumber              = oDoc.Lines.BatchNumbers.BatchNumber;
                            oOriginalDoc.Lines.BatchNumbers.Location                 = oDoc.Lines.BatchNumbers.Location;
                            oOriginalDoc.Lines.BatchNumbers.Notes                    = oDoc.Lines.BatchNumbers.Notes;

                            // set the bins - even if the batches were set in the SO, the bins are always in the PL.
                            if (pdtPLs != null)
                            {
                                for (int x = 0; x < pdtPLs.Rows.Count; x++)// We will want to DRY this set of code out and set it into a new class with properties and methods EB 6/16/21
                                {
                                    if (int.Parse(pdtPLs.Rows[x]["OrderLine"].ToString()) != baseDocRow)
                                        continue;
                                    if (oDoc.Lines.BinAllocations.BinAbsEntry != 0) oDoc.Lines.BinAllocations.Add();
                                    oDoc.Lines.BinAllocations.BinAbsEntry = int.Parse(pdtPLs.Rows[x]["BinAbs"].ToString());
                                    oDoc.Lines.BinAllocations.Quantity = double.Parse(pdtPLs.Rows[x]["PickQtty"].ToString());
                                    oDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = j;
                                }
                                //System.Data.DataRow[] dtRow = pdtPLs.Select("OrderLine = " + baseDocRow.ToString());
                                //foreach (var dr in dtRow)
                                //{
                                //    if (oDoc.Lines.BinAllocations.BinAbsEntry != 0) oDoc.Lines.BinAllocations.Add();
                                //    oDoc.Lines.BinAllocations.BinAbsEntry                   = int.Parse(dr["BinAbs"].ToString());
                                //    oDoc.Lines.BinAllocations.Quantity                      = double.Parse(dr["PickQtty"].ToString());
                                //    oDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = j;
                                //}
                            }

                            var oFrom = NSC_DI.SAP.BatchItems.GetInfo(oDoc.Lines.BatchNumbers.Location);
                            NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.BatchNumbers, oFrom, new string[1] { "U_NSC_StateID" });
                        }

                        //// set the bins
                        //if (pdtPLs != null)
                        //{
                        //    System.Data.DataRow[] dtRow = pdtPLs.Select("OrderLine = " + baseDocRow.ToString());
                        //    foreach (var dr in dtRow)
                        //    {
                        //        if (oDoc.Lines.BinAllocations.BinAbsEntry != 0) oDoc.Lines.BinAllocations.Add();
                        //        oDoc.Lines.BinAllocations.BinAbsEntry                   = int.Parse(dr["BinAbs"].ToString());
                        //        oDoc.Lines.BinAllocations.Quantity                      = double.Parse(dr["PickQtty"].ToString());
                        //        oDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = int.Parse(dr["Pkl2LinNum"].ToString());
                        //    }
                        //}
                    }

                    //foreach (System.Data.DataRow row in pDT.Rows)
                    //{
                    //	if (oDoc.Lines.ItemCode != "") oDoc.Lines.Add();

                    //	oDoc.Lines.ItemCode										 = row["ItemCode"].ToString();
                    //	oDoc.Lines.Quantity										 = Convert.ToDouble(row["Qty"]);
                    //	oDoc.Lines.LineTotal									 = Convert.ToDouble(row["RowTot"]);
                    //	oDoc.Lines.WarehouseCode								 = row["WH"].ToString();
                    //	oDoc.Lines.BatchNumbers.Quantity						 = oDoc.Lines.Quantity;
                    //	oDoc.Lines.BatchNumbers.BatchNumber						 = NSC_DI.UTIL.AutoStrain.NextSubBatch(row["ItemCode"].ToString(), row["Batch"].ToString());
                    //	oDoc.Lines.BatchNumbers.Location						 = row["Batch"].ToString();     // put the source batch 
                    //	oDoc.Lines.BatchNumbers.Notes							 = row["Batch"].ToString();     // put the source batch 
                    //	oDoc.Lines.BatchNumbers.ManufacturerSerialNumber		 = row["StateID"].ToString();
                    //	oDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value = row["StateID"].ToString();

                    //  var oFrom = NSC_DI.SAP.BatchItems.GetInfo(oDoc.Lines.BatchNumbers.Location);
                    //  NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.BatchNumbers, oFrom, new string[1] { "U_NSC_StateID" });

                    //  // update the Data Table with the new Batch Number
                    //  row["SubBatch"] = oDoc.Lines.BatchNumbers.BatchNumber;
                    //}

                    var xm = oDoc.GetAsXML();
                    NSC_DI.SAP.Document.Add(oDoc);

					if (commit && Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
				}
				catch (Exception ex)
				{
					if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
					throw new Exception(NSC_DI.UTIL.Message.Format(ex));
				}
				finally
				{
                    NSC_DI.UTIL.Misc.KillObject(oPL);
                    NSC_DI.UTIL.Misc.KillObject(oDoc);
                    GC.Collect();
				}
			}

            public static void DeAllocate(SAPbobsCOM.Documents pDoc)
            {
                // replace the batches with nothing in a document 
                // this is only applicable for Sales Orders.

                if (pDoc.Lines.Count < 1) return;

                try
                {
                    for (var i = 0; i < pDoc.Lines.Count; i++)
                    {
                        pDoc.Lines.SetCurrentLine(i);

                        for (var j = 0; j < pDoc.Lines.BatchNumbers.Count; j++)
                        {
                            pDoc.Lines.BatchNumbers.SetCurrentLine(j);
                            pDoc.Lines.BatchNumbers.Quantity = 0d;
                            //pDoc.Lines.BatchNumbers.BatchNumber              = "";
                            //pDoc.Lines.BatchNumbers.ManufacturerSerialNumber = "";
                            //pDoc.Lines.BatchNumbers.Location                 = "";
                        }

                        for (var j = 0; j < pDoc.Lines.BinAllocations.Count; j++)
                        {
                            pDoc.Lines.BinAllocations.SetCurrentLine(j);
                            pDoc.Lines.BinAllocations.Quantity = 0;
                        }
                    }

                    NSC_DI.SAP.Document.Update(pDoc);

                }
                catch (Exception ex)
                {
                    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }
                finally
                {
                    GC.Collect();
                }
            }

            public static void Allocate(SAPbobsCOM.Documents pDoc, System.Data.DataTable pDT)
			{
				// replace the batches with the sub batches in a document
				if (pDT.Rows.Count < 1) return;

				try
				{
					foreach (System.Data.DataRow row in pDT.Rows)
					{
						pDoc.Lines.SetCurrentLine(Convert.ToInt32(row["LineNum"]) - 1);

						pDoc.Lines.BatchNumbers.Quantity				 = Convert.ToDouble(row["Qty"]);
						pDoc.Lines.BatchNumbers.BatchNumber				 = row["SubBatch"].ToString();
						pDoc.Lines.BatchNumbers.ManufacturerSerialNumber = row["StateID"].ToString();
						pDoc.Lines.BatchNumbers.Location				 = row["Batch"].ToString();
					}
				}
				catch (Exception ex)
				{
					throw new Exception(NSC_DI.UTIL.Message.Format(ex));
				}
				finally
				{
					GC.Collect();
				}
			}

			public static System.Data.DataTable DT_Load(SAPbobsCOM.Documents pDoc, out string pError)
			{
				// load the sub batche table from a document

				pError = "";

				try
				{
                    // OP 8356. don't use the "scan", use compliance type
                    //var IsScan = (NSC_DI.UTIL.Settings.Value.Get("State Plant ID Source") == "SCAN");
                    var IsScan = (NSC_DI.UTIL.Settings.Version.GetCompliance() == "METRC");
                    var hasErrs = false;
					var errLines = "You must have Batch Numbers";
					if (IsScan)
						errLines += " and State IDs";
					errLines += " for the following rows." + Environment.NewLine;
					errLines += "Line    Item";

					var dt = DT_Define();

					// Get each Item on the order
					for (int i = 0; i < pDoc.Lines.Count; i++)
					{
						pDoc.Lines.SetCurrentLine(i);
						if (pDoc.Lines.ItemCode == "") continue;

						// item has to be batch managed
						if (NSC_DI.SAP.Items.GetField<string>(pDoc.Lines.ItemCode, "ManBtchNum") != "Y") continue;

                        var line    = pDoc.Lines.LineNum + 1;  // pDoc.Lines.VisualOrder + 1;  // changed to line num
						var batch	= pDoc.Lines.BatchNumbers.BatchNumber;
						var stateID = pDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value;
                        double qty = NSC_DI.SAP.SalesOrder.GetPickQty(pDoc.DocObjectCodeEx, pDoc.DocEntry, pDoc.Lines.LineNum);

                        dt.Rows.Add(new object[] { line, pDoc.Lines.ItemCode, qty, pDoc.Lines.WarehouseCode, pDoc.Lines.LineTotal, stateID, batch, "", pDoc.DocEntry.ToString(), pDoc.DocObjectCodeEx });

						// check for no batches and no State IDs, if IDs are SCAN
						if (batch == "" || (IsScan && stateID == ""))
						{
							hasErrs = true;
							errLines += Environment.NewLine + line.ToString() + "        " + pDoc.Lines.ItemCode;
						}
					}

					pError = (hasErrs) ? errLines : "";
					return dt;
				}
				catch (Exception ex)
				{
					throw new Exception(NSC_DI.UTIL.Message.Format(ex));
				}
				finally
				{
					GC.Collect();
				}
			}

			public static System.Data.DataTable DT_Define()
			{
				// create the sub batches Data Table

				try
				{
					var dt = new System.Data.DataTable();

					dt.Columns.Add("LineNum", typeof(int));
					dt.Columns.Add("ItemCode");
					dt.Columns.Add("Qty", typeof(double));
					dt.Columns.Add("WH");
					dt.Columns.Add("RowTot", typeof(double));
					dt.Columns.Add("StateID");
					dt.Columns.Add("Batch");
					dt.Columns.Add("SubBatch");
                    dt.Columns.Add("DocEntry");
                    dt.Columns.Add("DocType");

                    return dt;
				}
				catch (Exception ex)
				{
					throw new Exception(NSC_DI.UTIL.Message.Format(ex));
				}
				finally
				{
					GC.Collect();
				}
			}

            public class BatchRec
            {
                public string Item { get; set; }
                public string FromWarehouse { get; set; }
                public string ToWarehouse { get; set; }
                public int Bin { get; set; }
                public string OldBatch { get; set; }
                public string NewBatch { get; set; }
                public double Quantity { get; set; }
                public string MnfSerial { get; set; }

            }
        }
    }
}
