﻿using System;

namespace NSC_DI.SAP
{
    /// <summary>
    /// A "Water Log" is a line item entry keeping track of the water applied to a plant, who watered it, how much, and when the plant was watered.
    /// The controller class will contain functions and variables relevant to creating and maintaing Water Log entries.
    /// </summary>
    public class WaterLog 
    {
	    /// <summary>
	    /// Adds some water to a plant.  This will create a "Plant Journal" entry, a "Water Log", and consume water from the inventory of SAP Business One via a "Goods Issued" document.
	    /// </summary>
	    /// <param name="PlantID">The unique identifier of the plant we will apply the water to</param>
	    /// <param name="AmountOfWaterApplied">The amount of water we are applying.</param>
	    /// <param name="UnitOfMeasurement">The unit of measurement for the water being applied to the plant.</param>
	    public static void AddWaterLog(string PlantID, double AmountOfWaterApplied, Globals.LiquidMeasurements UnitOfMeasurement, string PPM, string PHLevel,
            string Dionized)
	    {
		    // Grab the next available code from the SQL database
			string NextCode = UTIL.SQL.GetNextAvailableCodeForItem(Globals.tWaterLog, "Code");

		    SAPbobsCOM.UserTable sboTable = null;

		    try
		    {

                // Prepare a connection to the user table
                //****Calling the wrong table we are watering here and not doing a treatment
                //sboTable = Globals.oCompany.UserTables.Item(Globals.tTreatLog);
                sboTable = Globals.oCompany.UserTables.Item(Globals.tWaterLog);

                // Set the values for the newly created item
                //****The code field is set to auto increment so I commented it out
                //sboTable.Code = NextCode;
                sboTable.Name = NextCode;
			    sboTable.UserFields.Fields.Item("U_PlantID").Value = PlantID;
			    sboTable.UserFields.Fields.Item("U_DateCreated").Value = DateTime.Now;
			    sboTable.UserFields.Fields.Item("U_Amount").Value = AmountOfWaterApplied;
			    sboTable.UserFields.Fields.Item("U_Measure").Value = UnitOfMeasurement.ToString();
			    sboTable.UserFields.Fields.Item("U_PPM").Value = PPM;
			    sboTable.UserFields.Fields.Item("U_PHLEVEL").Value = PHLevel;
			    sboTable.UserFields.Fields.Item("U_DIONIZED").Value = Dionized;
                sboTable.UserFields.Fields.Item("U_GUID").Value = "WATER";

                // Attempt to add the new item to the database
                if (sboTable.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

			    // Add a plant journal entry
			    PlantJournal.AddPlantJournalEntry(PlantID, Globals.JournalEntryType.Water, "Watered " + AmountOfWaterApplied + " " + UnitOfMeasurement);
		    }
		    catch (Exception ex)
		    {
			    throw new Exception(UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    UTIL.Misc.KillObject(sboTable);
			    GC.Collect();
		    }
	    }

	    /// <summary>
        /// Returns the number of milliliters given a liquid amount.
        /// </summary>
        /// <param name="Measurement">The type of liquid measurment</param>
        /// <param name="Amount">The amount of the type of liquid measurement</param>
        /// <returns>Returns a decimal number of milliliters in a given liquid amount</returns>
        public static double GetMillilitersFromAmountOfLiquid(Globals.LiquidMeasurements Measurement, double Amount)
        {
            // Convert to milliliters from various liquid measurements
            switch (Measurement)
            {
                case Globals.LiquidMeasurements.gal:
                    return Amount * 3785.41;

                case Globals.LiquidMeasurements.liters:
                    return Amount * 1000.00;

                case Globals.LiquidMeasurements.qt:
                    return Amount * 946.353;

                case Globals.LiquidMeasurements.pt:
                    return Amount * 473.176;

                case Globals.LiquidMeasurements.cups:
                    return Amount * 236.588;

                case Globals.LiquidMeasurements.ml:
                    return Amount;

                case Globals.LiquidMeasurements.floz:
                    return Amount * 29.5735;
            }

            return Amount;
        }

        /// <summary>
        /// Returns a specified amount given milliliter amount of water.
        /// </summary>
        /// <param name="Measurement">The liquid measurement type to return</param>
        /// <param name="MilliliterAmount">The number of milliliters of water</param>
        /// <returns>Returns a decimal number of the specific liquid measurement type given milliliters of water.</returns>
        public static double GetAmountFromMilliliters(Globals.LiquidMeasurements Measurement, double MilliliterAmount)
        {
            // Convert to milliliters from various liquid measurements
            switch (Measurement)
            {
                case Globals.LiquidMeasurements.gal:
                    return MilliliterAmount / 3785.41;

                case Globals.LiquidMeasurements.liters:
                    return MilliliterAmount / 1000.00;

                case Globals.LiquidMeasurements.qt:
                    return MilliliterAmount / 946.353;

                case Globals.LiquidMeasurements.pt:
                    return MilliliterAmount / 473.176;

                case Globals.LiquidMeasurements.cups:
                    return MilliliterAmount / 236.588;

                case Globals.LiquidMeasurements.ml:
                    return MilliliterAmount;

                case Globals.LiquidMeasurements.floz:
                    return MilliliterAmount / 29.5735;
            }

            return MilliliterAmount;
        }

        public static Globals.LiquidMeasurements GetLiquidMeasurementFromString(string Measurement)
        {
            return (Globals.LiquidMeasurements)Enum.Parse(typeof(Globals.LiquidMeasurements), Measurement.ToLowerInvariant());
        }
    }
}
