﻿using System;

namespace NSC_DI.SAP
{
    public static class Document
    {
        public static T GetDocNum<T>(string pTable, int pDocEntry)
        {
            // get the DocNum for the specified DocEntry.

            try
            {
                return GetDocNum<T>(pTable, pDocEntry.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static T GetDocNum<T>(string pTable, string pDocEntry)
        {
            // get the DocNum for the specified DocEntry.

            if (string.IsNullOrEmpty(pTable)) throw new Exception("Table not specified");
            if (string.IsNullOrEmpty(pDocEntry)) throw new Exception("DocEntry not specified");

            try
            {
                var sql = "SELECT DocNum FROM " + pTable + " WHERE DocEntry = '" + pDocEntry + "'";
                var rtn = UTIL.SQL.GetValue<string>(sql);
                if (string.IsNullOrEmpty(rtn)) throw new Exception("DocEntry does not exist.");
                return (T)Convert.ChangeType(rtn, typeof(T));
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static T GetDocEntry<T>(string pTable, int pDocNum)
        {
            // get the DocEntry for the specified DocNum.

            try
            {
                return GetDocEntry<T>(pTable, pDocNum.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static T GetDocEntry<T>(string pTable, string pDocNum)
        {
            // get the DocEntry for the specified DocNum.

            if (string.IsNullOrEmpty(pTable)) throw new Exception("Table not specified");
            if (string.IsNullOrEmpty(pDocNum)) throw new Exception("DocNum not specified");

            try
            {
                var sql = "SELECT DocEntry FROM " + pTable + " WHERE DocNum = '" + pDocNum + "'";
                var rtn = UTIL.SQL.GetValue<string>(sql);
                if (string.IsNullOrEmpty(rtn)) throw new Exception("DocNum does not exist.");
                return (T)Convert.ChangeType(rtn, typeof(T));
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static string GetTableT(string pDocType, string pType)
        {
            // get the Marketing Document table based on the Document Type.
            // T title  or R row

            string sTable = sGetMDTableD(pDocType);

            switch (pType)
            {
                case "T":
                    // title
                    sTable = "O" + sTable;
                    break;
                case "R":
                    // row
                    sTable = sTable + "1";
                    break;
            }

            return sTable;
        }

        public static string sGetMDTableD(string pDocType)
        {

            // get the Marketing Document table based on the Document Type.

            string sTable = "";
            int iDocType = Convert.ToInt32(pDocType);

            switch (iDocType)
            {
                case (int)SAPbobsCOM.BoObjectTypes.oDrafts:
                    // 112   Draft
                    sTable = "DRF";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oQuotations:
                    // 23    Sales Quotes 
                    sTable = "QUT";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oOrders:
                    // 17    Sales Orders
                    sTable = "RDR";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oDeliveryNotes:
                    // 15    Deliveries
                    sTable = "DLN";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oReturns:
                    // 16    Returns
                    sTable = "RDN";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oInvoices:
                    // 13    A/R Invoices
                    sTable = "INV";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oCreditNotes:
                    // 14    A/R Credit Memos
                    sTable = "RIN";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oDownPayments:
                    // 203   A/R Down Payment
                    sTable = "DPI";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oPurchaseOrders:
                    // 22    Purchase Orders
                    sTable = "POR";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oPurchaseDeliveryNotes:
                    // 20    Goods Receipt POs
                    sTable = "PDN";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oPurchaseReturns:
                    // 21    Goods Returns
                    sTable = "RPD";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oPurchaseInvoices:
                    // 18    A/P Invoices
                    sTable = "PCH";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oPurchaseCreditNotes:
                    // 19    A/P Credit Memos
                    sTable = "RPC";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oPurchaseDownPayments:
                    // 204   A/P Down Payment
                    sTable = "DPO";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oIncomingPayments:
                    // 24    Incoming Payments
                    sTable = "RCT";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oVendorPayments:
                    // 46    Outgoing Payments
                    sTable = "VPM";
                    break;

                case 25:
                    // 25    Deposits
                    sTable = "DPS";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oChecksforPayment:
                    // 57    Checks For Payment
                    sTable = "CHO";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oJournalEntries:
                    // 30    Journal Entries
                    sTable = "JDT";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oStockTransfer:
                    // 67    Inventory Transfers
                    sTable = "WTR";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oInventoryGenExit:
                    // 60    Goods Issue
                    sTable = "IGE";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oInventoryGenEntry:
                    //  59   Goods Receipt
                    sTable = "IGN";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oMaterialRevaluation:
                    // 162   Inventory Revaluation
                    sTable = "MRV";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oProductionOrders:
                    // 202   Production Orders
                    sTable = "WOR";
                    break;

                case (int)SAPbobsCOM.BoObjectTypes.oItems:
                    // 4     Items
                    sTable = "ITM";
                    break;
            }

            return sTable;

            //Member                         Value   Description 
            //oChartOfAccounts                     1 ChartOfAccounts object. 
            //oBusinessPartners                    2 BusinessPartners object.  
            //oBanks                               3 Banks Object. 
            //oItems                               4 Items object.  
            //oVatGroups                           5 VatGroups Object. 
            //oPriceLists                          6 PriceLists object. 
            //oSpecialPrices                       7 SpecialPrices object. 
            //oItemProperties                      8 ItemProperties 
            //oUsers                              12 Users object.  
            //oInvoices                           13 Documents object that represents a draft of sales invoice document. 
            //oCreditNotes                        14 Documents object that represents a draft of sales credit note document. 
            //oDeliveryNotes                      15 Documents object that represents a draft of sales delivery note document. 
            //oReturns                            16 Documents object that represents a draft of sales return document. 
            //oOrders                             17 Documents object that represents a draft of sales order document. 
            //oPurchaseInvoices                   18 Documents object that represents a draft of purchase invoice document. 
            //oPurchaseCreditNotes                19 Documents object that represents a draft of purchase credit note document. 
            //oPurchaseDeliveryNotes              20 Documents object that represents a draft of purchase delivery note document. 
            //oPurchaseReturns                    21 Documents object that represents a draft of a purchase return document. 
            //oPurchaseOrders                     22 Documents object that represents a draft of purchase order document. 
            //oQuotations                         23 Documents object that represents a draft of sales quotation document. 
            //oIncomingPayments                   24 Payments object. 
            //oJournalVouchers                    28 JournalVouchers object.  
            //oJournalEntries                     30 JournalEntries object that represents a normal journal entry. 
            //oStockTakings                       31 StockTaking object. 
            //oContacts                           33 Contacts object.  
            //oCreditCards                        36 CreditCards Object 
            //oCurrencyCodes                      37 Currencies object. 
            //oPaymentTermsTypes                  40 PaymentTermsTypes object. 
            //oBankPages                          42 BankPages object. 
            //oManufacturers                      43 Manufacturers 
            //oVendorPayments                     46 Payments object that represents payments to vendors. 
            //oLandedCostsCodes                   48 LandedCostsCodes 
            //oShippingTypes                      49 ShippingTypes 
            //oLengthMeasures                     50 LengthMeasures 
            //oWeightMeasures                     51 WeightMeasures 
            //oItemGroups                         52 ItemGroups object. 
            //oSalesPersons                       53 SalesPersons 
            //oCustomsGroups                      56 CustomsGroups 
            //oChecksforPayment                   57 ChecksforPayment object.  
            //oInventoryGenEntry                  59 Documents object that is used to enter general items to the inventory. 
            //oInventoryGenExit                   60 Documents object that is used to exit general items from inventory. 
            //oWarehouses                         64 Warehouses object.  
            //oCommissionGroups                   65 CommissionGroups 
            //oProductTrees                       66 ProductTrees object. 
            //oStockTransfer                      67 StockTransfer object. 
            //oWorkOrders                         68 WorkOrders object. 
            //oCreditPaymentMethods               70 CreditPaymentMethods 
            //oCreditCardPayments                 71 CreditCardPayments 
            //oAlternateCatNum                    73 AlternateCatNum object. 
            //oBudget                             77 Budget object. 
            //oBudgetDistribution                 78 BudgetDistribution object. 
            //oMessages                           81 Messages object. 
            //oBudgetScenarios                    91 BudgetScenarios object. 
            //oSalesOpportunities                 97 SalesOpportunities object.  
            //oUserDefaultGroups                  93 UserDefaultGroups 
            //oSalesStages                       101 SalesStages 
            //oActivityTypes                     103 ActivityTypes object.  
            //oActivityLocations                 104 ActivityLocations object.  
            //oDrafts                            112 Documents object that represents a draft document (see Creating a draft document sample). 
            //oDeductionTaxHierarchies           116 DeductionTaxHierarchies object.  
            //oDeductionTaxGroups                117 DeductionTaxGroups object.  
            //oAdditionalExpenses                125 AdditionalExpenses object.  
            //oSalesTaxAuthorities               126 SalesTaxAuthorities object. 
            //oSalesTaxAuthoritiesTypes          127 SalesTaxAuthoritiesTypes object. 
            //oSalesTaxCodes                     128 SalesTaxCodes object. 
            //oQueryCategories                   134 QueryCategories object.  
            //oFactoringIndicators               138 object. FactoringIndicators  
            //oPaymentsDrafts                    140 Payments object. 
            //oAccountSegmentations              142 AccountSegmentations object. 
            //oAccountSegmentationCategories     143 AccountSegmentationCategories object. 
            //oWarehouseLocations                144 WarehouseLocations object. 
            //oForms1099                         145 Forms1099 object. 
            //oInventoryCycles                   146 InventoryCycles object. 
            //oWizardPaymentMethods              147 WizardPaymentMethods object. 
            //oBPPriorities                      150 BPPriorities object. 
            //oDunningLetters                    151 DunningLetters object. 
            //oUserFields                        152 UserFieldsMD object.  
            //oUserTables                        153 UserTablesMD object.  
            //oPickLists                         156 PickLists object. 
            //oPaymentRunExport                  158 PaymentRunExport object. 
            //oUserQueries                       160 UserQueries object. 
            //oMaterialRevaluation               162 MaterialRevaluation object.  
            //oCorrectionPurchaseInvoice         163 Documents object that represents a draft of purchase invoice correction document. 
            //oCorrectionPurchaseInvoiceReversal 164 Documents object that represents a draft of reverse purchase invoice correction document. 
            //oCorrectionInvoice                 165 Documents object that represents a draft of correction invoice document. 
            //oCorrectionInvoiceReversal         166 Documents object that represents a draft of reverse invoice correction document. 
            //oContractTemplates                 170 ContractTemplates object.  
            //oEmployeesInfo                     171 EmployeesInfo object.  
            //oCustomerEquipmentCards            176 CustomerEquipmentCards object. 
            //oWithholdingTaxCodes               178 WithholdingTaxCodes object. 
            //oBillOfExchangeTransactions        182 BillOfExchangeTransaction object. 
            //oKnowledgeBaseSolutions            189 KnowledgeBaseSolutions object.  
            //oServiceContracts                  190 ServiceContracts object.  
            //oServiceCalls                      191 ServiceCalls object.  
            //oUserKeys                          193 UserKeysMD object. 
            //oQueue                             194 Queue object. 
            //oSalesForecast                     198 SalesForecast object. 
            //oTerritories                       200 Territories object. 
            //oIndustries                        201 Industries object. 
            //oProductionOrders                  202 ProductionOrders object. 
            //oPackagesTypes                     205 PackagesTypes object. 
            //oUserObjectsMD                     206 UserObjectsMD object. 
            //oTeams                             211 Teams object. 
            //oRelationships                     212 Relationships object. 
            //oUserPermissionTree                214 UserPermissionTree object. 
            //oActivityStatus                    217 ActivityStatus object. 
            //oChooseFromList                    218 ChooseFromList object. 
            //oFormattedSearches                 219 FormattedSearches object. 
            //oAttachments2                      221 Attachments2 object. 
            //oUserLanguages                     223 UserLanguages object. 
            //oMultiLanguageTranslations         224 MultiLanguageTranslations object. 
            //oDynamicSystemStrings              229 DynamicSystemStrings object. 
            //oHouseBankAccounts                 231 HouseBankAccounts object. 
            //oBusinessPlaces                    247 BusinessPlaces object. 
            //oLocalEra                          250 LocalEra object. 
            //oSalesTaxInvoice                   280 Sales tax invoice. See TaxInvoices object and DocType property with the valid value botit_Invoice.  
            //oPurchaseTaxInvoice                281 Purchase tax invoice. See TaxInvoices object and DocType property with the valid value botit_Payment. 
            //BoRecordset                        300 Recordset object. 
            //BoBridge                           305 SBObob object. 
            //oNotaFiscalUsage                   260 NotaFiscalUsage object. 
            //oNotaFiscalCFOP                    258 NotaFiscalCFOP object. 
            //oNotaFiscalCST                     259 NotaFiscalCST object. 
            //oClosingDateProcedure              261 ClosingDateProcedure object. 
            //oBusinessPartnerGroups              10 BusinessPartnerGroups object. 
            //oBPFiscalRegistryID                278 BPFiscalRegistryID object. 
            //oDownPayments                      203 Documents object. 
            //oPurchaseDownPayments              204 Documents object. 

            //ACT	1	Chart of Accounts			
            //CRD	2	Business Partner Cards			
            //ITM	4	Items			
            //PLN	6	Price list names			
            //SPP	7	Special prices			
            //CPR	11	Contact employees			
            //USR	12	Users			
            //INV	13	Invoices			
            //RIN	14	Revert invoices			
            //DLN	15	Delivery notes			
            //RDN	16	Revert delivery notes			
            //RDR	17	Orders			
            //PCH	18	Purchases			
            //RPC	19	Revert purchases			
            //PDN	20	Purchase delivery notes			
            //RPD	21	Revert purchase delivery notes			
            //POR	22	Purchase orders			
            //QUT	23	Quotations			
            //RCT	24	Receipts incoming payments			
            //DPS	25	Bill of Exchange Deposits			
            //BTD	29	Journal vouchers			
            //JDT	30	Journal entries			
            //ITW	31	Item warehouse			
            //CLG	33	Contact activities			
            //CRN	37	Currency codes			
            //CTG	40	Payment terms types			
            //BNK	42	Bank pages			
            //VPM	46	Payments to vendors			
            //ITB	52	Item groups			
            //CHO	57	Checks for payment			
            //IGN	59	Inventory general entry			
            //IGE	60	Inventory general exit			
            //WHS	64	Warehouses codes and names			
            //ITT	66	Product trees			
            //WTR	67	Stock transfer			
            //WKO	68	Work orders			
            //SCN	73	Alternate catalog numbers			
            //BGT	77	Budget			
            //BGD	78	Budget Distribution			
            //ALR	81	Alerts messages			
            //BGS	91	Budget scenarios			
            //SRI	94	Items serial numbers			
            //OPR	97	Sales Opportunities			
            //IBT	106	Item batch numbers			
            //DRF	112	Document draft			
            //EXD	125	Additional Expenses			
            //STA	126	Sales tax authorities			
            //STT	127	Sales tax authorities type			
            //STC	128	Sales tax code			
            //DUN	151	Dunning letters			
            //UTB	153	User tables			
            //PEX	158	Payment run export			
            //MRV	162	Polish - Material revaluation			
            //CTT	170	Contract templates			
            //HEM	171	Employees			
            //INS	176	Customer equipment cards			
            //WHT	178	Withholding tax data			
            //BOE	181	Bill of exchange for payment			
            //BOT	182	Bill of exchange transaction			
            //CRB	187	Business partner - bank accounts			
            //SLT	189	Service call solutions			
            //CTR	190	Service contracts			
            //SCL	191	Service call			
            //UKD	193	User keys description			
            //QUE	194	Queues			
            //FCT	198	Sales forecast			
            //TER	200	Territories			
            //OND	201	Industries			
            //PKG	205	Packages types			
            //UDO	206	User-defined objects			
            //ORL	212	Relationships			
            //UPT	214	User permission tree			
            //CLA	217	Activity status			
            //CHF *	218	Choose From List Settings			
            //CSHS *	219	Search Object			
            //ACP *	220	Period Category			
            //ATC *	221	Attachments			
            //GFL *	222	Grid Filter			
            //LNG *	223	Languages			
            //MLT *	224	Multi Language Translation			
            //SDIS *	229	System Dynamic Strings			
            //SVR *	230	Saved Reconciliations			
            //DSC1	231	House Bank Account			
            //RDOC *	232	Report Layout Representation			
            //DGP *	233	Document Generation Parameter Sets			
        }

        public static string sGetMDTableF(string pFormNum, string pType)
        {
            // get the Marketing Document table based on the form.         
            // Type - T title  or R row

            string sTable = sGetMDTableF(pFormNum);

            switch (pType)
            {
                case "T":
                    sTable = "O" + sTable;
                    break;
                case "R":
                    sTable = sTable + "1";
                    break;
            }

            return sTable;
        }

        public static string sGetMDTableF(string pFormNum)
        {
            // get the Marketing Document table based on the form.         

            string sTable = "";

            switch (pFormNum)
            {
                case "149":
                    // Sales Quotes 
                    sTable = "QUT";

                    break;
                case "139":
                    // Sales Orders
                    sTable = "RDR";

                    break;
                case "133":
                    sTable = "INV";

                    break;
                case "180":
                    sTable = "RDN";

                    break;
                case "142":
                    // Purchase Order
                    sTable = "POR";
                    break;
            }

            return sTable;
        }

        public static void UpdateUDF(SAPbobsCOM.BoObjectTypes pType, int pDocEnt, string pName, dynamic pValue)
        {
            // set the type then get the Document

            try
            {
                var Doc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(pType);
                GetByKey(Doc, pDocEnt);

                Doc.UserFields.Fields.Item(pName).Value = pValue;
                if (Doc.Update() != 0) throw new Exception(Globals.oCompany.GetLastErrorCode().ToString() + " - " + Globals.oCompany.GetLastErrorDescription());

            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw new System.Collections.Generic.KeyNotFoundException(ex.Message);
            }

            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void GetByKey(out SAPbobsCOM.Documents pDoc, SAPbobsCOM.BoObjectTypes pType, int pDocEnt)
        {
            // set the type then get the Document

            try
            {
                pDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(pType);
                GetByKey(pDoc, pDocEnt);
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                throw new System.Collections.Generic.KeyNotFoundException(ex.Message);
            }

            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void GetByKey(SAPbobsCOM.Documents pDoc, int pDocEnt)
        {
            // get the Document

            try
            {
                if (pDoc.GetByKey(pDocEnt)) return;
                throw new System.Collections.Generic.KeyNotFoundException(Globals.oCompany.GetLastErrorDescription());
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static int Add(dynamic pDoc)
        {
            // Add the Document

            try
            {
                if (pDoc.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorCode().ToString() + " - " + Globals.oCompany.GetLastErrorDescription()));

                string newCode = null;

                // Get the newly created object's code
                Globals.oCompany.GetNewObjectCode(out newCode);
                if (newCode != null) return Convert.ToInt32(newCode);
                return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void Update(dynamic pDoc)
        {
            // Add the Document

            try
            {
                if (pDoc.Update() == 0) return;
                throw new System.Collections.Generic.KeyNotFoundException(Globals.oCompany.GetLastErrorCode().ToString() + " - " + Globals.oCompany.GetLastErrorDescription());
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static SAPbobsCOM.Documents Copy(SAPbobsCOM.Documents pDocFrom, SAPbobsCOM.BoObjectTypes pTypeTo)
        {
            // Copy the Document from an existing Document
            SAPbobsCOM.Documents oDoc = null;

            try
            {
                oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(pTypeTo);
                Copy(pDocFrom, oDoc);
                return oDoc;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void Copy(SAPbobsCOM.Documents pDocFrom, SAPbobsCOM.Documents pDocTo)
        {
            // Copy the Document from an existing Document

            try
            {
                // only set the DocDueDate if not allready set. The other dates are set by default or will be set.
                if (pDocTo.DocDueDate.Year < 2016) pDocTo.DocDueDate = DateTime.Today;

                pDocTo.CardCode = pDocFrom.CardCode;
                pDocTo.BPL_IDAssignedToInvoice = pDocFrom.BPL_IDAssignedToInvoice;
                if (NSC_DI.UTIL.Options.Value.GetSingle("User Can Set Delivery Date") == "Y")
                {
                    // REBEL 1889 if the user can set the delivery due date on SO it says the Delivery Date...
                    pDocTo.DocDate = pDocFrom.DocDueDate;
                    pDocTo.DocDueDate = pDocFrom.DocDueDate; // ...on Delivery to the one input on SO
                }
                //if (pDocFrom.Expenses.Count > 0)
                Freight(pDocFrom, pDocTo); //REBEL 2053

                for (int i = 0; i < pDocFrom.Lines.Count; i++)
                {
                    pDocFrom.Lines.SetCurrentLine(i);
                    if (i > 0) pDocTo.Lines.Add();
                    pDocTo.Lines.BaseEntry = pDocFrom.DocEntry;
                    pDocTo.Lines.BaseType = Convert.ToInt32(pDocFrom.DocObjectCodeEx);
                    pDocTo.Lines.BaseLine = pDocFrom.Lines.LineNum;

                    var batchSerQty = 0.0;

                    for (var j = 0; j < pDocFrom.Lines.BatchNumbers.Count; j++)
                    {
                        pDocFrom.Lines.BatchNumbers.SetCurrentLine(j);
                        if (pDocFrom.Lines.BatchNumbers.BatchNumber == "") continue;
                        if (j > 0) pDocTo.Lines.BatchNumbers.Add();
                        pDocTo.Lines.BatchNumbers.Quantity = pDocFrom.Lines.BatchNumbers.Quantity;
                        pDocTo.Lines.BatchNumbers.BatchNumber = pDocFrom.Lines.BatchNumbers.BatchNumber;
                        pDocTo.Lines.BatchNumbers.Location = pDocFrom.Lines.BatchNumbers.Location;
                        pDocTo.Lines.BatchNumbers.ManufacturerSerialNumber = pDocFrom.Lines.BatchNumbers.ManufacturerSerialNumber;
                        batchSerQty += pDocFrom.Lines.BatchNumbers.Quantity;
                    }

                    for (var j = 0; j < pDocFrom.Lines.SerialNumbers.Count; j++)
                    {
                        pDocFrom.Lines.SerialNumbers.SetCurrentLine(j);
                        if (pDocFrom.Lines.SerialNumbers.InternalSerialNumber == "") continue;
                        if (j > 0) pDocTo.Lines.SerialNumbers.Add();
                        pDocTo.Lines.SerialNumbers.InternalSerialNumber = pDocFrom.Lines.SerialNumbers.InternalSerialNumber;
                        pDocTo.Lines.SerialNumbers.SystemSerialNumber = pDocFrom.Lines.SerialNumbers.SystemSerialNumber;
                        pDocTo.Lines.SerialNumbers.Quantity = pDocFrom.Lines.SerialNumbers.Quantity;
                        batchSerQty += pDocFrom.Lines.SerialNumbers.Quantity;
                    }

                    if (batchSerQty > 0.0) pDocTo.Lines.Quantity = batchSerQty;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
        //

        public static void SetDefaultDistributionRules(SAPbobsCOM.Document_Lines pDocLine)
        {
            // get the Warehouse from the line to get the Default Distribution Rules
            //
            // **************   WAREHOUSE MUST BE SET BEFORE CALLING THIS SUB   **************

            try
            {
                if (NSC_DI.UTIL.Options.Value.GetSingle("Copy Distribution Rules", 1, "Copy Distribution Rules / Dimensions.", "Y") != "Y") return;
                var wh = pDocLine.WarehouseCode;
                var oRS = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery($"SELECT U_NSC_OcrCode1, U_NSC_OcrCode2, U_NSC_OcrCode3, U_NSC_OcrCode4, U_NSC_OcrCode5 FROM OWHS WHERE WhsCode = '{wh}'");

                pDocLine.CostingCode = oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                pDocLine.CostingCode2 = oRS.Fields.Item("U_NSC_OcrCode2").Value.ToString();
                pDocLine.CostingCode3 = oRS.Fields.Item("U_NSC_OcrCode3").Value.ToString();
                pDocLine.CostingCode4 = oRS.Fields.Item("U_NSC_OcrCode4").Value.ToString();
                pDocLine.CostingCode5 = oRS.Fields.Item("U_NSC_OcrCode5").Value.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void SetDefaultDistributionRules(SAPbobsCOM.StockTransfer_Lines pDocLine)
        {
            // get the Warehouse from the line to get the Default Distribution Rules
            //
            // **************   WAREHOUSE MUST BE SET BEFORE CALLING THIS SUB   **************

            try
            {
                if (NSC_DI.UTIL.Options.Value.GetSingle("Copy Distribution Rules", 1, "Copy Distribution Rules / Dimensions.", "Y") != "Y") return;
                var wh = pDocLine.WarehouseCode;
                var oRS = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery($"SELECT U_NSC_OcrCode1, U_NSC_OcrCode2, U_NSC_OcrCode3, U_NSC_OcrCode4, U_NSC_OcrCode5 FROM OWHS WHERE WhsCode = '{wh}'");

                pDocLine.DistributionRule = oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                pDocLine.DistributionRule2 = oRS.Fields.Item("U_NSC_OcrCode2").Value.ToString();
                pDocLine.DistributionRule3 = oRS.Fields.Item("U_NSC_OcrCode3").Value.ToString();
                pDocLine.DistributionRule4 = oRS.Fields.Item("U_NSC_OcrCode4").Value.ToString();
                pDocLine.DistributionRule5 = oRS.Fields.Item("U_NSC_OcrCode5").Value.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void SetDefaultDistributionRules(SAPbobsCOM.ProductionOrders pDoc)
        {
            // get the Warehouse from each line to get the Default Distribution Rules
            //
            // **************   WAREHOUSE MUST BE SET BEFORE CALLING THIS SUB   **************

            try
            {
                if (NSC_DI.UTIL.Options.Value.GetSingle("Copy Distribution Rules", 1, "Copy Distribution Rules / Dimensions.", "Y") != "Y") return;
                for (int i = 0; i < pDoc.Lines.Count; i++)
                {
                    pDoc.Lines.SetCurrentLine(i);
                    SetDefaultDistributionRules(pDoc.Lines);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void SetDefaultDistributionRules(SAPbobsCOM.ProductionOrders_Lines pDocLine)
        {
            // get the Warehouse from the line to get the Default Distribution Rules
            //
            // **************   WAREHOUSE MUST BE SET BEFORE CALLING THIS SUB   **************

            try
            {
                if (NSC_DI.UTIL.Options.Value.GetSingle("Copy Distribution Rules", 1, "Copy Distribution Rules / Dimensions.", "Y") != "Y") return;
                var wh = pDocLine.Warehouse;
                var oRS = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery($"SELECT U_NSC_OcrCode1, U_NSC_OcrCode2, U_NSC_OcrCode3, U_NSC_OcrCode4, U_NSC_OcrCode5 FROM OWHS WHERE WhsCode = '{wh}'");

                pDocLine.DistributionRule = oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                pDocLine.DistributionRule2 = oRS.Fields.Item("U_NSC_OcrCode2").Value.ToString();
                pDocLine.DistributionRule3 = oRS.Fields.Item("U_NSC_OcrCode3").Value.ToString();
                pDocLine.DistributionRule4 = oRS.Fields.Item("U_NSC_OcrCode4").Value.ToString();
                pDocLine.DistributionRule5 = oRS.Fields.Item("U_NSC_OcrCode5").Value.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void Freight(SAPbobsCOM.Documents pDocFrom, SAPbobsCOM.Documents pDocTo)
        {
            // REBEL 2053 - Sets the needed Freight line items for copying a doc using the complete button.
            try
            {
                for (int i = 0; i < pDocFrom.Expenses.Count; i++)
                {
                    pDocFrom.Expenses.SetCurrentLine(i);
                    if (pDocFrom.Expenses.LineGross <= 0)
                        continue;
                    if (i > 0)
                        pDocTo.Expenses.Add();
                    pDocTo.Expenses.DistributionMethod = pDocFrom.Expenses.DistributionMethod;
                    pDocTo.Expenses.DistributionRule = pDocFrom.Expenses.DistributionRule;
                    pDocTo.Expenses.DistributionRule2 = pDocFrom.Expenses.DistributionRule2;
                    pDocTo.Expenses.DistributionRule3 = pDocFrom.Expenses.DistributionRule3;
                    pDocTo.Expenses.DistributionRule4 = pDocFrom.Expenses.DistributionRule4;
                    pDocTo.Expenses.DistributionRule5 = pDocFrom.Expenses.DistributionRule5;
                    pDocTo.Expenses.ExpenseCode = pDocFrom.Expenses.ExpenseCode;
                    pDocTo.Expenses.LastPurchasePrice = pDocFrom.Expenses.LastPurchasePrice;
                    pDocTo.Expenses.LineGross = pDocFrom.Expenses.LineGross;
                    pDocTo.Expenses.LineTotal = pDocFrom.Expenses.LineTotal;
                    pDocTo.Expenses.Project = pDocFrom.Expenses.Project;
                    pDocTo.Expenses.Remarks = pDocFrom.Expenses.Remarks;
                    pDocTo.Expenses.Stock = pDocFrom.Expenses.Stock;
                    pDocTo.Expenses.TaxCode = pDocFrom.Expenses.TaxCode;
                    pDocTo.Expenses.VatGroup = pDocFrom.Expenses.VatGroup;
                    pDocTo.Expenses.WTLiable = pDocFrom.Expenses.WTLiable;
                }
            }
            catch(Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
        }               
    }
}
