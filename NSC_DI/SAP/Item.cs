﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NSC_DI.SAP
{
    public static class Item
    {
        public class BillOfMaterialItem
        {
            public string ItemCode { get; set; }
            public double Quantity { get; set; }
            public SAPbobsCOM.BoIssueMethod IssueMethod { get; set; }
            public string SourceWarehouseCode { get; set; }
        }

        public static void DeactivateItems(IEnumerable<string> pItemCodes)
        {
            SAPbobsCOM.Items item = null;
            try
            {
                item = (SAPbobsCOM.Items)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                foreach (var itemCode in pItemCodes)
                {
                    if (item.GetByKey(itemCode))
                    {
                        item.Valid = SAPbobsCOM.BoYesNoEnum.tNO;
                        if (item.Update() != 0)
                        {
                            throw new Exception(Globals.oCompany.GetLastErrorDescription());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                UTIL.Misc.KillObject(item);
                GC.Collect();
            }
        }
        /// <summary>
        /// This F() will allow you to pass a Named UDF (i.e., _NextNum) and update 
        /// </summary>
        /// <param name="pItemCode">Item code for the UDF you will update</param>
        /// <param name="pUDF">The System Name of the UDF</param>
        /// <param name="pVal">The integer value you will be setting the UDF to</param>
        public static void UpdateItemUDF_Int(string pItemCode, string pUDF, int pVal)
        {
            SAPbobsCOM.Items item = null;
            try
            {
                item = (SAPbobsCOM.Items)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);

                if (item.GetByKey(pItemCode))
                {
                    item.UserFields.Fields.Item("U_" + Globals.SAP_PartnerCode + pUDF).Value = pVal;
                    if (item.Update() != 0)
                    {
                        throw new Exception(Globals.oCompany.GetLastErrorDescription());
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                UTIL.Misc.KillObject(item);
                GC.Collect();
            }
        }
        public static void AddItem(string ItemCode, string ItemName, Dictionary<string, string> UserDefinedFields, SAPbobsCOM.BoYesNoEnum ManageSerialNumbers, SAPbobsCOM.BoYesNoEnum ManageBatchNumbers, SAPbobsCOM.BoYesNoEnum InventoryItem
, SAPbobsCOM.BoYesNoEnum PurchaseItem, SAPbobsCOM.BoYesNoEnum SalesItem, int? ItemGroupCode = null, double? AvgStdPrice = null, string DefaultWarehouse = null, string propertyToSet= null, bool IsTemplateItem = false)
        {
            SAPbobsCOM.Items newItem = null;
            try
            {
                newItem	                    = (SAPbobsCOM.Items)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);

                newItem.ItemCode			= ItemCode;
                newItem.ItemName			= ItemName;
                newItem.ManageSerialNumbers = ManageSerialNumbers;
                newItem.ManageBatchNumbers	= ManageBatchNumbers;
                newItem.InventoryItem		= InventoryItem;
                newItem.PurchaseItem		= PurchaseItem;
                newItem.SalesItem			= SalesItem;

                if (!string.IsNullOrEmpty(propertyToSet)) SetItemProperty(newItem, propertyToSet);
                
                //SetItemProperty("Template", newItem);

                if (!string.IsNullOrEmpty(DefaultWarehouse)) newItem.DefaultWarehouse = DefaultWarehouse;
                
                foreach (string fieldName in UserDefinedFields.Keys)
                {
                    newItem.UserFields.Fields.Item(fieldName).Value = UserDefinedFields[fieldName].ToString();
                }

                // TODO: Allow defined manage by warehouse vs assuming they want it warehouse managed
                newItem.ManageStockByWarehouse = SAPbobsCOM.BoYesNoEnum.tYES;

                if (ItemGroupCode.HasValue) newItem.ItemsGroupCode = ItemGroupCode.Value;
                
                if (AvgStdPrice.HasValue) newItem.PriceList.Price = AvgStdPrice.Value;

                newItem.MaterialType = SAPbobsCOM.BoMaterialTypes.mt_FinishedGoods;

                if (newItem.Add() != 0) throw new Exception(Globals.oCompany.GetLastErrorDescription());
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                UTIL.Misc.KillObject(newItem);
                GC.Collect();
            }
        }

        public static void SetItemProperty(SAPbobsCOM.Items pItem, string pSettingsProperty, SAPbobsCOM.BoYesNoEnum pVal = SAPbobsCOM.BoYesNoEnum.tYES)
        {
            if (int.TryParse(UTIL.Settings.Value.Get(Globals.SettingsItemPropPrefix + pSettingsProperty), out int propertyID) && propertyID >= 1 && propertyID <= 64)
            {
                pItem.Properties[propertyID] = pVal;
            }
            else throw new Exception(UTIL.Message.Format("Could not find property for item: " + pSettingsProperty));
        }

        public static void AddBOM(string ItemCode,  List<BillOfMaterialItem> BillOfMaterialItems = null, bool IsTemplateItem = false, string WarehouseCode = "")
		{
            SAPbobsCOM.ProductTrees newBom = null;
            try
			{
				newBom = (SAPbobsCOM.ProductTrees)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductTrees);

                newBom.TreeCode = ItemCode;
                newBom.TreeType =  ItemCode.StartsWith("~") ? SAPbobsCOM.BoItemTreeTypes.iTemplateTree : SAPbobsCOM.BoItemTreeTypes.iProductionTree;
                if (!string.IsNullOrEmpty(WarehouseCode)) newBom.Warehouse = WarehouseCode;

				foreach (var bomItem in BillOfMaterialItems)
                {
                    if (bomItem != BillOfMaterialItems.First()) newBom.Items.Add();
                    newBom.Items.ItemCode = bomItem.ItemCode;
                    newBom.Items.ItemType = SAPbobsCOM.ProductionItemType.pit_Item;
                    newBom.Items.Quantity = bomItem.Quantity;
                    newBom.Items.IssueMethod = bomItem.IssueMethod;
                    if (!string.IsNullOrEmpty(bomItem.SourceWarehouseCode)) newBom.Items.Warehouse = bomItem.SourceWarehouseCode;
                    
                }

				if (newBom.Add() != 0) throw new Exception(Globals.oCompany.GetLastErrorDescription());
				
			}
			catch (Exception ex)
			{

				throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
			}
            finally
            {
                UTIL.Misc.KillObject(newBom);
            }
		}
	}
}
