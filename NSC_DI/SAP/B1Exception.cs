﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSC_DI.SAP
{
    public class B1Exception : Exception
    {
        public B1Exception() : this(Globals.oCompany.GetLastErrorCode(), Globals.oCompany.GetLastErrorDescription())
        {
        }

        public B1Exception(string message) : base(message) { }

        public B1Exception(string message, Exception inner) : base(message, inner) { }

        public B1Exception(int errorCode, string errorDescription) : base(errorCode + " : " + errorDescription) { }

        public static void RaiseException()
        {
            if (InventoryBelowZeroException.HasException()) throw new InventoryBelowZeroException();
            else if (ProductNumberMissingException.HasException()) throw new ProductNumberMissingException();
            else throw new B1Exception();
        }
    }
    public class ProductNumberMissingException : B1Exception
    {
        public ProductNumberMissingException() : base() { }
        internal static bool HasException()
        {
            Globals.oCompany.GetLastError(out int errCode, out string errMsg);

            var ci = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;

            return (errCode == -5002) && ci.IndexOf(errMsg, "product number is missing", System.Globalization.CompareOptions.IgnoreCase) >= 0;
        }
    }

    public class InventoryBelowZeroException : B1Exception
    {
        public InventoryBelowZeroException() : base() { }

        internal static bool HasException()
        {
            Globals.oCompany.GetLastError(out int errCode, out string errMsg);

            return (errCode == -5002) && errMsg.Contains("Make sure that the consumed quantity of the component item would not cause the item's inventory to fall below zero");
        }
    }
}
