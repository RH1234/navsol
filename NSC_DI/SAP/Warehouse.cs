﻿using System;

namespace NSC_DI.SAP
{
	public static class Warehouse 
    {
		public static string GetType(string code)
        {
            // Create a SQL Query
			string query = "SELECT [U_" + Globals.SAP_PartnerCode + "_WhrsType] FROM [OWHS] WHERE [WhsCode] = '" + code + "'";
			return UTIL.SQL.GetValue<string>(query, "");
        }

        public static T GetField<T>(string pWH, string pField)
        {
            // routine to get an Item field
            if (string.IsNullOrEmpty(pWH)) throw new Exception("Warehouse Code not specified.");
            if (string.IsNullOrEmpty(pField)) throw new Exception("Field not specified.");

            try
            {
                var sql = "SELECT " + pField + " FROM OWHS WHERE WhsCode ='" + pWH + "'";
                var rtn = UTIL.SQL.GetValue<string>(sql, "");
                //if (string.IsNullOrEmpty(rtn)) throw new Exception("Warehouse Code does not exist.");
                return (T)Convert.ChangeType(rtn, typeof(T));
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static string GetName(string whsCode)
        {
            try
            {
                string qry = $"SELECT WhsName FROM OWHS WHERE WhsCode = '{whsCode}'";
                return UTIL.SQL.GetValue<string>(qry, string.Empty);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static string GetCode(string whsName)
        {
            try
            {
                string qry = $"SELECT TOP 1 WhsCode FROM OWHS WHERE WhsName = '{whsName}'";
                return UTIL.SQL.GetValue<string>(qry, string.Empty);
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static string GetCode(dynamic pBranch, string pType)
        {
            try
            {
                string query = $"SELECT WhsCode FROM [OWHS] WHERE [BPLid] = '{pBranch.ToString()}' AND U_NSC_WhrsType = '{pType}'";
                var ret = UTIL.SQL.GetValue<string>(query, "");
                return ret;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static int GetBranch(string code)
        {
            try
            {
                string query = "SELECT BPLid FROM [OWHS] WHERE [WhsCode] = '" + code + "'";
                int ret = UTIL.SQL.GetValue<int>(query, -1);
                return ret;
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static string Get_InterCoCode(string pWHCode = "")
        {
            // get the inter company code.
            // the value should be on the company, but was put on the warehouse.
            // if the wh is blank, then get the 1st non-blank inter company code.

            try
            {
                var sql = "";
                if (string.IsNullOrEmpty(pWHCode?.Trim()))// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pWHCode))
                    sql = $"SELECT TOP 1 ISNULL(U_NSC_SubsidiaryID,'') FROM OWHS WHERE ISNULL(U_NSC_SubsidiaryID,'') <> ''";
                else
                    sql = $"SELECT ISNULL(U_NSC_SubsidiaryID,'') FROM OWHS WHERE WhsCode = '{pWHCode}'";
                var code = UTIL.SQL.GetValue<string>(sql, "");
                var interCo = NSC_DI.UTIL.Settings.Value.Get("Inter-Company");

                return (interCo == "Y") ? code : "";
                    
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
