﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace NSC_DI.SAP
{
    public static class ProductionOrder
    {
        public static int CreateFromBOM(string pItemCode, double pPlannedQty, DateTime DueDate, bool pRelease = false, string pProcess = "", string pRemarks = "")
        {
            try
            {
                return CreateFromBOM(pItemCode, pPlannedQty, DueDate, null, pRelease, "N", pProcess, pRemarks);
            }
            catch (B1Exception ex) { throw ex; }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static int CreateFromBOM(string pItemCode, double pPlannedQty, DateTime DueDate, string DestinationWarehouse = null, bool pRelease = false,
                                        string pIsCannabis = "N", string pProcess = "", string pRemarks = "", string pProdBatchID = null)
        {
            var Key = 0;
            SAPbobsCOM.ProductionOrders oPdO = null;
            SAPbobsCOM.ProductTrees oBom = null;

            try
            {
                var itemName = NSC_DI.SAP.Items.GetItemNameFromItemCode(pItemCode);
                if (itemName.Length > 1 && itemName.Substring(0, 1) == "~") itemName = itemName.Substring(1);

                // #10824 - if branches are used, then have to change the WHs on the BOM
                var branch = NSC_DI.SAP.Branch.Get(DestinationWarehouse);
                if (branch >= 0)
                {
                    NSC_DI.SAP.BillOfMaterials.GetByKey(out oBom, pItemCode);
                    for (var i = 0; i < oBom.Items.Count; i++)
                    {
                        oBom.Items.SetCurrentLine(i);
                        var type = NSC_DI.SAP.Warehouse.GetType(oBom.Items.Warehouse);
                        oBom.Items.Warehouse = NSC_DI.SAP.Warehouse.GetCode(branch, type);
                    }
                    if (oBom.Update() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }

                oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
                oPdO.ProductionOrderType = SAPbobsCOM.BoProductionOrderTypeEnum.bopotStandard;
                oPdO.ItemNo = pItemCode;
                oPdO.ProductDescription = itemName;
                oPdO.PlannedQuantity = pPlannedQty;
                oPdO.DueDate = DueDate;
                oPdO.Remarks = pRemarks;
                oPdO.UserFields.Fields.Item("U_NSC_CnpProduct").Value = (pIsCannabis == "") ? "N" : pIsCannabis;
                var defaultProcess = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_NSC_DefProcess FROM OITT WHERE Code ='{pItemCode}'", ""); // get the process from the BOM
                if (!string.IsNullOrEmpty(defaultProcess?.Trim()))// Whitespace-Change (!NSC_DI.UTIL.Strings.Empty(defaultProcess))
                    pProcess = defaultProcess;
                oPdO.UserFields.Fields.Item("U_NSC_Process").Value = pProcess;     // this needs to be set after UDFS are copied (if they are copied).
                if (pProdBatchID != null) oPdO.UserFields.Fields.Item("U_NSC_ProductionBatchID").Value = pProdBatchID;

                if (DestinationWarehouse != null) oPdO.Warehouse = DestinationWarehouse;//"7001";

                //NSC_DI.SAP.Document.SetDefaultDistributionRules(oPdO);     // can't do this here because there are not rows until doc is added

                if (oPdO.Add() != 0) B1Exception.RaiseException();

                Key = B1Object.GetLastAdded();
                if (Key != 0)
                {
                    oPdO.GetByKey(Key);
                    NSC_DI.SAP.Document.SetDefaultDistributionRules(oPdO);

                    // Change status to released
                    if (pRelease) UpdateStatus(oPdO, SAPbobsCOM.BoProductionOrderStatusEnum.boposReleased);
                    else oPdO.Update();
                }

                return Key;
            }
            catch (B1Exception ex) { throw ex; }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                NSC_DI.UTIL.Misc.KillObject(oBom);
                GC.Collect();
            }
        }

        public static int CreateSpecialCropProdOrder(string pItemCode, double pPlannedQty, string pDestinationWarehouseCode, DateTime pDueDate, DateTime pStartDate, bool pRelease = false,
                                                    string pIsCannabis = "N", string pProcess = "", string pStageID = null, string pFinProj = null)
        {
            var Key = 0;
            SAPbobsCOM.ProductionOrders oPdO = null;
            try
            {
                oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
                oPdO.ProductionOrderType = SAPbobsCOM.BoProductionOrderTypeEnum.bopotStandard;
                oPdO.ItemNo = pItemCode;
                oPdO.PlannedQuantity = pPlannedQty;
                oPdO.DueDate = pDueDate;
                oPdO.StartDate = pStartDate;
                oPdO.Warehouse = pDestinationWarehouseCode;
                oPdO.ProductionOrderStatus = SAPbobsCOM.BoProductionOrderStatusEnum.boposPlanned;
                oPdO.UserFields.Fields.Item("U_NSC_CnpProduct").Value = pIsCannabis;
                oPdO.UserFields.Fields.Item("U_NSC_Special").Value = "Y";
                oPdO.UserFields.Fields.Item("U_NSC_Process").Value = pProcess; // this needs to be set after UDFS are copied (if they are copied).
                if (pStageID.Length > 0)
                {
                    oPdO.UserFields.Fields.Item("U_NSC_CropStageID").Value = pStageID;
                }

                string[] strHolder = pStageID.Split('_');
                if (pFinProj.Length > 0) oPdO.Project = pFinProj;

                ////Add Components -->
                //oPdO.Lines.ItemNo = "HOLDER";
                //oPdO.Lines.PlannedQuantity = 1;
                //oPdO.Lines.BaseQuantity = 1;
                //oPdO.Lines.Warehouse ="10";
                //NSC_DI.SAP.Document.SetDefaultDistributionRules(oPdO.Lines);

                //if (pFinProj.Length > 0) oPdO.Lines.Project = pFinProj;

                //oPdO.Lines.Add();

                if (oPdO.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                Key = B1Object.GetLastAdded();
                if (Key != 0)
                {
                    oPdO.GetByKey(Key);
                    for (int i = 0; i < oPdO.Lines.Count; i++)
                    {
                        oPdO.Lines.SetCurrentLine(i);
                        if (pFinProj.Length > 0) oPdO.Lines.Project = pFinProj;
                    }
                    oPdO.Update();
                    NSC_DI.SAP.Document.SetDefaultDistributionRules(oPdO);
                    // Change status to released
                    if (pRelease) UpdateStatus(Key, SAPbobsCOM.BoProductionOrderStatusEnum.boposReleased);

                }
                return Key;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                GC.Collect();
            }
        }

        public static int CreateSpecialProdOrderAndIssue(string pItemCode, double pPlannedQty, string pDestinationWarehouseCode, DateTime DueDate, List<SpecialProductionOrderComponentLineItem> components, bool pRelease = false, string pIsCannabis = "N", string pProcess = "", string pStageID = null)
        {
            var Key = 0;
            SAPbobsCOM.ProductionOrders oPdO = null;
            try
            {
                oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
                oPdO.ProductionOrderType = SAPbobsCOM.BoProductionOrderTypeEnum.bopotSpecial;
                oPdO.ItemNo = pItemCode;
                oPdO.PlannedQuantity = pPlannedQty;
                oPdO.DueDate = DueDate;
                oPdO.Warehouse = pDestinationWarehouseCode;
                oPdO.ProductionOrderStatus = SAPbobsCOM.BoProductionOrderStatusEnum.boposPlanned;
                oPdO.UserFields.Fields.Item("U_NSC_CnpProduct").Value = pIsCannabis;
                oPdO.UserFields.Fields.Item("U_NSC_Special").Value = "Y";
                oPdO.UserFields.Fields.Item("U_NSC_Process").Value = pProcess; // this needs to be set after UDFS are copied (if they are copied).
                if (pStageID != null && pStageID.Length > 0)
                {
                    oPdO.UserFields.Fields.Item("U_NSC_CropStageID").Value = pStageID;
                }

                //Add Components -->
                foreach (var item in components)
                {
                    oPdO.Lines.ItemNo = item.ItemCodeForItemOnBOM;
                    oPdO.Lines.PlannedQuantity = item.BaseQuantity;
                    oPdO.Lines.BaseQuantity = item.BaseQuantity;
                    if (item.SourceWarehouseCode != null)
                    {
                        oPdO.Lines.Warehouse = item.SourceWarehouseCode;
                    }
                    NSC_DI.SAP.Document.SetDefaultDistributionRules(oPdO.Lines);

                    oPdO.Lines.Add();
                }

                if (oPdO.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                Key = B1Object.GetLastAdded();
                if (Key != 0)
                {
                    // Change status to released
                    if (pRelease) UpdateStatus(Key, SAPbobsCOM.BoProductionOrderStatusEnum.boposReleased);


                    //Issue Components
                    SAPbobsCOM.Documents oDoc = null;
                    oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
                    oDoc.DocDate = DateTime.Now;
                    oDoc.DocDueDate = DateTime.Now;
                    oDoc.JournalMemo = "Issue for Production";
                    int i = 0;
                    foreach (var item in components)
                    {
                        if (oDoc.Lines.BaseEntry > 0) oDoc.Lines.Add();
                        oDoc.Lines.BaseEntry = Key;
                        oDoc.Lines.BaseLine = i;
                        oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                        oDoc.Lines.BatchNumbers.Quantity = item.BaseQuantity;
                        //Batch
                        oDoc.Lines.BatchNumbers.BatchNumber = item.BatchNo;
                        i++;
                    }

                    int returnCode = oDoc.Add();
                }
                return Key;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                GC.Collect();
            }
        }

        public static void Release(int pPdO)
        {
            try
            {
                NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(pPdO, SAPbobsCOM.BoProductionOrderStatusEnum.boposReleased);
            }
            catch (B1Exception ex) { throw ex; }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void UpdateRowWH(int pPdO, string pWH, string pItemCode, int pRow = -1)
        {
            //  update the row warehouse 

            SAPbobsCOM.ProductionOrders oPdO = null;

            try
            {
                oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                if (oPdO.GetByKey(pPdO) == false) NSC_DI.SAP.B1Exception.RaiseException();

                //--------------------------------------
                // get the correct line
                if (pRow != -1)
                {
                    oPdO.Lines.SetCurrentLine(pRow);
                    if (oPdO.Lines.ItemNo != pItemCode) throw new Exception(NSC_DI.UTIL.Message.Format("Item Code does not match Line Number."));
                }
                else
                {
                    for (int i = 0; i < oPdO.Lines.Count; i++)
                    {
                        oPdO.Lines.SetCurrentLine(i);
                        if (oPdO.Lines.ProductionOrderIssueType != SAPbobsCOM.BoIssueMethod.im_Manual) continue;
                        if (oPdO.Lines.ItemNo != pItemCode) continue;
                        pRow = i;
                        break;
                    }
                    if (pRow == -1) return;     // didn't find the item code.
                }

                //--------------------------------------
                // 
                if (pWH == "") pWH = oPdO.Lines.Warehouse;
                oPdO.Lines.Warehouse = pWH;

                if (oPdO.Update() != 0) NSC_DI.SAP.B1Exception.RaiseException();
            }
            catch (B1Exception ex) { throw ex; }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                GC.Collect();
            }
        }

        public static void AddBatchSN(int pPdO, string pItemCode, int pRow = -1, string pWH = "", string pBatchSN = "", double pQty = -99)
        {
            // Add the Batch or Serial Number to line on a Production Order
            // if 

            var Key = 0;

            SAPbobsCOM.ProductionOrders oPdO = null;

            try
            {
                oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                if (oPdO.GetByKey(pPdO) == false) NSC_DI.SAP.B1Exception.RaiseException();

                //--------------------------------------
                // get the correct line
                if (pRow != -1)
                {
                    oPdO.Lines.SetCurrentLine(pRow);
                    if (oPdO.Lines.ItemNo != pItemCode) throw new Exception(NSC_DI.UTIL.Message.Format("Item Code does not match Line Number."));
                }
                else
                {
                    for (int i = 0; i < oPdO.Lines.Count; i++)
                    {
                        oPdO.Lines.SetCurrentLine(i);
                        if (oPdO.Lines.ProductionOrderIssueType != SAPbobsCOM.BoIssueMethod.im_Manual) continue;
                        if (oPdO.Lines.ItemNo != pItemCode) continue;
                        pRow = i;
                        break;
                    }
                    if (pRow == -1) return;     // didn't find the item code.
                }

                //--------------------------------------
                // 
                if (pWH == "") pWH = oPdO.Lines.Warehouse;
                if (pQty == -99) pQty = oPdO.Lines.PlannedQuantity;
                oPdO.Lines.Warehouse = pWH;

                //--------------------------------------
                // Item is a batch
                if (NSC_DI.SAP.Items.GetField<string>(pItemCode, "ManBtchNum") == "Y")
                {
                    oPdO.Lines.BatchNumbers.SetCurrentLine(0);  // check if an entry already exists
                    if (oPdO.Lines.BatchNumbers.Count > 1 || oPdO.Lines.BatchNumbers.BatchNumber != "") oPdO.Lines.BatchNumbers.Add();
                    if (pBatchSN == "") pBatchSN = NSC_DI.SAP.Items.Batches.GetNo(pItemCode, pWH, -1, pQty);
                    if (pBatchSN == "") throw new Exception(NSC_DI.UTIL.Message.Format($"Batch ID is blank for Item: {pItemCode}   and WH: {pWH}"));
                    oPdO.Lines.BatchNumbers.BatchNumber = pBatchSN;
                    oPdO.Lines.BatchNumbers.Quantity = pQty;
                    //oPdO.Lines.BatchNumbers.BaseLineNumber = pRow;
                }

                //------------------------------------------------------------------------------------------------------------------------------------------
                // Item is a SN             *****************  NOT IMPLEMENTED  *******************
                if (NSC_DI.SAP.Items.GetField<string>(pItemCode, "ManSerNum") == "Y")
                {
                    oPdO.Lines.SerialNumbers.SetCurrentLine(0); // check if an entry already exists
                    if (oPdO.Lines.SerialNumbers.Count > 1 || oPdO.Lines.SerialNumbers.InternalSerialNumber == "") oPdO.Lines.SerialNumbers.Add();
                    oPdO.Lines.SerialNumbers.InternalSerialNumber = pBatchSN;
                    oPdO.Lines.SerialNumbers.Quantity = pQty;
                }

                if (oPdO.Update() != 0) NSC_DI.SAP.B1Exception.RaiseException();
            }
            catch (B1Exception ex) { throw ex; }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                GC.Collect();
            }
        }

        public static int Issue(int pPdO, string pItemProp, double pActualQty, string pSN)
        {
            return Issue(pPdO, pItemProp, pActualQty, null, new[] { pSN });
        }

        public static int Issue(int pPdO, string pItemProp, double pActualQty, string pBatch = null, IEnumerable<string> pSN = null, string pCropFinProj = null,
                                Dictionary<string, double> pdicPlants = null, string pHarvestName = "")
        {
            if (pActualQty == 0) return 0;
            var SQL = String.Empty;
            var Key = 0;

            //bool addLine = false;

            SAPbobsCOM.Documents oDoc = null;
            SAPbobsCOM.Recordset oRecordSet = null;

            try
            {
                // get the Item Group Code
                SQL = $@"
SELECT WOR1.LineNum,WOR1.BaseQty,OWOR.PlannedQty,OITM.ManBtchNum,WOR1.IssueType, OITM.ItmsGrpCod, OITM.ManSerNum, OITM.ItemCode FROM WOR1
JOIN OITM ON WOR1.ItemCode = OITM.ItemCode 
JOIN OWOR ON WOR1.DocEntry = OWOR.DocEntry
WHERE WOR1.DocEntry = '{pPdO.ToString()}' AND OWOR.Status='R' AND WOR1.PlannedQty > 0";

                //SQL += "SELECT WOR1.LineNum,WOR1.BaseQty,OWOR.PlannedQty,OITM.ManBtchNum,WOR1.IssueType, OITM.ItmsGrpCod, OITM.ManSerNum, OITM.ItemCode FROM WOR1" + Environment.NewLine;
                //SQL += "  JOIN OITM ON WOR1.ItemCode = OITM.ItemCode join OWOR on WOR1.DocEntry = OWOR.DocEntry" + Environment.NewLine;
                //SQL += $"JOIN OBTN ON OITM.ItemCode = obtn.ItemCode and OBTN.DistNumber = {pBatch}";
                //SQL += " WHERE WOR1.DocEntry = " + pPdO.ToString() + " AND OWOR.Status='R' AND WOR1.PlannedQty > 0"; // + " AND OITM.ItmsGrpCod = '" + pItmGrpCode + "' AND WOR1.PlannedQty > 0";

                oRecordSet = (SAPbobsCOM.Recordset)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                //oRecordSet = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(SQL);
                oRecordSet.DoQuery(SQL);


                //            var ItemLine = NSC_DI.UTIL.SQL.GetValue<int>(SQL, -1);
                //if (ItemLine == -1) throw new Exception(NSC_DI.UTIL.Message.Format("Unable to locate specific line in Production Order."));

                if (oRecordSet.RecordCount == 0) throw new Exception(NSC_DI.UTIL.Message.Format("Unable to locate specific line in Production Order." + Environment.NewLine + SQL + Environment.NewLine));

                oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
                oDoc.DocDate = DateTime.Now;
                oDoc.DocDueDate = DateTime.Now;
                oDoc.JournalMemo = "Issue for Production";

                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null)
                    oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                if (pCropFinProj != null && pCropFinProj.Length > 0)
                {
                    oDoc.Project = pCropFinProj;

                }

                // set the Branch
                var wh = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Warehouse FROM OWOR WHERE DocEntry = {pPdO}", null);
                var br = NSC_DI.SAP.Branch.Get(wh);
                if (br > 0) oDoc.BPL_IDAssignedToInvoice = br;

                for (int i = 0; i <= oRecordSet.RecordCount - 1; i++)
                {

                    if (i > 0) oRecordSet.MoveNext();

                    //**** only process the items that have the correct property set ****
                    //Check to see if there are multiple properties passed. 
                    //bool ProperyCheck = false;
                    if (pItemProp.Contains(',') == true)
                    {
                        string[] strHolder = pItemProp.Split(',');
                        for (int j = 0; j > strHolder.Length; j++)
                        {
                            if (NSC_DI.SAP.Items.Property.Get(oRecordSet.Fields.Item("ItemCode").Value, strHolder[j]) != "Y") continue;
                        }
                    }
                    else if (NSC_DI.SAP.Items.Property.Get(oRecordSet.Fields.Item("ItemCode").Value, pItemProp) != "Y") continue;

                    if (oDoc.Lines.BaseEntry > 0) oDoc.Lines.Add();
                    //addLine = true;

                    //oDoc.Lines.SetCurrentLine(i);
                    oDoc.Lines.BaseEntry = pPdO;
                    oDoc.Lines.BaseLine = oRecordSet.Fields.Item("LineNum").Value; //ItemLine;
                    oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                    if (pCropFinProj != null && pCropFinProj.Length > 0)
                    {
                        oDoc.Lines.ProjectCode = pCropFinProj;
                    }

                    // BATCHES
                    if (oRecordSet.Fields.Item("ManBtchNum").Value == "Y")
                    {
                        //oDoc.Lines.Quantity = pActualQty;
                        oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_HarvestName").Value = pHarvestName;

                        if (string.IsNullOrEmpty(pBatch?.Trim()) && pdicPlants == null) throw new Exception(NSC_DI.UTIL.Message.Format($"Item {oRecordSet.Fields.Item("ItemCode").Value} is Batch Managed. Check the BOM."));// Whitespace-Change (!NSC_DI.UTIL.Strings.Empty(pBatch))

                        if (pdicPlants == null)
                        {
                            oDoc.Lines.BatchNumbers.Quantity = pActualQty * oRecordSet.Fields.Item("BaseQty").Value;
                            oDoc.Lines.BatchNumbers.BatchNumber = pBatch;

                        }
                        else
                        {
                            int CountHolder = 0;
                            oDoc.Lines.Quantity = pActualQty * oRecordSet.Fields.Item("BaseQty").Value;
                            foreach (KeyValuePair<string, double> PlantBatch in pdicPlants)
                            {
                                if (CountHolder != 0) oDoc.Lines.BatchNumbers.Add();
                                oDoc.Lines.BatchNumbers.Quantity = PlantBatch.Value;
                                oDoc.Lines.BatchNumbers.BatchNumber = PlantBatch.Key;
                                CountHolder++;
                            }
                        }

                    }

                    // SERIAL NUMBERS
                    if (oRecordSet.Fields.Item("ManSerNum").Value == "Y")
                    {
                        if (pSN == null || !pSN.Any()) throw new Exception(NSC_DI.UTIL.Message.Format($"Item {oRecordSet.Fields.Item("ItemCode").Value} is Serial Number Managed, but no erial Numbers are specified."));
                        //if (i != 0) oDoc.Lines.Add();
                        //oDoc.Lines.SetCurrentLine(i);
                        oDoc.Lines.Quantity = pSN.Count();
                        foreach (var s in pSN)
                        {
                            if (s != pSN.First()) oDoc.Lines.SerialNumbers.Add();
                            //oDoc.Lines.SerialNumbers.BatchID = s.ToString();
                            oDoc.Lines.SerialNumbers.SystemSerialNumber = Convert.ToInt32(s);
                            oDoc.Lines.SerialNumbers.Quantity = 1;
                            
                            //oDoc.Lines.SerialNumbers.UserFields.Fields.Item("U_NSC_WetWgt").Value = "11";
                            oDoc.Lines.SerialNumbers.UserFields.Fields.Item("U_NSC_HarvestName").Value = pHarvestName;
                        }
                    }
                }
                if (oDoc.Lines.BaseEntry < 1)
                    throw new Exception(NSC_DI.UTIL.Message.Format($"No component found with property of {pItemProp}"));

                int returnCode = oDoc.Add();

                if (returnCode == 0) Key = B1Object.GetLastAdded();
                return Key;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDoc);
                NSC_DI.UTIL.SQL.KillObject(oRecordSet);
                GC.Collect();
            }
        }

        public static int Issue(SAPbobsCOM.Document_Lines pPdO_Lines, int pLineNum = -1)
        {
            var Key = 0;
            SAPbobsCOM.Documents oDoc = null;

            try
            {
                oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
                oDoc.DocDate = DateTime.Now;
                oDoc.JournalMemo = "Issue for Production";

                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null)
                    oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                for (int i = 0; i < pPdO_Lines.Count; i++)
                {
                    if (i == 0)
                    {
                        // set the Branch
                        var wh = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Warehouse FROM OWOR WHERE DocEntry = {pPdO_Lines.DocEntry}", null);
                        var br = NSC_DI.SAP.Branch.Get(wh);
                        if (br > 0) oDoc.BPL_IDAssignedToInvoice = br;
                    }

                    if (i > 0 && pLineNum != -1) oDoc.Lines.Add();
                    pPdO_Lines.SetCurrentLine(i);
                    if (pPdO_Lines.LineNum != pLineNum && pLineNum != -1) continue;  // a specific line was referenced
                    oDoc.Lines.BaseEntry = pPdO_Lines.DocEntry;
                    oDoc.Lines.BaseLine = pPdO_Lines.LineNum;
                    oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                    oDoc.Lines.Quantity = pPdO_Lines.Quantity;
                    for (int j = 0; j < pPdO_Lines.BatchNumbers.Count; j++)
                    {
                        if (j > 0) oDoc.Lines.BatchNumbers.Add();
                        pPdO_Lines.BatchNumbers.SetCurrentLine(j);
                        oDoc.Lines.BatchNumbers.BatchNumber = pPdO_Lines.BatchNumbers.BatchNumber;
                        oDoc.Lines.BatchNumbers.Quantity = pPdO_Lines.BatchNumbers.Quantity;
                    }
                    for (int j = 0; j < pPdO_Lines.SerialNumbers.Count; j++)
                    {
                        if (j > 0) oDoc.Lines.BatchNumbers.Add();
                        pPdO_Lines.BatchNumbers.SetCurrentLine(j);
                        oDoc.Lines.SerialNumbers.InternalSerialNumber = pPdO_Lines.SerialNumbers.InternalSerialNumber;
                        oDoc.Lines.SerialNumbers.Quantity = pPdO_Lines.SerialNumbers.Quantity;
                    }
                }
                if (oDoc.Add() != 0) Key = B1Object.GetLastAdded();

                return Key;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDoc);
                GC.Collect();
            }
        }

        public static int Issue_RHH(int pPdO, string pItmGrpNam, double pActualQty, string pBatch = null, IEnumerable<int> pSN = null)
        {
            if (pActualQty == 0) return 0;
            var SQL = String.Empty;
            var Key = 0;
            SAPbobsCOM.Documents oDoc = null;
            SAPbobsCOM.Document_Lines oDocLine = null;
            try
            {
                SQL = "";
                SQL += "SELECT LineNum FROM OWOR";
                SQL += "  JOIN OITM ON WOR1.ItemCode = OITM.ItemCode";
                SQL += " WHERE DocEntry = " + pPdO.ToString() + " AND ItmGrpCod = '" + NSC_DI.SAP.ItemGroups.GetCode<string>(pItmGrpNam) + "' AND PlannedQty > 0";
                var ItemLine = NSC_DI.UTIL.SQL.GetValue<int>(SQL, -1);
                if (ItemLine == -1) throw new Exception(NSC_DI.UTIL.Message.Format("Unable to locate specific line in Production Order."));
                oDocLine = new SAPbobsCOM.Document_Lines()
                {
                    BaseEntry = pPdO,
                    BaseLine = ItemLine,
                    BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders,
                    Quantity = pActualQty
                };
                if (!string.IsNullOrEmpty(pBatch))
                {
                    oDocLine.BatchNumbers.BatchNumber = pBatch;
                    oDocLine.BatchNumbers.Quantity = pActualQty;

                }
                if (pSN != null && pSN.Any())
                {
                    foreach (var s in pSN)
                    {
                        if (s != pSN.First()) oDocLine.SerialNumbers.Add();
                        oDocLine.SerialNumbers.InternalSerialNumber = s.ToString();
                        oDocLine.SerialNumbers.Quantity = pActualQty;
                    }
                }

                return Issue(oDocLine);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDoc);
                UTIL.Misc.KillObject(oDocLine);
                GC.Collect();
            }
        }

        public static int IssueManual(int pPdO, bool issueAll = false, double issueAmt = 0)
        {
            // issue all manual items on a Production Order

            var Key = 0;

            SAPbobsCOM.ProductionOrders oPdO = null;
            SAPbobsCOM.Documents oIss = null;

            try
            {
                oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                oIss = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);

                if (oPdO.GetByKey(pPdO) == false) NSC_DI.SAP.B1Exception.RaiseException();
                oIss.DocDate = DateTime.Now;

                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null)
                    oIss.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                // set the Branch
                var wh = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Warehouse FROM OWOR WHERE DocEntry = {pPdO}", null);
                var br = NSC_DI.SAP.Branch.Get(wh);
                if (br > 0) oIss.BPL_IDAssignedToInvoice = br;

                for (int i = 0; i < oPdO.Lines.Count; i++)
                {

                    oPdO.Lines.SetCurrentLine(i);
                    //Skip Manually Issued Resources 
                    string srHolder = oPdO.Lines.ItemType.ToString();
                    if (oPdO.Lines.ItemType.ToString() != "pit_Resource")
                    {
                        var ss = oPdO.Lines.ItemNo;
                        var s2 = NSC_DI.UTIL.Options.Value.GetSingle("Packaging PP - Issue all");
                        if (oPdO.Lines.ProductionOrderIssueType != SAPbobsCOM.BoIssueMethod.im_Manual) continue;
                        // if the item is batch\SN controlled and there is no batch\SN, skip this item. 
                        // this is due to the change where the user does not want all manual items isued automatically from Pack Prod Planner

                        if (NSC_DI.SAP.Items.GetField<string>(oPdO.Lines.ItemNo, "ManBtchNum") == "N" &&
                            NSC_DI.SAP.Items.GetField<string>(oPdO.Lines.ItemNo, "ManSerNum") == "N" &&
                            NSC_DI.UTIL.Options.Value.GetSingle("Packaging PP - Issue all") != "Y") continue;
                        if (NSC_DI.SAP.Items.GetField<string>(oPdO.Lines.ItemNo, "ManBtchNum") == "Y" && string.IsNullOrEmpty(oPdO.Lines.BatchNumbers.BatchNumber?.Trim())) continue;// Whitespace-Change NSC_DI.UTIL.Strings.Empty(oPdO.Lines.BatchNumbers.BatchNumber))
                        if (NSC_DI.SAP.Items.GetField<string>(oPdO.Lines.ItemNo, "ManSerNum") == "Y" && string.IsNullOrEmpty(oPdO.Lines.SerialNumbers.InternalSerialNumber?.Trim())) continue;// Whitespace-Change NSC_DI.UTIL.Strings.Empty(oPdO.Lines.BatchNumbers.InternalSerialNumber))
                        if (oIss.Lines.BaseEntry != 0) oIss.Lines.Add();

                        oIss.Lines.BaseEntry = oPdO.Lines.DocumentAbsoluteEntry;
                        oIss.Lines.BaseLine = oPdO.Lines.LineNumber;
                        oIss.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                        if (issueAll)                     
                            oIss.Lines.Quantity = issueAmt;                    
                        else
                            oIss.Lines.Quantity = oPdO.Lines.PlannedQuantity;
                        for (int j = 0; j < oPdO.Lines.BatchNumbers.Count; j++)
                        {
                            if (j > 0) oIss.Lines.BatchNumbers.Add();
                            oPdO.Lines.BatchNumbers.SetCurrentLine(j);
                            oIss.Lines.BatchNumbers.BatchNumber = oPdO.Lines.BatchNumbers.BatchNumber;
                            if (issueAll)
                                oIss.Lines.BatchNumbers.Quantity = issueAmt;
                            else
                                oIss.Lines.BatchNumbers.Quantity = oPdO.Lines.BatchNumbers.Quantity;
                        }
                        for (int j = 0; j < oPdO.Lines.SerialNumbers.Count; j++)
                        {
                            if (j > 0) oIss.Lines.SerialNumbers.Add();
                            oPdO.Lines.SerialNumbers.SetCurrentLine(j);
                            oIss.Lines.SerialNumbers.InternalSerialNumber = oPdO.Lines.SerialNumbers.InternalSerialNumber;
                            oIss.Lines.SerialNumbers.Quantity = oPdO.Lines.SerialNumbers.Quantity;
                        }
                    }
                    else
                    {
                        //Resource Do Nothing
                    }
                }
                if (oIss.Add() != 0) NSC_DI.SAP.B1Exception.RaiseException();

                Key = B1Object.GetLastAdded();
                return Key;
            }
            catch (B1Exception ex) { throw ex; }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                NSC_DI.UTIL.Misc.KillObject(oIss);
                GC.Collect();
            }
        }

        public static int Receive(int pPdo, string pItmGrpNam, double pActualQty, string pSN)
        {
            return Receive(pPdo, pItmGrpNam, pActualQty, null, new[] { pSN });
        }

        public static int ReceiveCuring(int pPdo, string pProcess = null, string pRawProperty = "", Dictionary<string, string> pByProductAndWeight = null,
                                        dynamic pUserDefinedFields = null, Dictionary<string, Dictionary<string, string>> pByProdUDFs = null)
        {
            // **** THIS FUNCTION IS FOR CURING ONLY **** 

            if (pByProductAndWeight.Count == 0) return 0;


            var Key = 0;

            var pParentBatch = string.Empty;

            double ActualQty = 0.0;
            SAPbobsCOM.Documents oDoc = null;

            try
            {
                var qry = $"SELECT LineNum, WOR1.ItemCode, Wor1.BaseQty, OITM.ManBtchNum, WOR1.IssueType, OITM.ItmsGrpCod, WOR1.wareHouse FROM WOR1 JOIN OWOR ON WOR1.DocEntry = OWOR.DocEntry JOIN OITM ON WOR1.ItemCode = OITM.ItemCode WHERE OWOR.Status = 'R' AND OWOR.DocNum = {pPdo}";
                var dtLineItems = UTIL.SQL.DataTable(qry);


                oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                oDoc.DocDate = DateTime.Now;

                oDoc.DocDueDate = DateTime.Now;

                oDoc.JournalMemo = "Receipt from Production";

                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null)
                    oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                // set the Branch
                var wh = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Warehouse FROM OWOR WHERE DocEntry = {pPdo}", null);
                var br = NSC_DI.SAP.Branch.Get(wh);
                if (br > 0) oDoc.BPL_IDAssignedToInvoice = br;

                //Check for User defined fields
                if (pUserDefinedFields != null)
                    NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.SerialNumbers, pUserDefinedFields, new string[1] { "U_NSC_StateID" });
                //{
                //    foreach (KeyValuePair<string, string> pair in pUserDefinedFields)
                //        oDoc.Lines.SerialNumbers.UserFields.Fields.Item(pair.Key).Value = pair.Value;
                //}

                //****First line on on good receipt Prod will be the item being created from the production order

                string ParentItemCode = UTIL.SQL.GetValue<string>($"Select ItemCode from OWOR where DocNum = {pPdo}", null);

                //add batches to Curred Bud

                //pParentBatch = UTIL.AutoStrain.NextBatch(ParentItemCode);
                pParentBatch = NSC_DI.SAP.BatchItems.NextBatch(ParentItemCode);

                oDoc.Lines.BaseType = 0;
                oDoc.Lines.BaseEntry = pPdo; //NSC_DI.UTIL.SQL.GetValue<int>(string.Format(@"Select DocEntry from OWOR where DocNum = {0}", pPdO), null); 
                oDoc.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                oDoc.Lines.WarehouseCode = UTIL.SQL.GetValue<string>(string.Format(@"Select warehouse from OWOR where DocNum = {0}", pPdo), null);

                NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                //---------------------------------------------------
                // Parent Item processing
                if (pByProductAndWeight.ContainsKey(ParentItemCode))
                {
                    var baseQty = int.Parse(dtLineItems.Rows[0]["BaseQty"] as string);

                    var val = baseQty * double.Parse(pByProductAndWeight[ParentItemCode]);

                    oDoc.Lines.Quantity = val;

                    ActualQty = val;

                    oDoc.Lines.BatchNumbers.BatchNumber = pParentBatch;

                    oDoc.Lines.BatchNumbers.Quantity = val;
                    if (pByProdUDFs.ContainsKey(ParentItemCode))
                    {
                        foreach (var kvp in pByProdUDFs[ParentItemCode])
                        {
                            if (kvp.Key == "ManufacturerSerialNumber") oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = kvp.Value;
                            else oDoc.Lines.BatchNumbers.UserFields.Fields.Item(kvp.Key).Value = kvp.Value;
                        }
                        NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.BatchNumbers, pUserDefinedFields, new string[1] { "U_NSC_StateID" });
                    }
                }

                #region ByProducts
                //---------------------------------------------------
                //Check For ByProducts

                var drs = dtLineItems.Select("BaseQty < 0");

                if (drs.Any())
                {
                    var pByProdBatch = GetBatchNumber(pPdo, pRawProperty);

                    foreach (var dr in dtLineItems.Select("BaseQty < 0"))
                    {

                        // Process only batch-managed items

                        if (!string.Equals(dr["ManBtchNum"] as string, "Y", StringComparison.InvariantCultureIgnoreCase)) continue;

                        oDoc.Lines.Add();

                        oDoc.Lines.BaseType = 0;

                        oDoc.Lines.BaseEntry = pPdo;

                        oDoc.Lines.BaseLine = int.Parse(dr["LineNum"] as string);

                        var itemCode = dr["ItemCode"] as string;

                        if (pByProductAndWeight.ContainsKey(itemCode))
                        {
                            var baseQuantity = double.Parse(dr["BaseQty"] as string) * -1;

                            var enteredQuantity = double.Parse(pByProductAndWeight[itemCode]);

                            oDoc.Lines.Quantity = baseQuantity * enteredQuantity;

                            oDoc.Lines.BatchNumbers.BatchNumber = pByProdBatch;

                            oDoc.Lines.BatchNumbers.Quantity = oDoc.Lines.Quantity;

                            if (pByProdUDFs.ContainsKey(ParentItemCode))
                            {
                                foreach (var kvp in pByProdUDFs[ParentItemCode])
                                {
                                    if (kvp.Key == "ManufacturerSerialNumber") oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = kvp.Value;
                                    else oDoc.Lines.BatchNumbers.UserFields.Fields.Item(kvp.Key).Value = kvp.Value;
                                }
                                NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.BatchNumbers, pUserDefinedFields, new string[1] { "U_NSC_StateID" });
                            }
                        }

                        oDoc.Lines.WarehouseCode = dr["wareHouse"] as string;

                        if (oDoc.Lines.WarehouseCode == "9999")
                        {
                            oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_QuarState").Value = "DEST";
                            oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_TimerQuar").Value = UTIL.Dates.CurrentTimeStamp.ToString();
                        }

                        NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                        //Check for User defined fields -COMMENT OUT FOR NOW BECAUSE WILL NEED A SEPERATE DICTIONARY FOR THESE UDFs
                        //if (pUserDefinedFields != null)
                        //{
                        //	foreach (KeyValuePair<string, string> pair in pUserDefinedFields)
                        //		oDoc.Lines.SerialNumbers.UserFields.Fields.Item(pair.Key).Value = pair.Value;
                        //}
                    }
                }

                var returnCode = oDoc.Add();

                if (returnCode == 0) Key = B1Object.GetLastAdded();
                else B1Exception.RaiseException();

                return Key;
            }
            catch (B1Exception ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {

                NSC_DI.UTIL.Misc.KillObject(oDoc);
                GC.Collect();
            }
            #endregion
        }

        public static int Receive_OLD(int pPdo, string pItmGrpNam, string pProcess = null, string pRawProperty = "", Dictionary<string, string> pByProductAndWeight = null, dynamic pUserDefinedFields = null, Dictionary<string, Dictionary<string, string>> pByProdUDFs = null)
        {
            // **** THIS FUNCTION IS FOR CURING ONLY **** 

            if (pByProductAndWeight.Count == 0) return 0;


            var Key = 0;

            var pParentBatch = string.Empty;

            double ActualQty = 0.0;
            SAPbobsCOM.Documents oDoc = null;

            try
            {
                var qry = $"SELECT LineNum, WOR1.ItemCode, Wor1.BaseQty, OITM.ManBtchNum, WOR1.IssueType, OITM.ItmsGrpCod, WOR1.wareHouse FROM WOR1 JOIN OWOR ON WOR1.DocEntry = OWOR.DocEntry JOIN OITM ON WOR1.ItemCode = OITM.ItemCode WHERE OWOR.Status = 'R' AND OWOR.DocNum = {pPdo}";
                var dtLineItems = UTIL.SQL.DataTable(qry);


                oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                oDoc.DocDate = DateTime.Now;

                oDoc.DocDueDate = DateTime.Now;

                oDoc.JournalMemo = "Receipt from Production";

                //Check for User defined fields
                if (pUserDefinedFields != null)
                    NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.SerialNumbers, pUserDefinedFields, new string[1] { "U_NSC_StateID" });
                //{
                //    foreach (KeyValuePair<string, string> pair in pUserDefinedFields)
                //        oDoc.Lines.SerialNumbers.UserFields.Fields.Item(pair.Key).Value = pair.Value;
                //}

                //****First line on on good receipt Prod will be the item being created from the production order

                string ParentItemCode = UTIL.SQL.GetValue<string>($"Select ItemCode from OWOR where DocNum = {pPdo}", null);

                //add batches to Curred Bud

                pParentBatch = NSC_DI.SAP.BatchItems.NextBatch(ParentItemCode);

                oDoc.Lines.BaseType = 0;
                oDoc.Lines.BaseEntry = pPdo; //NSC_DI.UTIL.SQL.GetValue<int>(string.Format(@"Select DocEntry from OWOR where DocNum = {0}", pPdO), null); 
                oDoc.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                oDoc.Lines.WarehouseCode = UTIL.SQL.GetValue<string>(string.Format(@"Select warehouse from OWOR where DocNum = {0}", pPdo), null);

                //---------------------------------------------------
                // Parent Item processing
                if (pByProductAndWeight.ContainsKey(ParentItemCode))
                {
                    var baseQty = int.Parse(dtLineItems.Rows[0]["BaseQty"] as string);

                    var val = baseQty * double.Parse(pByProductAndWeight[ParentItemCode]);

                    oDoc.Lines.Quantity = val;

                    ActualQty = val;

                    oDoc.Lines.BatchNumbers.BatchNumber = pParentBatch;

                    oDoc.Lines.BatchNumbers.Quantity = val;
                    if (pByProdUDFs.ContainsKey(ParentItemCode))
                    {
                        foreach (var kvp in pByProdUDFs[ParentItemCode])
                        {
                            oDoc.Lines.BatchNumbers.UserFields.Fields.Item(kvp.Key).Value = kvp.Value;
                        }
                        NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.BatchNumbers, pUserDefinedFields, new string[1] { "U_NSC_StateID" });
                    }
                }

                #region ByProducts
                //---------------------------------------------------
                //Check For ByProducts

                var drs = dtLineItems.Select("BaseQty < 0");

                if (drs.Any())
                {
                    var pByProdBatch = GetBatchNumber(pPdo, pRawProperty);

                    foreach (var dr in dtLineItems.Select("BaseQty < 0"))
                    {

                        // Process only batch-managed items

                        if (!string.Equals(dr["ManBtchNum"] as string, "Y", StringComparison.InvariantCultureIgnoreCase)) continue;

                        oDoc.Lines.Add();

                        oDoc.Lines.BaseType = 0;

                        oDoc.Lines.BaseEntry = pPdo;

                        oDoc.Lines.BaseLine = int.Parse(dr["LineNum"] as string);

                        var itemCode = dr["ItemCode"] as string;

                        if (pByProductAndWeight.ContainsKey(itemCode))
                        {
                            var baseQuantity = double.Parse(dr["BaseQty"] as string) * -1;

                            var enteredQuantity = double.Parse(pByProductAndWeight[itemCode]);

                            oDoc.Lines.Quantity = baseQuantity * enteredQuantity;

                            oDoc.Lines.BatchNumbers.BatchNumber = pByProdBatch;

                            oDoc.Lines.BatchNumbers.Quantity = oDoc.Lines.Quantity;

                            if (pByProdUDFs.ContainsKey(ParentItemCode))
                            {
                                foreach (var kvp in pByProdUDFs[ParentItemCode])
                                {
                                    oDoc.Lines.BatchNumbers.UserFields.Fields.Item(kvp.Key).Value = kvp.Value;
                                }
                                NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.BatchNumbers, pUserDefinedFields, new string[1] { "U_NSC_StateID" });
                            }
                        }

                        oDoc.Lines.WarehouseCode = dr["wareHouse"] as string;

                        if (oDoc.Lines.WarehouseCode == "9999")
                        {
                            oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_QuarState").Value = "DEST";
                            oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_TimerQuar").Value = UTIL.Dates.CurrentTimeStamp.ToString();
                        }

                        //Check for User defined fields -COMMENT OUT FOR NOW BECAUSE WILL NEED A SEPERATE DICTIONARY FOR THESE UDFs
                        //if (pUserDefinedFields != null)
                        //{
                        //	foreach (KeyValuePair<string, string> pair in pUserDefinedFields)
                        //		oDoc.Lines.SerialNumbers.UserFields.Fields.Item(pair.Key).Value = pair.Value;
                        //}
                    }
                }

                var returnCode = oDoc.Add();

                if (returnCode == 0) Key = B1Object.GetLastAdded();
                else B1Exception.RaiseException();

                return Key;
            }
            catch (B1Exception ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {

                NSC_DI.UTIL.Misc.KillObject(oDoc);
                GC.Collect();
            }
            #endregion
        }

        public static int Receive_NEW(int pPdo, string pItmGrpNam, string pProcess = null, string pRawProperty = "", Dictionary<string, string> pByProductAndWeight = null,
                                    dynamic pUserDefinedFields = null, Dictionary<string, Dictionary<string, string>> pByProdUDFs = null)
        {
            // **** THIS FUNCTION IS FOR CURING ONLY **** 

            if (pByProductAndWeight.Count == 0) return 0;


            var Key = 0;

            var pParentBatch = string.Empty;

            double ActualQty = 0.0;
            SAPbobsCOM.Documents oDoc = null;

            try
            {
                var qry = $"SELECT LineNum, WOR1.ItemCode, Wor1.BaseQty, OITM.ManBtchNum, WOR1.IssueType, OITM.ItmsGrpCod, WOR1.wareHouse FROM WOR1 JOIN OWOR ON WOR1.DocEntry = OWOR.DocEntry JOIN OITM ON WOR1.ItemCode = OITM.ItemCode WHERE OWOR.Status = 'R' AND OWOR.DocNum = {pPdo}";
                var dtLineItems = UTIL.SQL.DataTable(qry);


                oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                oDoc.DocDate = DateTime.Now;

                oDoc.DocDueDate = DateTime.Now;

                oDoc.JournalMemo = "Receipt from Production";

                //Check for User defined fields
                if (pUserDefinedFields != null)
                    NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.BatchNumbers, pUserDefinedFields);
                //{
                //    foreach (KeyValuePair<string, string> pair in pUserDefinedFields)
                //        oDoc.Lines.SerialNumbers.UserFields.Fields.Item(pair.Key).Value = pair.Value;
                //}

                //****First line on on good receipt Prod will be the item being created from the production order

                string ParentItemCode = UTIL.SQL.GetValue<string>($"Select ItemCode from OWOR where DocNum = {pPdo}", null);

                //add batches to Curred Bud

                pParentBatch = NSC_DI.SAP.BatchItems.NextBatch(ParentItemCode);

                oDoc.Lines.BaseType = 0;
                oDoc.Lines.BaseEntry = pPdo; //NSC_DI.UTIL.SQL.GetValue<int>(string.Format(@"Select DocEntry from OWOR where DocNum = {0}", pPdO), null); 
                oDoc.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                oDoc.Lines.WarehouseCode = UTIL.SQL.GetValue<string>(string.Format(@"Select warehouse from OWOR where DocNum = {0}", pPdo), null);
                oDoc.Lines.BatchNumbers.BatchNumber = pParentBatch;
                oDoc.Lines.BatchNumbers.Quantity = NSC_DI.UTIL.SQL.GetValue<double>($"SELECT PlannedQty FROM OWOR WHERE DocEntry = {pPdo}", 0);

                NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                //---------------------------------------------------
                // Parent Item processing
                if (pByProductAndWeight.ContainsKey(ParentItemCode))
                {
                    var baseQty = int.Parse(dtLineItems.Rows[0]["BaseQty"] as string);

                    var val = baseQty * double.Parse(pByProductAndWeight[ParentItemCode]);

                    oDoc.Lines.Quantity = val;

                    ActualQty = val;

                    oDoc.Lines.BatchNumbers.BatchNumber = pParentBatch;

                    oDoc.Lines.BatchNumbers.Quantity = val;
                    if (pByProdUDFs.ContainsKey(ParentItemCode))
                    {
                        foreach (var kvp in pByProdUDFs[ParentItemCode])
                        {
                            oDoc.Lines.BatchNumbers.UserFields.Fields.Item(kvp.Key).Value = kvp.Value;
                        }
                        NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.BatchNumbers, pUserDefinedFields, new string[1] { "U_NSC_StateID" });
                    }
                }

                #region ByProducts
                //---------------------------------------------------
                //Check For ByProducts

                var drs = dtLineItems.Select("BaseQty < 0");

                if (drs.Any())
                {
                    var pByProdBatch = GetBatchNumber(pPdo, pRawProperty);

                    foreach (var dr in dtLineItems.Select("BaseQty < 0"))
                    {

                        // Process only batch-managed items

                        if (!string.Equals(dr["ManBtchNum"] as string, "Y", StringComparison.InvariantCultureIgnoreCase)) continue;

                        oDoc.Lines.Add();

                        oDoc.Lines.BaseType = 0;

                        oDoc.Lines.BaseEntry = pPdo;

                        oDoc.Lines.BaseLine = int.Parse(dr["LineNum"] as string);

                        var itemCode = dr["ItemCode"] as string;

                        if (pByProductAndWeight.ContainsKey(itemCode))
                        {
                            var baseQuantity = double.Parse(dr["BaseQty"] as string) * -1;

                            var enteredQuantity = double.Parse(pByProductAndWeight[itemCode]);

                            oDoc.Lines.Quantity = baseQuantity * enteredQuantity;

                            oDoc.Lines.BatchNumbers.BatchNumber = pByProdBatch;

                            oDoc.Lines.BatchNumbers.Quantity = oDoc.Lines.Quantity;

                            if (pByProdUDFs.ContainsKey(ParentItemCode))
                            {
                                foreach (var kvp in pByProdUDFs[ParentItemCode])
                                {
                                    oDoc.Lines.BatchNumbers.UserFields.Fields.Item(kvp.Key).Value = kvp.Value;
                                }
                                NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.BatchNumbers, pUserDefinedFields, new string[1] { "U_NSC_StateID" });
                            }
                        }

                        oDoc.Lines.WarehouseCode = dr["wareHouse"] as string;

                        if (oDoc.Lines.WarehouseCode == "9999")
                        {
                            oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_QuarState").Value = "DEST";
                            oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_TimerQuar").Value = UTIL.Dates.CurrentTimeStamp.ToString();
                        }

                        NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                        //Check for User defined fields -COMMENT OUT FOR NOW BECAUSE WILL NEED A SEPERATE DICTIONARY FOR THESE UDFs
                        //if (pUserDefinedFields != null)
                        //{
                        //	foreach (KeyValuePair<string, string> pair in pUserDefinedFields)
                        //		oDoc.Lines.SerialNumbers.UserFields.Fields.Item(pair.Key).Value = pair.Value;
                        //}
                    }
                }

                var returnCode = oDoc.Add();

                if (returnCode == 0) Key = B1Object.GetLastAdded();
                else B1Exception.RaiseException();

                return Key;
            }
            catch (B1Exception ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {

                NSC_DI.UTIL.Misc.KillObject(oDoc);
                GC.Collect();
            }
            #endregion
        }

        public static int Receive(int pPdO, string pItmGrpNam, double pActualQty, string pBatch = "", IEnumerable<string> pSN = null, string pProcess = null,
                                  dynamic pUserDefinedFields = null, dynamic pLineUDFs = null, System.Data.DataTable pBins = null, string pStateID = null, double pTotalWaste = 0.0
                                   , string pWasteBatch = "", string pCropID = null, string[,] pHarvestUDFs = null)
        {
            //if (pActualQty == 0d) return 0;

            var Key = 0;
            SAPbobsCOM.Documents oDoc = null;
            SAPbobsCOM.Recordset oRecordSet = null;

            try
            {
                // Get the Production Batch ID
                var sql = $"SELECT U_NSC_ProductionBatchID FROM OWOR WHERE DocEntry = {pPdO.ToString()}";
                var prodBatchID = NSC_DI.UTIL.SQL.GetValue<string>(sql, null);

                oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                oDoc.DocDate = DateTime.Now;
                oDoc.DocDueDate = DateTime.Now;
                oDoc.JournalMemo = "Receipt from Production";

                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null) oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                //the line below is a work around because the optional variable was losing its value and always coming in as null. 
                string strCropID = NSC_DI.UTIL.SQL.GetValue<string>("Select OWOR.Project from OWOR where owor.docnum=" + pPdO.ToString(), "");
                if (pCropID != null && strCropID.Length > 0)
                {
                    oDoc.Project = strCropID;
                }

                // set the branch
                var wh = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Warehouse FROM OWOR WHERE DocEntry = {pPdO}", null);
                var br = NSC_DI.SAP.Branch.Get(wh);
                if (br > 0) oDoc.BPL_IDAssignedToInvoice = br;
                string strItemCode = NSC_DI.UTIL.SQL.GetValue<string>(string.Format(@"Select ItemCode from OWOR where DocNum = {0}", pPdO), null);
                // Erics new stuff, 
                // Runs check to see if the user wants any of the items to be utilized

                if (pActualQty > 0)
                {
                    oDoc.Lines.BaseType = 0;
                    oDoc.Lines.BaseEntry = pPdO;
                    //oDoc.Lines.TransactionType  = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;    // #6354
                    oDoc.Lines.Quantity = pActualQty;
                    oDoc.Lines.WarehouseCode = NSC_DI.UTIL.SQL.GetValue<string>(string.Format(@"Select warehouse from OWOR where DocNum = {0}", pPdO), null);

                    NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                    if (strCropID.Length > 0)
                    {
                        oDoc.Lines.ProjectCode = strCropID;
                    }
                    //NSC_DI.UTIL.UDO.CopyUDFs(oDoc, pLineUDFs, new string[1] { "U_NSC_StateID" });
                    ////if (pLineUDFs?.ContainsKey("") == true)
                    ////{
                    ////    foreach (var kvp in pLineUDFs[""])
                    ////    {
                    ////        oDoc.Lines.UserFields.Fields.Item(kvp.Key).Value = kvp.Value;
                    ////    }
                    ////}

                    int i = 0;
                    if (!string.IsNullOrEmpty(pBatch) && pSN == null)
                    {
                        //Standard Batch Item
                        //oDoc.Lines.BatchNumbers.Add();
                        oDoc.Lines.BatchNumbers.BatchNumber = pBatch;
                        oDoc.Lines.BatchNumbers.Quantity = pActualQty;
                        oDoc.Lines.BatchNumbers.InternalSerialNumber = prodBatchID; // THIS IS "Lot Number" / "Batch Attribute 2"

                        //StateIF
                        if (!string.IsNullOrEmpty(pStateID)) oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = pStateID;
                        //Check for User defined fields
                        if (pUserDefinedFields != null) NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.BatchNumbers, pUserDefinedFields, new string[2] { "U_NSC_StateID", "U_NSC_QuarState" });

                        // set the Harvest UDFs
                        if (pHarvestUDFs != null)
                        {
                            for (var j = 0; j < pHarvestUDFs.GetLength(0); j++)
                            {
                                var typ = oDoc.Lines.BatchNumbers.UserFields.Fields.Item(pHarvestUDFs[j, 0]).Type;
                                oDoc.Lines.BatchNumbers.UserFields.Fields.Item(pHarvestUDFs[j, 0]).Value = pHarvestUDFs[j, 1];
                            }
                        }

                        if (pBins != null && pBins.Rows.Count > 0)
                        {
                            // serial number is in same table as bins so we will have to set the batch bumber
                            int j = 0;
                            foreach (System.Data.DataRow r in pBins.Rows)
                            {
                                //Check for User defined fields
                                if (pUserDefinedFields != null) NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.BatchNumbers, pUserDefinedFields, new string[2] { "U_NSC_StateID", "U_NSC_QuarState" });
                                if (j > 0) oDoc.Lines.BinAllocations.Add();
                                //oDoc.Lines.WarehouseCode                = r["BinAbs"]
                                oDoc.Lines.BinAllocations.BinAbsEntry = Convert.ToInt32(r["BinAbs"]);
                                oDoc.Lines.BinAllocations.Quantity = Convert.ToDouble(r["Quantity"]);
                                oDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = j;
                                j++;
                            }
                        }
                        ////{
                        ////    foreach (KeyValuePair<string, string> pair in pUserDefinedFields)
                        ////        oDoc.Lines.BatchNumbers.UserFields.Fields.Item(pair.Key).Value = pair.Value;
                        ////}
                    }
                    else
                    {
                        if (pSN != null && pSN.Count() > 0)
                        {
                            // serial number is not in same table as bins
                            int SNCount = pSN.Count();
                            foreach (var s in pSN)
                            {
                                if (i > 0) oDoc.Lines.SerialNumbers.Add();
                                oDoc.Lines.SerialNumbers.InternalSerialNumber = s;
                                oDoc.Lines.SerialNumbers.Quantity = 1;
                                oDoc.Lines.SerialNumbers.BaseLineNumber = 0;
                                oDoc.Lines.SerialNumbers.BatchID = prodBatchID; // THIS IS "Lot Number" 

                                //Check for User defined fields
                                if (pUserDefinedFields != null) NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.SerialNumbers, pUserDefinedFields, new string[2] { "U_NSC_StateID", "U_NSC_QuarState" });
                                if (pBins != null)
                                {
                                    if (i > 0) oDoc.Lines.BinAllocations.Add();
                                    oDoc.Lines.BinAllocations.BinAbsEntry = Convert.ToInt32(pBins.Rows[i]["BinAbs"]);
                                    oDoc.Lines.BinAllocations.Quantity = 1;
                                    oDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = i;
                                }
                                i++;
                            }
                        }
                        else if (pBins != null && pBins.Rows.Count > 0)
                        {
                            // serial number is in same table as bins
                            foreach (System.Data.DataRow r in pBins.Rows)
                            {
                                if (i > 0) oDoc.Lines.SerialNumbers.Add();
                                oDoc.Lines.SerialNumbers.InternalSerialNumber = r["Serial Number"].ToString();
                                oDoc.Lines.SerialNumbers.Quantity = 1;
                                //Check for User defined fields
                                if (pUserDefinedFields != null) NSC_DI.UTIL.UDO.CopyUDFs(oDoc.Lines.SerialNumbers, pUserDefinedFields, new string[2] { "U_NSC_StateID", "U_NSC_QuarState" });
                                if (i > 0) oDoc.Lines.BinAllocations.Add();
                                oDoc.Lines.BinAllocations.BinAbsEntry = Convert.ToInt32(r["BinAbs"]);
                                oDoc.Lines.BinAllocations.Quantity = 1;
                                oDoc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = i;
                                i++;
                            }
                        }
                    }
                    //oDoc.Lines.Add();

                }

                #region ByProducts
                //Check For ByProducts
                string SQL = null;
                SQL = String.Format(@"SELECT WOR1.LineNum,WOR1.BaseQty,OWOR.PlannedQty,OITM.ManBtchNum,WOR1.IssueType, Oitm.ItmsGrpCod,wor1.ItemCode, WOR1.wareHouse FROM WOR1
                JOIN OITM ON WOR1.ItemCode = OITM.ItemCode join OWOR on WOR1.DocEntry = OWOR.DocEntry
                 WHERE WOR1.DocEntry = {0} AND OWOR.Status = 'R' and WOR1.BaseQty < 0", pPdO);
                var addToDest = false;
                oRecordSet = (SAPbobsCOM.Recordset)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordSet = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(SQL);
                if (oRecordSet.RecordCount > 0)
                {
                    //By Products Exist
                    for (int i = 0; i <= oRecordSet.RecordCount - 1; i++)
                    {
                        if (i == 0)
                        {
                            oRecordSet.MoveFirst();
                        }
                        else
                        {
                            oRecordSet.MoveNext();
                        }

                        //*****THis is for a receipt from production ****
                        if (oRecordSet.Fields.Item(1).Value < 0)
                        {
                            //byproduct

                            if (pLineUDFs?.ContainsKey(oRecordSet.Fields.Item(0).Value) == true)
                            {
                                foreach (var kvp in pLineUDFs[oRecordSet.Fields.Item(0).Value])
                                {
                                    oDoc.Lines.UserFields.Fields.Item(kvp.Key).Value = kvp.Value;
                                }
                            }

                            //1. determine variance between planned and actual Qty(s). Actual should always be equal or less than planned
                            double dblVariance = oRecordSet.Fields.Item(2).Value - pActualQty;
                            string pItemProp = "";
                            if (dblVariance > 0 || pTotalWaste > 0.0)
                            {
                                //Process by "Process Type"
                                switch (pProcess)
                                {
                                    //Propogation 
                                    case ("Clone"):
                                        pItemProp = "Clone";
                                        if (NSC_DI.SAP.Items.Property.Get(oRecordSet.Fields.Item("ItemCode").Value, pItemProp) == "Y")
                                        //if (oRecordSet.Fields.Item(5).Value == 103)
                                        {
                                            // Erics other new stuff, checks to see if the default line instantiated by SAP utilized and then adds a new line if it is                                                                                      
                                            if (oDoc.Lines.BaseEntry > 0)
                                                oDoc.Lines.Add();
                                            //byproduct was created 
                                            oDoc.Lines.BaseType = 0;
                                            oDoc.Lines.BaseEntry = pPdO;
                                            oDoc.Lines.BaseLine = oRecordSet.Fields.Item(0).Value;
                                            //oDoc.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                                            oDoc.Lines.Quantity = dblVariance * (oRecordSet.Fields.Item(1).Value * -1);
                                            oDoc.Lines.WarehouseCode = oRecordSet.Fields.Item("wareHouse").Value as string;

                                            NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                                            if (pCropID != null && strCropID.Length > 0)
                                            {
                                                oDoc.Lines.ProjectCode = strCropID;
                                            }
                                            //Check for batch Managed Items. (This will currently work for only one batch managed item. )
                                            if (oRecordSet.Fields.Item(3).Value == "Y" && !string.IsNullOrEmpty(pBatch))
                                            {
                                                //Destruction Batch (Need to pull the batch Number for the clones on the issued to production batch for clones)
                                                string strSql_Holder = string.Format(@"SELECT
                                                                        T2.[BatchNum]
                                                                        FROM
                                                                        OWOR T0  inner JOIN WOR1 T1 ON T0.DocEntry = T1.DocEntry inner join IBT1 T2 on T2.[BsDocEntry]  = t0.docentry and T2.[BsDocLine]  =  T1.[LineNum]
                                                                        WHERE
                                                                        T2.[BaseType]  = 60 
                                                                        and T0.DocEntry= {0} and T2.ItemCode= '{1}'", pPdO.ToString(), oRecordSet.Fields.Item("ItemCode").Value);
                                                string IssuedCloneBatch = NSC_DI.UTIL.SQL.GetValue<string>(strSql_Holder);
                                                oDoc.Lines.BatchNumbers.BatchNumber = IssuedCloneBatch;
                                                oDoc.Lines.BatchNumbers.Quantity = dblVariance * (oRecordSet.Fields.Item("BaseQty").Value * -1);
                                                oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_QuarState").Value = Globals.QuarantineStates.DEST.ToString();
                                                //ToDo: Set the State ID of the batch
                                                addToDest |= (oDoc.Lines.WarehouseCode == "9999");

                                            }

                                        }
                                        break;
                                    case ("Seed"):
                                        pItemProp = "Seed";
                                        if (NSC_DI.SAP.Items.Property.Get(oRecordSet.Fields.Item("ItemCode").Value, pItemProp) == "Y")
                                        //if (oRecordSet.Fields.Item(5).Value == 101)
                                        {
                                            // more eric new stuff, refernce case clone
                                            if (oDoc.Lines.BaseEntry > 0)
                                                oDoc.Lines.Add();
                                            oDoc.Lines.BaseType = 0;
                                            oDoc.Lines.BaseEntry = pPdO;
                                            oDoc.Lines.BaseLine = oRecordSet.Fields.Item(0).Value;
                                            //oDoc.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                                            oDoc.Lines.Quantity = dblVariance * (oRecordSet.Fields.Item(1).Value * -1);
                                            if (strCropID.Length > 0)
                                            {
                                                oDoc.Lines.ProjectCode = strCropID;
                                            }
                                            string strSQL_Warehouse = string.Format(@"SELECT wor1.wareHouse
                                    FROM WOR1
                                    JOIN OITM ON WOR1.ItemCode = OITM.ItemCode join OWOR on WOR1.DocEntry = OWOR.DocEntry
                                    WHERE WOR1.DocEntry = {0} AND OWOR.Status = 'R' and WOR1.BaseQty < 0", pPdO);
                                            oDoc.Lines.WarehouseCode = NSC_DI.UTIL.SQL.GetValue<string>(strSQL_Warehouse, null);

                                            NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                                            //Check for batch Managed Items. (This will currently work for only one batch managed item. )
                                            if (oRecordSet.Fields.Item(3).Value == "Y" && !string.IsNullOrEmpty(pBatch))
                                            {

                                                //Destruction Batch (Need to pull the batch Number for the Seeds on the issued to production batch for Seeds)
                                                string strSql_Holder = string.Format(@"SELECT
                                                                        T2.[BatchNum]
                                                                        FROM
                                                                        OWOR T0  inner JOIN WOR1 T1 ON T0.DocEntry = T1.DocEntry inner join IBT1 T2 on T2.[BsDocEntry]  = t0.docentry and T2.[BsDocLine]  =  T1.[LineNum]
                                                                        WHERE
                                                                        T2.[BaseType]  = 60 
                                                                        and T0.DocEntry= {0} and T2.ItemCode= '{1}'", pPdO.ToString(), oRecordSet.Fields.Item("ItemCode").Value);
                                                string IssuedSeedBatch = NSC_DI.UTIL.SQL.GetValue<string>(strSql_Holder);
                                                oDoc.Lines.BatchNumbers.BatchNumber = IssuedSeedBatch;
                                                oDoc.Lines.BatchNumbers.Quantity = dblVariance * (oRecordSet.Fields.Item(1).Value * -1);
                                                oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_QuarState").Value = Globals.QuarantineStates.DEST.ToString();
                                                addToDest |= (oDoc.Lines.WarehouseCode == "9999");

                                            }

                                        }
                                        break;
                                    case ("Tissue"):
                                        pItemProp = "Plant Tissue";
                                        if (NSC_DI.SAP.Items.Property.Get(oRecordSet.Fields.Item("ItemCode").Value, pItemProp) == "Y")
                                        //if (oRecordSet.Fields.Item(5).Value == 102)
                                        {
                                            // More eric new stuff reference case clone
                                            if (oDoc.Lines.BaseEntry > 0)
                                                oDoc.Lines.Add();
                                            oDoc.Lines.BaseType = 0;
                                            oDoc.Lines.BaseEntry = pPdO;
                                            //oDoc.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                                            oDoc.Lines.Quantity = dblVariance * (oRecordSet.Fields.Item(1).Value * -1);
                                            if (strCropID.Length > 0)
                                            {
                                                oDoc.Lines.ProjectCode = strCropID;
                                            }
                                            string strSQL_Warehouse = string.Format(@"SELECT wor1.wareHouse
                                    FROM WOR1
                                    JOIN OITM ON WOR1.ItemCode = OITM.ItemCode join OWOR on WOR1.DocEntry = OWOR.DocEntry
                                    WHERE WOR1.DocEntry = {0} AND OWOR.Status = 'R' and WOR1.BaseQty < 0", pPdO);
                                            oDoc.Lines.WarehouseCode = NSC_DI.UTIL.SQL.GetValue<string>(strSQL_Warehouse, null);

                                            NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                                            //Check for batch Managed Items. (This will currently work for only one batch managed item. )
                                            if (oRecordSet.Fields.Item(3).Value == "Y" && !string.IsNullOrEmpty(pBatch))
                                            {

                                                //Destruction Batch (Need to pull the batch Number for the Seeds on the issued to production batch for Seeds)
                                                string strSql_Holder = string.Format(@"SELECT
                                                                        T2.[BatchNum]
                                                                        FROM
                                                                        OWOR T0  inner JOIN WOR1 T1 ON T0.DocEntry = T1.DocEntry inner join IBT1 T2 on T2.[BsDocEntry]  = t0.docentry and T2.[BsDocLine]  =  T1.[LineNum]
                                                                        WHERE
                                                                        T2.[BaseType]  = 60 
                                                                        and T0.DocEntry= {0} and T2.ItemCode= '{1}'", pPdO.ToString(), oRecordSet.Fields.Item("ItemCode").Value);
                                                string IssuedTissueBatch = NSC_DI.UTIL.SQL.GetValue<string>(strSql_Holder);
                                                oDoc.Lines.BatchNumbers.BatchNumber = pBatch;
                                                oDoc.Lines.BatchNumbers.Quantity = dblVariance * (oRecordSet.Fields.Item(1).Value * -1);
                                                oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_QuarState").Value = Globals.QuarantineStates.DEST.ToString();
                                                addToDest |= (oDoc.Lines.WarehouseCode == "9999");

                                            }

                                        }
                                        break;
                                    //Harvest Plant -->Wet Cannabis (Waste)
                                    case ("HARVEST"):
                                        pItemProp = "Cannabis Waste";
                                        if (NSC_DI.SAP.Items.Property.Get(oRecordSet.Fields.Item("ItemCode").Value, pItemProp) == "Y")
                                        //if (oRecordSet.Fields.Item(5).Value == 103)
                                        {
                                            // More eric new stuff reference case clone
                                            if (oDoc.Lines.BaseEntry > 0)
                                                oDoc.Lines.Add();
                                            //byproduct was created 
                                            oDoc.Lines.BaseType = 0;
                                            oDoc.Lines.BaseEntry = pPdO;
                                            oDoc.Lines.BaseLine = oRecordSet.Fields.Item(0).Value;
                                            //oDoc.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                                            oDoc.Lines.Quantity = pTotalWaste;

                                            oDoc.Lines.WarehouseCode = oRecordSet.Fields.Item("wareHouse").Value as string;
                                            NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                                            if (strCropID.Length > 0)
                                            {
                                                oDoc.Lines.ProjectCode = strCropID;
                                            }

                                            //Check for batch Managed Items. (This will currently work for only one batch managed item. )
                                            if (oRecordSet.Fields.Item(3).Value == "Y" && !string.IsNullOrEmpty(pBatch))
                                            {

                                                //Destruction Batch
                                                oDoc.Lines.BatchNumbers.BatchNumber = pWasteBatch;
                                                oDoc.Lines.BatchNumbers.Quantity = pTotalWaste;
                                                oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_QuarState").Value = "DEST";
                                                oDoc.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_TimerQuar").Value = NSC_DI.UTIL.Dates.CurrentTimeStamp.ToString();
                                                //ToDo: Set the State ID of the batch - Currently it is set to the state ID of the harvest.
                                                oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = pStateID;
                                                addToDest |= (oDoc.Lines.WarehouseCode == "9999");

                                            }

                                        }
                                        break;
                                    //Wet Cannabis-->Dry Cannabis
                                    case ("Dry"):

                                        break;

                                    //Dry Cannabis --> Cured Unbatched
                                    case ("Curing"):

                                        break;
                                    //Default - Non byproduct line item  
                                    default:
                                        // more eric new stuff, refrence case clone
                                        if (oDoc.Lines.BaseEntry > 0)
                                            oDoc.Lines.Add();
                                        oDoc.Lines.SetCurrentLine(i);
                                        oDoc.Lines.BaseEntry = pPdO;
                                        oDoc.Lines.BaseLine = oRecordSet.Fields.Item(0).Value; //ItemLine;
                                        oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                                        oDoc.Lines.BatchNumbers.Quantity = pActualQty * oRecordSet.Fields.Item(1).Value;
                                        oDoc.Lines.BatchNumbers.BatchNumber = pBatch;
                                        if (strCropID.Length > 0)
                                        {
                                            oDoc.Lines.ProjectCode = strCropID;
                                        }
                                        break;
                                }

                                // add destruction quarantine state and time stamp if item is assigned to the destruction warehouse

                            }
                        }

                    }
                }
                //oDoc.Lines.Add();
                #endregion



                int returnCode = oDoc.Add();
                var s2 = Globals.oCompany.GetLastErrorDescription();
                if (addToDest)
                {
                    if (pWasteBatch.Length > 0)
                    {
                        //No Need for this because it is waste and automatically goes to destruction 
                        //BatchItems.ChangeQuarantineStatus(pWasteBatch, Globals.QuarantineStates.DEST);
                    }
                    //else
                    //{
                    //BatchItems.ChangeQuarantineStatus(pBatch, Globals.QuarantineStates.DEST);
                    //}
                }


                if (returnCode == 0)
                {
                    Key = B1Object.GetLastAdded();
                }
                else
                {
                    B1Exception.RaiseException();
                }

                return Key;
            }
            catch (B1Exception ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDoc);
                NSC_DI.UTIL.SQL.KillObject(oRecordSet);
                GC.Collect();
            }
        }

        public static int ReceiveParent(int productionOrderKey, DateTime documentDate, string pBatch = "", IEnumerable<string> pSN = null, double? pQty = null, string pProdBatchID = null)
        {
            SAPbobsCOM.Documents oDoc = null;
            SAPbobsCOM.ProductionOrders oPdO = null;

            try
            {
                int NewlyCreatedKey = 0;

                oPdO = NSC_DI.SAP.ProductionOrder.OpenPdO(productionOrderKey);

                // Prepare a Goods Receipt document
                oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                // Add items to the Goods Receipt document
                oDoc.DocDate = documentDate;
                oDoc.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items;

                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null)
                    oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                oDoc.Lines.BaseEntry = productionOrderKey;
                oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                //oDoc.Lines.TransactionType  = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                oDoc.Lines.Quantity = oPdO.PlannedQuantity;
                if (pQty != null) oDoc.Lines.Quantity = Convert.ToDouble(pQty);
                oDoc.Lines.WarehouseCode = oPdO.Warehouse;

                // set the branch
                var br = NSC_DI.SAP.Branch.Get(oPdO.Warehouse);
                if (br > 0) oDoc.BPL_IDAssignedToInvoice = br;

                NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                // Determine if item is batch managed
                if (NSC_DI.SAP.Items.GetField<string>(oPdO.ItemNo, "ManBtchNum") == "Y")
                {
                    oDoc.Lines.BatchNumbers.BatchNumber = pBatch;
                    oDoc.Lines.BatchNumbers.Quantity = oDoc.Lines.Quantity;//oPdO.PlannedQuantity;
                    //oDoc.Lines.BatchNumbers.Add();
                }

                if (NSC_DI.SAP.Items.GetField<string>(oPdO.ItemNo, "ManSerNum") == "Y")
                {
                    foreach (var serialNumber in pSN)
                    {
                        if (oDoc.Lines.SerialNumbers.InternalSerialNumber != "") oDoc.Lines.SerialNumbers.Add();
                        oDoc.Lines.SerialNumbers.InternalSerialNumber = serialNumber;
                        oDoc.Lines.SerialNumbers.Quantity = 1;
                    }
                }

                //oDoc.Lines.Add();

                // Attempt to add the new item to the database
                int ResponseCode = oDoc.Add();

                // Attempt to add the goods issued document
                if (ResponseCode != 0)
                {
                    throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }

                string newCode = null;

                // Get the newly created object's code
                Globals.oCompany.GetNewObjectCode(out newCode);

                if (newCode != null) NewlyCreatedKey = Convert.ToInt32(newCode);

                return NewlyCreatedKey;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDoc);
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                GC.Collect();
            }
        }

        public static int ReceiveByProducts(int productionOrderKey, System.Data.DataTable pDT_ByProd, SAPbobsCOM.BatchNumberDetail oBatchDetail = null, string pExpDate = null)
        {
            SAPbobsCOM.Documents oDoc = null;
            // oPdO = NSC_DI.SAP.ProductionOrder.OpenPdO(intDocNum);
            try
            {
                if (pDT_ByProd.Rows.Count < 1) return 0;
                int NewlyCreatedKey = 0;

                // Prepare a Goods Receipt document
                oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                // Add items to the Goods Receipt document
                oDoc.DocDate = DateTime.Now.Date;
                oDoc.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items;

                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null)
                    oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                // set the Branch
                var wh = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Warehouse FROM OWOR WHERE DocEntry = {productionOrderKey}", null);
                var br = NSC_DI.SAP.Branch.Get(wh);
                if (br > 0) oDoc.BPL_IDAssignedToInvoice = br;


                //oDoc.Lines.TransactionType   = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                //oDoc.Lines.Quantity = GI_Line.Quantity;
                // oDoc.Lines.WarehouseCode = GI_Line.WarehouseCode;

                // delete 20201231
                //Inter-Company Check 
                //string InterCoSubCode = NSC_DI.UTIL.SQL.GetValue<string>($@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 where T0.WhsCode = '{wh}'");
                //string InterCo = NSC_DI.UTIL.SQL.GetValue<string>("select T0.U_Value from dbo.[@NSC_SETTINGS] T0 where T0.Name = 'Inter-Company'");


                for (var i = 0; i <= pDT_ByProd.Rows.Count - 1; i++)
                {
                    string ByP_ItemCode = null;
                    string ByP_ItemName = null;
                    var ByP_Quantity = 0.0;
                    string strNextBatchSerialNumber = null;
                    int LineNum = 0;
                    string strWarehouse = null;
                    string strStateID = null;

                    ByP_ItemCode = pDT_ByProd.Rows[i]["ItemCode"].ToString();
                    ByP_ItemName = pDT_ByProd.Rows[i]["ItemName"].ToString();
                    ByP_Quantity = Convert.ToDouble(pDT_ByProd.Rows[i]["PlannedQty"]);
                    LineNum = Convert.ToInt16(pDT_ByProd.Rows[i]["LineNum"]);
                    strWarehouse = pDT_ByProd.Rows[i]["wareHouse"].ToString();
                    strNextBatchSerialNumber = pDT_ByProd.Rows[i]["BatchID"].ToString();
                    strNextBatchSerialNumber = NSC_DI.SAP.BatchItems.NextBatch(ByP_ItemCode);
                    //try
                    //{
                    //    //Inter-Company Check 
                    //    if (InterCo == "Y" && InterCoSubCode.Trim().Length > 0)
                    //    {
                    //        strNextBatchSerialNumber = InterCoSubCode.Trim() + "-" + strNextBatchSerialNumber;
                    //    }

                    //}
                    //catch
                    //{
                    //    throw new Exception(NSC_DI.UTIL.Message.Format("Inter-Company Error: Please check your settings (Y or N) and make sure you have a subsidiary code on your warehouse."));
                    //}

                    strStateID = pDT_ByProd.Rows[i]["StateID"].ToString();

                    if (ByP_Quantity == 0) continue;

                    if (oDoc.Lines.BaseEntry != 0) oDoc.Lines.Add();

                    oDoc.Lines.BaseEntry = productionOrderKey;
                    oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                    oDoc.Lines.SetCurrentLine(i);
                    oDoc.Lines.BaseLine = LineNum;
                    oDoc.Lines.WarehouseCode = strWarehouse;

                    NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                    if (ByP_Quantity < 0) ByP_Quantity = ByP_Quantity * -1;
                    oDoc.Lines.Quantity = ByP_Quantity;

                    // Determine if item is batch managed
                    if (NSC_DI.SAP.Items.GetField<string>(ByP_ItemCode, "ManBtchNum") == "Y")
                    {
                        // Determine if item is batch managed or Serial 
                        oDoc.Lines.BatchNumbers.BatchNumber = strNextBatchSerialNumber;                        
                        oDoc.Lines.BatchNumbers.Quantity = ByP_Quantity;
                        if (!string.IsNullOrEmpty(pExpDate))
                            oDoc.Lines.BatchNumbers.ExpiryDate = NSC_DI.UTIL.Dates.FromUI(pExpDate);
                        if (strStateID.Trim().Length > 0)
                        {
                            oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = strStateID;
                        }
                        if (oBatchDetail != null)                       
                        {
                            for (var j = 0; j < oDoc.Lines.BatchNumbers.UserFields.Fields.Count; j++)
                            {
                                var s = oDoc.Lines.BatchNumbers.UserFields.Fields.Item(j).Name;
                                try
                                {
                                    oDoc.Lines.BatchNumbers.UserFields.Fields.Item(j).Value = oBatchDetail.UserFields.Item(s).Value;
                                }
                                catch { }
                            }
                        }

                        oDoc.Lines.BatchNumbers.Add();
                    }

                    if (NSC_DI.SAP.Items.GetField<string>(ByP_ItemCode, "ManSerNum") == "Y")
                    {
                        //ToDO:

                        //foreach (var serialNumber in pSN)
                        //{
                        //    if (oDoc.Lines.SerialNumbers.InternalSerialNumber != "") oDoc.Lines.SerialNumbers.Add();
                        //    oDoc.Lines.SerialNumbers.InternalSerialNumber = serialNumber;
                        //    oDoc.Lines.SerialNumbers.Quantity = 1;
                        //}
                    }
                }




                // Attempt to add the new item to the database
                int ResponseCode = oDoc.Add();

                // Attempt to add the goods issued document
                if (ResponseCode != 0)
                {
                    throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }

                string newCode = null;

                // Get the newly created object's code
                Globals.oCompany.GetNewObjectCode(out newCode);

                if (newCode != null) NewlyCreatedKey = Convert.ToInt32(newCode);

                return NewlyCreatedKey;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDoc);
            }
        }

        public static double ReceiveByProducts(int productionOrderKey, SAPbobsCOM.Documents oDoc, System.Data.DataTable pDT_ByProd, SAPbobsCOM.BatchNumberDetail oBatchDetail = null)
        {
            try
            {
                if (pDT_ByProd.Rows.Count < 1) return 0.00;
                double dblTotalReceivedQty = 0.00;

                // set the Branch
                var wh = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Warehouse FROM OWOR WHERE DocEntry = {productionOrderKey}", null);
                var br = NSC_DI.SAP.Branch.Get(wh);
                if (br > 0) oDoc.BPL_IDAssignedToInvoice = br;

                for (var i = 0; i < pDT_ByProd.Rows.Count; i++)
                {
                    string ByP_ItemCode = null;
                    string ByP_ItemName = null;
                    var ByP_Quantity = 0.0;
                    string strNextBatchSerialNumber = null;
                    int LineNum = 0;
                    string strWarehouse = null;
                    string strStateID = null;

                    ByP_ItemCode = pDT_ByProd.Rows[i]["ItemCode"].ToString();
                    ByP_ItemName = pDT_ByProd.Rows[i]["ItemName"].ToString();
                    ByP_Quantity = Math.Abs(Convert.ToDouble(pDT_ByProd.Rows[i]["PlannedQty"]));
                    LineNum = Convert.ToInt16(pDT_ByProd.Rows[i]["LineNum"]);
                    strWarehouse = pDT_ByProd.Rows[i]["wareHouse"].ToString();
                    strNextBatchSerialNumber = pDT_ByProd.Rows[i]["BatchID"].ToString();
                    strStateID = pDT_ByProd.Rows[i]["StateID"].ToString();

                    if (ByP_Quantity == 0) continue;
                    if (oDoc.Lines.BaseEntry != 0) oDoc.Lines.Add();

                    oDoc.Lines.BaseEntry = productionOrderKey;
                    oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                    oDoc.Lines.BaseLine = LineNum;
                    oDoc.Lines.WarehouseCode = strWarehouse;

                    NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                    oDoc.Lines.Quantity = ByP_Quantity;
                    dblTotalReceivedQty += ByP_Quantity;

                    // Determine if item is batch managed
                    if (NSC_DI.SAP.Items.GetField<string>(ByP_ItemCode, "ManBtchNum") == "Y")
                    {
                        // Determine if item is batch managed or Serial 
                        oDoc.Lines.BatchNumbers.BatchNumber = strNextBatchSerialNumber;
                        oDoc.Lines.BatchNumbers.Quantity = ByP_Quantity;
                        if (strStateID.Trim().Length > 0)
                        {
                            oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = strStateID;
                        }    
                        
                        if (oBatchDetail != null)
                        {
                            for (var j = 0; j < oDoc.Lines.BatchNumbers.UserFields.Fields.Count; j++)
                            {
                                var s = oDoc.Lines.BatchNumbers.UserFields.Fields.Item(j).Name;
                                try
                                {
                                    oDoc.Lines.BatchNumbers.UserFields.Fields.Item(j).Value = oBatchDetail.UserFields.Item(s).Value;
                                }
                                catch { }
                            }                                                    
                        }                        
                    }

                    if (NSC_DI.SAP.Items.GetField<string>(ByP_ItemCode, "ManSerNum") == "Y")
                    {
                        //ToDO:

                        //foreach (var serialNumber in pSN)
                        //{
                        //    if (oDoc.Lines.SerialNumbers.InternalSerialNumber != "") oDoc.Lines.SerialNumbers.Add();
                        //    oDoc.Lines.SerialNumbers.InternalSerialNumber = serialNumber;
                        //    oDoc.Lines.SerialNumbers.Quantity = 1;
                        //}
                    }                   
                }
               

                return dblTotalReceivedQty;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static int ReceiveParent(int productionOrderKey, DateTime documentDate, GoodsReceipt.GoodsReceipt_Lines GI_Line)
        {
            SAPbobsCOM.Documents oDoc = null;

            try
            {
                int NewlyCreatedKey = 0;

                // Prepare a Goods Receipt document
                oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                // Add items to the Goods Receipt document
                oDoc.DocDate = documentDate;
                oDoc.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items;

                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null)
                    oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                // set the branch
                var wh = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Warehouse FROM OWOR WHERE DocEntry = {productionOrderKey}", null);
                var br = NSC_DI.SAP.Branch.Get(wh);
                if (br > 0) oDoc.BPL_IDAssignedToInvoice = br;

                oDoc.Lines.BaseEntry = productionOrderKey;
                oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                //oDoc.Lines.TransactionType   = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                oDoc.Lines.Quantity = GI_Line.Quantity;
                oDoc.Lines.WarehouseCode = GI_Line.WarehouseCode;

                NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                // Determine if item is batch managed
                if (GI_Line.ListOfBatchLineItems != null && GI_Line.ListOfBatchLineItems.Count > 0)
                {
                    foreach (var batchLineItem in GI_Line.ListOfBatchLineItems)
                    {
                        oDoc.Lines.BatchNumbers.BatchNumber = batchLineItem.BatchNumber;
                        oDoc.Lines.BatchNumbers.Quantity = batchLineItem.Quantity;
                        oDoc.Lines.BatchNumbers.ManufacturerSerialNumber = batchLineItem.ManufacturerSerialNumber;
                        oDoc.Lines.BatchNumbers.InternalSerialNumber = batchLineItem?.LotNumber;
                        if (batchLineItem.ExpDate != DateTime.MinValue)
                            oDoc.Lines.BatchNumbers.ExpiryDate = batchLineItem.ExpDate;                        

                        // If user defined fields were passed to the line item
                        if (batchLineItem.UserDefinedFields != null)
                        {
                            // Add each user defined field to the line item of the "Receipt from Production" document. These are specific fields from user input.
                            foreach (string key in batchLineItem.UserDefinedFields.Keys)
                            {
                                oDoc.Lines.BatchNumbers.UserFields.Fields.Item(key).Value = batchLineItem.UserDefinedFields[key].ToString();
                            }
                        }

                        oDoc.Lines.BatchNumbers.Add();
                    }
                }

                if (GI_Line.SerialNumbers != null)
                {
                    foreach (var serialNumber in GI_Line.SerialNumbers)
                    {
                        if (oDoc.Lines.SerialNumbers.InternalSerialNumber != "") oDoc.Lines.SerialNumbers.Add();
                        oDoc.Lines.SerialNumbers.InternalSerialNumber = serialNumber;
                        oDoc.Lines.SerialNumbers.Quantity = 1;

                        // If user defined fields were passed to the line item
                        if (GI_Line.UserDefinedFields != null)
                        {
                            // Add each user defined field to the line item of the "Receipt from Production" document. These are specific fields from user input.
                            foreach (string key in GI_Line.UserDefinedFields.Keys)
                            {
                                oDoc.Lines.SerialNumbers.UserFields.Fields.Item(key).Value = GI_Line.UserDefinedFields[key].ToString();
                            }
                        }
                    }
                }

                oDoc.Lines.Add();



                // Attempt to add the new item to the database
                int ResponseCode = oDoc.Add();

                // Attempt to add the goods issued document
                if (ResponseCode != 0)
                {
                    throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }

                string newCode = null;

                // Get the newly created object's code
                Globals.oCompany.GetNewObjectCode(out newCode);

                if (newCode != null) NewlyCreatedKey = Convert.ToInt32(newCode);

                return NewlyCreatedKey;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDoc);
            }
        }

        public static int GetLineNum(int pPoNum, string pItemPropertyName)
        {
            // Gets the line number for the production order and property.

            try
            {
                var property = NSC_DI.SAP.Items.Property.GetFieldName(pItemPropertyName);
                var qry = $"SELECT LineNum FROM WOR1 INNER JOIN OITM ON WOR1.ItemCode = OITM.ItemCode WHERE WOR1.DocEntry = {pPoNum} AND OITM." + property + " = 'Y'";

                return UTIL.SQL.GetValue<int>(qry, -1);
            }
            catch (Exception ex)
            {

                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static int GetVisOrder(int pPoNum, string pItemPropertyName)
        {
            // Gets the line number for the production order and property.

            try
            {
                var property = NSC_DI.SAP.Items.Property.GetFieldName(pItemPropertyName);
                var qry = $"SELECT VisOrder FROM WOR1 INNER JOIN OITM ON WOR1.ItemCode = OITM.ItemCode WHERE WOR1.DocEntry = {pPoNum} AND OITM." + property + " = 'Y'";

                return UTIL.SQL.GetValue<int>(qry, -1);
            }
            catch (Exception ex)
            {

                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static int UpdateGEN_CropProdLine(int pkey, string pItemCode, double pQty, string pWhrs, string pFinCropID)
        {
            try
            {
                // Prepare a new production order object
                SAPbobsCOM.ProductionOrders PO = (SAPbobsCOM.ProductionOrders)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);

                // Attempt to get the production order by its key
                if (PO.GetByKey(pkey))
                {
                    // Change the status of the production order
                    PO.Lines.Add();
                    PO.Lines.ItemNo = pItemCode;
                    PO.Lines.PlannedQuantity = pQty;
                    PO.Lines.Warehouse = pWhrs;
                    PO.Lines.Project = pFinCropID;
                    PO.Lines.ProductionOrderIssueType = SAPbobsCOM.BoIssueMethod.im_Manual;
                    //Distribution Rule 
                    if (NSC_DI.UTIL.Options.Value.GetSingle("Copy Distribution Rules", 1, "Copy Distribution Rules / Dimensions.", "Y") == "Y")
                    {
                        //I commented out the other rules as I was getting an error. 
                        var oRS = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery($"SELECT U_NSC_OcrCode1, U_NSC_OcrCode2, U_NSC_OcrCode3, U_NSC_OcrCode4, U_NSC_OcrCode5 FROM OWHS WHERE WhsCode = '{pWhrs}'");
                        PO.Lines.DistributionRule = oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                        //PO.Lines.DistributionRule2 = oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                        //PO.Lines.DistributionRule3= oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                        //PO.Lines.DistributionRule4 = oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                        //PO.DistributionRule5 = oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                    }


                    // Attempt to update the production order
                    int ReturnCode = PO.Update();

                    //Issue Components
                    SAPbobsCOM.Documents oDoc = null;
                    oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
                    oDoc.DocDate = DateTime.Now;
                    oDoc.DocDueDate = DateTime.Now;
                    oDoc.JournalMemo = "Issue for Production";

                    oDoc.Lines.BaseEntry = pkey;
                    oDoc.Lines.BaseLine = PO.Lines.Count - 1;
                    oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                    oDoc.Lines.BatchNumbers.Quantity = pQty;
                    //Batch
                    oDoc.Lines.BatchNumbers.BatchNumber = NSC_DI.SAP.BatchItems.NextBatch(pItemCode);
                    NSC_DI.SAP.Document.SetDefaultDistributionRules(oDoc.Lines);

                    int returnCode = oDoc.Add();



                    // If the update was successful
                    if (ReturnCode == 0 && returnCode == 0)

                    {


                        int NewlyCreatedKey = 0;
                        string newCode = null;

                        // Get the newly created object's code
                        Globals.oCompany.GetNewObjectCode(out newCode);

                        if (newCode != null) NewlyCreatedKey = Convert.ToInt32(newCode);

                        return NewlyCreatedKey;
                    }
                    // The update was not successful
                    else
                    {
                        B1Exception.RaiseException();
                        return -1;
                    }
                }
                // Could not find the production order
                else
                {
                    throw new B1Exception(UTIL.Message.Format("Could not find the production order given the key " + pkey.ToString()));
                }
            }
            catch (B1Exception ex) { throw ex; }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static int UpdateGEN_CropProdLine_Receipt(int pkey, string pItemCode, double pQty, string pWhrs, string pFinCropID)
        {
            try
            {
                // Prepare a new production order object
                SAPbobsCOM.ProductionOrders PO = (SAPbobsCOM.ProductionOrders)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);

                // Attempt to get the production order by its key
                if (PO.GetByKey(pkey))
                {
                    // Change the status of the production order
                    PO.Lines.Add();
                    PO.Lines.ItemNo = pItemCode;
                    PO.Lines.PlannedQuantity = -1 * pQty;//change to a byproduct since it is a receipt from production.
                    PO.Lines.Warehouse = pWhrs;
                    PO.Lines.Project = pFinCropID;
                    PO.Lines.ProductionOrderIssueType = SAPbobsCOM.BoIssueMethod.im_Manual;
                    if (NSC_DI.UTIL.Options.Value.GetSingle("Copy Distribution Rules", 1, "Copy Distribution Rules / Dimensions.", "Y") == "Y")
                    {
                        //I commented out the other rules as I was getting an error. 
                        var oRS = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery($"SELECT U_NSC_OcrCode1, U_NSC_OcrCode2, U_NSC_OcrCode3, U_NSC_OcrCode4, U_NSC_OcrCode5 FROM OWHS WHERE WhsCode = '{pWhrs}'");
                        PO.Lines.DistributionRule = oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                        //PO.Lines.DistributionRule2 = oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                        //PO.Lines.DistributionRule3= oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                        //PO.Lines.DistributionRule4 = oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                        //PO.DistributionRule5 = oRS.Fields.Item("U_NSC_OcrCode1").Value.ToString();
                    }

                    // Attempt to update the production order
                    int ReturnCode = PO.Update();

                    //Issue Components
                    SAPbobsCOM.Documents oDoc = null;
                    oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);
                    oDoc.DocDate = DateTime.Now;
                    oDoc.DocDueDate = DateTime.Now;
                    oDoc.JournalMemo = "Receipt for Production";

                    oDoc.Lines.BaseEntry = pkey;
                    oDoc.Lines.BaseLine = PO.Lines.Count - 1;
                    oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                    oDoc.Lines.BatchNumbers.Quantity = pQty;
                    //Batch
                    oDoc.Lines.BatchNumbers.BatchNumber = NSC_DI.SAP.BatchItems.NextBatch(pItemCode);

                    int returnCode = oDoc.Add();

                    // If the update was successful
                    if (ReturnCode == 0 && returnCode == 0)

                    {
                        int NewlyCreatedKey = 0;
                        string newCode = null;

                        // Get the newly created object's code
                        Globals.oCompany.GetNewObjectCode(out newCode);

                        if (newCode != null) NewlyCreatedKey = Convert.ToInt32(newCode);

                        return NewlyCreatedKey;
                    }
                    // The update was not successful
                    else
                    {
                        B1Exception.RaiseException();
                        return -1;
                    }
                }
                // Could not find the production order
                else
                {
                    throw new B1Exception(UTIL.Message.Format("Could not find the production order given the key " + pkey.ToString()));
                }
            }
            catch (B1Exception ex) { throw ex; }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }


        public static bool UpdateStatus(int key, SAPbobsCOM.BoProductionOrderStatusEnum status)
        {
            try
            {
                // Prepare a new production order object
                SAPbobsCOM.ProductionOrders PO = (SAPbobsCOM.ProductionOrders)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);

                // Attempt to get the production order by its key
                if (PO.GetByKey(key))
                {
                    // Change the status of the production order
                    PO.ProductionOrderStatus = status;

                    // Attempt to update the production order
                    int ReturnCode = PO.Update();

                    // If the update was successful
                    if (ReturnCode == 0)

                    {
                        return true;
                    }
                    // The update was not successful
                    else
                    {
                        B1Exception.RaiseException();
                        return false;
                    }
                }
                // Could not find the production order
                else
                {
                    throw new B1Exception(UTIL.Message.Format("Could not find the production order given the key " + key.ToString()));
                }
            }
            catch (B1Exception ex) { throw ex; }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void UpdateStatus(SAPbobsCOM.ProductionOrders oPO, SAPbobsCOM.BoProductionOrderStatusEnum status)
        {
            try
            {
                // Change the status of the production order
                oPO.ProductionOrderStatus = status;

                // Attempt to update the production order
                int ReturnCode = oPO.Update();

                // If the update was successful
                if (ReturnCode == 0) return;
                B1Exception.RaiseException();
            }
            catch (B1Exception ex) { throw ex; }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static string GetBatchNumberFromIssue(int pPoNum, string pItemCode = null, string pItemPropertyName = null)
        {
            // Gets the batch number from the first component item that has the Item Code or property name.
            // If neither is specified, find the first issue
            // Throws an exception if none found

            SAPbobsCOM.ProductionOrders oProdOrder = null;
            try
            {
                var qry = "";
                var batchNum = "";
                var lineNum = -1;

                if (string.IsNullOrEmpty(pItemCode?.Trim()))// Whitespace-Change (!NSC_DI.UTIL.Strings.Empty(pItemCode))
                {
                    if (string.IsNullOrEmpty(pItemPropertyName?.Trim()))// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pItemPropertyName))
                    {
                        // neither item code nor Property was specified.
                        qry = "";
                        qry += "SELECT TOP 1 IBT1.BatchNum" + Environment.NewLine;
                        qry += "  FROM IGE1" + Environment.NewLine;
                        qry += " INNER JOIN IBT1 ON IGE1.ItemCode = IBT1.ItemCode" + Environment.NewLine;
                        qry += $"   AND IBT1.BaseEntry = IGE1.DocEntry AND IBT1.BaseType = {(int)SAPbobsCOM.BoObjectTypes.oInventoryGenExit}";
                        qry += $" WHERE IGE1.BaseEntry = {pPoNum} AND IGE1.BaseType = {(int)SAPbobsCOM.BoObjectTypes.oProductionOrders}" + Environment.NewLine;
                        qry += " ORDER BY IGE1.DocEntry ASC" + Environment.NewLine;
                        batchNum = UTIL.SQL.GetValue<string>(qry, "");
                        if (string.IsNullOrEmpty(batchNum?.Trim())) throw new B1Exception($"Batch number not found for Production Order '{pPoNum}'");// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(batchNum))
                        return batchNum;
                    }
                    else
                    {
                        // item code was not specified, but Property was. So get it using the Property
                        var field = NSC_DI.SAP.Items.Property.GetFieldName(pItemPropertyName);
                        qry += "SELECT OITM.ItemCode" + Environment.NewLine;
                        qry += "  FROM OITM" + Environment.NewLine;
                        qry += " INNER JOIN IBT1 ON OITM.ItemCode = IBT1.ItemCode" + Environment.NewLine;
                        qry += $" WHERE IBT1.BsDocEntry = {pPoNum} AND IBT1.BsDocType = {(int)SAPbobsCOM.BoObjectTypes.oProductionOrders} AND OITM.{field} = 'Y'";
                        pItemCode = UTIL.SQL.GetValue<string>(qry);
                    }
                }

                qry = "";
                qry += "SELECT TOP 1 IBT1.BatchNum" + Environment.NewLine;
                qry += "  FROM IBT1" + Environment.NewLine;
                qry += $" WHERE IBT1.BsDocEntry = {pPoNum} AND IBT1.BsDocType = {(int)SAPbobsCOM.BoObjectTypes.oProductionOrders} AND IBT1.ItemCode = '{pItemCode}'";
                if (lineNum >= 0)
                    qry += $"   AND IBT1.BaseLinNum = {lineNum}";   // use BaseLinNum and not BsDocLine because BsDocLine is for the Production Order, but lineNum is for the Issue

                batchNum = UTIL.SQL.GetValue<string>(qry);

                if (string.IsNullOrEmpty(batchNum)) throw new B1Exception($"Batch number not found for Production Order '{pPoNum}'");

                return batchNum;
            }
            catch (Exception ex)
            {

                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(oProdOrder);
                GC.Collect();
            }
        }

        public static string GetBatchNumber(int pPoNum, string pItemPropertyName)
        {

            // ********************************************************************************* IS NOT COMPLETED ???
            // Gets the batch number from the first component item that has the property name
            // Throws an exception if none found

            SAPbobsCOM.ProductionOrders oProdOrder = null;
            try
            {
                var qry = "SELECT I1.BatchNum FROM OWOR AS OW" +
                    " INNER JOIN WOR1 W1 ON OW.DocEntry = W1.DocEntry" +
                    " LEFT JOIN IBT1 I1 on I1.BsDocEntry = OW.docentry AND I1.BsDocLine = W1.LineNum" +
                    " JOIN OBTN T4 on T4.DistNumber = I1.BatchNum" +
                    $" WHERE I1.[BsDocType] = {(int)SAPbobsCOM.BoObjectTypes.oProductionOrders} and(OW.[Status] IN('R', 'L')) and OW.DocEntry = {pPoNum}";

                var batchNum = UTIL.SQL.GetValue<string>(qry);

                if (string.IsNullOrEmpty(batchNum)) throw new Exception($"Batch number not found for production order '{pPoNum}'");

                return batchNum;
            }
            catch (Exception ex)
            {

                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                UTIL.Misc.KillObject(oProdOrder);
                GC.Collect();
            }
        }

        public static SAPbobsCOM.BatchNumbers GetBatchRow(int pPdO, string pBatch)
        {
            // retrurn the batch number object (used for copying UDFs)

            if (pPdO < 1 || pBatch.Trim() == "") return null;

            var Key = 0;
            SAPbobsCOM.ProductionOrders oPdO = null;
            try
            {
                oPdO = OpenPdO(pPdO);
                for (var i = 0; i < oPdO.Lines.Count; i++)
                {
                    oPdO.Lines.SetCurrentLine(i);
                    for (var b = 0; b < oPdO.Lines.BatchNumbers.Count; b++)
                    {
                        oPdO.Lines.BatchNumbers.SetCurrentLine(b);
                        if (oPdO.Lines.BatchNumbers.BatchNumber == pBatch) return oPdO.Lines.BatchNumbers;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                GC.Collect();
            }
        }

        public static SAPbobsCOM.ProductionOrders OpenPdO(int key)
        {
            try
            {
                // Prepare a new production order object
                SAPbobsCOM.ProductionOrders PO = (SAPbobsCOM.ProductionOrders)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);

                // Attempt to get the production order by its key
                if (PO.GetByKey(key))
                {
                    return PO;
                }
                else
                {
                    B1Exception.RaiseException();
                    throw new B1Exception();
                }
            }
            catch (B1Exception ex) { throw ex; }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void CheckProject(int key)
        {
            // if a project is specified on the header, make sure all lines have a project

            SAPbobsCOM.ProductionOrders oPdO = null;
            try
            {
                oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                if (oPdO.GetByKey(key) == false) throw new B1Exception(UTIL.Message.Format("Could not find the production order given the key " + key.ToString()));

                // see if there is a project
                if (oPdO.Project == "") return;

                // make sure all rows have a Project specified
                if (NSC_DI.UTIL.SQL.GetValue<int>($"SELECT COUNT(*) FROM WOR1 WHERE DocEntry = {key} AND ISNULL(Project, '') = ''", 0) == 0) return;
                throw new Exception(UTIL.Message.Format("Not all rows have a project specified for Production Order " + key.ToString()));
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void SetUDF(int key, string pField, dynamic pValue, int? pRow = null)
        {
            // set a UDF. if row = -1, then set all rows. if row is null, then set the header UDF

            SAPbobsCOM.ProductionOrders oPdO = null;
            try
            {
                oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                if (oPdO.GetByKey(key) == false) throw new B1Exception(UTIL.Message.Format("Could not find the production order given the key " + key.ToString()));

                // HEADER
                if (pRow == null)
                {
                    oPdO.UserFields.Fields.Item(pField).Value = pValue;
                }

                //  SPECIFIC ROW
                if (pRow >= 0)
                {
                    oPdO.Lines.SetCurrentLine((int)pRow);
                    oPdO.Lines.UserFields.Fields.Item(pField).Value = pValue;
                }

                // ALL ROWS
                if (pRow < 0)
                {
                    for (int i = 0; i < oPdO.Lines.Count; i++)
                    {
                        oPdO.Lines.SetCurrentLine(i);
                        oPdO.Lines.UserFields.Fields.Item(pField).Value = pValue;
                    }
                }

                if (oPdO.Update() == 0) return;
                throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                GC.Collect();
            }
        }

        public static void SetUDF(SAPbobsCOM.ProductionOrders oPdO, string pField, dynamic pValue, int? pRow = null)
        {
            // set a UDF. if row = -1, then set all rows. if row is null, then set the header UDF

            try
            {
                // HEADER
                if (pRow == null)
                {
                    oPdO.UserFields.Fields.Item(pField).Value = pValue;
                }

                //  SPECIFIC ROW
                if (pRow >= 0)
                {
                    oPdO.Lines.SetCurrentLine((int)pRow);
                    oPdO.Lines.UserFields.Fields.Item(pField).Value = pValue;
                }

                // ALL ROWS
                for (int i = 0; i < oPdO.Lines.Count; i++)
                {
                    oPdO.Lines.SetCurrentLine(i);
                    oPdO.Lines.UserFields.Fields.Item(pField).Value = pValue;
                }

                if (oPdO.Update() == 0) return;
                throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public class SpecialProductionOrderComponentLineItem
        {
            public double BaseQuantity { get; set; }
            public double PlannedQuantity { get; set; }
            public string ItemCodeForItemOnBOM { get; set; }
            public string SourceWarehouseCode { get; set; }
            public string BatchNo { get; set; }
        }

        public static class DisassemblyPdO
        {
            public static int CreateFromBOM(string pItemCode, double pPlannedQty, DateTime DueDate, string DestinationWarehouse = null, bool pRelease = false,
                                        string pIsCannabis = "N", string pProcess = "", string pRemarks = "", string pProdBatchID = null)
            {
                var Key = 0;
                SAPbobsCOM.ProductionOrders oPdO = null;
                SAPbobsCOM.ProductTrees oBom = null;

                try
                {
                    var itemName = NSC_DI.SAP.Items.GetItemNameFromItemCode(pItemCode);
                    if (itemName.Length > 1 && itemName.Substring(0, 1) == "~") itemName = itemName.Substring(1);

                    // #10824 - if branches are used, then have to change the WHs on the BOM
                    var branch = NSC_DI.SAP.Branch.Get(DestinationWarehouse);
                    if (branch >= 0)
                    {
                        NSC_DI.SAP.BillOfMaterials.GetByKey(out oBom, pItemCode);
                        for (var i = 0; i < oBom.Items.Count; i++)
                        {
                            oBom.Items.SetCurrentLine(i);
                            var type = NSC_DI.SAP.Warehouse.GetType(oBom.Items.Warehouse);
                            oBom.Items.Warehouse = NSC_DI.SAP.Warehouse.GetCode(branch, type);
                        }
                        if (oBom.Update() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                    }

                    oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
                    oPdO.ProductionOrderType = SAPbobsCOM.BoProductionOrderTypeEnum.bopotDisassembly;
                    oPdO.ItemNo = pItemCode;
                    oPdO.ProductDescription = itemName;
                    oPdO.PlannedQuantity = pPlannedQty;
                    oPdO.DueDate = DueDate;

                    // MIGHT NOT NEED SOME BETWEEN HERE AND \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                    oPdO.Remarks = pRemarks;
                    oPdO.UserFields.Fields.Item("U_NSC_CnpProduct").Value = (pIsCannabis == "") ? "N" : pIsCannabis;
                    var defaultProcess = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_NSC_DefProcess FROM OITT WHERE Code ='{pItemCode}'", ""); // get the process from the BOM
                    if (!string.IsNullOrEmpty(defaultProcess?.Trim()))// Whitespace-Change (!NSC_DI.UTIL.Strings.Empty(defaultProcess))
                        pProcess = defaultProcess;
                    oPdO.UserFields.Fields.Item("U_NSC_Process").Value = pProcess;     // this needs to be set after UDFS are copied (if they are copied). 
                    // HERE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                    if (pProdBatchID != null)
                        oPdO.UserFields.Fields.Item("U_NSC_ProductionBatchID").Value = pProdBatchID;                    

                    if (DestinationWarehouse != null) oPdO.Warehouse = DestinationWarehouse;//"7001";

                    //NSC_DI.SAP.Document.SetDefaultDistributionRules(oPdO);     // can't do this here because there are not rows until doc is added

                    if (oPdO.Add() != 0) B1Exception.RaiseException();

                    Key = B1Object.GetLastAdded();
                    if (Key != 0)
                    {
                        oPdO.GetByKey(Key);
                        NSC_DI.SAP.Document.SetDefaultDistributionRules(oPdO);

                        for (int x = 0; x < oPdO.Lines.Count; x++)
                        {
                            oPdO.Lines.SetCurrentLine(x);
                            if (oPdO.Lines.ProductionOrderIssueType == SAPbobsCOM.BoIssueMethod.im_Backflush)
                            {
                                oPdO.Lines.Delete();
                            }
                        }

                        // Change status to released
                        if (pRelease) ProductionOrder.UpdateStatus(oPdO, SAPbobsCOM.BoProductionOrderStatusEnum.boposReleased);
                        else oPdO.Update();
                    }

                    return Key;
                }
                catch (B1Exception ex) { throw ex; }
                catch (Exception ex)
                {
                    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }
                finally
                {
                    NSC_DI.UTIL.Misc.KillObject(oPdO);
                    NSC_DI.UTIL.Misc.KillObject(oBom);
                    GC.Collect();
                }
            }

            public static int Issue(int pPdoKey, string pBatchSN = "")
            {
                SAPbobsCOM.Documents oDoc = null;
                SAPbobsCOM.ProductionOrders oPdO = null;

                try
                {
                    int IssueKey = 0;

                    // get the Item Group Code
                    oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;

                    oPdO.GetByKey(pPdoKey);

                    oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
                    oDoc.DocDate = DateTime.Now;
                    oDoc.DocDueDate = DateTime.Now;
                    oDoc.JournalMemo = "Issue for Production";

                    // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                    var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                    if (getEmpID != null)
                        oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                    oDoc.Lines.BaseEntry = pPdoKey;
                    oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;

                    var managedType = NSC_DI.SAP.Items.ManagedBy(oPdO.ItemNo);

                    if (managedType == "B")
                    {
                        oDoc.Lines.BatchNumbers.Quantity = oPdO.PlannedQuantity;
                        oDoc.Lines.BatchNumbers.BatchNumber = pBatchSN;
                    }

                    // SERIAL NUMBERS
                    //if (NSC_DI.UTIL.SQL.GetValue<string>($"SELECT OITM.ManSerNum FROM WOR1 join OWOR on WOR1.DocEntry = OWOR.DocEntry WHERE WOR1.DocEntry = '{pPdoKey}'") == "Y")
                    //{
                    //    if (pSN == null || !pSN.Any()) throw new Exception(NSC_DI.UTIL.Message.Format($"Item {oRecordSet.Fields.Item("ItemCode").Value} is Serial Number Managed, but no erial Numbers are specified."));
                    //    //if (i != 0) oDoc.Lines.Add();
                    //    //oDoc.Lines.SetCurrentLine(i);
                    //    oDoc.Lines.Quantity = pSN.Count();
                    //    foreach (var s in pSN)
                    //    {
                    //        if (s != pSN.First()) oDoc.Lines.SerialNumbers.Add();
                    //        //oDoc.Lines.SerialNumbers.BatchID = s.ToString();
                    //        oDoc.Lines.SerialNumbers.SystemSerialNumber = Convert.ToInt32(s);
                    //        oDoc.Lines.SerialNumbers.Quantity = 1;
                    //        //oDoc.Lines.SerialNumbers.UserFields.Fields.Item("U_NSC_WetWgt").Value = "11";
                    //        oDoc.Lines.SerialNumbers.UserFields.Fields.Item("U_NSC_HarvestName").Value = pHarvestName;
                    //    }
                    //}

                    int returnCode = oDoc.Add();

                    if (returnCode != 0) throw new Exception(Globals.oCompany.GetLastErrorDescription());
                        IssueKey = B1Object.GetLastAdded();
                    
                    return IssueKey;
                }
                catch (Exception ex)
                {
                    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }
                finally
                {
                    NSC_DI.UTIL.Misc.KillObject(oDoc);
                    NSC_DI.UTIL.SQL.KillObject(oPdO);
                    GC.Collect();
                }
            }

            public static int Receive(int pPdoKey)
            {
                SAPbobsCOM.ProductionOrders oPdO = null;
                SAPbobsCOM.Documents oDoc = null;

                try
                {
                    int receiptKey = 0;

                    oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;

                    oPdO.GetByKey(pPdoKey);

                    oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);
                    oDoc.DocDate = DateTime.Now;
                    oDoc.DocDueDate = DateTime.Now;
                    oDoc.JournalMemo = "Receipt from Production";

                    // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                    var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                    if (getEmpID != null)
                        oDoc.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                    for (int x = 0; x < oPdO.Lines.Count; x++)
                    {
                        oDoc.Lines.SetCurrentLine(x);
                        //oDoc.Lines.ItemCode = oPdO.Lines.ItemNo;
                        //oDoc.Lines.Quantity = oPdO.Lines.PlannedQuantity;
                        oDoc.Lines.BaseLine = oPdO.Lines.LineNumber;
                        oDoc.Lines.BaseEntry = pPdoKey;
                        oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;

                        var managedType = NSC_DI.SAP.Items.ManagedBy(oPdO.ItemNo);

                        if (managedType == "B")
                        {
                            oDoc.Lines.BatchNumbers.Quantity = oPdO.PlannedQuantity;
                            //oDoc.Lines.BatchNumbers.BatchNumber = NSC_DI.UTIL.AutoStrain.NextBatch(oPdO.Lines.ItemNo);
                            oDoc.Lines.BatchNumbers.BatchNumber = NSC_DI.SAP.BatchItems.NextBatch(oPdO.Lines.ItemNo);
                            // interco check??
                        }
                    }

                    int returnCode = oDoc.Add();

                    if (returnCode == 0)                    
                        receiptKey = B1Object.GetLastAdded();                    
                    else
                    if (returnCode != 0) throw new Exception(Globals.oCompany.GetLastErrorDescription());

                    return receiptKey;
                }
                catch (Exception ex)
                {
                    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }
                finally
                {
                    NSC_DI.UTIL.Misc.KillObject(oDoc);
                    NSC_DI.UTIL.SQL.KillObject(oPdO);
                    GC.Collect();
                }
            }
        }
    }

    public static class ProductionOrder_OLD
	{
        public static int Receive(int pPdO, string pItmGrpNam, double pActualQty, string pBatch = "", IEnumerable<string> pSN = null, string pProcess = null, Dictionary<string, string> pUserDefinedFields = null, Dictionary<string, Dictionary<string, string>> pLineUDFs = null)
        {
            if (pActualQty == 0d) return 0;
            //var SQL = "";
            var Key = 0;
            SAPbobsCOM.Documents oDoc = null;
            SAPbobsCOM.Recordset oRecordSet = null;

            try
            {
                oDoc = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                oDoc.DocDate = DateTime.Now;
                oDoc.DocDueDate = DateTime.Now;
                //oDoc.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items; 
                //oDoc.DocNum = Convert.ToInt16(NSC_DI.UTIL.SQL.GetNextAvailableCodeForItem("OIGN", "DocEntry")); 
                oDoc.JournalMemo = "Receipt from Production";

                //****First line on on good receipt Prod will be the item being created from the production order
                //oDoc.Lines.BaseLine =2;
                //oDoc.Lines.SetCurrentLine(0);  
                string strItemCode = NSC_DI.UTIL.SQL.GetValue<string>(string.Format(@"Select ItemCode from OWOR where DocNum = {0}", pPdO), null);
                //oDoc.Lines.ItemCode = strItemCode;
                oDoc.Lines.BaseType = 0; //(int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                oDoc.Lines.BaseEntry = pPdO; //NSC_DI.UTIL.SQL.GetValue<int>(string.Format(@"Select DocEntry from OWOR where DocNum = {0}", pPdO), null); 
                //oDoc.Lines.Price = 0;
                oDoc.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                oDoc.Lines.Quantity = pActualQty;
                oDoc.Lines.WarehouseCode = NSC_DI.UTIL.SQL.GetValue<string>(string.Format(@"Select warehouse from OWOR where DocNum = {0}", pPdO), null);

                if (pLineUDFs?.ContainsKey("") == true)
                {
                    foreach (var kvp in pLineUDFs[""])
                    {
                        oDoc.Lines.UserFields.Fields.Item(kvp.Key).Value = kvp.Value;
                    }
                }

                if (!string.IsNullOrEmpty(pBatch) && pSN == null)
                {
                    //Standard Batch Item
                    //oDoc.Lines.BatchNumbers.Add();
                    oDoc.Lines.BatchNumbers.BatchNumber = pBatch;
                    oDoc.Lines.BatchNumbers.Quantity = pActualQty;
                    //Check for User defined fields
                    if (pUserDefinedFields != null)
                    {
                        foreach (KeyValuePair<string, string> pair in pUserDefinedFields)
                            oDoc.Lines.BatchNumbers.UserFields.Fields.Item(pair.Key).Value = pair.Value;
                    }

                }
                int i = 1;
                if (pSN != null)
                {
                    int SNCount = pSN.Count();
                    foreach (var s in pSN)
                    {
                        if (i == 1)
                        {
                            oDoc.Lines.SerialNumbers.InternalSerialNumber = s;
                            oDoc.Lines.SerialNumbers.Quantity = 1;
                            //Check for User defined fields
                            if (pUserDefinedFields != null)
                            {
                                foreach (KeyValuePair<string, string> pair in pUserDefinedFields)
                                    oDoc.Lines.SerialNumbers.UserFields.Fields.Item(pair.Key).Value = pair.Value;
                            }

                            i++;
                        }
                        else
                        {
                            oDoc.Lines.SerialNumbers.Add();
                            oDoc.Lines.SerialNumbers.InternalSerialNumber = s;
                            oDoc.Lines.SerialNumbers.Quantity = 1;
                            //Check for User defined fields
                            if (pUserDefinedFields != null)
                            {
                                foreach (KeyValuePair<string, string> pair in pUserDefinedFields)
                                    oDoc.Lines.SerialNumbers.UserFields.Fields.Item(pair.Key).Value = pair.Value;
                            }

                            i++;
                        }
                        //Standard Serialized Items
                        //if (s != pSN.First()) oDoc.Lines.BatchNumbers.Add();   
                    }
                }
                //oDoc.Lines.Add();

                #region ByProducts
                //Check For ByProducts
                string SQL = null;
                SQL = String.Format(@"SELECT WOR1.LineNum,WOR1.BaseQty,OWOR.PlannedQty,OITM.ManBtchNum,WOR1.IssueType, Oitm.ItmsGrpCod,wor1.ItemCode, WOR1.wareHouse FROM WOR1
                JOIN OITM ON WOR1.ItemCode = OITM.ItemCode join OWOR on WOR1.DocEntry = OWOR.DocEntry
                 WHERE WOR1.DocEntry = {0} AND OWOR.Status = 'R' and WOR1.BaseQty < 0", pPdO);
                var addToDest = false;
                oRecordSet = (SAPbobsCOM.Recordset)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordSet = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(SQL);
                if (oRecordSet.RecordCount > 0)
                {
                    //By Products Exist
                    for (i = 0; i <= oRecordSet.RecordCount - 1; i++)
                    {
                        if (i == 0)
                        {
                            oRecordSet.MoveFirst();
                        }
                        else
                        {
                            oRecordSet.MoveNext();
                        }

                        //*****THis is for a receipt from production ****
                        if (oRecordSet.Fields.Item(1).Value < 0)
                        {
                            //byproduct

                            if (pLineUDFs?.ContainsKey(oRecordSet.Fields.Item(0).Value) == true)
                            {
                                foreach (var kvp in pLineUDFs[oRecordSet.Fields.Item(0).Value])
                                {
                                    oDoc.Lines.UserFields.Fields.Item(kvp.Key).Value = kvp.Value;
                                }
                            }

                            //1. determine variance between planned and actual Qty(s). Actual should always be equal or less than planned
                            double dblVariance = oRecordSet.Fields.Item(2).Value - pActualQty;
                            string pItemProp = "";
                            if (dblVariance > 0)
                            {
                                //Process by "Process Type"
                                switch (pProcess)
                                {
                                    //Propogation 
                                    case ("Clone"):
                                        pItemProp = "Clone";
                                        if (NSC_DI.SAP.Items.Property.Get(oRecordSet.Fields.Item("ItemCode").Value, pItemProp) == "Y")
                                        //if (oRecordSet.Fields.Item(5).Value == 103)
                                        {
                                            oDoc.Lines.Add();
                                            //byproduct was created 
                                            oDoc.Lines.BaseType = 0;
                                            oDoc.Lines.BaseEntry = pPdO;
                                            oDoc.Lines.BaseLine = oRecordSet.Fields.Item(0).Value;
                                            //oDoc.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                                            oDoc.Lines.Quantity = dblVariance * (oRecordSet.Fields.Item(1).Value * -1);

                                            oDoc.Lines.WarehouseCode = oRecordSet.Fields.Item("wareHouse").Value as string;

                                            //Check for batch Managed Items. (This will currently work for only one batch managed item. )
                                            if (oRecordSet.Fields.Item(3).Value == "Y" && !string.IsNullOrEmpty(pBatch))
                                            {

                                                //Destruction Batch
                                                oDoc.Lines.BatchNumbers.BatchNumber = pBatch;
                                                oDoc.Lines.BatchNumbers.Quantity = dblVariance * (oRecordSet.Fields.Item("BaseQty").Value * -1);
                                                //ToDo: Set the State ID of the batch

                                                addToDest |= (oDoc.Lines.WarehouseCode == "9999");

                                            }

                                        }
                                        break;
                                    case ("Seed"):
                                        pItemProp = "Seed";
                                        if (NSC_DI.SAP.Items.Property.Get(oRecordSet.Fields.Item("ItemCode").Value, pItemProp) == "Y")
                                        //if (oRecordSet.Fields.Item(5).Value == 101)
                                        {
                                            oDoc.Lines.Add();
                                            oDoc.Lines.BaseType = 0;
                                            oDoc.Lines.BaseEntry = pPdO;
                                            oDoc.Lines.BaseLine = oRecordSet.Fields.Item(0).Value;
                                            //oDoc.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                                            oDoc.Lines.Quantity = dblVariance * (oRecordSet.Fields.Item(1).Value * -1);
                                            string strSQL_Warehouse = string.Format(@"SELECT wor1.wareHouse
                                    FROM WOR1
                                    JOIN OITM ON WOR1.ItemCode = OITM.ItemCode join OWOR on WOR1.DocEntry = OWOR.DocEntry
                                    WHERE WOR1.DocEntry = {0} AND OWOR.Status = 'R' and WOR1.BaseQty < 0", pPdO);
                                            oDoc.Lines.WarehouseCode = NSC_DI.UTIL.SQL.GetValue<string>(strSQL_Warehouse, null);

                                            //Check for batch Managed Items. (This will currently work for only one batch managed item. )
                                            if (oRecordSet.Fields.Item(3).Value == "Y" && !string.IsNullOrEmpty(pBatch))
                                            {

                                                //Destruction Batch
                                                oDoc.Lines.BatchNumbers.BatchNumber = pBatch;
                                                oDoc.Lines.BatchNumbers.Quantity = dblVariance * (oRecordSet.Fields.Item(1).Value * -1);

                                                addToDest |= (oDoc.Lines.WarehouseCode == "9999");

                                            }
                                        }
                                        break;
                                    case ("Tissue"):
                                        pItemProp = "Plant Tissue";
                                        if (NSC_DI.SAP.Items.Property.Get(oRecordSet.Fields.Item("ItemCode").Value, pItemProp) == "Y")
                                        //if (oRecordSet.Fields.Item(5).Value == 102)
                                        {
                                            oDoc.Lines.Add();
                                            oDoc.Lines.BaseType = 0;
                                            oDoc.Lines.BaseEntry = pPdO;
                                            //oDoc.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                                            oDoc.Lines.Quantity = dblVariance * (oRecordSet.Fields.Item(1).Value * -1);
                                            string strSQL_Warehouse = string.Format(@"SELECT wor1.wareHouse
                                    FROM WOR1
                                    JOIN OITM ON WOR1.ItemCode = OITM.ItemCode join OWOR on WOR1.DocEntry = OWOR.DocEntry
                                    WHERE WOR1.DocEntry = {0} AND OWOR.Status = 'R' and WOR1.BaseQty < 0", pPdO);
                                            oDoc.Lines.WarehouseCode = NSC_DI.UTIL.SQL.GetValue<string>(strSQL_Warehouse, null);


                                            //Check for batch Managed Items. (This will currently work for only one batch managed item. )
                                            if (oRecordSet.Fields.Item(3).Value == "Y" && !string.IsNullOrEmpty(pBatch))
                                            {

                                                //Destruction Batch
                                                oDoc.Lines.BatchNumbers.BatchNumber = pBatch;
                                                oDoc.Lines.BatchNumbers.Quantity = dblVariance * (oRecordSet.Fields.Item(1).Value * -1);

                                                addToDest |= (oDoc.Lines.WarehouseCode == "9999");

                                            }
                                        }
                                        break;
                                    //Harvest Plant -->Wet Cannabis

                                    //Wet Cannabis-->Dry Cannabis

                                    //Dry Cannabis --> Cured Unbatched
                                    case ("Curing"):

                                        break;
                                    //Default - Non byproduct line item  
                                    default:
                                        oDoc.Lines.Add();
                                        oDoc.Lines.SetCurrentLine(i);
                                        oDoc.Lines.BaseEntry = pPdO;
                                        oDoc.Lines.BaseLine = oRecordSet.Fields.Item(0).Value; //ItemLine;
                                        oDoc.Lines.BaseType = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                                        oDoc.Lines.BatchNumbers.Quantity = pActualQty * oRecordSet.Fields.Item(1).Value;
                                        oDoc.Lines.BatchNumbers.BatchNumber = pBatch;
                                        break;
                                }

                                // add destruction quarantine state and time stamp if item is assigned to the destruction warehouse

                            }
                        }

                    }
                }
                //oDoc.Lines.Add();
                #endregion

                if (addToDest) BatchItems.ChangeQuarantineStatus(pBatch, Globals.QuarantineStates.DEST);

                int returnCode = oDoc.Add();



                if (returnCode == 0)
                {
                    Key = B1Object.GetLastAdded();
                }
                else
                {
                    B1Exception.RaiseException();
                }

                return Key;
            }
            catch (B1Exception ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDoc);
                NSC_DI.UTIL.SQL.KillObject(oRecordSet);
                GC.Collect();
            }
        }

        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Contains the ID or Code for the object that was recently created.
        /// </summary>
        public static int NewlyCreatedKey { get; set; }

        // Production orders are split into two different methods because the "Source" and "Destination" warehouses vary based on the production order type (Standard, Disassembly)

        /// <summary>
        /// Will attempt to create a "Standard" "Production Order" SAP document.  A standard production order will consume items in order to create a new item.
        /// </summary>
        /// <param name="ItemCodeForItemBeingCreated"></param>
        /// <param name="QuantityOfItemsToBuild"></param>
        /// <param name="DestinationWarehouseCode"></param>
        /// <param name="dueDate"></param>
        /// <param name="components"></param>
        /// <param name="ProjectedBatchNumber"></param>
        /// <returns></returns>
        public static int CreateStandardProductionOrder(string ItemCodeForItemBeingCreated, double QuantityOfItemsToBuild, string DestinationWarehouseCode,
            DateTime dueDate, List<StandardProductionOrderComponentLineItem> components, string ProjectedBatchNumber = null, string IsCannabisItem = "N", string pRemarks = "", string pProcess = "")
		{
			// Create a Production Order
			SAPbobsCOM.ProductionOrders productionOrder = null;

			try
			{
				productionOrder = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
				productionOrder.DueDate = dueDate;
				productionOrder.Warehouse = DestinationWarehouseCode;
				productionOrder.PlannedQuantity = QuantityOfItemsToBuild;
				productionOrder.ItemNo = ItemCodeForItemBeingCreated;
				productionOrder.ProductionOrderStatus = SAPbobsCOM.BoProductionOrderStatusEnum.boposPlanned;
				productionOrder.ProductionOrderType = SAPbobsCOM.BoProductionOrderTypeEnum.bopotStandard;
                productionOrder.Remarks = pRemarks;
                if (!String.IsNullOrEmpty(ProjectedBatchNumber)) productionOrder.UserFields.Fields.Item("U_NSC_Pln_Batch").Value = ProjectedBatchNumber;

				//if (Globals.IsCannabis) productionOrder.UserFields.Fields.Item("U_NSC_CnpProduct").Value = IsCannabisItem;
				productionOrder.UserFields.Fields.Item("U_NSC_CnpProduct").Value = IsCannabisItem;
                productionOrder.UserFields.Fields.Item("U_NSC_Process").Value = pProcess;

                foreach (var item in components)
				{
					productionOrder.Lines.ItemNo = item.ItemCodeForItemBeingDestroyed;
					productionOrder.Lines.PlannedQuantity = item.BaseQuantity*item.PlannedQuantity;

					if (item.SourceWarehouseCode != null)
					{
						productionOrder.Lines.Warehouse = item.SourceWarehouseCode;
					}
                    NSC_DI.SAP.Document.SetDefaultDistributionRules(productionOrder.Lines);

                    productionOrder.Lines.Add();
				}

				// Attempt to add the new production order
				int ProductionOrderSuccess = productionOrder.Add();

				if (ProductionOrderSuccess == 0)
				{
					string newCode = null;

					// Get the production order code
					Globals.oCompany.GetNewObjectCode(out newCode);
					var Key = 0;
					if (newCode != null) Key = Convert.ToInt32(newCode);

					return Key;
				}
				else
				{
					throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
				}
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(productionOrder);
				GC.Collect();
			}
		}

		public class StandardProductionOrderComponentLineItem
		{
			public double BaseQuantity { get; set; }
			public double PlannedQuantity { get; set; }
			public string ItemCodeForItemBeingDestroyed { get; set; }
			public string SourceWarehouseCode { get; set; }
		}

        /// <summary>
        /// Will create a SAP "Production Order" document marked as a "disassembly" where a single item is "destroyed" in order to "produce" one or many other items.
        /// </summary>
        /// <param name="ItemCodeForItemBeingDestroyed">The SAP </param>
        /// <param name="QuantityOfItemsToDestroy"></param>
        /// <param name="SourceWarehouseCode"></param>
        /// <param name="dueDate"></param>
        /// <param name="components"></param>
        /// <param name="ProjectedBatchNumber"></param>
        /// <returns></returns>
        public static int CreateDisassemblyProductionOrder(string ItemCodeForItemBeingDestroyed, double QuantityOfItemsToDestroy, string SourceWarehouseCode,
			DateTime dueDate, List<DisassemblyProductionOrderComponentLineItem> components, string ProjectedBatchNumber = null)
		{
			// Create a Production Order
			try
			{

				SAPbobsCOM.ProductionOrders productionOrder =
					Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
				productionOrder.DueDate = dueDate;
				productionOrder.Warehouse = SourceWarehouseCode;
				productionOrder.PlannedQuantity = QuantityOfItemsToDestroy;
				productionOrder.ItemNo = ItemCodeForItemBeingDestroyed;
				productionOrder.ProductionOrderStatus = SAPbobsCOM.BoProductionOrderStatusEnum.boposPlanned;
				productionOrder.ProductionOrderType = SAPbobsCOM.BoProductionOrderTypeEnum.bopotDisassembly;

				try
				{
					productionOrder.UserFields.Fields.Item("U_NSC_PLN_BATCH").Value = ProjectedBatchNumber;
				}
				catch
				{

				}

				foreach (var item in components)
				{
					productionOrder.Lines.BaseQuantity = item.BaseQuantity;
					productionOrder.Lines.PlannedQuantity = item.PlannedQuantity;
					productionOrder.Lines.ItemNo = item.ItemCodeForItemBeingCreated;
					if (item.DestinationWarehouseCode != null)
					{
						productionOrder.Lines.Warehouse = item.DestinationWarehouseCode;
					}
					productionOrder.Lines.Add();
				}

				// Attempt to add the new production order
				int ProductionOrderSuccess = productionOrder.Add();

				if (ProductionOrderSuccess == 0)
				{
					string newCode = null;

					// Get the production order code
					Globals.oCompany.GetNewObjectCode(out newCode);

					if (newCode != null)
					{
						NewlyCreatedKey = Convert.ToInt32(newCode);
					}
					return NewlyCreatedKey;
				}
				else
				{
					//log.Warn("CreateDisassemblyProductionOrder error (" + ProductionOrderSuccess + "): " + this.LastErrorMessage);
					throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
				}
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				//NSC_DI.UTIL.Misc.KillObject(oBar);
				GC.Collect();
			}
		}



		public class DisassemblyProductionOrderComponentLineItem
		{
			public double BaseQuantity { get; set; }
			public double PlannedQuantity { get; set; }
			public string ItemCodeForItemBeingCreated { get; set; }
			public string DestinationWarehouseCode { get; set; }
		}


		/// <summary>
		/// Will attempt to update the status on a production order
		/// </summary>
		/// <param name="key">The key identifier for the production order you want to change the status on.</param>
		/// <param name="status">The new status of the production order.</param>
		/// <returns>Returns true if successful, false if failed.</returns>
		public static bool UpdateStatus(int key, SAPbobsCOM.BoProductionOrderStatusEnum status)
		{
			// Prepare a new production order object
			SAPbobsCOM.ProductionOrders PO = (SAPbobsCOM.ProductionOrders) Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);

			// Attempt to get the production order by its key
			if (PO.GetByKey(key))
			{
				// Change the status of the production order
				PO.ProductionOrderStatus = status;

				// Attempt to update the production order
				int ReturnCode = PO.Update();

				// If the update was successful
				if (ReturnCode == 0)
				{
					return true;
				}
				// The update was not successful
				else
				{
					throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
					return false;
				}
			}
			// Could not find the production order
			else
			{
				throw new Exception(UTIL.Message.Format("Could not find the production order given the key " + key.ToString()));
				return false;
			}
		}
    }
}
