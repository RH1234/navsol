﻿using System;
using System.Collections.Generic;
using SAPbobsCOM;

namespace NSC_DI.SAP
{
	public class TestField 
    {
		public static void CreateTestField(string Code, string Name)
        {
			SAPbobsCOM.GeneralService oGeneralService = null;
			SAPbobsCOM.GeneralData oGeneralData = null;

            try
            {
                SAPbobsCOM.CompanyService oCompService = Globals.oCompany.GetCompanyService();

                oGeneralService = oCompService.GetGeneralService(Globals.tTestField + "_C");

                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

                oGeneralData.SetProperty("Code", Code);
                oGeneralData.SetProperty("U_Name", Name);

                oGeneralService.Add(oGeneralData);
			}
			catch (Exception ex)
			{
				throw new Exception(UTIL.Message.Format(ex));
			}
			finally
			{
				UTIL.Misc.KillObject(oGeneralService);
				UTIL.Misc.KillObject(oGeneralData);

				GC.Collect();
			}
		}

        // We will need relation soonish. 
        //        /// <summary>
        //        /// Create a Relation between Compound and Effect.
        //        /// </summary>
        //        /// <param name="Code">The code of the new row</param>
        //        /// <param name="CompoundCode">Compound Code</param>
        //        /// <param name="EffectCode">Effect Code</param>
        //        public void CreateCompoundEffectRelation(string Code, string CompoundCode, string EffectCode)
        //        {
        //            SAPbobsCOM.GeneralService oGeneralService;
        //            SAPbobsCOM.GeneralData oGeneralData;

        //            try
        //            {
        //                SAPbobsCOM.CompanyService oCompService = Globals.oCompany.GetCompanyService();

        //                oGeneralService = oCompService.GetGeneralService(Globals.tCompoundAndEffect.TableName + "_C");

        //                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

        //                oGeneralData.SetProperty("Code", Code);

        //                oGeneralData.SetProperty("U_" + Globals.SAP_PartnerCode + "_" + "CompoundCode", CompoundCode);

        //                oGeneralData.SetProperty("U_" + Globals.SAP_PartnerCode + "_" + "EffectCode", EffectCode);

        //                oGeneralService.Add(oGeneralData);

        //                this.WasSuccessful = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                this.WasSuccessful = false;
        //            }
        //        }

        //        public void RemoveCompoundEffectRelation(string EffectCode, string CompoundCode )
        //        {
        //            // Using a record set to delete a relation table record.
        //            try
        //            {
        //                // Prepare to run a SQL statement.
        //                SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

        //                string sqlQuery = string.Format(@"
        //DELETE [@" + Globals.SAP_PartnerCode + "_" + NavSol.Models.CompoundAndEffect.TableName + @"]
        //FROM [@" + Globals.SAP_PartnerCode + "_" + NavSol.Models.CompoundAndEffect.TableName + @"]
        //WHERE [@" + Globals.SAP_PartnerCode + "_" + NavSol.Models.CompoundAndEffect.TableName + @"].[U_" + Globals.SAP_PartnerCode + @"_EffectCode] = {0}
        //AND [@" + Globals.SAP_PartnerCode + "_" + NavSol.Models.CompoundAndEffect.TableName + @"].[U_" + Globals.SAP_PartnerCode + "_CompoundCode] = {1}", EffectCode, CompoundCode);

        //                // Count how many current records exist within the database.
        //                oRecordSet.DoQuery(sqlQuery);

        //                WasSuccessful = true;
        //            }
        //            catch(Exception e)
        //            {
        //                //TODO-LOG: Log error
        //                WasSuccessful = false;
        //            }
        //        }

    }
}
