﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSC_DI.UTIL
{
    public class Units
    {
        public class UndefinedUnitException : Exception
        {
            public UndefinedUnitException(string unit) : base($"Unit '{unit}' is not defined as a conversion factor. Valid units are in grams (g), pounds (lb), and kilograms (kg).") { }
        }
        public class UndefinedLiquidException : Exception
        {
            public UndefinedLiquidException(string unit) : base($@"Unit '{unit}' is not defined as a conversion factor. 
Valid units are in milliter (ml), liters (l), gallon (gal), quart (q), pint (pt), cups (cup), and fluid ounce (floz, oz).") { }
        }

        public static decimal ConvertWeight(decimal quantity, string fromUnit, string toUnit)
        {
            decimal fromConvFactor = GetUoMConversionFactor(fromUnit);
            decimal toConvFactor = GetUoMConversionFactor(toUnit);

            var convertedQuantity = (quantity * fromConvFactor) / toConvFactor;

            // SAP allows for only six decimal places
            return Math.Round(convertedQuantity, 6, MidpointRounding.AwayFromZero);
        }

        private static decimal GetUoMConversionFactor(string unit)
        {
            decimal value = -1;
            // Converts from unit to grams
            switch(unit.ToLowerInvariant())
            {
                case "g":
                case "gram":
                case "grams":
                     value = 1M;
                    break;
                case "lb":
                case "lbs":
                case "pound":
                case "pounds":
                    value = 453.59237M;
                    break;
                case "kg":
                case "kilogram":
                case "kilograms":
                    value = 1000M;
                    break;
                default:
                    throw new UndefinedUnitException(unit);
            }
            return value;
        }

        public static double ConvertLiquid(double quantity, string fromUnit, string toUnit)
        {
            double fromConvFactor = GetLiquidConversionFactor(fromUnit);
            double toConvFactor = GetLiquidConversionFactor(toUnit);

            var convertedQuantity = (quantity * fromConvFactor) / toConvFactor;

            // SAP allows for only six decimal places
            return Math.Round(convertedQuantity, 6, MidpointRounding.AwayFromZero);
        }

        private static double GetLiquidConversionFactor(string unit)
        {
            double value = -1;
            // Converts from unit to grams
            switch (unit.ToLowerInvariant())
            {
                case "ml":
                case "milliter":
                case "milliters":
                    value = 1.0;
                    break;
                case "l":
                case "liter":
                case "liters":
                    value = 1000.0;
                    break;
                case "gal":
                case "gallon":
                    value = 3785.40038;
                    break;
                case "qt":
                case "quart":
                    value = 946.35;
                    break;
                case "pt":
                case "pint":
                    value = 473.175;
                    break;
                case "cup":
                case "cups":
                    value = 236.5875;
                    break;
                case "oz":
                case "floz":
                case "fluid ounce":
                    value = 29.57344;
                    break;
                default:
                    throw new UndefinedLiquidException(unit);
            }
            return value;
        }
    }
}
