﻿using System;
using System.Runtime.InteropServices;
using System.Linq;
using System.Collections.Generic;
using Humanizer;

namespace NSC_DI.UTIL
{
    public static class Misc
    {
        private static void TemplateSub()   // Form pForm   // just a template for new subs
        {
            try
            {
            }
            catch (Exception ex)
            {
                //Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                //throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
            }
        }

        public static void KillObject(object Obj)
        {
            int ticks = 1000000;
            if (Obj == null || !Marshal.IsComObject(Obj)) return;
            while (Marshal.FinalReleaseComObject(Obj) != 0 && ticks != 0) ticks--;
            if (ticks == 0) throw new Exception("COM object cannot be released");
            Obj = null;
        }

        /// <summary>
        /// Converts a timestamp back into a useable DateTime.
        /// </summary>
        /// <param name="unixTimeStamp">Timestamp to convert.</param>
        /// <returns></returns>
        [Obsolete("Use NSC_DI.UTIL.Dates.FromTimeStamp")]
        public static DateTime TimeStampToDateTime(double unixTimeStamp)
        {
            return Dates.FromTimeStamp((int)unixTimeStamp);
        }

        /// <summary>
        /// Converts a regular DateTime(Non-UTC since I'm converting to UTC) into a timestamp(integer).
        /// </summary>
        /// <param name="dateToConvert">DateTime to convert.</param>
        /// <returns></returns>
        [Obsolete("Use NSC_DI.UTIL.Dates.ToTimeStamp")]
        public static int DateTimeToTimeStamp(DateTime DateToConvert)
        {
            return (int)Dates.ToTimeStamp(DateToConvert);
        }


		public static Globals.QuarantineStates GetQuarantineStateFromString(string QuarantineState)
		{
			return (Globals.QuarantineStates)Enum.Parse(typeof(Globals.QuarantineStates), QuarantineState);
		}

        public static string GetQuarantineStateFromType(string QuarantineType)
        {
            return NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_QuarState FROM [@{NSC_DI.Globals.tDestructTypes}] WHERE Code  = {QuarantineType}", null);
        }

        public static Boolean Connect(string pServer, string pCompanyDB, string pUserName, string pPass)
		{
			try
			{
				var iErr = 0;

				if ((Globals.oCompany != null))
				{
					if (Globals.oCompany.Connected) Globals.oCompany.Disconnect();
				}
				Globals.oCompany = null;
				Globals.oCompany = new SAPbobsCOM.Company();
				Globals.oCompany.CompanyDB = pCompanyDB;
				Globals.oCompany.Password = pPass;
				Globals.oCompany.Server = pServer;
				Globals.oCompany.UserName = pUserName;

				Globals.oCompany.language = SAPbobsCOM.BoSuppLangs.ln_English;
				Globals.oCompany.UseTrusted = true;

				//for(int i = 1; i <= SAPbobsCOM.BoDataServerTypes)
				foreach (SAPbobsCOM.BoDataServerTypes servType in Enum.GetValues(typeof(SAPbobsCOM.BoDataServerTypes)))
				{
					Globals.oCompany.DbServerType = servType;
					iErr = Globals.oCompany.Connect();
					if (iErr == 0) return true;
				}
				//throw new Exception(Message.Format("Error in connecting to the Company. " + Environment.NewLine + iErr.ToString() + " - " + oCompany.GetLastErrorDescription()));
				return false;
			}
			catch (Exception ex)
			{
				throw new Exception(Message.Format(ex));
			}
			finally
			{
			}
		}

        public static SAPbobsCOM.BoYesNoEnum ToSAPYesNo(string ynVal)
        {
            return ynVal.ToUpper().StartsWith("Y") ? SAPbobsCOM.BoYesNoEnum.tYES : SAPbobsCOM.BoYesNoEnum.tNO;
        }

        public static void SaveCSV(System.Data.DataTable dt, string fileName)
        {
            var csv = new List<string>();

            var colNames = string.Join(",", dt.Columns.Cast<System.Data.DataColumn>().Select(n => n.ColumnName));

            csv.Add(colNames);

            var rows = dt.Select().Select(n => string.Join(",", n.ItemArray.Select(m => m.ToString())));
                
            csv.AddRange(rows);

            System.IO.File.WriteAllLines(fileName, csv);
        }
	}
}
