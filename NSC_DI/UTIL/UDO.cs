﻿using System;
using System.Collections.Generic;
using SAPbobsCOM;

namespace NSC_DI.UTIL
{
    /// <summary>
    /// A UDO or "User Defined Object" can be registered within SAP Business One.  
    /// A UDO makes it easier to track objects within the database, and allows user to run reports, edit, etc the objects directly within SAP's client.
    /// </summary>
    public static class UDO
    {
        /// <summary>
        /// Attempts to create a user table within SAP Business One
        /// </summary>
        /// <param name="pTableName">The name of the user table.  The SAP Business One partner prefix will NOT be added automatically in code, include the prefix in your table name.</param>
        /// <param name="pDescription">A user friendly name for the user table.</param>
        /// <param name="pType">The type of user table being created.</param>
		public static bool AddUserTable(string pTableName, string pDescription, BoUTBTableType pType)
        {
            UserTablesMD oUDT = null;

	        try
	        {
                if (pTableName.Trim() == "") return false;
                pTableName = RemoveATsign(pTableName.Trim());

		        if (TableExists(pTableName)) return true;

		        // Initiate the user table
				oUDT = ((UserTablesMD)(Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserTables)));

		        // Set the table parameters
		        oUDT.TableName			= pTableName;
		        oUDT.TableDescription	= pDescription;
		        oUDT.TableType			= pType;

		        // Attempt to create the user table
		        var SAP_OperationReturnCode		= oUDT.Add();

		        if (SAP_OperationReturnCode == 0) return true;

		        // Get the error message from SAP Business One
		        var lastError = "";
				Globals.oCompany.GetLastError(out SAP_OperationReturnCode, out lastError);
		        throw new Exception(Message.Format("Error creating table: " + pTableName + Environment.NewLine + SAP_OperationReturnCode.ToString() + " - " + lastError));
	        }
	        catch (Exception ex)
	        {
				throw new Exception(Message.Format(ex));
	        }
			finally 
			{
				Misc.KillObject(oUDT);
		        GC.Collect();
	        }
        }

        public static bool UserTableRemove(string pTableName)
        {
            UserTablesMD oUDT = null;

            try
            {
                if (pTableName.Trim() == "") return false;
                pTableName = RemoveATsign(pTableName.Trim());

                if (TableExists(pTableName) == false) return true;

                // Initiate the user table
                oUDT = Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserTables);
                oUDT.GetByKey(pTableName);

                // Attempt to remove the user table
                var SAP_OperationReturnCode = oUDT.Remove();

                if (SAP_OperationReturnCode == 0) return true;

                // Get the error message from SAP Business One
                var lastError = "";
                Globals.oCompany.GetLastError(out SAP_OperationReturnCode, out lastError);
                throw new Exception(Message.Format("Error removing table: " + pTableName + Environment.NewLine + SAP_OperationReturnCode.ToString() + " - " + lastError));
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        public static void ChangeTableDescription(string pTableName, string pDescription)
        {
            UserTablesMD oUDT = null;
            try
            {
                if (pTableName.Trim() == "" || pDescription.Trim() == "")
                    return;                
                pTableName = RemoveATsign(pTableName.Trim());
                oUDT = ((UserTablesMD)(Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserTables)));
                oUDT.GetByKey($"{pTableName}");
                if (oUDT.TableDescription != pDescription)
                {
                    oUDT.TableDescription = pDescription;
                    oUDT.Update();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(oUDT);
                GC.Collect();
            }
        }   

        public static bool IsFieldInTable(string TableName, string UDF_Name)
        {
            try
            {
                if (UDF_Name.Length > 3 && UDF_Name.Substring(0,2) == "U_") // removes the U_ prefix to the udf name
                    UDF_Name = UDF_Name.Substring(2);
                
                string squery = string.Format("SELECT 'UDF Exists' FROM [CUFD] WHERE TableID = '{0}' AND AliasID = '{1}'", TableName, UDF_Name);
                string SQLResults = SQL.GetValue<string>(squery);
                return SQLResults != null; 
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void FieldRemove(string TableName, string Name, string PartnerCode = null)
        {
            // no U_ for the field
            try
            {
                PartnerCode = PartnerCode ?? Globals.SAP_PartnerCode;

                if (TableName.Length > 4 && TableName.StartsWith(Globals.SAP_PartnerCode + "_"))
                {
                    TableName = "@" + TableName;                            // user defined table. the "@" symbol is required for the table name
                    if (Name.Substring(0, 2) == "U_") Name.Substring(2);
                }
                else
                    Name = PartnerCode + "_" + Name;    // NOT user defined table. the Partner Code is required for the field name

                

                //before any UserFieldsMD logic (and thus COM object trickery and issues) check if we already exist...
                if (IsFieldInTable(TableName, Name) == false) return;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }

            UserFieldsMD oMD = null;

            try
            {
                // get the field ID
                var id = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT FieldID FROM CUFD WHERE TableID = '{TableName}' AND AliasID = '{Name}'", -1);
                if (id < 0) throw new Exception(NSC_DI.UTIL.Message.Format($"UDF {Name} not found."));

                oMD = Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserFields);
                if(oMD.GetByKey(TableName, id) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                oMD.Remove();
                //if (oMD.Remove() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMD);
                GC.Collect();
            }
        }

        /// <summary>
        /// Adds a field or column to a user created table.
        /// </summary>
        /// <param name="TableName">The database table name to insert the field into.</param>
        /// <param name="Name">The database column name.</param>
        /// <param name="Description">A user friendly description of the field.</param>
        /// <param name="Type">The type of field creating.</param>
        /// <param name="EditSize">The total allowed length of the data in the field.</param>
        /// <param name="ValidValues">A collection of value and descriptions which symbolize user defined accepted values for this field.</param>
        public static void AddFieldToTable(string TableName, string Name, string Description, BoFieldTypes Type, BoFldSubTypes SubType = BoFldSubTypes.st_None, int EditSize = 0,
                                           Dictionary<string, string> ValidValues = null, string DefaultValue = null, bool IsMandatory = false, string LinkTable = null,
                                           string PartnerCode = null, UDFLinkedSystemObjectTypesEnum LinkSysObjEnum = UDFLinkedSystemObjectTypesEnum.ulNone)            
	    {
            // the number of of char that can be in the name param including 'NSC_' is 20 (Error -2110)
            PartnerCode = PartnerCode ?? Globals.SAP_PartnerCode;

            if (TableName.Length > 4 && TableName.StartsWith(PartnerCode + "_")) 
				TableName = "@"  + TableName;					// user defined table. the "@" symbol is required for the table name
			else 
				Name = PartnerCode + "_" + Name;		// NOT user defined table. the Partner Code is required for the field name

		    //before any UserFieldsMD logic (and thus COM object trickery and issues) check if we already exist...
		    if (IsFieldInTable(TableName, Name)) return;

		    UserFieldsMD oUserFieldsMD = null;
		    int SAP_OperationReturnCode;
		    string lastError;

		    try
		    {
			    // Initiate our new user field
			    oUserFieldsMD = ((UserFieldsMD) (Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserFields)));

			    // Setting the Field's properties
			    oUserFieldsMD.Name          = Name;
			    oUserFieldsMD.Description   = Description;
			    oUserFieldsMD.Type          = Type;
			    oUserFieldsMD.SubType       = SubType;

			    if (IsMandatory)
			    {
				    oUserFieldsMD.Mandatory = BoYesNoEnum.tYES;
			    }

			    if (DefaultValue != null)
			    {
				    oUserFieldsMD.DefaultValue = DefaultValue;
			    }

                if (!string.IsNullOrEmpty(LinkTable))
                    oUserFieldsMD.LinkedTable = LinkTable;
                if (LinkSysObjEnum != UDFLinkedSystemObjectTypesEnum.ulNone)
                    oUserFieldsMD.LinkedSystemObject = LinkSysObjEnum;

                oUserFieldsMD.TableName = TableName;

			    // If a max size was passed add it to the data schema
			    if (EditSize > 0)
			    {
				    oUserFieldsMD.EditSize = EditSize;
			    }

			    // Adding Valid Values if present
			    if (ValidValues != null)
			    {
				    foreach (string Key in ValidValues.Keys)
				    {
					    oUserFieldsMD.ValidValues.Value = Key;
					    oUserFieldsMD.ValidValues.Description = ValidValues[Key].ToString();
					    oUserFieldsMD.ValidValues.Add();
				    }
			    }

			    // Adding the Field to the Table
			    if (oUserFieldsMD.Add() == 0) return;
			    Globals.oCompany.GetLastError(out SAP_OperationReturnCode, out lastError);
			    throw new Exception(Message.Format("Error creating Field: " + Name + Environment.NewLine + SAP_OperationReturnCode.ToString() + " - " + lastError));
		    }
		    catch (Exception ex)
		    {
			    throw new Exception(Message.Format(ex));
		    }
		    finally
		    {
				Misc.KillObject(oUserFieldsMD);
			    GC.Collect();
                GC.WaitForFullGCComplete();
		    }
	    }

        public static void AddFieldToTable(string Prefix, string TableName, string Name, string Description, BoFieldTypes Type, BoFldSubTypes SubType = BoFldSubTypes.st_None, int EditSize = 0, Dictionary<string, string> ValidValues = null, string DefaultValue = null, bool IsMandatory = false, string LinkTable = null)
        {
            if (TableName.Length > 4 && TableName.StartsWith(Prefix + "_"))
                TableName = "@" + TableName;                    // user defined table. the "@" symbol is required for the table name
            else
                Name = Prefix + "_" + Name;        // NOT user defined table. the Partner Code is required for the field name

            //before any UserFieldsMD logic (and thus COM object trickery and issues) check if we already exist...
            if (IsFieldInTable(TableName, Name))
            {
                return;
            }
            UserFieldsMD oUserFieldsMD = null;
            int SAP_OperationReturnCode;
            string lastError;

            try
            {
                // Initiate our new user field
                oUserFieldsMD = ((UserFieldsMD)(Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserFields)));

                // Setting the Field's properties
                oUserFieldsMD.Name = Name;
                oUserFieldsMD.Description = Description;
                oUserFieldsMD.Type = Type;
                oUserFieldsMD.SubType = SubType;

                if (IsMandatory)
                {
                    oUserFieldsMD.Mandatory = BoYesNoEnum.tYES;
                }

                if (DefaultValue != null)
                {
                    oUserFieldsMD.DefaultValue = DefaultValue;
                }

                if (!string.IsNullOrEmpty(LinkTable)) oUserFieldsMD.LinkedTable = LinkTable;

                oUserFieldsMD.TableName = TableName;

                // If a max size was passed add it to the data schema
                if (EditSize > 0)
                {
                    oUserFieldsMD.EditSize = EditSize;
                }

                // Adding Valid Values if present
                if (ValidValues != null)
                {
                    foreach (string Key in ValidValues.Keys)
                    {
                        oUserFieldsMD.ValidValues.Value = Key;
                        oUserFieldsMD.ValidValues.Description = ValidValues[Key].ToString();
                        oUserFieldsMD.ValidValues.Add();
                    }
                }

                // Adding the Field to the Table
                if (oUserFieldsMD.Add() == 0) return;
                Globals.oCompany.GetLastError(out SAP_OperationReturnCode, out lastError);
                throw new Exception(Message.Format("Error creating Field: " + Name + Environment.NewLine + SAP_OperationReturnCode.ToString() + " - " + lastError));
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(oUserFieldsMD);
                GC.Collect();
                GC.WaitForFullGCComplete();
            }
        }

        public static void AddFieldGUID(string pTableName, string pName = "GUID")
		{
			// adds a string field 38 chars long and is manditory.
			// also, a unique key is generated.
			if (IsFieldInTable(AddATsign(pTableName), pName)) return;
			AddFieldToTable(pTableName, pName, "GUID", BoFieldTypes.db_Alpha, EditSize: 38, IsMandatory: false);
			AddKey(pName, pTableName, pName, BoYesNoEnum.tNO);
		}

        public static void UpdateField(string TableName, string Name, string Description = null, int EditSize = 0, dynamic DefaultValue = null, bool IsMandatory = false, string LinkTable = null)
        {
            if (TableName.Length > 4 && TableName.StartsWith(Globals.SAP_PartnerCode + "_"))
                TableName = "@" + TableName;                    // user defined table. the "@" symbol is required for the table name
            else
                Name = Globals.SAP_PartnerCode + "_" + Name;        // NOT user defined table. the Partner Code is required for the field name

            //before any UserFieldsMD logic (and thus COM object trickery and issues) check if we already exist...
            if (IsFieldInTable(TableName, Name) == false) return;

            int fieldID = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT FieldID FROM CUFD WHERE TableID = '{TableName}' AND AliasID = '{Name}'");

            UserFieldsMD oUserFieldsMD = null;
            int SAP_OperationReturnCode;
            string lastError;

            try
            {
                // Initiate our new user field
                oUserFieldsMD = ((UserFieldsMD)(Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserFields)));

                if(oUserFieldsMD.GetByKey(TableName, fieldID) == false) throw new Exception(Message.Format("Error finding Field: " + Name));


                // Setting the Field's properties
                if (Description != null) oUserFieldsMD.Description = Description;

                if (IsMandatory) oUserFieldsMD.Mandatory = BoYesNoEnum.tYES;

                if (DefaultValue != null) oUserFieldsMD.DefaultValue = DefaultValue;

                if (LinkTable != null) oUserFieldsMD.LinkedTable = LinkTable;


                // If a max size was passed add it to the data schema
                if (EditSize > 0)  oUserFieldsMD.EditSize = EditSize;

                // remove all Valid Values
                if (oUserFieldsMD.ValidValues.Count > 0) oUserFieldsMD.ValidValues.Delete();

                // Adding the Field to the Table
                if (oUserFieldsMD.Update() == 0) return;
                Globals.oCompany.GetLastError(out SAP_OperationReturnCode, out lastError);
                throw new Exception(Message.Format("Error updating Field: " + Name + Environment.NewLine + SAP_OperationReturnCode.ToString() + " - " + lastError));
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(oUserFieldsMD);
                GC.Collect();
                GC.WaitForFullGCComplete();
            }
        }

        public static void UpdateField(string TableName, string Name, string Description, BoFieldTypes Type, BoFldSubTypes SubType = BoFldSubTypes.st_None, int EditSize = 0, Dictionary<string, string> ValidValues = null, string DefaultValue = null, bool IsMandatory = false, string LinkTable = null)
        {
            if (TableName.Length > 4 && TableName.StartsWith(Globals.SAP_PartnerCode + "_"))
                TableName = "@" + TableName;                    // user defined table. the "@" symbol is required for the table name
            else
                Name = Globals.SAP_PartnerCode + "_" + Name;        // NOT user defined table. the Partner Code is required for the field name

            //before any UserFieldsMD logic (and thus COM object trickery and issues) check if we already exist...
            if (IsFieldInTable(TableName, Name) == false) return;

            int fieldID = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT FieldID FROM CUFD WHERE TableID = '{TableName}' AND AliasID = '{Name}'");//??12121

            UserFieldsMD oUserFieldsMD = null;
            int SAP_OperationReturnCode;
            string lastError;

            try
            {
                // Initiate our new user field
                oUserFieldsMD = ((UserFieldsMD)(Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserFields)));

                oUserFieldsMD.GetByKey(TableName, fieldID);

                // Setting the Field's properties
                oUserFieldsMD.Description = Description;

                if (IsMandatory) oUserFieldsMD.Mandatory = BoYesNoEnum.tYES;

                oUserFieldsMD.DefaultValue = DefaultValue;

                oUserFieldsMD.LinkedTable = LinkTable;


                // If a max size was passed add it to the data schema
                if (EditSize > 0)
                {
                    oUserFieldsMD.EditSize = EditSize;
                }

                // remove all Valid Values
                int validValsCount = oUserFieldsMD.ValidValues.Count;
                if (validValsCount > 0)
                {
                    for (int i = 0; i <= validValsCount; i++)
                        oUserFieldsMD.ValidValues.Delete();
                }
                
                // Adding Valid Values if present
                if (ValidValues != null)
                {
                    foreach (string Key in ValidValues.Keys)
                    {
                        oUserFieldsMD.ValidValues.Value = Key;
                        oUserFieldsMD.ValidValues.Description = ValidValues[Key].ToString();
                        oUserFieldsMD.ValidValues.Add();
                    }
                }
                var test = oUserFieldsMD.ValidValues;
                // Adding the Field to the Table
                if (oUserFieldsMD.Update() == 0) return;
                Globals.oCompany.GetLastError(out SAP_OperationReturnCode, out lastError);
                throw new Exception(Message.Format("Error updating Field: " + Name + Environment.NewLine + SAP_OperationReturnCode.ToString() + " - " + lastError));
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(oUserFieldsMD);
                GC.Collect();
                GC.WaitForFullGCComplete();
            }
        }

        public static void AddValidValue(string pTableName, string pField, string pValue, string Description)
        {
            // add an additional Valid Value

            if (pTableName.Length > 4 && pTableName.StartsWith(Globals.SAP_PartnerCode + "_"))
                pTableName = "@" + pTableName;                    // user defined table. the "@" symbol is required for the table name
            else
                pField = Globals.SAP_PartnerCode + "_" + pField;        // NOT user defined table. the Partner Code is required for the field name

            //before any UserFieldsMD logic (and thus COM object trickery and issues) check if we already exist...
            if (IsFieldInTable(pTableName, pField) == false) return;

            int fieldID = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT FieldID FROM CUFD WHERE TableID = '{pTableName}' AND AliasID = '{pField}'");

            UserFieldsMD oUserFieldsMD = null;
            int SAP_OperationReturnCode;
            string lastError;

            try
            {
                // see if value already exists
                if (NSC_DI.UTIL.SQL.GetValue<string>($"SELECT FldValue FROM UFD1 WHERE TableID = '{pTableName}' AND FieldID = {fieldID} AND FldValue = '{pValue}'", null) != null) return;

                // Initiate our new user field
                oUserFieldsMD = ((UserFieldsMD)(Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserFields)));

                oUserFieldsMD.GetByKey(pTableName, fieldID);

                oUserFieldsMD.ValidValues.Add();
                oUserFieldsMD.ValidValues.Value = pValue;
                oUserFieldsMD.ValidValues.Description = Description;


                if (oUserFieldsMD.Update() == 0) return;
                Globals.oCompany.GetLastError(out SAP_OperationReturnCode, out lastError);
                throw new Exception(Message.Format("Error updating Field: " + pField + Environment.NewLine + SAP_OperationReturnCode.ToString() + " - " + lastError));
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(oUserFieldsMD);
                GC.Collect();
                GC.WaitForFullGCComplete();
            }
        }

        public static void AddKey(string pKeyName, string pTableName, string pField, BoYesNoEnum pUnique = BoYesNoEnum.tNO)
		{
			//	 no @ for UDTs. No "U_" for the fields

			int SAP_OperationReturnCode;
			string lastError;
			UserKeysMD oUserKeysMD = null;

			try
			{
				oUserKeysMD = Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserKeys);

				oUserKeysMD.TableName            = pTableName;          // MAX 20 CHARS
				oUserKeysMD.KeyName              = "IX_" + pKeyName;    // MAX 10 CHARS
				oUserKeysMD.Elements.ColumnAlias = pField;              // MAX 18 CHARS
				oUserKeysMD.Unique               = pUnique;

				SAP_OperationReturnCode = oUserKeysMD.Add();

				switch (SAP_OperationReturnCode)
				{
					case 0: // No Error
						break;

					case -1: // index already exists
						break;

					default:
						Globals.oCompany.GetLastError(out SAP_OperationReturnCode, out lastError);
						throw new Exception(Message.Format(SAP_OperationReturnCode.ToString() + " - " + lastError));
				}

			}
			catch (Exception ex)
			{
				throw new Exception(Message.Format(ex));
			}
			finally
			{
                Misc.KillObject(oUserKeysMD);
                GC.Collect();
			}
		}

        public static void RemoveKey(string pTableName, string pField)
        {
            // Added this but didnt end up needing it, but it could be useful later so I am keeping it for now
            int SAP_OperationReturnCode;
            string lastError;
            UserKeysMD oUserKeysMD = null;

            try
            {
                oUserKeysMD = Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserKeys);
                int indexID = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT KeyId FROM UKD1 WHERE TableName = '{pTableName}' AND ColAlias =  '{pField}'");//??12121
                oUserKeysMD.GetByKey(pTableName, indexID);
                SAP_OperationReturnCode = oUserKeysMD.Remove();

                switch (SAP_OperationReturnCode)
                {
                    case 0: // No Error
                        break;

                    case -1: // index already exists, may not need to be here for this method
                        break;

                    default:
                        Globals.oCompany.GetLastError(out SAP_OperationReturnCode, out lastError);
                        throw new Exception(Message.Format(SAP_OperationReturnCode.ToString() + " - " + lastError));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(oUserKeysMD);
                GC.Collect();
            }
        }

        public static string AddATsign(string sValue)
        {
            if (sValue.Substring(0, 1) != "@" && sValue.Substring(0, 1) != "[") return "@" + sValue;
            else return sValue;
        }

        public static string RemoveATsign(string pValue)
        {
            if (pValue.Substring(0, 1) == "@")            
                pValue = pValue.Substring(1);
            
            if (pValue.Length > 4 && pValue.StartsWith($"{Globals.SAP_PartnerCode}_") == false)            
                pValue = $"{Globals.SAP_PartnerCode}_{pValue}";
            
            return pValue;
        }

        public static string AddU_(string sValue)
        {
            if (sValue.Substring(0, 2) != "U_") return "U_" + sValue;
            else return sValue;
        }

		public static string Encase(string sValue, string pEncase)
		{
			// sub to encase a string with another string ..... surounding a B1 UDT with []
			if (pEncase.Trim().Length == 0) return sValue;
			if (sValue.Substring(0, pEncase.Length) != pEncase) return pEncase + sValue + pEncase;
			else return sValue;
		}

        public static string GetFieldSubType(string TableName, string UDF_Name)
        {
            // no U_ for the field
            try
            {
                string sql = $"SELECT EditType FROM [CUFD] WHERE TableID = '{TableName}' AND AliasID = '{UDF_Name}'";
                return (NSC_DI.UTIL.SQL.GetValue<string>(sql, null));
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                GC.Collect();
            }
        }

        /// <summary>
        /// Registers a user defined object based off of a user defined table.
        /// </summary>
        /// <param name="TableName">The table to register the user defined object off of.</param>
        public static void UDO_Add(string TableName, string pChildTable = null)
	    {
		    int SAP_OperationReturnCode;
		    string lastError;
		    UserObjectsMD oUserObjectMD = null;

			//oUserObjectMD.Remove();

		    try
		    {
			    // Define the UDO (User Defined Object) the we will register within SAP Business One
			    oUserObjectMD						= ((UserObjectsMD) (Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserObjectsMD)));
			    oUserObjectMD.CanCancel				= BoYesNoEnum.tYES;
			    oUserObjectMD.CanClose				= BoYesNoEnum.tYES;
			    oUserObjectMD.CanCreateDefaultForm	= BoYesNoEnum.tNO;
			    oUserObjectMD.CanDelete				= BoYesNoEnum.tYES;
			    oUserObjectMD.CanFind				= BoYesNoEnum.tYES;
			    oUserObjectMD.CanYearTransfer		= BoYesNoEnum.tYES;
			    oUserObjectMD.Code					= TableName + "_C";
			    oUserObjectMD.ManageSeries			= BoYesNoEnum.tYES;
			    oUserObjectMD.Name					= TableName;
			    oUserObjectMD.ObjectType			= BoUDOObjType.boud_MasterData;
			    oUserObjectMD.TableName				= TableName;
                if(pChildTable != null) oUserObjectMD.ChildTables.TableName = pChildTable;

                // Register and add the UDO object
                SAP_OperationReturnCode = oUserObjectMD.Add();

			    switch (SAP_OperationReturnCode)
			    {
				    case 0: // No Error
					    break;

				    case -2035: // Item already exists
                        var x = oUserObjectMD.Update();
					    break;
                    case -5002: // there are values in the table
                        break;

                    default:
					    Globals.oCompany.GetLastError(out SAP_OperationReturnCode, out lastError);
					    throw new Exception(Message.Format("Error creating UDO: " + oUserObjectMD.Code + Environment.NewLine + SAP_OperationReturnCode.ToString() + " - " + lastError));
			    }

		    }
		    catch (Exception ex)
		    {
			    throw new Exception(Message.Format(ex));
		    }
		    finally
		    {
                NSC_DI.UTIL.Misc.KillObject(oUserObjectMD);
                oUserObjectMD = null;
                GC.Collect();
		    }
	    }
        public static void UDO_Remove(string TableName)
        {
            int ret;
            string lastError;
            UserObjectsMD oUserObjectMD = null;

            try
            {
                // Define the UDO (User Defined Object) the we will register within SAP Business One
                oUserObjectMD = ((UserObjectsMD)(Globals.oCompany.GetBusinessObject(BoObjectTypes.oUserObjectsMD)));
                if (oUserObjectMD.GetByKey(TableName + "_C") == false) return;  // throw new Exception(Message.Format("Error Finding UDO"));
                ret = oUserObjectMD.Remove();

                switch (ret)
                {
                    case 0: // No Error
                        break;

                    //case -2035: // Item already exists
                    //    break;
                    //case -5002: // there are values in the table
                    //    break;

                    default:
                        Globals.oCompany.GetLastError(out ret, out lastError);
                        throw new Exception(Message.Format("Error Removing UDO: " + oUserObjectMD.Code + Environment.NewLine + ret.ToString() + " - " + lastError));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oUserObjectMD);
                oUserObjectMD = null;
                GC.Collect();
            }
        }

        public static bool TableExists(string TableName)
		{
			// checks if a user table already exists.

			bool Answer = false;
			Recordset rs = null;
			//if (TableName.Substring(0, 2) == "NB") TableName = "@" + TableName;
			try
			{
				if (TableName.Trim() == "") return false;
				if (TableName.Substring(0, 1) == "@") TableName = TableName.Substring(1);
				rs = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
				rs.DoQuery("SELECT 1 FROM OUTB WHERE \"TableName\" = '" + TableName.ToUpper() + "'");
				if (!rs.EoF) Answer = true;
				return Answer;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				Misc.KillObject(rs);
				GC.Collect();
			}
		}

        public static void GetByKey(SAPbobsCOM.UserTable pUDT, string pKey, int pLineNo = -1)
        {
            // get the Entry for the specified key and optional line number.

            if (pUDT == null) throw new Exception("Table not specified");
            if (string.IsNullOrEmpty(pKey)) throw new Exception("DocNum not specified");

            try
            {
                if(pUDT.GetByKey(pKey) == false) throw new System.Collections.Generic.KeyNotFoundException("Key not found.");
                if (pLineNo >= 0)
                {
                    try {
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        #region Data Manipulation past creation of UDO

        public static void CopyUDFs(dynamic pTo, dynamic pFrom, string[] pSkip = null)
        {
            // copy the UDFs

            // USAGE:                         
            // NSC_DI.UTIL.UDO.CopyUDFs(oIss.Lines.BatchNumbers, oPdO.Lines.BatchNumbers, new string[1] { "U_NSC_StateID" });

            try
            {
                // most UDF objects have "Fields" property, some don't (Bin info)
                // so have to account for this.

                dynamic oFrom;
                dynamic oTo;
                try
                {
                    oFrom = pFrom.UserFields.Fields;
                }
                catch
                {
                    oFrom = pFrom.UserFields;
                }

                try
                {
                    oTo = pTo.UserFields.Fields;
                }
                catch
                {
                    oTo = pTo.UserFields;
                }

                for (int j = 0; j < oFrom.Count; j++)
                {
                    try
                    {
                        var s = oFrom.Item(j).Name;
                        Console.WriteLine(s + " -> " + oTo.Item(s).Value + " -> " + oFrom.Item(j).Value);
                        if(pSkip == null || Array.IndexOf(pSkip,s) < 0) oTo.Item(s).Value = oFrom.Item(j).Value;
                        Console.WriteLine(s + " -> " + oTo.Item(s).Value);
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oPdO);
                GC.Collect();
            }
        }
        ///// <summary>
        ///// Gets a general service to be used with a specific type of UDO.
        ///// </summary>
        ///// <param name="pCompany">Company object.  Used to get the Company Service.</param>
        ///// <param name="pUDOCode">UDO code to be used to get a service.  Unique ID for each UDO type.</param>
        ///// <returns>General Service object used to interact with UDOs.</returns>
        //public static SAPbobsCOM.GeneralService GetUDOServByCode(this SAPbobsCOM.Company pCompany, string pUDOCode)//Gets the general service we use to work with UDOs.
        //{
        //    SAPbobsCOM.CompanyService oCompanyService = pCompany.GetCompanyService();
        //    SAPbobsCOM.GeneralService oGeneralService = oCompanyService.GetGeneralService(pUDOCode);
        //    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oCompanyService);
        //    oCompanyService = null;
        //    return oGeneralService;
        //}

        //public static dynamic GetFieldValue(this SAPbobsCOM.Recordset rs, string ColumnName)
        //{
        //    if (rs.Fields.Count < 1 || rs.RecordCount < 1)
        //        return null;
        //    if (rs.Fields.Item(ColumnName).IsNull() == SAPbobsCOM.BoYesNoEnum.tYES)
        //        return null;
        //    dynamic o = rs.Fields.Item(ColumnName).Value;
        //    if (o == null)
        //        return null;
        //    if (o.GetType().IsValueType)
        //        return o;
        //    if (o.GetType() == typeof(string))
        //        return o;
        //    if (o == DBNull.Value)
        //        return null;
        //    return o;
        //}

        ///// <summary>
        ///// Gets the correct key for a UDO based on single column.
        ///// </summary>
        ///// <param name="TableName">Name of the UDO tbl (used in the RecSet, so include the @).</param>
        ///// <param name="ColumnValue">Val of the unique key to use to find the matching rec</param>
        ///// <param name="ColumnName">Name of the unique key used to match the record.  DocEntry by default.</param>
        ///// <param name="KeyColumnName">Value of the UDO key to return from the matching record.</param>
        ///// <returns>The value of the key.</returns>
        //public static dynamic GetUDOKey(string pTableName, string pColumnName, string pColumnValue, string pKeyColumnName = "DocEntry")
        //{
        //    dynamic results;

        //    SAPbobsCOM.Recordset recSet = oCompany.GetRecordSet(string.Format("SELECT {0} FROM [{1}] WHERE {2}={3}", pKeyColumnName, pTableName, pColumnName, pColumnValue));
        //    results = recSet.GetFieldText(string.Format("{0}", pColumnName));
        //    results = recSet.GetFieldValue(0);
        //    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(recSet);
        //    recSet = null;
        //    return results;
        //}

        ///// <summary>
        ///// Gets a GeneralData object instance matching a specific Key (DocEntry by default) which can be used (in conjunction with a GeneralService) to update or delete an object.
        ///// </summary>
        ///// <param name="gService">General Service object.  Required to work with UDOs.</param>
        ///// <param name="KeyValue">Value of the unique key to use to find the matching record.</param>
        ///// <param name="KeyName">Name of the unique key used to match the record.  DocEntry by default.</param>
        ///// <returns>GeneralData object, where you can set properties and update.  If no match was found, returns null.</returns>
        //public static SAPbobsCOM.GeneralData GetUDOObjectByID(this SAPbobsCOM.GeneralService gService, dynamic KeyValue, string KeyName = "DocEntry")
        //{
        //    SAPbobsCOM.GeneralDataParams oGeneralParams = ((SAPbobsCOM.GeneralDataParams)(gService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)));
        //    try
        //    {
        //        oGeneralParams.SetProperty(KeyName, KeyValue);
        //        SAPbobsCOM.GeneralData oGeneralData = gService.GetByParams(oGeneralParams);
        //        return oGeneralData;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex.Message.Contains("No matching records found."))
        //            return null;//Stupid exceptions instead of getting some sort of result value.
        //    }
        //    return null;
        //}
        #endregion
    }
}