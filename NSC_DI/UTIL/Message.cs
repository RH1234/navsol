﻿using System;

namespace NSC_DI.UTIL
{
	public static class Message
	{
		public static string Format(int iErr, Exception oEX)
		{
			// entry point with an exception

			System.Diagnostics.StackTrace oTrace1 = new System.Diagnostics.StackTrace(oEX, true);
			// get the method and line number of the error
			System.Diagnostics.StackTrace oTrace2 = new System.Diagnostics.StackTrace(true);
			// get the method and line number of the calling methods
			System.Diagnostics.StackFrame oFrame = null;
			string sMethod = null;
			string sLine = null;
			string sFile = null;
			string sMsg = "";

			int i = 0;

			try
			{
				sMsg = sMsg + "ERROR: " + iErr.ToString() + "    " + oEX.Message + Environment.NewLine;

				// get the error method and line number
				oFrame = oTrace1.GetFrame(oTrace1.FrameCount - 1);
				sLine = oFrame.GetFileLineNumber().ToString();
				sLine = Right("        " + sLine.Trim(), 8);
				sMethod = oFrame.GetMethod().Name + "                                                                 ";
				sMethod = sMethod.Substring(0, 40);
				sFile = System.IO.Path.GetFileNameWithoutExtension(oFrame.GetFileName());

				sMsg = sMsg + Environment.NewLine + "Line:" + sLine + "     " + sFile + "." + sMethod;

				// now get the calling hierarchy
				for (i = 2; i <= oTrace2.FrameCount - 1; i++)
				{
					oFrame = oTrace2.GetFrame(i);
					sMethod = oTrace2.GetFrame(i).GetMethod().Name + "                                                  ";
					sMethod = sMethod.Substring(0, 40);
					sLine = oFrame.GetFileLineNumber().ToString();
					sLine = Right("        " + sLine.Trim(), 8);
					sFile = System.IO.Path.GetFileNameWithoutExtension(oFrame.GetFileName());
					// skip system routines
					if (oFrame.GetFileLineNumber() > 0)
					{
						sMsg = sMsg + Environment.NewLine + "Line:" + sLine + "     " + sFile + "." + sMethod;
					}
				}

			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show("(MessageERR - oEX)     " + ex.Message + Environment.NewLine + sMsg);
			}
			GC.Collect();
			return sMsg;
		}

		public static string Format(string pMsg, Exception oEX)
		{
			// entry point with an exception

			System.Diagnostics.StackTrace oTrace1 = new System.Diagnostics.StackTrace(oEX, true);
			// get the method and line number of the error
			System.Diagnostics.StackTrace oTrace2 = new System.Diagnostics.StackTrace(true);
			// get the method and line number of the calling methods
			System.Diagnostics.StackFrame oFrame = null;
			string sMethod = null;
			string sLine = null;
			string sFile = null;
			string sMsg = "";

			int i = 0;

			try
			{
				sMsg = sMsg + pMsg + Environment.NewLine + oEX.Message + Environment.NewLine;

				// get the error method and line number
				oFrame = oTrace1.GetFrame(oTrace1.FrameCount - 1);
				sLine = oFrame.GetFileLineNumber().ToString();
				sLine = Right("        " + sLine.Trim(), 8);
				sMethod = oFrame.GetMethod().Name + "                                                                 ";
				sMethod = sMethod.Substring(0, 40);
				sFile = System.IO.Path.GetFileNameWithoutExtension(oFrame.GetFileName());

				sMsg = sMsg + Environment.NewLine + "Line:" + sLine + "     " + sFile + "." + sMethod;

				// now get the calling hierarchy
				for (i = 2; i <= oTrace2.FrameCount - 1; i++)
				{
					oFrame = oTrace2.GetFrame(i);
					sMethod = oTrace2.GetFrame(i).GetMethod().Name + "                                                  ";
					sMethod = sMethod.Substring(0, 40);
					sLine = oFrame.GetFileLineNumber().ToString();
					sLine = Right("        " + sLine.Trim(), 8);
					sFile = System.IO.Path.GetFileNameWithoutExtension(oFrame.GetFileName());
					// skip system routines
					if (oFrame.GetFileLineNumber() > 0)
					{
						sMsg = sMsg + Environment.NewLine + "Line:" + sLine + "     " + sFile + "." + sMethod;
					}
				}

			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show("(MessageERR - oEX)     " + ex.Message + Environment.NewLine + sMsg);
			}
			GC.Collect();
			return sMsg;
		}

		public static string Format(Exception oEX, bool logInWS = true)
		{
			// entry point with an exception

			System.Diagnostics.StackTrace oTrace1 = new System.Diagnostics.StackTrace(oEX, true);
			// get the method and line number of the error
			System.Diagnostics.StackTrace oTrace2 = new System.Diagnostics.StackTrace(true);
			// get the method and line number of the calling methods
			System.Diagnostics.StackFrame oFrame = null;
			string sMethod = null;
			string sLine = null;
			string sFile = null;
			string sMsg = "";

			int i = 0;

			try
			{
				if (oEX.Message.StartsWith("ERROR:  ")) return oEX.Message;

				sMsg = sMsg + "ERROR:  " + oEX.Message + Environment.NewLine;

				// get the error method and line number
				oFrame = oTrace1.GetFrame(oTrace1.FrameCount - 1);
				sLine = oFrame.GetFileLineNumber().ToString();
				sLine = Right("        " + sLine.Trim(), 8);
				sMethod = oFrame.GetMethod().Name + "                                                                 ";
				sMethod = sMethod.Substring(0, 40);
				sFile = System.IO.Path.GetFileNameWithoutExtension(oFrame.GetFileName());

				sMsg = sMsg + Environment.NewLine + "Database:" + Globals.oCompany.CompanyDB + Environment.NewLine + "Line:" + sLine + "     " + sFile + "." + sMethod;

				// now get the calling hierarchy
				for (i = 2; i <= oTrace2.FrameCount - 1; i++)
				{
					oFrame = oTrace2.GetFrame(i);
					sMethod = oTrace2.GetFrame(i).GetMethod().Name + "                                                  ";
					sMethod = sMethod.Substring(0, 40);
					sLine = oFrame.GetFileLineNumber().ToString();
					sLine = Right("        " + sLine.Trim(), 8);
					sFile = System.IO.Path.GetFileNameWithoutExtension(oFrame.GetFileName());
					// skip system routines
					if (oFrame.GetFileLineNumber() > 0)
					{
						sMsg = sMsg + Environment.NewLine + "Line:" + sLine + "     " + sFile + "." + sMethod;
					}
				}

				if (logInWS)
                {
                    RemoteLogger.WriteError(sMsg);
                }

			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show("(MessageERR - oEX)     " + ex.Message + Environment.NewLine + sMsg);
			}
			GC.Collect();
			return sMsg;
		}

		public static string Format(int iErr, string sTxt)
		{
			// routine to create and format the error message
			// entry point with out an exception

			string sMethod = null;
			string sLine = null;
			string sFile = null;
			string sSub = null;
			string sMsg = "";
			System.Diagnostics.StackFrame oFrame = null;
			System.Diagnostics.StackTrace oTrace2 = new System.Diagnostics.StackTrace(true);
			// get the method and line number of the calling methods

			int i = 0;

			try
			{
				sMsg = sMsg + "ERROR: " + iErr.ToString() + "       " + sTxt;
				oFrame = new System.Diagnostics.StackFrame(1, true);
				sSub = oFrame.GetMethod().Name;
				//sLine = oFrame.GetFileLineNumber().ToString();
				sLine = Right("        " + oFrame.GetFileLineNumber().ToString(), 8);
				sFile = System.IO.Path.GetFileNameWithoutExtension(oFrame.GetFileName());
				sMsg = sMsg + Environment.NewLine + "Line:" + sLine + "     " + sFile + "." + sSub;

				// now get the calling hierarchy
				for (i = 2; i <= oTrace2.FrameCount - 1; i++)
				{
					oFrame = oTrace2.GetFrame(i);
					sMethod = oTrace2.GetFrame(i).GetMethod().Name + "                                                  ";
					sMethod = sMethod.Substring(0, 40);
					sLine = oFrame.GetFileLineNumber().ToString();
					sLine = Right("        " + sLine.Trim(), 8);
					sFile = System.IO.Path.GetFileNameWithoutExtension(oFrame.GetFileName());
					// skip system routines
					if (oFrame.GetFileLineNumber() > 0)
					{
						sMsg = sMsg + Environment.NewLine + "Line:" + sLine + "     " + sFile + "." + sSub;
					}
				}

			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show("(MessageERR - sTxt)     " + ex.Message + Environment.NewLine + sMsg);
			}
			finally
			{
				GC.Collect();
			}
			return sMsg;
		}

		public static string Format(string sTxt)
		{
			// routine to create and format the error message
			// entry point with out an exception

			// Then Throw New Exception("Matrix is empty.")

			int i = 0;

			string sMethod = null;
			string sLine = null;
			string sFile = null;
			string sSub = null;
			string sMsg = "";
			System.Diagnostics.StackFrame oFrame = null;
			System.Diagnostics.StackTrace oTrace2 = new System.Diagnostics.StackTrace(true);
			// get the method and line number of the calling methods


			try
			{
                if (sTxt.StartsWith("ERROR:  ")) return sTxt;

                sMsg = sTxt;
				oFrame = new System.Diagnostics.StackFrame(1, true);
				sSub = oFrame.GetMethod().Name;
				sLine = oFrame.GetFileLineNumber().ToString();
				sLine = Right("        " + sLine.Trim(), 8);
				sFile = System.IO.Path.GetFileNameWithoutExtension(oFrame.GetFileName());
				sMsg = sMsg + Environment.NewLine + "Database:" + Globals.oCompany.CompanyDB + Environment.NewLine + "Line:" + sLine + "     " + sFile + "." + sSub;

                // now get the calling hierarchy
                for (i = 2; i <= oTrace2.FrameCount - 1; i++)
				{
					oFrame = oTrace2.GetFrame(i);
					sMethod = oTrace2.GetFrame(i).GetMethod().Name + "                                                  ";
					sMethod = sMethod.Substring(0, 40);
					sLine = oFrame.GetFileLineNumber().ToString();
					sLine = Right("        " + sLine.Trim(), 8);
					sFile = System.IO.Path.GetFileNameWithoutExtension(oFrame.GetFileName());
					// skip system routines
					if (oFrame.GetFileLineNumber() > 0)
					{
						sMsg = sMsg + Environment.NewLine + "Line:" + sLine + "     " + sFile + "." + sMethod;
					}
				}

			}
			catch (Exception ex)
			{
				System.Windows.Forms.MessageBox.Show("(MessageERR - sTxt)     " + ex.Message + Environment.NewLine + sMsg);
			}
			finally
			{
				GC.Collect();
			}
			return sMsg;
		}

		public static string Right(string sValue, int iLen)
		{
			if (iLen < 1) return sValue;
			if (sValue.Length < iLen) return sValue;
			return sValue.Substring(sValue.Length - iLen, iLen);
		}

		//public static string Format(System.Data.Entity.Validation.DbEntityValidationException oEx)
		//{
		//    // routine to create and format the error message
		//    // entry point with out an exception


		//    string sMsg = "";

		//    try
		//    {
		//        // Retrieve the error messages as a list of strings.
		//        List<string> errorMessages = new List<string>();
		//        foreach (System.Data.Entity.Validation.DbEntityValidationResult validationResult in oEx.EntityValidationErrors)
		//        {
		//            string entityName = validationResult.Entry.Entity.GetType().Name;
		//            foreach (System.Data.Entity.Validation.DbValidationError error in validationResult.ValidationErrors)
		//            {
		//                errorMessages.Add(entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
		//                sMsg += sMsg + Environment.NewLine + entityName + "." + error.PropertyName + ": " + error.ErrorMessage;
		//            }
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        System.Windows.Forms.MessageBox.Show("(MessageERR - sTxt)     " + ex.Message + Environment.NewLine + sMsg);
		//    }
		//    finally
		//    {
		//        GC.Collect();
		//    }
		//    return sMsg;
		//}
	}
}
