﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace NSC_DI.UTIL
{
	public class Encryption
	{
		private static byte[] key = {};
		private static byte[] IV = {
		0x12,
		0x34,
		0x56,
		0x78,
		0x90,
		0xab,
		0xcd,
		0xef
			};

		/// <summary>
		/// This function enables you to decrypt an encrypted string.
		/// </summary>
		/// <param name="stringToDecrypt">This is the encrypted string that is to be decrypted.</param>
		/// <param name="sEncryptionKey">This is the key that is to be used to peform the decryption.  
		/// It must be equal to the key that was used for the original encryption.</param>
		/// <returns>Returns the decrpted string.</returns>
		/// <remarks>Provides an easy way to decrypt string data.</remarks>
		public static string Decrypt(string stringToDecrypt, string sEncryptionKey = "wm3DgkY4")
		{
			byte[] inputByteArray = new byte[stringToDecrypt.Length + 1];

			try
			{
				//key = System.Text.Encoding.UTF8.GetBytes(Strings.Left(sEncryptionKey, 8));
				string sTmp = sEncryptionKey.Substring(0, 8);
				key = Encoding.UTF8.GetBytes(sTmp);
				DESCryptoServiceProvider des = new DESCryptoServiceProvider();
				inputByteArray = Convert.FromBase64String(stringToDecrypt);
				MemoryStream ms = new MemoryStream();

				CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
				cs.Write(inputByteArray, 0, inputByteArray.Length);
				cs.FlushFinalBlock();
				Encoding encoding = Encoding.UTF8;
				return encoding.GetString(ms.ToArray());
			}
			catch (Exception e)
			{
				return e.Message;
			}
		}

		/// <summary>
		/// This function enables you to encrypt a string.
		/// </summary>
		/// <param name="stringToEncrypt">This is the string you want to encrypt.</param>
		/// <param name="SEncryptionKey">This is the key to be used as the basis for the encryption.</param>
		/// <returns>Returns the encrypted string.</returns>
		/// <remarks>Provides an easy way to encrypt as string value.</remarks>
		public static string Encrypt(string stringToEncrypt, string SEncryptionKey = "wm3DgkY4")
		{
			try
			{
				//key = System.Text.Encoding.UTF8.GetBytes(Strings.Left(SEncryptionKey, 8));
				string sTmp = SEncryptionKey.Substring(0, 8);
				key = Encoding.UTF8.GetBytes(sTmp);
				DESCryptoServiceProvider des = new DESCryptoServiceProvider();
				byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
				MemoryStream ms = new MemoryStream();
				CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
				cs.Write(inputByteArray, 0, inputByteArray.Length);
				cs.FlushFinalBlock();
				return Convert.ToBase64String(ms.ToArray());
			}
			catch (Exception e)
			{
				return e.Message;
			}
		}


	}

}
