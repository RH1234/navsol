﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace NSC_DI.UTIL
{
    public static class Strings
    {
        public static string RemoveBrackets(string s)
        {
            return Regex.Replace(s, "[\\[\\]]+", string.Empty);
        }
        public static string Search(string s)
        {
            // returns the string without blanks or special characters and upshifted.
            // used to create a vaule to looking for duplicate entries of a name.
            return Regex.Replace(s.ToUpper(), @"[^0-9A-Z]", string.Empty);
        }
        public static string SearchSQL(string pTable, string pField, string pValue)
        {
            // used to create a vaule to looking for duplicate entries of a name.

            //var sql = $"SELECT CASE WHEN  EXISTS(SELECT TOP 1 * FROM [{pTable}] WHERE {pField} = '{pValue}') THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END";
            var sql = $"SELECT CASE WHEN EXISTS(SELECT TOP 1 * FROM [{pTable}] WHERE {pField} = '{pValue}' COLLATE SQL_Latin1_General_CP1_CI_AS) THEN 1 ELSE 0 END";
            return sql;
        }
        public static string SearchSQLtHarvest(string pValue)
        {
            // use the Harvest UDT - this is the location for the harvest type.
            // returns the SQL string without blanks or special characters and upshifted.
            // used to create a vaule to looking for duplicate entries of a name - ONLY CHECK FOR FULL HARVESTS.
            pValue = Search(pValue);
            //var sql = $"SELECT CASE WHEN  EXISTS(SELECT TOP 1 * FROM [{pTable}] WHERE {pField} = '{pValue}') THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END";
            var sql = $"SELECT CASE WHEN EXISTS(SELECT TOP 1 * FROM [@{NSC_DI.Globals.tHarvest}] WHERE {"U_NameSearch"} = '{pValue}' AND U_HarvestType = 'F') THEN 1 ELSE 0 END";
            return sql;
        }

        public static string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        public static string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        public static string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        public static bool Empty(string pVal)
        {
            //return ((pVal ?? "").Trim() == "");
            //I REPLACED THIS METHOD WITH THE MORE EFFECTIVE:
            return string.IsNullOrEmpty(pVal?.Trim());
        }
    }
}
