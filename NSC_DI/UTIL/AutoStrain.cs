﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbobsCOM;

namespace NSC_DI.UTIL
{
    public class AutoStrain
    {
        public class AutoItemInfo
        {
            public AutoItemInfo(string pAutoItemCode, string pItemCode, string pItemName)
            {
                AutoItemCode = pAutoItemCode;
                ItemCode = pItemCode;
                ItemName = pItemName;
            }

            public string AutoItemCode { get; }
            public string ItemCode { get; }
            public string ItemName { get; }
        }

        public static IEnumerable<AutoItemInfo> GetAutoItemsInfo(string strainID, string strainName)
        {
            var qry = $"SELECT U_ItemCode AS Code, U_ItemName AS Name FROM [@{Globals.tAutoItem}] WHERE {SQL.QueryClauses.MicroVertical} AND {SQL.QueryClauses.WetCannabis}";
            if (UTIL.Settings.Value.Get("Use Template Items") == "Y")
            {
                qry = "SELECT ItemCode AS [Code], ItemName AS [Name] FROM OITM WITH (NOLOCK) WHERE QryGroup64 = 'Y'";
            }
            var dtAutoItem = SQL.DataTable(qry);
          
            foreach (var dr in dtAutoItem.Select())
            {
              
                var itemCode = GetAutoItemCodeValue(dr["Code"] as string, strainID);
                var itemName = GetAutoItemNameValue(dr["Name"] as string, strainName);

                yield return new AutoItemInfo(dr["Code"] as string, itemCode, itemName);
            }
        }

        public static string GetTemplate(string pProperty)
        {
            if (string.IsNullOrEmpty(pProperty?.Trim())) return null;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pProperty))

            try
            {
                var sql = $@"SELECT COUNT(ItemCode) FROM OITM WHERE OITM.{NSC_DI.SAP.Items.Property.GetFieldName("Template")} = 'Y' AND OITM.{NSC_DI.SAP.Items.Property.GetFieldName(pProperty)} = 'Y'";
                var cnt = NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);
                if (cnt == 0) throw new Exception(Message.Format($"Template Item not found for Property {pProperty}."));
                if (cnt > 1) return "~DUFL";    // throw new Exception(Message.Format($"{cnt} Template Items were found for Property {pProperty}."));
                
                return NSC_DI.UTIL.SQL.GetValue<string>($@"SELECT ItemCode FROM OITM WHERE OITM.{NSC_DI.SAP.Items.Property.GetFieldName("Template")} = 'Y' AND OITM.{NSC_DI.SAP.Items.Property.GetFieldName(pProperty)} = 'Y'", null);
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex), ex);
            }
            finally
            {
                //Misc.KillObject(oRS);
                GC.Collect();
            }
        }

        public static string GetAutoItemCodeValue(string autoItemCode, string strainID)
        {
            if (string.IsNullOrEmpty(autoItemCode?.Trim()))// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(autoItemCode))
            {
                throw new Exception(NSC_DI.UTIL.Message.Format("The Item Code does not have a valid value. \n\r There is a problem with your template items"));
            }
            var retCode = String.Empty;
            try
            {
                if (autoItemCode[0] == '~')
                {
                    if (autoItemCode.Length > 1)
                    {
                        string fmt = BuildFormat("Item Code", oAutoItemCodeComponents);
                        retCode = string.Format(fmt, autoItemCode.Replace("~", ""), strainID);
                    }
                    else retCode = strainID;
                }
                else retCode = autoItemCode;
                return retCode;
            }
            catch(Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static string GetAutoItemNameValue(string autoItemName, string strainName)
        {
            if (string.IsNullOrEmpty(autoItemName?.Trim()))// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(autoItemName))
            {
                throw new Exception(NSC_DI.UTIL.Message.Format("The Item Name does not have a valid value. \n\r There is a problem with your template items"));
            }
            var retCode = String.Empty;
            try
            {
                if (autoItemName[0] == '~')
                {
                    if (autoItemName.Length > 1)
                    {
                        string fmt = BuildFormat("Item Name", oAutoItemNameComponents);
                        retCode = string.Format(fmt, autoItemName.Replace("~", ""), strainName);
                    }
                    else retCode = strainName;
                }
                else retCode = autoItemName;
                return retCode;
            }
            catch(Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            
        }

        [Obsolete("Don't use - not complete", true)]
        private static string GetComponent(string pType, string pComponent, string pValue)
        {
            // change the code to work backwards

            Recordset oRS = null;
            if (string.IsNullOrEmpty(pType?.Trim()) || string.IsNullOrEmpty(pComponent?.Trim())) return null;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pType) || NSC_DI.UTIL.Strings.Empty(pComponent))

            try
            {
                oRS = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                var sql = $"SELECT U_Component, ISNULL(U_Prefix, ''), ISNULL(U_Suffix, '') FROM [@{NSC_DI.Globals.tAutoStruct} WHERE U_Type = {pType} ORDER BY U_Position";
                oRS.DoQuery(sql);
                if (oRS.RecordCount < 1) throw new Exception(Message.Format($"No entry found in {NSC_DI.Globals.tAutoStruct}"));

                var val = "";
                var ret = "";
                int indx = 0;

                for (int i = 1; i <= oRS.RecordCount; i++)
                {
                    if (i > 1) oRS.MoveNext();

                    string comp = oRS.Fields.Item("U_Component").Value.ToString();
                    string prefix = oRS.Fields.Item("U_Prefix").Value.ToString();
                    string suffix = oRS.Fields.Item("U_Suffix").Value.ToString();

                    if (oRS.RecordCount > 1 && prefix.Length == 0 && suffix.Length == 0) throw new Exception(Message.Format("Multiple cmponents and no seperator."));

                    int len = pValue.Length;

                    if (suffix.Length > 0) len = pValue.IndexOf(suffix);

                    //-----------------------------
                    // found the component
                    if (pComponent.ToUpper() == comp.ToUpper())
                    {
                        // have to get the length
                        if (i < oRS.RecordCount && suffix.Length == 0)
                        {
                            oRS.MoveNext();
                            prefix = oRS.Fields.Item("U_Prefix").Value.ToString();
                            if (prefix.Length == 0) throw new Exception(Message.Format("Multiple cmponents and no seperator."));

                        }

                        return pValue.Substring(prefix.Length, len - prefix.Length);
                    }

                    pValue = pValue.Substring(len) + suffix.Length;

                    // found the component - have to find the length
                    if (pComponent.ToUpper() == comp.ToUpper())
                    {
                        if (i < oRS.RecordCount && suffix.Length > 0) len = pValue.IndexOf(suffix);
                    }

                    if (suffix.Length > 0) pValue = pValue.Substring(pValue.IndexOf(suffix) + suffix.Length);
                    if (prefix.Length > 0) indx = pValue.IndexOf(prefix);

                }
                return null;

            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex), ex);
            }
            finally
            {
                Misc.KillObject(oRS);
                GC.Collect();
            }
        }

        public static string NextStrainCode(string strainTypeCode)
        {
            UserTable sboTable = null;
            try
            {
                sboTable = Globals.oCompany.UserTables.Item(Globals.tStrainType);
                if (!sboTable.GetByKey(strainTypeCode))
                {
                    throw new Exception(Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }
                var prefix = sboTable.UserFields.Fields.Item("U_Prefix").Value as string;
                var fmt = BuildStrainIDFormat();
                return NextFromStrain(NSC_DI.Globals.tStrainType, strainTypeCode, "U_NextStrain").Select(n => String.Format(fmt, prefix, n)).Single();
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(sboTable);
                GC.Collect();
            }
        }
        public static IEnumerable<string> NextSN(string pItemCode, string type = "Serial Plant", int quantity = 1, string pInterCoSubCode = "")
        {
            // Will get the SN based off of the item master. Nearly identical to NextBatch.

            string fmt = null;
            if (pInterCoSubCode.Trim().Length > 0)
            {
                fmt = BuildFormat(type, oSNComponents, pInterCoSubCode);
            }
            else
            {
                fmt = BuildFormat(type, oSNComponents);
            }
            //int lastNum = NSC_DI.SAP.BatchItems.GetLastNum(pItemCode);
            int lastNum = GetLastNumSerial(pItemCode, pInterCoSubCode, "P");
            return Next(pItemCode, quantity, lastNum).Select(n => String.Format(fmt, pItemCode, n));
        }

        public static IEnumerable<string> NextSNFromStrain(string pItemCode, string type = "Serial Plant", int quantity = 1)
        {
			// a "strain" can be passed in as well because every strain has an associated item of the same name.
			// passing the ItemCode just give a little mor flexability

			var strainID = SAP.Items.GetStrainID(pItemCode);
            var strainType = GetStrainTypeFromStrain(strainID);
            var fmt = BuildFormat(type, oSNComponents);
            return NextFromStrain(NSC_DI.Globals.tStrains, strainID, "U_NextSN", quantity).Select(n => String.Format(fmt, strainID, n));
        }
        public static int GetLastNumSerial(string pItemCode, string pInterCoCode = "", string pPrefix = "P")
        {
            // Get the last serial number from the master table
            // data Collect could have created entries and it does not update the NextNum on the Item Master
            try
            {
                string sql;
                if (pInterCoCode.Trim() != "")
                {
                    // for inter-company
                    sql = $@"
SELECT TOP 1 DistNumber FROM OSRN 
 WHERE ItemCode = '{pItemCode}' AND DistNumber LIKE '{pInterCoCode}-{pItemCode}-{pPrefix}-%'
 ORDER BY AbsEntry DESC";
                }
                else
                {
                    // for standard batches
                    sql = $@"
SELECT TOP 1 DistNumber FROM OSRN 
 WHERE ItemCode = '{pItemCode}' AND DistNumber LIKE '{pItemCode}-{pPrefix}-%'
 ORDER BY AbsEntry DESC";
                }
                var id = UTIL.SQL.GetValue<string>(sql, null);
                if (id == null)
                {
                    // For opening balances
                    string OBsql = $@"
SELECT TOP 1 DistNumber FROM OSRN 
 WHERE ItemCode = '{pItemCode}' AND DistNumber LIKE 'OB-{pItemCode}-{pPrefix}-%'
 ORDER BY AbsEntry DESC";
                    id = UTIL.SQL.GetValue<string>(OBsql, null);

                    if (id == null) return 0;
                }               

                var indx = id.LastIndexOf("-") + 1;
                var val = id.Substring(indx);
                return int.Parse(val);
            }
            catch (Exception ex)
            {

                throw new Exception(UTIL.Message.Format(ex));
            }
        }
        public static IEnumerable<string> NextBatchFromStrain(string pItemCode, string batchCode = "B", int quantity = 1)
        {
            // a "strain" can be passed in as well because every strain has an associated item of the same name.
            // passing the ItemCode just give a little mor flexability

            var strainID = SAP.Items.GetStrainID(pItemCode);
            var strainType = GetStrainTypeFromStrain(strainID);
            var fmt = BuildBatchFormat();
            return NextFromStrain(NSC_DI.Globals.tStrains, strainID, "U_NextBatch", quantity).Select(n => String.Format(fmt, strainID, n, batchCode));
        }

        public static string NextSubBatch(string pItemCode, string pBatchID)
        {
            try
            {
                return NextSubBatch(pItemCode, pBatchID, 1).First();
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static IEnumerable<string> NextSubBatch(string pItemCode, string pBatchID, int quantity)
        {
            SAPbobsCOM.Recordset oRecordSet = null;
            try
            {
                oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                string qry = $"SELECT TOP 1 U_{NSC_DI.Globals.SAP_PartnerCode}_NextSubNum AS Next FROM OBTN WHERE ItemCode = '{UTIL.SQL.FixSQL(pItemCode)}' AND DistNumber= '{SQL.FixSQL(pBatchID)}'";

                oRecordSet.DoQuery(qry);

                if (oRecordSet.RecordCount != 1) throw new Exception("No Record Found for Batch ID: " + pBatchID);

				var next = oRecordSet.Fields.Item("Next").Value.ToString();
				if (!int.TryParse(next, out int nextID) || nextID < 1) nextID = 1;

                var fmt = BuildSubBatchFormat();

                // check if the batch is already a sub-batch
                var i = pBatchID.IndexOf("-SB-");
                if (i > 0) pBatchID = pBatchID.Substring(0, i);
                
                // check to see if the next batch already exists - if it does, increment the next number
                while (true)
                {
                    qry = $"EXEC('UPDATE OBTN SET U_{NSC_DI.Globals.SAP_PartnerCode}_NextSubNum = ''{(nextID + quantity)}'' WHERE ItemCode = ''{UTIL.SQL.FixSQL(pItemCode)}'' AND DistNumber= ''{SQL.FixSQL(pBatchID)}'' ')";
                    oRecordSet.DoQuery(qry);
                    var val = string.Format(fmt, pBatchID, nextID);

                    qry = $"SELECT DistNumber FROM OBTN WHERE ItemCode = '{UTIL.SQL.FixSQL(pItemCode)}' AND DistNumber= '{val}'";
                    var ss = NSC_DI.UTIL.SQL.GetValue<string>(qry, "");
                    if (NSC_DI.UTIL.SQL.GetValue<string>(qry, "") != val) break;
                    nextID++;
                }
    
                return Enumerable.Range(nextID, quantity).Select(n => n.ToString()).Select(n => string.Format(fmt, pBatchID, n));
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(oRecordSet);
                GC.Collect();
            }
        }

        public static IEnumerable<string> Next(string pItemCode, int quantity = 1, int pLastNum = -1)
        {
            try
            {
                int nextID = pLastNum + 1; 
                 return Enumerable.Range(nextID, quantity).Select(n => n.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex), ex);
            }
            finally
            {
                GC.Collect();
            }
        }

        private static IEnumerable<string> Next_OLD(string pItemCode, int quantity = 1, int pLastNum = -1)
        {
            // this version uses the nextNum UDF on the ItemMaster
            Items oItem = null;
            try
            {
                oItem = Globals.oCompany.GetBusinessObject(BoObjectTypes.oItems);
                if (!oItem.GetByKey(pItemCode.Trim()))
                {
                    throw new Exception(Globals.oCompany.GetLastErrorDescription());
                }

                if (int.TryParse(oItem.UserFields.Fields.Item($"U_{NSC_DI.Globals.SAP_PartnerCode}_NextNum").Value.ToString(), out int nextID) == false || nextID < 1) nextID = 1;
                if (pLastNum >= 0 && nextID <= pLastNum) nextID = pLastNum + 1; // data collect could have crated numbers
                oItem.UserFields.Fields.Item($"U_{NSC_DI.Globals.SAP_PartnerCode}_NextNum").Value = (nextID + quantity).ToString();

                if (oItem.Update() < 0)
                {
                    throw new Exception(Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }
                return Enumerable.Range(nextID, quantity).Select(n => n.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex), ex);
            }
            finally
            {
                Misc.KillObject(oItem);
                GC.Collect();
            }
        }

        private static IEnumerable<string> NextFromStrain(string pTable, string pCode, string udf, int quantity = 1)
        {
			// the table will be either StrainType (for the next strain number), or Strain for the Next SN or Batch for that strain

            UserTable sboTable = null;
            try
            {
                sboTable = Globals.oCompany.UserTables.Item(pTable);
                if (!sboTable.GetByKey(pCode.Trim()))
                {
                    throw new Exception(Globals.oCompany.GetLastErrorDescription());
                }

				if (int.TryParse(sboTable.UserFields.Fields.Item(udf).Value.ToString(), out int nextID) == false || nextID < 1) nextID = 1;

				sboTable.UserFields.Fields.Item(udf).Value = (nextID + quantity).ToString();

                if (sboTable.Update() < 0)
                {
                    throw new Exception(Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }
                return Enumerable.Range(nextID, quantity).Select(n => n.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex), ex);
            }
            finally
            {
                Misc.KillObject(sboTable);
                GC.Collect();
            }
        }

        private static string GetStrainTypeFromStrain(string pStrainId)
        {
            var qry = $"SELECT U_Type FROM [@{Globals.tStrains}] WHERE Code = '{pStrainId}'";
            return SQL.GetValue<string>(qry, String.Empty);
        }

        public static string NextTagNumber(string pTag, int pOffset)
        {
            if (pTag.Trim().Length < 1) return "";
            int digits = 7;

            try
            {
                int iTag = 0;
                string sTag = "";

                if (pTag.Length <= digits)
                {
                    //iTag = Convert.ToInt32(pTag);
                    if (Int32.TryParse(pTag, out iTag) == false) return "";
                }
                else
                {
                    //if (Int32.TryParse(pTag, out iTag) == false) return "";
                    if (Int32.TryParse(NSC_DI.UTIL.Strings.Right(pTag, digits), out iTag) == false) return "";
                }

                iTag += pOffset;
                if (pTag.Length <= digits)
                    sTag = iTag.ToString();
                else
                    sTag = NSC_DI.UTIL.Strings.Left(pTag, pTag.Length - digits) + iTag.ToString().PadLeft(digits, '0');

                return sTag;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static readonly Dictionary<string, string> oAutoItemCodeComponents = new Dictionary<string, string>()
        {
            { "#Item Code", "{0}" },
            { "#Strain ID", "{1}" }
        };

        private static readonly Dictionary<string, string> oAutoItemNameComponents = new Dictionary<string, string>()
        {
            { "#Item Name", "{0}" },
            { "#Strain Name", "{1}" }
        };

        private static readonly Dictionary<string, string> oStrainIDComponents = new Dictionary<string, string>()
        {
            { "#Strain Code", "{0}" },
            { "#Number", "{1}" }
        };

        private static string BuildStrainIDFormat() => BuildFormat("Strain ID", oStrainIDComponents);

        private static readonly Dictionary<string, string> oSNComponents = new Dictionary<string, string>()
        {
            { "#Strain ID", "{0}" },
            { "#Number", "{1}" }
        };

        private static readonly Dictionary<string, string> oBatchComponents = new Dictionary<string, string>()
        {
            { "#Strain ID", "{0}" },
            { "#Number", "{1}" },
            { "#BatchCode", "{2}" },
        };

        private static readonly Dictionary<string, string> oSubBatchComponents = new Dictionary<string, string>()
        {
            { "#BatchID", "{0}" },
            { "#Number", "{1}" }
        };

        public static string BuildBatchFormat() => BuildFormat("Batch", oBatchComponents);

        public static string BuildSubBatchFormat() => BuildFormat("Sub Batch", oSubBatchComponents);

        //public static string GenerateStrainName(string pStrainName, string pItemGroupName)
        //{
        //    return string.Format(BuildStrainNameFormat(), pStrainName, pItemGroupName);
        //}

        //private static readonly Dictionary<string, string> oStrainNameComponents = new Dictionary<string, string>()
        //{
        //    { "#Strain Name", "{0}" },
        //    { "#Item Group Name", "{1}" }
        //};
        //private static string BuildStrainNameFormat()
        //{
        //    return BuildFormat("Strain Name", oStrainNameComponents);
        //}

        private static string BuildFormat(string type, Dictionary<string, string> mapping, string pInterCoSubCode = "")
        {
            try
            {
                var qry = $"SELECT U_Component, U_Prefix, U_Suffix FROM [@{Globals.tAutoStruct}] WHERE U_Type = '{type}' ORDER BY U_Position";
                var qryRes = SQL.DataTable(qry);
                var sb = new StringBuilder();
                foreach (var dr in qryRes.Select())
                {
                    var prefix = dr["U_Prefix"];
                    sb.Append(prefix);
                    var componentName = dr["U_Component"] as string;
                    if (componentName.Any() && componentName[0] == '#')
                    {
                        if (!mapping.ContainsKey(componentName))
                        {
                            throw new Exception($"'{componentName}' is not  a recognized  '{type}' component");
                        }
                        sb.Append(mapping[componentName]);
                    }
                    else
                    {
                        sb.Append(componentName);
                    }
                    var suffix = dr["U_Suffix"];
                    sb.Append(suffix);
                }
                if (pInterCoSubCode.Trim().Length > 0)
                {
                    return pInterCoSubCode + "-" + sb.ToString();
                }
                else
                {
                    return sb.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex), ex);
            }
        }

    }
}
