﻿using System;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace NSC_DI.UTIL
{
    public static class UriUtil
    {
        public static string GetShortLink(string longUri)
        {
            using (var client = new WebClient())
            {
                client.Headers["Content-Type"] = "application/json";
                var apiKey = Settings.Value.Get("GoogleApiKey", true);
                var requestUri = "https://www.googleapis.com/urlshortener/v1/url?key=" + apiKey;
                var requestJson = String.Format("{{\"longUrl\": \"{0}\"}}", longUri);
                var responseJson = client.UploadString(requestUri, requestJson);
                var response = JObject.Parse(responseJson);
                var uri = (string)response["id"];
                return uri;
            }
        }

        public static string GetGoogleMapsUri(string address)
        {
            address = Regex.Replace(address, "\r+", ", ");
            return "https://www.google.com/maps/search/?api=1&query=" + Uri.EscapeUriString(address).Replace("%20", "+");
        }

        public static string GetGoogleMapsShortUri(string address)
        {
            return GetShortLink(GetGoogleMapsUri(address));
        }
	}
}
