﻿using System;
using SAPbobsCOM;

namespace NSC_DI.UTIL
{
	public static class Settings
	{
		private static string sql = "";

		public static int GetCode(string pName)
		{
			// get the Code using the Nmae
			sql = string.Format("SELECT Code FROM [@{0}] WHERE Name = '{1}'", Globals.tSettings, pName);
			return SQL.GetValue<int>(sql, 0);
		}

        public static void Delete(string pName)
        {
            // delete the entry using the Name
            UserTable oUDT = null;

            try
            {
                sql = string.Format("SELECT Code FROM [@{0}] WHERE Name = '{1}'", Globals.tSettings, pName);
                var code = SQL.GetValue<string>(sql, "0");
                oUDT = Globals.oCompany.UserTables.Item(Globals.tSettings);

                if (oUDT.GetByKey(code) == false) return;
                oUDT.Remove();
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        public static void SetDesc(string pName, string pDesc = "")
        {
            UserTable oUDT = null;

            try
            {

                var code = GetCode(pName).ToString();
                oUDT = Globals.oCompany.UserTables.Item(Globals.tSettings);

                if (oUDT.GetByKey(code))
                {
                    if (String.IsNullOrEmpty(pDesc)) return;
                    oUDT.UserFields.Fields.Item("U_Description").Value = pDesc;
                    oUDT.Update();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        // DELETE 20201230
        //public static string concatBatchNum(string interCoSubCode, string batchNumber)
        //{
        //    string InterCo = NSC_DI.UTIL.SQL.GetValue<string>("select T0.U_Value from dbo.[@NSC_SETTINGS] T0 where T0.Name = 'Inter-Company'");
        //    string newBatchNumber = batchNumber;
        //    try
        //    {
        //        if (InterCo == "Y" && interCoSubCode.Trim().Length > 0)
        //        {
        //            newBatchNumber = interCoSubCode.Trim() + "-" + batchNumber;
        //            return newBatchNumber;
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(NSC_DI.UTIL.Message.Format("Inter-Company Error: Please check your settings (Y or N) and make sure you have a subsidiary code on your warehouse."));
        //    }
        //    return newBatchNumber;
        //}

        public static class Version
		{
			public static bool ProductChanged(string pVersion)
			{
				var ver = GetProduct();
				if (ver == pVersion) return false;

				SetProduct(pVersion);
				return true;
			}

			public static string GetProduct()
			{
				var x64 = (IntPtr.Size == 8);

				sql = string.Format("SELECT U_Value FROM [@{0}] WHERE Name = 'Version'", Globals.tSettings);
				return SQL.GetValue<string>(sql);
			}

			public static void SetProduct(string pVersion)
			{
				UserTable oUserTable = null;
				try
				{
					var code = GetCode("Version").ToString();
					oUserTable = Globals.oCompany.UserTables.Item(Globals.tSettings);
					if (oUserTable.GetByKey(code))
					{
						oUserTable.UserFields.Fields.Item("U_Value").Value = pVersion;
						oUserTable.Update();
					}
					else
					{
						oUserTable.Name = "Version";
						oUserTable.UserFields.Fields.Item("U_Value").Value = pVersion;
						oUserTable.Add();
					}
				}
				catch (Exception ex)
				{
					throw new Exception(Message.Format(ex));
				}
				finally
				{
					if (oUserTable != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserTable);
					oUserTable = null;
					GC.Collect();
				}
			}

			public static bool DBChanged(string pVersion)
			{
				var ver = GetDB();
				return ver != pVersion;
			}

			public static string GetDB()
			{
				sql = string.Format("SELECT U_Value FROM [@{0}] WHERE Name = 'DB Version'", Globals.tSettings);
				return SQL.GetValue<string>(sql);
			}

			public static bool SetDB(string pVersion)
			{
				UserTable oUserTable = null;
				try
				{
					var code = GetCode("DB Version").ToString();
					oUserTable = Globals.oCompany.UserTables.Item(Globals.tSettings);
					if (oUserTable.GetByKey(code))
					{
						oUserTable.UserFields.Fields.Item("U_Value").Value = pVersion;
						oUserTable.Update();
					}
					else
					{
						oUserTable.Name = "DB Version";
						oUserTable.UserFields.Fields.Item("U_Value").Value = pVersion;
						oUserTable.Add();
					}
					return true;
				}
				catch (Exception ex)
				{
					throw new Exception(Message.Format(ex));
				}
				finally
				{
					if (oUserTable != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserTable);
					oUserTable = null;
					GC.Collect();
				}
			}

            public static string GetCompliance()
            {
                sql = $"SELECT U_Value FROM [@{Globals.tSettings}] WHERE Name = 'State Compliance'";
                return SQL.GetValue<string>(sql).ToUpper();
            }
        }

        public static class Value
		{
			public static string Get(string pName, bool pDecrypt = false)
			{
				if (pName.Trim() == "") return null;

				UserTable oUDT = null;
				try
				{
					var code = GetCode(pName).ToString();
					oUDT = Globals.oCompany.UserTables.Item(Globals.tSettings);
					if (oUDT.GetByKey(code) == false)
					{
						// entry does not exist. create it.
						oUDT.Name = pName;
						oUDT.UserFields.Fields.Item("U_Value").Value = "";
						oUDT.Add();
					}

                    var val = oUDT.UserFields.Fields.Item("U_Value").Value as string; 
					if (pDecrypt && val.Trim() != "") val = Encryption.Decrypt(val);

					return val;
				}
				catch (Exception ex)
				{
					throw new Exception(Message.Format(ex));
				}
				finally
				{
                    NSC_DI.UTIL.Misc.KillObject(oUDT);
					GC.Collect();
				}
			}

			public static void Set(string pName, string pValue, string pDesc = "", bool pOverwrite = true, bool pEncrypt = false)
			{
                // pOverwrite - true= overwrite filled value, false= overwrite null or empty value
                UserTable oUDT = null;

                try
                {
					if (pEncrypt) pValue = Encryption.Encrypt(pValue);

					var code = GetCode(pName).ToString();
					oUDT = Globals.oCompany.UserTables.Item(Globals.tSettings);
                    
                    if (oUDT.GetByKey(code))
					{
                        var oVal = oUDT.UserFields.Fields.Item("U_Value").Value;
                        if (String.IsNullOrEmpty(oVal) || pOverwrite) oUDT.UserFields.Fields.Item("U_Value").Value = pValue;
                        if (String.IsNullOrEmpty(pDesc) == false && pOverwrite) oUDT.UserFields.Fields.Item("U_Description").Value = pDesc;
                        oUDT.Update();
					}
					else
					{
						oUDT.Name = pName;
						oUDT.UserFields.Fields.Item("U_Value").Value = pValue;
						oUDT.UserFields.Fields.Item("U_Description").Value = pDesc;
						oUDT.Add();
					}
				}
				catch (Exception ex)
				{
					throw new Exception(Message.Format(ex));
				}
				finally
				{
                    NSC_DI.UTIL.Misc.KillObject(oUDT);
					GC.Collect();
				}
			}

            public static void Set(string pName, string[] pValues, bool pOverwrite = true, string pDescription = null)
            {
                // pOverwrite - true= overwrite filled value, false= overwrite null or empty value
                UserTable oUDT = null;

                try
                {
                    var code = GetCode(pName).ToString();
                    oUDT = Globals.oCompany.UserTables.Item(Globals.tUserOptions);
                    var userFieldsCount = oUDT.UserFields.Fields.Count - 1;
                    if (userFieldsCount < pValues.Length) throw new ArgumentException("pValues", "Parameter length exceeds the user defined fields length. No fields have been changed.");

                    if (!oUDT.GetByKey(code))
                    {
                        oUDT.Name = pName;
                        oUDT.Add();
                        code = GetCode(pName).ToString();
                        oUDT.GetByKey(code);
                    }

                    if (pDescription != null)
                    {
                        var oDescription = oUDT.UserFields.Fields.Item("U_Description").Value;
                        if (String.IsNullOrEmpty(oDescription) || pOverwrite) oUDT.UserFields.Fields.Item("U_Description").Value = pDescription;
                    }
                    for (int i = 0; i < pValues.Length; i++)
                    {
                        var oValueId = "U_Value" + (i + 1);
                        var oVal = oUDT.UserFields.Fields.Item(oValueId).Value;
                        if (String.IsNullOrEmpty(oVal) || pOverwrite) oUDT.UserFields.Fields.Item(oValueId).Value = pValues[i];
                    }
                    oUDT.Update();

                }
                catch (Exception ex)
                {
                    throw new Exception(Message.Format(ex));
                }
                finally
                {
                    Misc.KillObject(oUDT);
                    GC.Collect();
                }
            }
        }
    }
}