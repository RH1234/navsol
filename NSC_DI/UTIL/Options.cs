﻿using System;
using SAPbobsCOM;

namespace NSC_DI.UTIL
{
    public static class Options
    {
        public static int GetCode(string pName)
        {
            // get the Code using the Nmae
            var sql = string.Format("SELECT Code FROM [@{0}] WHERE Name = '{1}'", Globals.tUserOptions, pName);
            return SQL.GetValue<int>(sql, 0);
        }

        public static class Value
        {
            public static string GetUseCFL_WareHouse()
            {
                try
                {
                    return NSC_DI.UTIL.Options.Value.GetSingle("Use CFL for Warehouses", 1, "N to use Drop Downs, Y to use CFLs", "N").ToUpper();
                }
                catch (Exception ex)
                {
                    throw new Exception(Message.Format(ex));
                }
                finally
                {
                    GC.Collect();
                }
            }
       
            public static string GetSingle(string pName, int pValueNum = 1, string pDesc = null, string pDefaultValue = null)
            {
                if (pName.Trim() == "") return null;

                UserTable oUDT = null;
                try
                {
                    var code = GetCode(pName).ToString();
                    oUDT = Globals.oCompany.UserTables.Item(Globals.tUserOptions);
                    var oValueId = "U_Value" + pValueNum;
                    if (oUDT.GetByKey(code) == false)
                    {

                        // entry does not exist. create it.
                        oUDT.Name = pName;
                        oUDT.UserFields.Fields.Item("U_Description").Value = pDesc ?? "";
                        oUDT.UserFields.Fields.Item(oValueId).Value = pDefaultValue ?? "";
                        if(oUDT.Add() != 0)
                        {
                            throw new Exception(Message.Format(Globals.oCompany.GetLastErrorDescription()));
                        }
                    }

                    var val = oUDT.UserFields.Fields.Item(oValueId).Value;
                    
                    return val;
                }
                catch (Exception ex)
                {
                    throw new Exception(Message.Format(ex));
                }
                finally
                {
                    Misc.KillObject(oUDT);
                    GC.Collect();
                }
            }

            public static string[] Get(string pName)
            {
                if (pName.Trim() == "") return null;

                UserTable oUserTable = null;
                try
                {
                    var code = GetCode(pName).ToString();
                    oUserTable = Globals.oCompany.UserTables.Item(Globals.tUserOptions);

                    var userFieldsCount = oUserTable.UserFields.Fields.Count - 1;
                    if (oUserTable.GetByKey(code) == false)
                    {
                        // entry does not exist. create it.
                        oUserTable.Name = pName;
                        for (int i = 1; i <= userFieldsCount; i++) oUserTable.UserFields.Fields.Item("U_Value" + i).Value = "";
                        oUserTable.Add();
                    }

                    var retAry = new string[userFieldsCount];
                    for (int i = 0; i < userFieldsCount; i++)
                    {
                        var val = oUserTable.UserFields.Fields.Item("U_Value" + (i + 1)).Value;
                        retAry[i] = val;
                    }

                    return retAry;
                }
                catch (Exception ex)
                {
                    throw new Exception(Message.Format(ex));
                }
                finally
                {
                    Misc.KillObject(oUserTable);
                    GC.Collect();
                }
            }

            public static void SetSingle(string pName, string pValue, int pValueNum=1, bool pOverwrite = true, string pDescription = null)
            {
                // pOverwrite - true= overwrite filled value, false= overwrite null or empty value
                UserTable oUserTable = null;

                try
                {
                    var code = GetCode(pName).ToString();
                    oUserTable = Globals.oCompany.UserTables.Item(Globals.tUserOptions);

                    if (!oUserTable.GetByKey(code))
                    {
                        oUserTable.Name = pName;
                        oUserTable.Add();
                        code = GetCode(pName).ToString();
                        oUserTable.GetByKey(code);
                    }

                    if (pDescription != null) { 
                        var oDescription = oUserTable.UserFields.Fields.Item("U_Description").Value;
                        if (String.IsNullOrEmpty(oDescription) || pOverwrite) oUserTable.UserFields.Fields.Item("U_Description").Value = pDescription;
                    }
                    var oValueID = "U_Value" + pValueNum;
                    var oVal = oUserTable.UserFields.Fields.Item(oValueID).Value;
                    if (String.IsNullOrEmpty(oVal) || pOverwrite) oUserTable.UserFields.Fields.Item(oValueID).Value = pValue;
                    oUserTable.Update();

                }
                catch (Exception ex)
                {
                    throw new Exception(Message.Format(ex));
                }
                finally
                {
                    Misc.KillObject(oUserTable);
                    GC.Collect();
                }
            }

            public static void Set(string pName, string[] pValues, bool pOverwrite = true, string pDescription = null)
            {
                // pOverwrite - true= overwrite filled value, false= overwrite null or empty value
                UserTable oUserTable = null;

                try
                {
                    var code = GetCode(pName).ToString();
                    oUserTable = Globals.oCompany.UserTables.Item(Globals.tUserOptions);
                    var userFieldsCount = oUserTable.UserFields.Fields.Count - 1;
                    if (userFieldsCount < pValues.Length) throw new ArgumentException("pValues", "Parameter length exceeds the user defined fields length. No fields have been changed.");

                    if (!oUserTable.GetByKey(code))
                    {
                        oUserTable.Name = pName;
                        oUserTable.Add();
                        code = GetCode(pName).ToString();
                        oUserTable.GetByKey(code);
                    }

                    if (pDescription != null)
                    {
                        var oDescription = oUserTable.UserFields.Fields.Item("U_Description").Value;
                        if (String.IsNullOrEmpty(oDescription) || pOverwrite) oUserTable.UserFields.Fields.Item("U_Description").Value = pDescription;
                    }
                    for (int i = 0; i < pValues.Length; i++)
                    {
                        var oValueId = "U_Value" + (i + 1);
                        var oVal = oUserTable.UserFields.Fields.Item(oValueId).Value;
                        if (String.IsNullOrEmpty(oVal) || pOverwrite) oUserTable.UserFields.Fields.Item(oValueId).Value = pValues[i];
                    }
                    oUserTable.Update();

                }
                catch (Exception ex)
                {
                    throw new Exception(Message.Format(ex));
                }
                finally
                {
                    Misc.KillObject(oUserTable);
                    GC.Collect();
                }
            }
        }
    }
}
