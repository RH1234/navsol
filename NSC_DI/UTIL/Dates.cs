﻿using System;
using Humanizer;

namespace NSC_DI.UTIL
{
    public class Dates
    {
        public static long CurrentTimeStamp
        {
            get
            {
                return ToTimeStamp(DateTime.Now);
            }
        }
        public static long ToTimeStamp(DateTime DateToConvert)
        {
            var dto = new DateTimeOffset(DateToConvert);

            return dto.ToUnixTimeSeconds();
        }

        public static DateTime FromTimeStamp(long unixTimeInSeconds)
        {
            //     Converts a Unix time expressed as the number of seconds that have elapsed since
            //     1970-01-01T00:00:00Z to a System.DateTimeOffset value.

            var dto = DateTimeOffset.FromUnixTimeSeconds(unixTimeInSeconds);

            return dto.LocalDateTime;
        }

        public static string GetFormattedTimespan(DateTime start)
        {
            return GetFormattedTimespan(start, DateTime.Now);
        }

        public static string GetFormattedTimespan(DateTime start, DateTime end)
        {
            var timespan = start - end;

            //return timespan.Humanize();
            return timespan.ToString();
        }

        public static string GetFormattedDate(long unixTimeInSeconds)
        {
            //return FromTimeStamp(unixTimeInSeconds).Humanize();
            return FromTimeStamp(unixTimeInSeconds).ToString();
        }

        public static string GetForUI(DateTime pDate)
        {
            try
            {
                return pDate.ToString("yyyyMMdd");
        }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
    }
            finally
            {
            }
        }

        public static DateTime FromUI(string pDateStr)
        {
            try
            {
                DateTime val = DateTime.ParseExact(pDateStr, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                return val;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
        public static TimeSpan SAPTimeToTimeSpan(string SAPTime)
        {
            SAPTime = SAPTime.PadLeft(4, '0');
            TimeSpan Time = new TimeSpan(int.Parse(SAPTime.Substring(0, 2)), int.Parse(SAPTime.Substring(2, 2)), 0);

            return Time;
        }

        public static void GridDate()
        {
            //This will set a datepicker in the grid to null
//            SELECT[OWOR].[DocEntry] as 'Production Order', [OWOR].[PlannedQty] as 'Total Quantity', [OITM].InvntryUom as 'UoM', OBTN.MnfSerial as StateID,
//(SELECT OITL.DocDate FROM OITL WHERE DocDate <= '12/31/1950') AS ExpDate -- This line is so that we can get a null date in the Grid column
//FROM[OWOR]
        }

        public class Quarantine
        {
            public static TimeSpan DestroyDuration
            {
                get
                {
                    if (!int.TryParse(NSC_DI.UTIL.Options.Value.GetSingle("QuarantineTimer"), out int duration)) duration = 72;
                    return new TimeSpan(duration, 0, 0);
                }
            }

            public static DateTime DestroyDate
            {
                get
                {
                    return DateTime.Now.Add(DestroyDuration.Negate());
                }
            }

            public static long DestroyTimeStamp
            {
                get
                { 
                    var ts = DestroyDuration.Negate();

                    return ToTimeStamp(DateTime.Now.Add(ts));
                }
            }
        }
    }
}
