﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QRCoder;
using System.Drawing;
using System.Drawing.Imaging;

namespace NSC_DI.UTIL
{
    public static class QRCodeUtil
    {
        public static void CreateMapsQRCode(string address, string destPath, int size=250)
        {
            CreateMapsQRCode(address, destPath, ImageFormat.Png, size);
        }
        public static void CreateMapsQRCode(string address, string destPath, ImageFormat format, int size = 250)
        {
            try
            {
                var uri = UriUtil.GetGoogleMapsUri(address);

                var qrGenerator = new QRCodeGenerator();
                var qrCodeData = qrGenerator.CreateQrCode(uri, QRCodeGenerator.ECCLevel.M);
                var qrCode = new QRCode(qrCodeData);
                var qrCodeAsBitmap = qrCode.GetGraphic(20);
                var resizedQrCode = new Bitmap(qrCodeAsBitmap, new Size(size, size));
                resizedQrCode.Save(destPath, format);
            }
            catch (Exception ex)
            {
                //throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static string GetPath()
        {
            // return the QR Code folder
            try
            {
                var path = Globals.oCompany.BitMapPath;
                if (path == null) throw new Exception(NSC_DI.UTIL.Message.Format("Bitmap Path not specified."));

                var subPath = path += "QRCodes";

                // if sub folder does not exist, try to create it. Users will probably not have auth, so just use the standard SAP folder
                if (System.IO.Directory.Exists(subPath) == false)
                {
                    try
                    {
                        System.IO.Directory.CreateDirectory(subPath);
                        path = subPath;
                    }
                    catch { }
                }


                return path;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oBar);
                GC.Collect();
            }
        }
    }
}
