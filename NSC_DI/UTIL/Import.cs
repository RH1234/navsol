﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSC_DI.UTIL
{
    public static class Import
    {
        public static System.Data.DataTable LoadFromExcel(string pFile, string pSheet, ref string pMsg)
        {
            pMsg = "";
            if (string.IsNullOrEmpty(pFile))
            {
                return null;
            }
            if (!System.IO.File.Exists(pFile))
            {
                return null;
            }

            //System.Data.OleDb.OleDbConnection conn = null;

            try
            {
                // ";Extended Properties=""Text;HDR=Yes;FMT=Delimited\"""
                // ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"""
                //  "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strPath.Trim & ";Extended Properties=""text;HDR=Yes;FMT=Delimited\""" 

                string ConStr = "";
                System.Data.DataTable dtObj = new System.Data.DataTable();
                System.Data.DataSet ds = new System.Data.DataSet();
                string ss = System.IO.Path.GetDirectoryName(pFile);

                if (pFile.Substring(pFile.Length - 4, 4).ToUpper() == ".CSV")
                {
                    System.IO.FileInfo Finfo = new System.IO.FileInfo(pFile);
                    ConStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"" + Finfo.DirectoryName + "\";Extended Properties=\"Text;HDR=Yes;FMT=Delimited\"";
                    using (var conn = new System.Data.OleDb.OleDbConnection(ConStr))
                    {
                        conn.Open();
                        System.Data.OleDb.OleDbDataAdapter daObj = new System.Data.OleDb.OleDbDataAdapter("Select * from [" + Finfo.Name + "]", conn);
                        daObj.Fill(dtObj);
                    }
                }
                else
                {
                    if (pSheet == "") pSheet = "Sheet1";
                    ConStr = (Convert.ToString("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=") + pFile) + ";Extended Properties=Excel 12.0";
                    string query = "Select * From [" + pSheet + "$]";
                    using (var conn = new System.Data.OleDb.OleDbConnection(ConStr))
                    {
                        conn.Open();
                        System.Data.OleDb.OleDbCommand cmdObj = new System.Data.OleDb.OleDbCommand(query, conn);
                        System.Data.OleDb.OleDbDataAdapter daObj = new System.Data.OleDb.OleDbDataAdapter(cmdObj);
                        daObj.Fill(dtObj);
                    }
                }

                return dtObj;
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

    }
}
