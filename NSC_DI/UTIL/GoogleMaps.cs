﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Net;
using Newtonsoft.Json.Linq;

namespace NSC_DI.UTIL
{
    public class GoogleMaps
    {
        private static string GetFormattedOriginDestinations(string[] destinations)
        {
            var originAddress = SQL.GetValue<string>("select CONCAT(Street, ', ', Block, ', ', City, ', ', State, ', ', ZipCode, ', ', Country) as formatted_address from OWHS where U_NSC_WhrsType = 'FIN'");
            if (String.IsNullOrEmpty(originAddress)) throw new System.ComponentModel.WarningException("Finished Goods warehouse not defined");
            var origins = "origins=" + originAddress;
            var dest = "destinations=" + String.Join("|", destinations.Select(n => Regex.Replace(n, "[\r\n]+", ", ")));
            return $"{origins}&{dest}";
        }

        public static string[] GetCalcualatedDistances(string[] destinations)
        {
            if (destinations.Length == 0) return new string[0];
            var originDestStr = GetFormattedOriginDestinations(destinations);
            var apiKey = Settings.Value.Get("GoogleApiKey", true);
            var uri = $"https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&" + originDestStr + "&api=" + apiKey;
            using (var client = new WebClient())
            {
                client.Headers["Content-Type"] = "application/json";
                var responseJson = client.DownloadString(uri);
                var response = JObject.Parse(responseJson);
                if ((string)response["origin_addresses"][0] == "")
                    throw new System.ComponentModel.WarningException("Facility address is invalid");
                var distances = (JArray)response["rows"][0]["elements"];
                var retDists = new List<string>();
                for (int i = 0; i < distances.Count; i++)
                {
                    if ((string)distances[i]["status"] == "OK")
                    {
                        var distance = (string)distances[i]["duration"]["text"];
                        retDists.Add(distance);
                    }
                    else
                        retDists.Add("Not found");
                }
                return retDists.ToArray();
            }
        }
    }
}
