﻿using System;
using System.Runtime.InteropServices;
using SAPbobsCOM;

namespace NSC_DI.UTIL
{
    /// <summary>
    /// SQL Helper will help run SQL queries against the SQL database assigned to the SAP Company object passed to it
    /// </summary>
	public static class SQL
    {
		public static void RunSQLQuery(string SQLQuery)
        {
            // Prepare to run a SQL statement.
            Recordset oRecordSet = null; 

            try
            {
                if (Globals.oCompany.UseTrusted && Globals.oCompany.DbServerType != BoDataServerTypes.dst_HANADB && !Globals.oCompany.InTransaction)
                {
                    using (var con = new System.Data.SqlClient.SqlConnection("Server=" + Globals.oCompany.Server + ";Initial Catalog=" + Globals.oCompany.CompanyDB + ";Integrated Security=SSPI"))
                    {
                        con.Open();
                        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(SQLQuery, con);
                        int rows = cmd.ExecuteNonQuery();
                    }
                }
                // Run the SQL query against the database (SAP Will throw an exception if it fails to run)
                oRecordSet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                oRecordSet.DoQuery(SQLQuery);
            }
            catch (Exception ex)
            {
	            throw new Exception(Message.Format(ex));
            }
            finally
            {
				Misc.KillObject(oRecordSet);
                GC.Collect();
            }
        }

        /// <summary>
        /// Attempts to run a SQL query against the SQL database assigned to the SAP Company object and returns a singular string value from the SQL statement provided
        /// </summary>
        /// <param name="SQLQuery">The SQL query to run against the SQL database.</param>
        /// <param name="pRetColumnName">The column value to return.</param>
		public static string RunSQLQuery(string SQLQuery, string pRetColumnName)
        {
            // Prepare to run a SQL statement.
            Recordset oRecordSet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            try
            {
                // Run the SQL query against the database (SAP Will throw an exception if it fails to run)
                oRecordSet.DoQuery(SQLQuery);

                // Move to the first record
                oRecordSet.MoveFirst();

                if (pRetColumnName == null) return null;

                // Grab the value of the first record in the specified column
				return oRecordSet.Fields.Item(pRetColumnName).Value.ToString();
			}
            catch (Exception ex)
            {
				throw new Exception(Message.Format(ex));
			}
            finally
            {
				Misc.KillObject(oRecordSet);
				GC.Collect();
            }
        }

        /// <summary>
        /// Returns an SAP Business One RecordSet given a SQL query to run against the SQL Database
        /// </summary>
        /// <param name="pSql">The SQL query to run</param>
        /// <returns>Returns a filled RecordSet</returns>
		public static Recordset GetRecordSetFromSQLQuery(string pSql)
        {
            // Keep track of RecordSet if SQL query is successful
            Recordset _RecordSet = null;
            // Prepare to run a SQL statement.
            Recordset oRecordSet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            try
            {
                // Run the SQL query against the database (SAP Will throw an exception if it fails to run)
                oRecordSet.DoQuery(pSql);

                // Set our local RecordSet variable so we can flush memory of the SQL connection later
                _RecordSet = oRecordSet;
				return _RecordSet;
			}
            catch (Exception ex)
            {
				throw new Exception(Message.Format(ex));
			}
            finally
            {
				//Misc.KillObject(oRecordSet);
				GC.Collect();
            }
        }

        /// <summary>
        /// Returns an SAP Business One "Fields" object given a SQL query.  This will basically return the first row of the query, and allow you to access the returned columns.
        /// </summary>
        /// <param name="SQLQuery">The SQL query to run against the SQL database.</param>
        /// <returns>Returns an SAP Business One "Fields" object.</returns>
		public static Fields GetFieldsFromSQLQuery(string SQLQuery)
        {
            // Keep track of fields if SQL query is successful
            Fields _fields = null;
            // Prepare to run a SQL statement.
            Recordset oRecordSet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            try
            {
                // Run the SQL query against the database (SAP Will throw an exception if it fails to run)
                oRecordSet.DoQuery(SQLQuery);
                if (oRecordSet.RecordCount > 0)return oRecordSet.Fields;
	            return null;
            }
            catch (Exception ex)
            {
				throw new Exception(Message.Format(ex));
			}
            finally
            {
				//Misc.KillObject(oRecordSet);
				GC.Collect();
            }
        }

	    /// <summary>
	    /// Grabs the next available code given a database table name and a colmn to search in.
	    /// </summary>
	    /// <param name="DatabaseTableName"></param>
	    /// <param name="CodeColumnName"></param>
	    /// <returns></returns>
	    public static string GetNextAvailableCodeForItem(string DatabaseTableName, string CodeColumnName, int pIncrement = 1)
	    {
		    // Grab the next available code from the SQL database
			DatabaseTableName = SetTableName(DatabaseTableName);
            return GetValue<string>($"SELECT ISNULL((MAX(CONVERT(int,[{CodeColumnName}]))+{pIncrement}),{pIncrement}) FROM {SetTableName(DatabaseTableName)}");
	    }

	    //public static T GetValue<T>(string SQL)
		//{

		//	SAPbobsCOM.Recordset oRecordSet = null;
			
		//	try
		//	{
		//		oRecordSet = AddOn.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
		//		object result = new object();

		//		oRecordSet.DoQuery(SQL);

		//		//result = System.Convert.ChangeType(result, typeof(T));
		//		if (oRecordSet.EoF) return (T)System.Convert.ChangeType(result, typeof(T));

		//		result = oRecordSet.Fields.Item(0).Value;

		//		return (T)System.Convert.ChangeType(result, typeof(T));
		//	}
		//	catch (Exception ex)
		//	{
		//		throw new Exception(UTIL.Message.Format(ex));
		//	}
		//	finally
		//	{
		//		Util.KillObject(oRecordSet);
		//		GC.Collect();
		//	}
		//}

		public static T GetValue<T>(string SQL, object defaultValue = null)
		{

			Recordset oRecordset = null;

			try
			{
                // DIRECT SQL DOES NOT WORK WITH V10

                //// trusted SQL
                //if (Globals.oCompany.UseTrusted && Globals.oCompany.DbServerType != BoDataServerTypes.dst_HANADB && !Globals.oCompany.InTransaction)
                //{
                //    using (var con = new System.Data.SqlClient.SqlConnection("Server=" + Globals.oCompany.Server + ";Initial Catalog=" + Globals.oCompany.CompanyDB + ";Integrated Security=SSPI"))
                //    {
                //        con.Open();
                //        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(SQL, con);
                //        var ds = new System.Data.DataSet();
                //        var da = new System.Data.SqlClient.SqlDataAdapter(cmd);

                //        da.Fill(ds);
                //        var dt = ds.Tables[0];
                //        if (dt.Rows.Count > 0 && dt.Columns.Count > 0)
                //        {
                //            try
                //            {
                //                return (T)Convert.ChangeType(dt.Rows[0][0], typeof(T));
                //            }
                //            catch(Exception ex)
                //            {
                //                string strError = ex.Message;
                //            }
                //        }
                //        return (T)defaultValue;
                        
                //    }
                //}

                oRecordset = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
				object result = new object();

				oRecordset.DoQuery(SQL);

				//result = System.Convert.ChangeType(result, typeof(T));
				if (oRecordset.EoF) return (T)defaultValue; // (T)System.Convert.ChangeType(result, typeof(T));
                if (oRecordset.Fields.Item(0).Value == null) return (T)defaultValue;
                if (oRecordset.Fields.Item(0).IsNull() == BoYesNoEnum.tYES) return (T)defaultValue;

                result = oRecordset.Fields.Item(0).Value;

                return (T)System.Convert.ChangeType(result, typeof(T));
			}
			catch (Exception ex)
			{
				throw new Exception(Message.Format(ex));
			}
			finally
			{
				if (oRecordset != null) Marshal.ReleaseComObject(oRecordset);
				oRecordset = null;
				GC.Collect();
			}
		}

	    public static int GetCodeFromGUID(string pTable,string pGUID)
	    {
		    return GetValue<int>("SELECT Code FROM " + SetTableName(pTable) + " WHERE U_GUID = '" + pGUID + "'", 0);
	    }

		public static string FixSQL(string pStr)
		{
			// replace a single quote with 2 single quotes
			return pStr.Replace("'", "''");
		}

		public static string SetTableName(string sValue)
		{
			// if this is a UDT, prefix with [@ and suffix with ]
			if (sValue.Length < 4) return sValue;											// not a UDT
			if (sValue.StartsWith(Globals.SAP_PartnerCode + "_") == false) return sValue;	// not a UDT
			if (sValue.Substring(0, 1) != "@" && sValue.Substring(0, 1) != "[") return "[@" + sValue + "]";
			return sValue;
		}

		public static System.Data.DataTable DataTable(string strSQL, string Dataname = "Data")
		{

			Recordset oRecordSet = null;
			var myDT = new System.Data.DataTable(Dataname);

			try
			{
				// trusted SQL
				if (Globals.oCompany.UseTrusted && Globals.oCompany.DbServerType != BoDataServerTypes.dst_HANADB && !Globals.oCompany.InTransaction)
				{
                    using (var con = new System.Data.SqlClient.SqlConnection("Server=" + Globals.oCompany.Server + ";Initial Catalog=" + Globals.oCompany.CompanyDB + ";Integrated Security=SSPI"))
                    {
                        con.Open();
                        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(strSQL, con);
                        var ds = new System.Data.DataSet();
                        var da = new System.Data.SqlClient.SqlDataAdapter(cmd);

                        da.Fill(ds, Dataname);
                        ds.Tables[0].TableName = Dataname;
                        return ds.Tables[0].Copy();
                    }
				}

				// RecordSet
				oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
				oRecordSet.DoQuery(strSQL);
				myDT = ConvertRecordset(oRecordSet);
				return myDT;
			}
			catch (Exception ex)
			{
				throw new Exception(Message.Format(ex));
			}
			finally
			{
				Misc.KillObject(oRecordSet);
				GC.Collect();
			}
		}

	    public static System.Data.DataTable ConvertRecordset(Recordset pRS)
	    {

		    // This function will take an SAP recordset from the SAPbobsCOM library and convert it to a more
		    // easily used ADO.NET datatable which can be used for data binding much easier.

		    System.Data.DataTable dtTable = new System.Data.DataTable();
		    System.Data.DataColumn NewCol = null;
		    System.Data.DataRow NewRow = null;
		    int ColCount = 0;

		    try
		    {
			    for (ColCount = 0; ColCount <= pRS.Fields.Count - 1; ColCount++)
			    {
				    NewCol = new System.Data.DataColumn(pRS.Fields.Item(ColCount).Name);
				    dtTable.Columns.Add(NewCol);
			    }

				pRS.MoveFirst();

			    while (!pRS.EoF)
			    {
				    NewRow = dtTable.NewRow();
				    //populate each column in the row we're creating

				    for (ColCount = 0; ColCount <= pRS.Fields.Count - 1; ColCount++)
				    {
					    NewRow[pRS.Fields.Item(ColCount).Name] = pRS.Fields.Item(ColCount).Value;
				    }

				    //Add the row to the datatable
				    dtTable.Rows.Add(NewRow);
				    pRS.MoveNext();
			    }

			    pRS.MoveFirst();
			    return dtTable;
		    }
		    catch (Exception ex)
		    {
			    throw new Exception(Message.Format(ex));
		    }
		    finally
		    {
			    GC.Collect();
		    }
	    }
        public static void KillObject(object Obj)
        {
            int ticks = 1000000;
            if (Obj == null || !Marshal.IsComObject(Obj)) return;
            while (Marshal.FinalReleaseComObject(Obj) != 0 && ticks != 0) ticks--;
            if (ticks == 0) throw new Exception("COM object cannot be released");
            Obj = null;
        }
        public class QueryClauses
        {
            public static string MicroVertical
            {
                get
                {
                    return "((U_MV_Retail = 'Y' AND(SELECT U_Value FROM[@NSC_SETTINGS] WHERE Name = 'MV_Retail') = 'Y') OR " +
                       "(U_MV_Grow = 'Y' AND(SELECT U_Value FROM[@NSC_SETTINGS] WHERE Name = 'MV_Grow') = 'Y') OR " +
                       "(U_MV_Distributor = 'Y' AND(SELECT U_Value FROM[@NSC_SETTINGS] WHERE Name = 'MV_Distributor') = 'Y') OR " +
                       "(U_MV_Processor = 'Y' AND(SELECT U_Value FROM[@NSC_SETTINGS] WHERE Name = 'MV_Processor') = 'Y'))";
                }
            }
            public static string WetCannabis
            {
                get
                {
                    return "(U_ProcessType = 'WetCannabis' AND(SELECT U_Value FROM[@NSC_SETTINGS] WHERE Name = 'WetCannabisProcess') = 'Y' OR U_ProcessType = 'Standard')";
                }
            }
        }
    }

}