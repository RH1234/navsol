﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSC_DI.UTIL
{
    public class RemoteLogger
    {
        public static void WriteError(string pErr)
        {
            try
            {
                if (System.Diagnostics.Debugger.IsAttached) return;

                var client = new iConnect.DevClient();

                var log = new iConnect.DevLog();

                log.Build = Globals.ProdVersion;

                log.CreateDate = DateTime.UtcNow;

                log.ErrorText = pErr;

                log.TenantID = Globals.oCompany.CompanyName;

                log.Status = "New";

                log.ModuleProcess = "Cannabis";

                log.SubProduct = "Viridian";
                
                client.AddUpdateDevLog(log);
            }
            catch (Exception ex)
            {
            }

        }
    }
}
