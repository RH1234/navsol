﻿namespace NSC_DI
{
	public static class NSCDI
	{
		public static void Init(SAPbobsCOM.Company pCompany, string pProdVersion = null)
		{
			Globals.oCompany = pCompany;
            Globals.oCompService = pCompany.GetCompanyService();
            Globals.ProdVersion = pProdVersion;
        }
    }
}
