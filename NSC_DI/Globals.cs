﻿using SAPbobsCOM;

namespace NSC_DI
{
	public static class Globals
	{
		//public const bool IsCannabis = true;

		internal static Company			oCompany;
		internal static CompanyService	oCompService;
        public static string            InterCoCode = "";

        internal static string ProdVersion { get; set; }

		public const string				SAP_PartnerCode = "NSC";

		public const string SettingsItemPropPrefix = "Item Property for: ";    // the prefix of Item Property entry in the settings table.

		// user tables
		public const string tSettings		= SAP_PartnerCode + "_SETTINGS";
        public const string tSettingsMenu   = SAP_PartnerCode + "_SETTINGSMENU";
        public const string tUserOptions    = SAP_PartnerCode + "_USER_OPTIONS";

		public const string tAutoItem		= SAP_PartnerCode + "_AUTO_ITEM";
		public const string tAutoItemBOM	= SAP_PartnerCode + "_AUTO_ITEM_BOM";
		public const string tPDOPhase		= SAP_PartnerCode + "_PDO_PHASES";
		public const string tAutoStruct		= SAP_PartnerCode + "_AUTO_STRUCTURE";

		public const string tAirLog			= SAP_PartnerCode + "_AIRLOG";
        public const string tCompPackages   = SAP_PartnerCode + "_COMP_PACKAGES";
		public const string tCompliance		= SAP_PartnerCode + "_COMPLIANCE_LOG";
        public const string tCompTypes      = SAP_PartnerCode + "_COMP_TYPES";
        public const string tCompHarvest    = SAP_PartnerCode + "_COMP_HARVEST";
        public const string tCompounds		= SAP_PartnerCode + "_COMPOUND";
		public const string tCompEffect		= SAP_PartnerCode + "_COMP_EFFECT";
		public const string tDisease		= SAP_PartnerCode + "_DISEASE";
		public const string tDiseEffect		= SAP_PartnerCode + "_DISEASE_EFFECT";
		public const string tEffect			= SAP_PartnerCode + "_EFFECT";
		public const string tFeedLog		= SAP_PartnerCode + "_FEEDLOG";
		public const string tFilterLog		= SAP_PartnerCode + "_FLTRLOG";
		public const string tGBNote			= SAP_PartnerCode + "_GBNOTE";
		public const string tGBTask			= SAP_PartnerCode + "_GBTASK";
		public const string tGBTaskList		= SAP_PartnerCode + "_GBTASKLIST";
		public const string tGBTaskNote		= SAP_PartnerCode + "_GBTASKNOTE";
		public const string tHarvest		= SAP_PartnerCode + "_HARVEST";
		public const string tLightLife		= SAP_PartnerCode + "_LIGHTLIFE";
        public const string tPlants			= SAP_PartnerCode + "_PLANT";
		public const string tPlantJournal	= SAP_PartnerCode + "_PLANTJOURNAL";
        public const string tProdCategory   = SAP_PartnerCode + "_PRODCTGRY";
        public const string tRemediate      = SAP_PartnerCode + "_REMEDIATE";
		public const string tRouteDetls		= SAP_PartnerCode + "_ROUTE_DETAILS";
		public const string tRouteStops		= SAP_PartnerCode + "_ROUTE_STOPS";
		public const string tStrainType		= SAP_PartnerCode + "_STRAIN_TYPE";
		public const string tStrains		= SAP_PartnerCode + "_STRAIN";
        public const string tStateLocType   = SAP_PartnerCode + "_STATE_LOCATION";
		public const string tTests			= SAP_PartnerCode + "_TEST";
		public const string tTestTestField	= SAP_PartnerCode + "_TEST_TESTFIELD";
		public const string tTestComp		= SAP_PartnerCode + "_TEST_COMPOUND";
		public const string tTestSamp		= SAP_PartnerCode + "_TEST_SAMPLE";
		public const string tTCResult		= SAP_PartnerCode + "_TC_RESULT";
		public const string tTFResult		= SAP_PartnerCode + "_TF_RESULT";
		public const string tTestResults	= SAP_PartnerCode + "_TEST_RESULTS";
		public const string tTestField		= SAP_PartnerCode + "_TESTFIELD";
        public const string tTestChangeLog  = SAP_PartnerCode + "_TSTCHNGE_LOG";
        public const string tTreatLog		= SAP_PartnerCode + "_TREATLOG";
		public const string tVehicle		= SAP_PartnerCode + "_VEHICLE";
		public const string tWasteLog		= SAP_PartnerCode + "_WASTELOG";
		public const string tWaterLog		= SAP_PartnerCode + "_WATERLOG";
        public const string tWasteTypes     = SAP_PartnerCode + "_WASTETYPES";
        public const string tInvTypXFer     = SAP_PartnerCode + "_INVTYPEXFER";
        public const string tInvTypGdsIsu   = SAP_PartnerCode + "_INVTYPEGDSISU"; // obsolete
        public const string tBatSerTrace    = SAP_PartnerCode + "_BATCHSERIALTRA";
        public const string tDestructTypes  = SAP_PartnerCode + "_DESTRUCTTYPES";
        public const string tQuarantineType = SAP_PartnerCode + "_QUARANTINETYPE";
        public const string tInvAdjustTypes = SAP_PartnerCode + "_INVADJTYPES";
        public const string tWasteMeth      = SAP_PartnerCode + "_WASTEMETHOD";
        public const string tMixMats        = SAP_PartnerCode + "_MIXMATERIALS";

        public enum LiquidMeasurements { gal, liters, qt, pt, cups, ml, floz }

		public enum JournalEntryType { Water, Feed, Treat, Transfer, Waste, Manicure, Remarks }

		public enum QuarantineStates { NONE, SICK, DEST, SALE }

	    public enum SampleTestType { StandardTest = 0, ExtractTest = 1 }

	    public enum TestResultState { Success = 1, Failed = -1 }

	    public enum TestResultStatus { Failed = 0, Passed = 1 }

        public enum ComplianceAction { Add, Update, Delete, Get }

        public enum ComplianceStatus { Success, Failed }

        public enum ComplianceReasons
		{
            Employee_New,
            Inventory_Convert,
            Inventory_Create_Lot,
            Inventory_Destroy,
            Inventory_Destroy_Schedule,
            Inventory_Manifest,
            Inventory_Manifest_Void,
            Inventory_Move,
            Inventory_New,
            Inventory_QA_Sample,
            Inventory_Room_Add,
            Inventory_Sample,
            Inventory_Split,
            Inventory_Transfer_Inbound,
            Inventory_Transfer_Outbound,
            Plant_Cure,
            Plant_Destroy,
            Plant_Destroy_Schedule,
            Plant_Harvest,
            Plant_Harvest_Schedule,
            Plant_New,
            Plant_Modify,
            Plant_Move,
            Plant_Room_Add,
            Product_Add,
            Product_Destroy,
            Product_Get,
            Product_Modify,
            Sales_Add,
            Vehicle_New,
            Waste,
			Waste_Weight
		}

        public enum ComplianceType { Address, Consumer, ConsumerDetailed, Identity, Inventory, InventoryDetailed, Location, Order, OrderProduct, Product, WeightPrices }

        public enum ItemGroupTypes
		{
			CannabisSeed, CloneClipping, CannabisPlant, WetCannabis,
			DriedCannabis, ManicuredTrim, ManicuredBud, PlantWaste, PackagedCannabis, Concentrate, Additives, Nutrients, Pesticides, Fungicides, Sample, QABatch
		}
	}
}
