﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace NSC_DI.API
{
    public static class InventorySync
    {

        public static XDocument DoInventorySync()
        {
            XDocument result = null;
/*	// COMPLIANCE
            // Prepare a connection to the state API
            var TraceCon = new VirSci_SAP.Controllers.TraceabilityAPI(
                SAPBusinessOne_Application: _SBO_Application,
                SAPBusinessOne_Company: _SBO_Company,
                SAPBusinessOne_Form: _SBO_Form);

            BioTrack.API bioCrapAPI = TraceCon.new_API_obj();

            BioTrack.Sync.Inventory(ref bioCrapAPI);
            bioCrapAPI.PostToApi();

            if(bioCrapAPI.WasSuccesful)
            {
                result = bioCrapAPI.xDocFromResponse;
                WasSuccessful = true;
            }
            else
            {
                WasSuccessful = false;
            }
*/
            return result;
        }

        public static InventorySyncData ParseReponse(XDocument xmlResponse)
        {
            InventorySyncData inventorySyncData = new InventorySyncData();
            try
            {
                // Parse our Result data into an Object.
                inventorySyncData.ListOfInventory = (
                    from xmlElement in xmlResponse.Descendants("inventory")
                    select new Inventory
                    {
                        CurrentRoom = xmlElement.Element("currentroom").Value,
                        Deleted = xmlElement.Element("deleted").Value,
                        ID = xmlElement.Element("id").Value,
                        InventoryParentID = xmlElement.Element("inventoryparentid").Value,
                        InventoryStatus = xmlElement.Element("inventorystatus").Value,
                        InventoryStatusTime = xmlElement.Element("inventorystatustime").Value,
                        InventoryType = xmlElement.Element("inventorytype").Value,
                        Location = xmlElement.Element("location").Value,
                        ParentID = xmlElement.Element("parentid").Value,
                        PlantID = xmlElement.Element("plantid").Value,
                        ProductName = xmlElement.Element("productname").Value,
                        Seized = xmlElement.Element("seized").Value,
                        SessionTime = xmlElement.Element("sessiontime").Value,
                        SourceID = xmlElement.Element("source_id").Value,
                        Strain = xmlElement.Element("strain").Value,
                        TransactionID = xmlElement.Element("transactionid").Value,
                        TransactionIdOriginal = xmlElement.Element("transactionid_original").Value,
                        UseableWeight = xmlElement.Element("usable_weight").Value,
                        RemainingQuantity = xmlElement.Element("remaining_quantity").Value,
                        Wet = xmlElement.Element("wet").Value
                    }).ToList<Inventory>();
            }
            catch (Exception ex)
            {

            }

            return inventorySyncData;
        }
    }

    public class InventorySyncData
    {
        /*
    <xml>
        <inventorys>
            <inventory>
                <currentroom></currentroom>
                <deleted>0</deleted>
                <id>6811242086930204</id>
                <inventoryparentid></inventoryparentid>
                <inventorystatus></inventorystatus>
                <inventorystatustime></inventorystatustime>
                <inventorytype>6</inventorytype>
                <location>413038</location>
                <parentid></parentid>
                <plantid>8178755444143482</plantid>
                <productname></productname>
                <remaining_quantity>58.00</remaining_quantity>
                <seized></seized>
                <sessiontime>1416793751</sessiontime>
                <source_id></source_id>
                <strain>Berkeley Blues</strain>
                <transactionid>975192</transactionid>
                <transactionid_original>975192</transactionid_original>
                <usable_weight>58.00</usable_weight>
                <wet>0</wet>
            </inventory>
        </inventorys>
    </xml>
         */
        public List<Inventory> ListOfInventory { get; set; }
    }

    public class Inventory
    {
        public string CurrentRoom { get; set; }
        public string Deleted { get; set; }
        public string ID { get; set; }
        public string InventoryParentID { get; set; }
        public string InventoryStatus { get; set; }
        public string InventoryStatusTime { get; set; }
        public string InventoryType { get; set; }
        public string Location { get; set; }
        public string ParentID { get; set; }
        public string PlantID { get; set; }
        public string ProductName { get; set; }
        public string RemainingQuantity { get; set; }
        public string Seized { get; set; }
        public string SessionTime { get; set; }
        public string SourceID { get; set; }
        public string Strain { get; set; }
        public string TransactionID { get; set; }
        public string TransactionIdOriginal { get; set; }
        public string UseableWeight { get; set; }
        public string Wet { get; set; }

        public string ToCSVString()
        {
            string result = "";

            foreach (PropertyInfo prop in typeof(Inventory).GetProperties())
            {
                if (string.IsNullOrEmpty(result))
                {
                    result += (!string.IsNullOrEmpty(prop.GetValue(this).ToString()) ? prop.GetValue(this).ToString() : "Empty");
                }
                else
                {
                    result += " " + (!string.IsNullOrEmpty(prop.GetValue(this).ToString()) ? prop.GetValue(this).ToString().Replace(" ", string.Empty) : "Empty");
                }
            }
            result += "\n";
            return result;
        }

        public static string CVSHeader()
        {
            string result = "";

            foreach (PropertyInfo prop in typeof(Inventory).GetProperties())
            {
                if (string.IsNullOrEmpty(result))
                {
                    result += prop.Name;
                }
                else
                {
                    result += " " + prop.Name;
                }
            }
            return result;
        }
    }

}
