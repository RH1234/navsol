﻿using System;
using System.Collections.Generic;

namespace NSC_DI.API
{
	//public static class Compliance 
 //   {
 //       internal static string BaseURL = null;

 //       internal static void Init()
 //       {
 //           try
 //           {
 //               if (BaseURL == null) BaseURL = NSC_DI.UTIL.Settings.Value.Get("Compliance API URL");
 //           }
 //           catch (Exception ex)
 //           {
 //               throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //           }
 //           finally
 //           {
 //               GC.Collect();
 //           }
 //       }

 //       public static IO.Swagger.Client.Configuration GetRequestHeader(string pLocationID)
 //       {
 //           SAPbobsCOM.Recordset oRS = null;
 //           try
 //           {
 //               oRS = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
 //               var sql = $"SELECT * FROM [@{Globals.tSettings}] WHERE Name LIKE 'Compliance API %' AND Name NOT LIKE '% URL'";

 //               // may have to manage nuget packages and update the version of (RestSharp) to match the NSC_LIB
 //               // might also have to update the framework.
 //               // might have to change a line to:
 //               //   request.AddFile(param.Value.Name, param.Value.Writer, param.Value.FileName, param.Value.ContentLength, param.Value.ContentType);

 //               // NB9SwXC4Zi9w5tKJpaUFH56ykcYDajU92vO5Xrp5

 //               var config = new IO.Swagger.Client.Configuration();

 //               config.BasePath = NSC_DI.UTIL.Settings.Value.Get("Compliance API URL");

 //               oRS.DoQuery(sql);
 //               for (var i = 0; i < oRS.RecordCount; i++)
 //               {
 //                   if (i > 0) oRS.MoveNext();
 //                   config.AddDefaultHeader(oRS.Fields.Item("U_Description").Value, oRS.Fields.Item("U_Value").Value);
 //               }
 //               config.AddDefaultHeader("X-Location-Id", pLocationID);
 //               return config;
 //           }
 //           catch (Exception ex)
 //           {
 //               throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //           }
 //           finally
 //           {
 //               NSC_DI.UTIL.Misc.KillObject(oRS);
 //               GC.Collect();
 //           }
 //       }

 //       public static IO.Swagger.Client.Configuration GetRequestHeaderWH(string pWH = null)
 //       {
 //           try
 //           {
 //               if (string.IsNullOrWhiteSpace(pWH)) throw new Exception(NSC_DI.UTIL.Message.Format("Warehouse is not specified."));

 //               return GetRequestHeader(GetLocationID(pWH));
 //           }
 //           catch (Exception ex)
 //           {
 //               throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //           }
 //           finally
 //           {
 //               GC.Collect();
 //           }
 //       }

 //       private static bool get()
 //       {
 //           bool bubbleEvent = true;
 //           try
 //           {

 //               ////IO.Swagger.Client.to

 //               //IO.Swagger.Client.Configuration config = new IO.Swagger.Client.Configuration();
 //               //config.AccessToken = "";
 //               //config.BasePath = "";
 //               ////config.ApiKey =

 //               //IO.Swagger.Api.PlantApi api = new IO.Swagger.Api.PlantApi(config);
 //               //var response =  api.GetPlants(null, null);

 //               //var rtn1 = NSC_DI.API.Compliance.Consumer.Get();
 //               //var rtn2 = NSC_DI.API.Compliance.Product.Get();
 //               //var rtn3 = NSC_DI.API.Compliance.Consumer.Get("123");

 //               var prod = new IO.Swagger.Model.Product();
 //               //prod.Brand = "asas";
 //               //prod.CbdContent = 1.2d;

 //               //var config = new IO.Swagger.Client.Configuration();
 //               //config.AddDefaultHeader("X-Location-Id", "7");
 //               //config.AddDefaultHeader("x-api-key","NB9SwXC4Zi9w5tKJpaUFH56ykcYDajU92vO5Xrp5");
 //               //config.BasePath = @"https://compliance-api.data.green/nbs-dev";
 //               //config.;

 //               var config = NSC_DI.API.Compliance.GetRequestHeaderWH();
 //               var instance = new IO.Swagger.Api.ProductApi(config);
 //               //var ret = instance.ProductPost(prod);
 //               var ret = instance.ProductGet();
 //               foreach (var rec in ret)
 //               {
 //                   var s = rec.Name;
 //                   s = "";
 //               }

 //               var x = 0;
 //           }
 //           //catch (IO.Swagger.)
 //           catch (Exception ex)
 //           {
 //               throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //           }
 //           return bubbleEvent;
 //       }

 //       private static string GetLocationID(string pWH = null)
 //       {
 //           // the location ID is on the Warehouse
 //           try
 //           {
 //               if(string.IsNullOrWhiteSpace(pWH)) throw new Exception(NSC_DI.UTIL.Message.Format("A warehouse must be specified to retrieve the Location ID."));
 //               return NSC_DI.SAP.Warehouse.GetField<string>(pWH, $"U_{Globals.SAP_PartnerCode}_DG_LocationID");
 //           }
 //           catch (Exception ex)
 //           {
 //               throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //           }
 //       }

 //       private static string GetMethod()
 //       {
 //           // the Compliance Method
 //           try
 //           {
 //               var method = NSC_DI.UTIL.Settings.Value.Get("State Compliance").ToUpper();
 //               return (string.IsNullOrWhiteSpace(method)) ? "NONE" : method;
 //           }
 //           catch (Exception ex)
 //           {
 //               throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //           }
 //       }

 //       private static bool ProcessCompliance()
 //       {
 //           // check to see if the compliance code should be executed.
 //           try
 //           {
 //               // check the compliance type
 //               var CompType = NSC_DI.UTIL.Settings.Value.Get("State Compliance").ToUpper();
 //               if (string.IsNullOrWhiteSpace(CompType)) return false;
 //               if (CompType == "NONE") return false;

 //               return true;
 //           }
 //           catch (Exception ex)
 //           {
 //               throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //           }
 //       }

 //       private static bool CallAPI()
 //       {
 //           // check to see if the compliance call should be made.
 //           try
 //           {
 //               // check if there is a compliance method.
 //               if (GetMethod() == "NONE") return false;
 //               // check the DB Company name for Sandbox or NBS_
 //               if (Globals.oCompany.CompanyName.ToUpper().Contains("SANDBOX")) return false;
 //               if (Globals.oCompany.CompanyName.ToUpper().Contains("NBS_")) return false;

 //               // check if in development mode
 //               if (System.Diagnostics.Debugger.IsAttached == false) return false;

 //               return true;
 //           }
 //           catch (Exception ex)
 //           {
 //               throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //           }
 //       }

 //       private static string DateTimeToString(DateTime? pDateTime = null)
 //       {
 //           // convert a date to a string       2016-10-04T16:44:53.000
 //           try
 //           {
 //               var dateTime = pDateTime ?? DateTime.Now;
 //               return dateTime.ToString("yyyy-MM-ddTHH:mm:ss.fff");
 //           }
 //           catch (Exception ex)
 //           {
 //               throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //           }
 //       }

 //       public static class Log
 //       {
 //           internal class LogRec
 //           {
 //               internal Globals.ComplianceStatus Status { get; set; }
 //               internal String Reason       { get; set; }  //Globals.ComplianceReasons Reason { get; set; }
 //               internal String Action       { get; set; }  //Globals.ComplianceType Action { get; set; }
 //               internal String CompKey      { get; set; }
 //               internal String KeyType      { get; set; }
 //               internal String ItemCode     { get; set; } = "";
 //               internal String DocNum       { get; set; } = "";
 //               internal int    DocType      { get; set; } = 0;
 //               internal string API_Call     { get; set; } = "";
 //               internal string API_Response { get; set; } = "";
 //               internal string API_Resolved { get; set; } = "";
 //           }
 //           /// <summary>
 //           /// Creates a new compliance line item, to be handled by a seperate service.
 //           /// </summary>
 //           /// <param name="Reason">The reason the compliance post was created.</param>
 //           /// <param name="API_Call">The API call we need to push.</param>
 //           internal static string Post(Globals.ComplianceStatus pStatus, Globals.ComplianceReasons pReason, Globals.ComplianceAction pAction, String pCompKey, String pKeyType,
 //               String pItemCode = "", String pDocNum = "", int pDocType = 0, string pAPI_Call = "", string pAPI_Response = "", string pAPI_Resolved = "")
 //           {
 //               // pDocType is supposed to be SAPbouiCOM.BoLinkedObject    -->   SAPbouiCOM.BoLinkedObject x = (SAPbouiCOM.BoLinkedObject)pDocType

 //               try
 //               {
 //                   var oLogRec          = new LogRec();
 //                   oLogRec.Status       = pStatus;
 //                   oLogRec.Reason       = pReason.ToString();
 //                   oLogRec.API_Call     = pAPI_Call;
 //                   oLogRec.Action       = pAction.ToString();
 //                   oLogRec.CompKey      = pCompKey;
 //                   oLogRec.KeyType      = pKeyType;
 //                   oLogRec.ItemCode     = pItemCode;
 //                   oLogRec.DocNum       = pDocNum;
 //                   oLogRec.DocType      = pDocType;
 //                   oLogRec.API_Response = pAPI_Response;
 //                   oLogRec.API_Resolved = pAPI_Resolved;

 //                   return Post(oLogRec);
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(UTIL.Message.Format(ex));
 //               }
 //               finally
 //               {
 //               }
 //           }
 //           internal static string Post(LogRec pLogRec)
 //           {
 //               // pDocType is supposed to be SAPbobsCOM.BoObjectTypes    -->   SAPbobsCOM.BoObjectTypes x = (SAPbobsCOM.BoObjectTypes)pDocType

 //               // Prepare a connection to the user table
 //               SAPbobsCOM.UserTable oUDT = null;

 //               try
 //               {
 //                   oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tCompliance);
 //                   //Setting Data to Master Data Table Fields  
 //                   //sboTable.Code = NextCode; // AUTO INCREMENT
 //                   //sboTable.Name = NextCode;
 //                   var guid = Guid.NewGuid().ToString();
 //                   oUDT.UserFields.Fields.Item("U_DateCreated").Value = DateTime.Now;
 //                   oUDT.UserFields.Fields.Item("U_GUID").Value       = guid;

 //                   oUDT.UserFields.Fields.Item("U_Status").Value     = pLogRec.Status.ToString().Substring(0,1);
 //                   oUDT.UserFields.Fields.Item("U_Reason").Value     = pLogRec.Reason.ToString();
 //                   oUDT.UserFields.Fields.Item("U_APICall").Value    = pLogRec.API_Call;
 //                   oUDT.UserFields.Fields.Item("U_Action").Value     = pLogRec.Action;
 //                   oUDT.UserFields.Fields.Item("U_CompKey").Value    = pLogRec.CompKey;
 //                   oUDT.UserFields.Fields.Item("U_KeyType").Value    = pLogRec.KeyType;
 //                   oUDT.UserFields.Fields.Item("U_ItemCode").Value   = pLogRec.ItemCode;
 //                   oUDT.UserFields.Fields.Item("U_DocNum").Value     = pLogRec.DocNum;
 //                   oUDT.UserFields.Fields.Item("U_DocType").Value    = pLogRec.DocType.ToString().Substring(1);
 //                   oUDT.UserFields.Fields.Item("U_Response").Value   = pLogRec.API_Response;
 //                   oUDT.UserFields.Fields.Item("U_Resolved").Value   = pLogRec.API_Resolved;

 //                   // if the current environment is test, then make a note
 //                   if(CallAPI() == false) oUDT.UserFields.Fields.Item("U_Response").Value = "*** TEST *** - " + oUDT.UserFields.Fields.Item("U_Response").Value;

 //                   // Attempt to add the new item to the database
 //                   if (oUDT.Add() != 0) throw new Exception(UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

 //                   return UTIL.SQL.GetCodeFromGUID(Globals.tCompliance, guid).ToString();
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(UTIL.Message.Format(ex));
 //               }
 //               finally
 //               {
 //                   UTIL.Misc.KillObject(oUDT);
 //               }
 //           }

 //           public static void UpdateComliancy(string ComplianceID, string ResponseXML, Globals.ComplianceStatus Status, string reason = "")
 //           {
 //               if (string.IsNullOrWhiteSpace(ComplianceID)) return;
 //               SAPbobsCOM.UserTable sboTable = null;

 //               try
 //               {
 //                   sboTable = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tCompliance);
 //                   if (sboTable.GetByKey(ComplianceID) == false) throw new Exception(NSC_DI.UTIL.Message.Format("Error getting Compliance Log record ID:" + ComplianceID + Environment.NewLine + Globals.oCompany.GetLastErrorDescription()));

 //                   sboTable.UserFields.Fields.Item("U_Response").Value = ResponseXML.Replace("'", "''");
 //                   sboTable.UserFields.Fields.Item("U_Status").Value = Status.ToString().Substring(0,1);

 //                   // If the line was resolved we can set the reason.. otherwise leave it null so we can easily query.
 //                   if (!string.IsNullOrEmpty(reason))
 //                   {
 //                       sboTable.UserFields.Fields.Item("U_Resolved").Value = "Y";
 //                   }

 //                   if (sboTable.Update() == 0) return;

 //                   throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(UTIL.Message.Format(ex));
 //               }
 //               finally
 //               {
 //                   UTIL.Misc.KillObject(sboTable);
 //               }
 //           }

 //           /// <summary>
 //           /// Returns whether or not the company is within compliance.
 //           /// </summary>
 //           /// <returns>Returns true if company is compliant.  Returns false if company is not, or out of, compliancy.</returns>
 //           public static bool CheckCompliancy()
 //           {
 //               // TODO: Come up with a more advanced compliancy check

 //               // SQL query for counting the number of failed compliance posts.
 //               string SQL_Query_CountFailedItems = "SELECT COUNT(*) FROM " + UTIL.SQL.SetTableName(Globals.tCompliance) + "WHERE [U_Status] = '2'";

 //               // Attempt to run the SQL query and get the returned value from the query
 //               SAPbobsCOM.Fields ResultFromSQLQuery = UTIL.SQL.GetFieldsFromSQLQuery(SQL_Query_CountFailedItems);
 //               if (ResultFromSQLQuery == null) return false;

 //               try
 //               {
 //                   // If query was successfull and there are no failed compiance posts
 //                   string strIntHolder = ResultFromSQLQuery.Item(0).Value.ToString();
 //                   if (Convert.ToInt32(strIntHolder) == 0)
 //                   {
 //                       // Company is compliant, query ran and there are no failed compliance posts.
 //                       return true;
 //                   }

 //                   // Company is not compliant or the query failed to run
 //                   return false;
 //               }
 //               catch
 //               {
 //                   // Company is not compliant or the query failed to run
 //                   return false;
 //               }
 //           }

 //           public static void ReSend(string pID = null)
 //           {
 //               if (string.IsNullOrWhiteSpace(pID)) return;
 //               SAPbobsCOM.UserTable oTbl = null;

 //               // get the record from the Log table
 //               oTbl = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tCompliance);
 //               if (oTbl.GetByKey(pID) == false) throw new Exception(NSC_DI.UTIL.Message.Format("Error getting Compliance Log record ID:" + pID + Environment.NewLine + Globals.oCompany.GetLastErrorDescription()));

 //               try
 //               {
 //                   if (oTbl.UserFields.Fields.Item("U_Status").Value == "S") return;

 //                   var action   = oTbl.UserFields.Fields.Item("U_Action").Value;               // post, put, update, delete, get
 //                   var keyvalue = oTbl.UserFields.Fields.Item("U_CompKey").Value;
 //                   var keyType  = oTbl.UserFields.Fields.Item("U_KeyType").Value.ToUpper();    // Product, Location, Consumer, ect
 //                   var itemCode = oTbl.UserFields.Fields.Item("U_ItemCode").Value;
 //                   var docNum   = oTbl.UserFields.Fields.Item("U_DocNum").Value;
 //                   var docType  = oTbl.UserFields.Fields.Item("U_DocType").Value;

 //                   Enum.TryParse(action, true, out Globals.ComplianceAction CType);   // string to enum

 //                   //if (keyType == "PRODUCT")
 //               }
 //               catch
 //               {
 //                   return;
 //               }
 //           }
 //       }

 //       public static class Customer
 //       {
 //           internal static int GetTypeId(string pCardCode, int pCntctCode = 0)
 //           {
 //               // the Compliance consumer type code
 //               // if a contact Person exists, then the Customer Type is Caregiver (METRC)

 //               int ret = 0;
 //               var sql = "";

 //               try
 //               {
 //                   var cmpMethod = Compliance.GetMethod();
 //                   if (cmpMethod == "METRC")
 //                   {
 //                       if (pCntctCode > 0)
 //                           sql = $"SELECT U_NSC_CompTypeID FROM OCRG WHERE GroupName = 'Caregiver'";
 //                       else
 //                           sql = $"SELECT U_NSC_CompTypeID FROM OCRG INNER JOIN OCRD ON OCRG.GroupCode = OCRD.GroupCode WHERE OCRD.CardCode = '{pCardCode}'";
 //                       ret = NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);
 //                   }

 //                   return ret;
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //               }
 //           }

 //           internal static string GetStateId(string pCardCode, int pCntctCode = 0)
 //           {
 //               // the consumer State Id

 //               string ret = null;
 //               var  sql = "";

 //               try
 //               {
 //                   var cmpMethod = Compliance.GetMethod();
 //                   if (cmpMethod == "NONE") return ret;

 //                   // only return a license if there is a contact
 //                   //ret = NSC_DI.SAP.BusinessPartners.GetField<string>(pCardCode, "U_NSC_StateUBI");
 //                   if (pCntctCode < 1) return ret;

 //                   if (cmpMethod == "BIOTRACK")
 //                   { }

 //                   if (cmpMethod == "METRC")
 //                   {
 //                       if (pCntctCode > 0)
 //                       {
 //                           sql = $"SELECT U_NSC_CareGiverID FROM OCPR WHERE OCPR.CntctCode = {pCntctCode.ToString()} AND OCPR.CardCode = '{pCardCode}'";
 //                           ret = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
 //                       }
 //                   }

 //                   return ret;
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //               }
 //           }
 //       }

 //       public static class Sales
 //       {
 //           public static void Post(int pKey, SAPbobsCOM.BoObjectTypes pDocType)
 //           {
 //               //CardBObject.caregiver_card_id = InvRow("U_NSC_CareGiverID").ToString
 //               var oLogRec = new Compliance.Log.LogRec();
 //               SAPbobsCOM.Documents oDoc = null;
 //               try
 //               {
 //                   if (ProcessCompliance() == false) return;

 //                   /////////
 //                   // LOG
 //                   oLogRec.Action       = Globals.ComplianceAction.Add.ToString();
 //                   oLogRec.API_Resolved = "N";
 //                   oLogRec.CompKey      = "";
 //                   oLogRec.DocNum       = pKey.ToString();
 //                   oLogRec.DocType      = (int)pDocType;
 //                   oLogRec.KeyType      = "";
 //                   oLogRec.Reason       = Globals.ComplianceReasons.Sales_Add.ToString();
 //                   oLogRec.Status       = Globals.ComplianceStatus.Failed;

 //                   oDoc = Globals.oCompany.GetBusinessObject(pDocType);
 //                   if (!oDoc.GetByKey(pKey)) throw new Exception(Globals.oCompany.GetLastErrorDescription());
 //                   var BPGroup = NSC_DI.SAP.BusinessPartners.GetGroupName(oDoc.CardCode);
 //                   //if(oDoc.ContactPersonCode > 0)

 //                   IO.Swagger.Client.Configuration config = null;
 //                   IO.Swagger.Model.Receipt oDGDoc = new IO.Swagger.Model.Receipt();

 //                   /////////
 //                   // HEADER
 //                   oDGDoc.SaleDatetime         = DateTimeToString();
 //                   oDGDoc.LicenseNumber        = Compliance.Customer.GetStateId(oDoc.CardCode, oDoc.ContactPersonCode);
 //                   oDGDoc.IdentificationMethod = "string";
 //                   oDGDoc.CustomerTypeId       = Compliance.Customer.GetTypeId(oDoc.CardCode);

 //                   /////////
 //                   // ROWS
 //                   oDGDoc.Transactions = new List<IO.Swagger.Model.Transaction>();
 //                   for (var i = 0; i < oDoc.Lines.Count; i++)
 //                   {
 //                       oDoc.Lines.SetCurrentLine(i);

 //                       // use the warehouse from the 1st line
 //                       if (i == 0) config = NSC_DI.API.Compliance.GetRequestHeaderWH(oDoc.Lines.WarehouseCode);

 //                       IO.Swagger.Model.Transaction oDGRow = new IO.Swagger.Model.Transaction();
 //                       var itemCode = oDoc.Lines.ItemCode;

 //                       // Package ID
 //                       oDGRow.PackageId = Package.GetIdFromTag(oDoc.Lines.UserFields.Fields.Item("U_NSC_StateID").Value, oDoc.Lines.WarehouseCode).ToString();

 //                       // Amount - have to check if the UOM is quantity or weight
 //                       // If the quantity field accidentally gets the weight instead of quantity,
 //                       // Divide INV1.Quantity/OITM.SWeight1, which will give the quantity.
 //                       var uomType = NSC_DI.SAP.ItemGroups.GetComplCatUOM(oDoc.Lines.ItemCode);
 //                       switch (uomType)
 //                       {
 //                           case "C":
 //                               oDGRow.Amount = oDoc.Lines.Quantity;
 //                               break;

 //                           case "W":
 //                               oDGRow.Amount = oDoc.Lines.Weight1;
 //                               break;

 //                           default:
 //                               throw new Exception("Compliance Category UOM is not specified.");
 //                       }

 //                       // Price
 //                       oDGRow.Price = oDoc.Lines.Price;

 //                       // Unit ID
 //                       oDGRow.UnitId = Compliance.UOM.Get<long>(oDoc.Lines.UoMCode, oDoc.Lines.WarehouseCode);
 //                       //oDGRow.UnitId = 2;  // ***********************************************************************************
 //                                           //oDGRow.UnitId = Package.GetProductUnitId(oDGRow.PackageId, oDoc.Lines.WarehouseCode);

 //                       oDGDoc.Transactions.Add(oDGRow);
 //                   }

 //                   /////////
 //                   // SEND
 //                   IO.Swagger.Model.ReceiptOutput oReceipt = new IO.Swagger.Model.ReceiptOutput();
 //                   try
 //                   {
 //                       // check if in dubug mode - don't want to actually create an order during development.
 //                       if (CallAPI())
 //                       {
 //                           IO.Swagger.Api.SaleApi oAPI = new IO.Swagger.Api.SaleApi(config);
 //                           oReceipt = oAPI.CreateSale(oDGDoc);
 //                           // update the Doc with the transaction number
 //                           oDoc.UserFields.Fields.Item("U_NSC_CompID").Value = oReceipt.Id.ToString();
 //                           NSC_DI.SAP.Document.Update(oDoc);
 //                       }
 //                       oLogRec.API_Response = oReceipt.ReceiptNumber.ToString();
 //                       oLogRec.Status       = Globals.ComplianceStatus.Success;
 //                       oLogRec.API_Resolved = "Y";
 //                   }
 //                   catch (Exception ex)
 //                   {
 //                       oLogRec.API_Response = NSC_DI.UTIL.Message.Format(ex);
 //                   }
 //                   Compliance.Log.Post(oLogRec);
 //                   //var types = oAPI.SaleCustomerTypeGet();
 //                   //var type = types[0];

 //               }
 //               catch (Exception ex)
 //               {
 //                   oLogRec.API_Response = NSC_DI.UTIL.Message.Format(ex);
 //                   Compliance.Log.Post(oLogRec);
 //                   throw new Exception(oLogRec.API_Response);
 //               }
 //               finally
 //               {
 //                   NSC_DI.UTIL.Misc.KillObject(oDoc);
 //                   GC.Collect();
 //               }
 //           }
 //       }

 //       public static class Package
 //       {
 //           public static int GetIdFromTag(string pTag = null, string pWH = null)
 //           {
 //               try
 //               {
 //                   if (GetMethod() == "NONE") return 0;
 //                   var ids = Get(pTag, pWH);
 //                   return (int)ids[0].Id;
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //               }
 //           }

 //           public static List<IO.Swagger.Model.Package> Get(string pID = null, string pWH = null)
 //           {
 //               // pPackage can be the Tag or the ID

 //               try
 //               {
 //                   if (GetMethod() == "NONE") return null;

 //                   var config = NSC_DI.API.Compliance.GetRequestHeaderWH(pWH);
 //                   var oPackage = new IO.Swagger.Api.PackageApi(config);

 //                   var oLogRec = new Log.LogRec
 //                   {
 //                       Status       = Globals.ComplianceStatus.Success,
 //                       Reason       = Globals.ComplianceReasons.Product_Get.ToString(),
 //                       Action       = Globals.ComplianceAction.Get.ToString(),
 //                       CompKey      = pID.ToString() ?? "All",
 //                       KeyType      = Globals.ComplianceType.Product.ToString(),
 //                       ItemCode     = "",
 //                       DocNum       = "",
 //                       DocType      = 0,
 //                       API_Response = "",
 //                       API_Resolved = "Y"
 //                   };

 //                   List<IO.Swagger.Model.Package> ret = new List<IO.Swagger.Model.Package>();

 //                   try
 //                   {
 //                       if (pID == null)
 //                           ret = oPackage.FindPackages();
 //                       else
 //                       {
 //                           IO.Swagger.Model.Package ret2 = oPackage.GetPackageById(pID);
 //                           ret.Add(ret2);
 //                       }
 //                   }
 //                   catch (Exception ex)
 //                   {
 //                       oLogRec.Status       = Globals.ComplianceStatus.Failed;
 //                       oLogRec.API_Resolved = "N";
 //                       oLogRec.API_Response = NSC_DI.UTIL.Message.Format(ex);
 //                       Compliance.Log.Post(oLogRec);
 //                       throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //                   }

 //                   return ret;
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //               }
 //           }

 //           public static int GetProductUnitId(string pID = null, string pWH = null)
 //           {   // return the Product's Unit ID using the Package ID
 //               try
 //               {
 //                   if (NSC_DI.UTIL.Settings.Value.Get("State Compliance") == "NONE") return 0;
 //                   var ids = Get(pID, pWH);
 //                   var product =  (int)ids[0].ProductId;
 //                   return Product.GetUnitId(product, pWH);
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //               }
 //           }

 //           public static void Post()
 //           {
 //               //IO.Swagger.
 //           }
 //       }

 //       public static class Product
 //       {
 //           public static int GetUnitId(int? pId = null, string pWH = null)
 //           {
 //               try
 //               {
 //                   if (NSC_DI.UTIL.Settings.Value.Get("State Compliance") == "NONE") return 0;
 //                   var product = Get(pId.ToString(), pWH);
 //                   long? unitId = product[0].UnitId;
 //                   if(unitId == null) return 0;
 //                   return (int)unitId;
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //               }
 //           }

 //           public static List<IO.Swagger.Model.Product> Get(string pProduct = null, string pWH = null)
 //           {
 //               try
 //               {
 //                   var comp = NSC_DI.UTIL.Settings.Value.Get("State Compliance");
 //                   if (string.IsNullOrWhiteSpace(comp) || comp.Trim().ToUpper() == "NONE") return null;
 //                   //private const string cPath = @"/product";
 //                   //var client = new RestSharp.RestClient(BaseURL + cPath);
 //                   //var request = new RestSharp.RestRequest(RestSharp.Method.GET);
 //                   //SetRequestHeader(request);

 //                   //RestSharp.IRestResponse response = client.Execute(request);

 //                   //if (response.IsSuccessful)
 //                   //    return Newtonsoft.Json.JsonConvert.DeserializeObject<List<IO.Swagger.Model.Product>>(response.Content);
 //                   //else
 //                   //    throw new Exception(NSC_DI.UTIL.Message.Format(response.Content));
 //                   //var ret = instance.ProductPost(prod);


 //                   var config   = NSC_DI.API.Compliance.GetRequestHeaderWH(pWH);
 //                   var instance = new IO.Swagger.Api.ProductApi(config);

 //                   var oLogRec = new Log.LogRec
 //                   {
 //                       Status       = Globals.ComplianceStatus.Success,
 //                       Reason       = Globals.ComplianceReasons.Product_Get.ToString(),
 //                       Action       = Globals.ComplianceAction.Get.ToString(),
 //                       CompKey      = pProduct ?? "All",
 //                       KeyType      = Globals.ComplianceType.Product.ToString(),
 //                       ItemCode     = "",
 //                       DocNum       = "",
 //                       DocType      = 0,
 //                       API_Response = "",
 //                       API_Resolved = "Y"
 //                   };

 //                   List<IO.Swagger.Model.Product> ret = new List<IO.Swagger.Model.Product>();

 //                   try
 //                   {
 //                       if (pProduct == null)
 //                           ret = instance.ProductGet();
 //                       else
 //                       {
 //                           IO.Swagger.Model.Product ret2 = instance.ProductIdGet(Convert.ToInt64(pProduct));
 //                           ret.Add(ret2);
 //                       }
 //                   }
 //                   catch (Exception ex)
 //                   {
 //                       oLogRec.Status       = Globals.ComplianceStatus.Failed;
 //                       oLogRec.API_Resolved = "N";
 //                       oLogRec.API_Response = NSC_DI.UTIL.Message.Format(ex);
 //                       Compliance.Log.Post(oLogRec);
 //                       throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //                   }

 //                   Compliance.Log.Post(oLogRec);    // write to log table

 //                   //Newtonsoft.Json.JsonConvert.SerializeObject(ret);
 //                   //foreach (var rec in ret)
 //                   //{
 //                   //    var s = rec.Name;
 //                   //    s = "";
 //                   //}

 //                   return ret;
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //               }
 //           }

 //           public static void Post()
 //           {
 //               //IO.Swagger.
 //           }
 //       }

 //       public static class UOM
 //       {
 //           private static string Compny = "";
 //           private static IO.Swagger.Model.MeasurementUnit oUOM;
 //           private static List<IO.Swagger.Model.MeasurementUnit> lUOM;

 //           public static T Get<T>(string pUOM, string pWH)
 //           {
 //               // get the UOM code using the name
 //               try
 //               {
 //                   if (lUOM == null || lUOM.Count < 1) Get(pWH);  // if the list is empty, fill it
 //                   foreach(var rec in lUOM)
 //                   {
 //                       if (rec.Abbreviation.ToUpper() == pUOM.ToUpper()) return (T)System.Convert.ChangeType(rec.Id, typeof(T));
 //                       if (rec.Name.ToUpper() == pUOM.ToUpper()) return (T)System.Convert.ChangeType(rec.Id, typeof(T));
 //                   }

 //                   return (T)System.Convert.ChangeType(0, typeof(T));
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //               }
 //               finally
 //               {
 //                   //NSC_DI.UTIL.Misc.KillObject(oDoc);
 //                   GC.Collect();
 //               }
 //           }

 //           public static List<IO.Swagger.Model.MeasurementUnit> Get(string pWH)
 //           {

 //               // get all UOMs
 //               try
 //               {
 //                   var config   = NSC_DI.API.Compliance.GetRequestHeaderWH(pWH);
 //                   var instance = new IO.Swagger.Api.UnitsOfMeasureApi(config);
 //                   lUOM         = instance.UnitOfMeasureGet();

 //                   return lUOM;
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //               }
 //               finally
 //               {
 //                   //NSC_DI.UTIL.Misc.KillObject(oDoc);
 //                   GC.Collect();
 //               }
 //           }
 //       }

 //       public static class Consumer
 //       {

 //           private static void SetRequestHeader(RestSharp.RestRequest pRequest)
 //           {
 //               SAPbobsCOM.Recordset oRS = null;
 //               try
 //               {
 //                   oRS = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

 //                   var sql = $"SELECT * FROM [@{Globals.tSettings}] WHERE Name LIKE 'Compliance API %' AND Name NOT LIKE '% URL'";
 //                   oRS.DoQuery(sql);
 //                   pRequest.AddHeader("cache-control", "no-cache");
 //                   for (var i = 0; i < oRS.RecordCount; i++)
 //                   {
 //                       if (i > 0) oRS.MoveNext();
 //                       pRequest.AddHeader(oRS.Fields.Item("U_Description").Value, oRS.Fields.Item("U_Value").Value);
 //                   }
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //               }
 //               finally
 //               {
 //                   NSC_DI.UTIL.Misc.KillObject(oRS);
 //                   GC.Collect();
 //               }
 //           }

 //           private const string cPath = @"/consumer";

 //           public static List<oConsumer> Get()
 //           {
 //               try
 //               {
 //                   var client = new RestSharp.RestClient(BaseURL + cPath);
 //                   var request = new RestSharp.RestRequest(RestSharp.Method.GET);
 //                   SetRequestHeader(request);

 //                   RestSharp.IRestResponse response = client.Execute(request);

 //                   if (response.IsSuccessful)
 //                       return Newtonsoft.Json.JsonConvert.DeserializeObject<List<oConsumer>>(response.Content);
 //                   else
 //                       throw new Exception(NSC_DI.UTIL.Message.Format(response.Content));
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //               }
 //           }

 //           public static oConsumer Get(string pBPCode)
 //           {
 //               try
 //               {
 //                   var client  = new RestSharp.RestClient(BaseURL + cPath + "//" + pBPCode);
 //                   var request = new RestSharp.RestRequest(RestSharp.Method.GET);
 //                   SetRequestHeader(request);

 //                   RestSharp.IRestResponse response = client.Execute(request);

 //                   if (response.IsSuccessful)
 //                       return Newtonsoft.Json.JsonConvert.DeserializeObject<oConsumer>(response.Content);
 //                   else
 //                       throw new Exception(NSC_DI.UTIL.Message.Format(response.Content));
 //               }
 //               catch (Exception ex)
 //               {
 //                   throw new Exception(NSC_DI.UTIL.Message.Format(ex));
 //               }
 //           }

 //           //public class ReturnObj
 //           //{
 //           //    public RootObj[] Property1 { get; set; }
 //           //}

 //           public class oConsumer
 //           {
 //               public string id { get; set; }
 //               public string name_first_id { get; set; }
 //               public string name_first { get; set; }
 //               public string name_middle_id { get; set; }
 //               public string name_middle { get; set; }
 //               public string name_last_id { get; set; }
 //               public string name_last { get; set; }
 //               public string nickname_id { get; set; }
 //               public string nickname { get; set; }
 //               public string email { get; set; }
 //               public string gender_id { get; set; }
 //               public string gender_name { get; set; }
 //               public string birthdate { get; set; }
 //               public string contact_method_id { get; set; }
 //               public string contact_method_name { get; set; }
 //               public string tax_exempt { get; set; }
 //               public string physician_id { get; set; }
 //               public string physician_name { get; set; }
 //               public string physician_address { get; set; }
 //               public string physician_license { get; set; }
 //               public string consumer_type_id { get; set; }
 //               public string consumer_type_name { get; set; }
 //           }
 //       }
 //   }
}
