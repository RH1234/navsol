﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace NSC_DI.API
{
    public  class TestResults
    {
        public StateTestResults CheckStateTestsAPI(string stateSampleId, string sampleBatchNumber)
        {
            StateTestResults testResults = GetTestResultsFromStateSampleID(stateSampleID: stateSampleId);

            // This is only for testing. -- Can be removed.
            //            string testDeleteThis = string.Format(@"
            //<xml>
            //	<data>
            //		<result>1</result>
            //		<use>0</use>
            //		<test>
            //			<moisture>5</moisture>
            //			<type>1</type>
            //		</test>
            //		<test>
            //			<CBD>5</CBD>
            //			<THC>20</THC>
            //			<THCA>1</THCA>
            //			<Total>36</Total>
            //			<type>2</type>
            //		</test>
            //		<test>
            //			<Other>1</Other>
            //			<Stems>2</Stems>
            //			<type>3</type>
            //		</test>
            //		<test>
            //			<aerobic_bacteria>1000</aerobic_bacteria>
            //			<bile_tolerant>10000</bile_tolerant>
            //			<coliforms>10000</coliforms>
            //			<e_coli_and_salmonella>0</e_coli_and_salmonella>
            //			<yeast_and_mold>2500</yeast_and_mold>
            //			<type>4</type>
            //		</test>
            //		<test>
            //			<residual_solvent>150</residual_solvent >
            //			<type>5</type>
            //		</test>
            //	</data>
            //</xml>");
            //            testResults.Message = "Found Test Results!";
            //            testResults.ParseResponseXML(xmlResults: testDeleteThis);

            SAP.Test.MapTests(ref testResults);

            testResults.DefaultTestsMap.TestSampleCode = sampleBatchNumber;

            testResults.PopulateDefaultTestsWithResults();

            if (testResults.FoundResults)
            {
                // Populate our Generic Tests with Results if they exist.

                // NEED TO CHANGE THE ComplianceStatus ENUM
                //If the test was found.. update the batched item with a QA status of passed or fail.
                //if (testResults.TestingPassed == Globals.TestResultState.Success)
                //{
                //    // Update the Line to be passed? Give the User a Message that the Item has Passed and remove it from the List..

                //    SAP.BatchItems.UpdateQATestResults(batchNumber: sampleBatchNumber,
                //        newStatus: Globals.TestResultStatus.Passed);

                //}
                //else if (testResults.TestingPassed == Globals.TestResultState.Failed)
                //{
                //    // Update the Line to be failed..

                //    SAP.BatchItems.UpdateQATestResults(batchNumber: sampleBatchNumber,
                //        newStatus: Globals.TestResultStatus.Failed);
                //}
            }
            else
            {
                testResults.FoundResults = false;
                testResults.Message = "Not found";
            }

            return testResults;
        }

        private  StateTestResults GetTestResultsFromStateSampleID(string stateSampleID)
        {
            StateTestResults testResults = new StateTestResults();

            if (string.IsNullOrEmpty(stateSampleID))
            {
                return testResults;
            }
/*	// COMPLIANCE
            // Prepare a connection to the state API
            var TraceCon = new VirSci_SAP.Controllers.TraceabilityAPI(SAPBusinessOne_Application: _SBO_Application,
                SAPBusinessOne_Company: _SBO_Company);

            // Create compliance call to delete plant
            VirSci_SAP.Controllers.Compliance VirSci_Controllers_Compliance = new Controllers.Compliance(
                SAPBusinessOne_Application: _SBO_Application, SAPBusinessOne_Company: _SBO_Company);

            BioTrack.API bioTrackApi = TraceCon.new_API_obj();

            BioTrack.Inventory.QA_Check_All(
                BioTrackAPI: ref bioTrackApi,
                barcodeIDs: new List<string>() { stateSampleID }.ToArray());

            bioTrackApi.PostToApi();

            if (bioTrackApi.WasSuccesful)
            {
                string xmlResponse = bioTrackApi.xmlDocFromResponse.InnerXml.ToString();
                if (!string.IsNullOrEmpty(xmlResponse) && testResults.HasResults(xmlResponse))
                {
                    testResults.Message = "Found Test Results!";
                    testResults.ParseResponseXML(xmlResults: xmlResponse);
                }
            }
*/
            return testResults;
        }

        public class StateTestResults
        {
            public string Message { get; set; }
            public bool FoundResults { get; set; }

            public StateTestResults()
            {
                this.Message = "No results were found!";
                this.FoundResults = false;
            }

            /// <summary>
            /// integer value, sample use, 0 for standard
            /// test, 1 for test specifically for extract
            /// </summary>
            public Globals.SampleTestType TestUseage { get; set; }

            /// <summary>
            /// integer value, -1 for failure, 1 for success
            /// </summary>
            public Globals.TestResultState TestingPassed { get; set; }

            public MoistureTestResults MoistureResults { get; set; }
            public PotencyTestResults PotencyResults { get; set; }
            public ForeignMatterTestResults ForeignMatterResults { get; set; }
            public MicrobiologicalTestResults MicrobiologicalResults { get; set; }
            public ResidualSolventTestResults ResidualSolventResults { get; set; }

            public DefaultTestPopulateRequest DefaultTestsMap { get; set; }

            public bool HasResults(string xmlResults)
            {
                bool hasResults = false;

                // Get the response from the API as an XML document
                XDocument _xmlResponseTestResults = XDocument.Parse(xmlResults);

                // Find the "data" XML element in the response
                XElement xmlDataNode = _xmlResponseTestResults.Descendants("data").First();

                if (xmlDataNode.Element("result") != null)
                {
                    string valueCheck = xmlDataNode.Element("result").Value.ToString();
                    hasResults = string.IsNullOrEmpty(valueCheck) ? false : true;

                    if(hasResults)
                    {
                        if (xmlDataNode.Element("test") != null)
                        {
                            valueCheck = xmlDataNode.Element("test").Value.ToString();
                            hasResults = string.IsNullOrEmpty(valueCheck) ? false : true;
                        }
                        else
                        {
                            hasResults = false;
                        }
                    }
                }
                
                return hasResults;
            }

            public static void ParseResponseXML(string xmlResults)
            {
/*	// COMPLIANCE
                bool foundSomething = false;

                // Get the response from the API as an XML document
                XDocument _xmlResponseTestResults = XDocument.Parse(xmlResults);

                // Find the "data" XML element in the response
                XElement xmlDataNode = _xmlResponseTestResults.Descendants("data").First();

                if (xmlDataNode.Element("result") != null)
                {
                    string sampleTestResultState = xmlDataNode.Element("result").Value.ToString();
                    int resultStatus = string.IsNullOrEmpty(sampleTestResultState) ? (int)TestResultState.Failed : Convert.ToInt16(sampleTestResultState);
                    this.TestingPassed = (TestResultState)(resultStatus);
                    foundSomething = true;
                }
                else
                {
                    this.TestingPassed = TestResultState.Failed;
                }

                if (xmlDataNode.Element("use") != null)
                {
                    string testUseageType = xmlDataNode.Element("use").Value.ToString();
                    this.TestUseage = (SampleTestType)(Convert.ToInt16(testUseageType));
                    foundSomething = true;
                }

                if (xmlDataNode.Element("test") != null)
                {
                    foreach (var node in xmlDataNode.Elements("test"))
                    {
                        try
                        {
                            foundSomething = true;
                            switch (node.Element("type").Value)
                            {
                                case "1":
                                    this.ParseMoistureResults(node);
                                    break;
                                case "2":
                                    this.ParsePotencyResults(node);
                                    break;
                                case "3":
                                    this.ParseForeignMatterResults(node);
                                    break;
                                case "4":
                                    this.ParseMicrobiologicalResults(node);
                                    break;
                                case "5":
                                    this.ParseResidualSolventResults(node);
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }

                if (foundSomething)
                {
                    this.FoundResults = true;
                }
 */
            }

            private  void ParseMoistureResults(XElement node)
            {
                try
                {
                    // Find the "test" XML element in the "data" XML element
                    string TestResults_Moisture = node.Element("moisture").Value.ToString();

                    MoistureResults = new MoistureTestResults()
                    {
                        MoistureAmount = Convert.ToDecimal(TestResults_Moisture),
                    };
                }
                catch (Exception ex)
                {
                    //Failed to Parse XML of Test Results.
                }

            }

            private void ParsePotencyResults(XElement node)
            {
                try
                {
                    // Find the "test" XML element in the "data" XML element
                    string TestResults_THC = node.Element("THC").Value.ToString();
                    string TestResults_CBD = node.Element("CBD").Value.ToString();
                    string TestResults_THCA = node.Element("THCA").Value.ToString();
                    string TestResults_TotalTHC = node.Element("Total").Value.ToString();

                    PotencyResults = new PotencyTestResults()
                    {
                        THC = Convert.ToDecimal(TestResults_THC),
                        THCA = Convert.ToDecimal(TestResults_THCA),
                        CDB = Convert.ToDecimal(TestResults_CBD),
                        TotalTHC = Convert.ToDecimal(TestResults_TotalTHC)
                    };
                }
                catch (Exception ex)
                {
                    //Failed to Parse XML of Test Results.
                }
            }

            private void ParseForeignMatterResults(XElement node)
            {
                try
                {
                    string TestResults_Other = node.Element("Other").Value.ToString();
                    string TestResults_Stems = node.Element("Stems").Value.ToString();

                    ForeignMatterResults = new ForeignMatterTestResults()
                    {
                        Other = Convert.ToDecimal(TestResults_Other),
                        Stems = Convert.ToDecimal(TestResults_Stems),
                    };
                }
                catch (Exception ex)
                {
                    //Failed to Parse XML of Test Results.
                }
            }

            private void ParseMicrobiologicalResults(XElement node)
            {
                try
                {
                    // Find the "test" XML element in the "data" XML element
                    string TestResults_AerobicBacteria = node.Element("aerobic_bacteria").Value.ToString();
                    string TestResults_BileTolerant = node.Element("bile_tolerant").Value.ToString();
                    string TestResults_Coliforms = node.Element("coliforms").Value.ToString();
                    string TestResults_EColiAndSalonella = node.Element("e_coli_and_salmonella").Value.ToString();
                    string TestResults_YeastAndMold = node.Element("yeast_and_mold").Value.ToString();

                    MicrobiologicalResults = new MicrobiologicalTestResults()
                    {
                        AerobicBacteria = Convert.ToDecimal(TestResults_AerobicBacteria),
                        BileTolerant = Convert.ToDecimal(TestResults_BileTolerant),
                        Coliforms = Convert.ToDecimal(TestResults_Coliforms),
                        EColiAndSalonella = Convert.ToDecimal(TestResults_EColiAndSalonella),
                        YeastAndMold = Convert.ToDecimal(TestResults_YeastAndMold)
                    };
                }
                catch (Exception ex)
                {
                    //Failed to Parse XML of Test Results.
                }
            }

            private void ParseResidualSolventResults(XElement node)
            {
                try
                {
                    string TestResults_ResidualSolvent = node.Element("residual_solvent").Value.ToString();

                    ResidualSolventResults = new ResidualSolventTestResults()
                    {
                        ResidualSolvent = Convert.ToDecimal(TestResults_ResidualSolvent),
                    };
                }
                catch (Exception ex)
                {
                    //Failed to Parse XML of Test Results.
                }
            }

            public class DefaultTestPopulateRequest
            {
                public SAP.Test ControllerTest { get; set; }
                public string TestSampleCode { get; set; }
                public MoistureTestUpdate MoistureTableMap { get; set; }
                public PotencyTestUpdate PotencyTableMap { get; set; }
                public ForeignMatterTestUpdate ForeignMatterTableMap { get; set; }
                public MicrobiologicalTestUpdate MicrobiologicalTableMap { get; set; }
                public ResidualSolventTestUpdate ResidualSolventTableMap { get; set; }

                public class MoistureTestUpdate
                {
                    public string MoistureTestId { get; set; }
                    public string MoistureAmountFieldId { get; set; }
                }

                public class PotencyTestUpdate
                {
                    public string PotencyTestId { get; set; }
                    public string PotencyFieldId_THC { get; set; }
                    public string PotencyFieldId_THCA { get; set; }
                    public string PotencyFieldId_CDB { get; set; }
                    public string PotencyFieldId_TotalTHC { get; set; }
                }

                public class ForeignMatterTestUpdate
                {
                    public string ForeignMatterTestId { get; set; }
                    public string ForeignMatterFieldId_Stems { get; set; }
                    public string ForeignMatterFieldId_Other { get; set; }
                }

                public class MicrobiologicalTestUpdate
                {
                    public string MicrobiologicalTestId { get; set; }
                    public string MicrobiologicalFieldId_AerobicBacteria { get; set; }
                    public string MicrobiologicalFieldId_YeastAndMold { get; set; }
                    public string MicrobiologicalFieldId_Coliforms { get; set; }
                    public string MicrobiologicalFieldId_BileTolerant { get; set; }
                    public string MicrobiologicalFieldId_EColiAndSalonella { get; set; }
                }

                public class ResidualSolventTestUpdate
                {
                    public string ResidualSolventTestId { get; set; }
                    public string ResidualSolventFieldId_ResidualSolvent { get; set; }
                }

            }

            public void PopulateDefaultTestsWithResults()
            {
                SAP.Test testController = DefaultTestsMap.ControllerTest;
                string testSampleCode = this.DefaultTestsMap.TestSampleCode;

                #region Moisture Results

                if (MoistureResults != null && DefaultTestsMap.MoistureTableMap != null)
                {
                    string testCode = DefaultTestsMap.MoistureTableMap.MoistureTestId;
                    if (SAP.Test.ValidateTestExists(testCode))
                    {
                        bool resultsExist = SAP.Test.ValidateTestFieldResultExists(
                            TestSampleCode: testSampleCode,
                            TestFieldCode: this.DefaultTestsMap.MoistureTableMap.MoistureAmountFieldId);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestFieldResult(
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.MoistureTableMap.MoistureAmountFieldId,
                                Value: MoistureResults.MoistureAmount.ToString());
                        }
                        else
                        {
                            string newCode = SAP.Test.GetNextTestFieldResultCode();

                            SAP.Test.CreateTestFieldResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.MoistureTableMap.MoistureAmountFieldId,
                                Value: MoistureResults.MoistureAmount.ToString());
                        }
                    }
                }

                #endregion

                #region Potency Results

                if (PotencyResults != null && this.DefaultTestsMap.PotencyTableMap != null)
                {
                    string testCode = this.DefaultTestsMap.PotencyTableMap.PotencyTestId;
                    if (SAP.Test.ValidateTestExists(testCode))
                    {
                        string newCode = "";

                        // PotencyFieldId_THC
                        bool resultsExist = SAP.Test.ValidateTestCompoundResultExists(
                            TestSampleCode: testSampleCode,
                            TestFieldCode: this.DefaultTestsMap.PotencyTableMap.PotencyFieldId_THC);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestCompoundResult(
                                TestSampleCode: testSampleCode,
                                CompoundCode: this.DefaultTestsMap.PotencyTableMap.PotencyFieldId_THC,
                                Value: PotencyResults.THC.ToString());
                        }
                        else
                        {
                            newCode = SAP.Test.GetNextTestCompoundResultCode();
                            SAP.Test.CreateTestCompoundResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                CompoundCode: this.DefaultTestsMap.PotencyTableMap.PotencyFieldId_THC,
                                Value: PotencyResults.THC.ToString());
                        }

                        // PotencyFieldId_CDB
                        resultsExist = SAP.Test.ValidateTestCompoundResultExists(
                            TestSampleCode: testSampleCode,
                            TestFieldCode: this.DefaultTestsMap.PotencyTableMap.PotencyFieldId_CDB);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestCompoundResult(
                                TestSampleCode: testSampleCode,
                                CompoundCode: this.DefaultTestsMap.PotencyTableMap.PotencyFieldId_CDB,
                                Value: PotencyResults.CDB.ToString());
                        }
                        else
                        {
                            newCode = SAP.Test.GetNextTestCompoundResultCode();
                            SAP.Test.CreateTestCompoundResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                CompoundCode: this.DefaultTestsMap.PotencyTableMap.PotencyFieldId_CDB,
                                Value: PotencyResults.CDB.ToString());
                        }

                        // PotencyFieldId_THCA
                        resultsExist = SAP.Test.ValidateTestCompoundResultExists(
                            TestSampleCode: testSampleCode,
                            TestFieldCode: this.DefaultTestsMap.PotencyTableMap.PotencyFieldId_THCA);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestCompoundResult(
                                TestSampleCode: testSampleCode,
                                CompoundCode: this.DefaultTestsMap.PotencyTableMap.PotencyFieldId_THCA,
                                Value: PotencyResults.THCA.ToString());
                        }
                        else
                        {
                            newCode = SAP.Test.GetNextTestCompoundResultCode();
                            SAP.Test.CreateTestCompoundResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                CompoundCode: this.DefaultTestsMap.PotencyTableMap.PotencyFieldId_THCA,
                                Value: PotencyResults.THCA.ToString());
                        }

                        // PotencyFieldId_TotalTHC
                        resultsExist = SAP.Test.ValidateTestCompoundResultExists(
                            TestSampleCode: testSampleCode,
                            TestFieldCode: this.DefaultTestsMap.PotencyTableMap.PotencyFieldId_TotalTHC);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestCompoundResult(
                                TestSampleCode: testSampleCode,
                                CompoundCode: this.DefaultTestsMap.PotencyTableMap.PotencyFieldId_TotalTHC,
                                Value: PotencyResults.TotalTHC.ToString());
                        }
                        else
                        {
                            newCode = SAP.Test.GetNextTestCompoundResultCode();
                            SAP.Test.CreateTestCompoundResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                CompoundCode: this.DefaultTestsMap.PotencyTableMap.PotencyFieldId_TotalTHC,
                                Value: PotencyResults.TotalTHC.ToString());
                        }
                    }
                }

                #endregion

                #region ForeignMatter Results

                if (ForeignMatterResults != null && this.DefaultTestsMap.ForeignMatterTableMap != null)
                {
                    string testCode = this.DefaultTestsMap.ForeignMatterTableMap.ForeignMatterTestId;
                    if (SAP.Test.ValidateTestExists(testCode))
                    {
                        bool resultsExist = SAP.Test.ValidateTestFieldResultExists(
                            TestSampleCode: testSampleCode,
                            TestFieldCode: this.DefaultTestsMap.ForeignMatterTableMap.ForeignMatterFieldId_Other);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestFieldResult(
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.ForeignMatterTableMap.ForeignMatterFieldId_Other,
                                Value: ForeignMatterResults.Other.ToString());
                        }
                        else
                        {
                            string newCode = SAP.Test.GetNextTestFieldResultCode();

                            SAP.Test.CreateTestFieldResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.ForeignMatterTableMap.ForeignMatterFieldId_Other,
                                Value: ForeignMatterResults.Other.ToString());

                        }

                        resultsExist = SAP.Test.ValidateTestFieldResultExists(
                           TestSampleCode: testSampleCode,
                           TestFieldCode: this.DefaultTestsMap.ForeignMatterTableMap.ForeignMatterFieldId_Stems);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestFieldResult(
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.ForeignMatterTableMap.ForeignMatterFieldId_Stems,
                                Value: ForeignMatterResults.Stems.ToString());
                        }
                        else
                        {
                            string newCode = SAP.Test.GetNextTestFieldResultCode();

                            SAP.Test.CreateTestFieldResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.ForeignMatterTableMap.ForeignMatterFieldId_Stems,
                                Value: ForeignMatterResults.Stems.ToString());
                        }
                    }
                }
                #endregion

                #region Microbiological Results

                if (MicrobiologicalResults != null && this.DefaultTestsMap.MicrobiologicalTableMap != null)
                {
                    string testCode = this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalTestId;
                    if (SAP.Test.ValidateTestExists(testCode))
                    {
                        // Aerobic Bacteria
                        bool resultsExist = SAP.Test.ValidateTestFieldResultExists(
                            TestSampleCode: testSampleCode,
                            TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_AerobicBacteria);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestFieldResult(
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_AerobicBacteria,
                                Value: MicrobiologicalResults.AerobicBacteria.ToString());
                        }
                        else
                        {
                            string newCode = SAP.Test.GetNextTestFieldResultCode();

                            SAP.Test.CreateTestFieldResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_AerobicBacteria,
                                Value: MicrobiologicalResults.AerobicBacteria.ToString());
                        }

                        // Yeast and Mold
                        resultsExist = SAP.Test.ValidateTestFieldResultExists(
                            TestSampleCode: testSampleCode,
                            TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_YeastAndMold);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestFieldResult(
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_YeastAndMold,
                                Value: MicrobiologicalResults.YeastAndMold.ToString());
                        }
                        else
                        {
                            string newCode = SAP.Test.GetNextTestFieldResultCode();

                            SAP.Test.CreateTestFieldResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_YeastAndMold,
                                Value: MicrobiologicalResults.YeastAndMold.ToString());
                        }


                        // MicrobiologicalFieldId_Coliforms
                        resultsExist = SAP.Test.ValidateTestFieldResultExists(
                            TestSampleCode: testSampleCode,
                            TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_Coliforms);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestFieldResult(
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_Coliforms,
                                Value: MicrobiologicalResults.Coliforms.ToString());
                        }
                        else
                        {
                            string newCode = SAP.Test.GetNextTestFieldResultCode();

                            SAP.Test.CreateTestFieldResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_Coliforms,
                                Value: MicrobiologicalResults.Coliforms.ToString());
                        }

                        // MicrobiologicalFieldId_BileTolerant
                        resultsExist = SAP.Test.ValidateTestFieldResultExists(
                            TestSampleCode: testSampleCode,
                            TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_BileTolerant);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestFieldResult(
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_BileTolerant,
                                Value: MicrobiologicalResults.BileTolerant.ToString());
                        }
                        else
                        {
                            string newCode = SAP.Test.GetNextTestFieldResultCode();

                            SAP.Test.CreateTestFieldResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_BileTolerant,
                                Value: MicrobiologicalResults.BileTolerant.ToString());
                        }


                        // MicrobiologicalFieldId_BileTolerant
                        resultsExist = SAP.Test.ValidateTestFieldResultExists(
                            TestSampleCode: testSampleCode,
                            TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_EColiAndSalonella);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestFieldResult(
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_EColiAndSalonella,
                                Value: MicrobiologicalResults.EColiAndSalonella.ToString());
                        }
                        else
                        {
                            string newCode = SAP.Test.GetNextTestFieldResultCode();

                            SAP.Test.CreateTestFieldResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.MicrobiologicalTableMap.MicrobiologicalFieldId_EColiAndSalonella,
                                Value: MicrobiologicalResults.EColiAndSalonella.ToString());
                        }

                    }
                }

                #endregion

                #region Residual Solvent Results

                if (ResidualSolventResults != null && this.DefaultTestsMap.ResidualSolventTableMap != null)
                {
                    string testCode = this.DefaultTestsMap.ResidualSolventTableMap.ResidualSolventTestId;
                    if (SAP.Test.ValidateTestExists(testCode))
                    {
                        bool // MicrobiologicalFieldId_BileTolerant
                        resultsExist = SAP.Test.ValidateTestFieldResultExists(
                            TestSampleCode: testSampleCode,
                            TestFieldCode: this.DefaultTestsMap.ResidualSolventTableMap.ResidualSolventFieldId_ResidualSolvent);

                        if (resultsExist)
                        {
                            SAP.Test.UpdateTestFieldResult(
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.ResidualSolventTableMap.ResidualSolventFieldId_ResidualSolvent,
                                Value: ResidualSolventResults.ResidualSolvent.ToString());
                        }
                        else
                        {
                            string newCode = SAP.Test.GetNextTestFieldResultCode();

                            SAP.Test.CreateTestFieldResult(
                                Code: newCode,
                                TestSampleCode: testSampleCode,
                                TestFieldCode: this.DefaultTestsMap.ResidualSolventTableMap.ResidualSolventFieldId_ResidualSolvent,
                                Value: ResidualSolventResults.ResidualSolvent.ToString());
                        }
                    }
                }

                #endregion

            }
        }

        // Test # 1
        public class MoistureTestResults
        {
            public decimal MoistureAmount { get; set; }
        }

        // Test # 2
        public class PotencyTestResults
        {
            public decimal CDB { get; set; }
            public decimal THC { get; set; }
            public decimal THCA { get; set; }
            public decimal TotalTHC { get; set; }
        }

        // Test # 3
        public class ForeignMatterTestResults
        {
            public decimal Other { get; set; }
            public decimal Stems { get; set; }
        }

        // Test # 4
        public class MicrobiologicalTestResults
        {
            public decimal AerobicBacteria { get; set; }
            public decimal BileTolerant { get; set; }
            public decimal Coliforms { get; set; }
            public decimal EColiAndSalonella { get; set; }
            public decimal YeastAndMold { get; set; }
        }

        // Test # 5
        public class ResidualSolventTestResults
        {
            public decimal ResidualSolvent { get; set; }
        }
    }
}
