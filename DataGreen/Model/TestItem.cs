/* 
 * data.green API Gateway
 *
 * Unified access to major jurisdictionally-mandated track-and-trace software providers.
 *
 * OpenAPI spec version: 2.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// TestItem
    /// </summary>
    [DataContract]
    public partial class TestItem :  IEquatable<TestItem>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestItem" /> class.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="quanity">quanity.</param>
        /// <param name="passed">passed.</param>
        /// <param name="notes">notes.</param>
        /// <param name="thc">thc.</param>
        /// <param name="thca">thca.</param>
        /// <param name="cbd">cbd.</param>
        /// <param name="cbda">cbda.</param>
        /// <param name="total">total.</param>
        /// <param name="stems">stems.</param>
        /// <param name="other">other.</param>
        /// <param name="aerobicBacteria">aerobicBacteria.</param>
        /// <param name="yeasAndMold">yeasAndMold.</param>
        /// <param name="coliforms">coliforms.</param>
        /// <param name="bileTolerant">bileTolerant.</param>
        /// <param name="eColiAndSalmonella">eColiAndSalmonella.</param>
        /// <param name="aflatoxinB1">aflatoxinB1.</param>
        /// <param name="aflatoxinB2">aflatoxinB2.</param>
        /// <param name="aflatoxinG1">aflatoxinG1.</param>
        /// <param name="aflatoxinG2">aflatoxinG2.</param>
        /// <param name="pesticideResidue">pesticideResidue.</param>
        public TestItem(long? id = default(long?), long? quanity = default(long?), bool? passed = default(bool?), string notes = default(string), decimal? thc = default(decimal?), decimal? thca = default(decimal?), decimal? cbd = default(decimal?), decimal? cbda = default(decimal?), decimal? total = default(decimal?), decimal? stems = default(decimal?), decimal? other = default(decimal?), decimal? aerobicBacteria = default(decimal?), decimal? yeasAndMold = default(decimal?), decimal? coliforms = default(decimal?), decimal? bileTolerant = default(decimal?), decimal? eColiAndSalmonella = default(decimal?), decimal? aflatoxinB1 = default(decimal?), decimal? aflatoxinB2 = default(decimal?), decimal? aflatoxinG1 = default(decimal?), decimal? aflatoxinG2 = default(decimal?), decimal? pesticideResidue = default(decimal?))
        {
            this.Id = id;
            this.Quanity = quanity;
            this.Passed = passed;
            this.Notes = notes;
            this.Thc = thc;
            this.Thca = thca;
            this.Cbd = cbd;
            this.Cbda = cbda;
            this.Total = total;
            this.Stems = stems;
            this.Other = other;
            this.AerobicBacteria = aerobicBacteria;
            this.YeasAndMold = yeasAndMold;
            this.Coliforms = coliforms;
            this.BileTolerant = bileTolerant;
            this.EColiAndSalmonella = eColiAndSalmonella;
            this.AflatoxinB1 = aflatoxinB1;
            this.AflatoxinB2 = aflatoxinB2;
            this.AflatoxinG1 = aflatoxinG1;
            this.AflatoxinG2 = aflatoxinG2;
            this.PesticideResidue = pesticideResidue;
        }
        
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name="id", EmitDefaultValue=false)]
        public long? Id { get; set; }

        /// <summary>
        /// Gets or Sets Quanity
        /// </summary>
        [DataMember(Name="quanity", EmitDefaultValue=false)]
        public long? Quanity { get; set; }

        /// <summary>
        /// Gets or Sets Passed
        /// </summary>
        [DataMember(Name="passed", EmitDefaultValue=false)]
        public bool? Passed { get; set; }

        /// <summary>
        /// Gets or Sets Notes
        /// </summary>
        [DataMember(Name="notes", EmitDefaultValue=false)]
        public string Notes { get; set; }

        /// <summary>
        /// Gets or Sets Thc
        /// </summary>
        [DataMember(Name="thc", EmitDefaultValue=false)]
        public decimal? Thc { get; set; }

        /// <summary>
        /// Gets or Sets Thca
        /// </summary>
        [DataMember(Name="thca", EmitDefaultValue=false)]
        public decimal? Thca { get; set; }

        /// <summary>
        /// Gets or Sets Cbd
        /// </summary>
        [DataMember(Name="cbd", EmitDefaultValue=false)]
        public decimal? Cbd { get; set; }

        /// <summary>
        /// Gets or Sets Cbda
        /// </summary>
        [DataMember(Name="cbda", EmitDefaultValue=false)]
        public decimal? Cbda { get; set; }

        /// <summary>
        /// Gets or Sets Total
        /// </summary>
        [DataMember(Name="total", EmitDefaultValue=false)]
        public decimal? Total { get; set; }

        /// <summary>
        /// Gets or Sets Stems
        /// </summary>
        [DataMember(Name="stems", EmitDefaultValue=false)]
        public decimal? Stems { get; set; }

        /// <summary>
        /// Gets or Sets Other
        /// </summary>
        [DataMember(Name="other", EmitDefaultValue=false)]
        public decimal? Other { get; set; }

        /// <summary>
        /// Gets or Sets AerobicBacteria
        /// </summary>
        [DataMember(Name="aerobic_bacteria", EmitDefaultValue=false)]
        public decimal? AerobicBacteria { get; set; }

        /// <summary>
        /// Gets or Sets YeasAndMold
        /// </summary>
        [DataMember(Name="yeas_and_mold", EmitDefaultValue=false)]
        public decimal? YeasAndMold { get; set; }

        /// <summary>
        /// Gets or Sets Coliforms
        /// </summary>
        [DataMember(Name="coliforms", EmitDefaultValue=false)]
        public decimal? Coliforms { get; set; }

        /// <summary>
        /// Gets or Sets BileTolerant
        /// </summary>
        [DataMember(Name="bile_tolerant", EmitDefaultValue=false)]
        public decimal? BileTolerant { get; set; }

        /// <summary>
        /// Gets or Sets EColiAndSalmonella
        /// </summary>
        [DataMember(Name="e_coli_and_salmonella", EmitDefaultValue=false)]
        public decimal? EColiAndSalmonella { get; set; }

        /// <summary>
        /// Gets or Sets AflatoxinB1
        /// </summary>
        [DataMember(Name="aflatoxin_b1", EmitDefaultValue=false)]
        public decimal? AflatoxinB1 { get; set; }

        /// <summary>
        /// Gets or Sets AflatoxinB2
        /// </summary>
        [DataMember(Name="aflatoxin_b2", EmitDefaultValue=false)]
        public decimal? AflatoxinB2 { get; set; }

        /// <summary>
        /// Gets or Sets AflatoxinG1
        /// </summary>
        [DataMember(Name="aflatoxin_g1", EmitDefaultValue=false)]
        public decimal? AflatoxinG1 { get; set; }

        /// <summary>
        /// Gets or Sets AflatoxinG2
        /// </summary>
        [DataMember(Name="aflatoxin_g2", EmitDefaultValue=false)]
        public decimal? AflatoxinG2 { get; set; }

        /// <summary>
        /// Gets or Sets PesticideResidue
        /// </summary>
        [DataMember(Name="pesticide_residue", EmitDefaultValue=false)]
        public decimal? PesticideResidue { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class TestItem {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Quanity: ").Append(Quanity).Append("\n");
            sb.Append("  Passed: ").Append(Passed).Append("\n");
            sb.Append("  Notes: ").Append(Notes).Append("\n");
            sb.Append("  Thc: ").Append(Thc).Append("\n");
            sb.Append("  Thca: ").Append(Thca).Append("\n");
            sb.Append("  Cbd: ").Append(Cbd).Append("\n");
            sb.Append("  Cbda: ").Append(Cbda).Append("\n");
            sb.Append("  Total: ").Append(Total).Append("\n");
            sb.Append("  Stems: ").Append(Stems).Append("\n");
            sb.Append("  Other: ").Append(Other).Append("\n");
            sb.Append("  AerobicBacteria: ").Append(AerobicBacteria).Append("\n");
            sb.Append("  YeasAndMold: ").Append(YeasAndMold).Append("\n");
            sb.Append("  Coliforms: ").Append(Coliforms).Append("\n");
            sb.Append("  BileTolerant: ").Append(BileTolerant).Append("\n");
            sb.Append("  EColiAndSalmonella: ").Append(EColiAndSalmonella).Append("\n");
            sb.Append("  AflatoxinB1: ").Append(AflatoxinB1).Append("\n");
            sb.Append("  AflatoxinB2: ").Append(AflatoxinB2).Append("\n");
            sb.Append("  AflatoxinG1: ").Append(AflatoxinG1).Append("\n");
            sb.Append("  AflatoxinG2: ").Append(AflatoxinG2).Append("\n");
            sb.Append("  PesticideResidue: ").Append(PesticideResidue).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as TestItem);
        }

        /// <summary>
        /// Returns true if TestItem instances are equal
        /// </summary>
        /// <param name="input">Instance of TestItem to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(TestItem input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Id == input.Id ||
                    (this.Id != null &&
                    this.Id.Equals(input.Id))
                ) && 
                (
                    this.Quanity == input.Quanity ||
                    (this.Quanity != null &&
                    this.Quanity.Equals(input.Quanity))
                ) && 
                (
                    this.Passed == input.Passed ||
                    (this.Passed != null &&
                    this.Passed.Equals(input.Passed))
                ) && 
                (
                    this.Notes == input.Notes ||
                    (this.Notes != null &&
                    this.Notes.Equals(input.Notes))
                ) && 
                (
                    this.Thc == input.Thc ||
                    (this.Thc != null &&
                    this.Thc.Equals(input.Thc))
                ) && 
                (
                    this.Thca == input.Thca ||
                    (this.Thca != null &&
                    this.Thca.Equals(input.Thca))
                ) && 
                (
                    this.Cbd == input.Cbd ||
                    (this.Cbd != null &&
                    this.Cbd.Equals(input.Cbd))
                ) && 
                (
                    this.Cbda == input.Cbda ||
                    (this.Cbda != null &&
                    this.Cbda.Equals(input.Cbda))
                ) && 
                (
                    this.Total == input.Total ||
                    (this.Total != null &&
                    this.Total.Equals(input.Total))
                ) && 
                (
                    this.Stems == input.Stems ||
                    (this.Stems != null &&
                    this.Stems.Equals(input.Stems))
                ) && 
                (
                    this.Other == input.Other ||
                    (this.Other != null &&
                    this.Other.Equals(input.Other))
                ) && 
                (
                    this.AerobicBacteria == input.AerobicBacteria ||
                    (this.AerobicBacteria != null &&
                    this.AerobicBacteria.Equals(input.AerobicBacteria))
                ) && 
                (
                    this.YeasAndMold == input.YeasAndMold ||
                    (this.YeasAndMold != null &&
                    this.YeasAndMold.Equals(input.YeasAndMold))
                ) && 
                (
                    this.Coliforms == input.Coliforms ||
                    (this.Coliforms != null &&
                    this.Coliforms.Equals(input.Coliforms))
                ) && 
                (
                    this.BileTolerant == input.BileTolerant ||
                    (this.BileTolerant != null &&
                    this.BileTolerant.Equals(input.BileTolerant))
                ) && 
                (
                    this.EColiAndSalmonella == input.EColiAndSalmonella ||
                    (this.EColiAndSalmonella != null &&
                    this.EColiAndSalmonella.Equals(input.EColiAndSalmonella))
                ) && 
                (
                    this.AflatoxinB1 == input.AflatoxinB1 ||
                    (this.AflatoxinB1 != null &&
                    this.AflatoxinB1.Equals(input.AflatoxinB1))
                ) && 
                (
                    this.AflatoxinB2 == input.AflatoxinB2 ||
                    (this.AflatoxinB2 != null &&
                    this.AflatoxinB2.Equals(input.AflatoxinB2))
                ) && 
                (
                    this.AflatoxinG1 == input.AflatoxinG1 ||
                    (this.AflatoxinG1 != null &&
                    this.AflatoxinG1.Equals(input.AflatoxinG1))
                ) && 
                (
                    this.AflatoxinG2 == input.AflatoxinG2 ||
                    (this.AflatoxinG2 != null &&
                    this.AflatoxinG2.Equals(input.AflatoxinG2))
                ) && 
                (
                    this.PesticideResidue == input.PesticideResidue ||
                    (this.PesticideResidue != null &&
                    this.PesticideResidue.Equals(input.PesticideResidue))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Id != null)
                    hashCode = hashCode * 59 + this.Id.GetHashCode();
                if (this.Quanity != null)
                    hashCode = hashCode * 59 + this.Quanity.GetHashCode();
                if (this.Passed != null)
                    hashCode = hashCode * 59 + this.Passed.GetHashCode();
                if (this.Notes != null)
                    hashCode = hashCode * 59 + this.Notes.GetHashCode();
                if (this.Thc != null)
                    hashCode = hashCode * 59 + this.Thc.GetHashCode();
                if (this.Thca != null)
                    hashCode = hashCode * 59 + this.Thca.GetHashCode();
                if (this.Cbd != null)
                    hashCode = hashCode * 59 + this.Cbd.GetHashCode();
                if (this.Cbda != null)
                    hashCode = hashCode * 59 + this.Cbda.GetHashCode();
                if (this.Total != null)
                    hashCode = hashCode * 59 + this.Total.GetHashCode();
                if (this.Stems != null)
                    hashCode = hashCode * 59 + this.Stems.GetHashCode();
                if (this.Other != null)
                    hashCode = hashCode * 59 + this.Other.GetHashCode();
                if (this.AerobicBacteria != null)
                    hashCode = hashCode * 59 + this.AerobicBacteria.GetHashCode();
                if (this.YeasAndMold != null)
                    hashCode = hashCode * 59 + this.YeasAndMold.GetHashCode();
                if (this.Coliforms != null)
                    hashCode = hashCode * 59 + this.Coliforms.GetHashCode();
                if (this.BileTolerant != null)
                    hashCode = hashCode * 59 + this.BileTolerant.GetHashCode();
                if (this.EColiAndSalmonella != null)
                    hashCode = hashCode * 59 + this.EColiAndSalmonella.GetHashCode();
                if (this.AflatoxinB1 != null)
                    hashCode = hashCode * 59 + this.AflatoxinB1.GetHashCode();
                if (this.AflatoxinB2 != null)
                    hashCode = hashCode * 59 + this.AflatoxinB2.GetHashCode();
                if (this.AflatoxinG1 != null)
                    hashCode = hashCode * 59 + this.AflatoxinG1.GetHashCode();
                if (this.AflatoxinG2 != null)
                    hashCode = hashCode * 59 + this.AflatoxinG2.GetHashCode();
                if (this.PesticideResidue != null)
                    hashCode = hashCode * 59 + this.PesticideResidue.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
