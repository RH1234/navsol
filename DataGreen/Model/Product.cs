/* 
 * data.green API Gateway
 *
 * Unified access to major jurisdictionally-mandated track-and-trace software providers.
 *
 * OpenAPI spec version: 2.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = IO.Swagger.Client.SwaggerDateConverter;

namespace IO.Swagger.Model
{
    /// <summary>
    /// Product
    /// </summary>
    [DataContract]
    public partial class Product :  IEquatable<Product>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Product" /> class.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="name">Populated in both METRC and Biotrack..</param>
        /// <param name="category">Only populated in METRC. The category is used to describe the product..</param>
        /// <param name="unitId">unitId.</param>
        /// <param name="strainId">strainId.</param>
        /// <param name="brand">Only populated in METRC. This allows one to add branding to a product..</param>
        /// <param name="administrationMethod">Only populated in METRC. The administration_method describes how a product should be administered..</param>
        /// <param name="cbdPercent">Only populated in METRC. The cbd_percent describes the cdb percentage..</param>
        /// <param name="cbdContent">Only populated in METRC. The cbd_content describes the product cbd content..</param>
        /// <param name="cbdContentUnitId">cbdContentUnitId.</param>
        /// <param name="thcPercent">Only populated in METRC. The thc_percent describes the thc percentage..</param>
        /// <param name="thcContent">Only populated in METRC. The thc_content describes the product thc content..</param>
        /// <param name="thcContentUnitId">thcContentUnitId.</param>
        /// <param name="volumeAmount">Only populated in METRC. The volume amount of the product..</param>
        /// <param name="volumeUnitId">volumeUnitId.</param>
        /// <param name="weightAmount">Only populated in METRC. The weight amount of the product..</param>
        /// <param name="weightUnitId">Only populated in METRC. The unit of measure used to describe the weight of the product..</param>
        /// <param name="servingSize">Only populated in METRC. The serving size is the amount of teh product a user is recommended to take at a single time..</param>
        /// <param name="supplyDurationDays">Only populated in METRC. The amount of time the product is expected to last for..</param>
        /// <param name="ingredients">Only populated in METRC. The ingredients that contribute to creating the end product..</param>
        public Product(long? id = default(long?), string name = default(string), string category = default(string), long? unitId = default(long?), long? strainId = default(long?), string brand = default(string), string administrationMethod = default(string), double? cbdPercent = default(double?), double? cbdContent = default(double?), long? cbdContentUnitId = default(long?), double? thcPercent = default(double?), double? thcContent = default(double?), long? thcContentUnitId = default(long?), double? volumeAmount = default(double?), long? volumeUnitId = default(long?), double? weightAmount = default(double?), long? weightUnitId = default(long?), string servingSize = default(string), long? supplyDurationDays = default(long?), string ingredients = default(string))
        {
            this.Id = id;
            this.Name = name;
            this.Category = category;
            this.UnitId = unitId;
            this.StrainId = strainId;
            this.Brand = brand;
            this.AdministrationMethod = administrationMethod;
            this.CbdPercent = cbdPercent;
            this.CbdContent = cbdContent;
            this.CbdContentUnitId = cbdContentUnitId;
            this.ThcPercent = thcPercent;
            this.ThcContent = thcContent;
            this.ThcContentUnitId = thcContentUnitId;
            this.VolumeAmount = volumeAmount;
            this.VolumeUnitId = volumeUnitId;
            this.WeightAmount = weightAmount;
            this.WeightUnitId = weightUnitId;
            this.ServingSize = servingSize;
            this.SupplyDurationDays = supplyDurationDays;
            this.Ingredients = ingredients;
        }
        
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name="id", EmitDefaultValue=false)]
        public long? Id { get; set; }

        /// <summary>
        /// Populated in both METRC and Biotrack.
        /// </summary>
        /// <value>Populated in both METRC and Biotrack.</value>
        [DataMember(Name="name", EmitDefaultValue=false)]
        public string Name { get; set; }

        /// <summary>
        /// Only populated in METRC. The category is used to describe the product.
        /// </summary>
        /// <value>Only populated in METRC. The category is used to describe the product.</value>
        [DataMember(Name="category", EmitDefaultValue=false)]
        public string Category { get; set; }

        /// <summary>
        /// Gets or Sets UnitId
        /// </summary>
        [DataMember(Name="unit_id", EmitDefaultValue=false)]
        public long? UnitId { get; set; }

        /// <summary>
        /// Gets or Sets StrainId
        /// </summary>
        [DataMember(Name="strain_id", EmitDefaultValue=false)]
        public long? StrainId { get; set; }

        /// <summary>
        /// Only populated in METRC. This allows one to add branding to a product.
        /// </summary>
        /// <value>Only populated in METRC. This allows one to add branding to a product.</value>
        [DataMember(Name="brand", EmitDefaultValue=false)]
        public string Brand { get; set; }

        /// <summary>
        /// Only populated in METRC. The administration_method describes how a product should be administered.
        /// </summary>
        /// <value>Only populated in METRC. The administration_method describes how a product should be administered.</value>
        [DataMember(Name="administration_method", EmitDefaultValue=false)]
        public string AdministrationMethod { get; set; }

        /// <summary>
        /// Only populated in METRC. The cbd_percent describes the cdb percentage.
        /// </summary>
        /// <value>Only populated in METRC. The cbd_percent describes the cdb percentage.</value>
        [DataMember(Name="cbd_percent", EmitDefaultValue=false)]
        public double? CbdPercent { get; set; }

        /// <summary>
        /// Only populated in METRC. The cbd_content describes the product cbd content.
        /// </summary>
        /// <value>Only populated in METRC. The cbd_content describes the product cbd content.</value>
        [DataMember(Name="cbd_content", EmitDefaultValue=false)]
        public double? CbdContent { get; set; }

        /// <summary>
        /// Gets or Sets CbdContentUnitId
        /// </summary>
        [DataMember(Name="cbd_content_unit_id", EmitDefaultValue=false)]
        public long? CbdContentUnitId { get; set; }

        /// <summary>
        /// Only populated in METRC. The thc_percent describes the thc percentage.
        /// </summary>
        /// <value>Only populated in METRC. The thc_percent describes the thc percentage.</value>
        [DataMember(Name="thc_percent", EmitDefaultValue=false)]
        public double? ThcPercent { get; set; }

        /// <summary>
        /// Only populated in METRC. The thc_content describes the product thc content.
        /// </summary>
        /// <value>Only populated in METRC. The thc_content describes the product thc content.</value>
        [DataMember(Name="thc_content", EmitDefaultValue=false)]
        public double? ThcContent { get; set; }

        /// <summary>
        /// Gets or Sets ThcContentUnitId
        /// </summary>
        [DataMember(Name="thc_content_unit_id", EmitDefaultValue=false)]
        public long? ThcContentUnitId { get; set; }

        /// <summary>
        /// Only populated in METRC. The volume amount of the product.
        /// </summary>
        /// <value>Only populated in METRC. The volume amount of the product.</value>
        [DataMember(Name="volume_amount", EmitDefaultValue=false)]
        public double? VolumeAmount { get; set; }

        /// <summary>
        /// Gets or Sets VolumeUnitId
        /// </summary>
        [DataMember(Name="volume_unit_id", EmitDefaultValue=false)]
        public long? VolumeUnitId { get; set; }

        /// <summary>
        /// Only populated in METRC. The weight amount of the product.
        /// </summary>
        /// <value>Only populated in METRC. The weight amount of the product.</value>
        [DataMember(Name="weight_amount", EmitDefaultValue=false)]
        public double? WeightAmount { get; set; }

        /// <summary>
        /// Only populated in METRC. The unit of measure used to describe the weight of the product.
        /// </summary>
        /// <value>Only populated in METRC. The unit of measure used to describe the weight of the product.</value>
        [DataMember(Name="weight_unit_id", EmitDefaultValue=false)]
        public long? WeightUnitId { get; set; }

        /// <summary>
        /// Only populated in METRC. The serving size is the amount of teh product a user is recommended to take at a single time.
        /// </summary>
        /// <value>Only populated in METRC. The serving size is the amount of teh product a user is recommended to take at a single time.</value>
        [DataMember(Name="serving_size", EmitDefaultValue=false)]
        public string ServingSize { get; set; }

        /// <summary>
        /// Only populated in METRC. The amount of time the product is expected to last for.
        /// </summary>
        /// <value>Only populated in METRC. The amount of time the product is expected to last for.</value>
        [DataMember(Name="supply_duration_days", EmitDefaultValue=false)]
        public long? SupplyDurationDays { get; set; }

        /// <summary>
        /// Only populated in METRC. The ingredients that contribute to creating the end product.
        /// </summary>
        /// <value>Only populated in METRC. The ingredients that contribute to creating the end product.</value>
        [DataMember(Name="ingredients", EmitDefaultValue=false)]
        public string Ingredients { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Product {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Category: ").Append(Category).Append("\n");
            sb.Append("  UnitId: ").Append(UnitId).Append("\n");
            sb.Append("  StrainId: ").Append(StrainId).Append("\n");
            sb.Append("  Brand: ").Append(Brand).Append("\n");
            sb.Append("  AdministrationMethod: ").Append(AdministrationMethod).Append("\n");
            sb.Append("  CbdPercent: ").Append(CbdPercent).Append("\n");
            sb.Append("  CbdContent: ").Append(CbdContent).Append("\n");
            sb.Append("  CbdContentUnitId: ").Append(CbdContentUnitId).Append("\n");
            sb.Append("  ThcPercent: ").Append(ThcPercent).Append("\n");
            sb.Append("  ThcContent: ").Append(ThcContent).Append("\n");
            sb.Append("  ThcContentUnitId: ").Append(ThcContentUnitId).Append("\n");
            sb.Append("  VolumeAmount: ").Append(VolumeAmount).Append("\n");
            sb.Append("  VolumeUnitId: ").Append(VolumeUnitId).Append("\n");
            sb.Append("  WeightAmount: ").Append(WeightAmount).Append("\n");
            sb.Append("  WeightUnitId: ").Append(WeightUnitId).Append("\n");
            sb.Append("  ServingSize: ").Append(ServingSize).Append("\n");
            sb.Append("  SupplyDurationDays: ").Append(SupplyDurationDays).Append("\n");
            sb.Append("  Ingredients: ").Append(Ingredients).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Product);
        }

        /// <summary>
        /// Returns true if Product instances are equal
        /// </summary>
        /// <param name="input">Instance of Product to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Product input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Id == input.Id ||
                    (this.Id != null &&
                    this.Id.Equals(input.Id))
                ) && 
                (
                    this.Name == input.Name ||
                    (this.Name != null &&
                    this.Name.Equals(input.Name))
                ) && 
                (
                    this.Category == input.Category ||
                    (this.Category != null &&
                    this.Category.Equals(input.Category))
                ) && 
                (
                    this.UnitId == input.UnitId ||
                    (this.UnitId != null &&
                    this.UnitId.Equals(input.UnitId))
                ) && 
                (
                    this.StrainId == input.StrainId ||
                    (this.StrainId != null &&
                    this.StrainId.Equals(input.StrainId))
                ) && 
                (
                    this.Brand == input.Brand ||
                    (this.Brand != null &&
                    this.Brand.Equals(input.Brand))
                ) && 
                (
                    this.AdministrationMethod == input.AdministrationMethod ||
                    (this.AdministrationMethod != null &&
                    this.AdministrationMethod.Equals(input.AdministrationMethod))
                ) && 
                (
                    this.CbdPercent == input.CbdPercent ||
                    (this.CbdPercent != null &&
                    this.CbdPercent.Equals(input.CbdPercent))
                ) && 
                (
                    this.CbdContent == input.CbdContent ||
                    (this.CbdContent != null &&
                    this.CbdContent.Equals(input.CbdContent))
                ) && 
                (
                    this.CbdContentUnitId == input.CbdContentUnitId ||
                    (this.CbdContentUnitId != null &&
                    this.CbdContentUnitId.Equals(input.CbdContentUnitId))
                ) && 
                (
                    this.ThcPercent == input.ThcPercent ||
                    (this.ThcPercent != null &&
                    this.ThcPercent.Equals(input.ThcPercent))
                ) && 
                (
                    this.ThcContent == input.ThcContent ||
                    (this.ThcContent != null &&
                    this.ThcContent.Equals(input.ThcContent))
                ) && 
                (
                    this.ThcContentUnitId == input.ThcContentUnitId ||
                    (this.ThcContentUnitId != null &&
                    this.ThcContentUnitId.Equals(input.ThcContentUnitId))
                ) && 
                (
                    this.VolumeAmount == input.VolumeAmount ||
                    (this.VolumeAmount != null &&
                    this.VolumeAmount.Equals(input.VolumeAmount))
                ) && 
                (
                    this.VolumeUnitId == input.VolumeUnitId ||
                    (this.VolumeUnitId != null &&
                    this.VolumeUnitId.Equals(input.VolumeUnitId))
                ) && 
                (
                    this.WeightAmount == input.WeightAmount ||
                    (this.WeightAmount != null &&
                    this.WeightAmount.Equals(input.WeightAmount))
                ) && 
                (
                    this.WeightUnitId == input.WeightUnitId ||
                    (this.WeightUnitId != null &&
                    this.WeightUnitId.Equals(input.WeightUnitId))
                ) && 
                (
                    this.ServingSize == input.ServingSize ||
                    (this.ServingSize != null &&
                    this.ServingSize.Equals(input.ServingSize))
                ) && 
                (
                    this.SupplyDurationDays == input.SupplyDurationDays ||
                    (this.SupplyDurationDays != null &&
                    this.SupplyDurationDays.Equals(input.SupplyDurationDays))
                ) && 
                (
                    this.Ingredients == input.Ingredients ||
                    (this.Ingredients != null &&
                    this.Ingredients.Equals(input.Ingredients))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Id != null)
                    hashCode = hashCode * 59 + this.Id.GetHashCode();
                if (this.Name != null)
                    hashCode = hashCode * 59 + this.Name.GetHashCode();
                if (this.Category != null)
                    hashCode = hashCode * 59 + this.Category.GetHashCode();
                if (this.UnitId != null)
                    hashCode = hashCode * 59 + this.UnitId.GetHashCode();
                if (this.StrainId != null)
                    hashCode = hashCode * 59 + this.StrainId.GetHashCode();
                if (this.Brand != null)
                    hashCode = hashCode * 59 + this.Brand.GetHashCode();
                if (this.AdministrationMethod != null)
                    hashCode = hashCode * 59 + this.AdministrationMethod.GetHashCode();
                if (this.CbdPercent != null)
                    hashCode = hashCode * 59 + this.CbdPercent.GetHashCode();
                if (this.CbdContent != null)
                    hashCode = hashCode * 59 + this.CbdContent.GetHashCode();
                if (this.CbdContentUnitId != null)
                    hashCode = hashCode * 59 + this.CbdContentUnitId.GetHashCode();
                if (this.ThcPercent != null)
                    hashCode = hashCode * 59 + this.ThcPercent.GetHashCode();
                if (this.ThcContent != null)
                    hashCode = hashCode * 59 + this.ThcContent.GetHashCode();
                if (this.ThcContentUnitId != null)
                    hashCode = hashCode * 59 + this.ThcContentUnitId.GetHashCode();
                if (this.VolumeAmount != null)
                    hashCode = hashCode * 59 + this.VolumeAmount.GetHashCode();
                if (this.VolumeUnitId != null)
                    hashCode = hashCode * 59 + this.VolumeUnitId.GetHashCode();
                if (this.WeightAmount != null)
                    hashCode = hashCode * 59 + this.WeightAmount.GetHashCode();
                if (this.WeightUnitId != null)
                    hashCode = hashCode * 59 + this.WeightUnitId.GetHashCode();
                if (this.ServingSize != null)
                    hashCode = hashCode * 59 + this.ServingSize.GetHashCode();
                if (this.SupplyDurationDays != null)
                    hashCode = hashCode * 59 + this.SupplyDurationDays.GetHashCode();
                if (this.Ingredients != null)
                    hashCode = hashCode * 59 + this.Ingredients.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
