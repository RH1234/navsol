using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace B1WizardBase
{
  /// <summary>
  /// Helping class containing the Datas of a matrix in a form.
  /// To obtain the information this class is based on the Matrix.SerializeAsXml B1 SDK method.
  /// </summary>
  public class B1Matrix 
  {
    /// <summary>
    /// Basic class serialized, not exposed directly to avoid naming conflict with SAPbouiCOM.Matrix
    /// </summary>
    private B1WizardMatrix.Matrix serMatrix = null;

    /// <summary>
    /// Returns a B1Matrix (deserialized from the Xml obtained from SAPbouiCOM.Matrix.SerializeAsXML method) 
    /// B1Matrix contains all the information shown by the matrix in the form
    /// </summary>
    /// <example>
    /// In order to load the information in a matrix just do the following:
    /// SAPbouiCOM.Matrix oMatrix = (SAPbouiCOM.Matrix)form.Items.Item("xx").Specific
    /// B1Matrix b1Matrix = new B1Matrix(oMatrix);
    /// string str = b1Matrix.Rows[0].Columns[3].Value.ToString();
    /// </example>
    /// <param name="mtx">Matrix we want to read the information</param>
    /// <returns></returns>
    public B1Matrix(SAPbouiCOM.Matrix mtx)
    {
      try
      {
        string sMatrix = mtx.SerializeAsXML(SAPbouiCOM.BoMatrixXmlSelect.mxs_All);
        StringReader StrReader = new StringReader(sMatrix);
        System.Xml.Serialization.XmlSerializer XmlSerializer =
          new System.Xml.Serialization.XmlSerializer(typeof(B1WizardMatrix.Matrix));
        serMatrix = (B1WizardMatrix.Matrix)XmlSerializer.Deserialize(StrReader);
      }
      catch (Exception ex)
      {
        B1Connections.theAppl.MessageBox("Error in B1Matrix constructor " + ex.Message, 1, "Ok", "", "");
      }
    }

    /// <summary>
    /// Indicates whether this item triggers a change in the form mode.
    /// </summary>
    public bool AffectsFormMode
    {
      get
      {
        return serMatrix.affectsFormModeField;
      }
      set
      {
        serMatrix.affectsFormModeField = value;
      }
    }

    /// <summary>
    /// The background color of text or graphics in an item.
    /// </summary>
    public long BackColor
    {
      get
      {
        return serMatrix.backColorField;
      }
      set
      {
        serMatrix.backColorField = value;
      }
    }

    /// <summary>
    /// The item description in the SAP Business One status bar.
    /// </summary>
    public string Description
    {
      get
      {
        return serMatrix.descriptionField;
      }
      set
      {
        serMatrix.descriptionField = value;
      }
    }

    /// <summary>
    /// Indicates whether to show the description of valid values. 
    /// True: Displays the description of the valid value.
    /// False: Displays the valid value.
    /// </summary>
    public bool DisplayDesc
    {
      get
      {
        return serMatrix.displayDescField;
      }
      set
      {
        serMatrix.displayDescField = value;
      }
    }

    /// <remarks/>
    public bool Enabled
    {
      get
      {
        return serMatrix.enabledField;
      }
      set
      {
        serMatrix.enabledField = value;
      }
    }

    /// <summary>
    /// The font size for EditText, ComboBox, and StaticText items.
    /// </summary>
    public long FontSize
    {
      get
      {
        return serMatrix.fontSizeField;
      }
      set
      {
        serMatrix.fontSizeField = value;
      }
    }

    /// <summary>
    /// The foreground color of text or graphics in an item.
    /// </summary>
    public long ForeColor
    {
      get
      {
        return serMatrix.foreColorField;
      }
      set
      {
        serMatrix.foreColorField = value;
      }
    }

    /// <summary>
    /// The start of the range of panes on which the item is visible (default is 0). 
    /// If the form has more than one pane, you can specify a range of panes on which the
    /// item is displayed. 
    /// </summary>
    public long FromPane
    {
      get
      {
        return serMatrix.fromPaneField;
      }
      set
      {
        serMatrix.fromPaneField = value;
      }
    }

    /// <summary>
    /// The matrix's height.
    /// </summary>
    public long Height
    {
      get
      {
        return serMatrix.heightField;
      }
      set
      {
        serMatrix.heightField = value;
      }
    }

    /// <summary>
    /// The matrix layout.
    /// </summary>
    public long Layout
    {
      get
      {
        return serMatrix.layoutField;
      }
      set
      {
        serMatrix.layoutField = value;
      }
    }

    /// <summary>
    /// The matrix's left position.
    /// </summary>
    public long Left
    {
      get
      {
        return serMatrix.leftField;
      }
      set
      {
        serMatrix.leftField = value;
      }
    }

    /// <summary>
    /// A link to another item in the system.
    /// </summary>
    public string LinkTo
    {
      get
      {
        return serMatrix.linkToField;
      }
      set
      {
        serMatrix.linkToField = value;
      }
    }

    /// <summary>
    /// Indicates whether the item is right-justified.
    /// </summary>
    public bool RightJustified
    {
      get
      {
        return serMatrix.rightJustifiedField;
      }
      set
      {
        serMatrix.rightJustifiedField = value;
      }
    }

    /// <summary>
    /// Indicates the font style of text displayed by the item.
    /// </summary>
    public long TextStyle
    {
      get
      {
        return serMatrix.textStyleField;
      }
      set
      {
        serMatrix.textStyleField = value;
      }
    }

    /// <summary>
    /// The matrix's top position.
    /// </summary>
    public long Top
    {
      get
      {
        return serMatrix.topField;
      }
      set
      {
        serMatrix.topField = value;
      }
    }

    /// <summary>
    /// The end of the range of panes on which the item is visible (default is 0). 
    /// If the form has more than one pane, you can specify a range of panes on which the
    /// item is displayed. 
    /// </summary>
    public long ToPane
    {
      get
      {
        return serMatrix.toPaneField;
      }
      set
      {
        serMatrix.toPaneField = value;
      }
    }

    /// <summary>
    /// The matrix type.
    /// </summary>
    public long Type
    {
      get
      {
        return serMatrix.typeField;
      }
      set
      {
        serMatrix.typeField = value;
      }
    }

    /// <summary>
    /// The matrix unique id.
    /// </summary>
    public string UniqueID
    {
      get
      {
        return serMatrix.uniqueIDField;
      }
      set
      {
        serMatrix.uniqueIDField = value;
      }
    }

    /// <summary>
    /// Indicates whether the matrix is visible.
    /// </summary>
    public bool Visible
    {
      get
      {
        return serMatrix.visibleField;
      }
      set
      {
        serMatrix.visibleField = value;
      }
    }

    /// <summary>
    /// The matrix width.
    /// </summary>
    public long Width
    {
      get
      {
        return serMatrix.widthField;
      }
      set
      {
        serMatrix.widthField = value;
      }
    }

    /// <summary>
    /// Information about the matrix columns.
    /// </summary>
    [System.Xml.Serialization.XmlArrayItemAttribute("ColumnInfo", IsNullable = false)]
    public B1WizardMatrix.MatrixColumnInfo[] ColumnsInfo
    {
      get
      {
        return serMatrix.columnsInfoField;
      }
      set
      {
        serMatrix.columnsInfoField = value;
      }
    }

    /// <summary>
    /// Datas of the matrix per row.
    /// <example>
    /// b1Matrix.Rows[0].Columns[3].Value
    /// </example>
    /// </summary>
    [System.Xml.Serialization.XmlArrayItemAttribute("Row", IsNullable = false)]
    public B1WizardMatrix.MatrixRow[] Rows
    {
      get
      {
        return serMatrix.rowsField;
      }
      set
      {
        serMatrix.rowsField = value;
      }
    }

  }


  //public class B1Matrix : B1WizardBase.Matrix
  //{
  //  //B1WizardBase.Matrix serMatrix = null;

  //  /// <summary>
  //  /// Returns a B1Matrix (deserialized from the Xml obtained from SAPbouiCOM.Matrix.SerializeAsXML method) 
  //  /// B1Matrix contains all the information shown by the matrix in the form
  //  /// </summary>
  //  /// <example>
  //  /// In order to load the information in a matrix just do the following:
  //  /// B1MatrixDeserializedXml b1Matrix = B1MatrixDeserializedXml.GetData(oMatrix);
  //  /// string str = b1Matrix.Rows[0].Columns[3].Value.ToString();
  //  /// </example>
  //  /// <param name="mtx">Matrix we need to read its information</param>
  //  /// <returns></returns>
  //  public static B1Matrix LoadData(SAPbouiCOM.Matrix mtx)
  //  {
  //    string sMatrix = mtx.SerializeAsXML(BoMatrixXmlSelect.mxs_All);
  //    StringReader StrReader = new StringReader(sMatrix);
  //    System.Xml.Serialization.XmlSerializer XmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(B1WizardBase.Matrix));
  //    return (B1WizardBase.B1Matrix)XmlSerializer.Deserialize(StrReader);
  //  }
  //}

}
