//****************************************************************************
//
//  File:      B1Listener.cs
//
//  Copyright (c) SAP 
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
//****************************************************************************
using System;
using System.Reflection;

namespace B1WizardBase
{
  /// <summary>
  /// Represents a listener to manage the Events.
  /// </summary>
  public class B1Listener
  {
    /// <summary>
    /// Action to represent.
    /// </summary>
    public B1Action	Action;
    /// <summary>
    /// Name of the method representing the listener.
    /// </summary>
    public MethodInfo	Method;

    /////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Buils a new B1Listener.
    /// </summary>
    /// <param name="action">Action to represent</param>
    /// <param name="method">Name of the method associated</param>
    public B1Listener(B1Action action, MethodInfo method)
    {
      this.Action = action;
      this.Method = method;
    }
  }
}
