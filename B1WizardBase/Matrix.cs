﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.1433
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.IO;
using System.Xml.Serialization;

// 
// This source code was auto-generated by xsd, Version=2.0.50727.42.
// 

namespace B1WizardMatrix  
{

  /// <summary>
  /// Helping class containing the Datas of a matrix in a form.
  /// To obtain the information this class is based on the Matrix.SerializeAsXml B1 SDK method.
  /// Use the Init static method in order to obtain an instance of this class
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
  public partial class Matrix
  {
    /// <summary>
    /// Initialize an instance of the B1WizardMatrix.Matrix class in order to read
    /// the information from the SAPbouiCOm.Matrix object on B1 forms
    /// Returns a B1WizardMatrix.Matrix instance (deserialized from the Xml obtained from SAPbouiCOM.Matrix.SerializeAsXML method) 
    /// B1WizardMatrix.Matrix contains all the information shown by the matrix in the form
    /// <param name="oMtx">Matrix we want to read the information</param>
    /// </summary>
    /// <example>
    /// In order to load the information in a matrix just do the following:
    /// <code lang="Visual Basic">
    /// Dim oMatrix as SAPbouiCOM.Matrix = form.Items.Item("xx").Specific
    /// Dim b1Matrix as B1WizardMatrix.Matrix = new B1WizardMatrix.Matrix(oMatrix)
    /// Dim itemCode as string = b1Matrix.Rows(0).Columns(3).Value
    /// </code>
    /// <code lang="C#" escaped="true">
    /// SAPbouiCOM.Matrix oMatrix = (SAPbouiCOM.Matrix)form.Items.Item("xx").Specific;
    /// B1WizardMatrix.Matrix b1Matrix = new B1WizardMatrix.Matrix(oMatrix);
    /// string itemCode = b1Matrix.Rows[0].Columns[3].Value.ToString();
    /// </code>
    /// </example>
    static public B1WizardMatrix.Matrix Init(SAPbouiCOM.Matrix oMtx)
    {
      try
      {
        string sMatrix = oMtx.SerializeAsXML(SAPbouiCOM.BoMatrixXmlSelect.mxs_All);
        StringReader StrReader = new StringReader(sMatrix);
        System.Xml.Serialization.XmlSerializer XmlSerializer =
          new System.Xml.Serialization.XmlSerializer(typeof(B1WizardMatrix.Matrix));
        return (B1WizardMatrix.Matrix)XmlSerializer.Deserialize(StrReader);
      }
      catch (Exception ex)
      {
        B1WizardBase.B1Connections.theAppl.MessageBox("Error in B1Matrix constructor " + ex.Message, 1, "Ok", "", "");
      }
      return null;
    }

    /// <summary>
    /// Use B1WizardMatrix.Init method instead of B1WizardMatrix.Matrix constructor.
    /// Constructor blocked, use B1WizardMatrix.Init instead.
    /// </summary>
    private Matrix()  {}

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private bool affectsFormModeField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private long backColorField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private string descriptionField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private bool displayDescField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private bool enabledField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private long fontSizeField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private long foreColorField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private long fromPaneField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private long heightField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private long layoutField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private long leftField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private string linkToField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private bool rightJustifiedField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private long textStyleField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private long topField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private long toPaneField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private long typeField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private string uniqueIDField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private bool visibleField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private long widthField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private MatrixColumnInfo[] columnsInfoField;

    /// <summary>
    /// Use B1WizardBase.B1Matrix class instead of B1WizardMatrix.Matrix class.
    /// </summary>
    private MatrixRow[] rowsField;

    /// <summary>
    /// Indicates whether this item triggers a change in the form mode.
    /// </summary>
    public bool AffectsFormMode
    {
      get
      {
        return this.affectsFormModeField;
      }
      set
      {
        this.affectsFormModeField = value;
      }
    }

    /// <summary>
    /// The background color of text or graphics in an item.
    /// </summary>
    public long BackColor
    {
      get
      {
        return this.backColorField;
      }
      set
      {
        this.backColorField = value;
      }
    }

    /// <summary>
    /// The item description in the SAP Business One status bar.
    /// </summary>
    public string Description
    {
      get
      {
        return this.descriptionField;
      }
      set
      {
        this.descriptionField = value;
      }
    }

    /// <summary>
    /// Indicates whether to show the description of valid values. 
    /// True: Displays the description of the valid value.
    /// False: Displays the valid value.
    /// </summary>
    public bool DisplayDesc
    {
      get
      {
        return this.displayDescField;
      }
      set
      {
        this.displayDescField = value;
      }
    }

    /// <summary>
    /// Indicates whether the field is enabled or not. 
    /// </summary>
    public bool Enabled
    {
      get
      {
        return this.enabledField;
      }
      set
      {
        this.enabledField = value;
      }
    }

    /// <summary>
    /// The font size for EditText, ComboBox, and StaticText items.
    /// </summary>
    public long FontSize
    {
      get
      {
        return this.fontSizeField;
      }
      set
      {
        this.fontSizeField = value;
      }
    }

    /// <summary>
    /// The foreground color of text or graphics in an item.
    /// </summary>
    public long ForeColor
    {
      get
      {
        return this.foreColorField;
      }
      set
      {
        this.foreColorField = value;
      }
    }

    /// <summary>
    /// The start of the range of panes on which the item is visible (default is 0). 
    /// If the form has more than one pane, you can specify a range of panes on which the
    /// item is displayed. 
    /// </summary>
    public long FromPane
    {
      get
      {
        return this.fromPaneField;
      }
      set
      {
        this.fromPaneField = value;
      }
    }

    /// <summary>
    /// The matrix's height.
    /// </summary>
    public long Height
    {
      get
      {
        return this.heightField;
      }
      set
      {
        this.heightField = value;
      }
    }

    /// <summary>
    /// The matrix layout.
    /// </summary>
    public long Layout
    {
      get
      {
        return this.layoutField;
      }
      set
      {
        this.layoutField = value;
      }
    }

    /// <summary>
    /// The matrix's left position.
    /// </summary>
    public long Left
    {
      get
      {
        return this.leftField;
      }
      set
      {
        this.leftField = value;
      }
    }

    /// <summary>
    /// A link to another item in the system.
    /// </summary>
    public string LinkTo
    {
      get
      {
        return this.linkToField;
      }
      set
      {
        this.linkToField = value;
      }
    }

    /// <summary>
    /// Indicates whether the item is right-justified.
    /// </summary>
    public bool RightJustified
    {
      get
      {
        return this.rightJustifiedField;
      }
      set
      {
        this.rightJustifiedField = value;
      }
    }

    /// <summary>
    /// Indicates the font style of text displayed by the item.
    /// </summary>
    public long TextStyle
    {
      get
      {
        return this.textStyleField;
      }
      set
      {
        this.textStyleField = value;
      }
    }

    /// <summary>
    /// The matrix's top position.
    /// </summary>
    public long Top
    {
      get
      {
        return this.topField;
      }
      set
      {
        this.topField = value;
      }
    }

    /// <summary>
    /// The end of the range of panes on which the item is visible (default is 0). 
    /// If the form has more than one pane, you can specify a range of panes on which the
    /// item is displayed. 
    /// </summary>
    public long ToPane
    {
      get
      {
        return this.toPaneField;
      }
      set
      {
        this.toPaneField = value;
      }
    }

    /// <summary>
    /// The matrix type.
    /// </summary>
    public long Type
    {
      get
      {
        return this.typeField;
      }
      set
      {
        this.typeField = value;
      }
    }

    /// <summary>
    /// The matrix unique id.
    /// </summary>
    public string UniqueID
    {
      get
      {
        return this.uniqueIDField;
      }
      set
      {
        this.uniqueIDField = value;
      }
    }

    /// <summary>
    /// Indicates whether the matrix is visible.
    /// </summary>
    public bool Visible
    {
      get
      {
        return this.visibleField;
      }
      set
      {
        this.visibleField = value;
      }
    }

    /// <summary>
    /// The matrix width.
    /// </summary>
    public long Width
    {
      get
      {
        return this.widthField;
      }
      set
      {
        this.widthField = value;
      }
    }

    /// <summary>
    /// Information about the matrix columns.
    /// </summary>
    [System.Xml.Serialization.XmlArrayItemAttribute("ColumnInfo", IsNullable = false)]
    public MatrixColumnInfo[] ColumnsInfo
    {
      get
      {
        return this.columnsInfoField;
      }
      set
      {
        this.columnsInfoField = value;
      }
    }

    /// <summary>
    /// Datas of the matrix per row.
    /// <example>
    /// b1Matrix.Rows[0].Columns[3].Value
    /// </example>
    /// </summary>
    [System.Xml.Serialization.XmlArrayItemAttribute("Row", IsNullable = false)]
    public MatrixRow[] Rows
    {
      get
      {
        return this.rowsField;
      }
      set
      {
        this.rowsField = value;
      }
    }
  }

  /// <summary>
  /// Information about the matrix columns.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  public partial class MatrixColumnInfo
  {

    private bool affectsFormModeField;

    private long backColorField;

    private string chooseFromListAliasField;

    private string chooseFromListUIDField;

    private MatrixColumnInfoDataBind dataBindField;

    private string descriptionField;

    private bool displayDescField;

    private bool editableField;

    private long fontSizeField;

    private long foreColorField;

    private bool rightJustifiedField;

    private long textStyleField;

    private string titleField;

    private long typeField;

    private string uniqueIDField;

    private MatrixColumnInfoValidValue[] validValuesField;

    private string valOFFField;

    private string valONField;

    private bool visibleField;

    private long widthField;

    /// <summary>
    /// Indicates whether this item triggers a change in the form mode.
    /// </summary>
    public bool AffectsFormMode
    {
      get
      {
        return this.affectsFormModeField;
      }
      set
      {
        this.affectsFormModeField = value;
      }
    }

    /// <summary>
    /// The background color of text or graphics in an item.
    /// </summary>
    public long BackColor
    {
      get
      {
        return this.backColorField;
      }
      set
      {
        this.backColorField = value;
      }
    }

    /// <summary>
    /// The database field by which to filter a ChooseFromList attached to the column.
    /// </summary>
    public string ChooseFromListAlias
    {
      get
      {
        return this.chooseFromListAliasField;
      }
      set
      {
        this.chooseFromListAliasField = value;
      }
    }

    /// <summary>
    /// The unique ID of the ChooseFromList object to attach with this column.
    /// </summary>
    public string ChooseFromListUID
    {
      get
      {
        return this.chooseFromListUIDField;
      }
      set
      {
        this.chooseFromListUIDField = value;
      }
    }

    /// <summary>
    /// The DataBind object tied to this column.
    /// </summary>
    public MatrixColumnInfoDataBind DataBind
    {
      get
      {
        return this.dataBindField;
      }
      set
      {
        this.dataBindField = value;
      }
    }

    /// <summary>
    /// The item description in the SAP Business One status bar.
    /// </summary>
    public string Description
    {
      get
      {
        return this.descriptionField;
      }
      set
      {
        this.descriptionField = value;
      }
    }

    /// <summary>
    /// Indicates whether to show the description of valid values. 
    /// True: Displays the description of the valid value.
    /// False: Displays the valid value.
    /// </summary>
    public bool DisplayDesc
    {
      get
      {
        return this.displayDescField;
      }
      set
      {
        this.displayDescField = value;
      }
    }

    /// <summary>
    /// Indicates whether the column is editable.
    /// </summary>
    public bool Editable
    {
      get
      {
        return this.editableField;
      }
      set
      {
        this.editableField = value;
      }
    }

    /// <summary>
    /// The font size for EditText, ComboBox, and StaticText items.
    /// </summary>
    public long FontSize
    {
      get
      {
        return this.fontSizeField;
      }
      set
      {
        this.fontSizeField = value;
      }
    }

    /// <summary>
    /// The foreground color of text or graphics in an item.
    /// </summary>
    public long ForeColor
    {
      get
      {
        return this.foreColorField;
      }
      set
      {
        this.foreColorField = value;
      }
    }

    /// <summary>
    /// Indicates whether the item is right-justified.
    /// </summary>
    public bool RightJustified
    {
      get
      {
        return this.rightJustifiedField;
      }
      set
      {
        this.rightJustifiedField = value;
      }
    }

    /// <summary>
    /// Indicates the font style of text displayed by the item.
    /// </summary>
    public long TextStyle
    {
      get
      {
        return this.textStyleField;
      }
      set
      {
        this.textStyleField = value;
      }
    }

    /// <summary>
    /// The title of the column. 
    /// Deprecated in UI API 2004.
    /// The property is supported in the next two releases for backward compatibility. Use TitleObject instead.
    /// </summary>
    public string Title
    {
      get
      {
        return this.titleField;
      }
      set
      {
        this.titleField = value;
      }
    }

    /// <summary>
    /// The column type.
    /// </summary>
    public long Type
    {
      get
      {
        return this.typeField;
      }
      set
      {
        this.typeField = value;
      }
    }

    /// <summary>
    /// The column unique id.
    /// </summary>
    public string UniqueID
    {
      get
      {
        return this.uniqueIDField;
      }
      set
      {
        this.uniqueIDField = value;
      }
    }

    /// <summary>
    /// The valid values for this column. 
    /// </summary>
    [System.Xml.Serialization.XmlArrayItemAttribute("ValidValue", IsNullable = false)]
    public MatrixColumnInfoValidValue[] ValidValues
    {
      get
      {
        return this.validValuesField;
      }
      set
      {
        this.validValuesField = value;
      }
    }

    /// <summary>
    /// The data source value when the item is selected. 
    /// Setting the data source with this value selects the checkbox. If the checkbox is selected by the user, the data source is set with this value.
    /// </summary>
    public string ValOFF
    {
      get
      {
        return this.valOFFField;
      }
      set
      {
        this.valOFFField = value;
      }
    }

    /// <summary>
    /// The data source value when the item is not selected. 
    /// Setting the data source with this value deselects the checkbox. If the checkbox is deselected by the user, the data source is set with this value.
    /// </summary>
    public string ValON
    {
      get
      {
        return this.valONField;
      }
      set
      {
        this.valONField = value;
      }
    }

    /// <summary>
    /// Indicates whether the column is visible.
    /// </summary>
    public bool Visible
    {
      get
      {
        return this.visibleField;
      }
      set
      {
        this.visibleField = value;
      }
    }

    /// <summary>
    /// The column width.
    /// </summary>
    public long Width
    {
      get
      {
        return this.widthField;
      }
      set
      {
        this.widthField = value;
      }
    }
  }

  /// <summary>
  /// Represents data that is bounded to a matrix column.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  public partial class MatrixColumnInfoDataBind
  {

    private string aliasField;

    private bool dataBoundField;

    private string tableNameField;

    /// <summary>
    /// Returns the alias name of the database field that is connected to the column.
    /// </summary>
    public string Alias
    {
      get
      {
        return this.aliasField;
      }
      set
      {
        this.aliasField = value;
      }
    }

    /// <summary>
    /// Indicates whether the column is currently bound to a data source or data table.
    /// </summary>
    public bool DataBound
    {
      get
      {
        return this.dataBoundField;
      }
      set
      {
        this.dataBoundField = value;
      }
    }

    /// <summary>
    /// The name of the database table to which the column is bound.
    /// </summary>
    public string TableName
    {
      get
      {
        return this.tableNameField;
      }
      set
      {
        this.tableNameField = value;
      }
    }
  }

  /// <summary>
  /// Represents the valid values for a column. 
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  public partial class MatrixColumnInfoValidValue
  {

    private string descriptionField;

    private string valueField;

    /// <summary>
    /// Description of the valid value.
    /// </summary>
    public string Description
    {
      get
      {
        return this.descriptionField;
      }
      set
      {
        this.descriptionField = value;
      }
    }

    /// <summary>
    /// Valid value.
    /// </summary>
    public string Value
    {
      get
      {
        return this.valueField;
      }
      set
      {
        this.valueField = value;
      }
    }
  }

  /// <summary>
  /// Datas of a specifc matrix row.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  public partial class MatrixRow
  {

    private bool visibleField;

    private MatrixRowColumn[] columnsField;

    /// <summary>
    /// Indicates whether the field is visible.
    /// </summary>
    public bool Visible
    {
      get
      {
        return this.visibleField;
      }
      set
      {
        this.visibleField = value;
      }
    }

    /// <summary>
    /// Datas per column.
    /// </summary>
    [System.Xml.Serialization.XmlArrayItemAttribute("Column", IsNullable = false)]
    public MatrixRowColumn[] Columns
    {
      get
      {
        return this.columnsField;
      }
      set
      {
        this.columnsField = value;
      }
    }
  }

  /// <summary>
  /// Specific column datas inside a row.
  /// </summary>
  [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
  [System.SerializableAttribute()]
  [System.Diagnostics.DebuggerStepThroughAttribute()]
  [System.ComponentModel.DesignerCategoryAttribute("code")]
  [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
  public partial class MatrixRowColumn
  {

    private string idField;

    private string valueField;

    /// <summary>
    /// Column id.
    /// </summary>
    public string ID
    {
      get
      {
        return this.idField;
      }
      set
      {
        this.idField = value;
      }
    }

   /// <summary>
   /// Column value.
   /// </summary>
    public string Value
    {
      get
      {
        return this.valueField;
      }
      set
      {
        this.valueField = value;
      }
    }
  }
}