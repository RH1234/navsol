//****************************************************************************
//
//  File:      B1XmlFormMenu.cs
//
//  Copyright (c) SAP 
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
//****************************************************************************
using System;
using System.Collections;
using System.Xml;
using SAPbouiCOM;

namespace B1WizardBase
{
  /// <summary>
  /// Base class for the events management on a menu entity who 
  /// Loads an Xml form on the menu event.
  /// </summary>
  /// <remarks>
  /// In your project you will have a class inheriting from B1XmlFormMenu
  /// for each menu loading an xml form you want to handle one/several events.
  /// </remarks>
  public abstract class B1XmlFormMenu : B1Menu
  {
    static private int		counter = 0;
    /// <summary>
    /// Form unique ID tag.
    /// </summary>
    static private string	formUID;
    /// <summary>
    /// Path of the form unique ID tag inside a xml file representing a form.
    /// </summary>
    static private string	UIDPath = "Application/forms/action/form/@uid";
    private XmlDocument		xmlDoc;

    //////////////////////////////////////////////////////////////////////////////////
		
    /// <summary>
    /// Default constructor.
    /// </summary>
    protected B1XmlFormMenu()
    {
    }

    /// <summary>
    /// Loads the xml string defining a form.
    /// </summary>
    /// <param name="xmlFile">Xml file name defining a form.</param>
    protected void LoadXml(string xmlFile)
    {
      xmlDoc = new XmlDocument();

      if (!System.IO.File.Exists(xmlFile))
      {
        xmlFile = xmlFile.Insert(0, "..\\");
      }

      if (System.IO.File.Exists(xmlFile))
      {
        xmlDoc.Load(xmlFile);
        formUID = xmlDoc.SelectSingleNode(UIDPath).Value;
      }
      else
      {
        B1Connections.theAppl.MessageBox("ERROR: File " + xmlFile + " not found", -1, "", "", "");
      }
    }

    /// <summary>
    /// Opens and displays the Xml form previously loaded into the B1 application.
    /// </summary>
    protected void LoadForm()
    {
      if (xmlDoc.HasChildNodes)
      {
        xmlDoc.SelectSingleNode(UIDPath).Value = formUID + counter++;
        string xmlStr = xmlDoc.DocumentElement.OuterXml;
        B1Connections.theAppl.LoadBatchActions(ref xmlStr);
        Form oForm = B1Connections.theAppl.Forms.ActiveForm;
        try 
        {
          UserDataSource oUDS = oForm.DataSources.UserDataSources.Item("FolderDS");
          if (oUDS != null)
            oUDS.Value = "1";
        }
        catch (Exception) {}
      }
      else
      {
        B1Connections.theAppl.MessageBox("ERROR: XML File containing the form not found", -1, "", "", "");
      }
    }
  }
}
