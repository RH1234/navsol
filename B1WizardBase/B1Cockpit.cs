//****************************************************************************
//
//  File:      B1Cockpit.cs
//
//  Copyright (c) SAP 
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
//****************************************************************************
using System;
using System.CodeDom;
using System.Collections;
using SAPbobsCOM;

namespace B1WizardBase
{
  /// <summary>
  /// Manages the B1 SDK service CockpitService.
  /// </summary>
  /// <remarks>
  /// This class will be used by the class managing the Cockpits (class inheriting 
  /// from B1Db base class).
  /// </remarks>
  public class B1Cockpit 
  {
    /// <summary>
    /// String specifying the cockpit name.
    /// </summary>
    public string Name;
    /// <summary>
    /// String specifying the cockpit description.
    /// </summary>
    public string Description;
    /// <summary>
    /// String specifying the cockpit manufacturer.
    /// </summary>
    public string Manufacturer;
    /// <summary>
    /// String specifying the cockpit Unique ID.
    /// </summary>
    public string UID;
    /// <summary>
    /// Template and user cockpits have the same DI Code, equivalent to CockpitTypeID in UI API 
    /// </summary>
    public string TypeID;
    /// <summary>
    /// If UserSignature = -1 then Template
    /// </summary>
    public bool isTemplate;
    /// <summary>
    /// String specifying the cockpit publisher.
    /// </summary>
    public string Publisher;

    /////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Simple Constructor, to be used in order to add a new cockpit.
    /// </summary>
    public B1Cockpit(
      string name,
      string description,
      string manufacturer) : this(name, description, manufacturer, "", "", "", true)
    {}

    /// <summary>
    /// Complete Constructor with all properties.
    /// </summary>
    public B1Cockpit(
      string name,
      string description,
      string manufacturer,
      string uid,
      string typeID,
      string publisher,
      bool isTemplate)
    {
      this.Name = name;
      this.Description = description;
      this.Manufacturer = manufacturer;
      this.UID = uid;
      this.TypeID = typeID;
      this.Publisher = publisher;
      this.isTemplate = isTemplate;
    }

    /// <summary>
    /// Complete cockpit built from a DI API cockpit.
    /// </summary>
    /// <param name="ckpt"></param>
    public B1Cockpit(SAPbobsCOM.Cockpit ckpt) 
      : this(ckpt.Name, ckpt.Description, ckpt.Manufacturer, ckpt.AbsEntry.ToString(), ckpt.Code.ToString(), ckpt.Publisher, 
        ((ckpt.UserSignature == -1)? true : false))
    { }

		/// <summary>
		/// Obtain cockpit from cockpit Name.
		/// </summary>
		/// <param name="company">SAPbobsCOM.Company we are connected to.</param>
		/// <param name="ckptName">Cockpit Name</param>
		/// <returns></returns>
		public static B1Cockpit GetCockpit(Company company, string ckptName)
		{
			SAPbobsCOM.CockpitsService ckptService = null;
			SAPbobsCOM.CockpitParams ckptGetParams = null;
			SAPbobsCOM.CockpitsParams ckptsParams = null;
			SAPbobsCOM.Cockpit ckptGet = null;
			B1Cockpit cockpit = null;

			try
			{
				ckptService = (SAPbobsCOM.CockpitsService)company.GetCompanyService().GetBusinessService(ServiceTypes.CockpitsService);

				ckptsParams = ckptService.GetUserCockpitList();
				if (ckptsParams.Count > 0)
				{
					foreach (SAPbobsCOM.CockpitParams cParams in ckptsParams)
					{
						ckptGet = ckptService.GetCockpit(cParams);
						if (ckptGet.Name == ckptName)
						{
							cockpit = new B1Cockpit(ckptGet);
							break;
						}
					}
				}

				return cockpit;
			}
			catch (Exception ex)
			{
				return null;
			}
			finally
			{
				if (ckptGetParams != null)
				{
					System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptGetParams);
					ckptGetParams = null;
				}
				if (ckptGet != null)
				{
					System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptGet);
					ckptGet = null;
				}
				if (ckptService != null)
				{
					System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptService);
					ckptService = null;
				}
			}
			return cockpit;
		}

		/// <summary>
		/// Adds the cockpit into the current company Database.
		/// </summary>
		/// <param name="uiApp">Curren UI API application we are connected to</param>
		/// <param name="company">Current DI API Company connection</param>
		/// <returns>0 if correctly added, 1 if already existing</returns>
    public int Add(SAPbouiCOM.Application uiApp, Company company)
    {
      SAPbobsCOM.CockpitsService ckptService = null;
      SAPbobsCOM.Cockpit ckptAdd = null;
      SAPbobsCOM.CockpitParams ckptParams = null;
      SAPbobsCOM.CockpitsParams ckptsParams = null;
      try
      {
				if (GetCockpit(company, Name) == null)
        {
					ckptService = (SAPbobsCOM.CockpitsService)company.GetCompanyService().GetBusinessService(ServiceTypes.CockpitsService);

          ckptAdd = (SAPbobsCOM.Cockpit)ckptService.GetDataInterface(CockpitsServiceDataInterfaces.csCockpit);
          ckptAdd.Name = Name;
          ckptAdd.Description = Description;
          ckptAdd.Manufacturer = Manufacturer;

          ckptParams = ckptService.AddCockpit(ckptAdd);
          UID = ckptParams.AbsEntry.ToString();
					ckptAdd = ckptService.GetCockpit(ckptParams);
					TypeID = ckptAdd.Code.ToString();

					// refresh UI cockpits list
					uiApp.Cockpits.Refresh();

          return 0;
        }
        else
          return 1;

      }
      catch (Exception ex)
      {
        throw ex;
      }
      finally
      {
        if (ckptAdd != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptAdd);
          ckptAdd = null;
        }
        if (ckptsParams != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptsParams);
          ckptsParams = null;
        }
        if (ckptService != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptService);
          ckptService = null;
        }
      }
    }

    /// <summary>
    /// Deletes the cockpit object into the current company Database.
    /// </summary>
    /// <param name="company">SAPbobsCOM.Company we are connected to.</param>
    /// <returns>0 if deletion process went fine.</returns>
    public int Delete(Company company)
    {
      SAPbobsCOM.CockpitsService ckptService = null;
      SAPbobsCOM.CockpitParams ckptDelParams = null;
      try
      {
        ckptService = (SAPbobsCOM.CockpitsService)company.GetCompanyService().GetBusinessService(ServiceTypes.CockpitsService);

        ckptDelParams = (SAPbobsCOM.CockpitParams)ckptService.GetDataInterface(CockpitsServiceDataInterfaces.csCockpitParams);
        ckptDelParams.AbsEntry = Int32.Parse(UID);
        ckptService.DeleteCockpit(ckptDelParams);

        return 0;
      }
      catch (Exception ex)
      {
        throw ex;
      }
      finally
      {
        if (ckptDelParams != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptDelParams);
          ckptDelParams = null;
        }
        if (ckptService != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptService);
          ckptService = null;
        }
      }
    }

		//private void getTypeID(Company company)
		//{
		//  SAPbobsCOM.CockpitsService ckptService = null;
		//  SAPbobsCOM.CockpitParams ckptParams = null;
		//  SAPbobsCOM.Cockpit ckpt = null;
		//  try
		//  {
		//    if (UID == -111)
		//      throw new Exception("Unknown UID");

		//    ckptService = (SAPbobsCOM.CockpitsService)company.GetCompanyService().GetBusinessService(ServiceTypes.CockpitsService);
		//    ckptParams = (SAPbobsCOM.CockpitParams)ckptService.GetDataInterface(CockpitsServiceDataInterfaces.csCockpitParams);

		//    ckptParams.AbsEntry = UID;
		//    ckpt = (SAPbobsCOM.Cockpit)ckptService.GetCockpit(ckptParams);

		//    TypeID = ckpt.Code.ToString();
		//  }
		//  catch (Exception ex)
		//  {
		//    throw ex;
		//  }
		//  finally
		//  {
		//    if (ckptParams != null)
		//    {
		//      System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptParams);
		//      ckptParams = null;
		//    }
		//    if (ckpt != null)
		//    {
		//      System.Runtime.InteropServices.Marshal.ReleaseComObject(ckpt);
		//      ckpt = null;
		//    }
		//    if (ckptService != null)
		//    {
		//      System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptService);
		//      ckptService = null;
		//    }
		//  }
		//}

    /// <summary>
    /// Get Widgets of a specific cockpit
    /// </summary>
 //   public ArrayList getWidgets(Company company, SAPbouiCOM.Application uiApp)
		//{
		//  if (TypeID == "")
		//  {
		//    // For new cockpits get TypeID from DI API
		//    getTypeID(company);

		//    // and refresh the cockpits list
		//    uiApp.Cockpits.Refresh();
		//  }

		//  // Select cockpit based on the TypeID
		//  uiApp.Cockpits.SwitchCockpit(TypeID);
		//  SAPbouiCOM.Cockpit uiCockpit = uiApp.Cockpits.CurrentCockpit;

		//  ArrayList widgets = new ArrayList();
		//  SAPbouiCOM.Widgets uiWidgets = uiCockpit.Widgets;
		//  int countW = uiWidgets.Count;
		//  if (countW > 0)
		//  {
		//    for (int i = 0; i < countW; ++i)
		//    {
		//      widgets.Add(new B1Widget(uiWidgets.Item(i).WidgetUID));
		//    }
		//  }
		//  return widgets;
		//}

    /// <summary>
    /// Updates a cockpit into the current company Database.
    /// </summary>
    /// <param name="company">SAPbobsCOM.Company we are connected to.</param>
    /// <returns>0 sucess, -1 or other code error.</returns>
    public int Update(Company company)
    {
      SAPbobsCOM.CockpitsService ckptService = null;
      SAPbobsCOM.CockpitParams ckptUpdateParams = null;
      SAPbobsCOM.Cockpit ckptUp = null;
      try
      {
        ckptService = (SAPbobsCOM.CockpitsService)company.GetCompanyService().GetBusinessService(ServiceTypes.CockpitsService);
        ckptUpdateParams = (SAPbobsCOM.CockpitParams)ckptService.GetDataInterface(CockpitsServiceDataInterfaces.csCockpitParams);
        
        ckptUpdateParams.AbsEntry = Int32.Parse(UID);
        ckptUp = (SAPbobsCOM.Cockpit)ckptService.GetCockpit(ckptUpdateParams);

        ckptUp.Name = Name;
        ckptUp.Description = Description;
        ckptUp.Manufacturer = Manufacturer;
        ckptService.UpdateCockpit(ckptUp);

        return 0;
      }
      catch (Exception ex)
      {
        return -1;
      }
      finally
      {
        if (ckptUpdateParams != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptUpdateParams);
          ckptUpdateParams = null;
        }
        if (ckptUp != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptUp);
          ckptUp = null;
        }
        if (ckptService != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptService);
          ckptService = null;
        }
      }
    }

    /// <summary>
    /// Move a widget.
		/// </summary>
    /// <param name="uiApp"></param>
    /// <param name="widget"></param>
    public void MoveWidget(SAPbouiCOM.Application uiApp, B1Widget widget)
    {
      // Select cockpit based on the TypeID
      uiApp.Cockpits.SwitchCockpit(TypeID);
      SAPbouiCOM.Cockpit uiCockpit = uiApp.Cockpits.CurrentCockpit;

      uiCockpit.MoveWidget(widget.widgetUID, widget.lRow, widget.lCol);
    }

    /// <summary>
    /// Publish a cockpit.
		/// A cockpit needs to be published for other users to see it.
    /// </summary>
    /// <param name="company"></param>
    public int Publish(Company company)
    {
			SAPbobsCOM.CockpitsService ckptService = null;
			SAPbobsCOM.CockpitParams ckptPubParams = null;
			SAPbobsCOM.Cockpit ckptPub = null;
			try
			{
				ckptService = (SAPbobsCOM.CockpitsService)company.GetCompanyService().GetBusinessService(ServiceTypes.CockpitsService);
				ckptPubParams = (SAPbobsCOM.CockpitParams)ckptService.GetDataInterface(CockpitsServiceDataInterfaces.csCockpitParams);

				ckptPubParams.AbsEntry = Int32.Parse(UID);
				ckptPub = (SAPbobsCOM.Cockpit)ckptService.GetCockpit(ckptPubParams);

				if (ckptPub.Publisher == null || ckptPub.Publisher.Length == 0)
					ckptService.PublishCockpit(ckptPub);

				return 0;
			}
			catch (Exception ex)
			{
				return -1;
			}
			finally
			{
				if (ckptPubParams != null)
				{
					System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptPubParams);
					ckptPubParams = null;
				}
				if (ckptPub != null)
				{
					System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptPub);
					ckptPub = null;
				}
				if (ckptService != null)
				{
					System.Runtime.InteropServices.Marshal.ReleaseComObject(ckptService);
					ckptService = null;
				}
			}
		}

    /// <summary>
    /// Generates the code to add a new Cockpit. 
		/// This code is added in your AddOn_Cockpit class inheriting from B1CockpitsManager.
    /// </summary>
    /// <returns>CodeExpression containing the Cockpit information.</returns>
    public CodeExpression GenerateCtor()
    {
      /*
       new B1Cockpit(name,description,manufacturer);
     */

      return new CodeObjectCreateExpression(
        "B1Cockpit",
        new CodeExpression[3] {
                                 new CodePrimitiveExpression( Name ),
                                 new CodePrimitiveExpression( Description ),
                                 new CodePrimitiveExpression( Manufacturer )});
    }

    /////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Determines whether the specified B1Cockpit is equal to the current B1Cockpit.     
    /// </summary>
    /// <param name="obj">B1Cockpit to compare.</param>
    /// <returns>true if both objects are equal.</returns>
    public override bool Equals(object obj)
    {
      if (obj is B1Cockpit)
      {
        B1Cockpit cockpit = obj as B1Cockpit;
        return cockpit.Name == Name;
      }

      return base.Equals(obj);
    }

    /// <summary>
    /// Serves as a hash function for a particular type, suitable 
    /// for use in hashing algorithms and data structures like a hash table. 
    /// </summary>
		/// <returns>A hash code for the current B1CockpitManager.</returns>
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

  }

  /// <summary>
  /// Represents a Widget
  /// </summary>
  public class B1Widget
  {
		/// <summary>
		/// Cockpit name where the widget is located
		/// </summary>
		public string cockpitName;

    /// <summary>
    /// Widget unique id
    /// </summary>
    public string widgetUID;

    /// <summary>
    /// Widget type
    /// </summary>
    public string widgetType;

    /// <summary>
    /// widget category
    /// </summary>
    public string category;

    /// <summary>
    /// widget width
    /// </summary>
    public int width;
    /// <summary>
    /// widget height
    /// </summary>
    public int height;

    /// <summary>
    /// widget row position
    /// </summary>
    public int lRow;
    /// <summary>
    /// widget column position
    /// </summary>
    public int lCol;

    /// <summary>
    /// widget image
    /// </summary>
    public string imagePath;

    /// <summary>
    /// key values stored by the widget
    /// </summary>
    public Hashtable keyValues = new Hashtable();

		/// <summary>
		/// Constructor without specifying the widgetUID
		/// </summary>
		/// <param name="cockpitName"></param>
		/// <param name="widgetType"></param>
		/// <param name="row"></param>
		/// <param name="col"></param>
    public B1Widget(string cockpitName, string widgetType, int row, int col)
			: this(cockpitName, widgetType, "NEW " + widgetType + "(" + row + ", " + col + ")" , "", -1, -1, "", row, col)
    { }

    /// <summary>
		/// Constructor specifying the widgetUID
    /// </summary>
    /// <param name="widgetUID"></param>
    /// <param name="lRow"></param>
    /// <param name="lCol"></param>
		public B1Widget(string cockpitName, string widgetType, string widgetUID, int row, int col)
			: this(cockpitName, "", widgetUID, "", -1, -1, "", row, col)
    { }

    /// <summary>
    /// Internal containing all infos
    /// </summary>
    /// <param name="widgetUID"></param>
    /// <param name="widgetType"></param>
    /// <param name="category"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="imagePath"></param>
    /// <param name="lRow"></param>
    /// <param name="lCol"></param>
		private B1Widget(string cockpitName, string widgetType, string widgetUID, string category, int width, int height, string imagePath, int row, int col)
    {
			this.cockpitName = cockpitName;
      this.widgetUID = widgetUID;
      this.widgetType = widgetType;
      this.category = category;
      this.width = width;
      this.height = height;
      this.imagePath = imagePath;
      this.lRow = row;
      this.lCol = col;
    }

		/// <summary>
		/// Creates a new widget instance of a specific widget type
		/// </summary>
		/// <param name="uiApp"></param>
		/// <param name="company"></param>
		public void CreateWidgetInstance(SAPbouiCOM.Application uiApp, SAPbobsCOM.Company company)
		{
			B1Cockpit cockpit = B1Cockpit.GetCockpit(company, cockpitName);
			if (cockpit == null)
			{
				throw new Exception("B1Widget.CreateWidgetInstance: Cockpit with name " + cockpitName + " not found");
			}

			CreateWidgetInstance(uiApp, company, cockpit);
		}

		/// <summary>
		/// Creates a new widget instance of a specific widget type in a specific cockpit
		/// </summary>
		/// <param name="uiApp"></param>
		/// <param name="company"></param>
		/// <param name="cockpit"></param>
		public void CreateWidgetInstance(SAPbouiCOM.Application uiApp, SAPbobsCOM.Company company, B1Cockpit cockpit)
		{
			SAPbouiCOM.Cockpit uiCockpit = uiApp.Cockpits.Item(cockpit.TypeID);

			// Set cockpit as current (otherwise CreateWidgetInstance will have no effect)
			uiApp.Cockpits.SwitchCockpit(cockpit.TypeID); 

			uiCockpit.CreateWidgetInstance(widgetType, lRow, lCol);
		}


    /// <summary>
    /// Set a value for a specific key inside a widget
    /// </summary>
    /// <param name="uiApp"></param>
    /// <param name="key"></param>
    /// <param name="value"></param>
    public void SetKeyValue(SAPbouiCOM.Application uiApp, string key, string value)
    {
      keyValues.Add(key, value);
    }

		/// <summary>
		/// Generates the code to add a new Widget. 
		/// <para>This code is added in your AddOn_Cockpit class inheriting from B1CockpitManager.</para>
		/// </summary>
		/// <returns>CodeExpression containing the Widget information.</returns>
		public CodeExpression GenerateCtor()
		{
			/*
			 new B1Widget(cockpitName, widgetType, row, col);
		 */

			return new CodeObjectCreateExpression(
				"B1Widget",
				new CodeExpression[4] {
                                 new CodePrimitiveExpression( cockpitName ),
                                 new CodePrimitiveExpression( widgetType ),
                                 new CodePrimitiveExpression( lRow ),
                                 new CodePrimitiveExpression( lCol )});
		}

		/// <summary>
		/// Determines whether the specified B1Widget is equal to the current B1Widget.     
		/// </summary>
		/// <param name="obj">B1Widget to compare.</param>
		/// <returns>true if both objects are equal.</returns>
		public override bool Equals(object obj)
		{
			if (obj is B1Widget)
			{
				B1Widget widget = obj as B1Widget;
				return (widget.cockpitName + widget.widgetUID) == (cockpitName + widgetUID); 
			}

			return base.Equals(obj);
		}

  }

  /// <summary>
  /// To mange the creation of new widget types
  /// </summary>
  public class B1WidgetType
  {
    /// <summary>
    /// Widget name
    /// </summary>
    public string widgetName;

    /// <summary>
    /// Widget type
    /// </summary>
    public string widgetType;

    /// <summary>
    /// Widget category it will be presented in the available widgets list
    /// </summarys
    public string categoryUID;

    /// <summary>
		/// Widget width
    /// </summary>
    public int width;
    /// <summary>
		/// Widget height
    /// </summary>
    public int height;

    /// <summary>
		/// Widget image (icon in the widgets tree)
    /// </summary>
    public string imagePath;

     /// <summary>
    /// Useful for Registering a new widget
    /// </summary>
    /// <param name="widgetName"></param>
    /// <param name="widgetType"></param>
    /// <param name="categoryUID"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="imagePath"></param>
		public B1WidgetType(string widgetName, string widgetType, string categoryUID, int width, int height, string imagePath) 
    {
      this.widgetName = widgetName;
      this.widgetType = widgetType;
      this.categoryUID = categoryUID;
      this.width = width;
      this.height = height;
      this.imagePath = imagePath;
    }

    /// <summary>
    /// Register a new widget type
    /// </summary>
    /// <param name="uiApp"></param>
    public void RegisterWidget(SAPbouiCOM.Application uiApp)
    {
       SAPbouiCOM.WidgetRegParams widgetRegParams = 
				 (SAPbouiCOM.WidgetRegParams)uiApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_WidgetRegParams);
       try
       {
         widgetRegParams.WidgetName = widgetName;
         widgetRegParams.WidgetType = widgetType;
         widgetRegParams.CategoryUID = categoryUID;
         widgetRegParams.Width = width;
         widgetRegParams.Height = height;
         widgetRegParams.ImagePath = imagePath;

         uiApp.Cockpits.RegisterWidget(widgetRegParams);
       }
       catch (Exception ex)
       {
         throw ex;
       }
       finally
       {
         if (widgetRegParams != null)
         {
           System.Runtime.InteropServices.Marshal.ReleaseComObject(widgetRegParams);
           widgetRegParams = null;
         }
       }
    }

		/// <summary>
		/// Generates the code to add a new Widget Type. 
		/// <para>This code is added in your AddOn_Cockpit class inheriting from B1CockpitsManager.</para>
		/// </summary>
		/// <returns>CodeExpression containing the Widget information.</returns>
		public CodeExpression GenerateCtor()
		{
			/*
			 new B1WidgetType(widgetName, widgetType, categoryUID, width, height, imagePath);
		 */

			return new CodeObjectCreateExpression(
				"B1WidgetType",
				new CodeExpression[6] {
                                 new CodePrimitiveExpression( widgetName ),
                                 new CodePrimitiveExpression( widgetType ),
                                 new CodePrimitiveExpression( categoryUID ),
                                 new CodePrimitiveExpression( width ),
                                 new CodePrimitiveExpression( height ),
                                 new CodePrimitiveExpression( imagePath )});
		}

		/// <summary>
		/// Determines whether the specified B1WidgetType is equal to the current B1WidgetType.     
		/// </summary>
		/// <param name="obj">B1Widget to compare.</param>
		/// <returns>true if both objects are equal.</returns>
		public override bool Equals(object obj)
		{
			if (obj is B1WidgetType)
			{
				B1WidgetType widget = obj as B1WidgetType;
				return (widget.widgetType == widgetType);
			}

			return base.Equals(obj);
		}

  }

	/// <summary>
	/// Used for events handling
	/// </summary>
	public abstract class B1RegWidget : B1Action
	{
		/// <summary>
    /// String specifying the widget type.
    /// </summary>
    protected string WidgetType ="";

    /////////////////////////////////////////////////////////////////////////////////
		
    /// <summary>
    /// Empty Constructor.
    /// </summary>
		protected B1RegWidget()
    {
    }

    /// <summary>
    /// Returns the key identifying the widget entity for the events management.
    /// </summary>
    /// <param name="before">Boolean value specifying whether the action
    /// wants to handle the before or the after notification.</param>
    /// <returns>String identifying the action key.</returns>
    public override sealed string GetKey(bool before)
    {
			return EventTables.GetActionKey(WidgetType, "", before);
    }
	}

}

