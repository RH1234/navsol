//****************************************************************************
//
//  File:      B1Info.cs
//
//  Copyright (c) SAP 
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
//****************************************************************************
using System;
using System.Xml;
using System.Threading;
using SAPbouiCOM;

namespace B1WizardBase
{
  /// <summary>
  /// Displays to the current user a message window inside the SAP Business One application.
  /// </summary>
  public class B1Info
  {
    /// <summary>
    /// Message to show to the user.
    /// </summary>
    private string		msg;
    /// <summary>
    /// Current Application we are connected to.
    /// </summary>
    private Application theAppl;

    /////////////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// Shows a MessageBox in the Business One application.
    /// </summary>
    private void displayMsg()
    {
      theAppl.MessageBox(msg,-1,"","","");
    }

    /// <summary>
    /// Displays to the user a MessageBox inside the SAP Business One application.
    /// </summary>
    /// <param name="theAppl">SAPbouiCOM.Application we are connected to.</param>
    /// <param name="msg">Message to show to the user.</param>
    public B1Info(Application theAppl,string msg)
    {
      this.msg = msg;
      this.theAppl = theAppl;
      Thread t = new Thread(new ThreadStart(this.displayMsg));
      t.Start();
    }
  }

  /// <summary>
  /// Searchs for the results of the last batch action asked and
  /// shows them to the user on a MessageBox in the SAP Business One application.
  /// </summary>
  public class B1BatchInfo
  {
    /// <summary>
    /// Searchs for the results of the last batch action asked and
    /// shows them to the user on a MessageBox in the SAP Business One application.
    /// </summary>
    /// <param name="theAppl">SAPbouiCOM.Application we are connected to.</param>
    public B1BatchInfo(Application theAppl)
    {
      string result = theAppl.GetLastBatchResults();
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(result);

      /*
      <result>
        <errors>
          <error code=ERRORCODE descr=ERRORDESCR>
          </error>
        </errors>
      </result>
      */

      string errorPath = "result/errors/error";
      foreach	(XmlNode errorNode in doc.SelectNodes(errorPath))
      {
        XmlElement errorElem = (XmlElement)errorNode;
        XmlAttribute errAttr = errorElem.Attributes[ "code" ];
        if	(errAttr != null)
        {
          XmlAttribute descAttr = errorElem.Attributes[ "descr" ];
          new B1Info( theAppl,"ERROR " + errAttr.Value + " : " + 
            ((descAttr != null)? descAttr.Value : ""));
        }
      }
    }
  }
}
