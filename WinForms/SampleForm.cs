﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;

namespace WinForms
{
    public partial class SampleForm : Form
    {
        public SampleForm()
        {
			// Initialize the form
            InitializeComponent();

            // Set all items on form to use the same theme
            ThemeResolutionService.ApplicationThemeName = "Office2010Blue";
        }

        private void radButtonElement1_Click(object sender, EventArgs e)
        {
/*   // RHH  TODO: implement this call
			Producer.ViewModels.F_GreenbookTask.Form_Load_Create();
*/
		}

        private void SampleForm_Load(object sender, EventArgs e)
        {
/*   // RHH  TODO: use B1 tasks
            // Prepare an SAP RecordSet
            Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            // Run the SQL query against the SQL database
            oRecordSet.DoQuery("SELECT * FROM [@NSC_GBTASK]");

            // Move to the first record
            oRecordSet.MoveFirst();

            // While we are not on the last record
            while (!oRecordSet.EoF)
            {
                try
                {
                    // Grab details from the current record
                    DateTime task_duedate = Convert.ToDateTime(oRecordSet.Fields.Item("U_DueDate").Value.ToString());
                    string task_name = oRecordSet.Fields.Item("U_TaskName").Value.ToString();
                    string task_details = oRecordSet.Fields.Item("U_Task").Value.ToString();

                    // Create a new RadScheduler Appointment
                    Appointment newAppintment = new Appointment(task_duedate, TimeSpan.FromMinutes(30), task_name, task_details);
                    newAppintment.StatusId = 2;
                    newAppintment.BackgroundId = 1;
                    //newAppintment.AllowDelete = false;
                    //newAppintment.AllowEdit = false;

                    // Add a RadScheduler appointment
                    radScheduler1.Appointments.Add(newAppintment);
                }
                catch (Exception ex)
                {

                }

                // Move to the next record.
                oRecordSet.MoveNext();
            }
*/
        }
    }
}
