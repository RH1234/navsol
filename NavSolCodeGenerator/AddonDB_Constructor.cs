﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NavSolCodeGenerator
{
    partial class AddOnDb
    {
        private DataTable m_dtAutoItem;
        private DataTable m_dtItemGroup;
        private DataTable m_dtAutoItemBOM;
        private DataTable m_dtWarehouse;
        private DataTable m_dtPhase;
        //private DataTable m_dtMicroverticals;
        private DataTable m_dtSMVMenuItem;
        public AddOnDb()
        {
            m_dtAutoItem = Table.Load("Resources/auto_item_mv.csv", true);
            m_dtItemGroup = Table.Load("Resources/item_groups_mv.csv", true);
            m_dtAutoItemBOM = Table.Load("Resources/auto_item_bom.csv", true);
            m_dtWarehouse = Table.Load("Resources/Warehouses.csv", true);
            m_dtPhase = Table.Load("Resources/phases_production.csv", true);
            //m_dtMicroverticals = Table.Load("Resources/microverticals.csv", true);
            m_dtSMVMenuItem = Table.Load("Resources/menuitem.csv", true);
        }
    }
}
