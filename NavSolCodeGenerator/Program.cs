﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
namespace NavSolCodeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            AddOnDb autoItem = new AddOnDb();
            string output = autoItem.TransformText();
            File.WriteAllText("out.cs", output);
            Console.WriteLine("Output succeeded");
            Console.ReadLine();
        }
    }
}
