﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Globalization;

namespace NavSolCodeGenerator
{
    public static class Table
    {

        public static DataTable Load(string filename, bool isFirstRowHeader, string format="")
        {
            string header = isFirstRowHeader ? "Yes" : "No";

            string sql = "SELECT * FROM [" + Path.GetFileName(filename) + "]";

            using (OleDbConnection connection = new OleDbConnection(
                      "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Path.GetDirectoryName(filename) +
                      ";Extended Properties=\"Text;HDR=" + header + (String.IsNullOrEmpty(format) ? "" : ";FMT=" + format) + "\""))
            using (OleDbCommand command = new OleDbCommand(sql, connection))
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
                DataTable dataTable = new DataTable();
                dataTable.Locale = CultureInfo.CurrentCulture;
                adapter.Fill(dataTable);
                return dataTable;
            }
        }
    }
}
