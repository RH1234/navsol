﻿using System;
using System.Reflection;
using System.Windows.Forms;
using SAPbobsCOM;

namespace NavSol
{
	public static class Globals
	{
		public static string SAP_PartnerCode	= "NSC";
		public static string pathToImg   = System.IO.Path.Combine(Application.StartupPath, @"ImageFiles\");		// readonly 
		public static string UserName	 = "";

		public static String ProdCode	 = Application.ProductName;
		public static String ProdName	 = ((AssemblyTitleAttribute)AssemblyTitleAttribute.GetCustomAttribute(Assembly.GetExecutingAssembly(), typeof(AssemblyTitleAttribute))).Title;
		public static String ProdVersion = Assembly.GetEntryAssembly().GetName().Version.ToString();
		public static String SAPVersion  = "";

		public static int	 DBVersion   = 119;

        public static bool CancelSetup   = false;

        public static int    BranchDflt  = -1;
        public static string BranchList  = "";

        public static bool	IsCannabis	 = true;
		public static bool	IsGrower	 = false;
        public static bool AddonInitialized { get; set; } = false;
		public static SAPbouiCOM.Application	oApp;
		public static Company					oCompany;
		public static CompanyService			oCompService;

        public static System.Windows.Threading.Dispatcher oSTAThreadDispatcher;
	}
}
