﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using Matrix = NavSol.CommonUI.Matrix;

namespace NavSol.Forms
{
	public class F_GreenbookTask : B1Event
    {
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "NSC_GBOOK_TASK";

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "BTN_Add":
					BTN_Add_Click(oForm);
					break;

				case "TAB_Notes":
					TAB_Notes_Click(oForm);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
		[B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
		public virtual void OnAfterChooseFromList(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "txt_asgnto":
					CFL_emp_ItemSelected(oForm, (IChooseFromListEvent)pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			try
			{
				pForm.Freeze(true);

				pForm.Mode = BoFormMode.fm_ADD_MODE;

				pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		public static Form Form_Load_Find(Int32? ID = null)
		{
			EditText task_id = null;
			Button	 btn_1	 = null;

			try
			{
				var pForm = FormCreate();
				pForm.Freeze(true);
				pForm.Mode = BoFormMode.fm_FIND_MODE;

				task_id = pForm.Items.Item("task_id").Specific;
				btn_1	= pForm.Items.Item("1").Specific;
				
				// Only allow task_id to be edited during "Find" mode.
				task_id.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_All), BoModeVisualBehavior.mvb_False);
				task_id.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_Find), BoModeVisualBehavior.mvb_True);

				// Enable UDO Form navigation buttons
				pForm.DataBrowser.BrowseBy = "task_id";

				// Set the ID of the task
				task_id.Value = ID.ToString();

				if (ID.HasValue) btn_1.Item.Click();	// Click the "Find" button

				// Select the first tab by default
				pForm.Items.Item("TAB_GenDet").Specific.Select();

				pForm.VisibleEx = true;
				return pForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(task_id);
				NSC_DI.UTIL.Misc.KillObject(btn_1);
				GC.Collect();
			}
		}

		public static Form Form_Load_Create()
		{
			Form pForm = null;

			try
			{
				pForm = FormCreate();
				pForm.Freeze(true);

				pForm.Mode	= BoFormMode.fm_ADD_MODE;

				//task_id.Value	= NSC_DI.UTIL.SQL.GetValue<string>("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tGBTask + "]", "1");
				pForm.Items.Item("txt_cdate").Specific.Value = DateTime.UtcNow.ToString("yyyyMMdd");
				pForm.Items.Item("txt_crby").Specific.Value = Globals.oCompany.UserName;

				pForm.Items.Item("TAB_GenDet").Specific.Select();	// Select the first tab by default
				pForm.Items.Item("TAB_Notes").Visible = false;		// Hide the notes tab during creation
				
				pForm.VisibleEx = true;
				return pForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
				GC.Collect();
			}
		}

	    private static void CFL_emp_ItemSelected(Form pForm, IChooseFromListEvent pChooseFromListEvent)
	    {
			SAPbouiCOM.ChooseFromList oChooseFromList = pForm.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

		    DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

		    try
		    {
			    string userID = oDataTable.GetValue(0, 0).ToString();
			    string usersFirstName = oDataTable.GetValue(2, 0).ToString();
			    string usersLastName = oDataTable.GetValue(1, 0).ToString();

			    // Set the userdatasource for the name textbox
			    pForm.DataSources.UserDataSources.Item("uds_asgnto").ValueEx = Convert.ToString(usersFirstName + " " + usersLastName);

			    // Find the textbox for the employee ID
			    pForm.Items.Item("txt_empid").Specific.Value = userID;
		    }
		    catch (Exception ex)
		    {

		    }
	    }

	    private static void BTN_Add_Click(Form pForm)
	    {
		    EditText task_id = null;
		    EditText TXT_NDet = null;

		    try
		    {
				// Validate text for note exists
			    TXT_NDet = pForm.Items.Item("TXT_NDet").Specific;

			    if (TXT_NDet.Value == "")
			    {
				    Globals.oApp.StatusBar.SetText("Please enter some note details.", BoMessageTime.bmt_Short);
				    return;
			    }

			    // Grab the taskID from the form UI
			    task_id = pForm.Items.Item("task_id").Specific;

			    // Attempt to create the Greenbook Task Note
				NSC_DI.SAP.GreenbookTaskNote.CreateNewGreenbookTaskNote(Globals.oApp.Company.UserName, task_id.Value, TXT_NDet.Value);
				Load_Matrix_TaskNotes(pForm);
			    TXT_NDet.Value = "";
			    Globals.oApp.StatusBar.SetText("Successfully added note to this task!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
		    }

		    catch (Exception ex)
		    {
			    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    pForm.Freeze(false);
				NSC_DI.UTIL.Misc.KillObject(task_id);
				NSC_DI.UTIL.Misc.KillObject(TXT_NDet);
			    GC.Collect();
		    }
	    }

	    private static void TAB_Notes_Click(Form pForm)
        {
            Load_Matrix_TaskNotes(pForm);
        }

		private static void Load_Matrix_TaskNotes(Form pForm)
        {
			EditText task_id = null;

			try
			{
            // Grab the ID textbox from the form UI
				task_id = pForm.Items.Item("task_id").Specific;

            // Load the matrix of tasks
				Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tGBTask, "MTX_TNote", new List<Matrix.MatrixColumn>() {
                        new Matrix.MatrixColumn(){ Caption="ID", ColumnWidth=40, ColumnName="Code", ItemType= BoFormItemTypes.it_EDIT}
                        ,new Matrix.MatrixColumn(){ Caption="Created By", ColumnWidth=80, ColumnName="U_CreatedBy", ItemType = BoFormItemTypes.it_EDIT}               
                        ,new Matrix.MatrixColumn(){ Caption="Created On", ColumnWidth=80, ColumnName="U_CreatedOn", ItemType = BoFormItemTypes.it_EDIT}  
                        ,new Matrix.MatrixColumn(){ Caption="Note", ColumnWidth=80, ColumnName="U_Note", ItemType = BoFormItemTypes.it_EDIT}  
                    }, "SELECT * FROM [@" + NSC_DI.Globals.tGBTaskNote + "] WHERE [U_TaskID] = '" + task_id.Value.ToString() + "'");
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(task_id);
				GC.Collect();
			}
		}

	}
}
