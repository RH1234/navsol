﻿using System;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Lab_Controls
{
	public class F_EffectInfo : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_EFFECT_INFO";
        // FOR OLD CODE
        public VERSCI.Forms _VirSci_Helper_Form;
        public SAPbouiCOM.Application _SBO_Application;
        public SAPbobsCOM.Company _SBO_Company;
        public SAPbouiCOM.Form _SBO_Form;
        public string FormUID { get; set; }

        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			if (pVal.ItemUID == "1" && oForm.Items.Item("1").Specific.Caption == "Add") BubbleEvent = BTN_Add_Click(oForm);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "1":
					if (oForm.Items.Item("1").Specific.Caption == "OK") BTN_OK_Click(oForm);
					if (oForm.Items.Item("1").Specific.Caption == "Add") FormSetup(oForm); ;	// RHH ???
					break;

				case "BTN_CADD":
					BTN_ADD_COMPOUND_Click(oForm);
					break;

				case "BTN_CRMVE":
					BTN_REMOVE_COMPOUND_Click(oForm);
					break;

				case "CMD_DADD":
					BTN_ADD_DISEASE_Click(oForm);
					break;

				case "BTN_DRMVE":
					BTN_REMOVE_DISEASE_Click(oForm);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "MTX_COMP":
					MTX_COMPOUND_MATRIX_LINK_PRESSED(oForm, pVal);
					break;
				case "MTX_DISEA":
					MTX_DISEASE_MATRIX_LINK_PRESSED(oForm, pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			PictureBox IMG_Main = null;
			EditText TextBox_ID = null;
			try
			{
				pForm.Freeze(true);

				pForm.Mode = BoFormMode.fm_ADD_MODE;

				//// Set the Logo image
				//IMG_Main = pForm.Items.Item("IMG_Main").Specific;
				//IMG_Main.Picture = Globals.pathToImg + @"\Icons\" + "air-quality-icon.bmp";

				pForm.Items.Item("FLD_GENRL").Specific.Select();	// Select the first tab by default

				TextBox_ID = pForm.Items.Item("TXT_ID").Specific;
				TextBox_ID.Value =
					NSC_DI.UTIL.SQL.GetValue<string>("SELECT MAX(CONVERT(int,[Code])) + 1 FROM [@" + NSC_DI.Globals.tEffect + "]", "1");

				// Only allow task_id to be edited during "Find" mode.
				TextBox_ID.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_All), BoModeVisualBehavior.mvb_False);
				TextBox_ID.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_Find), BoModeVisualBehavior.mvb_True);

				CommonUI.LabControls.Load_Compound_Matrix(pForm, TextBox_ID);
				CommonUI.LabControls.Load_Disease_Matrix(pForm, TextBox_ID);
				CommonUI.LabControls.LoadCBO(pForm, "CMB_DISEA", NSC_DI.Globals.tDisease);
				CommonUI.LabControls.LoadCBO(pForm, "CMB_COMP", NSC_DI.Globals.tCompounds);
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.VisibleEx = true;
				pForm.Freeze(false);

				NSC_DI.UTIL.Misc.KillObject(IMG_Main);
				NSC_DI.UTIL.Misc.KillObject(TextBox_ID);
				GC.Collect();
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Form oForm = null;
			Item oItm = null;
			try
			{
				oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return null;
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

        public static void Form_Load()
        {
            string EffectID = "Hello";
        }

        public static void Form_Load_Find(string EffectID)
		{
			EditText TextBox_ID = null;

			try
			{
				var oForm = FormCreate();
				oForm.Freeze(true);
				oForm.Mode = BoFormMode.fm_FIND_MODE;

				// Enable UDO navigation buttons
				oForm.DataBrowser.BrowseBy = "TXT_ID";

				// Set the textbox for the ID 
				TextBox_ID = oForm.Items.Item("TXT_ID").Specific as EditText;
				TextBox_ID.Value = EffectID;

				oForm.Items.Item("1").Click();

				// Select the first tab by default
				oForm.Items.Item("FLD_GENRL").Specific.Select();

				CommonUI.LabControls.Load_Compound_Matrix(oForm, TextBox_ID);
				CommonUI.LabControls.Load_Disease_Matrix(oForm, TextBox_ID);

				oForm.Freeze(false);
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(TextBox_ID);
				GC.Collect();
			}
		}

		private static void BTN_OK_Click(Form pForm)
		{

		}

		private static bool BTN_Add_Click(Form pForm)
		{
			EditText txtId = null;
			EditText txtName = null;

			try
			{
				if (EffectValidate(pForm) == false) return false;

				txtId = pForm.Items.Item("TXT_ID").Specific;
				txtName = pForm.Items.Item("TXT_NAME").Specific;

				if (string.IsNullOrEmpty(txtName.Value))
				{
					Globals.oApp.StatusBar.SetText("Please enter a name for the Effect!");
					txtName.Active = true;
					return true;
				}

				// Automatically generate items from the newly created strain.
				NSC_DI.SAP.Effect.CreateEffect(txtId.Value, txtName.Value);

				Globals.oApp.StatusBar.SetText("Success!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

				txtName.Value = "";
				return true;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return false;
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(txtId);
				NSC_DI.UTIL.Misc.KillObject(txtName);
				GC.Collect();
			}
		}

		private static bool EffectValidate(Form pForm)
		{
			EditText TextBox_Name = null;

			try
			{
				// Check for existing Compound
				TextBox_Name = pForm.Items.Item("TXT_NAME").Specific;

				string SQL_Query_FindCompoundByName = "SELECT [Code] FROM [@" + NSC_DI.Globals.tEffect + "] WHERE [U_Name] = '" + TextBox_Name.Value + "'";

				SAPbobsCOM.Fields ResultsFromSQL = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(SQL_Query_FindCompoundByName);

				if (ResultsFromSQL == null || ResultsFromSQL.Count <= 0) return true;

				Globals.oApp.StatusBar.SetText("There is already a Effect named '" + TextBox_Name.Value + "'");
				return false;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
				NSC_DI.UTIL.Misc.KillObject(TextBox_Name);
				GC.Collect();
			}
		}

		private static void BTN_ADD_COMPOUND_Click(Form pForm)
		{
			try
			{
				CommonUI.LabControls.ADD_COMPOUND_EFFECT(pForm);

				CommonUI.LabControls.Load_Compound_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
				CommonUI.LabControls.Load_Disease_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
			}
			catch (System.ComponentModel.WarningException)
			{
				// message was already displayed
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private static void BTN_REMOVE_COMPOUND_Click(Form pForm)
		{
			try
			{
				CommonUI.LabControls.REMOVE_COMPOUND(pForm);

				CommonUI.LabControls.Load_Compound_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
				CommonUI.LabControls.Load_Disease_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
			}
			catch (System.ComponentModel.WarningException)
			{
				// message was already displayed
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private static void MTX_COMPOUND_MATRIX_LINK_PRESSED(Form pForm, ItemEvent SAP_UI_ItemEvent)
		{
			try
			{
				int SelectedRowID = SAP_UI_ItemEvent.Row;

				if (SelectedRowID > 0)
				{
					// Which column stores the ID
					int ColumnIDForIDOfItemSelected = 0;

					// Get the ID of the note selected
					string ItemSelected = CommonUI.Forms.GetField<string>(pForm, "MTX_COMP", SelectedRowID, ColumnIDForIDOfItemSelected);

					// Open the plant manager for the selected plant
					Form_Load_Find(ItemSelected);
				}
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private static void BTN_ADD_DISEASE_Click(Form pForm)
		{
			try
			{
				CommonUI.LabControls.ADD_DISEASE(pForm);

				CommonUI.LabControls.Load_Compound_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
				CommonUI.LabControls.Load_Disease_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
			}
			catch (System.ComponentModel.WarningException)
			{
				// message was already displayed
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private static void BTN_REMOVE_DISEASE_Click(Form pForm)
		{
			try
			{
				CommonUI.LabControls.REMOVE_DISEASE(pForm);

				CommonUI.LabControls.Load_Compound_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
				CommonUI.LabControls.Load_Disease_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
			}
			catch (System.ComponentModel.WarningException)
			{
				// message was already displayed
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private static void MTX_DISEASE_MATRIX_LINK_PRESSED(Form pForm, ItemEvent SAP_UI_ItemEvent)
		{
			try
			{
				int SelectedRowID = SAP_UI_ItemEvent.Row;

				if (SelectedRowID > 0)
				{
					// Which column stores the ID
					int ColumnIDForIDOfItemSelected = 0;

					// Get the ID of the note selected
					string ItemSelected = CommonUI.Forms.GetField<string>(pForm, "MTX_DISEA", SelectedRowID, ColumnIDForIDOfItemSelected);

					// Open the plant manager for the selected plant
					Form_Load_Find(ItemSelected);
				}
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}
	}
}