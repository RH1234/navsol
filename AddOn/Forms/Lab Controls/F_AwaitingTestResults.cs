﻿using System;
using System.Collections.Generic;
using SAPbouiCOM;
using Matrix = NavSol.CommonUI.Matrix;

namespace NavSol.Forms.Lab_Controls
{
    class F_AwaitingTestResults 
    {
/*	// COMPLIANCE
        private const string BATCH_PREFIX = "PK";

        private Globals.ItemGroupTypes itemGroupType = Globals.ItemGroupTypes.PackagedCannabis;

        override public void ItemEventHandler(string FormUID, ref ItemEvent pVal, out bool BubbleEvent)
        {
            // Prepare the private form field
            _SBO_Form = Forms.GetFormFromUID(cFormID);

            // Assume we want to complete the bubble event unless otherwise noted
            BubbleEvent = true;

            switch (pVal.BeforeAction)
            {
                case false:

                    switch (pVal.EventType)
                    {
                        case BoEventTypes.et_ITEM_PRESSED: // An item has been pressed
                            switch (pVal.ItemUID)
                            {
                                case "btnCheck":
                                    CheckSelectedSamples();
                                    break;

                                case "BTN_LOOKUP":
                                    BTN_LOOKUP_ITEM_PRESSED();
                                    break;
                            }
                            break;

                        case BoEventTypes.et_MATRIX_LINK_PRESSED:
                            switch (pVal.ItemUID)
                            {
                                case "mtxMain":
      
                                    break;
                            }
                            break;
                    }

                    break;
            }
        }

        #region Initial Form Load

        public AwaitingTestResults(Application SAPBusinessOne_Application, Company SAPBusinessOne_Company)
        {
            // this.FormUID = "VSC_WAIT_RESULT";
            this.MenuText = "Awaiting Test Results";
            this.SubMenu = SubMenus.MENU_TEST;
            this.MenuPosition = 3;

            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
            Forms = new Forms(SAPBusinessOne_Application: Globals.oApp);
        }

        override public void Form_Load()
        {
            // Load form from XML
            Forms.ShowFormFromXMLFile(FormXMLLocation);

            // If the form displayed properly, or is already in view
            if (Forms.WasSuccessful || Forms.LastErrorMessage.Contains("[66000-11]"))
            {
                // Set our private form field
                _SBO_Form = Forms.GetFormFromUID(cFormID);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                // Set the main image
                //((SAPbouiCOM.PictureBox)Helpers.Forms.GetControlFromForm(Helpers.Forms.FormControlTypes.PictureBox, "IMG_MAIN", FormToSearchIn: _SBO_Form))
                //    .Picture = Globals.pathToImg + @"\Icons\" + iconImage;

                Load_Matrix();

                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                // Show the form
                _SBO_Form.VisibleEx = true;
            }
        }

        private enum MatrixColumns {BatchNumber, ItemName, DateCreated, OnHand, Warehouse, SampleBatchId, StateSampleId }

        private void Load_Matrix()
        {
            // Prepare a Matrix helper
            Matrix oMatrixHelper = new Matrix(SAPBusinessOne_Application: Globals.oApp, FormToUse: _SBO_Form);

            string sqlQuery = string.Format(@"
SELECT
	[OIBT].[ItemName] AS [ItemName]
	,[OIBT].[Quantity] AS [OnHand]
	,[OIBT].[Quantity] AS [Quantity]
	,[OBTN].[U_VSC_MotherID] AS [BatchNumber]
	,[OIBT].[WhsCode] AS [Warehouse]
	,[OBTN].[U_VSC_PassedQA]
	, CASE [OBTN].[U_VSC_PassedQA]
	        WHEN 1  THEN 'Yes'
	                ELSE 'No'
	  END AS [QAPassed]
	,[OBTN].[U_VSC_LabTestID] AS [StateSampleID]
	,[OIBT].[BatchNum] AS [SampleBatchId]
	,[OIBT].[CreateDate] AS [DateCreated]
FROM [OIBT]
JOIN [OITM] ON [OITM].[ItemCode] = [OIBT].[ItemCode] 
JOIN [OBTN] ON [OBTN].[DistNumber] = [OIBT].[BatchNum]
WHERE ISNULL([OBTN].[U_VSC_LabTestID], '') != ''
    AND [OBTN].[U_VSC_PassedQA] != '1'
	AND ISNULL([OBTN].[U_VSC_MotherID], '') != ''

");

            // Load plants into the matrix
            oMatrixHelper.LoadDatabaseDataIntoMatrix(
                          "Table",
                          "mtxMain",
                          new List<Matrix.MatrixColumn>() { 
                    // new Helpers.Matrix.MatrixColumn(){ ColumnName=MatrixColumns.QAPassed.ToString(), Caption="QAPassed", ColumnWidth=80, ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT  }
                    //,
                    new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.BatchNumber.ToString(), Caption="Batch", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                    ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.ItemName.ToString(), Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                    ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.DateCreated.ToString(), Caption="Created On", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }      
                    ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.OnHand.ToString(), Caption="OnHand", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                    ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.Warehouse.ToString(), Caption="Warehouse", ItemType = BoFormItemTypes.it_EDIT  }
                    ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.SampleBatchId.ToString(), Caption="SampleBatchId", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                    ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.StateSampleId.ToString(), Caption="StateSampleId", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                    
                    
                          }, SQLQuery: sqlQuery);

            // Grab the matrix from the form UI
            SAPbouiCOM.Matrix matrix = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "mtxMain", _SBO_Form) as SAPbouiCOM.Matrix;

            // Allow multiple Samples to be checked.
            matrix.SelectionMode = BoMatrixSelect.ms_Auto;

        }

        #endregion

        #region Main Form Actions - Includes Button Events.

        public void CheckSelectedSamples()
        {
            SAPbouiCOM.Matrix matrix = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "mtxMain", _SBO_Form) as SAPbouiCOM.Matrix;

            List<PassedTestResult> passedResults = new List<PassedTestResult>();

            int rowCount = 0;
            try
            {
                rowCount = matrix.RowCount + 1;
            }
            catch(Exception ex)
            { }

            for (int i = 1; i < rowCount; i++)
            {
                if (!matrix.IsRowSelected(i))
                {
                    continue;
                }

                string batchNumber = matrix.Columns.Item(MatrixColumns.BatchNumber).Cells.Item(i).Specific.Value;
                string sampleId = matrix.Columns.Item(MatrixColumns.SampleBatchId).Cells.Item(i).Specific.Value;
                string stateSampleId = matrix.Columns.Item(MatrixColumns.StateSampleId).Cells.Item(i).Specific.Value;
                string itemName = matrix.Columns.Item(MatrixColumns.ItemName).Cells.Item(i).Specific.Value;

                TestResults helperTestResults = new WAStateHelpers.TestResults(
                    company: Globals.oCompany, application: Globals.oApp);

                TestResults.StateTestResults testResults = helperTestResults.CheckStateTestsAPI(stateSampleId, sampleId);

                //testResults.PopulateDefaultTestsWithResults();

                if (testResults.FoundResults)
                {
                    PassedTestResult newPass = new PassedTestResult()
                    {
                        ItemName = itemName,
                        Result = testResults,
                        BatchNumber = batchNumber,
                        SampleBatchNumber = sampleId
                    };
                    passedResults.Add(newPass);
                }
            }

            Controllers.BatchItems controllerBatchItem = new Controllers.BatchItems(
                   SAPBusinessOne_Application: Globals.oApp,
                   SAPBusinessOne_Company: Globals.oCompany);

            int confirmResult = -1;

            int passedCount = 0;
            if (passedResults.Count > 0)
            {
                string messageToUser = string.Format(@"Results Success! Found #{0} test results.
                        Would you like to populate existing tests with results as well?", passedResults.Count);

                confirmResult = Globals.oApp.MessageBox(messageToUser, 1, "Yes", "No");

                foreach (var passedResult in passedResults)
                {
                    if (confirmResult == 1)
                    {
                        passedResult.Result.PopulateDefaultTestsWithResults();
                    }

                    if (passedResult.Result.TestingPassed == TestResults.TestResultState.Success)
                    {
                        messageToUser = string.Format(@"Would you like to release the batch for processing? 
Select 'No' if you sent multiple samples of the same batch for QA", passedResults.Count);

                        confirmResult = Globals.oApp.MessageBox(messageToUser, 1, "Yes", "No");

                        if (confirmResult == 1)
                        {
                            controllerBatchItem.UpdateQATestResults(batchNumber: passedResult.BatchNumber,
                                newStatus: NSC_DI.Globals.testResultStatus.Passed);

                            controllerBatchItem.UpdateQATestResults(batchNumber: passedResult.SampleBatchNumber,
                                newStatus: NSC_DI.Globals.testResultStatus.Passed);

                            passedCount++;
                        }
                    }
                    else if (passedResult.Result.TestingPassed == TestResults.TestResultState.Failed)
                    {
                        controllerBatchItem.UpdateQATestResults(batchNumber: passedResult.BatchNumber,
                            newStatus: NSC_DI.Globals.testResultStatus.Failed);

                        controllerBatchItem.UpdateQATestResults(batchNumber: passedResult.SampleBatchNumber,
                            newStatus: NSC_DI.Globals.testResultStatus.Failed);
                    }
                }

                confirmResult = Globals.oApp.MessageBox(string.Format("{0} batches are now ready to be processed or sold!", passedCount));
                Load_Matrix();
            }
            else
            {
                confirmResult = Globals.oApp.MessageBox("No Test Results Found!");
            }
        }

        private void BTN_LOOKUP_ITEM_PRESSED()
        {
            // Grad the textbox for the barcode scanner from the form UI
            EditText TXT_LOOKUP = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", _SBO_Form);

            // If nothing was passed in the textbox, just return
            if (TXT_LOOKUP.Value.Length == 0)
            {
                return;
            }

            // Grab the matrix from the form UI
            SAPbouiCOM.Matrix mtxMain = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "mtxMain", _SBO_Form) as SAPbouiCOM.Matrix;

            // Keep track of the warehouse ID from the barcode (if passed)
            string WarehouseID = "";

            // If the first four characters of the barcode are "WHSE", then the rest of the barcode contains the warehouse code.
            if (TXT_LOOKUP.Value.Length >= 4 && TXT_LOOKUP.Value.Substring(0, 4) == "WHSE")
            {
                WarehouseID = TXT_LOOKUP.Value.Replace("WHSE-", "");
            }

            // For each row already selected
            for (int i = 1; i < (mtxMain.RowCount + 1); i++)
            {
                // Grab the row's Plant ID column
                string batchId = ((EditText)mtxMain.Columns.Item(MatrixColumns.BatchNumber).Cells.Item(i).Specific).Value;

                // Grab the row's Warehouse Code column
                string batchWarehouse = ((EditText)mtxMain.Columns.Item(MatrixColumns.Warehouse).Cells.Item(i).Specific).Value;

                // Grab the row's Stated ID column
                string sampleBatch = ((EditText)mtxMain.Columns.Item(MatrixColumns.SampleBatchId).Cells.Item(i).Specific).Value;

                // Grab the row's Stated ID column
                string sampleStateId = ((EditText)mtxMain.Columns.Item(MatrixColumns.StateSampleId).Cells.Item(i).Specific).Value;

                // If the warehouse was scanned
                if (WarehouseID != "")
                {
                    // If the scanned warehouse code matches the row's warehouse code
                    if (WarehouseID == batchWarehouse)
                    {
                        // Select the row where the warehouse codes match
                        mtxMain.SelectRow(i, true, true);
                    }
                }
                // No warehouse code was passed, so we are trying to identify an individual plant
                else
                {
                    // If the plant's ID matches the scanned barcode
                    if (TXT_LOOKUP.Value == batchId)
                    {
                        // Select the row where the plant ID's match
                        mtxMain.SelectRow(i, true, true);
                        // Break out of the code since we are done finding our selection
                        break;
                    }

                    if (TXT_LOOKUP.Value == sampleBatch)
                    {
                        // Select the row where the warehouse codes match
                        mtxMain.SelectRow(i, true, true);
                    }

                    if (TXT_LOOKUP.Value == sampleStateId)
                    {
                        // Select the row where the warehouse codes match
                        mtxMain.SelectRow(i, true, true);
                    }
                }
            }

            // Empty the barcode scanner field
            TXT_LOOKUP.Value = "";

            // Focus back into the barcode scanner's text field
            TXT_LOOKUP.Active = true;
        }

        #endregion

        #region Data Classes

        public class PassedTestResult
        {
            public string ItemName { get; set; }
            public string BatchNumber { get; set; }
            public string SampleBatchNumber { get; set; }
            public TestResults.StateTestResults Result { get; set; }
        }

        #endregion
*/
    }
}