﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms.Lab_Controls
{
    public class F_TestFieldList : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "NSC_TESTFIELD_LS";


        // FOR OLD CODE
        public VERSCI.Forms _VirSci_Helper_Form;
        public SAPbouiCOM.Application _SBO_Application;
        public SAPbobsCOM.Company _SBO_Company;
        public SAPbouiCOM.Form _SBO_Form;
        public string FormUID { get; set; }
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;


			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "BTN_ADD":
					F_TestFieldInfo.FormCreate();
					break;

				case "BTN_FIND":
					this.Load_Matrix();
					break;

				case "BTN_IMPORT":
					BTN_IMPORT_Click();
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "mtx_plan":
					PLAN_MATRIX_LINK_PRESSED(pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
        //--------------------------------------------------------------------------------------- et_FORM_ACTIVATE
        [B1Listener(BoEventTypes.et_FORM_ACTIVATE, false, new string[] { cFormID })]
        public virtual void OnAfterFormActivate(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            Refresh();

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

		public F_TestFieldList() : this(Globals.oApp, Globals.oCompany){}	// FOR OLD CODE

	    public F_TestFieldList(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
	    {
		    _SBO_Application = SAPBusinessOne_Application;
		    _SBO_Company = SAPBusinessOne_Company;
	    }

	    public void Form_Load()
	    {
		    try
		    {
			    _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                _SBO_Form.Items.Item("BTN_IMPORT").Visible = false;

			    // Set the main image
			    CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_Main", @"list-icon.bmp");

			    Load_Matrix();

			    // Load the total count of Compounds.
				_SBO_Form.Items.Item("txt_total").Specific.Value = NSC_DI.UTIL.SQL.GetValue<string>("SELECT COUNT(*)  FROM [@" + NSC_DI.Globals.tTestField + "]", "");

			    // Load the total count of Compounds.
				_SBO_Form.Items.Item("txt_crnt").Specific.Value =
					NSC_DI.UTIL.SQL.GetValue<string>("SELECT COUNT(*)  FROM [@" + NSC_DI.Globals.tTestField + "] WHERE [CreateDate] >= CONVERT(DATE,'1/1/" + DateTime.Now.Year + @"')", "");
			}
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    _SBO_Form.Freeze(false);
			    _SBO_Form.VisibleEx = true;

			    //NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

        public void Refresh()
        {
            try
            {
                _SBO_Form.Items.Item("txt_total").Specific.Value = NSC_DI.UTIL.SQL.GetValue<string>("SELECT COUNT(*)  FROM [@" + NSC_DI.Globals.tTestField + "]", "");

                _SBO_Form.Items.Item("txt_crnt").Specific.Value =
                    NSC_DI.UTIL.SQL.GetValue<string>("SELECT COUNT(*)  FROM [@" + NSC_DI.Globals.tTestField + "] WHERE [CreateDate] >= CONVERT(DATE,'1/1/" + DateTime.Now.Year + @"')", "");

                Load_Matrix();
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

	    private void Load_Matrix()
        {
            EditText txt_name = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txt_name", _SBO_Form);

			VERSCI.Matrix VirSci_Helper_Matrix = new VERSCI.Matrix(Globals.oApp, _SBO_Form);

            if (txt_name.Value == "")
            {
                VirSci_Helper_Matrix.LoadDatabaseDataIntoMatrix("@" + NSC_DI.Globals.tCompounds, "mtx_plan", new List<VERSCI.Matrix.MatrixColumn>() { 
                        new VERSCI.Matrix.MatrixColumn() { Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON } 
                        , new VERSCI.Matrix.MatrixColumn() { Caption = "Name", ColumnName = "U_Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                    }, "SELECT * FROM [@" + NSC_DI.Globals.tTestField + "] ORDER BY CONVERT(int,[Code]) DESC");
            }
            else
            {
                VirSci_Helper_Matrix.LoadDatabaseDataIntoMatrix("@" + NSC_DI.Globals.tCompounds, "mtx_plan", new List<VERSCI.Matrix.MatrixColumn>() { 
                        new VERSCI.Matrix.MatrixColumn() { Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON } 
                        , new VERSCI.Matrix.MatrixColumn() { Caption = "Name", ColumnName = "U_Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                    }, "SELECT * FROM [@" + NSC_DI.Globals.tTestField + @"] WHERE [U_Name] LIKE '%" + txt_name.Value.Replace("'", "''") + @"%' ORDER BY CONVERT(int,[Code]) DESC");
            }

        }

        private void BTN_IMPORT_Click()
        {
            /*
            // Ask the user on another thread the select an image file
            Thread thread_new = new Thread(new ThreadStart(AskForFile));
            thread_new.SetApartmentState(ApartmentState.STA);
            thread_new.Start();

            // Wait until user has selected a CSV file to import.
            while (thread_new.IsAlive)
            {
                Thread.Sleep(100);
            }

            List<CSV_Compound> ListOfCompounds = new List<CSV_Compound>();

            using (TextReader TextReader_Items = File.OpenText(SelectedFileName))
            {
                TextReader_Items.Read();

                CsvHelper.Configuration.CsvConfiguration CSV_Config = new CsvHelper.Configuration.CsvConfiguration();
                CSV_Config.HasHeaderRecord = true;
                CSV_Config.IgnoreHeaderWhiteSpace = true;
                CSV_Config.TrimHeaders = false;

                // Prepare a new CSV Reader object
                CsvHelper.CsvReader CSV_Reader_Items = new CsvHelper.CsvReader(TextReader_Items, CSV_Config);
                CSV_Reader_Items.Configuration.AutoMap<CSV_Compound>();

                // Count the total number of records we are creating
                ListOfCompounds = CSV_Reader_Items.GetRecords<CSV_Compound>().ToList();
            }

            // Create a Progress Bar
            oProgBar = Globals.oApp.StatusBar.CreateProgressBar("Starting to create items.", ListOfCompounds.Count, false);

            foreach (CSV_Compound Compound in ListOfCompounds)
            {
                // Prepare to run a SQL statement.
                SAPbobsCOM.SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + Globals.SAP_PartnerCode + "_" + NavSol.Models.Compound.TableName + "] ");

                // The single digigt plant type is "S", "H", or "I".  For "Sativa", "Hybrid", and "Indica" respectivley.
                string PlantTypeSingleDigitCode = Compound.Type;
                string NewCompoundID = oRecordSet.Fields.Item(0).Value.ToString();
                string CompoundName = Compound.Name;
                string CompoundType = "";

                switch (Compound.Type)
                {
                    case "I":
                        CompoundType = "Indica";
                        break;
                    case "S":
                        CompoundType = "Sativa";
                        break;
                    case "H":
                        CompoundType = "Hybrid";
                        break;
                }

                // Prepare a Compound Controller
                NSCNSC_DI.SAP._DI.UTIL.Misc.KillObject(oForm);Compound VirSci_Controllers_Compound = new Controllers.Compound(SAPBusinessOne_Application: Globals.oApp, SAPBusinessOne_Company: Globals.oCompany, SAPBusinessOne_Form: _SBO_Form);

                // Create Compound UDO
                VirSci_Controllers_Compound.CreateCompound(Code: NewCompoundID, Name: CompoundName, Type: CompoundType);

                // Attempt to add the new Compound items
                VirSci_Controllers_Compound.AddItemsToCompound(PlantTypeSingleDigitCode: PlantTypeSingleDigitCode, CompoundID: NewCompoundID, CompoundName: CompoundName, CompoundType: CompoundType);

                oProgBar.Text = "Compound " + CompoundName + " created!";

                oProgBar.Value += 1;
            }

            oProgBar.Stop();
             * */
        }

		private static bool BTN_Add_Click(Form pForm)
		{
			EditText txtId = null;
			EditText txtName = null;

			try
			{
				if (pForm.Items.Item("1").Specific.Caption != "Add") return true;
				if (CommonUI.LabControls.TestValidate(pForm) == false) return false;

				txtId = pForm.Items.Item("TXT_ID").Specific;
				txtName = pForm.Items.Item("TXT_NAME").Specific;

				if (string.IsNullOrEmpty(txtName.Value))
				{
					Globals.oApp.StatusBar.SetText("Please enter a name for the Test!");
					txtName.Active = true;
					return true;
				}

				// Automatically generate items from the newly created strain.
				NSC_DI.SAP.Disease.CreateDisease(txtId.Value, txtName.Value);

				Globals.oApp.StatusBar.SetText("Success!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

				txtName.Value = "";
				return true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(txtId);
				NSC_DI.UTIL.Misc.KillObject(txtName);
				GC.Collect();
			}
		}

	    private void PLAN_MATRIX_LINK_PRESSED(ItemEvent SAP_UI_ItemEvent)
	    {
		    // Attempt to grab the selected row ID
		    int SelectedRowID = SAP_UI_ItemEvent.Row;

		    if (SelectedRowID <= 0) return;

		    try
		    {

			    // Which column stores the ID
			    int ColumnIDForIDOfItemSelected = 0;

                string ItemSelected = CommonUI.Forms.GetField<string>(_SBO_Form, "mtx_plan", SelectedRowID, ColumnIDForIDOfItemSelected);

				F_TestFieldInfo.Form_Load_Find(ItemSelected);
			}
		    catch (Exception ex)
		    {
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
	    }

	    public class CSV_TestField
        {
            public string Name { get; set; }
            public string Type { get; set; }
        }

        /// <summary>
        /// Stores the selected file from the seperate thread
        /// </summary>
        private string SelectedFileName;
        private void AskForFile()
        {
			System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();

            var y = ofd.ShowDialog();

            SelectedFileName = ofd.FileName;
        }
    }
}