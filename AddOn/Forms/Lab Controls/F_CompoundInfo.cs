﻿using System;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Lab_Controls
{
    public class F_CompoundInfo : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_COMP_INFO";

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			if (pVal.ItemUID == "1" && oForm.Items.Item("1").Specific.Caption == "Add") BubbleEvent = BTN_Add_Click(oForm);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "1":
					if (oForm.Items.Item("1").Specific.Caption == "OK") BTN_OK_Click(oForm);
					if (oForm.Items.Item("1").Specific.Caption == "Add") FormSetup(oForm);;	
					break;

				case "BTN_ADD":
					BTN_ADD_EFFECT_Click(oForm);
					break;

				case "BTN_DELE":
					BTN_REMOVE_EFFECT_Click(oForm);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "mtx_plan":
					MTX_EFFCT_MATRIX_LINK_PRESSED(oForm, pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			PictureBox IMG_Main = null;
			EditText TextBox_ID = null;
			try
			{
				pForm.Freeze(true);

                // Hide Effect and Disease for now

                pForm.Items.Item("FLD_EFFCT").Visible = false;

                pForm.Items.Item("FLD_DISEA").Visible = false;

				pForm.Mode = BoFormMode.fm_ADD_MODE;

				pForm.Items.Item("FLD_GENRL").Specific.Select();	// Select the first tab by default

				TextBox_ID = pForm.Items.Item("TXT_ID").Specific;
				TextBox_ID.Value =
					NSC_DI.UTIL.SQL.GetValue<string>("SELECT COALESCE(MAX(CONVERT(int,[Code])),0) + 1 FROM [@" + NSC_DI.Globals.tCompounds + "]", "1");

				// Only allow task_id to be edited during "Find" mode.
				TextBox_ID.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_All), BoModeVisualBehavior.mvb_False);
				TextBox_ID.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_Find), BoModeVisualBehavior.mvb_True);

				CommonUI.LabControls.Load_Effect_Matrix(pForm, TextBox_ID);
				CommonUI.LabControls.Load_Disease_Matrix(pForm, TextBox_ID);
				CommonUI.LabControls.LoadCBO(pForm, "CMB_EFFCT", NSC_DI.Globals.tEffect);
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.VisibleEx = true;
				pForm.Freeze(false);
				NSC_DI.UTIL.Misc.KillObject(IMG_Main);
				NSC_DI.UTIL.Misc.KillObject(TextBox_ID);
				GC.Collect();
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Form oForm = null;
			Item oItm = null;
			try
			{
				oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return null;
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

	    public static void Form_Load_Find(string CompoundID)
	    {
		    EditText TextBox_ID = null;

		    try
		    {
			    var oForm = FormCreate();
			    oForm.Freeze(true);
			    oForm.Mode = BoFormMode.fm_FIND_MODE;

			    // Enable UDO navigation buttons
			    oForm.DataBrowser.BrowseBy = "TXT_ID";

			    // Find the ID and Name textboxes in the UI
			    TextBox_ID = oForm.Items.Item("TXT_ID").Specific as EditText;

			    // Set the textbox for the ID to the pre-determined number.
			    TextBox_ID.Value = CompoundID;

				oForm.Items.Item("1").Click();

			    // Select the first tab by default
				oForm.Items.Item("FLD_GENRL").Specific.Select();

				CommonUI.LabControls.Load_Effect_Matrix(oForm, TextBox_ID);
				CommonUI.LabControls.Load_Disease_Matrix(oForm, TextBox_ID);
				
				oForm.Freeze(false);
			}
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
				NSC_DI.UTIL.Misc.KillObject(TextBox_ID);
			    GC.Collect();
		    }
	    }

		private static void BTN_OK_Click(Form pForm)
        {

        }

		private static bool BTN_Add_Click(Form pForm)
	    {
		    EditText txtId = null;
		    EditText txtName = null;
		    ComboBox cmbCompoundType = null;

		    try
		    {
			    if (CompoundValidate(pForm) == false) return false;

			    txtId = pForm.Items.Item("TXT_ID").Specific;
			    txtName = pForm.Items.Item("TXT_NAME").Specific;
			    cmbCompoundType = pForm.Items.Item("CMB_TYPE").Specific;

			    if (string.IsNullOrEmpty(txtName.Value))
			    {
				    Globals.oApp.StatusBar.SetText("Please enter a name for the compound!");
				    txtName.Active = true;
				    return true;
			    }

			    if (string.IsNullOrEmpty(cmbCompoundType.Value))
			    {
				    Globals.oApp.StatusBar.SetText("Please select a type of compound!");
				    cmbCompoundType.Active = true;
					return true;
			    }

			    // Automatically generate items from the newly created strain.
			    NSC_DI.SAP.Compound.CreateCompound(txtId.Value, txtName.Value, cmbCompoundType.Value);
			    Globals.oApp.StatusBar.SetText("Success!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
			    txtName.Value = "";

			    //Globals.oApp.StatusBar.SetText("Failed to add Compound!", BoMessageTime.bmt_Short);
				return true;
			}
		    catch (Exception ex)
		    {
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			    return false;
		    }
		    finally
		    {
			    NSC_DI.UTIL.Misc.KillObject(txtId);
			    NSC_DI.UTIL.Misc.KillObject(txtName);
			    NSC_DI.UTIL.Misc.KillObject(cmbCompoundType);
			    GC.Collect();
		    }
	    }

	    private static bool CompoundValidate(Form pForm)
	    {
			EditText TextBox_Name = null;

		    try
		    {
			    // Check for existing Compound
			     TextBox_Name = pForm.Items.Item("TXT_NAME").Specific;

			    string SQL_Query_FindCompoundByName = "SELECT [Code] FROM [@" + NSC_DI.Globals.tCompounds + "] WHERE [U_Name] = '" + TextBox_Name.Value + "'";

				SAPbobsCOM.Fields ResultsFromSQL = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(SQL_Query_FindCompoundByName);

				if (ResultsFromSQL == null || ResultsFromSQL.Count <= 0) return true;

				Globals.oApp.StatusBar.SetText("There is already a Compound named '" + TextBox_Name.Value + "'");
			    return false;
		    }
		    catch (Exception ex)
		    {
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
		    finally
		    {
			    pForm.Freeze(false);
				NSC_DI.UTIL.Misc.KillObject(TextBox_Name);
			    GC.Collect();
		    }
	    }

		private static void BTN_ADD_EFFECT_Click(Form pForm)
		{
			try
			{
				CommonUI.LabControls.ADD_EFFECT(pForm);

				CommonUI.LabControls.Load_Effect_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
				CommonUI.LabControls.Load_Disease_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
			}
			catch (System.ComponentModel.WarningException)
			{
				// message was already displayed
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

	    private static void BTN_REMOVE_EFFECT_Click(Form pForm)
	    {
		    SAPbouiCOM.Matrix MTX_EFFCT = null;
		    try
		    {
				CommonUI.LabControls.REMOVE_EFFECT(pForm);
				CommonUI.LabControls.Load_Disease_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
				CommonUI.LabControls.Load_Effect_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
			}
			catch (System.ComponentModel.WarningException)
			{
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(MTX_EFFCT);
				GC.Collect();
			}
		}

	    private static void MTX_EFFCT_MATRIX_LINK_PRESSED(Form pForm, ItemEvent SAP_UI_ItemEvent)
	    {
		    try
		    {
			    int SelectedRowID = SAP_UI_ItemEvent.Row;

			    if (SelectedRowID > 0)
			    {
				    // Which column stores the ID
				    int ColumnIDForIDOfItemSelected = 0;

				    // Get the ID of the note selected
				    var ItemSelected = CommonUI.Forms.GetField<string>(pForm, "MTX_EFFCT", SelectedRowID, ColumnIDForIDOfItemSelected);

					// Open the plant manager for the selected plant
					F_EffectInfo.Form_Load_Find(ItemSelected);
			    }
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    GC.Collect();
		    }
	    }
    }
}
