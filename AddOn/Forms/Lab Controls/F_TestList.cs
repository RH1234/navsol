﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms.Lab_Controls
{
    class F_TestList : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "NSC_TEST_LS";
        private static NSC_DI.Globals.ItemGroupTypes itemGroupType = NSC_DI.Globals.ItemGroupTypes.Sample;
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPressed(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "BTN_ADD":
                    F_TestInfo.FormCreate(pCallingFormUID:oForm.UniqueID);
                    break;

                case "BTN_FIND":
                    Load_Matrix(oForm);
                    break;

                case "BTN_FIELD":
                    BTN_FIELD_Click(oForm);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
        [B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "mtx_plan":
                    PLAN_MATRIX_LINK_PRESSED(oForm, pVal);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        //--------------------------------------------------------------------------------------- et_FORM_ACTIVATE
        [B1Listener(BoEventTypes.et_FORM_ACTIVATE, false, new string[] { cFormID })]
        public virtual void OnAfterFormActivate(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            Refresh(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public static Form FormCreate(bool singleInstance = false, string pCallingFormUID = null)
        {
            Form oForm = null;
            Item oItm = null;
            try
            {
                oForm = CommonUI.Forms.Load(cFormID, singleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
                {
                    oItm = oForm.Items.Add("TXT_PARE", BoFormItemTypes.it_EDIT);
                    oItm.Top = -20;
                    oItm.Specific.Value = pCallingFormUID;
                }

                FormSetup(oForm);
                return oForm;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }
	    private static void FormSetup(Form pForm, bool makeVisible = true)
	    {
            try
            {
			    // Freeze the Form UI
			    pForm.Freeze(true);

			    // Set the main image
			    CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_Main", @"list-icon.bmp");

			    Load_Matrix(pForm);

			    // Load the total count of Tests.
			    pForm.Items.Item("txt_total").Specific.Value = NSC_DI.UTIL.SQL.GetValue<string>("SELECT COUNT(*)  FROM [@" + NSC_DI.Globals.tTests + "]", "");

			    // Load the total count of Tests.
			    pForm.Items.Item("txt_crnt").Specific.Value =
				    NSC_DI.UTIL.SQL.GetValue<string>("SELECT COUNT(*)  FROM [@" + NSC_DI.Globals.tTests + "] WHERE DATEPART(YEAR, CreateDate) >= " + DateTime.Now.Year + @"", "");
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
				pForm.Freeze(false);
				if (makeVisible) pForm.VisibleEx = true;

				//NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

        public static void Refresh(Form pForm)
        {
            try
            {
                FormSetup(pForm, false);
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

	    private static void Load_Matrix(Form pForm)
        {
		    try
		    {
			    EditText txt_name = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txt_name", pForm);
                

			    if (txt_name.Value == "")
			    {
				    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tTests, "mtx_plan", new List<CommonUI.Matrix.MatrixColumn>()
				    {new CommonUI.Matrix.MatrixColumn() {Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
					    new CommonUI.Matrix.MatrixColumn()
					    {
						    Caption = "Name",
						    ColumnName = "U_Name",
						    ColumnWidth = 80,
						    ItemType = BoFormItemTypes.it_EDIT
					    }
				    }, "SELECT * FROM [@" + NSC_DI.Globals.tTests + @"] WHERE ISNULL([@" + NSC_DI.Globals.tTests + @"].[U_Inactive], 'N') = 'N' ORDER BY CONVERT(int,[Code]) ASC");
			    }
			    else
			    {
                    var testName = NSC_DI.UTIL.SQL.FixSQL(CommonUI.Forms.GetField<string>(pForm, "txt_name"));

                    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tTests, "mtx_plan", new List<CommonUI.Matrix.MatrixColumn>()
				    {
					    new CommonUI.Matrix.MatrixColumn() {Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON}
					    ,
					    new CommonUI.Matrix.MatrixColumn()
					    {
						    Caption = "Name",
						    ColumnName = "U_Name",
						    ColumnWidth = 80,
						    ItemType = BoFormItemTypes.it_EDIT
					    }
				    },
					    "SELECT * FROM [@" + NSC_DI.Globals.tTests + "] WHERE [U_Name] LIKE '%" + testName +
					    @"%' ORDER BY CONVERT(int,[Code]) ASC");
			    }
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    //NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }

        }

        private static void BTN_FIELD_Click(Form pForm)
        {
			(new NavSol.Forms.Lab_Controls.F_TestFieldList()).Form_Load();
		}

		private void PLAN_MATRIX_LINK_PRESSED(Form pForm, ItemEvent SAP_UI_ItemEvent)
		{
			// Attempt to grab the selected row ID
			int SelectedRowID = SAP_UI_ItemEvent.Row;

			if (SelectedRowID <= 0) return;

			try
			{
				// Which column stores the ID
				int ColumnIDForIDOfItemSelected = 0;

				var ItemSelected = CommonUI.Forms.GetField<string>(pForm, "mtx_plan", SelectedRowID, ColumnIDForIDOfItemSelected);

				F_TestInfo.Form_Load_Find(ItemSelected);
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
		}
	}
}