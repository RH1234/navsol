﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using NavSol.VERSCI;
using SAPbobsCOM;
using SAPbouiCOM;
using Matrix = NavSol.CommonUI.Matrix;

namespace NavSol.Forms.Lab_Controls
{
    public class F_SampleInfo : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "NSC_SAMPLE_INFO";

        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPressed(ItemEvent pVal)
        {
			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
        }
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_KEY_DOWN
        [B1Listener(BoEventTypes.et_KEY_DOWN, false, new string[] { cFormID })]
        public virtual void OnAfterKeyDown(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "txtTCFL":
                    if (pVal.CharPressed == 9) HandleTestSelectionCFL(oForm);
                    break;

                case "txtTRCFL":
                    if (pVal.CharPressed == 9)
                    {
                        EditText txtTRCFL = FormControl.GetControlFromForm(FormControl.FormControlTypes.EditText, "txtTRCFL", pForm
                        );

                        // Only handle the tab event if the text field is empty.
                        if (string.IsNullOrEmpty(txtTRCFL.Value))
                        {
                            txtTRCFL.Active = true;
                            HandleTestResultSelectionCFL();
                        }
                    }
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "BTN_ADDF":
                    BTN_ADD_COMPOUND_Click(oForm);
                    break;

                case "BTN_REMF":
                    BTN_REMOVE_COMPOUND_Click(oForm);
                    break;

                case "BTN_ADDC":
                    BTN_ADD_TESTFIELD_Click(oForm);
                    break;

                case "BTN_REMC":
                    BTN_REMOVE_TESTFIELD_Click(oForm);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
        [B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "MTX_TCOMP":
                    MTX_COMPOUND_MATRIX_LINK_PRESSED(oForm, pVal);
                    break;
                case "MTX_TFIELD":
                    MTX_TESTFIELD_MATRIX_LINK_PRESSED(oForm, pVal);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------
		private static string FormXML()
        {
            string xml = "";
            switch (Globals.oCompany.language)
            {
                case (BoSuppLangs.ln_English):
                    xml = @"
<?xml version='1.0' encoding='UTF-16'?>
<Application>
  <forms>
    <action type='add'>
      <form appformnumber='(NSC_FORM)' FormType='(NSC_FORM)' type='4' BorderStyle='4' uid='' title='Sample Information' visible='0' default_button='' pane='1' color='0' left='370' top='20' width='640' height='460' client_width='' client_height='' AutoManaged='1' SupportedModes='15' ObjectType='' mode='1'>
        <datasources>
          <DataTables />
          <dbdatasources>
            <action type='add' />
          </dbdatasources>
          <userdatasources>
            <action type='add'>
              <datasource type='9' size='1' uid='UCHK_CMP' />
              <datasource uid='FLD_GENRL' type='9' size='3' />
            </action>
          </userdatasources>
        </datasources>
        <Menus />
        <items>
          <action type='add'>
            <item uid='Item_3' type='100' left='15' tab_order='0' width='610' top='29' height='360' visible='1' enabled='1' from_pane='0' to_pane='0' disp_desc='0' right_just='0' description='' linkto='' forecolor='-1' backcolor='-1' text_style='0' font_size='-1' supp_zeros='0' AffectsFormMode='1'>
              <AutoManagedAttribute />
              <specific />
            </item>
            <item top='10' left='15' width='100' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='' right_just='0' type='99' visible='1' uid='FLD_GENRL' IsAutoGenerated='0'>
              <specific pane='1' caption='General Details' AutoPaneSelection='1'>
                <databind databound='1' table='' alias='FLD_GENRL' />
              </specific>
            </item>
            <item top='10' left='115' width='100' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='' right_just='0' type='99' visible='1' uid='FLD_FIELDS' IsAutoGenerated='0'>
              <specific pane='2' caption='Tests' AutoPaneSelection='1'>
                <databind databound='1' table='' alias='FLD_GENRL' />
              </specific>
            </item>
            <item top='10' left='215' width='100' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='' right_just='0' type='99' visible='1' uid='FLD_COMP' IsAutoGenerated='0'>
              <specific pane='3' caption='Test Results' AutoPaneSelection='1'>
                <databind databound='1' table='' alias='FLD_GENRL' />
              </specific>
            </item>
            <item top='60' left='35' width='120' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='TXT_ID' right_just='0' type='8' visible='1' uid='lbl_ID' IsAutoGenerated='0'>
              <specific caption='Code' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='60' left='181' width='150' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='' right_just='0' type='16' visible='1' uid='TXT_ID' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item top='90' left='35' width='120' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='TXT_NAME' right_just='0' type='8' visible='1' uid='lbl_Name' IsAutoGenerated='0'>
              <specific caption='Name' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='90' left='181' width='150' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='' right_just='0' type='16' visible='1' uid='TXT_NAME' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item cellHeight='16' tab_order='0' titleHeight='20' top='104' left='40' width='540' height='251' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='2' to_pane='2' linkto='' right_just='0' type='127' visible='1' uid='MTX_TEST' IsAutoGenerated='0'>
              <specific layout='0' SelectionMode='1'>
                <columns>
                  <action type='add'>
                    <column backcolor='16777215' ChooseFromListIsAutoFill='0' font_size='-1' forecolor='-1' text_style='0' disp_desc='0' visible='1' AffectsFormMode='1' val_on='Y' IsAutoGenerated='0' val_off='N' title='Code' width='75' editable='1' type='16' right_just='0' uid='#' sortable='0' />
                    <column backcolor='-1' ChooseFromListIsAutoFill='0' font_size='-1' forecolor='-1' text_style='0' disp_desc='0' visible='1' AffectsFormMode='1' val_on='Y' IsAutoGenerated='0' val_off='N' title='Effect Name' width='350' editable='1' type='16' right_just='0' uid='Col_0' sortable='0' />
                  </action>
                </columns>
              </specific>
            </item>
            <item top='40' left='35' width='245' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='2' to_pane='2' linkto='' right_just='0' type='8' visible='1' uid='lbl_Effect' IsAutoGenerated='0'>
              <specific caption='Which Tests should be run on this Sample' />
            </item>
            <item cellHeight='16' tab_order='0' titleHeight='20' top='134' left='40' width='540' height='221' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='3' to_pane='3' linkto='' right_just='0' type='127' visible='1' uid='MTX_TRSLT' IsAutoGenerated='0'>
              <specific layout='0' SelectionMode='1'>
                <columns>
                  <action type='add'>
                    <column backcolor='16777215' ChooseFromListIsAutoFill='0' font_size='-1' forecolor='-1' text_style='0' disp_desc='0' visible='1' AffectsFormMode='1' val_on='Y' IsAutoGenerated='0' val_off='N' title='Code' width='75' editable='1' type='16' right_just='0' uid='#' sortable='0' />
                    <column backcolor='-1' ChooseFromListIsAutoFill='0' font_size='-1' forecolor='-1' text_style='0' disp_desc='0' visible='1' AffectsFormMode='1' val_on='Y' IsAutoGenerated='0' val_off='N' title='Test Name' width='350' editable='1' type='16' right_just='0' uid='Col_0' sortable='0' />
                  </action>
                </columns>
              </specific>
            </item>
            <item top='40' left='35' width='245' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='3' to_pane='3' linkto='' right_just='0' type='8' visible='1' uid='lbl_Comp' IsAutoGenerated='0'>
              <specific caption='Results of the selected Test' />
            </item>
            <item top='360' left='515' width='65' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='3' to_pane='3' linkto='' right_just='1' type='4' visible='1' uid='BTN_SAVE' IsAutoGenerated='0'>
              <specific caption='Save' />
            </item>
            <item top='400' left='40' width='65' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='' right_just='1' type='4' visible='1' uid='2' IsAutoGenerated='0'>
              <specific caption='Cancel' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='70' left='242' width='120' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='2' to_pane='2' linkto='' right_just='0' type='16' visible='1' uid='txtTCFL' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item top='70' left='130' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='2' to_pane='2' linkto='' right_just='0' type='8' visible='1' uid='lblToAdd' IsAutoGenerated='0'>
              <specific caption='Test To Add' />
            </item>
            <item top='360' left='515' width='65' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='2' to_pane='2' linkto='' right_just='0' type='4' visible='1' uid='BTN_REMT' IsAutoGenerated='0'>
              <specific caption='Remove' />
            </item>
            <item top='70' left='130' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='3' to_pane='3' linkto='' right_just='0' type='8' visible='1' uid='LBL_CADD' IsAutoGenerated='0'>
              <specific caption='Test Results From' />
            </item>
            <!--<item top='110' left='465' width='119' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='3' to_pane='3' linkto='' right_just='0' type='4' visible='1' uid='BTN_IMPRT' IsAutoGenerated='0'>
              <specific caption='Import Test Results' />
            </item>-->
            <item top='363' left='284' width='136' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='3' to_pane='3' linkto='CHK_COMP' right_just='0' type='8' visible='1' uid='Item_10' IsAutoGenerated='0'>
              <specific caption='Test Results Status' />
            </item>
            <item top='363' left='425' width='76' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='3' to_pane='3' linkto='' right_just='0' type='121' visible='1' uid='CHK_COMP' IsAutoGenerated='0'>
              <specific caption='Complete' val_on='Y' val_off='N'>
                <databind databound='1' table='' alias='UCHK_CMP' />
              </specific>
            </item>
            <item top='90' left='400' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='TXT_BATCH' right_just='0' type='8' visible='1' uid='Item_0' IsAutoGenerated='0'>
              <specific caption='Batch Number' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='90' left='515' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='' right_just='0' type='16' visible='1' uid='TXT_BATCH' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item top='120' left='35' width='120' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='TXT_WHRS' right_just='0' type='8' visible='1' uid='Item_2' IsAutoGenerated='0'>
              <specific caption='Warehouse Location:' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='120' left='181' width='150' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='' right_just='0' type='16' visible='1' uid='TXT_WHRS' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='120' left='515' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='' right_just='0' type='16' visible='1' uid='TXT_WHRSC' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item top='60' left='400' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='TXT_QTY' right_just='0' type='8' visible='1' uid='Item_5' IsAutoGenerated='0'>
              <specific caption='Quantity' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='60' left='515' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='' right_just='0' type='16' visible='1' uid='TXT_QTY' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item top='406' left='284' width='177' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='TXT_TCOM' right_just='0' type='8' visible='1' uid='lblTComp' IsAutoGenerated='0'>
              <specific caption='Test Status Total:' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='406' left='465' width='150' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='0' from_pane='0' to_pane='0' linkto='' right_just='0' type='16' visible='1' uid='TXT_TCOM' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item top='120' left='400' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='TXT_WHRSC' right_just='0' type='8' visible='1' uid='Item_1' IsAutoGenerated='0'>
              <specific caption='Warehouse Code' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='110' left='240' width='120' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='0' from_pane='3' to_pane='3' linkto='' right_just='0' type='16' visible='1' uid='TXT_RTEST' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item top='110' left='130' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='3' to_pane='3' linkto='' right_just='0' type='8' visible='1' uid='Item_9' IsAutoGenerated='0'>
              <specific caption='Selected Test' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='110' left='370' width='75' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='0' from_pane='3' to_pane='3' linkto='' right_just='0' type='16' visible='1' uid='TXT_RTESTC' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item top='220' left='280' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='TXT_WHRS' right_just='0' type='8' visible='1' uid='lblSample' IsAutoGenerated='0'>
              <specific caption='State Sample ID' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='223' left='395' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='0' from_pane='1' to_pane='1' linkto='' right_just='0' type='16' visible='1' uid='txtSample' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item top='245' left='280' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='TXT_WHRS' right_just='0' type='8' visible='1' uid='lblTStatus' IsAutoGenerated='0'>
              <specific caption='State Test Status' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='248' left='395' width='100' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='0' from_pane='1' to_pane='1' linkto='' right_just='0' type='16' visible='1' uid='txtTStatus' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item top='178' left='35' width='150' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='' right_just='0' type='4' visible='1' uid='btnCheck' IsAutoGenerated='0'>
              <specific caption='State Sample Results Check' />
            </item>
            <item font_size='-1' supp_zeros='0' tab_order='0' text_style='' top='223' left='35' width='215' height='83' AffectsFormMode='1' description='' disp_desc='0' enabled='0' from_pane='1' to_pane='1' linkto='' right_just='0' type='118' visible='1' uid='txtMessage' IsAutoGenerated='0'>
              <specific ScrollBars='2' />
            </item>
            <item top='205' left='35' width='121' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='' right_just='0' type='8' visible='1' uid='lblMessage' IsAutoGenerated='0'>
              <specific caption='Results Message' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='70' left='240' width='120' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='3' to_pane='3' linkto='' right_just='0' type='16' visible='1' uid='txtTRCFL' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
          </action>
        </items>
        <ChooseFromListCollection>
          <action type='add'>
            <ChooseFromList UniqueID='-1' ObjectType='-1' MultiSelection='0' IsSystem='1' />
          </action>
        </ChooseFromListCollection>
        <DataBrowser BrowseBy='' />
        <Settings MatrixUID='' Enabled='0' EnableRowFormat='0' />
        <items>
          <action type='group'>
            <item uid='FLD_GENRL' />
            <item uid='FLD_FIELDS' />
            <item uid='FLD_COMP' />
          </action>
        </items>
      </form>
    </action>
  </forms>
</Application>";
                    break;

                case (BoSuppLangs.ln_Spanish_La):
                    xml = "";
                    break;
            }
            return xml;
        }

        private static void FormSetup(Form pForm)
        {
            PictureBox IMG_Main = null;
            EditText TextBox_ID = null;
            try
            {
                pForm.Freeze(true);

                pForm.Mode = BoFormMode.fm_ADD_MODE;

                pForm.VisibleEx = true;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(IMG_Main);
                NSC_DI.UTIL.Misc.KillObject(TextBox_ID);
                GC.Collect();
            }
        }

        public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
        {
            Form oForm = null;
            Item oItm = null;
            try
            {
                string formStr = FormXML();
                formStr = formStr.Replace("(NSC_FORM)", cFormID);
                oForm = CommonUI.Forms.LoadXML(formStr);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
                {
                    oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
                    oItm.Top = -20;
                    oItm.Specific.Value = pCallingFormUID;
                }

                FormSetup(oForm);
                return oForm;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return null;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        public static void Form_Load_Find(string SampleId, string BatchNumber)
        {
            Form oForm = null;
            Recordset oRecordSet = null;

            try
            {
                oForm = FormCreate();
                oForm.Freeze(true);
                oForm.Mode = BoFormMode.fm_FIND_MODE;

                //Disable Text Fields
                oForm.Items.Item("TXT_ID").Enabled      = false;
                oForm.Items.Item("TXT_NAME").Enabled    = false;
                oForm.Items.Item("TXT_QTY").Enabled     = false;
                oForm.Items.Item("TXT_BATCH").Enabled   = false;
                oForm.Items.Item("TXT_WHRS").Enabled    = false;
                oForm.Items.Item("TXT_WHRSC").Enabled   = false;

				LoadSampleGeneralInformation(oForm, SampleId, BatchNumber);

				Load_Tests_Matrix(oForm);
				Load_TestResult_Matrix(oForm);
				LoadTestCompletionCount(oForm);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oForm);
                NSC_DI.UTIL.Misc.KillObject(oRecordSet);
                GC.Collect();
            }
        }

		#region Sample Tests Actions
		private static void HandleCheckEvent(Form pForm)
		{

			EditText TXT_RTESTC = FormControl.GetControlFromForm(FormControl.FormControlTypes.EditText, "TXT_RTESTC", pForm) as EditText;

			string TestCode = TXT_RTESTC.Value;

			if (string.IsNullOrEmpty(TestCode))
			{
				return;
			}

			CheckBox CHK_COMP = FormControl.GetControlFromForm(FormControl.FormControlTypes.CheckBox, "CHK_COMP", pForm);

			string SampleCode = pForm.Items.Item("TXT_BATCH").Specific.Value;
			NSC_DI.SAP.Test.UpdateCompletionStatus(TestCode, SampleCode, CHK_COMP.Checked);

			// Update our Completed Test Display.
			LoadTestCompletionCount(pForm);

			// Reload the Matrix since we now are setting columns to uneditable when it is finished.
			Load_TestResult_Matrix(pForm, TestCode);
		}

		private static void AddTestToSample(Form pForm, string testCode)
		{

			// Grab the matrix from the form UI
			SAPbouiCOM.Matrix MTX_TEST = FormControl.GetControlFromForm(FormControl.FormControlTypes.Matrix, "MTX_TEST", pForm) as SAPbouiCOM.Matrix;

			if (string.IsNullOrEmpty(testCode))
			{
				Globals.oApp.StatusBar.SetText("Please select an Test to add.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
				throw new System.ComponentModel.WarningException();
			}

			// Validate that we aren't adding the same Test Twice.
			Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as Recordset;

			string sqlQuery = string.Format(@"SELECT Top 1 [U_SampleCode] FROM [@"+ NSC_DI.Globals.tTestSamp+@"] WHERE [U_SampleCode] = '{0}'
AND [U_TestCode] = {1}", pForm.Items.Item("TXT_BATCH").Specific.Value, testCode);

			// Count how many current records exist within the database.
			oRecordSet.DoQuery(sqlQuery);

			if (oRecordSet.RecordCount > 0)
			{
				Globals.oApp.StatusBar.SetText("Test already exists on this Sample", BoMessageTime.bmt_Short);
				throw new System.ComponentModel.WarningException();
			}

			// Count how many current records exist within the database.
			oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tTestSamp + @"]");

			// Set the textbox for the ID to the pre-determined number.
			string nextCode = oRecordSet.Fields.Item(0).Value.ToString();

			NSC_DI.SAP.Test.CreateSampleRelation(Code: nextCode, TestCode: testCode, SampleId: pForm.Items.Item("TXT_BATCH").Specific.Value);
			Load_Tests_Matrix(pForm);
			LoadTestCompletionCount(pForm);
		}

		private static void BTN_RemoveTestFromSample_Click(Form pForm)
		{
			// Grab the matrix from the form UI
			SAPbouiCOM.Matrix MTX_TEST = FormControl.GetControlFromForm(FormControl.FormControlTypes.Matrix, "MTX_TEST", pForm) as SAPbouiCOM.Matrix;

			string selectedTestCode = "";

			// For each row already selected
			for (int i = 1; i < (MTX_TEST.RowCount + 1); i++)
			{
				if (MTX_TEST.IsRowSelected(i))
				{
					try
					{
						selectedTestCode = ((EditText)MTX_TEST.Columns.Item(0).Cells.Item(i).Specific).Value;
					}
					catch { }
				}
			}

			NSC_DI.SAP.Test.RemoveSampleRelation(TestCode: selectedTestCode, SampleCode: pForm.Items.Item("TXT_BATCH").Specific.Value);
			Load_Tests_Matrix(pForm);
			LoadTestCompletionCount(pForm);
		}

		private void Load_Tests_Matrix(Form pForm)
		{
			string sqlQuery = string.Format(@"
SELECT DISTINCT [@" + NSC_DI.Globals.tTests + @"]CODE AS [Code], [@" + NSC_DI.Globals.tTests + @"].[U_Name] AS [Name]
  FROM [@VSC_TEST_SAMPLE]
 INNER JOIN [@" + NSC_DI.Globals.tTests + @"] ON [@" + NSC_DI.Globals.tTests + @"].Code = [@" + NSC_DI.Globals.tTestSamp + @"].[U_TestCode]
 WHERE [@" + NSC_DI.Globals.tTestSamp + @"].[U_SampleCode] = '{0}'", pForm.Items.Item("TXT_BATCH").Specific.Value.ToString());


			Matrix.LoadDatabaseDataIntoMatrix(pForm, NSC_DI.Globals.tTestSamp, "MTX_TEST", new List<Matrix.MatrixColumn>() { 
                        new Matrix.MatrixColumn() { Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON } 
                        , new Matrix.MatrixColumn() { Caption = "Name", ColumnName = "Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                    }, sqlQuery);
		}
		#endregion

		#region Custom Choose From Lists
		private static void HandleTestSelectionCFL(Form pForm)
		{

			// Specifying the columns we want the Superior CFL to have.
			var MatrixColumns = new List<Matrix.MatrixColumn>()
            {
                 new Matrix.MatrixColumn(){ Caption="Code", ColumnWidth=80, ColumnName="Code", ItemType = BoFormItemTypes.it_EDIT}               
                ,new Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=40, ColumnName="Name", ItemType= BoFormItemTypes.it_EDIT}
            };

			//int cflWidth = Math.Max(450, clfHelper.MatrixColumns.Count * 150);
			//int cflHeight = 420;

			//// Max Width is 80% of the users screen space.
			//clfHelper.FormSettings.Width = Math.Min((int)(_SBO_Application.Desktop.Width * 0.8), cflWidth);
			//clfHelper.FormSettings.Height = Math.Min((int)(_SBO_Application.Desktop.Height * 0.8), cflHeight);

			//clfHelper.FormSettings.Top = (_SBO_Application.Desktop.Height - clfHelper.FormSettings.Height) / 2;
			//clfHelper.FormSettings.Left = (_SBO_Application.Desktop.Width - clfHelper.FormSettings.Width) / 2;

			var SQLQuery = "SELECT Code, [U_Name] AS [Name] FROM [@" + NSC_DI.Globals.tTests + "]";

			var CFL = F_CFL.FormCreate("Test Choose From List", SQLQuery, MatrixColumns);

			if (F_CFL.rowsSelected.Count > 0) AddTestToSample(pForm, F_CFL.rowsSelected[1]["Code"]);
			CFL.Close();
		}

		private static void HandleTestResultSelectionCFL(Form pForm)
		{
			var testCode = "";

			// Specifying the columns we want the Superior CFL to have.
			var MatrixColumns = new List<Matrix.MatrixColumn>()
            {
                 new Matrix.MatrixColumn(){ Caption="Code", ColumnWidth=80, ColumnName="Code", ItemType = BoFormItemTypes.it_EDIT}               
                ,new Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=40, ColumnName="Name", ItemType= BoFormItemTypes.it_EDIT}
            };

			//int cflWidth = Math.Max(450, clfHelper.MatrixColumns.Count * 150);
			//int cflHeight = 420;

			//clfHelper.FormSettings = new ChooseFromListHelper.FormControlSettings();

			//// Max Width is 80% of the users screen space.
			//clfHelper.FormSettings.Width = Math.Min((int)(_SBO_Application.Desktop.Width * 0.8), cflWidth);
			//clfHelper.FormSettings.Height = Math.Min((int)(_SBO_Application.Desktop.Height * 0.8), cflHeight);

			//clfHelper.FormSettings.Top = (_SBO_Application.Desktop.Height - clfHelper.FormSettings.Height) / 2;
			//clfHelper.FormSettings.Left = (_SBO_Application.Desktop.Width - clfHelper.FormSettings.Width) / 2;

			// And lastly the Query to populate the Matrix.
			var SQLQuery = string.Format(@"
SELECT [@" + NSC_DI.Globals.tTests + @"].Code, [@" + NSC_DI.Globals.tTests + @"].[U_Name] AS [Name]
  FROM [@" + NSC_DI.Globals.tTests + @"]
  JOIN [@" + NSC_DI.Globals.tTestSamp + @"] ON [@" + NSC_DI.Globals.tTestSamp + @"].[U_SampleCode] = '{0}'
   AND [@" + NSC_DI.Globals.tTestSamp + @"].[U_TestCode] = [@" + NSC_DI.Globals.tTests + @"].Code", pForm.Items.Item("TXT_BATCH").Specific.Value);

			var CFL = F_CFL.FormCreate("Test Choose From List", SQLQuery, MatrixColumns);

			if (F_CFL.rowsSelected.Count > 0)
			{

			testCode = F_CFL.rowsSelected[1]["Code"];
			pForm.Items.Item("TXT_RTESTC").Specific.Value = testCode;
			pForm.Items.Item("TXT_RTEST").Specific.Value = F_CFL.rowsSelected[1]["Name"];

			string sqlQuery = string.Format(@"SELECT [U_IsComplete] FROM [@" + NSC_DI.Globals.tTestSamp + @"] WHERE [U_SampleCode] = '{0}' 
AND [U_TestCode]= '{1}'", pForm.Items.Item("TXT_BATCH").Specific.Value, testCode);

			Recordset oRecordSet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
			oRecordSet.DoQuery(sqlQuery);

			pForm.Freeze(true);
			try
			{
				pForm.Items.Item("TXT_RTESTC").Specific.Checked = oRecordSet.Fields.Item(0).Value.Equals("0");
			}
			catch (Exception e)
			{
			}
			}


			Load_TestResult_Matrix(pForm, testCode);

			pForm.Freeze(false);
	
			CFL.Close();
		}
		#endregion

		#region Sample Test Results Actions
		private static void Load_TestResult_Matrix(Form pForm, string testCode = "")
		{

			string testSampleBatch = pForm.Items.Item("TXT_BATCH").Specific.Value;
			string labTestId = pForm.Items.Item("txtSample").Specific.Value;

			string sqlQueryForCode = string.Format(@"
SELECT TestSample.Code
FROM [@" + NSC_DI.Globals.tTestSamp + @"] As TestSample
JOIN [OBTN] ON [OBTN].DistNumber = TestSample.[U_SampleCode]
WHERE TestSample.[U_SampleCode] = '{0}'
	AND TestSample.[U_TestCode] = '{1}'
    AND [OBTN].[U_NSC_LabTestID] = '{2}'", testSampleBatch, testCode, labTestId);

			// Prepare to run a SQL statement.
			Recordset oRecordSet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
			oRecordSet.DoQuery(sqlQueryForCode);

			string testSampleCode = "0";
			if (oRecordSet.RecordCount > 0)
			{
				testSampleCode = oRecordSet.Fields.Item(0).Value.ToString();
			}

			// Defaulting to editable test results.
			bool setEditable = true;

			// Hideous Query.. To get all Compounds and Test Fields that can contain values.
			string sqlQuery = @"
SELECT [ID], [Name], [Value], [Type]
FROM (

SELECT
{Compound}.[Code] AS [ID]
, {Compound}.{CompoundName} AS [Name]
, ( SELECT TOP 1 {TestCompoundResults}.{TestCompoundResultValue} 
    FROM {TestCompoundResults} 
    WHERE {TestCompoundResults}.{TestCompoundResultCompoundCode} = {Compound}.[Code] 
    AND {TestCompoundResults}.{TestFieldResultsTestSampleCode} = '{TestSampleCodeToSearch}'
) AS [Value]
, 'Compound' AS [Type]
FROM {Compound}
JOIN {TestCompound} ON {TestCompound}.{TestCompoundCode}  = {Compound}.[Code]
JOIN [OBTN] ON [OBTN].DistNumber = '{TestSampleBatchToSearch}'
WHERE {TestCompound}.{TestCompoundTestCode} = '{TestCodeToSearch}' 
    AND [OBTN].[U_NSC_LabTestID] = '{LabTestID}'

UNION ALL

SELECT
{TestField}.[Code] AS [ID]
, {TestField}.{TestFieldName} AS [Name]
, ( SELECT TOP 1 {TestFieldResults}.{TestFieldResultsValue} 
    FROM {TestFieldResults} 
    WHERE {TestFieldResults}.{TestFieldResultsFieldCode} = {TestField}.[Code] 
    AND {TestFieldResults}.{TestFieldResultsTestSampleCode} = '{TestSampleCodeToSearch}'
) AS [Value]
, 'Field' AS [Type]
FROM {TestField} 
JOIN {Test_TestTestField} ON {Test_TestTestField}.{Test_TestFieldCode} = {TestField}.[Code]
JOIN [OBTN] ON [OBTN].DistNumber = '{TestSampleBatchToSearch}'
WHERE {Test_TestTestField}.{Test_TestFieldTestCode} = '{TestCodeToSearch}' 
    AND [OBTN].[U_NSC_LabTestID] = '{LabTestID}'
) AS [TestTable]";


			if (string.IsNullOrEmpty(testCode))
			{
				sqlQuery = string.Format(@"
SELECT * 
FROM 
(
    SELECT 
    '9999ABCDEF' AS [ID]
    ,'99999ABCDEFGHIJKL' AS [Name]
    ,'999999' AS [Value]
    ,'AB' AS [Type] 
) AS [TestTable]");
			}
			else
			{
				// So many joins..
				sqlQuery = ConvertToSQLQuery(sqlQuery, new Dictionary<string, string>()
                {
                   {"{Compound}", "[@" + NSC_DI.Globals.tTests + "]"},
                   {"{CompoundName}", "U_Name"},
                   {"{TestSample}", "[@" + NSC_DI.Globals.tTestSamp + "]"},
                   {"{TestSampleTestCode}", TestAndSample.SQLTestCode},
                   {"{TestCompound}", TestAndCompound.SQLTableName},
                   {"{TestCompoundTestCode}", TestAndCompound.SQLTestCode},
                   {"{TestCompoundCode}", TestAndCompound.SQLCompoundCode},
                   {"{TestSampleCode}", TestAndSample.SQLSampleCode},
                   {"{TestField}", TestField.SQLTableName},
                   {"{TestFieldName}", TestField.SQLName},
                   {"{Test_TestTestField}", TestAndTestField.SQLTableName},
                   {"{Test_TestFieldCode}", TestAndTestField.SQLTestFieldCode},
                   {"{Test_TestFieldTestCode}", TestAndTestField.SQLTestCode},
                   {"{TestCompoundResults}", TestCompoundResult.SQLTableName},
                   {"{TestCompoundResultValue}", TestCompoundResult.SQLValue},
                   {"{TestCompoundResultCompoundCode}", TestCompoundResult.SQLCompoundCode},
                   {"{TestFieldResults}", TestFieldResult.SQLTableName},
                   {"{TestFieldResultsValue}", TestFieldResult.SQLValue},
                   {"{TestFieldResultsFieldCode}", TestFieldResult.SQLFieldCode},
                   {"{TestFieldResultsTestSampleCode}", TestFieldResult.SQLTestSampleCode},
                   {"{TestCodeToSearch}", testCode},
                   {"{TestSampleBatchToSearch}", testSampleBatch}, 
                   {"{TestSampleCodeToSearch}", testSampleCode}, 
                   {"{LabTestID}", labTestId}
                });

				string testCompleteQueryCheck = string.Format(@"
SELECT [U_IsComplete]
FROM [@" + NSC_DI.Globals.tTestSamp + @"]
WHERE  [U_SampleCode]= '{0}'
    AND [U_TestCode] = {1}
	AND [U_IsComplete] = '0'", pForm.Items.Item("TXT_BATCH").Specific.Value, testCode);

				oRecordSet.DoQuery(testCompleteQueryCheck);

				bool isComplete = oRecordSet.Fields.Item(0).Value.ToString() == "0" ? true : false;

				if (isComplete)
				{
					setEditable = false;
				}
			}

			VirSci_Helper_Matrix.LoadDatabaseDataIntoMatrix(
					DatabaseTableName: "TestTable",
					MatrixUID: "MTX_TRSLT",
					ListOfColumns: new List<Matrix.MatrixColumn>() { 
                        new Matrix.MatrixColumn() { Caption = "ID", ColumnName = "ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                        , new Matrix.MatrixColumn() { Caption = "Field", ColumnName = "Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                        , new Matrix.MatrixColumn() { Caption = "Value", ColumnName = "Value", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT, IsEditable = setEditable }
                       , new Matrix.MatrixColumn() { Caption = "Type", ColumnName = "Type", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                    },
					SQLQuery: sqlQuery);

			if (string.IsNullOrEmpty(testCode))
			{
				// Remove the first item in the Matrix
				DataTable DataTable = pForm.DataSources.DataTables.Item("TestTable");
				DataTable.Rows.Remove(0);

				SAPbouiCOM.Matrix MTX_TRSLT = FormControl.GetControlFromForm(FormControl.FormControlTypes.Matrix, "MTX_TRSLT", pForm) as SAPbouiCOM.Matrix;

				MTX_TRSLT.LoadFromDataSource();
			}

		}

		#endregion

		private void BTN_SAVE_TEST_Click()
		{
			if (ControllerGeneralControls == null)
			{
				ControllerGeneralControls = new GeneralTabControls(pForm, _VirSci_Helper_Form);
				ControllerGeneralControls.LoadControls();
			}

			EditText TXT_RTESTC = FormControl.GetControlFromForm(FormControl.FormControlTypes.EditText, "TXT_RTESTC", pForm) as EditText;

			string testCode = TXT_RTESTC.Value;
			string testSampleBatch = pForm.Items.Item("TXT_BATCH").Specific.Value;
			string labTestId = ControllerGeneralControls.txtStateSampleID.Value;

			VirSci_SAP.Controllers.Test controllerTest = new Controllers.Test(_SBO_Application, Globals.oCompany, pForm);

			// Prepare to run a SQL statement.
			Recordset oRecordSet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

			#region Get the TestSample Code By the SampleId and TestCode
			string testSampleCodeQuery = string.Format(@"
SELECT CODE
FROM [@" + NSC_DI.Globals.tTestSamp + @"]
JOIN [OBTN] ON [OBTN].DistNumber = [U_SampleCode]
WHERE [U_SampleCode] = '{0}'
    AND [U_TestCode] = '{1}'
    AND [OBTN].[U_NSC_LabTestID] = '{2}'", testSampleBatch, testCode, labTestId);

			oRecordSet.DoQuery(testSampleCodeQuery);

			string testSampleCode = oRecordSet.Fields.Item(0).Value.ToString();
			#endregion

			SAPbouiCOM.Matrix MTX_TRSLT = FormControl.GetControlFromForm(FormControl.FormControlTypes.Matrix, "MTX_TRSLT", pForm) as SAPbouiCOM.Matrix;

			bool allSucceeded = true;
			// For each row already selected
			for (int i = 1; i < (MTX_TRSLT.RowCount + 1); i++)
			{
				try
				{
					string id = ((EditText)MTX_TRSLT.Columns.Item(0).Cells.Item(i).Specific).Value;
					string fieldName = ((EditText)MTX_TRSLT.Columns.Item(1).Cells.Item(i).Specific).Value;
					string value = ((EditText)MTX_TRSLT.Columns.Item(2).Cells.Item(i).Specific).Value;
					string fieldType = ((EditText)MTX_TRSLT.Columns.Item(3).Cells.Item(i).Specific).Value;

					if (fieldType == "Field")
					{
						string sqlQuery = string.Format(@"
SELECT  [@" + NSC_DI.Globals.tTFResult + @"].Code
FROM [@" + NSC_DI.Globals.tTFResult + @"]
JOIN [@" + NSC_DI.Globals.tTestSamp + @"] ON [@VSC_TEST_SAMPLE].Code = [@" + NSC_DI.Globals.tTFResult + @"].[U_TestSampleCode]
JOIN [OBTN] ON [OBTN].DistNumber = [@" + NSC_DI.Globals.tTestSamp + @"].[U_SampleCode]
WHERE [U_TestSampleCode] = '{1}'
    AND [U_FieldCode] = '{2}'
    AND [OBTN].[U_NSC_LabTestID] = '{3}'", testSampleBatch, testSampleCode, id, labTestId);

						oRecordSet.DoQuery(sqlQuery);

						if (oRecordSet.RecordCount == 0)
						{
							// Count how many current records exist within the database.
							oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tTFResult + "]");

							// Set the textbox for the ID to the pre-determined number.
							string nextCode = oRecordSet.Fields.Item(0).Value.ToString();

							NSC_DI.SAP.Test.CreateTestFieldResult(nextCode, testSampleCode, id, value);
						}
						else
						{
							NSC_DI.SAP.Test.UpdateTestFieldResult(testSampleCode, id, value);
						}
					}
					else if (fieldType == "Compound")
					{
						string sqlQuery = string.Format(@"
SELECT  [@VSC_TC_RESULT].Code
FROM [@VSC_TC_RESULT]
JOIN [@" + NSC_DI.Globals.tTestSamp + @"] ON [@" + NSC_DI.Globals.tTestSamp + @"].Code = [@" + NSC_DI.Globals.tTCResult + @"].[U_TestSampleCode]
JOIN [OBTN] ON [OBTN].DistNumber = [@" + NSC_DI.Globals.tTestSamp + @"].[U_SampleCode]
WHERE [U_TestSampleCode] = '{1}'
    AND [U_CompoundCode] = '{2}'
    AND [OBTN].[U_NSC_LabTestID] = '{3}'", testSampleBatch, testSampleCode, id, labTestId);

						oRecordSet.DoQuery(sqlQuery);

						if (oRecordSet.RecordCount == 0)
						{
							// Count how many current records exist within the database.
							oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tTestSamp + "]");

							// Set the textbox for the ID to the pre-determined number.
							string nextCode = oRecordSet.Fields.Item(0).Value.ToString();

							NSC_DI.SAP.Test.CreateTestCompoundResult(nextCode, testSampleCode, id, value);
						}
						else
						{
							NSC_DI.SAP.Test.UpdateTestCompoundResult(testSampleCode, id, value);
						}
					}

					if (!NSC_DI.SAP.Test.WasSuccessful)
					{
						allSucceeded = false;
					}
				}
				catch { }
			}

			if (allSucceeded)
			{
				Globals.oApp.StatusBar.SetText("Saved all Fields for Test!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
			}
			else
			{
				Globals.oApp.StatusBar.SetText("Some of the results are not saved correctly!", BoMessageTime.bmt_Short);
			}
		}

		private void BTN_IMPORT_Click()
		{
			// Ask the user on another thread the select an image file
			Thread thread_new = new Thread(new ThreadStart(AskForFile));
			thread_new.SetApartmentState(ApartmentState.STA);
			thread_new.Start();

			// Wait until user has selected an image.
			while (thread_new.IsAlive)
			{
				Thread.Sleep(100);
			}

			try
			{
				// No Item was selected.
				if (string.IsNullOrEmpty(SelectedFileName))
					return;

			}
			catch (Exception e)
			{
				//TODO-LOG: Log an error.

				_SBO_Application.SetStatusBarMessage(
					Text: "Failed to load!",
					Seconds: BoMessageTime.bmt_Short,
					IsError: false);

				return;
			}

			_SBO_Application.SetStatusBarMessage(
				Text: "You selected the file '" + SelectedFileName + "'!",
				Seconds: BoMessageTime.bmt_Short,
				IsError: false);
		}

        private static void BTN_CheckStateTestsAPI()
        {
/*	// COMPLIANCE
			NSC_DI.API.TestResults helperTestResults = new NSC_DI.API.TestResults();

            VirSci_SAP.WAStateHelpers.TestResults.StateTestResults testResults = helperTestResults.CheckStateTestsAPI(
                stateSampleId: ControllerGeneralControls.txtStateSampleID.Value,
                sampleBatchNumber: pForm.Items.Item("TXT_BATCH").Specific.Value.ToString());

            testResults.PopulateDefaultTestsWithResults();

            if (testResults.FoundResults)
            {
                // If the test was found.. update the batched item with a QA status of passed or fail.
                // ALSO: Update the Mother_ID (Batch Item that was used for the sample to Pass or Fail)

                SAP_BusinessOne.Controllers.BatchItems controllerBatchItem = new SAP_BusinessOne.Controllers.BatchItems(
                    SAPBusinessOne_Application: _SBO_Application,
                    SAPBusinessOne_Company: Globals.oCompany);

                string sqlQuery = string.Format(@"
SELECT [U_VSC_MotherID]
FROM OBTN
WHERE OBTN.DistNumber = '{0}'", pForm.Items.Item("TXT_BATCH").Specific.Value.ToString());

                string sampleOriginatingBatch = "";

                SAPbobsCOM.IRecordset dbCommunication = (SAPbobsCOM.IRecordset)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                try
                {
                    dbCommunication.DoQuery(sqlQuery);
                    dbCommunication.MoveFirst();
                    if (dbCommunication.RecordCount >= 1)
                    {
                        sampleOriginatingBatch = dbCommunication.Fields.Item("TestId").Value.ToString();
                    }
                }
                catch (Exception ex)
                {
                    log.Error("SampleInfo - CheckAPITest: Failed to get originating batch number for sample");
                }

                if (testResults.TestingPassed == VirSci_SAP.WAStateHelpers.TestResults.TestResultState.Success)
                {
                    ControllerGeneralControls.txtPassedQA.Value = "Passed!";

                    controllerBatchItem.UpdateQATestResults(batchNumber: sampleOriginatingBatch,
                        newStatus: SAP_BusinessOne.Controllers.BatchItems.TestResultStatus.Passed);

                    controllerBatchItem.UpdateQATestResults(batchNumber: pForm.Items.Item("TXT_BATCH").Specific.Value.ToString(),
                        newStatus: SAP_BusinessOne.Controllers.BatchItems.TestResultStatus.Passed);

                }
                else if (testResults.TestingPassed == VirSci_SAP.WAStateHelpers.TestResults.TestResultState.Failed)
                {
                    ControllerGeneralControls.txtPassedQA.Value = "Failed!";

                    controllerBatchItem.UpdateQATestResults(batchNumber: sampleOriginatingBatch,
                        newStatus: SAP_BusinessOne.Controllers.BatchItems.TestResultStatus.Failed);

                    controllerBatchItem.UpdateQATestResults(batchNumber: pForm.Items.Item("TXT_BATCH").Specific.Value.ToString(),
                        newStatus: SAP_BusinessOne.Controllers.BatchItems.TestResultStatus.Failed);
                }

                ControllerGeneralControls.txtAPIMessage.Value = testResults.Message;
            }
            else
            {
                ControllerGeneralControls.txtAPIMessage.Value = testResults.Message;
                ControllerGeneralControls.txtPassedQA.Value = "Not Found";
            }
*/
        }

	    private static void LoadSampleGeneralInformation(Form pForm, string SampleId, string BatchNumber)
	    {
		    Recordset oRecordSet = null;
		    try
		    {

			    string sqlQuery = string.Format(@"
SELECT DISTINCT [OITM].ItemCode, [OITM].ItemName, [OBTQ].Quantity, [OBTN].DistNumber, [OWHS].WhsName, [OBTQ].WhsCode, [OBTN].[U_VSC_LabTestID] AS [StateSampleID], [OBTN].[U_VSC_PassedQA] AS [PassedQA]
  FROM [OITM] 
  JOIN [OITB] ON [OITB].[ItmsGrpCod] = [OITM].[ItmsGrpCod]
  JOIN [OBTN] ON [OBTN].[ItemCode] = [OITM].[ItemCode]
  JOIN [OBTQ] ON [OBTQ].[ItemCode] =  [OBTN].[ItemCode]
  JOIN [OWHS] ON [OWHS].[WhsCode] = [OBTQ].[WhsCode]
   AND [OBTQ].[SysNumber] = [OBTN].[SysNumber]
 WHERE [OITM].ItemCode = '{0}' AND [OBTN].DistNumber = '{1}'", SampleId, BatchNumber);

			    oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
			    oRecordSet.DoQuery(sqlQuery);

			    pForm.Items.Item("TXT_ID").Specific.Value	 = SampleId;
			    pForm.Items.Item("TXT_NAME").Specific.Value	 = oRecordSet.Fields.Item("ItemName").Value.ToString();
			    pForm.Items.Item("TXT_QTY").Specific.Value	 = oRecordSet.Fields.Item("Quantity").Value.ToString();
			    pForm.Items.Item("TXT_BATCH").Specific.Value = BatchNumber;
			    pForm.Items.Item("TXT_WHRS").Specific.Value	 = oRecordSet.Fields.Item("WhsName").Value.ToString();
			    pForm.Items.Item("TXT_WHRSC").Specific.Value = oRecordSet.Fields.Item("WhsCode").Value.ToString();

			    // COMPLIANCE
			    // Two new columns for the State API Complaincy.
			    pForm.Items.Item("txtSample").Specific.Value  = oRecordSet.Fields.Item("StateSampleID").Value.ToString();
			    pForm.Items.Item("txtTStatus").Specific.Value = oRecordSet.Fields.Item("PassedQA").Value.ToString();

			    BTN_CheckStateTestsAPI();
		    }
		    catch (Exception ex)
		    {
			    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    NSC_DI.UTIL.Misc.KillObject(oRecordSet);
			    GC.Collect();
		    }
	    }

		private void LoadTestCompletionCount()
		{
			if (ControllerGeneralControls == null)
			{
				ControllerGeneralControls = new GeneralTabControls(pForm, _VirSci_Helper_Form);
				ControllerGeneralControls.LoadControls();
			}

			string completeQuery = string.Format(@"
SELECT COUNT(*)
FROM [@" + NSC_DI.Globals.tTestSamp + @"]
WHERE [U_SampleCode] = '{0}'
	AND [U_IsComplete] = '0'", ControllerGeneralControls.txtSampleBatchNumber.Value);

			string incompleteQuery = string.Format(@"
SELECT COUNT(*) FROM [@" + NSC_DI.Globals.tTestSamp + @"]
WHERE [U_SampleCode] = '{0}'
  AND ([U_IsComplete] = '1' OR [U_IsComplete] IS NULL)", ControllerGeneralControls.txtSampleBatchNumber.Value);

			// Prepare to run a SQL statement.
			Recordset oRecordSet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
			oRecordSet.DoQuery(completeQuery);

			string complete = oRecordSet.Fields.Item(0).Value.ToString();

			oRecordSet.DoQuery(incompleteQuery);

			string incomplete = oRecordSet.Fields.Item(0).Value.ToString();
			int numberOfTotal = Int32.Parse(complete) + Int32.Parse(incomplete);

			EditText TXT_TCOM = FormControl.GetControlFromForm(FormControl.FormControlTypes.EditText, "TXT_TCOM", pForm);

			TXT_TCOM.Value = string.Format("{0} / {1} Completed", complete, numberOfTotal);
		}

		private string ConvertToSQLQuery(string Query, Dictionary<string, string> parameters)
		{
			foreach (KeyValuePair<string, string> param in parameters)
			{
				Query = Query.Replace(param.Key, param.Value);
			}

			return Query;
		}
    }
}