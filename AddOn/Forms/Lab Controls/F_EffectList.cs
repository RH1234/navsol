﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms.Lab_Controls
{
    class F_EffectList: B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "NSC_EFFECT_LS";

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		public string FormUID { get; set; }
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}

		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "BTN_ADD":
					F_EffectInfo.FormCreate();
					break;

				case "BTN_FIND":
					this.Load_Matrix();
					break;

				case "BTN_IMPORT":
					BTN_IMPORT_Click();
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "mtx_plan":
					PLAN_MATRIX_LINK_PRESSED(pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		public F_EffectList() : this(Globals.oApp, Globals.oCompany) { }	// FOR OLD CODE

		public F_EffectList(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
			_SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

	    public void Form_Load()
	    {
		    try
		    {
			    _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                _SBO_Form.Items.Item("BTN_IMPORT").Visible = false;

			    // Set the main image
			    CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_Main", @"list-icon.bmp");

			    Load_Matrix();

			    //SAP_BusinessOne.Helpers.SQL VirSci_Helper_SQL = new SAP_BusinessOne.Helpers.SQL(SAPBusinessOne_Company: Globals.oCompany);

//                SAPbobsCOM.Fields ResultFromSQLQuery = VirSci_Helper_SQL.GetFieldsFromSQLQuery(@"
//SELECT COUNT(*) 
//FROM [@" + Globals.SAP_PartnerCode + "_" + NavSol.Models.Compound.TableName + "]");

//                // Load the total count of Effects.
//                ((SAPbouiCOM.EditText)Helpers.Forms.GetControlFromForm(Helpers.Forms.FormControlTypes.EditText, "txt_total", FormToSearchIn: _SBO_Form)).Value = ResultFromSQLQuery.Item(0).Value.ToString();

//                ResultFromSQLQuery = VirSci_Helper_SQL.GetFieldsFromSQLQuery(@"
//SELECT COUNT(*) 
//FROM [@" + Globals.SAP_PartnerCode + "_" + NavSol.Models.Compound.TableName + @"]
//WHERE [CreateDate] >= CONVERT(DATE,'1/1/" + DateTime.Now.Year + @"')
//                ");

//                // Load the total count of Effects.
//                ((SAPbouiCOM.EditText)Helpers.Forms.GetControlFromForm(Helpers.Forms.FormControlTypes.EditText, "txt_crnt", FormToSearchIn: _SBO_Form)).Value = ResultFromSQLQuery.Item(0).Value.ToString();

		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
				_SBO_Form.Freeze(false);
				_SBO_Form.VisibleEx = true;

				//NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

	    private void Load_Matrix()
	    {
		    try
		    {
			    EditText txt_name = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txt_name", _SBO_Form);

			    VERSCI.Matrix VirSci_Helper_Matrix = new VERSCI.Matrix(Globals.oApp, _SBO_Form);

			    if (txt_name.Value == "")
			    {
				    VirSci_Helper_Matrix.LoadDatabaseDataIntoMatrix("@" + NSC_DI.Globals.tEffect, "mtx_plan", new List<VERSCI.Matrix.MatrixColumn>()
				    {new VERSCI.Matrix.MatrixColumn() {Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
					    new VERSCI.Matrix.MatrixColumn(){Caption = "Name",ColumnName = "U_Name",ColumnWidth = 80,ItemType = BoFormItemTypes.it_EDIT}
				    }, "SELECT * FROM [@" + NSC_DI.Globals.tEffect + @"] ORDER BY [Code] DESC");
			    }
			    else
			    {
				    VirSci_Helper_Matrix.LoadDatabaseDataIntoMatrix("@" + NSC_DI.Globals.tEffect, "mtx_plan", new List<VERSCI.Matrix.MatrixColumn>()
				    {
					    new VERSCI.Matrix.MatrixColumn() {Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
					    new VERSCI.Matrix.MatrixColumn(){Caption = "Name",ColumnName = "U_Name",ColumnWidth = 80,ItemType = BoFormItemTypes.it_EDIT}},
					    "SELECT * FROM [@" + NSC_DI.Globals.tEffect + @"] WHERE [U_Name] LIKE '%" + txt_name.Value.Replace("'", "''") +
					    @"%' ORDER BY [Code] DESC ");
			    }
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    //NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

	    private void BTN_IMPORT_Click()
        {
            /*
            // Ask the user on another thread the select an image file
            Thread thread_new = new Thread(new ThreadStart(AskForFile));
            thread_new.SetApartmentState(ApartmentState.STA);
            thread_new.Start();

            // Wait until user has selected a CSV file to import.
            while (thread_new.IsAlive)
            {
                Thread.Sleep(100);
            }

            List<CSV_Effect> ListOfEffects = new List<CSV_Effect>();

            using (TextReader TextReader_Items = File.OpenText(SelectedFileName))
            {
                TextReader_Items.Read();

                CsvHelper.Configuration.CsvConfiguration CSV_Config = new CsvHelper.Configuration.CsvConfiguration();
                CSV_Config.HasHeaderRecord = true;
                CSV_Config.IgnoreHeaderWhiteSpace = true;
                CSV_Config.TrimHeaders = false;

                // Prepare a new CSV Reader object
                CsvHelper.CsvReader CSV_Reader_Items = new CsvHelper.CsvReader(TextReader_Items, CSV_Config);
                CSV_Reader_Items.Configuration.AutoMap<CSV_Effect>();

                // Count the total number of records we are creating
                ListOfEffects = CSV_Reader_Items.GetRecords<CSV_Effect>().ToList();
            }

            // Create a Progress Bar
            oProgBar = Globals.oApp.StatusBar.CreateProgressBar("Starting to create items.", ListOfEffects.Count, false);

            foreach (CSV_Effect Effect in ListOfEffects)
            {
                // Prepare to run a SQL statement.
                SAPbobsCOM.SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + Globals.SAP_PartnerCode + "_" + NavSol.Models.Effect.TableName + "] ");

                // The single digigt plant type is "S", "H", or "I".  For "Sativa", "Hybrid", and "Indica" respectivley.
                string PlantTypeSingleDigitCode = Effect.Type;
                string NewEffectID = oRecordSet.Fields.Item(0).Value.ToString();
                string EffectName = Effect.Name;
                string EffectType = "";

                switch (Effect.Type)
                {
                    case "I":
                        EffectType = "Indica";
                        break;
                    case "S":
                        EffectType = "Sativa";
                        break;
                    case "H":
                        EffectType = "Hybrid";
                        break;
                }

                // Prepare a Effect Controller
                NSCNSC_DI.SAP._DI.UTIL.Misc.KillObject(oForm);Effect VirSci_Controllers_Effect = new Controllers.Effect(SAPBusinessOne_Application: Globals.oApp, SAPBusinessOne_Company: Globals.oCompany, SAPBusinessOne_Form: _SBO_Form);

                // Create Effect UDO
                VirSci_Controllers_Effect.CreateEffect(Code: NewEffectID, Name: EffectName, Type: EffectType);

                // Attempt to add the new Effect items
                VirSci_Controllers_Effect.AddItemsToEffect(PlantTypeSingleDigitCode: PlantTypeSingleDigitCode, EffectID: NewEffectID, EffectName: EffectName, EffectType: EffectType);

                oProgBar.Text = "Effect " + EffectName + " created!";

                oProgBar.Value += 1;
            }

            oProgBar.Stop();
             */
        }

		private void PLAN_MATRIX_LINK_PRESSED(ItemEvent SAP_UI_ItemEvent)
		{
			// Attempt to grab the selected row ID
			int SelectedRowID = SAP_UI_ItemEvent.Row;

			if (SelectedRowID <= 0) return;

			try
			{
				// Which column stores the ID
				int ColumnIDForIDOfItemSelected = 0;

				string ItemSelected = _SBO_Form.Items.Item("mtx_plan").Specific.GetCellSpecific(ColumnIDForIDOfItemSelected, SelectedRowID).Value;

				F_EffectInfo.Form_Load_Find(ItemSelected);
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
		}

        public class CSV_Effect
        {
            public string Name { get; set; }
            public string Type { get; set; }
        }

        /// <summary>
        /// Stores the selected file from the seperate thread
        /// </summary>
        private string SelectedFileName;
        private void AskForFile()
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();

            var y = ofd.ShowDialog();

            SelectedFileName = ofd.FileName;
        }
    }
}