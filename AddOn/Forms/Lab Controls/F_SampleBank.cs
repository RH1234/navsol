﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using Matrix = NavSol.VERSCI.Matrix;

namespace NavSol.Forms.Lab_Controls
{
    class F_SampleBank : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_SAMPLEBANK";
        private const string cItemType = "Sample";

        private Dictionary<string, int> MatrixColumns = new Dictionary<string, int>()
        {
            {"empty", 0},
            {"ItemCode", 1},
            {"ItemName", 2},
            {"BatchNum",   3},
            {"MnfSerial", 4},
            {"Quantity",  5},
            {"", 6},
            {"GrossWeigth", 7}, 
            {"WarehouseName",8},
            {"CreateDate", 9},
            {"TestID",10},
            {"SampledBatch",11},
            {"SampledItem",12},
            {"Qty",13},
            {"UOM",14},
            {"UnderlyingStateID",15},
            {"Warehouse",16},
            {"OverallPassed",17},
        };

        protected enum TestSampleCompletionStatus
		{
			Complete, Incomplete
		}

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		public string FormUID { get; set; }
		private NSC_DI.Globals.ItemGroupTypes itemGroupType = NSC_DI.Globals.ItemGroupTypes.Sample;
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
	    [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
	    public virtual bool OnBeforeItemPressed(ItemEvent pVal)
	    {
		    bool BubbleEvent = true;
		    Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
		    _SBO_Form = oForm;

		    switch (pVal.EventType)
		    {
			    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
				    switch (pVal.ItemUID)
				    {
					    case "TAB_INCMP":

                            _SBO_Form.DataSources.UserDataSources.Item("TAB_INCMP").Value = "Ye";
                            _SBO_Form.DataSources.UserDataSources.Item("TAB_CMPL").Value = "No";
                            this.Load_Matrix(oForm, TestSampleCompletionStatus.Incomplete);
						    break;
					    case "TAB_CMPL":
                            _SBO_Form.DataSources.UserDataSources.Item("TAB_INCMP").Value = "No";
                            _SBO_Form.DataSources.UserDataSources.Item("TAB_CMPL").Value = "Ye";
                            this.Load_Matrix(oForm, TestSampleCompletionStatus.Complete);
						    break;
				    }
				    break;
		    }

		    NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();

		    return BubbleEvent;
	    }
	    #endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            _SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "MTX_ITEMS":
					MAIN_MATRIX_LINK_PRESSED(oForm, pVal);
					break;
               
            }

			NSC_DI.UTIL.Misc.KillObject(oForm);
          

            GC.Collect();
		}

        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;
            // Grab the matrix from the form UI
            SAPbouiCOM.Matrix matrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", oForm) as SAPbouiCOM.Matrix;
            SAPbouiCOM.CheckBox oChk = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_STID", oForm) as SAPbouiCOM.CheckBox;
            SAPbouiCOM.Button oBTN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "BTN_LOOKUP", oForm) as SAPbouiCOM.Button;
            SAPbouiCOM.EditText oEdit = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", oForm) as SAPbouiCOM.EditText;
            SAPbouiCOM.EditText oEditSearch = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_SRCH", oForm) as SAPbouiCOM.EditText;
            SAPbouiCOM.ComboBox oSearch = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CBX_SRCH", _SBO_Form) as SAPbouiCOM.ComboBox;
            SAPbouiCOM.Folder oFolder = _SBO_Form.Items.Item("TAB_INCMP").Specific;

            switch (pVal.ItemUID)
            {
                case "TAB_CMPL":
                    // Set state ID field to not be editable 
                    matrix.Columns.Item("col_3").Editable = false;
                    _SBO_Form.DataSources.UserDataSources.Item("TAB_INCMP").Value = "N";
                    _SBO_Form.DataSources.UserDataSources.Item("TAB_CMPL").Value = "Y";                    
                    break;
                case "TAB_INCMP":
                    // Set state ID field to be editable 
                    matrix.Columns.Item("col_3").Editable = false;
                    _SBO_Form.DataSources.UserDataSources.Item("TAB_INCMP").Value = "Ye";
                    _SBO_Form.DataSources.UserDataSources.Item("TAB_CMPL").Value = "No";

                    break; 
                case "CHK_STID":

                    if (oChk.Checked == true)
                    {
                        if (_SBO_Form.DataSources.UserDataSources.Item("TAB_CMPL").Value == "Y")
                        {
                            Globals.oApp.StatusBar.SetText("You May Not Update State IDs on Completed Tests");
                            oChk.Checked = false;
                        }
                        else
                        {
                            oBTN.Caption = "Set State ID";
                            _SBO_Form.Items.Item("TXT_LOOKUP").Visible = true;
                            oEdit.Active = true;
                        }
                    }
                    else
                    {
                        oBTN.Caption = "Search";
                        try { oEdit.Active = false; }
                        catch
                        {
                            //do nothing the txt box is not visible 
                        }
                       
                        _SBO_Form.Items.Item("TXT_LOOKUP").Visible = false;
                        
                    }
                    break;
                case "CHK_SUB0":
                    if(_SBO_Form.DataSources.UserDataSources.Item("U_SUB0").Value == "Y")
                        _SBO_Form.DataSources.UserDataSources.Item("U_SUB0").Value = "Y";
                    else
                        _SBO_Form.DataSources.UserDataSources.Item("U_SUB0").Value = "N";

                    if(_SBO_Form.DataSources.UserDataSources.Item("TAB_INCMP").Value == "No")
                        Load_Matrix(oForm, TestSampleCompletionStatus.Complete);
                    else
                        Load_Matrix(oForm, TestSampleCompletionStatus.Incomplete);
                    break;
                case "BTN_LOOKUP":
                    if (oChk.Checked == true)
                    {//Assign a state ID
                        if (oEdit.Value.Length == 0)
                        {
                            Globals.oApp.StatusBar.SetText("No Data To Scan! - Please Enter A Valid Value.");
                            return;
                        }
                        else
                        {
                            AssignStateID(_SBO_Form, oEdit.Value);
                        }

                    }
                    else
                    {//Search For something
                        if (oEditSearch.Value.Length == 0 && oSearch.Value!="14")
                        {
                            Globals.oApp.StatusBar.SetText("No Data To Scan! - Please Enter A Valid Value.");
                            return;
                        }
                        else { Search(_SBO_Form, oEditSearch.Value); }
                    }
                        break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            NSC_DI.UTIL.Misc.KillObject(matrix);
            NSC_DI.UTIL.Misc.KillObject(oChk);
            NSC_DI.UTIL.Misc.KillObject(oBTN);
            NSC_DI.UTIL.Misc.KillObject(oEdit); 
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_FORM_RESIZE
        [B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
        public virtual void OnAfterFormResize(ItemEvent pVal)
        {
            if (pVal.InnerEvent) return;
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            ReSize_Form(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public F_SampleBank() : this(Globals.oApp, Globals.oCompany) { }

		public F_SampleBank(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            try
            {
                _VirSci_Helper_Form = new VERSCI.Forms();   // FOR OLD CODE

                _SBO_Application = SAPBusinessOne_Application;
                _SBO_Company = SAPBusinessOne_Company;
                //_VirSci_Helper_Form = new SAP_BusinessOne.Helpers.Forms(SAPBusinessOne_Application: _SBO_Application);
                //_VirSci_Helper_SQL = new SAP_BusinessOne.Helpers.SQL(Globals.oCompany);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }            
        }

        /// <summary>
        /// This function will allow calling forms to refresh any changes in MX data. 
        /// </summary>
        public void ReloadActiveMX()
        {
            try
            {
                _SBO_Form = CommonUI.Forms.GetActiveForm();
                //Identify active tab and click it. 
                Folder TAB_INCMP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_INCMP", this._SBO_Form);
                Folder TAB_CMPL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_CMPL", this._SBO_Form);

                string strComp = _SBO_Form.DataSources.UserDataSources.Item("TAB_CMPL").Value;
                string strIncomp = _SBO_Form.DataSources.UserDataSources.Item("TAB_INCMP").Value;
                if (strIncomp == "N" && strComp == "Y")
                {
                    //Complete Tab is selected.
                    TAB_CMPL.Select();
                }
                else
                {
                    TAB_INCMP.Select();
                }

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }          
        }

	    /// <summary>
	    /// Loads the Matrix with data with a given completion status
	    /// </summary>
	    /// <param name="status">Enumeration if the Test Sample is complete or not.</param>
	    private void Load_Matrix(Form pForm, TestSampleCompletionStatus status,string pSearch="")
	    {
            CommonSetting oColor = null;
            ProgressBar oBar = null;
            try
            {
                oBar = Globals.oApp.StatusBar.CreateProgressBar("Loading grid.", 0, false);
                pForm.Freeze(true);
			    // Prepare a Matrix helper
			    Matrix oMatrixHelper = new Matrix(Globals.oApp, _SBO_Form);

                string queryModifier = null;
                string queryModifier2 = null;
                // 11739 - Needed for setting the Results and Create dates for the Complete/Incomplete tabs
                string dateSQL = "SPACE(0)";                
                string dateCaption = "";
                string testRsltsJoin = "";
                bool displayDateCol = false;
                var testRsltsOverallPassedF = ",'' AS OverallPassed";
                var testRsltsOverallPassedL = "";

                SAPbouiCOM.CheckBox oSubZeros = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_SUB0", _SBO_Form) as SAPbouiCOM.CheckBox;

                switch (status)
                {
                    case TestSampleCompletionStatus.Complete:
                        queryModifier = "In";
                        if (oSubZeros.Checked == true)
                            queryModifier2 = "and OBTQ.Quantity > 0 and OBTQ_1.Quantity > 0 ";

                        if (NSC_DI.UTIL.Options.Value.GetSingle("Use Test Results Table for Sample Bank") == "Y")
                        {
                            dateSQL = "[@NSC_TEST_RESULTS].[U_ReleaseDate]";
                            dateCaption = "Results Recieved";
                            testRsltsJoin = "INNER JOIN [@NSC_TEST_RESULTS] ON OBTN.MnfSerial = [@NSC_TEST_RESULTS].[U_SourceStateID]"; // needed for getting results from TEST_RESULTS
                            displayDateCol = true;
                        }

                        break;
                    case TestSampleCompletionStatus.Incomplete:
                        queryModifier = "Not In";
                        if (oSubZeros.Checked == true)
                            queryModifier2 = "and OBTQ.Quantity > 0 and OBTQ_1.Quantity > 0";

                        dateSQL = "OBTN.InDate";                       
                        dateCaption = "Create Date";
                        displayDateCol = true;

                        if (NSC_DI.UTIL.Options.Value.GetSingle("Use Test Results Table for Sample Bank") == "Y")
                        {
                            testRsltsJoin = "  LEFT JOIN [@NSC_TEST_RESULTS] ON OBTN.MnfSerial = [@NSC_TEST_RESULTS].[U_SourceStateID]"; // needed for getting results from TEST_RESULTS
                            testRsltsOverallPassedF = ", [@NSC_TEST_RESULTS].U_OverallPassed AS OverallPassed";
                            testRsltsOverallPassedL = "OverallPassed";
                        }
                        break;
                }
                if(queryModifier2!=null) pSearch += queryModifier2;

                var onHand = (oSubZeros.Checked == true) ? " AND OBTQ.Quantity > 0 and OBTQ_1.Quantity > 0 " : "";
                //var Complete = (status == TestSampleCompletionStatus.Complete) ? "IN" : "NOT IN";
                var Complete = (status == TestSampleCompletionStatus.Complete) ? "Y' OR OBTN.U_NSC_PassedQA = 'F" : "N";

                // #10823 --->
                // limit the list to items in ths pecific branch
                var defBranch = Globals.BranchDflt;         //  -1 is no branches, 0 is no default branch
                var brList = Globals.BranchList;

                // #10823 <---

                // 11739 - Added the DISTINCT to handle the results that are pulled in from the SQL for the Complete tab
                var sql = $@"SELECT DISTINCT OBTN.ItemCode AS 'empty',
--DENSE_RANK() OVER(ORDER BY OBTN.DistNumber, [@NSC_TEST_SAMPLE].U_SampledBatch) AS #,
OBTN.ItemCode, OITM.ItemName, OBTQ.Quantity, OITM.InvntryUom AS UoM, OBTN.DistNumber, 
     OBTN.MnfSerial AS StateID, OBTN.[U_NSC_GrossWeight] AS GrossWeight, OBTQ.WhsCode, OWHS.WhsName AS WhsName, {dateSQL} AS CreateDate, OBTN.U_NSC_LabTestID AS TestID, 
     [@NSC_TEST_SAMPLE].U_SampledBatch AS UnderLot, OITM_1.ItemName AS UnderName, OBTQ_1.Quantity AS UnderQty, OITM_1.InvntryUom AS UnderUoM, 
     OBTN_1.MnfSerial AS UnderStateID, OBTQ_1.WhsCode AS UnderWhrs, OWHS_1.WhsName AS UnderWhrsName,
     [@NSC_TEST_SAMPLE].U_SampledItem AS UnderItem {testRsltsOverallPassedF}
FROM [@NSC_TEST_SAMPLE]
LEFT JOIN OBTN ON OBTN.DistNumber = [@NSC_TEST_SAMPLE].U_SampleCode 
INNER JOIN OITM ON OBTN.ItemCode = OITM.ItemCode 
INNER JOIN OBTQ ON OBTN.ItemCode = OBTQ.ItemCode AND OBTN.SysNumber = OBTQ.SysNumber 
INNER JOIN OWHS ON OWHS.WhsCode = OBTQ.WhsCode
{testRsltsJoin} -- // 11739 needed for the complete release date pull
LEFT JOIN OITM AS OITM_1 ON [@NSC_TEST_SAMPLE].U_SampledItem = OITM_1.ItemCode 
LEFT JOIN OBTN AS OBTN_1 ON OITM_1.ItemCode = OBTN_1.ItemCode AND OBTN_1.DistNumber = [@NSC_TEST_SAMPLE].U_SampledBatch
LEFT JOIN OBTQ AS OBTQ_1 ON OBTN_1.ItemCode = OBTQ_1.ItemCode AND OBTN_1.SysNumber = OBTQ_1.SysNumber 
LEFT JOIN OWHS AS OWHS_1 ON OWHS_1.WhsCode = OBTQ_1.WhsCode
WHERE OBTN.U_NSC_PassedQA = '{Complete}' {onHand}";

                if (defBranch >= 0) sql += $" AND OWHS.BPLid IN ({brList})";        // #10823
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm,
				    "OITM",
				    "MTX_ITEMS",
				    new List<CommonUI.Matrix.MatrixColumn>()
				    {
                        //new CommonUI.Matrix.MatrixColumn() {ColumnName = "#", Caption = "#", ColumnWidth = 10, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "empty", Caption = "w", ColumnWidth = 1, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "ItemCode", Caption = "Item Code", ColumnWidth = 40, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
					    new CommonUI.Matrix.MatrixColumn() {ColumnName = "ItemName", Caption = "Item Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
					    new CommonUI.Matrix.MatrixColumn() {ColumnName = "DistNumber", Caption = "Batch Number", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "StateID", Caption = "State ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "Quantity", Caption = "Quantity", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "UoM", Caption = "UoM", ColumnWidth = 20, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "GrossWeight", Caption="Gross Weight", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "WhsName", Caption = "Warehouse", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "CreateDate", Caption = dateCaption, ColumnWidth = 40, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "TestID", Caption = "Test ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "UnderLot", Caption = "Sampled Batch", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "UnderName", Caption = "Sampled Item", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "UnderQty", Caption = "Qty", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "UnderUoM", Caption = "UOM", ColumnWidth = 20, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "UnderStateID", Caption = "Underlying State ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "UnderWhrsName", Caption = "Warehouse", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "UnderItem", Caption = "Sampled Item Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {ColumnName = "OverallPassed", Caption = testRsltsOverallPassedL, ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT
                                                            , IsVisable = (testRsltsOverallPassedL != "")}

                    }, sql);

			    // Grab the matrix from the form UI
			    SAPbouiCOM.Matrix matrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", pForm) as SAPbouiCOM.Matrix;

                matrix.Columns.Item(matrix.Columns.Count-2).Visible = false;    // hide the under item code
                matrix.Columns.Item(MatrixColumns["CreateDate"]).Visible = displayDateCol;

                matrix.Columns.Item(matrix.Columns.Count - 1).Visible = (testRsltsOverallPassedL != "");    // hide  OverallPassed

                matrix.AutoResizeColumns();

                matrix.Columns.Item(0).Width = 1;

                // Allow multi-selection on the Matrix
                matrix.SelectionMode = BoMatrixSelect.ms_Single;

			    Load_TabCount(pForm);

                // high light the rows
                if (NSC_DI.UTIL.Options.Value.GetSingle("Use Test Results Table for Sample Bank") == "Y" && status == TestSampleCompletionStatus.Incomplete)
                {
                    int redColor = System.Drawing.Color.Red.R | (System.Drawing.Color.Red.G << 8) | (System.Drawing.Color.Red.B << 16);
                    int greenColor = System.Drawing.Color.GreenYellow.R | (System.Drawing.Color.GreenYellow.G << 8) | (System.Drawing.Color.GreenYellow.B << 16);
                    oColor = matrix.CommonSetting;
                    for (var i = 1; i <= matrix.RowCount; i++)
                    {
                        var val = matrix.Columns.Item(matrix.Columns.Count - 1).Cells.Item(i).Specific.Value;
                        if (val == "N") oColor.SetRowBackColor(i, redColor);
                        if (val == "Y") oColor.SetRowBackColor(i, greenColor);
                    }
                }
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
                pForm.Freeze(false);
                oBar.Stop();
                NSC_DI.UTIL.Misc.KillObject(oBar);
                NSC_DI.UTIL.Misc.KillObject(oColor);
                GC.Collect();
		    }
	    }

        /// <summary>
        /// This function takes a field and searched based on the slected column and the provided value. 
        /// </summary>
        /// <param name="pSearchValue"> Value to search for</param>
        private void Search(Form pForm, string pSearchValue)
        {
            try
            {
                //Determine what item was selected and set up the desired where clause
                SAPbouiCOM.ComboBox oSearch = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CBX_SRCH", _SBO_Form) as SAPbouiCOM.ComboBox;
                if (oSearch.Value.Length == 0) return;
                string sqlWhere = null;
                switch (oSearch.Value)
                {
                    case "0":
                        sqlWhere = " AND [OITM].ItemCode LIKE '%" + pSearchValue + "%'";
                        break;
                    case "1":
                        sqlWhere = " AND [OITM].ItemName LIKE'%" + pSearchValue + "%'";
                        break;
                    case "2":
                        sqlWhere = " AND [OBTN].DistNumber LIKE '%" + pSearchValue + "%'";
                        break;
                    case "3":
                        sqlWhere = " AND [OBTN].MnfSerial LIKE '%" + pSearchValue + "%'";
                        break;
                    case "4":
                        sqlWhere = " AND [OBTQ].Quantity=" + pSearchValue;
                        break;
                    case "5":
                        sqlWhere = " AND [OITM].InvntryUom ='" + pSearchValue + "'";
                        break;
                    case "6":
                        sqlWhere = " AND [OWHS].WhsName='" + pSearchValue + "'";
                        break;
                    case "7":
                        sqlWhere = " AND [OBTN].U_NSC_LabTestID='" + pSearchValue + "'";
                        break;
                    case "8":
                        sqlWhere = " AND OBTN_1.DistNumber='" + pSearchValue + "'";
                        break;
                    case "9":
                        sqlWhere = " AND OBTN_1.ItemName LIKE '%" + pSearchValue + "%'";
                        break;
                    case "10":
                        sqlWhere = " AND OBTQ_1.Quantity =" + pSearchValue;
                        break;
                    case "11":
                        sqlWhere = " AND OITM_1.InvntryUom ='" + pSearchValue + "'";
                        break;
                    case "12":
                        sqlWhere = " AND OBTN_1.MnfSerial='" + pSearchValue + "'";
                        break;
                    case "13":
                        sqlWhere = " AND OWHS_1.WhsName='" + pSearchValue + "'";
                        break;
                    case "14":
                        sqlWhere = " ";
                        break;
                }

                //Success Reload Matrix
                string strHolder = _SBO_Form.DataSources.UserDataSources.Item("TAB_INCMP").Value;

                if (strHolder == "Ye")               
                    this.Load_Matrix(pForm, TestSampleCompletionStatus.Incomplete, sqlWhere);              
                else               
                    this.Load_Matrix(pForm, TestSampleCompletionStatus.Complete, sqlWhere);
                
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void AssignStateID(Form pForm, string pStateID)
        {

            try
            {
                // Grab the matrix from the form UI
                SAPbouiCOM.Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", pForm) as SAPbouiCOM.Matrix;
                SAPbouiCOM.EditText oEditSearch = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", pForm) as SAPbouiCOM.EditText;
                Folder TAB_INCMP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_INCMP", pForm);
                Folder TAB_CMPL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_CMPL", pForm);

                bool bRowIsSelected = false;
                    int intSelectedRowCount = 0;
                    int intLastRowIndex = 0;
                    //Add Code to Validate Single row selection
                    for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
                    {
                        bRowIsSelected = MTX_MAIN.IsRowSelected(i);
                        if (bRowIsSelected == true)
                        {
                            intLastRowIndex = i;
                            intSelectedRowCount++;
                            bRowIsSelected = false;
                        }
                    }
                    if (intSelectedRowCount > 1)
                    {
                        //Show error message to indicate you can only select one row when assigning StateId
                        Globals.oApp.StatusBar.SetText("You may only select one row when assigning a StateID");
                        return;
                    }
                    else if (intSelectedRowCount == 0)
                    {
                        //Show error message to indicate you can only select one row when assigning StateId
                        Globals.oApp.StatusBar.SetText("You select a row when assigning a StateID");
                        return;
                    }
                    else
                    {
                    
                        //Check if ID already exist --, and (assign ID or Overwrite Existing)
                        string StateID = ((EditText)MTX_MAIN.Columns.Item("col_" + MatrixColumns["MnfSerial"].ToString()).Cells.Item(intLastRowIndex).Specific).Value;
                        string DistNumber = ((EditText)MTX_MAIN.Columns.Item("col_" + MatrixColumns["BatchNum"].ToString()).Cells.Item(intLastRowIndex).Specific).Value;
                        SAPbobsCOM.BatchNumberDetail oBatchDetails = null;
                        CompanyService oCoService = Globals.oCompany.GetCompanyService() as CompanyService;
                        BatchNumberDetailsService oBatchService = oCoService.GetBusinessService(ServiceTypes.BatchNumberDetailsService);
                        BatchNumberDetailParams oParams = oBatchService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);

                        try
                        {
                            if (StateID.Trim().Length == 0)
                            {
                                string strSQL = string.Format(@"Select AbsEntry From [OBTN] where DistNumber='{0}'", DistNumber);
                                int DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                                oParams.DocEntry = DocEntry;
                                oBatchDetails = oBatchService.Get(oParams);
                                oBatchDetails.BatchAttribute1 = pStateID;
                                oBatchService.Update(oBatchDetails);
                            //Success Reload Matrix
                           string strHolder = _SBO_Form.DataSources.UserDataSources.Item("TAB_INCMP").Value;
                            
                            if (strHolder == "Y")
                            {
                                // Select the first tab 
                                ((Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_INCMP", _SBO_Form)).Select();
                            }
                            else 
                            {
                                // Select the second tab 
                                ((Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_CMPL", _SBO_Form)).Select();
                            }
                            // Empty the search field
                            oEditSearch.Value = "";
                            // Focus back into the barcode scanner's text field
                            oEditSearch.Active = true;
                                //Move to next Row if not last row
                                if (MTX_MAIN.RowCount != intLastRowIndex)
                                {
                                    MTX_MAIN.SelectRow(intLastRowIndex + 1, true, true);
                                }
                            }
                            else
                            {
                                //Update field in DB
                                int iReturnVal = Globals.oApp.MessageBox("A State ID already exists for this item would you like to replace it?", 2, "Cancel", "OK");
                                switch (iReturnVal)
                                {
                                    case 1:
                                        //Cancel was selected do nothing                                         
                                        break;
                                    case 2:
                                        string strSQL = string.Format(@"Select AbsEntry From [OBTN] where DistNumber='{0}'", DistNumber);
                                        int DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                                        oParams.DocEntry = DocEntry;
                                        oBatchDetails = oBatchService.Get(oParams);
                                        oBatchDetails.BatchAttribute1 = oEditSearch.Value;
                                        oBatchService.Update(oBatchDetails);
                                    //Success Reload Matrix
                                    string strHolder = _SBO_Form.DataSources.UserDataSources.Item("TAB_INCMP").Value;

                                    if (strHolder=="Y")
                                    {
                                        // Select the first tab 
                                        ((Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_INCMP", _SBO_Form)).Select();
                                    }
                                    else
                                    {
                                        // Select the second tab 
                                        ((Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_CMPL", _SBO_Form)).Select();
                                    }
                                    // Empty the barcode scanner field
                                    oEditSearch.Value = "";
                                    // Focus back into the barcode scanner's text field
                                    oEditSearch.Active = true;
                                        //Move to next Row if not last row
                                        if (MTX_MAIN.RowCount != intLastRowIndex)
                                        {
                                            MTX_MAIN.SelectRow(intLastRowIndex + 1, true, true);
                                        }
                                        break;
                                }

                            }

                            //Update Matrix cell with data or Reload Matrix
                        }
                        catch (Exception ex)
                        {
                            Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                        }
                        finally
                        {
                            //Clean up the objects and Garbage Collect
                          //  _SBO_Form.Freeze(false);
                            NSC_DI.UTIL.Misc.KillObject(oBatchDetails);
                            NSC_DI.UTIL.Misc.KillObject(oCoService);
                            NSC_DI.UTIL.Misc.KillObject(oBatchService);

                            GC.Collect();
                        }
                    }

               // }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }

        }
        private void Load_TabCount(Form pForm)
        {
		    try
		    {
			    Folder TAB_INCMP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_INCMP", pForm);
			    Folder TAB_CMPL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_CMPL", pForm);

			    // Prepare to run a SQL statement.
			    SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

			    string sqlQuery = @"
SELECT 
(Select TOP 1 COUNT(*) OVER ()
FROM [@" + NSC_DI.Globals.tTestSamp + @"]
    GROUP BY U_SampleCode, U_IsComplete
    HAVING U_IsComplete = 'T' AND U_SampleCode NOT IN (

        Select U_SampleCode
        FROM [@" + NSC_DI.Globals.tTestSamp + @"]
        GROUP BY U_SampleCode, U_IsComplete
        HAVING U_IsComplete = 'N' OR U_IsComplete IS NULL) 
) As Complete
, 
(
SELECT COUNT(*)
FROM [OITM] 
JOIN [OITB] ON [OITB].[ItmsGrpCod] = [OITM].[ItmsGrpCod]
JOIN [OBTN] ON [OBTN].[ItemCode] = [OITM].[ItemCode]
JOIN [OBTQ] ON [OBTQ].[ItemCode] =  [OBTN].[ItemCode]
	AND [OBTQ].[SysNumber] = [OBTN].[SysNumber]
WHERE [OITM].OnHand > 0 
	AND [OITB].ItmsGrpNam = '" + cItemType + "' ) As Total";

			    // Count how many current records exist within the database.
			    oRecordSet.DoQuery(sqlQuery);
			    string completeTestSamples = oRecordSet.Fields.Item(0).Value.ToString();
			    int completeNumber = Int32.Parse(completeTestSamples);

			    string totalTestSamples = oRecordSet.Fields.Item(1).Value.ToString();
			    int totalNumber = Int32.Parse(totalTestSamples);

			    int incompleteNumber = totalNumber - completeNumber;

                ///To Do: Adjust the above to have the correct numbers.
                ///I am commenting out now so inaccurate numbers will not appear
                //TAB_INCMP.Caption = "Incomplete (" + incompleteNumber + ")";
                //TAB_CMPL.Caption = "Complete (" + completeNumber + ")";
                TAB_INCMP.Caption = "Incomplete";
                TAB_CMPL.Caption = "Complete";
            }
            catch (Exception ex)
		    {
			    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    //NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
        }

        public void Form_Load()
        {
            try
            { 
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_MAIN", "lab-icon.bmp");

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                //load search combo box with mtx column headers
                SAPbouiCOM.ComboBox oSearch = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CBX_SRCH", _SBO_Form) as SAPbouiCOM.ComboBox;
                SAPbouiCOM.Matrix oMatrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", _SBO_Form) as SAPbouiCOM.Matrix;

                SAPbouiCOM.CheckBox oChk = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_STID", _SBO_Form) as SAPbouiCOM.CheckBox;

                SAPbouiCOM.CheckBox oChkSubZero = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_SUB0", _SBO_Form) as SAPbouiCOM.CheckBox;
                //Default the Suppress Zero Check box to Yes to initially suppress all zero values
                _SBO_Form.DataSources.UserDataSources.Item("U_SUB0").Value = "N";

                //this.Load_Matrix(_SBO_Form, TestSampleCompletionStatus.Incomplete); // this is causing th grid to be loaded twice

                int i = 0;
                if (oSearch.ValidValues.Count > 0)
                {
                    for (i = 0; i < oMatrix.Columns.Count; i++)
                    {
                        oSearch.ValidValues.Remove(i);
                    }
                }
                for (i = 0; i < oMatrix.Columns.Count; i++)
                {
                    string ColTitle = oMatrix.Columns.Item(i).Title;
                    oSearch.ValidValues.Add(i.ToString(), ColTitle);
                }
                oSearch.ValidValues.Add(i.ToString(), "All");


                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                // Show the form
                _SBO_Form.VisibleEx = true;
                oChk.Checked = false;

                //Default select incomplete folder
                ((Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_INCMP", _SBO_Form)).Select();

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void MAIN_MATRIX_LINK_PRESSED(Form pForm, ItemEvent SAP_UI_ItemEvent)
        {
		    try
		    {
			    // Attempt to grab the selected row ID
			    int SelectedRowID = SAP_UI_ItemEvent.Row;

                if (SelectedRowID > 0)
                {
                    // Grab the matrix from the Form UI
                    SAPbouiCOM.Matrix MTX_ITEMS = (SAPbouiCOM.Matrix)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", pForm);

                    if (SAP_UI_ItemEvent.ColUID == "col_10")
                    {
                        // Get the IDs of the test selected
                        string BatchItem    = ((EditText)MTX_ITEMS.Columns.Item(1).Cells.Item(SelectedRowID).Specific).Value.ToString();
                        string BatchNumber  = ((EditText)MTX_ITEMS.Columns.Item(3).Cells.Item(SelectedRowID).Specific).Value.ToString();
                        string SampleSourceName  = ((EditText)MTX_ITEMS.Columns.Item(12).Cells.Item(SelectedRowID).Specific).Value.ToString();
                        string SampleSourceBatch = ((EditText)MTX_ITEMS.Columns.Item(11).Cells.Item(SelectedRowID).Specific).Value.ToString();
                        var SampleSourceStateID = MTX_ITEMS.Columns.Item(15).Cells.Item(SelectedRowID).Specific.Value.ToString();
                        var SourceItemCode = MTX_ITEMS.Columns.Item(MTX_ITEMS.Columns.Count - 2).Cells.Item(SelectedRowID).Specific.Value.ToString();

                        // get the lab test ID
                        var sql = $"SELECT U_LabTestID FROM [@{NSC_DI.Globals.tTestResults}] WHERE U_SourceStateID = '{SampleSourceStateID}'";
                        var LabTestID = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
                        var oForm = new F_SampleInfo(Globals.oApp, Globals.oCompany);
                        oForm.Form_Load_Find(BatchItem, BatchNumber, SampleSourceName, SampleSourceBatch, SourceItemCode, LabTestID);
                    }
                    if (SAP_UI_ItemEvent.ColUID == "col_1")
                    {
                        try
                        {
                            // Get the ID of the item master to open 
                            string SampleCode = ((EditText)MTX_ITEMS.Columns.Item(0).Cells.Item(SelectedRowID).Specific).Value.ToString();

                            // Open up the Item Master Data form
                            Globals.oApp.ActivateMenuItem("3073");

                            // Grab the "Item Master" form from the UI
                            Form formItemMaster = Globals.oApp.Forms.GetForm("150", 0);

                            // Freeze the "Item Master" form
                            formItemMaster.Freeze(true);

                            // Change the "Item Master" form to find mode
                            formItemMaster.Mode = BoFormMode.fm_FIND_MODE;

                            // Insert the Item Master Item Code in the appropriate text field
                            ((EditText)formItemMaster.Items.Item("5").Specific).Value = SampleCode;

                            // Click the "Find" button
                            ((Button)formItemMaster.Items.Item("1").Specific).Item.Click();

                            // Un-Freeze the "Item Master" form
                            formItemMaster.Freeze(false);
                        }

                        catch (Exception ex)
                        {
                            _SBO_Form.Close();
                            Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                        }
                        finally
                        {
                            //NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                            GC.Collect();
                        }

                    }
                    if (SAP_UI_ItemEvent.ColUID == "col_11"|| SAP_UI_ItemEvent.ColUID == "col_3")
                    {
                        try
                        {
                            // Get the ID of the note selected
                            string BatchNumber = ((EditText)MTX_ITEMS.Columns.Item(SAP_UI_ItemEvent.ColUID).Cells.Item(SelectedRowID).Specific).Value.ToString();

                            // Open up the Production Order Master Data form
                            Globals.oApp.ActivateMenuItem("12290");

                            // Grab the "Batch Details" form from the UI
                            Form formBatchDetails = Globals.oApp.Forms.GetForm("65053", 0);

                            // Freeze the "Production Order" form
                            formBatchDetails.Freeze(true);

                            // Change the "Production Order" form to find mode
                            formBatchDetails.Mode = BoFormMode.fm_FIND_MODE;

                            // Insert the Production Order ID in the appropriate text field
                            ((EditText)formBatchDetails.Items.Item("62").Specific).Value = BatchNumber;

                            // Click the "Find" button
                            ((Button)formBatchDetails.Items.Item("37").Specific).Item.Click();

                            // Un-Freeze the "Production Order" form
                            formBatchDetails.Freeze(false);
                        }

                        catch (Exception ex)
                        {
                            _SBO_Form.Close();
                            Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                        }
                        finally
                        {
                            //NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                            GC.Collect();
                        }
                    }
                }
		    }
		    catch (Exception ex)
		    {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
		    {
			    GC.Collect();
		    }
        }
        private void ReSize_Form(Form pForm)
        {
            try
            {
                if (pForm.Items.Item("MTX_ITEMS").Specific.RowCount < 1) return;    // for initial form load
                pForm.Items.Item("MTX_ITEMS").Specific.AutoResizeColumns();
                pForm.Items.Item("MTX_ITEMS").Specific.Columns.Item(0).Width = 1;
            }
            catch (Exception ex)
		    {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
		    {
			    GC.Collect();
		    }
        }
    }
}