﻿using System;
using SAPbobsCOM;
using SAPbouiCOM;
using B1WizardBase;

namespace NavSol.Forms.Lab_Controls
{
	public class F_TestResults : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_TEST_RESULTS";

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}

		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "BTN_FLTR":
                    BTN_FLTR_PRESS(oForm);
                    break;
            }			

            NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------
        public void Form_Load(string pSourceStateID, string pBatchNum)
        {
            Form oForm = null;
            try
            {
              
                oForm = CommonUI.Forms.Load(cFormID, true);
                oForm.DataSources.DataTables.Add("dsGrid");

                // 12975 new form items. the combo box will have the source options for filtering. The txt field stores the state source id from the batch details
                ComboBox CBO_Source = ((ComboBox)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CBO_SRC", oForm));
                EditText TXT_SrcStID = ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_SRCID", oForm));
                EditText TXT_DSTNUM = ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DSTNUM", oForm));

                TXT_DSTNUM.Value = pBatchNum;
                TXT_SrcStID.Value = pSourceStateID;
                LoadCombo(oForm, CBO_Source); //12975 loads the Source filter options. 
                LoadGrid(oForm, pBatchNum, TXT_SrcStID.Value.ToString());

                oForm.Mode = BoFormMode.fm_OK_MODE; // had to move it here because this clears date field
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oForm.VisibleEx = true;
                //NSC_DI.UTIL.Misc.KillObject(oRS);
                GC.Collect();
            }
        }

        private void LoadGrid(Form pForm, string pBatchNum, string pSourceStateID, string pSource = null)//string pItemNum, string pBatchNum,
        {
            Grid oGrd = null;

            try
            {
                string gridSQL = $@"
DECLARE @BatchNumber NVARCHAR(36);
SET @BatchNumber = '{pBatchNum}'
SELECT OBTN.ItemCode, OBTN.DistNumber AS BatchNumber, OBTN.MnfSerial AS StateID, TR.U_LabTestID AS[LabTestID], OBTN.U_NSC_TestType as [TestType], TR.U_ReleaseDate AS[ReleaseDate],
TR.U_CompoundName AS[CompoundName], TR.U_CompoundValue AS[CompoundValue], TR.U_ValuePassed AS[ValuePassed],
TR.U_ValueComment AS[ValueComment], TR.U_Source AS[Source]
FROM OBTN
INNER JOIN OITM ON OBTN.ItemCode = OITM.ItemCode
INNER JOIN [@NSC_TEST_RESULTS] TR ON OBTN.MnfSerial = TR.U_SourceStateID
WHERE OBTN.DistNumber = @BatchNumber AND OITM.QryGroup55 = 'Y'
UNION
SELECT b2.ItemCode, b2.DistNumber AS BatchNumber, b2.MnfSerial AS StateID, TR.U_LabTestID AS [LabTestID], b2.U_NSC_TestType as [TestType], TR.U_ReleaseDate AS [ReleaseDate],
TR.U_CompoundName AS [CompoundName], TR.U_CompoundValue AS [CompoundValue], TR.U_ValuePassed AS [ValuePassed],
TR.U_ValueComment AS [ValueComment], TR.U_Source AS [Source]
FROM OBTN 
LEFT JOIN OITM ON OBTN.ItemCode = OITM.ItemCode
INNER JOIN [@NSC_TEST_SAMPLE] TS ON OBTN.DistNumber = TS.U_SampledBatch
INNER JOIN OBTN B2 ON TS.U_SampleCode = B2.DistNumber
INNER JOIN [@NSC_TEST_RESULTS] TR ON B2.MnfSerial = TR.U_SourceStateID
WHERE OBTN.DistNumber = @BatchNumber AND OITM.QryGroup55 = 'N'";
               
                if (!string.IsNullOrEmpty(pSource)) //12975 checks to see if there is a source filter established and then adds it 
                    gridSQL += $" AND U_Source = '{pSource}'";

                gridSQL += $" ORDER BY U_LabTestID ASC, U_ReleaseDate DESC";
                //grid itemnum
                //gird batchnum
                oGrd = pForm.Items.Item("grdResults").Specific;
                oGrd.DataTable = pForm.DataSources.DataTables.Item("dsGrid");
                oGrd.DataTable.ExecuteQuery(gridSQL);
                oGrd.AutoResizeColumns();
                oGrd.Item.Enabled = false;
                oGrd.SelectionMode = BoMatrixSelect.ms_None;

                for(var i = 0; i < oGrd.Columns.Count; i++)
                {
                    oGrd.Columns.Item(i).Editable = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }

        private void LoadCombo(Form pForm, ComboBox pComboBox)
        {
            // 12975 for now just loads the source drop down but could be used for other future combos
            Recordset oRS = null;
            try
            {
                oRS = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                string SQL = $@"
SELECT DISTINCT U_Source
FROM [@{NSC_DI.Globals.tTestResults}]
WHERE(U_Source IS NOT NULL) AND (U_SourceStateID = '{pForm.Items.Item("TXT_SRCID").Specific.Value.ToString()}')"; //12975 sql for now only works for getting the records for the source drop down

                oRS.DoQuery(SQL);

                int cboVldVals = pComboBox.ValidValues.Count;

                if (cboVldVals > 0) // 12975 removes any values should they exist.
                    for (int x = cboVldVals; x-- > 0;)
                        pComboBox.ValidValues.Remove(x, BoSearchKey.psk_Index);                  

                if (oRS.RecordCount > 1) //12975 adds default blank valid vals
                    pComboBox.ValidValues.Add("", "");

                for (int i = 0; i < oRS.RecordCount; i++) //12975 sets the recordset vals to the combobox
                { 
                    string value = oRS.Fields.Item(0).Value.ToString();
                    pComboBox.ValidValues.Add(value, value);
                    oRS.MoveNext();                    
                }            
                                
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oRS);
                GC.Collect();
            }
        }

        private void BTN_FLTR_PRESS(Form pForm)
        {
            // 12975 filter button pressed
            try
            {
                string srcCboVal = pForm.Items.Item("CBO_SRC").Specific.Value?.ToString().Trim();
                string batchNum = pForm.Items.Item("TXT_DSTNUM").Specific.Value?.ToString();
                if (string.IsNullOrEmpty(srcCboVal))// 12975 if the Source combobox is blank it ends the fuction
                    return;
                LoadGrid(pForm, batchNum, pForm.Items.Item("TXT_SRCID").Specific.Value.ToString(), srcCboVal);// 12975 reloads the grid, now with the Source filter inserted
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }
    }
}