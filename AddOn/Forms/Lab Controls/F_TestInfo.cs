﻿using System;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using System.Collections.Generic;

namespace NavSol.Forms.Lab_Controls
{
    public class F_TestInfo : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "NSC_TEST_INFO";

        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPressed(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "1") BubbleEvent = BTN_Add_Click(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "1":
                    if (oForm.Items.Item("1").Specific.Caption == "OK") BTN_OK_Click(oForm);
                    if (oForm.Items.Item("1").Specific.Caption == "Add") BTN_ADD_Click_After(oForm);
                    break;

                case "BTN_ADDF":
                    BTN_ADD_TESTFIELD_Click(oForm);
                    break;

                case "BTN_REMF":
                    BTN_REMOVE_TESTFIELD_Click(oForm);
                    break;

                case "BTN_ADDC":
                    BTN_ADD_COMPOUND_Click(oForm);
                    break;

                case "BTN_REMC":
                    BTN_REMOVE_COMPOUND_Click(oForm);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
        [B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "MTX_TCOMP":
                    MTX_COMPOUND_MATRIX_LINK_PRESSED(oForm, pVal);
                    break;
                case "MTX_TFIELD":
                    MTX_TESTFIELD_MATRIX_LINK_PRESSED(oForm, pVal);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        [B1Listener(BoEventTypes.et_FORM_CLOSE, false, new string[] { cFormID })]
        public virtual void OnAfterFormClose(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            FORM_CLOSE(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }


        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        private static void FormSetup(Form pForm)
        {
            PictureBox IMG_Main = null;
            EditText TextBox_ID = null;
            try
            {
                pForm.Freeze(true);

                pForm.Mode = BoFormMode.fm_ADD_MODE;

                //// Set the Logo image
                //IMG_Main = pForm.Items.Item("IMG_Main").Specific;
                //IMG_Main.Picture = Globals.pathToImg + @"\Icons\" + "air-quality-icon.bmp";

                pForm.Items.Item("FLD_GENRL").Specific.Select();	// Select the first tab by default
                //pForm.Items.Item("TXT_CDATE").Specific.Value = DateTime.UtcNow.ToString("yyyyMMdd");

                TextBox_ID = pForm.Items.Item("TXT_ID").Specific;
                TextBox_ID.Value =
                    NSC_DI.UTIL.SQL.GetValue<string>("SELECT COALESCE(MAX(CONVERT(int,[Code])),0) + 1 FROM [@" + NSC_DI.Globals.tTests + "]", "1");
                pForm.Items.Item("TXT_NAME").Visible = true;

                // Only allow task_id to be edited during "Find" mode.
                TextBox_ID.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_All), BoModeVisualBehavior.mvb_False);
                TextBox_ID.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_Find), BoModeVisualBehavior.mvb_True);

                //CommonUI.LabControls.Load_Compound_Matrix(pForm, TextBox_ID);
                CommonUI.LabControls.Load_TestFields_Matrix(pForm, TextBox_ID);

                CommonUI.LabControls.LoadCBO(pForm, "CMB_FIELD", NSC_DI.Globals.tTestField);
                CommonUI.LabControls.LoadCBO(pForm, "CMB_COMP", NSC_DI.Globals.tCompounds);

                LoadMatrices(pForm); // hack

                pForm.VisibleEx = true;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(IMG_Main);
                NSC_DI.UTIL.Misc.KillObject(TextBox_ID);
                GC.Collect();
            }
        }

        public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
        {

            Form oForm = null;
            Item oItm = null;
            try
            {
                oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                //Hide 'inactive' checkbox
                oForm.Items.Item("CHK_INACTV").Visible = false;


                // create hidden field to save the calling form
                if (pCallingFormUID != null)
                {
                    oItm = oForm.Items.Add("TXT_PARE", BoFormItemTypes.it_EDIT);
                    oItm.Top = -20;
                    oItm.Specific.Value = pCallingFormUID;
                }

                FormSetup(oForm);
                return oForm;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return null;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        public static void Form_Load_Find(string TestID)
        {
            EditText TextBox_ID = null;

            try
            {
                var oForm = FormCreate();
                oForm.Freeze(true);
                oForm.Mode = BoFormMode.fm_FIND_MODE;

                //Unhide 'inactive' checkbox
                oForm.Items.Item("CHK_INACTV").Visible = true;

                // Enable UDO navigation buttons
                oForm.DataBrowser.BrowseBy = "TXT_ID";

                // Find the ID and Name textboxes in the UI
                TextBox_ID = oForm.Items.Item("TXT_ID").Specific as EditText;

                // Set the textbox for the ID to the pre-determined number.
                TextBox_ID.Value = TestID;

                oForm.Items.Item("1").Click();

                // Select the first tab by default
                oForm.Items.Item("FLD_GENRL").Specific.Select();

                LoadMatrices(oForm);

                oForm.Freeze(false);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(TextBox_ID);
                GC.Collect();
            }
        }

        private static void BTN_OK_Click(Form pForm)
        {
  
        }

        private static void BTN_ADD_Click_After(Form pForm)
        {
           
            try
            {
                if (pForm.Mode == BoFormMode.fm_ADD_MODE) FormSetup(pForm); // reset the form ?  

                //F_TestList.Refresh()
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static bool BTN_Add_Click(Form pForm)
        {
            EditText txtId = null;
            EditText txtName = null;
            CheckBox checkInactive = null;

            try
            {
                // it's not in Add mode so we don't care
                if (pForm.Mode != BoFormMode.fm_ADD_MODE) return true;

                if (TestValidate(pForm) == false) return false;

                txtId = pForm.Items.Item("TXT_ID").Specific;
                txtName = pForm.Items.Item("TXT_NAME").Specific;
                checkInactive = pForm.Items.Item("CHK_INACTV").Specific;

                if (string.IsNullOrEmpty(txtName.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter a name for the Test!");
                    txtName.Active = true;
                    return true;
                }

                // Automatically generate items from the newly created strain.
                NSC_DI.SAP.Test.CreateTest(txtId.Value, txtName.Value);

                Globals.oApp.StatusBar.SetText("Success!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

                txtName.Value = "";
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(txtId);
                NSC_DI.UTIL.Misc.KillObject(txtName);
                NSC_DI.UTIL.Misc.KillObject(checkInactive);
                GC.Collect();
            }
        }

        private static bool TestValidate(Form pForm)
        {
            EditText TextBox_Name = null;

            try
            {
                // Check for existing Compound
                TextBox_Name = pForm.Items.Item("TXT_NAME").Specific;

                string SQL_Query_FindCompoundByName = "SELECT [Code] FROM [@" + NSC_DI.Globals.tTests + "] WHERE [U_Name] = '" + TextBox_Name.Value + "'";

                SAPbobsCOM.Fields ResultsFromSQL = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(SQL_Query_FindCompoundByName);

                if (ResultsFromSQL == null) return true;

                Globals.oApp.StatusBar.SetText("There is already a Test named '" + TextBox_Name.Value + "'");
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(TextBox_Name);
                GC.Collect();
            }
        }

        private static void BTN_ADD_COMPOUND_Click(Form pForm)
        {
            try
            {
                var code = NSC_DI.UTIL.SQL.GetValue<string>("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tTestComp + "]", "1");

                var testCode = CommonUI.Forms.GetField<string>(pForm, "TXT_ID");

                var testCompCode = CommonUI.Forms.GetField<string>(pForm, "CMB_COMP", true);

                if (NSC_DI.UTIL.SQL.GetValue<int>($"SELECT COUNT(*) FROM [@{NSC_DI.Globals.tTestComp}] WHERE U_TestCode = '{testCode}' AND U_CompoundCode = '{testCompCode}'") > 0)
                {
                    Globals.oApp.StatusBar.SetText("Cannot add compound because it already exists.");

                    throw new System.ComponentModel.WarningException();
                }

                NSC_DI.SAP.Test.CreateCompoundRelation(code, testCode, testCompCode);

                LoadMatrices(pForm);
            }
            catch (System.ComponentModel.WarningException)
            {
                // message was already displayed
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void LoadCompoundMatrix(Form pForm)
        {
            var id = CommonUI.Forms.GetField<string>(pForm, "TXT_ID");

            var qry = $"SELECT CP.Code, CP.U_Name FROM [@{NSC_DI.Globals.tCompounds}] AS CP JOIN [@{NSC_DI.Globals.tTestComp}] AS TC ON CP.Code = TC.U_CompoundCode WHERE TC.U_TestCode = '{id}'";

            var cols = new List<CommonUI.Matrix.MatrixColumn>()
            {
                new CommonUI.Matrix.MatrixColumn() { Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON },
                new CommonUI.Matrix.MatrixColumn() { Caption = "Name", ColumnName = "U_Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
            };

            CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, NSC_DI.Globals.tCompounds, "MTX_TCOMP", cols, qry);
        }

        private static void LoadFieldMatrix(Form pForm)
        {
            var id = CommonUI.Forms.GetField<string>(pForm, "TXT_ID");

            var qry = $"SELECT F.Code, F.U_Name, TF.U_Group FROM [@{NSC_DI.Globals.tTestField}] AS F JOIN [@{NSC_DI.Globals.tTestTestField}] AS TF ON F.Code = TF.U_TestFieldCode WHERE TF.U_TestCode = '{id}'";

            var cols = new List<CommonUI.Matrix.MatrixColumn>()
            {
                new CommonUI.Matrix.MatrixColumn() { Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON },
                new CommonUI.Matrix.MatrixColumn() { Caption = "Name", ColumnName = "U_Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT },
                new CommonUI.Matrix.MatrixColumn() { Caption = "Group", ColumnName = "U_Group", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
            };

            CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, NSC_DI.Globals.tTestField, "MTX_TFIELD", cols, qry);
        }

        private static void LoadMatrices(Form pForm)
        {
            LoadCompoundMatrix(pForm);
            LoadFieldMatrix(pForm);
        }

        private static void BTN_REMOVE_COMPOUND_Click(Form pForm)
        {
            Matrix matrix = null;
            try
            {
                var testCode = CommonUI.Forms.GetField<string>(pForm, "TXT_ID");

                matrix = pForm.Items.Item("MTX_TCOMP").Specific as SAPbouiCOM.Matrix;

                var selectedRow = matrix.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);

                while (selectedRow > 0)
                { 
                    var compCode = CommonUI.Forms.GetField<string>(pForm, "MTX_TCOMP", selectedRow, 0);

                    NSC_DI.SAP.Test.RemoveCompoundRelation(testCode, compCode);

                    selectedRow = matrix.GetNextSelectedRow(selectedRow, BoOrderType.ot_RowOrder);
                }

                LoadMatrices(pForm);
            }
            catch (System.ComponentModel.WarningException)
            {
                // message was already displayed
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void MTX_COMPOUND_MATRIX_LINK_PRESSED(Form pForm, ItemEvent SAP_UI_ItemEvent)
        {
            try
            {
                int SelectedRowID = SAP_UI_ItemEvent.Row;

                if (SelectedRowID > 0)
                {
                    // Which column stores the ID
                    int ColumnIDForIDOfItemSelected = 0;

                    // Get the ID of the note selected
                    string ItemSelected = CommonUI.Forms.GetField<string>(pForm, "MTX_TCOMP", SelectedRowID, ColumnIDForIDOfItemSelected);

                    // Open the plant manager for the selected plant
                    Form_Load_Find(ItemSelected);
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void BTN_ADD_TESTFIELD_Click(Form pForm)
        {
            try
            {
                var code = NSC_DI.UTIL.SQL.GetValue<string>("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tTestTestField + "] ");

                var testCode = CommonUI.Forms.GetField<string>(pForm, "TXT_ID");

                var testFieldCode = CommonUI.Forms.GetField<string>(pForm, "CMB_FIELD", true);

                var group = CommonUI.Forms.GetField<string>(pForm, "TXT_GRP");

                var qry = $"SELECT COUNT(*) FROM [@{NSC_DI.Globals.tTestTestField}] WHERE U_TESTCODE = '{testCode}' AND U_TestFieldCode = '{testFieldCode}' AND U_Group = '{group}'";

                if (NSC_DI.UTIL.SQL.GetValue<int>(qry) > 0)
                {
                    Globals.oApp.StatusBar.SetText("Test Field already exists.");
                    throw new System.ComponentModel.WarningException();
                }

                NSC_DI.SAP.Test.CreateTestFieldRelation(code, testCode,  testFieldCode, group);

                LoadMatrices(pForm);
            }
            catch (System.ComponentModel.WarningException)
            {
                // message was already displayed
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void BTN_REMOVE_TESTFIELD_Click(Form pForm)
        {
            Matrix matrix = null;
            try
            {
                var testCode = CommonUI.Forms.GetField<string>(pForm, "TXT_ID");

                matrix = pForm.Items.Item("MTX_TFIELD").Specific as Matrix;

                var selectedRow = matrix.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);

                while (selectedRow > 0)
                { 
                    var testFieldCode = CommonUI.Forms.GetField<string>(pForm, "MTX_TFIELD", selectedRow, 0);

                    var group = CommonUI.Forms.GetField<string>(pForm, "MTX_TFIELD", selectedRow, 2);

                    NSC_DI.SAP.Test.RemoveTestFieldRelation(testCode, testFieldCode, group);

                    selectedRow = matrix.GetNextSelectedRow(selectedRow, BoOrderType.ot_RowOrder);
                }

                LoadMatrices(pForm);
            }
            catch (System.ComponentModel.WarningException)
            {
                // message was already displayed
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void MTX_TESTFIELD_MATRIX_LINK_PRESSED(Form pForm, ItemEvent SAP_UI_ItemEvent)
        {
            try
            {
                int SelectedRowID = SAP_UI_ItemEvent.Row;

                if (SelectedRowID > 0)
                {
                    // Which column stores the ID
                    int ColumnIDForIDOfItemSelected = 0;

                    // Get the ID of the note selected
                    string ItemSelected = CommonUI.Forms.GetField<string>(pForm, "MTX_TFIELD", SelectedRowID, ColumnIDForIDOfItemSelected);

                    F_TestFieldInfo.Form_Load_Find(ItemSelected);
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void FORM_CLOSE(Form oForm)
        {
            // TODO : remove the data from the join tables if it isn't added
        }
    }
}