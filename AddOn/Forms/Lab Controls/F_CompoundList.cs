﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Lab_Controls
{
	public class F_CompoundList : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_COMP_LS";

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "BTN_ADD":
					F_CompoundInfo.FormCreate();
					break;

				case "BTN_FIND":
					Load_Matrix(oForm);
					break;

				case "BTN_IMPORT":
					BTN_IMPORT_Click();
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "mtx_plan":
					MTX_PLAN_MATRIX_LINK_PRESSED(oForm, pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
        //--------------------------------------------------------------------------------------- et_FORM_ACTIVATE
        [B1Listener(BoEventTypes.et_FORM_ACTIVATE, false, new string[] { cFormID })]
        public virtual void OnAfterFormActivate(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            Refresh(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{

			try
			{
				pForm.Freeze(true);

                pForm.Items.Item("BTN_IMPORT").Visible = false;

				pForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;

                CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_Main", "list-icon.bmp");

				Load_Matrix(pForm);

				pForm.Items.Item("txt_total").Specific.Value = 
					NSC_DI.UTIL.SQL.GetValue<string>("SELECT COUNT(*) FROM [@" + NSC_DI.Globals.tCompounds + "]");

				pForm.Items.Item("txt_crnt").Specific.Value =
					NSC_DI.UTIL.SQL.GetValue<string>("SELECT COUNT(*) FROM [@" + NSC_DI.Globals.tCompounds + "] WHERE [CreateDate] >= CONVERT(DATE,'1/1/" + DateTime.Now.Year + "')");

				pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Form oForm = null;
			Item oItm = null;
			try
			{
				oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return null;
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

        public static void Refresh(Form pForm)
        {
            try
            {
                FormSetup(pForm);
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

		private static void Load_Matrix(Form pForm)
		{
			try
			{
				EditText txt_name = pForm.Items.Item("txt_name").Specific;

				if (txt_name.Value == "")
				{
					CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tCompounds, "mtx_plan", new List<CommonUI.Matrix.MatrixColumn>()
					{
						new CommonUI.Matrix.MatrixColumn() {Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
						new CommonUI.Matrix.MatrixColumn() {Caption = "Name", ColumnName = "U_Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
						new CommonUI.Matrix.MatrixColumn() {Caption = "Type", ColumnName = "U_Type", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}},
						"SELECT * FROM [@" + NSC_DI.Globals.tCompounds + "] ORDER BY CONVERT(int,[Code]) DESC");
				}
				else
				{
					CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tCompounds, "mtx_plan", new List<CommonUI.Matrix.MatrixColumn>()
					{
						new CommonUI.Matrix.MatrixColumn() {Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
						new CommonUI.Matrix.MatrixColumn() {Caption = "Name", ColumnName = "U_Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
						new CommonUI.Matrix.MatrixColumn() {Caption = "Type", ColumnName = "U_Type", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}},
						"SELECT * FROM [@" + NSC_DI.Globals.tCompounds + "] WHERE [U_Name] LIKE '%" + txt_name.Value.Replace("'", "''") + @"%' ORDER BY CONVERT(int,[Code]) DESC");

				}
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				//NSC_DI.UTIL.Misc.KillObject(oBar);
				GC.Collect();
			}
		}

		private static void BTN_IMPORT_Click()
        {
            /*
            // Ask the user on another thread the select an image file
            Thread thread_new = new Thread(new ThreadStart(AskForFile));
            thread_new.SetApartmentState(ApartmentState.STA);
            thread_new.Start();

            // Wait until user has selected a CSV file to import.
            while (thread_new.IsAlive)
            {
                Thread.Sleep(100);
            }

            List<CSV_Compound> ListOfCompounds = new List<CSV_Compound>();

            using (TextReader TextReader_Items = File.OpenText(SelectedFileName))
            {
                TextReader_Items.Read();

                CsvHelper.Configuration.CsvConfiguration CSV_Config = new CsvHelper.Configuration.CsvConfiguration();
                CSV_Config.HasHeaderRecord = true;
                CSV_Config.IgnoreHeaderWhiteSpace = true;
                CSV_Config.TrimHeaders = false;

                // Prepare a new CSV Reader object
                CsvHelper.CsvReader CSV_Reader_Items = new CsvHelper.CsvReader(TextReader_Items, CSV_Config);
                CSV_Reader_Items.Configuration.AutoMap<CSV_Compound>();

                // Count the total number of records we are creating
                ListOfCompounds = CSV_Reader_Items.GetRecords<CSV_Compound>().ToList();
            }

            // Create a Progress Bar
            oProgBar = Globals.oApp.StatusBar.CreateProgressBar("Starting to create items.", ListOfCompounds.Count, false);

            foreach (CSV_Compound Compound in ListOfCompounds)
            {
                // Prepare to run a SQL statement.
                SAPbobsCOM.SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + Globals.SAP_PartnerCode + "_" + NavSol.Models.Compound.TableName + "] ");

                // The single digigt plant type is "S", "H", or "I".  For "Sativa", "Hybrid", and "Indica" respectivley.
                string PlantTypeSingleDigitCode = Compound.Type;
                string NewCompoundID = oRecordSet.Fields.Item(0).Value.ToString();
                string CompoundName = Compound.Name;
                string CompoundType = "";

                switch (Compound.Type)
                {
                    case "I":
                        CompoundType = "Indica";
                        break;
                    case "S":
                        CompoundType = "Sativa";
                        break;
                    case "H":
                        CompoundType = "Hybrid";
                        break;
                }

                // Prepare a Compound Controller
                NSCNSC_DI.SAP._DI.UTIL.Misc.KillObject(oForm);Compound VirSci_Controllers_Compound = new Controllers.Compound(SAPBusinessOne_Application: Globals.oApp, SAPBusinessOne_Company: Globals.oCompany, SAPBusinessOne_Form: _SBO_Form);

                // Create Compound UDO
                VirSci_Controllers_Compound.CreateCompound(Code: NewCompoundID, Name: CompoundName, Type: CompoundType);

                // Attempt to add the new Compound items
                VirSci_Controllers_Compound.AddItemsToCompound(PlantTypeSingleDigitCode: PlantTypeSingleDigitCode, CompoundID: NewCompoundID, CompoundName: CompoundName, CompoundType: CompoundType);

                oProgBar.Text = "Compound " + CompoundName + " created!";

                oProgBar.Value += 1;
            }

            oProgBar.Stop();
             * */
        }

		private static void MTX_PLAN_MATRIX_LINK_PRESSED(Form pForm, ItemEvent SAP_UI_ItemEvent)
		{
			try
			{
				int SelectedRowID = SAP_UI_ItemEvent.Row;

				if (SelectedRowID > 0)
				{
					// Which column stores the ID
					int ColumnIDForIDOfItemSelected = 0;

					// Get the ID of the note selected
					string ItemSelected = CommonUI.Forms.GetField<string>(pForm, "mtx_plan", SelectedRowID, ColumnIDForIDOfItemSelected);

					// Open the plant manager for the selected plant
					F_CompoundInfo.Form_Load_Find(ItemSelected);
				}
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}


		public class CSV_Compound
        {
            public string Name { get; set; }
            public string Type { get; set; }
        }

        /// <summary>
        /// Stores the selected file from the seperate thread
        /// </summary>
        private string SelectedFileName;
        private void AskForFile()
        {
            var ofd = new System.Windows.Forms.OpenFileDialog();

            var y = ofd.ShowDialog();

            SelectedFileName = ofd.FileName;
		}
	}
}