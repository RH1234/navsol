﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using SAPbobsCOM;
using SAPbouiCOM;
using B1WizardBase;
using NSC_DI.UTIL;

namespace NavSol.Forms.Lab_Controls
{
	public class F_SampleInfo : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_SAMPLE_INFO";


		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		public string FormUID { get; set; }
		private SAPbouiCOM.ProgressBar oProgBar;
		private GeneralTabControls ControllerGeneralControls;

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

            if(pVal.ItemUID == "1") btnUPDATE(oForm, ref BubbleEvent);

            NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}

		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_KEY_DOWN
		[B1Listener(BoEventTypes.et_KEY_DOWN, false, new string[] {cFormID})]
		public virtual void OnAfterKeyDown(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			    var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "txtTCFL":
					if (pVal.CharPressed == 9) HandleTestSelectionCFL();
					break;

				case "txtTRCFL":
					if (pVal.CharPressed == 9)
					{
						EditText txtTRCFL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtTRCFL", oForm);

						// Only handle the tab event if the text field is empty.
						if (string.IsNullOrEmpty(txtTRCFL.Value))
						{
							txtTRCFL.Active = true;
							HandleTestResultSelectionCFL();
						}
					}
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "BTN_REMT":
					BTN_RemoveTestFromSample_Click();
					break;

				case "BTN_SAVE":
					BTN_SAVE_TEST_Click();
					break;

				case "BTN_IMPRT":
					BTN_IMPORT_Click();
					break;

				case "CHK_COMP":
					HandleCheckEvent();
					break;
                case "CHK_QCP":
                    HandleCheckEvent_QC();
                    break;

                case "btnCheck":
                    CheckState_or_SetStateID(oForm);
                                                          
					break;

                case "btnRemSave":
                    btnRemSave();
                    break;

                case "btnRemNew":
                    LoadRemFields(true); 
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

        //--------------------------------------------------------------------------------------- et_COMBO_SELECT
        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
        public virtual void OnAfterComboSelect(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            //ComboSelect(oForm, pVal); //--------------- OBSOLETE 2021 05 26

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public F_SampleInfo() : this(Globals.oApp, Globals.oCompany) { }	// FOR OLD CODE

		public F_SampleInfo(SAPbouiCOM.Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
		{
			// this.FormUID = "VSC_SAMPLE_INFO";		// FOR OLD CODE
			_VirSci_Helper_Form = new VERSCI.Forms();	// FOR OLD CODE

			_SBO_Application = SAPBusinessOne_Application;
			_SBO_Company = SAPBusinessOne_Company;
			//_VirSci_Helper_Form = new SAP_BusinessOne.Helpers.Forms(SAPBusinessOne_Application: _SBO_Application);	// FOR OLD CODE
		}

        public void Form_Load_Find(string BatchItem, string BatchNumber, string pSampleSoureName, string pSampleSourceBatchID, string SourceItemCode, string pSampleSourceStateID)
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Free the Form UI
                _SBO_Form.Freeze(true);

                // Set form to Find mode
                _SBO_Form.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;

                // Select the first tab by default
                ((SAPbouiCOM.Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "FLD_GENRL", _SBO_Form)).Select();

                ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
                ControllerGeneralControls.LoadControls();
                ControllerGeneralControls.DisableTextFields();
                LoadSampleGeneralInformation(BatchItem, BatchNumber, pSampleSoureName, pSampleSourceBatchID);

                _SBO_Form.DataSources.DBDataSources.Add("@" + NSC_DI.Globals.tRemediate);
                _SBO_Form.Items.Item("txtRemDate").Specific.DataBind.SetBound(true, "@" + NSC_DI.Globals.tRemediate, "U_Date");

                _SBO_Form.Mode = BoFormMode.fm_OK_MODE; // had to move it here because this clears date field

                LoadRemFields();

                Load_Tests_Matrix();
                Load_TestResult_Matrix();
                LoadTestCompletionCount();

                // the sampled Item code - don't need to see it
                CommonUI.Forms.Create.Item(_SBO_Form, "txtSamItmC", BoFormItemTypes.it_EDIT, -10, -10, 10, 10, bSetAutoManaged: false);
                _SBO_Form.Items.Item("txtSamItmC").Specific.Value = SourceItemCode;


                // set the new State ID field - maybe
                if (NSC_DI.UTIL.Options.Value.GetSingle("Use Test Results Table for Sample Bank") == "Y")
                {
                    if (NSC_DI.UTIL.Strings.Empty(pSampleSourceStateID) == false && pSampleSourceStateID != _SBO_Form.Items.Item("txtNSample").Specific.Value)
                    {
                        _SBO_Form.Items.Item("txtNSample").Specific.Value = pSampleSourceStateID;
                    }
                }

                _SBO_Form.Mode = BoFormMode.fm_OK_MODE;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {

                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                // Show the form
                _SBO_Form.VisibleEx = true;
                GC.Collect();
            }
        }

		#region Sample Tests Actions

		private void HandleCheckEvent()
		{
			if (ControllerGeneralControls == null)
			{
				ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
				ControllerGeneralControls.LoadControls();
			}
            else
            {
                ControllerGeneralControls = null;
               ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
                ControllerGeneralControls.LoadControls();
            }

			SAPbouiCOM.EditText TXT_RTESTC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_RTESTC", _SBO_Form) as SAPbouiCOM.EditText;

			string TestCode = TXT_RTESTC.Value;

			if (string.IsNullOrEmpty(TestCode))
			{
				return;
			}

			string chkValue = CommonUI.CheckBox.GetValue(_SBO_Form, "CHK_COMP");
			string SampleCode = ControllerGeneralControls.txtSampleBatchNumber.Value;
			NSC_DI.SAP.Test.UpdateCompletionStatus(TestCode, SampleCode, chkValue);

			// Update our Completed Test Display.
			LoadTestCompletionCount();

			// Reload the Matrix since we now are setting columns to uneditable when it is finished.
			Load_TestResult_Matrix(TestCode);
		}

        private void HandleCheckEvent_QC()
        {
            if (ControllerGeneralControls == null)
            {
                ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
                ControllerGeneralControls.LoadControls();
            }
            else
            {
                ControllerGeneralControls = null;
                ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
                ControllerGeneralControls.LoadControls();
            }

            string chkValue = CommonUI.CheckBox.GetValue(_SBO_Form, "CHK_QCP");
            string SampleCode = ControllerGeneralControls.txtSampleBatchNumber.Value;
            string StateLabID = ControllerGeneralControls.txtStateSampleID.Value;
            NSC_DI.SAP.Test.UpdateQC_onBatch(SampleCode, chkValue, StateLabID); 


        }

        private void AddTestToSample(string testCode)
        {
            SAPbobsCOM.Recordset oRS = null;

            try
            {
                if (ControllerGeneralControls == null)
                {
                    ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
                    ControllerGeneralControls.LoadControls();
                }

                // Grab the matrix from the form UI
                SAPbouiCOM.Matrix MTX_TEST =
                    CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_TEST", _SBO_Form) as SAPbouiCOM.Matrix;

                if (string.IsNullOrEmpty(testCode))
                {
                    _SBO_Application.StatusBar.SetText("Please select an Test to add.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);

                    return;
                }

                // Validate that we aren't adding the same Test Twice.
                oRS = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                string sqlQuery = string.Format(@"SELECT Top 1 [U_SampleCode] FROM [@" + NSC_DI.Globals.tTestSamp + @"] WHERE [U_SampleCode] = '{0}'
AND [U_TestCode] = {1}", _SBO_Form.Items.Item("TXT_BATCH").Specific.Value, testCode);

                // Count how many current records exist within the database.
                oRS.DoQuery(sqlQuery);

                if (oRS.RecordCount > 0)
                {
                    Globals.oApp.StatusBar.SetText("Test already exists on this Sample", SAPbouiCOM.BoMessageTime.bmt_Short);

                    return;
                }


                // Count how many current records exist within the database.
                oRS.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tTestSamp + @"]");

                // Set the textbox for the ID to the pre-determined number.
                string nextCode = oRS.Fields.Item(0).Value.ToString();

                NSC_DI.SAP.Test.CreateSampleRelation(nextCode, testCode, ControllerGeneralControls.txtSampleBatchNumber.Value);
                Load_Tests_Matrix();
                LoadTestCompletionCount();
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oRS);
                GC.Collect();
            }
        }

        private void CheckState_or_SetStateID(SAPbouiCOM.Form pForm)
        {

            //Update field in DB
            int iReturnVal = Globals.oApp.MessageBox("What would you like to do?", 2, " Check State ", " Set New ID ", " Cancel ");
            switch (iReturnVal)
            {
                case 1:
                    //Check the State for any test results to set. 
                    //Add logic for updating test ID or call state API for test Results
                    BTN_CheckStateTestsAPI();
                    break;
                case 2:
                    //***Set New State Test ID****
                    //
                    //Validate Input Data (txtNSample and if existing ID is there)
                    //
                    SAPbouiCOM.EditText oNewSampleId = ((SAPbouiCOM.EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtNSample", pForm));
                    SAPbouiCOM.EditText oOLDSampleId = ((SAPbouiCOM.EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtSample", pForm));
                    SAPbouiCOM.EditText oSampleBatchNumber = ((SAPbouiCOM.EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_BATCH", pForm));
                    SAPbouiCOM.EditText oSampleItemCode = ((SAPbouiCOM.EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ID", pForm));


                    string strNewSampleStateID = oNewSampleId.Value;
                    string strOLDSampleStateID = oOLDSampleId.Value;
                    string strSampleBatchNumber = oSampleBatchNumber.Value;
                    string strSampleItemCode = oSampleItemCode.Value;

                    //New ID Exists
                    int ValidNewID = NSC_DI.UTIL.SQL.GetValue<int>(string.Format("select Count(OBTN.AbsEntry) from OBTN where OBTN.U_NSC_LabTestID='{0}'", strNewSampleStateID));


                    if (strNewSampleStateID.Trim().Length > 0 && ValidNewID == 0)
                    {
                        

                        if (strOLDSampleStateID.Trim().Length > 0 )
                        {
                            //int iReturnVal2 = Globals.oApp.MessageBox("WARNING: If you have product that has been processed or is in process with this already assigned ID it will retain the OLD ID. /n Don't worry a link wil be established so you can find them for tracability.", 2, " Set New ID ", " Cancel ");
                            int iReturnVal2 = Globals.oApp.MessageBox("A Sample ID already exists do you want to ..."
                                            + System.Environment.NewLine + "replace it and all linked batches or just the Initial Batch Entered?"
                                            + System.Environment.NewLine + "  Note: If you choose only the inital batch and you have processed material the subsequent batches will retain the old test ID. "
                                            + System.Environment.NewLine + "No Worries though we will establish a link to these old files so you will be able to find them.", 2, "Original Only", " All Links ", " Cancel ");

                            Recordset oRecordSet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                            SAPbobsCOM.BatchNumberDetail oBatchDetails = null;
                            CompanyService oCoService = Globals.oCompany.GetCompanyService() as CompanyService;
                            BatchNumberDetailsService oBatchService = oCoService.GetBusinessService(ServiceTypes.BatchNumberDetailsService);
                            BatchNumberDetailParams oParams = oBatchService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);


                            switch (iReturnVal2)
                            {
                                case 1:
                                    //Update Sample and Original Sampled Batch
                                    try
                                    {
                                        string strSQL = string.Format(@"select OBTN.AbsEntry,OBTN.DistNumber,OBTN.ItemCode from OBTN 
                                                                        where OBTN.U_NSC_LabTestID='{0}'
                                                                        order by OBTN.CreateDate Asc", strOLDSampleStateID);

                                        oRecordSet = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(strSQL);
                                        oRecordSet.MoveFirst();
                                        //Update Underlying (Initial Batch)
                                        oParams.DocEntry = oRecordSet.Fields.Item(0).Value;
                                        oBatchDetails = oBatchService.Get(oParams);
                                        oBatchDetails.UserFields.Item("U_NSC_LabTestID").Value = strNewSampleStateID;
                                        oBatchService.Update(oBatchDetails);

                                        string strHolder = "";
                                        while (!oRecordSet.EoF)
                                        {
                                            strHolder = "";
                                            strHolder = oRecordSet.Fields.Item(1).Value;

                                            if (strHolder.StartsWith("SA-"))
                                            {
                                                oParams.DocEntry = oRecordSet.Fields.Item(0).Value;
                                                oBatchDetails = oBatchService.Get(oParams);
                                                oBatchDetails.UserFields.Item("U_NSC_LabTestID").Value = strNewSampleStateID;
                                                oBatchService.Update(oBatchDetails);

                                            }
                                            oRecordSet.MoveNext();

                                        }

                                        //Add and update Lab Change log F()
                                        ChangeSampleID_Log(strOLDSampleStateID, strNewSampleStateID, strSampleBatchNumber, strSampleItemCode);

                                        //Clean up Data and Reload form
                                        oNewSampleId.Value = "";
                                        pForm.Close();
                                        NavSol.Forms.Lab_Controls.F_SampleBank oSBAnk = new F_SampleBank();
                                        oSBAnk.ReloadActiveMX();
                                    }
                                    catch (Exception ex)
                                    {
                                        //Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                                        Globals.oApp.MessageBox("An error occured with your transaction.", 1);
                                        Globals.oApp.MessageBox(ex.ToString(), 1);
                                    }
                                    finally
                                    {
                                        //Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                                        Globals.oApp.MessageBox("Successfully Updated Original Batch", 1);
                                        NSC_DI.UTIL.Misc.KillObject(oRecordSet);
                                        NSC_DI.UTIL.Misc.KillObject(oBatchDetails);
                                        NSC_DI.UTIL.Misc.KillObject(oCoService);
                                        NSC_DI.UTIL.Misc.KillObject(oBatchService);
                                        NSC_DI.UTIL.Misc.KillObject(oParams);

                                        GC.Collect();
                                        
                                    }

                                    //End Transaction 

                                    break;
                                case 2:
                                    //Update Sample and All Related Batches. 
                                    try
                                    {

                                        string strSQL = string.Format(@"select OBTN.AbsEntry,OBTN.DistNumber,OBTN.ItemCode from OBTN 
                                                                        where OBTN.U_NSC_LabTestID='{0}'
                                                                        order by OBTN.CreateDate Asc", strOLDSampleStateID);

                                        oRecordSet = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(strSQL);
                                        oRecordSet.MoveFirst();
                                        //Update All Underlying
                                        while (!oRecordSet.EoF)
                                        {
                                            oParams.DocEntry = oRecordSet.Fields.Item(0).Value;
                                            oBatchDetails = oBatchService.Get(oParams);
                                            oBatchDetails.UserFields.Item("U_NSC_LabTestID").Value = strNewSampleStateID;
                                            oBatchService.Update(oBatchDetails);
                                            oRecordSet.MoveNext();
                                        }

                                        //Add and update Lab Change log F()
                                        ChangeSampleID_Log(strOLDSampleStateID, strNewSampleStateID, strSampleBatchNumber, strSampleItemCode);

                                        //Clean up Data and Reload form
                                        oNewSampleId.Value = "";
                                        pForm.Close();
                                        NavSol.Forms.Lab_Controls.F_SampleBank oSBAnk = new F_SampleBank();
                                        oSBAnk.ReloadActiveMX();
                                       
                                    }
                                    catch (Exception ex)
                                    {
                                        //Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                                        Globals.oApp.MessageBox("An error occured with your transaction.", 1);
                                        Globals.oApp.MessageBox(ex.ToString(), 1);
                                    }
                                    finally
                                    {
                                       // Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                                        Globals.oApp.MessageBox("Sucessfully Linked All Records", 1);
                                        NSC_DI.UTIL.Misc.KillObject(oRecordSet);
                                        NSC_DI.UTIL.Misc.KillObject(oBatchDetails);
                                        NSC_DI.UTIL.Misc.KillObject(oCoService);
                                        NSC_DI.UTIL.Misc.KillObject(oBatchService);
                                        NSC_DI.UTIL.Misc.KillObject(oParams);
                                        GC.Collect();
                                    }

                                    //End Transaction 
  
                                    break;
                                case 3:
                                    //Cancel
                                    return;
                            }
                        }

                    }
                    else
                    {
                        Globals.oApp.MessageBox("Your New Test ID already Exists and is Associated with other Records. Or it is empty.", 1);

                    }
                                              
                    break;
                case 3:
                    //Cancel was selected do nothing     
                    break;
            }
        }

        private void BTN_RemoveTestFromSample_Click()
		{
			if (ControllerGeneralControls == null)
			{
				ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
				ControllerGeneralControls.LoadControls();
			}

			// Grab the matrix from the form UI
			SAPbouiCOM.Matrix MTX_TEST =
				CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_TEST", _SBO_Form) as SAPbouiCOM.Matrix;

			string selectedTestCode = "";

			// For each row already selected
			for (int i = 1; i < (MTX_TEST.RowCount + 1); i++)
			{
				if (MTX_TEST.IsRowSelected(i))
				{
					try
					{
						selectedTestCode = ((SAPbouiCOM.EditText) MTX_TEST.Columns.Item(0).Cells.Item(i).Specific).Value;
					}
					catch
					{
					}
				}
			}

			NSC_DI.SAP.Test.RemoveSampleRelation(selectedTestCode, ControllerGeneralControls.txtSampleBatchNumber.Value);
			Load_Tests_Matrix();
			LoadTestCompletionCount();
		}

		private void Load_Tests_Matrix()
		{
			VERSCI.Matrix VirSci_Helper_Matrix = new VERSCI.Matrix(_SBO_Application, _SBO_Form);

			string sqlQuery = string.Format(@"
SELECT DISTINCT [@" + NSC_DI.Globals.tTests + @"].CODE AS [Code], [@" + NSC_DI.Globals.tTests + @"].[U_Name] AS [Name]
  FROM [@NSC_TEST_SAMPLE]
 INNER JOIN [@" + NSC_DI.Globals.tTests + @"] ON [@" + NSC_DI.Globals.tTests + @"].Code = [@" + NSC_DI.Globals.tTestSamp + @"].[U_TestCode]
 WHERE [@" + NSC_DI.Globals.tTestSamp + @"].[U_SampleCode] = '{0}'", ControllerGeneralControls.txtSampleBatchNumber.Value.ToString());


			VirSci_Helper_Matrix.LoadDatabaseDataIntoMatrix("@" + NSC_DI.Globals.tTestSamp, "MTX_TEST", new List<VERSCI.Matrix.MatrixColumn>()
				{
					new VERSCI.Matrix.MatrixColumn()
					{
						Caption = "ID",
						ColumnName = "Code",
						ColumnWidth = 80,
						ItemType = SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON
					}
					,
					new VERSCI.Matrix.MatrixColumn()
					{
						Caption = "Name",
						ColumnName = "Name",
						ColumnWidth = 80,
						ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT
					}
				}, sqlQuery);
		}

		#endregion

		#region Custom Choose From Lists

		private void HandleTestSelectionCFL()
        {
			var formTitle = "Test Choose From List";
       

			// Specifying the columns we want the Superior CFL to have.
			var MatrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
			{
				new CommonUI.Matrix.MatrixColumn()
				{
					Caption = "Code",
					ColumnWidth = 80,
					ColumnName = "Code",
					ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT
				}
				,
				new CommonUI.Matrix.MatrixColumn()
				{
					Caption = "Name",
					ColumnWidth = 40,
					ColumnName = "Name",
					ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT
				}
			};

			//int cflWidth = Math.Max(450, MatrixColumns.Count*150);
			//int cflHeight = 420;
			// Max Width is 80% of the users screen space.
            //This does not currently work correctly in the Web Browser version so we are hard coding the dimensions of the form.
            var width = 700;    //Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
            var height = 400;   //Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);

            var top = 40;       //(int)((Globals.oApp.Desktop.Height - height) / 0.7);
            var left = 380;     //(Globals.oApp.Desktop.Width - width) / 2;

                                // And lastly the Query to populate the Matrix.
            var sql = string.Format(@"SELECT Code, [U_Name] AS [Name] FROM [@" + NSC_DI.Globals.tTests + "]");

            F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pResults)
            {
                _SBO_Form = B1Connections.theAppl.Forms.Item(pCallingFormUid); 

                TestChooseFromListCallBack(pResults);
            };
            Form oForm_Cfl = F_CFL.FormCreate(formTitle, _SBO_Form.UniqueID, sql, MatrixColumns, false, null, top, left, height, width);
		}

		public void TestChooseFromListCallBack(System.Data.DataTable results)
		{
			try
			{
				//SAPbouiCOM.EditText TXT_TESTC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_TESTC", _SBO_Form) as SAPbouiCOM.EditText;

				//SAPbouiCOM.EditText TXT_STEST = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_STEST", _SBO_Form) as SAPbouiCOM.EditText;

				// Select our current form.
				_SBO_Form.Select();

				//results["Code"];
				//results["Name"];

				AddTestToSample(results.Rows[0]["Code"] as string);
			}
			catch (Exception ex)
			{
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        private void HandleTestResultSelectionCFL()
		{

            if (ControllerGeneralControls == null)
            {
                ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
				ControllerGeneralControls.LoadControls();
			}
            else
            {
                ControllerGeneralControls = null;
                ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
                ControllerGeneralControls.LoadControls();
            }

			var title = "Test Choose From List";


			// Specifying the columns we want the Superior CFL to have.
			var matrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
			{
				new CommonUI.Matrix.MatrixColumn()
				{
					Caption = "Code",
					ColumnWidth = 80,
					ColumnName = "Code",
					ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT
				}
				,
				new CommonUI.Matrix.MatrixColumn()
				{
					Caption = "Name",
					ColumnWidth = 40,
					ColumnName = "Name",
					ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT
				}
			};

			int cflWidth = Math.Max(450, matrixColumns.Count*150);
			int cflHeight = 420;


			// Max Width is 80% of the users screen space.
			var width = Math.Min((int) (Globals.oApp.Desktop.Width*0.8), cflWidth);
			var height = Math.Min((int) (Globals.oApp.Desktop.Height *0.8), cflHeight);

			var top = (Globals.oApp.Desktop.Height - height)/2;
			var left = (Globals.oApp.Desktop.Width - width)/2;

			// And lastly the Query to populate the Matrix.
			var sql = string.Format(@"
SELECT [@" + NSC_DI.Globals.tTests + @"].Code, [@" + NSC_DI.Globals.tTests + @"].[U_Name] AS [Name]
  FROM [@" + NSC_DI.Globals.tTests + @"]
  JOIN [@" + NSC_DI.Globals.tTestSamp + @"] ON [@" + NSC_DI.Globals.tTestSamp + @"].[U_SampleCode] = '{0}'
   AND [@" + NSC_DI.Globals.tTestSamp + @"].[U_TestCode] = [@" + NSC_DI.Globals.tTests + @"].Code", ControllerGeneralControls.txtSampleBatchNumber.Value);

            F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pResults)
            {
                _SBO_Form = B1Connections.theAppl.Forms.Item(pCallingFormUid);

                TestResultsChooseFromListCallBack(pResults);
            };
            Form oForm_Cfl = F_CFL.FormCreate(title, _SBO_Form.UniqueID, sql, matrixColumns, false, null, top, left, height, width);
		}

		public void TestResultsChooseFromListCallBack(System.Data.DataTable results)
		{
			try
			{
				SAPbouiCOM.EditText TXT_RTESTC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_RTESTC", _SBO_Form) as SAPbouiCOM.EditText;

				SAPbouiCOM.EditText TXT_RTEST = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_RTEST", _SBO_Form) as SAPbouiCOM.EditText;

				// Select our current form.
				_SBO_Form.Select();

				TXT_RTESTC.Value = results.Rows[0]["Code"] as string;
				TXT_RTEST.Value = results.Rows[0]["Name"] as string;

				string testCode = results.Rows[0]["Code"] as string;

				//SAPbouiCOM.CheckBox CHK_COMP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox,
				//	FormToSearchIn: _SBO_Form, ItemUID: "CHK_COMP");

				// Prepare to run a SQL statement.
				SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

				string sqlQuery = string.Format(@"SELECT [U_IsComplete] FROM [@" + NSC_DI.Globals.tTestSamp + @"] WHERE [U_SampleCode] = '{0}' 
AND [U_TestCode]= '{1}'", ControllerGeneralControls.txtSampleBatchNumber.Value, testCode);

				oRecordSet.DoQuery(sqlQuery);
				// Freeze the Form UI
				//_SBO_Form.Freeze(true);

				//try
				//{
				//	CHK_COMP.Checked = oRecordSet.Fields.Item(0).Value.Equals("Y");
				//}
				//catch (Exception ex)
				//{
    //                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				//}

				Load_TestResult_Matrix(testCode);

				_SBO_Form.Freeze(false);

			}
			catch (Exception ex)
			{
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
		}

		#endregion

		#region Sample Test Results Actions

		private void Load_TestResult_Matrix(string testCode = "")
		{
			if (ControllerGeneralControls == null)
			{
				ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
				ControllerGeneralControls.LoadControls();
			}

			VERSCI.Matrix VirSci_Helper_Matrix = new VERSCI.Matrix(_SBO_Application, _SBO_Form);

			string testSampleBatch = ControllerGeneralControls.txtSampleBatchNumber.Value;
			string labTestId = ControllerGeneralControls.txtStateSampleID.Value;

			string sqlQueryForCode = string.Format(@"
SELECT TestSample.Code
FROM [@" + NSC_DI.Globals.tTestSamp + @"] As TestSample
JOIN [OBTN] ON [OBTN].DistNumber = TestSample.[U_SampleCode]
WHERE TestSample.[U_SampleCode] = '{0}'
	AND TestSample.[U_TestCode] = '{1}'
    AND [OBTN].[U_NSC_LabTestID] = '{2}'", testSampleBatch, testCode, labTestId);

			// Prepare to run a SQL statement.
			SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset) Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
			oRecordSet.DoQuery(sqlQueryForCode);

			string testSampleCode = "0";
			if (oRecordSet.RecordCount > 0)
			{
				testSampleCode = oRecordSet.Fields.Item(0).Value.ToString();
			}

			// Defaulting to editable test results.
			bool setEditable = true;

			// Hideous Query.. To get all Compounds and Test Fields that can contain values.
			string sqlQuery = @"
SELECT [ID], [Name], [Value], [Type]
FROM (

SELECT
{Compound}.[Code] AS [ID]
, {Compound}.{CompoundName} AS [Name]
, ( SELECT TOP 1 {TestCompoundResults}.{TestCompoundResultValue} 
    FROM {TestCompoundResults} 
    WHERE {TestCompoundResults}.{TestCompoundResultCompoundCode} = {Compound}.[Code] 
    AND {TestCompoundResults}.{TestFieldResultsTestSampleCode} = '{TestSampleCodeToSearch}'
) AS [Value]
, 'Compound' AS [Type]
FROM {Compound}
JOIN {TestCompound} ON {TestCompound}.{TestCompoundCode}  = {Compound}.[Code]
JOIN [OBTN] ON [OBTN].DistNumber = '{TestSampleBatchToSearch}'
WHERE {TestCompound}.{TestCompoundTestCode} = '{TestCodeToSearch}' 
    AND [OBTN].[U_NSC_LabTestID] = '{LabTestID}'

UNION ALL

SELECT
{TestField}.[Code] AS [ID]
, {TestField}.{TestFieldName} AS [Name]
, ( SELECT TOP 1 {TestFieldResults}.{TestFieldResultsValue} 
    FROM {TestFieldResults} 
    WHERE {TestFieldResults}.{TestFieldResultsFieldCode} = {TestField}.[Code] 
    AND {TestFieldResults}.{TestFieldResultsTestSampleCode} = '{TestSampleCodeToSearch}'
) AS [Value]
, 'Field' AS [Type]
FROM {TestField} 
JOIN {Test_TestTestField} ON {Test_TestTestField}.{Test_TestFieldCode} = {TestField}.[Code]
JOIN [OBTN] ON [OBTN].DistNumber = '{TestSampleBatchToSearch}'
WHERE {Test_TestTestField}.{Test_TestFieldTestCode} = '{TestCodeToSearch}' 
    AND [OBTN].[U_NSC_LabTestID] = '{LabTestID}'
) AS [TestTable]";


			if (string.IsNullOrEmpty(testCode))
			{
				sqlQuery = string.Format(@"
SELECT * 
FROM 
(
    SELECT 
    '9999ABCDEF' AS [ID]
    ,'99999ABCDEFGHIJKL' AS [Name]
    ,'999999' AS [Value]
    ,'AB' AS [Type] 
) AS [TestTable]");
			}
			else
			{
				// So many joins..
				sqlQuery = ConvertToSQLQuery(sqlQuery, new Dictionary<string, string>()
				{
					{"{Compound}", "[@"+NSC_DI.Globals.tCompounds+"]"},
					{"{CompoundName}", "U_Name"},
					{"{TestSample}", "[@"+NSC_DI.Globals.tTestSamp+"]"},
					{"{TestSampleTestCode}", "U_TestCode"},
					{"{TestCompound}", "[@"+NSC_DI.Globals.tTestComp+"]"},
					{"{TestCompoundTestCode}", "U_TestCode"},
					{"{TestCompoundCode}", "U_CompoundCode"},
					{"{TestSampleCode}", "U_SampleCode"},
					{"{TestField}", "[@"+NSC_DI.Globals.tTestField+"]"},
					{"{TestFieldName}", "U_Name"},
					{"{Test_TestTestField}", "[@"+NSC_DI.Globals.tTestTestField+"]"},
					{"{Test_TestFieldCode}", "U_TestFieldCode"},
					{"{Test_TestFieldTestCode}", "U_TestCode"},
					{"{TestCompoundResults}", "[@"+NSC_DI.Globals.tTCResult+"]"},
					{"{TestCompoundResultValue}", "U_Value"},
					{"{TestCompoundResultCompoundCode}", "U_CompoundCode"},
					{"{TestFieldResults}", "[@"+NSC_DI.Globals.tTFResult+"]"},
					{"{TestFieldResultsValue}", "U_Value"},
					{"{TestFieldResultsFieldCode}", "U_FieldCode"},
					{"{TestFieldResultsTestSampleCode}", "U_TestSampleCode"},
					{"{TestCodeToSearch}", testCode},
					{"{TestSampleBatchToSearch}", testSampleBatch},
					{"{TestSampleCodeToSearch}", testSampleCode},
					{"{LabTestID}", labTestId}
				});

				string testCompleteQueryCheck = string.Format(@"
SELECT [U_IsComplete]
FROM [@" + NSC_DI.Globals.tTestSamp + @"]
WHERE  [U_SampleCode]= '{0}'
    AND [U_TestCode] = {1}
	AND [U_IsComplete] = 'Y'", ControllerGeneralControls.txtSampleBatchNumber.Value, testCode); // AND [U_IsComplete] = '0'", ControllerGeneralControls.txtSampleBatchNumber.Value, testCode);

                oRecordSet.DoQuery(testCompleteQueryCheck);
                // this is the original code. 
                //bool isComplete = oRecordSet.Fields.Item(0).Value.ToString() == "0" ? true : false;
                // this is what we think it is supposed to be
                bool isComplete = oRecordSet.Fields.Item(0).Value.ToString() == "Y" ? true : false;
                if (isComplete)
				{
					setEditable = false;
				}
			}

			VirSci_Helper_Matrix.LoadDatabaseDataIntoMatrix("TestTable", "MTX_TRSLT", new List<VERSCI.Matrix.MatrixColumn>()
				{
					new VERSCI.Matrix.MatrixColumn() {Caption = "ID", ColumnName = "ID", ColumnWidth = 80, ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT}
					,
					new VERSCI.Matrix.MatrixColumn()
					{
						Caption = "Field",
						ColumnName = "Name",
						ColumnWidth = 80,
						ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT
					}
					,
					new VERSCI.Matrix.MatrixColumn()
					{
						Caption = "Value",
						ColumnName = "Value",
						ColumnWidth = 80,
						ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT,
						IsEditable = setEditable
					}
					,
					new VERSCI.Matrix.MatrixColumn()
					{
						Caption = "Type",
						ColumnName = "Type",
						ColumnWidth = 80,
						ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT
					}
				}, sqlQuery);

			if (string.IsNullOrEmpty(testCode))
			{
				// Remove the first item in the Matrix
				SAPbouiCOM.DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("TestTable");
				DataTable.Rows.Remove(0);

				SAPbouiCOM.Matrix MTX_TRSLT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_TRSLT", _SBO_Form) as SAPbouiCOM.Matrix;

				MTX_TRSLT.LoadFromDataSource();
			}

		}

        #endregion

        private void LoadRemFields(bool pClear = false)
        {
            SAPbobsCOM.Recordset oRS = null;
            try
            {
                // clear the fields
                if(pClear)
                {
                    //var s1 = _SBO_Form.DataSources.DBDataSources.Item("@" + NSC_DI.Globals.tRemediate);
                    //s1.Clear();
                    _SBO_Form.Items.Item("txtRemStat").Specific.Value = "Ready";
                    _SBO_Form.Items.Item("txtRemDate").Specific.Value = NSC_DI.UTIL.Dates.GetForUI(DateTime.Today);
                    _SBO_Form.Items.Item("txtRemStep").Specific.Value = "";
                    _SBO_Form.Items.Item("txtRemName").Specific.Value = "";
                    _SBO_Form.Items.Item("txtRemCode").Specific.Value = "";
                    return;
                }

                //  get last entry
                var sql = $@"
SELECT TOP 1 *
  FROM [@{NSC_DI.Globals.tRemediate}]
 WHERE U_BatchID = '{_SBO_Form.Items.Item("TXT_BATCH").Specific.Value}'
 ORDER BY Code DESC";

                oRS = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(sql);

                // load fields
                if (oRS.RecordCount > 0)
                {
                    sql = $"SELECT UFD1.Descr FROM CUFD INNER JOIN UFD1 ON CUFD.TableID = UFD1.TableID AND CUFD.FieldID = UFD1.FieldID" 
                        + $" WHERE CUFD.TableID = '@{NSC_DI.Globals.tRemediate}' AND CUFD.AliasID = 'StateSyncStatus' AND UFD1.FldValue = '{oRS.Fields.Item("U_StateSyncStatus").Value.ToString()}'";

                    _SBO_Form.Items.Item("txtRemDate").Specific.Value = NSC_DI.UTIL.Dates.GetForUI(oRS.Fields.Item("U_Date").Value);
                    _SBO_Form.Items.Item("txtRemStat").Specific.Value = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
                    _SBO_Form.Items.Item("txtRemName").Specific.Value = oRS.Fields.Item("U_MethodName").Value.ToString();
                    _SBO_Form.Items.Item("txtRemStep").Specific.Value = oRS.Fields.Item("U_Steps").Value.ToString();
                    _SBO_Form.Items.Item("txtRemCode").Specific.Value = oRS.Fields.Item("Code").Value.ToString();
                }

                _SBO_Form.Items.Item("txtRemCode").Visible = false;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oRS);
                GC.Collect();
            }
        }

        private void btnUPDATE(Form pForm, ref bool BubbleEvent)
        {
            UserTable oUDT = null;
            try
            {
                if (pForm.Mode != SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) return;
                var newtestID = pForm.Items.Item("txtNSample").Specific.Value;
                string passed = pForm.Items.Item("cboPassQC").Specific.Value;
                string testID = pForm.Items.Item("TXT_RTESTC").Specific.Value;
                string sampleBatch = pForm.Items.Item("TXT_BATCH").Specific.Value;
                passed = passed.Substring(0, 1);
                // only update the test IDs if 'Yes' or 'Failed' or user option is 'N'
                if (passed == "N") newtestID = "";  
                if (NSC_DI.UTIL.Options.Value.GetSingle("Use Test Results Table for Sample Bank") != "Y") newtestID = "";
                // the actual sample
                NSC_DI.SAP.BatchItems.UpdateQATestResults(pForm.Items.Item("TXT_ID").Specific.Value, sampleBatch, passed, newtestID);
               
                if(!NSC_DI.UTIL.Strings.Empty(testID))
                { 
                    string code = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Code FROM [@{NSC_DI.Globals.tTestSamp}] WHERE U_SampleCode = '{sampleBatch}' AND U_TestCode = '{testID}'");
                    oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tTestSamp);// sets the datatable to Comp Harvest
                    oUDT.GetByKey(code); // gets the item that will be updated
                    oUDT.UserFields.Fields.Item("U_IsComplete").Value = "Y"; // updates U_Finished to Y
                    if (oUDT.Update() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription())); // throws error if there is an issue updating the schema
                }
                BTN_SAVE_TEST_Click(); // updates the compounds on the test results tab
                // parent Batch
                var sampledBatch = pForm.Items.Item("txtSampldB").Specific.Value;
                if(!NSC_DI.UTIL.Strings.Empty(sampledBatch))
                    NSC_DI.SAP.BatchItems.UpdateQATestResults(pForm.Items.Item("txtSamItmC").Specific.Value, sampledBatch, passed, newtestID);
                Globals.oApp.StatusBar.SetText("QC Status updated", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                LoadTestCompletionCount();
            }
            catch (Exception ex)
            {
                BubbleEvent = false;
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void btnRemSave()
        {
            SAPbobsCOM.UserTable oUDT = null;
            try
            {
                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tRemediate);
                var code = _SBO_Form.Items.Item("txtRemCode").Specific.Value.ToString();
                if(code != "" && !oUDT.GetByKey(code)) throw new Exception(NSC_DI.UTIL.Message.Format("Remediation Code not found."));

                oUDT.UserFields.Fields.Item("U_MethodName").Value   = _SBO_Form.Items.Item("txtRemName").Specific.Value;
                oUDT.UserFields.Fields.Item("U_Steps").Value        = _SBO_Form.Items.Item("txtRemStep").Specific.Value;
                oUDT.UserFields.Fields.Item("U_Date").Value         = NSC_DI.UTIL.Dates.FromUI(_SBO_Form.Items.Item("txtRemDate").Specific.Value);
                oUDT.UserFields.Fields.Item("U_SampleBatch").Value  = _SBO_Form.Items.Item("txtSampldB").Specific.Value;
                oUDT.UserFields.Fields.Item("U_SampleItem").Value   = _SBO_Form.Items.Item("txtSampldI").Specific.Value;
                oUDT.UserFields.Fields.Item("U_BatchID").Value      = _SBO_Form.Items.Item("TXT_BATCH").Specific.Value;
                oUDT.UserFields.Fields.Item("U_BatchItem").Value    = _SBO_Form.Items.Item("TXT_ID").Specific.Value;

                if (code == "")
                {
                    if (oUDT.Add() != 0)
                    {
                        Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                        return;
                    }
                    // get the code 
                    var sql = $"SELECT MAX(Code) FROM [@{NSC_DI.Globals.tRemediate}]  WHERE U_BatchID = '{_SBO_Form.Items.Item("TXT_BATCH").Specific.Value}'";
                    code = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
                    _SBO_Form.Items.Item("txtRemCode").Specific.Value = code;
                }
                else
                {
                    if (oUDT.Update() != 0)
                    {
                        Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                        return;
                    }
                }

                // update Batch UDF
                NSC_DI.SAP.BatchItems.SetUDF(_SBO_Form.Items.Item("txtSampldI").Specific.Value, _SBO_Form.Items.Item("txtSampldB").Specific.Value, "U_NSC_RemediateCode", code);

                Globals.oApp.StatusBar.SetText("Remediation saved.", SAPbouiCOM.BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        private void BTN_SAVE_TEST_Click()
		{
			if (ControllerGeneralControls == null)
			{
				ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
				ControllerGeneralControls.LoadControls();
			}

			SAPbouiCOM.EditText TXT_RTESTC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_RTESTC", _SBO_Form) as SAPbouiCOM.EditText;

			string testCode = TXT_RTESTC.Value;
			string testSampleBatch = ControllerGeneralControls.txtSampleBatchNumber.Value;
			string labTestId = ControllerGeneralControls.txtStateSampleID.Value;

			// Prepare to run a SQL statement.
			SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset) Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

			#region Get the TestSample Code By the SampleId and TestCode

			string testSampleCodeQuery = string.Format(@"
SELECT CODE
FROM [@" + NSC_DI.Globals.tTestSamp + @"]
JOIN [OBTN] ON [OBTN].DistNumber = [U_SampleCode]
WHERE [U_SampleCode] = '{0}'
    AND [U_TestCode] = '{1}'
    AND [OBTN].[U_NSC_LabTestID] = '{2}'", testSampleBatch, testCode, labTestId);

			oRecordSet.DoQuery(testSampleCodeQuery);

			string testSampleCode = oRecordSet.Fields.Item(0).Value.ToString();

			#endregion

			SAPbouiCOM.Matrix MTX_TRSLT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_TRSLT", _SBO_Form) as SAPbouiCOM.Matrix;

			bool allSucceeded = true;
			// For each row already selected
			for (int i = 1; i < (MTX_TRSLT.RowCount + 1); i++)
			{
				try
				{
					string id = ((SAPbouiCOM.EditText) MTX_TRSLT.Columns.Item(0).Cells.Item(i).Specific).Value;
					string fieldName = ((SAPbouiCOM.EditText) MTX_TRSLT.Columns.Item(1).Cells.Item(i).Specific).Value;
					string value = ((SAPbouiCOM.EditText) MTX_TRSLT.Columns.Item(2).Cells.Item(i).Specific).Value;
					string fieldType = ((SAPbouiCOM.EditText) MTX_TRSLT.Columns.Item(3).Cells.Item(i).Specific).Value;

					if (fieldType == "Field")
					{
						string sqlQuery = string.Format(@"
SELECT  [@" + NSC_DI.Globals.tTFResult + @"].Code
FROM [@" + NSC_DI.Globals.tTFResult + @"]
JOIN [@" + NSC_DI.Globals.tTestSamp + @"] ON [@NSC_TEST_SAMPLE].Code = [@" + NSC_DI.Globals.tTFResult + @"].[U_TestSampleCode]
JOIN [OBTN] ON [OBTN].DistNumber = [@" + NSC_DI.Globals.tTestSamp + @"].[U_SampleCode]
WHERE [U_TestSampleCode] = '{1}'
    AND [U_FieldCode] = '{2}'
    AND [OBTN].[U_NSC_LabTestID] = '{3}'", testSampleBatch, testSampleCode, id, labTestId);

						oRecordSet.DoQuery(sqlQuery);

						if (oRecordSet.RecordCount == 0)
						{
							// Count how many current records exist within the database.
							oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tTFResult + "]");

							// Set the textbox for the ID to the pre-determined number.
							string nextCode = oRecordSet.Fields.Item(0).Value.ToString();

							NSC_DI.SAP.Test.CreateTestFieldResult(nextCode, testSampleCode, id, value);
						}
						else
						{
							NSC_DI.SAP.Test.UpdateTestFieldResult(testSampleCode, id, value);
						}
					}
					else if (fieldType == "Compound")
					{
						string sqlQuery = string.Format(@"
SELECT  [@NSC_TC_RESULT].Code
FROM [@NSC_TC_RESULT]
JOIN [@" + NSC_DI.Globals.tTestSamp + @"] ON [@" + NSC_DI.Globals.tTestSamp + @"].Code = [@" + NSC_DI.Globals.tTCResult + @"].[U_TestSampleCode]
JOIN [OBTN] ON [OBTN].DistNumber = [@" + NSC_DI.Globals.tTestSamp + @"].[U_SampleCode]
WHERE [U_TestSampleCode] = '{1}'
    AND [U_CompoundCode] = '{2}'
    AND [OBTN].[U_NSC_LabTestID] = '{3}'", testSampleBatch, testSampleCode, id, labTestId);

						oRecordSet.DoQuery(sqlQuery);

						if (oRecordSet.RecordCount == 0)
						{
							// Count how many current records exist within the database.
							oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tTCResult + "]");

							// Set the textbox for the ID to the pre-determined number.
							string nextCode = oRecordSet.Fields.Item(0).Value.ToString();

							NSC_DI.SAP.Test.CreateTestCompoundResult(nextCode, testSampleCode, id, value);
						}
						else
						{
							NSC_DI.SAP.Test.UpdateTestCompoundResult(testSampleCode, id, value);
						}
					}

					//if (!controllerTest.WasSuccessful)
					//{
					//	allSucceeded = false;
					//}
				}
				catch
				{
					allSucceeded = false;
				}
			}

			if (allSucceeded)
			{
				Globals.oApp.StatusBar.SetText("Saved all Fields for Test!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
			}
			else
			{

                Globals.oApp.StatusBar.SetText("Some of the results are not saved correctly!", SAPbouiCOM.BoMessageTime.bmt_Short);
			}
		}

		private void BTN_IMPORT_Click()
		{
			// Ask the user on another thread the select an image file
			Thread thread_new = new Thread(new ThreadStart(AskForFile));
			thread_new.SetApartmentState(ApartmentState.STA);
			thread_new.Start();

			// Wait until user has selected an image.
			while (thread_new.IsAlive)
			{
				Thread.Sleep(100);
			}

			try
			{
				// No Item was selected.
				if (string.IsNullOrEmpty(SelectedFileName))
					return;

			}
			catch (Exception e)
			{
				//TODO-LOG: Log an error.

				_SBO_Application.SetStatusBarMessage("Failed to load!", SAPbouiCOM.BoMessageTime.bmt_Short, false);

				return;
			}

			_SBO_Application.SetStatusBarMessage("You selected the file '" + SelectedFileName + "'!", SAPbouiCOM.BoMessageTime.bmt_Short, false);
		}

		private void BTN_CheckStateTestsAPI()
		{
/*	// COMPLIANCE
			if (ControllerGeneralControls == null)
			{
				ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
				ControllerGeneralControls.LoadControls();
			}

			VirSci_SAP.WAStateHelpers.TestResults helperTestResults = new WAStateHelpers.TestResults(
				company: _SBO_Company, application: _SBO_Application);

			VirSci_SAP.WAStateHelpers.TestResults.StateTestResults testResults = helperTestResults.CheckStateTestsAPI(
			                                                                                                          stateSampleId:
				                                                                                                          ControllerGeneralControls.txtStateSampleID
					                                                                                                          .Value,
				sampleBatchNumber: ControllerGeneralControls.txtSampleBatchNumber.Value.ToString());

			testResults.PopulateDefaultTestsWithResults();

			if (testResults.FoundResults)
			{
				// If the test was found.. update the batched item with a QA status of passed or fail.
				// ALSO: Update the Mother_ID (Batch Item that was used for the sample to Pass or Fail)

				SAP_BusinessOne.Controllers.BatchItems controllerBatchItem = new SAP_BusinessOne.Controllers.BatchItems(
					SAPBusinessOne_Application: _SBO_Application,
					SAPBusinessOne_Company: _SBO_Company);

				string sqlQuery = string.Format(@"
SELECT [U_VSC_MotherID]
FROM OBTN
WHERE OBTN.DistNumber = '{0}'", ControllerGeneralControls.txtSampleBatchNumber.Value.ToString());

				string sampleOriginatingBatch = "";

				SAPbobsCOM.IRecordset dbCommunication = (SAPbobsCOM.IRecordset) _SBO_Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

				try
				{
					dbCommunication.DoQuery(sqlQuery);
					dbCommunication.MoveFirst();
					if (dbCommunication.RecordCount >= 1)
					{
						sampleOriginatingBatch = dbCommunication.Fields.Item("TestId").Value.ToString();
					}
				}
				catch (Exception ex)
				{
					log.Error("SampleInfo - CheckAPITest: Failed to get originating batch number for sample");
				}

				if (testResults.TestingPassed == VirSci_SAP.WAStateHelpers.TestResults.TestResultState.Success)
				{
					ControllerGeneralControls.txtPassedQA.Value = "Passed!";

					controllerBatchItem.UpdateQATestResults(batchNumber: sampleOriginatingBatch,
						newStatus: SAP_BusinessOne.Controllers.BatchItems.TestResultStatus.Passed);

					controllerBatchItem.UpdateQATestResults(batchNumber: ControllerGeneralControls.txtSampleBatchNumber.Value.ToString(),
						newStatus: SAP_BusinessOne.Controllers.BatchItems.TestResultStatus.Passed);

				}
				else if (testResults.TestingPassed == VirSci_SAP.WAStateHelpers.TestResults.TestResultState.Failed)
				{
					ControllerGeneralControls.txtPassedQA.Value = "Failed!";

					controllerBatchItem.UpdateQATestResults(batchNumber: sampleOriginatingBatch,
						newStatus: SAP_BusinessOne.Controllers.BatchItems.TestResultStatus.Failed);

					controllerBatchItem.UpdateQATestResults(batchNumber: ControllerGeneralControls.txtSampleBatchNumber.Value.ToString(),
						newStatus: SAP_BusinessOne.Controllers.BatchItems.TestResultStatus.Failed);
				}

				ControllerGeneralControls.txtAPIMessage.Value = testResults.Message;
			}
			else
			{
				ControllerGeneralControls.txtAPIMessage.Value = testResults.Message;
				ControllerGeneralControls.txtPassedQA.Value = "Not Found";
			}
*/
		}

        public static void ChangeSampleID_Log(string pOldID, string pNewID, string pSamBatchID, string pSamItemCode)
        {
            // Grab the next available code from the SQL database
            string NextCode = NSC_DI.UTIL.SQL.GetNextAvailableCodeForItem(NSC_DI.Globals.tTestChangeLog, "Code");

            SAPbobsCOM.UserTable sboTable = null;

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                // Prepare a connection to the user table
                sboTable = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tTestChangeLog);

                //sboTable.Code = NextCode;
                sboTable.Name = NextCode;
                sboTable.UserFields.Fields.Item("U_OldID").Value = pOldID;
                sboTable.UserFields.Fields.Item("U_NewID").Value = pNewID;
                sboTable.UserFields.Fields.Item("U_SamBatchID").Value = pSamBatchID;
                sboTable.UserFields.Fields.Item("U_SamItemCode").Value = pSamItemCode;
                sboTable.UserFields.Fields.Item("U_UserID").Value = Globals.oCompany.UserSignature;
                sboTable.UserFields.Fields.Item("U_UserName").Value = Globals.oCompany.UserName;
                sboTable.UserFields.Fields.Item("U_DateCreated").Value = DateTime.Now;

                // Attempt to add the new item to the database
                Exception ex = new Exception("Error updating Sample change log");
                if (sboTable.Add() != 0) throw ex; 
            }
            catch (Exception ex)
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                NSC_DI.UTIL.Misc.KillObject(sboTable);
                GC.Collect();
            }
        }

        private void LoadSampleGeneralInformation(string SampleId, string BatchNumber, string pSampleSoureName, string pSampleSourcBatchID)
		{
            _SBO_Form.Items.Item("txtSampldB").Specific.Value = pSampleSourcBatchID;
            _SBO_Form.Items.Item("txtSampldI").Specific.Value = pSampleSoureName;

            string sqlQuery = string.Format(@"
SELECT DISTINCT [OITM].ItemCode, [OITM].ItemName, [OBTQ].Quantity, [OBTN].DistNumber, [OBTN].CreateDate, [OWHS].WhsName, [OBTQ].WhsCode,
[OBTN].[U_NSC_LabTestID] AS [StateSampleID], [OBTN].[U_NSC_PassedQA] AS [PassedQA], [OBTN].[U_NSC_TestType] as [TestType]
  FROM [OITM] 
  JOIN [OITB] ON [OITB].[ItmsGrpCod] = [OITM].[ItmsGrpCod]
  JOIN [OBTN] ON [OBTN].[ItemCode] = [OITM].[ItemCode]
  JOIN [OBTQ] ON [OBTQ].[ItemCode] =  [OBTN].[ItemCode]
  JOIN [OWHS] ON [OWHS].[WhsCode] = [OBTQ].[WhsCode]
   AND [OBTQ].[SysNumber] = [OBTN].[SysNumber]
 WHERE [OITM].ItemCode = '{0}' AND [OBTN].DistNumber = '{1}'", SampleId, BatchNumber);


			SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset) _SBO_Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
			oRecordSet.DoQuery(sqlQuery);

			ControllerGeneralControls.txtSampleId.Value = SampleId;
			ControllerGeneralControls.txtSampleName.Value = oRecordSet.Fields.Item("ItemName").Value.ToString();
			ControllerGeneralControls.txtSampleQuantity.Value = oRecordSet.Fields.Item("Quantity").Value.ToString();
			ControllerGeneralControls.txtSampleBatchNumber.Value = BatchNumber;
			ControllerGeneralControls.txtWarehouse.Value = oRecordSet.Fields.Item("WhsName").Value.ToString();
			ControllerGeneralControls.txtWarehouseCode.Value = oRecordSet.Fields.Item("WhsCode").Value.ToString();
            string testType = oRecordSet.Fields.Item("TestType").Value.ToString();
            if(!string.IsNullOrEmpty(testType))
            {
                string testTypeDesc = "";
                switch(testType.ToUpper())
                {
                    case "L":
                        testTypeDesc = "Initial Lab";
                        break;
                    case "I":
                        testTypeDesc = "Internal";
                        break;
                    case "R":
                        testTypeDesc = "Retest";
                        break;
                }
                ControllerGeneralControls.txtTestType.Value = testTypeDesc;
            }
            
            // Two new columns for the State API Complaincy.
            ControllerGeneralControls.txtStateSampleID.Value = oRecordSet.Fields.Item("StateSampleID").Value.ToString();

            CommonUI.ComboBox.ComboBoxAdd_UFD1(_SBO_Form, "cboPassQC", "OBTN", "NSC_PassedQA");
            string strQcState = null;
            strQcState = oRecordSet.Fields.Item("PassedQA").Value.ToString();
            CommonUI.Forms.SetField(_SBO_Form, "cboPassQC", strQcState);

            string dateString = oRecordSet.Fields.Item("CreateDate").Value.ToString().Remove(10);
            _SBO_Form.Items.Item("txtTStatus").Specific.Value = dateString;
            
            //         if(strQcState=="Y")
            //         {
            //             strQcState = "Yes";
            //             ControllerGeneralControls.chkPassedQA.Checked = true;
            //         }
            //         else
            //         {
            //             strQcState = "No";
            //             ControllerGeneralControls.chkPassedQA.Checked = false;
            //         }
            //ControllerGeneralControls.txtPassedQA.Value = strQcState;



            BTN_CheckStateTestsAPI();
		}

		private void LoadTestCompletionCount()
		{
			if (ControllerGeneralControls == null)
			{
				ControllerGeneralControls = new GeneralTabControls(_SBO_Form, _VirSci_Helper_Form);
				ControllerGeneralControls.LoadControls();
			}

			string completeQuery = string.Format(@"
SELECT COUNT(*)
FROM [@" + NSC_DI.Globals.tTestSamp + @"]
WHERE [U_SampleCode] = '{0}'
	AND [U_IsComplete] = 'Y'", ControllerGeneralControls.txtSampleBatchNumber.Value); //ORIGINAL: AND[U_IsComplete] = '0'", ControllerGeneralControls.txtSampleBatchNumber.Value);


            string incompleteQuery = string.Format(@"
SELECT COUNT(*) FROM [@" + NSC_DI.Globals.tTestSamp + @"]
WHERE [U_SampleCode] = '{0}'
  AND ([U_IsComplete] = 'N' OR [U_IsComplete] IS NULL)", ControllerGeneralControls.txtSampleBatchNumber.Value); //ORIGINAL: AND ([U_IsComplete] = '1' OR [U_IsComplete] IS NULL)", ControllerGeneralControls.txtSampleBatchNumber.Value);

            // Prepare to run a SQL statement.
            SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset) Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
			oRecordSet.DoQuery(completeQuery);

			string complete = oRecordSet.Fields.Item(0).Value.ToString();

			oRecordSet.DoQuery(incompleteQuery);

			string incomplete = oRecordSet.Fields.Item(0).Value.ToString();
			int numberOfTotal = Int32.Parse(complete) + Int32.Parse(incomplete);

			SAPbouiCOM.EditText TXT_TCOM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_TCOM", _SBO_Form);

			TXT_TCOM.Value = string.Format("{0} / {1}", complete, numberOfTotal);
		}

		private string ConvertToSQLQuery(string Query, Dictionary<string, string> parameters)
		{
			foreach (KeyValuePair<string, string> param in parameters)
			{
				Query = Query.Replace(param.Key, param.Value);
			}

			return Query;
		}

		/// <summary>
		/// Stores the selected file from the seperate thread
		/// </summary>
		private string SelectedFileName;

		private void AskForFile()
		{
			var ofd = new System.Windows.Forms.OpenFileDialog();
			var y = ofd.ShowDialog();

			SelectedFileName = ofd.FileName;
		}

        private void ComboSelect(Form pForm, ItemEvent pVal)
        {

            try
            {

                if (pVal.ItemUID == "cboPassQC" && pVal.ItemChanged == true)
                {
                    if (NSC_DI.UTIL.Options.Value.GetSingle("Use Test Results Table for Sample Bank") == "Y")
                    {
                        // UPDATE THE TEST ID
                        var newtestID = pForm.Items.Item("txtNSample").Specific.Value;
                        // the actual sample
                        NSC_DI.SAP.BatchItems.UpdateQATestResults(pForm.Items.Item("TXT_ID").Specific.Value, pForm.Items.Item("TXT_BATCH").Specific.Value, pForm.Items.Item("cboPassQC").Specific.Value, newtestID);
                        // parent Batch
                        NSC_DI.SAP.BatchItems.UpdateQATestResults(pForm.Items.Item("txtSamItmC").Specific.Value, pForm.Items.Item("txtSampldB").Specific.Value, pForm.Items.Item("cboPassQC").Specific.Value, newtestID);
                    }
                    else
                    {
                        // UPDATE JUST THE PassedQA FIELD
                        // the actual sample
                        NSC_DI.SAP.BatchItems.UpdateQATestResults(pForm.Items.Item("TXT_ID").Specific.Value, pForm.Items.Item("TXT_BATCH").Specific.Value, pForm.Items.Item("cboPassQC").Specific.Value, "");
                        // parent Batch
                        NSC_DI.SAP.BatchItems.UpdateQATestResults(pForm.Items.Item("txtSamItmC").Specific.Value, pForm.Items.Item("txtSampldB").Specific.Value, pForm.Items.Item("cboPassQC").Specific.Value, "");
                    }

                    Globals.oApp.StatusBar.SetText("QC Status updated", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                }
                //pForm.Items.Item("MTX_ITEMS").Specific.AutoResizeColumns();
                //pForm.Items.Item("MTX_ITEMS").Specific.Columns.Item(0).Width = 1;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }

            //--------------- OBSOLETE 2021 05 26
            //try
            //{
            //    if (NSC_DI.UTIL.Options.Value.GetSingle("Use Test Results Table for Sample Bank") != "Y") return;

            //    if (pVal.ItemUID == "cboPassQC" && pVal.ItemChanged == true)
            //    {
            //        var newtestID = pForm.Items.Item("txtNSample").Specific.Value;
            //        // the actual sample
            //        NSC_DI.SAP.BatchItems.UpdateQATestResults(pForm.Items.Item("TXT_ID").Specific.Value, pForm.Items.Item("TXT_BATCH").Specific.Value, pForm.Items.Item("cboPassQC").Specific.Value, newtestID);
            //        // parent Batch
            //        NSC_DI.SAP.BatchItems.UpdateQATestResults(pForm.Items.Item("txtSamItmC").Specific.Value, pForm.Items.Item("txtSampldB").Specific.Value, pForm.Items.Item("cboPassQC").Specific.Value, newtestID);
            //        Globals.oApp.StatusBar.SetText("QC Status updated", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            //    }
            //    //pForm.Items.Item("MTX_ITEMS").Specific.AutoResizeColumns();
            //    //pForm.Items.Item("MTX_ITEMS").Specific.Columns.Item(0).Width = 1;
            //}
            //catch (Exception ex)
            //{
            //    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            //}
            //finally
            //{
            //    GC.Collect();
            //}

        }
        public class GeneralTabControls
		{
			public SAPbouiCOM.EditText txtSampleId;
			public SAPbouiCOM.EditText txtSampleName;
			public SAPbouiCOM.EditText txtSampleQuantity;
			public SAPbouiCOM.EditText txtSampleBatchNumber;
			public SAPbouiCOM.EditText txtWarehouse;
			public SAPbouiCOM.EditText txtWarehouseCode;

			public SAPbouiCOM.EditText txtPassedQA;
			public SAPbouiCOM.EditText txtStateSampleID;
			public SAPbouiCOM.EditText txtAPIMessage;
            public SAPbouiCOM.EditText txtTestType;
            //public SAPbouiCOM.CheckBox chkPassedQA;

			public SAPbouiCOM.Button btnCheck;

			private SAPbouiCOM.Form _FormToSearch;
			private VERSCI.Forms _FormHelper;

			public GeneralTabControls(SAPbouiCOM.Form _FormToSearch, VERSCI.Forms _FormHelper)
			{
				this._FormToSearch = _FormToSearch;
				this._FormHelper = _FormHelper;
			}

			public void LoadControls()
			{
				txtSampleId = ((SAPbouiCOM.EditText) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ID", _FormToSearch));

				txtSampleName = ((SAPbouiCOM.EditText) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_NAME", _FormToSearch));

				txtSampleQuantity = ((SAPbouiCOM.EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QTY", _FormToSearch));

				txtSampleBatchNumber = ((SAPbouiCOM.EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_BATCH", _FormToSearch));

				txtWarehouse = ((SAPbouiCOM.EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WHRS", _FormToSearch));

				txtWarehouseCode = ((SAPbouiCOM.EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WHRSC", _FormToSearch));

				txtPassedQA = ((SAPbouiCOM.EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtTStatus", _FormToSearch));

				txtStateSampleID = ((SAPbouiCOM.EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtSample", _FormToSearch));

				txtAPIMessage = ((SAPbouiCOM.EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtMessage", _FormToSearch));

				btnCheck = ((SAPbouiCOM.Button)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "btnCheck", _FormToSearch));

                txtTestType = ((SAPbouiCOM.EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_TSTYPE", _FormToSearch));

                //chkPassedQA = ((SAPbouiCOM.CheckBox)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_QCP", _FormToSearch));

            }

			public void DisableTextFields()
			{
				txtSampleId.Item.Enabled = false;
				txtSampleName.Item.Enabled = false;
				txtSampleQuantity.Item.Enabled = false;
				txtSampleBatchNumber.Item.Enabled = false;
				txtWarehouse.Item.Enabled = false;
				txtWarehouseCode.Item.Enabled = false;
			}
		}

	}
}