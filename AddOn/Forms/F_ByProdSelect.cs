﻿using System;
using B1WizardBase;
using SAPbouiCOM;


namespace NavSol.Forms
{
    class F_ByProdSelect : B1Event
    {
        // ---------------------------------- VARS, CLASSES --------------------------------------------------------
        const string cFormID = "NSC_BYPROD";

        private static string _callingFormUid = String.Empty;
        public static ByProdSelectCallback ByProdsCB = null;
        public delegate void ByProdSelectCallback(string callingFormUid, System.Data.DataTable pdtByProd);

        // ---------------------------------- BEFORE EVENT --------------------------------------------------------
       
        // ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_FORM_RESIZE
        [B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
        public virtual void OnAfterFormResize(ItemEvent pVal)
        {
            if (pVal.InnerEvent) return;
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            ReSize_Form(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            switch (pVal.ItemUID)
            {
                case "1":
                     Press_OK();
                    break;
                case "2":
                    oForm.Close();
                    ByProdsCB(_callingFormUid, null);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CLICK
        [B1Listener(BoEventTypes.et_CLICK, false, new string[] { cFormID })]
        public virtual void OnAfterClick(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "1":
                    Press_OK();
                    break;
                case "2":
                    oForm.Close();
                    ByProdsCB(_callingFormUid, null);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //---------------------------------------------- SUBS       --------------------------------------------------------
        private static string FormXML()
        {
            string xml = "";
            xml = @"<?xml version='1.0' encoding='UTF-16' ?>
<Application>
    <forms>
    <action type='add'>
        <form appformnumber='(NSC_FORM)' FormType='(NSC_FORM)' type='0' BorderStyle='0' uid='(NSC_FORM)' title='Byproduct Assignment' visible='1' default_button='1' pane='0' color='0' left='384' top='86' width='610' height='542' client_width='1469' client_height='523' AutoManaged='1' SupportedModes='15' ObjectType='' modality='0'> 
            <datasources>
                <dbdatasources>
                    <action type='add'/>
                </dbdatasources>
                <userdatasources>
                    <action type='add'/>
                </userdatasources>
            </datasources>
            <Menus><action type='enable'>
                <Menu uid='5890'/>
                </action><action type='disable'/>
            </Menus>
            <items>
                <action type='add'>
                    <item uid='1' type='4' left='15' tab_order='0' width='74' top='479' height='19' visible='1' enabled='1' from_pane='0' to_pane='0' disp_desc='0' right_just='0' description='' linkto='' forecolor='-1' backcolor='-1' text_style='0' font_size='-1' supp_zeros='0' AffectsFormMode='1'>
                        <AutoManagedAttribute/>
                        <specific caption='Finish Order'/>
                    </item>
                        <item uid='2' type='4' left='96' tab_order='0' width='65' top='479' height='19' visible='1' enabled='1' from_pane='0' to_pane='0' disp_desc='0' right_just='0' description='' linkto='' forecolor='-1' backcolor='-1' text_style='0' font_size='-1' supp_zeros='0' AffectsFormMode='1'>
                            <AutoManagedAttribute/>
                            <specific caption='Cancel'/>
                        </item>
                        <item uid='12' type='8' left='15' tab_order='0' width='96' top='16' height='14' visible='1' enabled='1' from_pane='0' to_pane='0' disp_desc='0' right_just='0' description='' linkto='' forecolor='-1' backcolor='-1' text_style='0' font_size='-1' supp_zeros='0' AffectsFormMode='1'>
                            <AutoManagedAttribute/><specific caption='ByProducts:'/>
                        </item>
                        <item uid='grdSrcBy' type='128' left='15' tab_order='0' width='570' top='60' height='403' visible='1' enabled='1' from_pane='0' to_pane='0' disp_desc='0' right_just='0' description='' linkto='' forecolor='-1' backcolor='-1' text_style='0' font_size='-1' supp_zeros='0' AffectsFormMode='1'>
                            <AutoManagedAttribute/><specific SelectionMode='0' DataTable='' CollapseLevel='0'>
                                <RowHeaders Width='20'/><GridColumns/>
                            </specific>
                        </item>

                        </action>
                    </items>
                <Settings Enabled='1' MatrixUID='grdDstLocs' EnableRowFormat='1'/>
            </form>
        </action>
    </forms>
</Application>
";
            return xml;
        }

        public static void FormCreate(string pCallingFormUID, SAPbouiCOM.DataTable pSrcDT)
        {
            Form oForm = null;
            Item oItm = null;

            try
            {
                var formStr = FormXML();
                formStr = formStr.Replace("(NSC_FORM)", cFormID);
                oForm = CommonUI.Forms.LoadXML_Modal(formStr);

                _callingFormUid = pCallingFormUID;

                oForm.Freeze(true);
                //to do
                FormSetup(oForm);
                //to do
                FormLoad(oForm, pSrcDT);

                oForm.Mode = BoFormMode.fm_OK_MODE;


            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oForm.Freeze(false);
                oForm.VisibleEx = true;
                NSC_DI.UTIL.Misc.KillObject(oForm);
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private static void FormSetup(Form pForm)
        {
            try
            {
                pForm.DataSources.DataTables.Add("dtSrcBy");
                pForm.Items.Item("grdSrcBy").Specific.DataTable = pForm.DataSources.DataTables.Item("dtSrcBy");
                pForm.Items.Item("grdSrcBy").Specific.SelectionMode = BoMatrixSelect.ms_Auto;
                pForm.Items.Item("grdSrcBy").Enabled = true;


            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.VisibleEx = true;
            }
        }

        private static void FormLoad(Form pForm, SAPbouiCOM.DataTable dtSource)
        {
            Grid oGrd = null;

            try
            {
                // SOURCE GRID
                oGrd = pForm.Items.Item("grdSrcBy").Specific;
                oGrd.DataTable.CopyFrom(dtSource);

                // only the Quantity & Warehouse column is editable
                for (var i = 0; i < oGrd.Columns.Count; i++)
                {
                    oGrd.Columns.Item(i).Editable = false;
                }
                oGrd.Columns.Item("PlannedQty").Editable = true;

                oGrd.Columns.Item("ItemCode").Visible = true;
               
                oGrd.Columns.Item("ItemName").Visible = true;
                oGrd.Columns.Item("PlannedQty").Visible = true;
                oGrd.Columns.Item("PlannedQty").RightJustified = true;
                oGrd.Columns.Item("LineNum").Visible = false;
                oGrd.Columns.Item("BatchID").Visible = false;
                oGrd.Columns.Item("StateID").Visible = true;
                oGrd.Columns.Item("StateID").Editable = true;
                oGrd.Columns.Item("StateID").RightJustified = false;
                oGrd.Columns.Item("Warehouse").Editable = true;     // 11320 makes the warehouse editable           
                pForm.Items.Item("grdSrcBy").Enabled = true;
                pForm.Items.Item("grdSrcBy").Specific.AutoResizeColumns();
                // 11320 sets the warehouse grd to a combo box and fills it with all active warehouses. also handles branches
                string sql = $@"
                SELECT  [OWHS].[WhsCode], [OWHS].[WhsName]
                 FROM[OWHS]
                LEFT JOIN OBPL ON OWHS.BPLid = OBPL.BPLid        --//10823
                 WHERE [OWHS].[Inactive] = 'N'";
                if (Globals.BranchDflt >= 0) sql += $" AND OBPL.BPLid IN ({Globals.BranchList})"; // 10823
                oGrd.Columns.Item("Warehouse").Type = BoGridColumnType.gct_ComboBox;
                ((SAPbouiCOM.ComboBoxColumn)oGrd.Columns.Item("Warehouse")).DisplayType = BoComboDisplayType.cdt_Value;
                CommonUI.ComboBox.ComboBoxAddVals(pForm, "grdSrcBy", "Warehouse", sql, " ");
                // 12753 Sets a default value of the warehouse combo box for each row in the grid to the default warehouse in the BOM for each item
                for (int i = 0; i < oGrd.Rows.Count; i++) // i will be the row index that I need to set the value so it should run to count - 1
                {
                    var itemCode = oGrd.DataTable.GetValue(0, i); //Commmented out so I can make a build, need
                    sql = $@"
                    SELECT DfltWH
                    FROM OITM
                    WHERE (ItemCode = '{itemCode}')";
                    string dfltWH = NSC_DI.UTIL.SQL.GetValue<string>(sql);
                    CommonUI.ComboBox.SetVal(pForm, "grdSrcBy", dfltWH, BoSearchKey.psk_ByValue, "Warehouse", i);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void ReSize_Form(Form pForm)
        {
            SAPbouiCOM.Item oItm = null;

            try
            {
                pForm.Freeze(true);
                pForm.Items.Item("grdSrcBy").Specific.AutoResizeColumns();

                var aa = oItm.Left + oItm.Width + 34;
                //pForm.Width = pForm.Width;  // oItm.Left + oItm.Width + 34;
            }
            catch (Exception ex)
            {
                //Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));   // the fields do not exist when the for m is first opened
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }
                
        private void Press_OK()
        {
            SAPbouiCOM.Form oForm = null;
            SAPbouiCOM.DataTable oDT = null;

            try
            {
                //pForm.Close();
               oForm = B1Connections.theAppl.Forms.GetForm(cFormID, 1);

                oDT = oForm.Items.Item("grdSrcBy").Specific.DataTable;
                if (oDT.Rows.Count <= 0) return;

                // convert to a VS dataTable
                System.Data.DataTable newDT = new System.Data.DataTable("ByProd");
                newDT.Columns.Add("ItemCode", typeof(string));
                newDT.Columns.Add("ItemName", typeof(string));
                newDT.Columns.Add("PlannedQty", typeof(double));
                newDT.Columns.Add("Warehouse", typeof(string));
                newDT.Columns.Add("LineNum", typeof(string));
                newDT.Columns.Add("BatchID", typeof(string));
                newDT.Columns.Add("StateID", typeof(string));

                for (var i = 0; i < oDT.Rows.Count; i++)
                {
                    var z = oDT.GetValue("PlannedQty", i);

                    if (oDT.GetValue("Warehouse", i) == "") // 11320 checks to see if the user has entered in a valid warehouse
                    {
                        Exception e = new Exception($"You need to select a warehouse to send {oDT.GetValue("ItemName", i)} to. If you do not want to report byproducts please remove the line item from the production order.");
                        throw (e);
                    }
                    if (oDT.GetValue("PlannedQty", i) < 0d)
                    {
                        Exception e = new Exception("Byproducts must be greater than zero. If you do not want to report byproducts please remove the line item from the production order.");
                        throw (e);
                    }
                    else                                        
                        newDT.Rows.Add(oDT.GetValue("ItemCode", i), oDT.GetValue("ItemName", i), oDT.GetValue("PlannedQty", i), oDT.GetValue("Warehouse", i), oDT.GetValue("LineNum", i), oDT.GetValue("BatchID", i), oDT.GetValue("StateID", i));                   
                }
                oForm.Close();              

                ByProdsCB(_callingFormUid, newDT);                
            }
            catch (Exception ex)
            {
                //Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }
    }
}