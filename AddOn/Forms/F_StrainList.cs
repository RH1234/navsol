﻿using System;
using System.Collections.Generic;
using System.IO;
using B1WizardBase;
using SAPbouiCOM;
using SAPbobsCOM;
using Matrix = NavSol.CommonUI.Matrix;

namespace NavSol.Forms
{
	public class F_StrainList : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_STRAIN_LS";

		public class CSV_Strain
		{
			public string Name { get; set; }
			public string Type { get; set; }
		}
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "BTN_ADD":
					BTN_ADD(oForm);
					break;

				case "BTN_FIND":
					Load_Matrix_Main(oForm);
					break;

				case "BTN_IMPORT":
					BTN_IMPORT_Click();
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "mtx_plan":
					mtx_plan_Matrix_Link_Pressed(oForm, pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			try
			{
				pForm.Freeze(true);

				CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_Main", @"list-icon.bmp");
				Load_Matrix_Main(pForm);

				// Load the total count of strains.
				var cnt = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT COUNT(*) FROM[@{NSC_DI.Globals.tStrains}]");
				pForm.Items.Item("txt_total").Specific.Value = cnt;

				cnt = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT COUNT(*) FROM [@{NSC_DI.Globals.tStrains}] WHERE DATEPART(YEAR, [U_CreateDate]) = '{DateTime.Now.Year}'");
				pForm.Items.Item("txt_crnt").Specific.Value = cnt;

                // hide the import button for now...
                pForm.Items.Item("BTN_IMPORT").Visible = false;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.VisibleEx = true;
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);

				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

        public static void Refresh(Form pForm)
        {
            pForm.Items.Item("txt_name").Specific.Value = String.Empty;
            FormSetup(pForm);
        }

		private static void Load_Matrix_Main(Form pForm)
		{
			try
			{
				var name = pForm.Items.Item("txt_name").Specific.Value.ToString();
				if (name == "")
				{
					Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tStrains, "mtx_plan", new List<Matrix.MatrixColumn>()
						{new Matrix.MatrixColumn() {Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
						 new Matrix.MatrixColumn() {Caption = "Name", ColumnName = "Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}, //ColumnName = "U_StrainName"
						 new Matrix.MatrixColumn() {Caption = "Type", ColumnName = "U_Type", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}}, 
						 @"SELECT * FROM [@" + NSC_DI.Globals.tStrains + @"]"); // ORDER BY DocEntry DESC
                }
				else
				{
					Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tStrains, "mtx_plan", new List<Matrix.MatrixColumn>()
						{new Matrix.MatrixColumn() {Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
						 new Matrix.MatrixColumn() {Caption = "Name", ColumnName = "Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}, //ColumnName = "U_StrainName"
						 new Matrix.MatrixColumn() {Caption = "Type", ColumnName = "U_Type", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}},
						 @"SELECT * FROM [@" + NSC_DI.Globals.tStrains + "] WHERE [Name] LIKE '%" + name.Replace("'", "''") + "%'"); // WHERE [U_StrainName] ORDER BY DocEntry DESC
                }
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private void BTN_ADD(Form pForm)
		{
			try
			{
				var val = pForm.Items.Item("txt_name").Specific.Value.ToString();
				F_StrainInfo.FormCreate_Load(val, false, pForm.UniqueID);
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
		}

		private void BTN_IMPORT_Click()
		{
			try
			{
				var ListOfStrains = NSC_DI.UTIL.SQL.DataTable("SELECT * FROM " + NSC_DI.Globals.tAutoItem);

				// Create a Progress Bar
				//oProgBar = Globals.oApp.StatusBar.CreateProgressBar("Starting to create items.", ListOfStrains.Count, false);

				foreach (System.Data.DataRow dr in ListOfStrains.Rows)
				{
					var Strain = new {Name = dr["U_Name"] as string, Type = dr["U_Type"] as string};

					// Prepare to run a SQL statement.
					SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

					// Count how many current records exist within the database.
					oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tStrains + "] ");

					// The single digigt plant type is "S", "H", or "I".  For "Sativa", "Hybrid", and "Indica" respectivley.
					string PlantTypeSingleDigitCode = Strain.Type;
					string NewStrainID = oRecordSet.Fields.Item(0).Value.ToString();
					string StrainName = Strain.Name;
					string StrainType = "";

					switch (Strain.Type)
					{
						case "I":
							StrainType = "Indica Dominant";
							break;
						case "S":
							StrainType = "Sativa Dominant";
							break;
						case "H":
							StrainType = "Hybrid";
							break;
					}

					// Create Strain UDO
					NSC_DI.SAP.Strain.CreateStrain(NewStrainID, StrainName, StrainType);

					// Attempt to add the new strain items
					NSC_DI.SAP.Strain.AddItemsToStrain(NewStrainID, StrainName);

					// oProgBar.Text = "Strain " + StrainName + " created!";

					//oProgBar.Value += 1;
				}

				// oProgBar.Stop();
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private void mtx_plan_Matrix_Link_Pressed(Form pForm, ItemEvent pVal)
		{
			if (pVal.Row <= 0) return;

			SAPbouiCOM.Matrix oMat = null;

			try
			{
				oMat = pForm.Items.Item("mtx_plan").Specific;
				var val = oMat.GetCellSpecific(0, pVal.Row).Value.ToString();
				F_StrainInfo.FormCreate_Find(val, false, pForm.UniqueID);
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oMat);
				GC.Collect();
			}
		}

		private static string AskForFile()
		{
			try
			{
				var ofd = new System.Windows.Forms.OpenFileDialog();
				var y = ofd.ShowDialog();

				return ofd.FileName;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
		}

	}
}