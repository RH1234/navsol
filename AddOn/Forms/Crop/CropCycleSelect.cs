﻿using System;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms.Crop
{
    class CropCycleSelect : B1Event
    {
        // ---------------------------------- VARS, CLASSES --------------------------------------------------------
        const string cFormID = "NSC_CROPSEL";

        private static string _callingFormUid = String.Empty;
        public static CropProjectCallBack CropProjCB = null;
        public delegate void CropProjectCallBack (string callingFormUid, System.Data.DataTable pdtCropProj);

        // ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_VALIDATE

        // ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_FORM_RESIZE
        [B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
        public virtual void OnAfterFormResize(ItemEvent pVal)
        {
            if (pVal.InnerEvent) return;
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            ReSize_Form(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "1") Press_OK();


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CLICK
        [B1Listener(BoEventTypes.et_CLICK, false, new string[] { cFormID })]
        public virtual void OnAfterClick(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "1") Press_OK();

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //---------------------------------------------- SUBS       --------------------------------------------------------
        private static string FormXML()
        {
            string xml = "";
            xml = @"<?xml version='1.0' encoding='UTF-16' ?>
<Application>
    <forms>
    <action type='add'>
        <form appformnumber='(NSC_FORM)' FormType='(NSC_FORM)' type='0' BorderStyle='0' uid='(NSC_FORM)' title='Active Crop Cycles:' visible='1' default_button='1' pane='0' color='0' left='384' top='86' width='610' height='542' client_width='1469' client_height='523' AutoManaged='1' SupportedModes='15' ObjectType='' modality='0'> 
            <datasources>
                <dbdatasources>
                    <action type='add'/>
                </dbdatasources>
                <userdatasources>
                    <action type='add'/>
                </userdatasources>
            </datasources>
            <Menus><action type='enable'>
                <Menu uid='5890'/>
                </action><action type='disable'/>
            </Menus>
            <items>
                <action type='add'>
                    <item uid='1' type='4' left='15' tab_order='0' width='74' top='479' height='19' visible='1' enabled='1' from_pane='0' to_pane='0' disp_desc='0' right_just='0' description='' linkto='' forecolor='-1' backcolor='-1' text_style='0' font_size='-1' supp_zeros='0' AffectsFormMode='1'>
                        <AutoManagedAttribute/>
                        <specific caption='Crops'/>
                    </item>
                        <item uid='2' type='4' left='96' tab_order='0' width='65' top='479' height='19' visible='1' enabled='1' from_pane='0' to_pane='0' disp_desc='0' right_just='0' description='' linkto='' forecolor='-1' backcolor='-1' text_style='0' font_size='-1' supp_zeros='0' AffectsFormMode='1'>
                            <AutoManagedAttribute/>
                            <specific caption='Cancel'/>
                        </item>
                        <item uid='12' type='8' left='15' tab_order='0' width='96' top='16' height='14' visible='1' enabled='1' from_pane='0' to_pane='0' disp_desc='0' right_just='0' description='' linkto='' forecolor='-1' backcolor='-1' text_style='0' font_size='-1' supp_zeros='0' AffectsFormMode='1'>
                            <AutoManagedAttribute/><specific caption='Crop Cycles:'/>
                        </item>
                        <item uid='grdSrcCrop' type='128' left='15' tab_order='0' width='570' top='60' height='403' visible='1' enabled='1' from_pane='0' to_pane='0' disp_desc='0' right_just='0' description='' linkto='' forecolor='-1' backcolor='-1' text_style='0' font_size='-1' supp_zeros='0' AffectsFormMode='1' IsAutoGenerated='0'>
                                <specific SelectionMode='2' DataTable='' CollapseLevel='0'>
                                <RowHeaders Width='20'/><GridColumns/>
                            </specific>
                        </item>

                        </action>
                    </items>
            </form>
        </action>
    </forms>
</Application>
";
            return xml;
        }

        public static void FormCreate(string pCallingFormUID, SAPbouiCOM.DataTable pSrcDT)
        {
            Form oForm = null;
            Item oItm = null;

            try
            {
                var formStr = FormXML();
                formStr = formStr.Replace("(NSC_FORM)", cFormID);
                oForm = CommonUI.Forms.LoadXML_Modal(formStr);

                _callingFormUid = pCallingFormUID;

                oForm.Freeze(true);
                //to do
                FormSetup(oForm);
                //to do
                FormLoad(oForm, pSrcDT);

                oForm.Mode = BoFormMode.fm_OK_MODE;


            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oForm.Freeze(false);
                oForm.VisibleEx = true;
                NSC_DI.UTIL.Misc.KillObject(oForm);
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private static void FormSetup(Form pForm)
        {
            try
            {
                pForm.DataSources.DataTables.Add("dtSrcCrop");
                pForm.Items.Item("grdSrcCrop").Specific.DataTable = pForm.DataSources.DataTables.Item("dtSrcCrop");
                pForm.Items.Item("grdSrcCrop").Specific.SelectionMode = BoMatrixSelect.ms_Auto;
                pForm.Items.Item("grdSrcCrop").Enabled = true;


            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.VisibleEx = true;
            }
        }

        private static void FormLoad(Form pForm, SAPbouiCOM.DataTable dtSource)
        {
            Grid oGrd = null;

            try
            {

                //-------------------------------
                // SOURCE GRID
                oGrd = pForm.Items.Item("grdSrcCrop").Specific;
                oGrd.DataTable.CopyFrom(dtSource);
                oGrd.SelectionMode = BoMatrixSelect.ms_Single;

                // only the Quantity column is editable
                for (var i = 0; i < oGrd.Columns.Count; i++)
                {
                    oGrd.Columns.Item(i).Editable = false;
                }
                oGrd.Columns.Item("Crop ID").Visible = true;
                oGrd.Columns.Item("NAME").Visible = true;
                oGrd.Columns.Item("Start Date").Visible = true;
                oGrd.Columns.Item("End Date").Visible = true;
                pForm.Items.Item("grdSrcCrop").Specific.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void ReSize_Form(Form pForm)
        {
            SAPbouiCOM.Item oItm = null;

            try
            {
                pForm.Freeze(true);
                pForm.Items.Item("grdSrcCrop").Specific.AutoResizeColumns();
                

                var aa = oItm.Left + oItm.Width + 34;
                //pForm.Width = pForm.Width;  // oItm.Left + oItm.Width + 34;
            }
            catch (Exception ex)
            {
                //Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));   // the fields do not exist when the for m is first opened
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private void Press_OK()
        {
            SAPbouiCOM.Form oForm = null;
            SAPbouiCOM.DataTable oDT = null;

            try
            {
                //pForm.Close();
                oForm = B1Connections.theAppl.Forms.GetForm(cFormID, 1);
                SAPbouiCOM.Grid Grid_Holder = oForm.Items.Item("grdSrcCrop").Specific; 
                oDT = oForm.Items.Item("grdSrcCrop").Specific.DataTable;

                if (oDT.Rows.Count <= 0) return;

                // convert to a VS dataTable
                System.Data.DataTable newDT = new System.Data.DataTable("CropProjD");
                newDT.Columns.Add("Crop ID", typeof(string));
                newDT.Columns.Add("NAME", typeof(string));
                newDT.Columns.Add("Start Date", typeof(DateTime));
                newDT.Columns.Add("End Date", typeof(DateTime));
                //
                for (var i = 0; i < oDT.Rows.Count; i++)
                {
                    if (Grid_Holder.Rows.IsSelected(i)){
                        newDT.Rows.Add(oDT.GetValue("Crop ID", i), oDT.GetValue("NAME", i), oDT.GetValue("Start Date", i), oDT.GetValue("End Date", i));
                    }
                }

                oForm.Close();

                CropProjCB(_callingFormUid, newDT);


            }
            catch (Exception ex)
            {
                //Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }





    }
}
