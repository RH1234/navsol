﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Crop
{

    /// <summary>
    /// This class manages the essentials for the implementation of crop cycle management into the application. 
    /// The essence is to incorporate SAP projects into the process flow from propogation to harvest.
    /// The setting that triggers this functionality is found in the users options and is called "Crop Cycle Management."
    /// It is a boolean value. 
    /// </summary>
    /// 

    class CropCycleManagement : B1Event
    {
        #region GlobalVariables
        public Application _SBO_Application;
        public SAPbobsCOM.Company _SBO_Company;
        public Form _SBO_Form;
        public int CropID { get; set; }
        public string CropName { get; set; }
        public string CropFinProjName { get; set; }

        const string cFormID = "NSC_CROP_MGT";
        private static System.Data.DataTable _dtFinProj = new System.Data.DataTable("FinProj");

        private static ProgressBar _progressBar;

        #endregion

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        #endregion BEFORE EVENT
        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_ITEM_LostFocus
        [B1Listener(BoEventTypes.et_LOST_FOCUS, false, new string[] { cFormID })]
        public virtual void OnAfterEditLostFocus(ItemEvent pVal)
        {
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            LOST_FOCUS(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_ITEM_LINK_PRESSED
        [B1Listener(BoEventTypes.et_CLICK, false, new string[] { cFormID })]
        public virtual void OnClick(ItemEvent pVal)
        {
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CLICK(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CHOOSE_FROM_LIST(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion

        #region event_handling
        private void LOST_FOCUS(Form pForm, ItemEvent pVal)
        {
            Matrix MX = null;
            EditText oEdit = null;

            try
            {
                switch (pVal.ItemUID)
                {
                    case "TXT_PROJS":
                        EditText oStartDate = pForm.Items.Item("TXT_PROJS").Specific;
                        MX = pForm.Items.Item("MTX_STAGE").Specific;
                        oEdit = MX.GetCellSpecific(3, 1);
                        oEdit.Value = oStartDate.Value;
                        break;
                    case "TXT_PROJE":
                        EditText oEndDateProj = pForm.Items.Item("TXT_PROJE").Specific;
                        MX = pForm.Items.Item("MTX_STAGE").Specific;
                        oEdit = MX.GetCellSpecific(4, 5);
                        oEdit.Value = oEndDateProj.Value;
                        break;
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(MX);
                NSC_DI.UTIL.Misc.KillObject(oEdit);
                GC.Collect();
            }
        }

        private void CLICK(Form pForm, ItemEvent pVal)
        {
            EditText oEdit = null;

            try
            {
                switch (pVal.ItemUID)
                {
                    //case "BTN_NEWPRO":
                    //    //Attempt to opent he financial project menu item.
                    //    Globals.oApp.Menus.Item("8457").Activate();
                    //    break;
                    case "BTN_SELPRO":
                        var pFormID = pForm.UniqueID; // Save unique id to get form in callback
                        SAPbouiCOM.DataTable dtFinProj = null;
                        try
                        {
                            if (pForm.DataSources.DataTables.Item("FinSrc").IsEmpty == false)
                            {
                                pForm.DataSources.DataTables.Item("FinSrc").Clear();
                                dtFinProj = pForm.DataSources.DataTables.Item("FinSrc");
                            }
                        }
                        catch
                        {
                            dtFinProj = pForm.DataSources.DataTables.Add("FinSrc");
                        }

                        var sql = @"
SELECT OPRJ.PrjCode, OPRJ.PrjName, OPRJ.ValidFrom, OPRJ.ValidTo 
  FROM OPRJ 
 WHERE OPRJ.Active='Y'";
                        dtFinProj.ExecuteQuery(sql);

                        // open the form; 
                        CropCycleSelect.FormCreate(pFormID, dtFinProj);

                        CropCycleSelect.CropProjCB = delegate (string callingFormUid, System.Data.DataTable pdtFinProj)
                        {
                            Form oFormDel = null;

                            oFormDel = Globals.oApp.Forms.Item(pFormID) as Form;
                            if (pdtFinProj.Rows.Count == 0) return;

                            System.Data.DataRow row = pdtFinProj.Rows[0];
                            var strFinProjCode = row.ItemArray.GetValue(0);
                            var strFinProjName = row.ItemArray.GetValue(1);

                            oEdit = pForm.Items.Item("TXT_FIN").Specific;
                            oEdit.Value = strFinProjName.ToString();
                            oEdit = pForm.Items.Item("TXT_FINC").Specific;
                            oEdit.Value = strFinProjCode.ToString();
                        };

                        break;

                    case "BTN_NEWCRO":
                        try
                        {
                            // Prepare a new progress bar
                            _progressBar = Globals.oApp.StatusBar.CreateProgressBar("Creating Your Crop!", 4, false);

                            this.UpdateProgressBar("Building Your Crop Stages.", 1);

                            CreateNewCropCycle_Project(pForm);
                            this.UpdateProgressBar("You Have Successfully Created Your Crop.", 1);

                            CleanUpNewCropForm(pForm);
                            this.UpdateProgressBar("Enjoy Your Grow!", 1);
                        }
                        catch
                        { }
                        finally
                        {
                            try
                            {
                                _progressBar.Stop();
                            }
                            catch { }
                        }
                        break;
                    case "BTN_CLSCRO":
                        Globals.oApp.MessageBox("Are you sure you want to close the General production order for this stage?");
                        //Close_Stage(pVal.FormUID);
                        break;
                    case "TAB_CROP":
                        //nothing
                        break;

                    case "TAB_ACT":
                        UpdateCropDG(pForm, "Active");
                        break;

                    case "TAB_FIN":
                        UpdateCropDG(pForm, "Finished");

                        break;

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oEdit);
                GC.Collect();
            }
        }

        private void CHOOSE_FROM_LIST(Form pForm, ItemEvent pVal)
        {
            Matrix MX = null;
            EditText oEdit = null;

            try
            {
                switch (pVal.ItemUID)
                {
                    // Employee ChooseFromList
                    case "TXT_OWN":
                        TXT_OWN_ChooseFromList_Selected(pForm, (IChooseFromListEvent)pVal);
                        break;
                    case "TXT_FIN":
                        TXT_FIN_ChooseFromList_Selected(pForm, (IChooseFromListEvent)pVal);
                        break;
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(MX);
                NSC_DI.UTIL.Misc.KillObject(oEdit);
                GC.Collect();
            }
        }
        #endregion

        #region subs-Form
        public int Close_Stage(string pFormID)
        {
            int retCode = 0;
            var oForm = B1Connections.theAppl.Forms.Item(pFormID);
            Grid oGrid = oForm.Items.Item("DG_PRO").Specific;
            SelectedRows SR = oGrid.Rows.SelectedRows;

            string strProjName = oForm.DataSources.DataTables.Item("dtPRO").GetValue("Crop Name", oGrid.GetDataTableRowIndex(SR.Item(0, BoOrderType.ot_SelectionOrder) + 1));
            //Globals.oApp.MessageBox(HstrProjName);
            int StageID = oForm.DataSources.DataTables.Item("dtPRO").GetValue("StageID", oGrid.GetDataTableRowIndex(SR.Item(0, BoOrderType.ot_SelectionOrder) + 1));
            // Globals.oApp.MessageBox(StageID.ToString());
            string StageName = oForm.DataSources.DataTables.Item("dtPRO").GetValue("Stage Name", oGrid.GetDataTableRowIndex(SR.Item(0, BoOrderType.ot_SelectionOrder) + 1));
            if (Globals.oApp.MessageBox("Would you like to close the " + StageName + " stage for the " + strProjName + " crop?", 1, "OK", "Cancel") == 1)
            {
                //close stage
                Globals.oApp.MessageBox("closing Stage");
                string strSQLStage = @"select T0.AbsEntry from PMG1 T0
                                        inner join PMC2 T1 on T1.StageID = T0.StageID
                                        inner join PMC6 T2 on T0.Task = t2.TaskID
                                        inner join OPMG T3 on T0.AbsEntry = T3.AbsEntry
                                        where T3.Name = '" + strProjName + "' and T1.StageID = '" + StageID + "'";

                int AbsEntry = 0;
                AbsEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQLStage);

                //close production orders
                Globals.oApp.MessageBox("Close Production Orders");
                //TODO: Close the General Production Orders for the given stage and project. 

                CloseCropCycle_Stage(AbsEntry, StageID.ToString());

     
            }



            return retCode;

        }
        public int CloseCropCycle_Stage(int pAbsEntry, string pStagetype)
        {
            _SBO_Company = Globals.oCompany;
            SAPbobsCOM.CompanyService oCompServ = (SAPbobsCOM.CompanyService)_SBO_Company.GetCompanyService();
            SAPbobsCOM.ProjectManagementService pmgService = (SAPbobsCOM.ProjectManagementService)oCompServ.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectManagementService);
            SAPbobsCOM.PM_ProjectDocumentParams projectToUpdateParam = pmgService.GetDataInterface(SAPbobsCOM.ProjectManagementServiceDataInterfaces.pmsPM_ProjectDocumentParams);

            try
            {
                projectToUpdateParam.AbsEntry = pAbsEntry;
                SAPbobsCOM.PM_ProjectDocumentData project = pmgService.GetProject(projectToUpdateParam);
                SAPbobsCOM.PM_StagesCollection stagesCollection = project.PM_StagesCollection;
                //check if stage exist if so get stage ID
                string strSQLStage = @"select isnull((select Distinct T0.StageID from PMG1 T0
                                            inner join PMC2 T1 on T1.StageID = T0.StageID
                                            inner join PMC6 T2 on T0.Task = t2.TaskID
                                            where T0.AbsEntry = " + pAbsEntry + " and T1.StageID = '" + pStagetype + "'),0) as [StageID]";

                int intStageID = 0;
                intStageID = NSC_DI.UTIL.SQL.GetValue<int>(strSQLStage);
                //    string strSQLTask = @"Select isnull((Select Distinct T0.Task
                //                        from PMG1 T0
                //                        inner join PMC2 T1 on T1.StageID = T0.StageID
                //                        inner join PMC6 T2 on T0.Task = t2.TaskID
                //                        where T0.AbsEntry = " + pAbsEntry + " and T2.Name ='" + pTask + "'),0) as [TaskID]";

                //    int intTaskID = 0;
                //    intTaskID = NSC_DI.UTIL.SQL.GetValue<int>(strSQLTask);

                //    string strSQLDescription = @"Select isnull((Select T0.DSCRIPTION 
                //                        from PMG1 T0
                //                        inner join PMC2 T1 on T1.StageID = T0.StageID
                //                        inner join PMC6 T2 on T0.Task = t2.TaskID
                //                        where T0.AbsEntry = " + pAbsEntry + " and T2.Name ='" + pTask + "' and T0.DSCRIPTION Like'" + pDescription + "'),'0') as [Desc]";

                //    string strDesc = null;
                //    strDesc = NSC_DI.UTIL.SQL.GetValue<string>(strSQLDescription);

                SAPbobsCOM.PM_StageData stage = null;

                //    if (intStageID > 0 && intTaskID > 0 && strDesc == pDescription)
                //    {
                //AddProdToProject(pmgService, projectToUpdateParam, intStageID, pDocEntry, pDocNum);
                // pmgService.UpdateProject(project);
                //        return 1;
                //    }
                //    else
                //    {
                stage = stagesCollection.Item(intStageID);//1;


                stage.IsFinished = BoYesNoEnum.tYES;
                //        intTaskID = NSC_DI.UTIL.SQL.GetValue<int>("Select distinct T2.TaskID from PMC6 T2 where T2.Name = '" + pTask + "'");
                //        stage.StartDate = DateTime.Now;
                //        //stage.CloseDate = stage.StartDate.AddDays(30);
                //        stage.Task = intTaskID;//1;
                //        stage.Description = pDescription;
                //        stage.ExpectedCosts = pExpectedCost;// 150;
                //        stage.PercentualCompletness = pPercentComplete;
                //        stage.IsFinished = SAPbobsCOM.BoYesNoEnum.tYES;
                //        //stage.StageOwner = 5;
                //        //stage.AttachmentEntry = 1;
                //       stage = stagesCollection.Add();
                //        //stage.StageType = 2;
                //        //stage.StartDate = DateTime.Now.AddMonths(1);
                //        //stage.CloseDate = stage.StartDate.AddDays(30);
                //        //stage.Task = 2;
                //        //stage.Description = "StageWithDocByDI_02";
                //        //stage.ExpectedCosts = 250;
                //        //stage.PercentualCompletness = 8;
                //        //stage.IsFinished = SAPbobsCOM.BoYesNoEnum.tNO;
                //        //stage.StageOwner = 5;
                //        //stage.DependsOnStage1 = 1;
                //        //stage.StageDependency1Type = SAPbobsCOM.StageDepTypeEnum.sdt_Project;
                //        //stage.DependsOnStageID1 = 1;
                pmgService.UpdateProject(project);
                //        //AddProdToProject(pmgService, projectToUpdateParam, intStageID, pDocEntry, pDocNum);


                //}
            }

            catch (Exception ex)
            {
                Globals.oApp.MessageBox(ex.Message);
            }

            finally
            {
                if (pmgService != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(pmgService);
                }
            }

            return -1;

        }

        private void UpdateProgressBar(string Message, int ProgressToIncreaseBy = 0)
        {
            try
            {
                // Set progress bar message
                _progressBar.Text = Message;

                // If we are increasing the progress bar size
                if (ProgressToIncreaseBy > 0)
                {
                    // Calculate the new progress
                    int NewProgressBarValue = _progressBar.Value + ProgressToIncreaseBy;

                    // Make sure our new progress amount is not greater than the maxium allowed
                    if (NewProgressBarValue <= _progressBar.Maximum)
                    {
                        _progressBar.Value = NewProgressBarValue;
                    }
                }
            }
            catch { }
        }

        public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
        {
            Item oItm = null;
            SAPbouiCOM.Form oForm = null;
            try
            {
                //var formStr = FormXML();
                //formStr = formStr.Replace("(NSC_FORM)", cFormID);
                oForm = CommonUI.Forms.Load(cFormID, true);
                //var oForm = CommonUI.Forms.LoadXML(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
                {
                    oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
                    oItm.Top = -20;
                    oItm.Specific.Value = pCallingFormUID;
                }
                //!!!!!!!!!!!!!!!!!!MAKE SURE TO UNCOMMENT BELOW WHEN DONE!!!!!!!!!!!!!!!!!
                FormSetup(oForm);

                return oForm;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (oForm != null)
                {
                    oForm.Freeze(false);
                    oForm.VisibleEx = true;
                }                
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();                          
            }
        }

        private static void FormSetup(Form pForm)
        {
            try
            {
                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_MAIN", @"clone-icon.bmp");
                
                DataTable oDTPRO = pForm.DataSources.DataTables.Add("dtPRO");
                var sql = @"
SELECT DISTINCT PMC6.TaskID,PMC6.Name 
  FROM PMC6
 WHERE PMC6.Name IN ('Seed Generation', 'Clone Generation', 'Feed', 'Water', 'Prune', 'Treat', 'Other')";
                oDTPRO.ExecuteQuery(sql);
                DataTable oDTSTA = pForm.DataSources.DataTables.Add("dtSTA");
                oDTSTA.ExecuteQuery("Select distinct T2.TaskID,T2.Name from PMC6 T2 where T2.Name in ('Seed Generation', 'Clone Generation', 'Feed', 'Water', 'Prune', 'Treat', 'Other')");

                LoadStages_MX(pForm);
                //Form.Items.Item("MTX_ITEMS").Specific();
                SAPbouiCOM.EditText oEdit = (EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_REM", pForm);

                SAPbouiCOM.Grid oGrid = pForm.Items.Item("DG_PRO").Specific;
                //oGrid.CollapseLevel = 0;
                //oGrid.CommonSetting.EnableArrowKey = true;

                oGrid.SelectionMode = BoMatrixSelect.ms_Single;

                CommonUI.Forms.CreateUserDataSource(pForm, "TXT_FIN", "UDS_CRO", BoDataType.dt_SHORT_TEXT, 50);
                CommonUI.CFL.Create(pForm, "C_CRF", BoLinkedObject.lf_ProjectCodes);               
                pForm.Items.Item("TXT_FIN").Specific.ChooseFromListUID = "C_CRF";
                pForm.Items.Item("TXT_FIN").Specific.ChooseFromListAlias = "prjName";

                CommonUI.Forms.CreateUserDataSource(pForm, "TXT_OWN", "UDS_EMP", BoDataType.dt_SHORT_TEXT, 50);
                CommonUI.CFL.Create(pForm, "C_OW", BoLinkedObject.lf_Employee);
                pForm.Items.Item("TXT_OWN").Specific.ChooseFromListUID = "C_OW";
                pForm.Items.Item("TXT_OWN").Specific.ChooseFromListAlias = "lastName";
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
          
        }

//        private static string FormXML() { return @"<?xml version='1.0' encoding='UTF-16'?>
//<Application>
//  <forms>
//    <action type='add'>
//      <form appformnumber='(NSC_FORM)' FormType='(NSC_FORM)' type='0' BorderStyle='0' uid='' title='Crop Cycle Management' visible='0' default_button='1' pane='1' color='0' left='370' top='20' width='840' height='480' client_width='640' client_height='480' AutoManaged='1' SupportedModes='15' ObjectType='' mode='1'>
//        <datasources>
//          <DataTables>
//            <action type='add'>
//                <DataTable Uid='dtSTA'>
//                  <Query>Select distinct T2.TaskID,T2.Name from PMC6 T2 where T2.Name in ('Seed Generation', 'Clone Generation', 'Feed', 'Water', 'Prune', 'Treat', 'Other') </Query >
//                </DataTable >
//                <DataTable Uid='dtPRO'>
//                  <Query>Select distinct T2.TaskID,T2.Name from PMC6 T2 where T2.Name in ('Seed Generation', 'Clone Generation', 'Feed', 'Water', 'Prune', 'Treat', 'Other') </Query >
//                </DataTable >
//            </action>
//           </DataTables >
//          <dbdatasources>
//            <action type='add' />
//          </dbdatasources>
//          <userdatasources>
//            <action type='add'>
//              <datasource uid='TAB_CROP' type='9' size='3' />
//              <datasource uid='EDT1' type='10' size='0' />
//              <datasource uid='SDT1' type='10' size='0' />
//              <datasource uid='UDS_EMP' type='9' size='254'/>
//              <datasource uid='UDS_CRO' type='9' size='254'/>

//            </action>
//          </userdatasources>
//        </datasources>
//        <Menus />
//        <items>
//          <action type='add'>
//            <item top='10' left='10' width='100' height='100' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='' right_just='0' type='117' visible='1' uid='IMG_MAIN' IsAutoGenerated='0'>
//              <specific picture='' />
//            </item>
//            <item uid='Item_0' type='100' left='133' tab_order='0' width='625' top='29' height='410' visible='1' enabled='1' from_pane='0' to_pane='0' disp_desc='0' right_just='0' description='' linkto='IMG_MAIN' forecolor='-1' backcolor='-1' text_style='0' font_size='-1' supp_zeros='0' AffectsFormMode='1'>
//              <AutoManagedAttribute />
//              <specific />
//            </item>
//            <item top='10' left='133' width='100' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='IMG_MAIN' right_just='0' type='99' visible='1' uid='TAB_CROP' IsAutoGenerated='0'>
//              <specific pane='1' caption='New Crop' AutoPaneSelection='1'>
//                <databind databound='1' table='' alias='TAB_CROP' />
//              </specific>
//			</item>
//			<item top='10' left='233' width='100' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='IMG_MAIN' right_just='0' type='99' visible='1' uid='TAB_ACT' IsAutoGenerated='0'>
//				<specific pane='2' caption='Active Crops' AutoPaneSelection='1'>
//                 <databind databound='1' table='' alias='TAB_CROP' />
//                </specific>
//			</item>
//            <item top='10' left='333' width='100' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='IMG_MAIN' right_just='0' type='99' visible='1' uid='TAB_FIN' IsAutoGenerated='0'>
//			 <specific pane='3' caption='Finished Crops' AutoPaneSelection='1'>
//                <databind databound='1' table='' alias='TAB_CROP' />
//              </specific>
//            </item>







//            <item top='40' left='150' width='180' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='8' visible='1' uid='LBL_FIN' IsAutoGenerated='0'>
//                <specific caption='Financial Project (Required):' />
//            </item>
//            <!--item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='43' left='330' width='110' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='16' visible='1' uid='TXT_FIN' IsAutoGenerated='0'>
//                <specific ChooseFromListAlias = '' ChooseFromListIsAutoFill = '0' ChooseFromListUID = '' IsPassword = '0' supp_zeros = '0' />             
//            </item-->
//            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='43' left='330' width='110' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='' right_just='0' type='16' visible='1' uid='TXT_FIN' IsAutoGenerated='0'>
//              <specific ChooseFromListAlias='cfCR' ChooseFromListIsAutoFill='1' ChooseFromListUID='C_CRF' IsPassword='0' supp_zeros='0'>
//                <databind databound='1' table='' alias='UDS_CRO'/>
//              </specific>
//            </item>
//            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='43' left='440' width='1' height='1' AffectsFormMode='1' description='' disp_desc='0' enabled='0' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='16' visible='1' uid='TXT_FINC' IsAutoGenerated='0'>
//                <specific ChooseFromListAlias = '' ChooseFromListIsAutoFill = '0' ChooseFromListUID = '' IsPassword = '0' supp_zeros = '0' />             
//            </item>
//            <!--item top='40' left='450' width='65' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='4' visible='1' uid='BTN_SELPRO' IsAutoGenerated='0'>
//              <specific caption = 'Select Project' />
//             </item>
//            <item top = '40' left = '570' width = '165' height = '20' AffectsFormMode = '1' description = '' disp_desc = '0' enabled = '1' from_pane = '1' to_pane = '1' linkto = 'IMG_MAIN' right_just = '0' type = '4' visible = '1' uid = 'BTN_NEWPRO' IsAutoGenerated = '0'>
//             <specific caption = 'New Financial Project' />
//            </item-->                   
//            <item top='70' left='150' width='150' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='8' visible='1' uid='LBL_CROP' IsAutoGenerated='0'>
//                <specific caption='Crop Cycle Name (Required):' />
//            </item>
//            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='73' left='330' width='210' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='16' visible='1' uid='TXT_CROP' IsAutoGenerated='0'>
//                <specific ChooseFromListAlias = '' ChooseFromListIsAutoFill = '0' ChooseFromListUID = '' IsPassword = '0' supp_zeros = '0' />             
//            </item>
//            <item top='40' left='445' width='80' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='8' visible='1' uid='LBL_PROJS' IsAutoGenerated='0'>
//                <specific caption='Start Date:' />
//            </item>
//            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='43' left='500' width='80' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='16' visible='1' uid='TXT_PROJS' IsAutoGenerated='0'>
//                <specific >
//                  <databind databound='1' table='' alias='SDT1' />
//                </specific >
//            </item>
//            <item top='40' left='585' width='80' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='8' visible='1' uid='LBL_PROJE' IsAutoGenerated='0'>
//                <specific caption='End Date:' />
//            </item>
//            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='43' left='640' width='80' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='16' visible='1' uid='TXT_PROJE' IsAutoGenerated='0' pickertype='1'>
//                <specific >
//                  <databind databound='1' table='' alias='EDT1' />
//                </specific >             
//            </item>

//            <item top='70' left='585' width='80' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='8' visible='1' uid='LBL_OWN' IsAutoGenerated='0'>
//                <specific caption='Owner:' />
//            </item>
//            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='70' left='640' width='80' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='16' visible='1' uid='TXT_OWN' IsAutoGenerated='0'>
//                <specific ChooseFromListAlias='CFOW' ChooseFromListIsAutoFill='1' ChooseFromListUID='C_OW' IsPassword='0' supp_zeros='0'>
//                    <databind databound='1' table='' alias='UDS_EMP'/>
//                </specific>
//            </item>
//            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='95' left='725' width='1' height='1' AffectsFormMode='1' description='' disp_desc='0' enabled='0' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='16' visible='1' uid='TXT_OWNC' IsAutoGenerated='0'>
//                <specific />
//            </item>

//            <item top='95' left='150' width='280' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='8' visible='1' uid='LBL_STAGE' IsAutoGenerated='0'>
//                <specific caption='Crop Cycle Predicted Stage Timing:' />
//            </item>
//            <item cellHeight='16' tab_order='0' titleHeight='20' top='115' left='145' width='580' height='150' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='127' visible='1' uid='MTX_STAGE' IsAutoGenerated='0'>
//              <specific layout='0' SelectionMode='2'>
//                <columns>
//                  <action type='add'>
//                    <column backcolor='16777215' ChooseFromListIsAutoFill='0' font_size='-1' forecolor='-1' text_style='0' disp_desc='0' visible='1' AffectsFormMode='1' val_on='Y' IsAutoGenerated='0' val_off='N' title='#' width='25' editable='1' type='16' right_just='0' uid='#' sortable='0' />
//                    <column backcolor='-1' ChooseFromListIsAutoFill='0' font_size='-1' forecolor='-1' text_style='0' disp_desc='0' visible='1' AffectsFormMode='1' val_on='Y' IsAutoGenerated='0' val_off='N' title='Col_0' width='50' editable='1' type='16' right_just='0' uid='Col_0' sortable='0' />
//                  </action>
//                </columns>
//              </specific>
//            </item>
       
//            <item top='275' left='145' width='180' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='8' visible='1' uid='LBL_FIN' IsAutoGenerated='0'>
//                <specific caption='Inital Crop Remarks:' />
//            </item>
//            <item font_size='-1' supp_zeros='0' tab_order='0' text_style='' top='300' left='145' width='580' height='100' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='' right_just='0' type='118' visible='1' uid='TXT_REM' IsAutoGenerated='0'>
//                <specific ScrollBars = '2'/>   
//            </item>
//            <item top='410' left='350' width='165' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='1' to_pane='1' linkto='IMG_MAIN' right_just='0' type='4' visible='1' uid='BTN_NEWCRO' IsAutoGenerated='0'>
//              <specific caption = 'Create New Crop Cycle' />
//             </item>



//            <item top='30' left='150' width='100' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='2' to_pane='2' linkto='IMG_MAIN' right_just='0' type='8' visible='1' uid='LBL_cat' IsAutoGenerated='0'>
//                <specific caption='Current Crops:' />
//            </item>
//            <item top='410' left='350' width='165' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='2' to_pane='2' linkto='IMG_MAIN' right_just='0' type='4' visible='1' uid='BTN_CLSCRO' IsAutoGenerated='0'>
//              <specific caption = 'Close Crop Stage' />
//             </item>


//            <item top='50' left='145' width='600' height='300' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='2' to_pane='3' linkto='IMG_MAIN' right_just='0' type='128' visible='1' uid='DG_PRO' IsAutoGenerated='0'>
//                <specific CollapseLevel = '2' DataTable = 'dtPRO' SelectionMode = '0'>     
//                    <RowHeaders Width = '100' />      
//                </specific>      
//            </item>


//            <item top='30' left='150' width='100' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='3' to_pane='3' linkto='IMG_MAIN' right_just='0' type='8' visible='1' uid='LBL_cat2' IsAutoGenerated='0'>
//                <specific caption='Finished Crop Cycles:' />
//            </item>



//            <item top='418' left='10' width='100' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='IMG_MAIN' right_just='0' type='4' visible='1' uid='2' IsAutoGenerated='0'>
//              <specific caption='Cancel' />
//            </item>
//          </action>
//        </items>
//        <ChooseFromListCollection>
//          <action type='add'>
//            <ChooseFromList UniqueID='-1' ObjectType='-1' MultiSelection='0' IsSystem='1' />
//            <ChooseFromList UniqueID='C_OW' ObjectType='171' MultiSelection='0' IsSystem='0' />
//            <ChooseFromList UniqueID='C_CRF' ObjectType='63' MultiSelection='0' IsSystem='0' />
//          </action>
//        </ChooseFromListCollection>
//        <DataBrowser BrowseBy='' />
//        <Settings MatrixUID='' Enabled='0' EnableRowFormat='0' />
//        <items>
//          <action type='group'>
//            <item uid='TAB_CROP' />
//			<item uid='TAB_ACT' />
//			<item uid='TAB_FIN' />
//          </action>
//        </items>
//      </form>
//    </action>
//  </forms>

//</Application>
//"; }

        private static void TXT_FIN_ChooseFromList_Selected(Form pForm, IChooseFromListEvent pChooseFromListEvent)
        {
            SAPbouiCOM.ChooseFromList oChooseFromList = pForm.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

            if (pChooseFromListEvent.BeforeAction == false)
            {
                DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

                try
                {
                    string oFinCode = oDataTable.GetValue(0, 0).ToString();
                    string oFinName = oDataTable.GetValue(1, 0).ToString();

                    // Save the business partners code
                    ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_FINC", pForm))
                        .Value = oFinCode;

                    // Set the userdatasource for the name textbox
                    pForm.DataSources.UserDataSources.Item("UDS_CRO").ValueEx = Convert.ToString(oFinName);
                }
                catch (Exception ex)
                {
                    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                }
                finally
                {
                    Globals.oApp.Forms.ActiveForm.Close();
                    SAPbouiCOM.EditText oEdit = pForm.Items.Item("TXT_REM").Specific;
                    oEdit = pForm.Items.Item("TXT_REM").Specific;
                }
            }
        }

        private static void TXT_OWN_ChooseFromList_Selected(Form pForm, IChooseFromListEvent pChooseFromListEvent)
        {
            SAPbouiCOM.ChooseFromList oChooseFromList = pForm.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

            if (pChooseFromListEvent.BeforeAction == false)
            {
                DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

                try
                {
                    string oEmpCode = oDataTable.GetValue(0, 0).ToString();
                    string oEmpName = oDataTable.GetValue(1, 0).ToString();

                    // Save the business partners code
                    ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_OWNC", pForm))
                        .Value = oEmpCode;

                    // Set the userdatasource for the name textbox
                    pForm.DataSources.UserDataSources.Item("UDS_EMP").ValueEx = Convert.ToString(oEmpName);
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    Globals.oApp.Forms.ActiveForm.Close();
                    SAPbouiCOM.EditText oEdit =  pForm.Items.Item("TXT_REM").Specific;
                    oEdit = pForm.Items.Item("TXT_REM").Specific;
                }
            }
        }
        private static void LoadStages_MX(Form pForm)
        {
            string strSQL = @"SELECT T0.StageID,T0.Name,T0.Dscription,Getdate() as [Start_Date] ,Getdate() as [End_Date]
 FROM [dbo].[PMC2] T0
 where T0.Name in ('Intake Stage', 'Propagation Stage', 'Vegetative Stage', 'Flowering Stage', 'Harvest Stage')";

            // Prepare a Matrix helper
            VERSCI.Matrix MatrixHelper = new VERSCI.Matrix(SAPBusinessOne_Application: Globals.oApp, FormToUse: pForm);

            // Load the matrix of tasks
            MatrixHelper.LoadDatabaseDataIntoMatrix(
               DatabaseTableName: "PMC2",
                MatrixUID: "MTX_STAGE",
                ListOfColumns:
                    new List<VERSCI.Matrix.MatrixColumn>() {
                            new VERSCI.Matrix.MatrixColumn(){ Caption="Stage ID", ColumnWidth=40, ColumnName="StageID", ItemType= BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Stage Name", ColumnWidth=80, ColumnName="Name", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Stage Description", ColumnWidth=80, ColumnName="Dscription", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Start Date", ColumnWidth=80, ColumnName="Start_Date", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="End Date", ColumnWidth=80, ColumnName="End_Date", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}
                            ,//new VERSCI.Matrix.MatrixColumn(){ Caption="Closing Date", ColumnWidth=80, ColumnName="Closing_Date", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true},
   
                    },

                    SQLQuery: strSQL);
        }

        private static void UpdateCropDG(Form pForm,string pStatus)
        {
            SAPbouiCOM.Grid oGrid = null;

            try
            {
                pForm.Freeze(true);

                if (pStatus == "Active")
                {
                    pForm.DataSources.DataTables.Item("dtPRO").ExecuteQuery(@"Select T0.NAME as 'Crop Name',T3.Name as 'Stage Name',T4.Name as 'Task Name',Case when T1.FINISH='Y' then 'Yes' else 'No' end as 'Finished',T1.[START] as 'Start Date',
Case 
when T0.RISK = 'L' Then 'Low'
when T0.RISK = 'M' Then 'Low'
else 'HIGH' end as [Risk],T1.[CLOSE] as 'End Date' 
from [OPMG] T0 
join [PMG1] T1 on T0.AbsEntry =T1.AbsEntry
Join [PMC2] T3 on T1.StageID=T3.StageID
join [PMC6] T4 on T1.Task=T4.TaskID
where T0.STATUS!='F' and T0.STATUS !='N'
group by T0.AbsEntry,T1.StageID,T4.TaskID,T0.NAME,T3.Name,T4.Name,T1.FINISH,T0.RISK,T1.[START],T1.[CLOSE]
Order by T0.AbsEntry ASC,T1.StageID ASC, T4.TaskID ASC, T1.[START] DESC");

                }
                else
                {
                    pForm.DataSources.DataTables.Item("dtPRO").ExecuteQuery(@"Select T0.NAME as 'Crop Name',T3.Name as 'Stage Name',T4.Name as 'Task Name',Case when T1.FINISH='Y' then 'Yes' else 'No' end as 'Finished',T1.[START] as 'Start Date',
Case 
when T0.RISK = 'L' Then 'Low'
when T0.RISK = 'M' Then 'Low'
else 'HIGH' end as [Risk],T1.[CLOSE] as 'End Date' 
from [OPMG] T0 
join [PMG1] T1 on T0.AbsEntry =T1.AbsEntry
Join [PMC2] T3 on T1.StageID=T3.StageID
join [PMC6] T4 on T1.Task=T4.TaskID
where T0.STATUS='F''
group by T0.AbsEntry,T1.StageID,T4.TaskID,T0.NAME,T3.Name,T4.Name,T1.FINISH,T0.RISK,T1.[START],T1.[CLOSE]
Order by T0.AbsEntry ASC,T1.StageID ASC, T4.TaskID ASC, T1.[START] DESC");
                                 
                }
                oGrid                               = pForm.Items.Item("DG_PRO").Specific;
                oGrid.CommonSetting.EnableArrowKey  = true;
                oGrid.DataTable                     = pForm.DataSources.DataTables.Item("dtPRO");
                if (pForm.DataSources.DataTables.Item("dtPRO").Rows.Count < 1) return;
                oGrid.SelectionMode                 = BoMatrixSelect.ms_Single;
                oGrid.CollapseLevel                 = 2;
                oGrid.AutoResizeColumns();
                oGrid.Rows.CollapseAll();

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oGrid);
            }
        }

        private static void CleanUpNewCropForm(Form pForm)
        {
            try {
                pForm.Freeze(true);
                //ClearForm
                SAPbouiCOM.EditText oEdit_Holder = (EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_REM", pForm);
                oEdit_Holder.Value = "";
                oEdit_Holder = pForm.Items.Item("TXT_FIN").Specific;
                oEdit_Holder.Value = "";
                oEdit_Holder = pForm.Items.Item("TXT_FINC").Specific;
                oEdit_Holder.Value = "";
                oEdit_Holder = pForm.Items.Item("TXT_CROP").Specific;
                oEdit_Holder.Value = "";
                oEdit_Holder = pForm.Items.Item("TXT_PROJS").Specific;
                oEdit_Holder.Value = "";
                oEdit_Holder = pForm.Items.Item("TXT_PROJE").Specific;
                oEdit_Holder.Value = "";
                oEdit_Holder = pForm.Items.Item("TXT_OWN").Specific;
                oEdit_Holder.Value = "";
                oEdit_Holder = pForm.Items.Item("TXT_OWNC").Specific;
                oEdit_Holder.Value = "";
                oEdit_Holder = pForm.Items.Item("TXT_REM").Specific;
                oEdit_Holder.Value = "";

                //MTX_STAGE
                LoadStages_MX(pForm);

            }
            catch
            {

            }
            finally
            {
                pForm.Freeze(false);
            }
        }
        private static void CreateNewCropCycle_Project(Form pForm)
        {
            try
            {
                //Check to see if there is an exiting FInancial Project if not create it.
                string strFinProj = ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_FIN", pForm)).Value.ToString();
                string strCropProj = ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_CROP", pForm)).Value.ToString();
                string strFinProjCODE = ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_FINC", pForm)).Value.ToString();
                string strRemarks = ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_REM", pForm)).Value.ToString();
                string strSDAtE = pForm.DataSources.UserDataSources.Item("SDT1").ValueEx;
                string strEDAtE = pForm.DataSources.UserDataSources.Item("EDT1").ValueEx;
                string strOWNER = ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_OWNC", pForm)).Value.ToString();

                if (strFinProj.Length == 0 || strCropProj.Length == 0)
                {
                    Globals.oApp.MessageBox("Please enter both a Financial Project and a Crop Cycle name.");
                    return;
                }
                // Check if Fin. Project Exists if not there was a error or typo in from deligate Check spelling error

                int FinRecCount = NSC_DI.UTIL.SQL.GetValue<int>("Select COUNT(PrjCode) from OPRJ where PrjName ='" + strFinProj + "'");
                if (FinRecCount == 0)
                {
                    Globals.oApp.MessageBox("Financial Project does not match system financial projects. Please check spelling.", 1);
                    return;
                }
                else
                {

                    //Create New Crop Cycle (Project)
                    int ret = CreateCropCycle(pForm, strFinProjCODE, strCropProj, strSDAtE, strEDAtE, strRemarks, strOWNER);
                    //Clear form
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        #endregion

        #region Subs-Data
        public int AddStages_DB()
        {
            int absEntryOfCreatedProject = -1;

            //Check to see if the standard stages exist.
            int iStageCount =  NSC_DI.UTIL.SQL.GetValue<int>("SELECT COUNT(T0.Name) FROM [dbo].[PMC2] T0 where T0.Name in ('Intake Stage','Propagation Stage','Vegetative Stage','Flowering Stage','Harvest Stage')");
            if (iStageCount < 5)
            {
                //Add the stages for crop cyle mangement to the Project Stages Setup table [PMC2]
                CompanyService oCompService = null;
                ProjectManagementConfigurationService oPMConfigServ = null;
                oCompService = Globals.oCompany.GetCompanyService() as CompanyService;
                try
                {
                    oPMConfigServ = oCompService.GetBusinessService(ServiceTypes.ProjectManagementConfigurationService);
                    SAPbobsCOM.PMC_StageTypeCollection oStageCol = (SAPbobsCOM.PMC_StageTypeCollection)oPMConfigServ.GetDataInterface(ProjectManagementConfigurationServiceDataInterfaces.pmcsPMC_StageTypeCollection);

                    oStageCol = oPMConfigServ.GetStageTypes();

                    int iCount = oStageCol.Count;

                    //SAP Initial Stages
                    List<string> SAP_Stages = new List<string> { "Conception/Initiation", "Definition/Planning", "Launch/Execution", "Performance and control", "Finishing Stage" };

                    for (int i = 0; i < iCount; i++)
                    {
                        if (SAP_Stages.Contains(oStageCol.Item(i).StageName))
                        {
                            switch (oStageCol.Item(i).StageName.Trim())
                            {
                                case "Conception/Initiation":
                                    oStageCol.Item(i).StageName = "Intake Stage";
                                    oStageCol.Item(i).StageDescription = "Tracks the aspects of intaking cannabis";
                                    break;
                                case "Definition/Planning":
                                    oStageCol.Item(i).StageName = "Propagation Stage";
                                    oStageCol.Item(i).StageDescription = "Tracks aspects of your propagation stage";
                                    break;
                                case "Launch/Execution":
                                    oStageCol.Item(i).StageName = "Vegetative Stage";
                                    oStageCol.Item(i).StageDescription = "Tracks aspects of your vegetation stage";
                                    break;
                                case "Performance and control":
                                    oStageCol.Item(i).StageName = "Flowering Stage";
                                    oStageCol.Item(i).StageDescription = "Tracks aspects of your flowering stage";
                                    break;
                                case "Finishing Stage":
                                    oStageCol.Item(i).StageName = "Harvest Stage";
                                    oStageCol.Item(i).StageDescription = "Tracks the aspects of your harvesting stage";
                                    break;

                            }
                        }
                        oPMConfigServ.UpdateStageTypes(oStageCol);
                    }

                    AddTasks_DB();

                }
                catch (Exception ex)
                {
                    Globals.oApp.MessageBox(ex.Message);
                }

                finally
                {
                    if (oPMConfigServ != null)
                    {
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oPMConfigServ);
                    }
                }
            }

            return absEntryOfCreatedProject;
        }

        public void AddTasks_DB()
        {
            //Add the Tasks for crop cyle mangement to the Project Tasks Setup table [PMC6]
            CompanyService oCompService = null;
            ProjectManagementConfigurationService oPMConfigServ = null;
            oCompService = Globals.oCompany.GetCompanyService() as CompanyService;
            try
            {
                oPMConfigServ = oCompService.GetBusinessService(ServiceTypes.ProjectManagementConfigurationService);
                SAPbobsCOM.PMC_TaskCollection oTaskCol = (SAPbobsCOM.PMC_TaskCollection)oPMConfigServ.GetDataInterface(ProjectManagementConfigurationServiceDataInterfaces.pmcsPMC_TaskCollection);

                //Update Tasks 
                //
                int iHold = oTaskCol.Count;
                oTaskCol.Add();
                oTaskCol.Item(iHold).TaskID = iHold+1;
                oTaskCol.Item(iHold).TaskName = "Seed Generation";

                iHold = oTaskCol.Count;
                oTaskCol.Add();
                oTaskCol.Item(iHold).TaskID = iHold + 1;
                oTaskCol.Item(iHold).TaskName = "Clone Generation";

                iHold = oTaskCol.Count;
                oTaskCol.Add();
                oTaskCol.Item(iHold).TaskID = iHold + 1;
                oTaskCol.Item(iHold).TaskName = "Feed";

                iHold = oTaskCol.Count;
                oTaskCol.Add();
                oTaskCol.Item(iHold).TaskID = iHold + 1;
                oTaskCol.Item(iHold).TaskName = "Water";

                iHold = oTaskCol.Count;
                oTaskCol.Add();
                oTaskCol.Item(iHold).TaskID = iHold + 1;
                oTaskCol.Item(iHold).TaskName = "Prune";

                iHold = oTaskCol.Count;
                oTaskCol.Add();
                oTaskCol.Item(iHold).TaskID = iHold + 1;
                oTaskCol.Item(iHold).TaskName = "Treat";

                iHold = oTaskCol.Count;
                oTaskCol.Add();
                oTaskCol.Item(iHold).TaskID = iHold + 1;
                oTaskCol.Item(iHold).TaskName = "Other";


                oPMConfigServ.AddTasks(oTaskCol);

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(ex.Message);
            }

            finally
            {
                if (oPMConfigServ != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oPMConfigServ);
                }
            }
        }

        public static int CreateCropCycle(Form pForm, string pFinPRoject, string pCropProject, string pStartDate, string pEndDate, string pReason = "", string pOwner ="0")
        {

            int absEntryOfCreatedProject = -1;
            CompanyService oCoService = null;
            ProjectManagementService oProjMngService = null;
            oCoService = Globals.oCompany.GetCompanyService() as CompanyService;
            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                oProjMngService = oCoService.GetBusinessService(ServiceTypes.ProjectManagementService);
                SAPbobsCOM.PM_ProjectDocumentData oProject = (SAPbobsCOM.PM_ProjectDocumentData)oProjMngService.GetDataInterface(SAPbobsCOM.ProjectManagementServiceDataInterfaces.pmsPM_ProjectDocumentData);
                oProject.ProjectName = pCropProject;
                if(pOwner!="0")
                {
                    oProject.Owner = Convert.ToInt32(pOwner);
                }
                oProject.StartDate = Convert.ToDateTime(pStartDate.Substring(4, 2) + "/" + pStartDate.Substring(6, 2) + "/" + pStartDate.Substring(0, 4));
                oProject.DueDate = Convert.ToDateTime(pEndDate.Substring(4, 2) + "/" + pEndDate.Substring(6, 2) + "/" + pEndDate.Substring(0, 4));
                oProject.ProjectType = SAPbobsCOM.ProjectTypeEnum.pt_Internal;
                oProject.AllowSubprojects = SAPbobsCOM.BoYesNoEnum.tYES;
                oProject.ProjectStatus = SAPbobsCOM.ProjectStatusTypeEnum.pst_Started;
                oProject.FinancialProject = pFinPRoject;
                oProject.RiskLevel = SAPbobsCOM.RiskLevelTypeEnum.rlt_Low;
                //oProject.Industry = 2;
                oProject.Reason = pReason;
                SAPbobsCOM.PM_ProjectDocumentParams projectParam = oProjMngService.AddProject(oProject);
                absEntryOfCreatedProject = projectParam.AbsEntry;


                //Add Stages and tast to the Crop Cycle
                AddStagesToCrop(pForm, absEntryOfCreatedProject);

                //Add Production Orders
                //NSC_DI.SAP.ProductionOrder.SpecialProductionOrderComponentLineItem oLines = new NSC_DI.SAP.ProductionOrder.SpecialProductionOrderComponentLineItem();
                //NSC_DI.SAP.ProductionOrder.CreateSpecialProdOrderAndIssue("GEN_FEED",1,10,

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
            }

            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                NSC_DI.UTIL.Misc.KillObject(oProjMngService);
                GC.Collect();
            }
            return absEntryOfCreatedProject;
        }

        private static int AddStagesToCrop(Form pForm, int pAbsEntry)
        {
            SAPbobsCOM.CompanyService oCompServ = (SAPbobsCOM.CompanyService)Globals.oCompany.GetCompanyService();
            SAPbobsCOM.ProjectManagementService pmgService = (SAPbobsCOM.ProjectManagementService)oCompServ.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectManagementService);
            SAPbobsCOM.PM_ProjectDocumentParams projectToUpdateParam = pmgService.GetDataInterface(SAPbobsCOM.ProjectManagementServiceDataInterfaces.pmsPM_ProjectDocumentParams);

            try
            {
                projectToUpdateParam.AbsEntry = pAbsEntry; //1;
                SAPbobsCOM.PM_ProjectDocumentData project = pmgService.GetProject(projectToUpdateParam);
                SAPbobsCOM.PM_StagesCollection stagesCollection = project.PM_StagesCollection;
  
                SAPbobsCOM.PM_StageData stage = null;
                // DataTable of all the items that we are staging.
                try
                {
                    if( pForm.DataSources.DataTables.Item("dtSTA").IsEmpty!=true)
                    {
                        //Do nothing this it already exists
                    }
                }
                catch
                {
                    //dtSTA does not exist so create it
                    pForm.DataSources.DataTables.Add("dtSTA").ExecuteQuery("Select distinct T2.TaskID,T2.Name from PMC6 T2 where T2.Name in ('Seed Generation','Clone Generation','Feed','Water','Prune','Treat','Other')");
                }

                SAPbouiCOM.DataTable oDT = pForm.DataSources.DataTables.Item("dtSTA");
                SAPbouiCOM.Matrix oMX = pForm.Items.Item("MTX_STAGE").Specific;
                SAPbouiCOM.EditText oEdit = null;
                
                //Add Stages
                for(int i=1;i<=oMX.RowCount;i++)
                {
                    for(int j = 1; j<=oDT.Rows.Count;j++)
                    {
                        //Add a stage for each task 
                        stage = stagesCollection.Add();
                        oEdit = oMX.GetCellSpecific(0, i);
                        stage.StageType = Convert.ToInt32(oEdit.Value);
                        oEdit = oMX.GetCellSpecific(3, i);
                        stage.StartDate = Convert.ToDateTime(oEdit.Value.Substring(4, 2) + "/" + oEdit.Value.Substring(6, 2) + "/" + oEdit.Value.Substring(0, 4));
                        oEdit = oMX.GetCellSpecific(4, i);
                        stage.CloseDate = Convert.ToDateTime(oEdit.Value.Substring(4, 2) + "/" + oEdit.Value.Substring(6, 2) + "/" + oEdit.Value.Substring(0, 4));
                        //stage.StageOwner = 5;
                        stage.Task = oDT.GetValue(0, j-1);
                        oEdit = oMX.GetCellSpecific(1, i);
                        string strStageName = oEdit.Value;
                        string strUNI_Holder = SetStageUniqueID(strStageName, oDT.GetValue(1, j - 1), pAbsEntry);
                        stage.UniqueID = strUNI_Holder;


                    }
                }

                pmgService.UpdateProject(project);

                //Add ProductionOrders
                for (int i = 1; i <= oMX.RowCount; i++)
                {
                    for (int j = 1; j <= oDT.Rows.Count; j++)
                    {
                        //Add a stage for each task 
                        //oEdit = oMX.GetCellSpecific(0, i);
                        //stage.StageType = Convert.ToInt32(oEdit.Value);
                        oEdit = oMX.GetCellSpecific(3, i);
                        DateTime StartDate = Convert.ToDateTime(oEdit.Value.Substring(4, 2) + "/" + oEdit.Value.Substring(6, 2) + "/" + oEdit.Value.Substring(0, 4));
                        oEdit = oMX.GetCellSpecific(4, i);
                        DateTime CloseDate = Convert.ToDateTime(oEdit.Value.Substring(4, 2) + "/" + oEdit.Value.Substring(6, 2) + "/" + oEdit.Value.Substring(0, 4));

                        //stage.Task = oDT.GetValue(0, j - 1);
                        oEdit = oMX.GetCellSpecific(1, i);
                        string strStageName = oEdit.Value;
                        string strUNI_Holder = SetStageUniqueID(strStageName, oDT.GetValue(1, j - 1), pAbsEntry);
                        stage.UniqueID = strUNI_Holder;
                        string[] strHolder = strUNI_Holder.Split('_');
                        string strItemCode = "GEN_" + strHolder[0].ToString() + "_" + strHolder[1].ToString();
                        string strProcess = strHolder[1].ToString();

                        int Key = 0;
                        if (strStageName == "Vegetative Stage" || strStageName == "Flowering Stage")
                        {
                            oEdit = pForm.Items.Item("TXT_FINC").Specific;
                            string CropFinID = oEdit.Value;

                            Key = NSC_DI.SAP.ProductionOrder.CreateSpecialCropProdOrder(strItemCode, 1, "10",CloseDate,StartDate, true, "N", strProcess, strUNI_Holder, CropFinID);

                            //Add the new Stage Specific Production Order to the Project
                            CropCycleManagement oCrop = new CropCycleManagement();
                            int ProdDocNum = NSC_DI.UTIL.SQL.GetValue<int>("Select DocNum from OWOR where DocENtry=" + Key);
                            string ProjDocNum = NSC_DI.UTIL.SQL.GetValue<string>("Select DocNum from OPMG where AbsEntry=" + pAbsEntry);

                            oCrop.AddProdToProject(Key, ProdDocNum, ProjDocNum, strUNI_Holder);
                        }
                    }
                }

            }

            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

            finally
            {
                if (pmgService != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(pmgService);
                }
            }

            return -1;

        }

        private static string SetStageUniqueID(string pStageName,string pTaskName,int pAbsEntry)
        {
            string retCode = null;

            switch (pStageName)
            {
               
                case "Intake Stage":
                    retCode = "IN_";
                    break;
                case "Propagation Stage":
                    retCode = "PROP_";
                    break;
                case "Vegetative Stage":
                    retCode = "VEG_";
                    break;
                case "Flowering Stage":
                    retCode = "FLOW_";
                    break;
                case "Harvest Stage":
                    retCode = "HARV_";
                    break;
            }

            switch (pTaskName)
            {
                case "Seed Generation":
                    retCode += "SEED";
                    break;
                case "Clone Generation":
                    retCode += "CLONE";
                    break;
                case "Feed":
                    retCode += "FEED";
                    break;
                case "Water":
                    retCode += "WATER";
                    break;
                case "Prune":
                    retCode += "PRUNE";
                    break;
                case "Treat":
                    retCode += "TREAT";
                    break;
                case "Other":
                    retCode += "OTHER";
                    break;
            }

            retCode += "_" + pAbsEntry.ToString();
            return retCode;
        }

        public int UpdateCropCycle_StageAndDoc(int pAbsEntry, string pStagetype, string pTask, string pDescription, double pExpectedCost, int pPercentComplete, SAPbobsCOM.PMDocumentTypeEnum pDocType, int pDocEntry)
        {
            _SBO_Company = Globals.oCompany;
            SAPbobsCOM.CompanyService oCompServ = (SAPbobsCOM.CompanyService)_SBO_Company.GetCompanyService();
            SAPbobsCOM.ProjectManagementService pmgService = (SAPbobsCOM.ProjectManagementService)oCompServ.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectManagementService);
            SAPbobsCOM.PM_ProjectDocumentParams projectToUpdateParam = pmgService.GetDataInterface(SAPbobsCOM.ProjectManagementServiceDataInterfaces.pmsPM_ProjectDocumentParams);

            
            try
            {
                projectToUpdateParam.AbsEntry = pAbsEntry; //1;
                SAPbobsCOM.PM_ProjectDocumentData project = pmgService.GetProject(projectToUpdateParam);
                SAPbobsCOM.PM_StagesCollection stagesCollection = project.PM_StagesCollection;
                //check if stage exist if so get stage ID
                string strSQLStage = @"select isnull((select Distinct T0.StageID from PMG1 T0
                                        inner join PMC2 T1 on T1.StageID = T0.StageID
                                        inner join PMC6 T2 on T0.Task = t2.TaskID
                                        where T0.AbsEntry = " + pAbsEntry + " and T1.Name = '" + pStagetype + "'),0) as [StageID]";

                int intStageID = 0;
                intStageID = NSC_DI.UTIL.SQL.GetValue<int>(strSQLStage);
                string strSQLTask = @"Select isnull((Select Distinct T0.Task
                                    from PMG1 T0
                                    inner join PMC2 T1 on T1.StageID = T0.StageID
                                    inner join PMC6 T2 on T0.Task = t2.TaskID
                                    where T0.AbsEntry = " + pAbsEntry + " and T2.Name ='" + pTask + "'),0) as [TaskID]";//??12121

                int intTaskID = 0;
                intTaskID = NSC_DI.UTIL.SQL.GetValue<int>(strSQLTask);
                SAPbobsCOM.PM_StageData stage = null;
                if (intStageID > 0 && intTaskID > 0)
                {
                    //AddDocToProject(pmgService, projectToUpdateParam, intStageID, pDocType, pDocEntry);
                    pmgService.UpdateProject(project);
                    return 1;
                }
                else
                {
                    intStageID = NSC_DI.UTIL.SQL.GetValue<int>("Select distinct T1.StageID from PMC2 T1 where T1.Name = '" + pStagetype + "'"); //??12121
                    intTaskID = NSC_DI.UTIL.SQL.GetValue<int>("Select distinct T2.TaskID from PMC6 T2 where T2.Name = '" + pTask + "'");//??12121
                    stage = stagesCollection.Add();
                    stage.StageType = intStageID;//1;
                    stage.StartDate = DateTime.Now;
                    //stage.CloseDate = stage.StartDate.AddDays(30);
                    stage.Task = intTaskID;//1;
                    stage.Description = pDescription;
                    stage.ExpectedCosts = pExpectedCost;// 150;
                    stage.PercentualCompletness = pPercentComplete;
                    stage.IsFinished = SAPbobsCOM.BoYesNoEnum.tNO;
                    //stage.StageOwner = 5;
                    //stage.AttachmentEntry = 1;
                    //stage = stagesCollection.Add();
                    //stage.StageType = 2;
                    //stage.StartDate = DateTime.Now.AddMonths(1);
                    //stage.CloseDate = stage.StartDate.AddDays(30);
                    //stage.Task = 2;
                    //stage.Description = "StageWithDocByDI_02";
                    //stage.ExpectedCosts = 250;
                    //stage.PercentualCompletness = 8;
                    //stage.IsFinished = SAPbobsCOM.BoYesNoEnum.tNO;
                    //stage.StageOwner = 5;
                    //stage.DependsOnStage1 = 1;
                    //stage.StageDependency1Type = SAPbobsCOM.StageDepTypeEnum.sdt_Project;
                    //stage.DependsOnStageID1 = 1;
                    pmgService.UpdateProject(project);
                    //AddDocToProject(pmgService, projectToUpdateParam, intStageID, pDocType, pDocEntry);


                }
            }

            catch (Exception ex)
            {
                Globals.oApp.MessageBox(ex.Message);
            }

            finally
            {
                if (pmgService != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(pmgService);
                }
            }

            return -1;

        }

        public int UpdateCropCycle_StageAndProd(int pAbsEntry, string pStagetype, string pTask, string pDescription, double pExpectedCost, int pPercentComplete, int pDocEntry, int pDocNum)
        {
            _SBO_Company = Globals.oCompany;
            SAPbobsCOM.CompanyService oCompServ = (SAPbobsCOM.CompanyService)_SBO_Company.GetCompanyService();
            SAPbobsCOM.ProjectManagementService pmgService = (SAPbobsCOM.ProjectManagementService)oCompServ.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectManagementService);
            SAPbobsCOM.PM_ProjectDocumentParams projectToUpdateParam = pmgService.GetDataInterface(SAPbobsCOM.ProjectManagementServiceDataInterfaces.pmsPM_ProjectDocumentParams);

            try
            {
                projectToUpdateParam.AbsEntry = pAbsEntry; //1;
                SAPbobsCOM.PM_ProjectDocumentData project = pmgService.GetProject(projectToUpdateParam);
                SAPbobsCOM.PM_StagesCollection stagesCollection = project.PM_StagesCollection;
                //check if stage exist if so get stage ID
                string strSQLStage = @"select isnull((select Distinct T0.StageID from PMG1 T0
                                        inner join PMC2 T1 on T1.StageID = T0.StageID
                                        inner join PMC6 T2 on T0.Task = t2.TaskID
                                        where T0.AbsEntry = " + pAbsEntry + " and T1.Name = '" + pStagetype + "'),0) as [StageID]";

                int intStageID = 0;
                intStageID = NSC_DI.UTIL.SQL.GetValue<int>(strSQLStage);
                string strSQLTask = @"Select isnull((Select Distinct T0.Task
                                    from PMG1 T0
                                    inner join PMC2 T1 on T1.StageID = T0.StageID
                                    inner join PMC6 T2 on T0.Task = t2.TaskID
                                    where T0.AbsEntry = " + pAbsEntry + " and T2.Name ='" + pTask + "'),0) as [TaskID]";

                int intTaskID = 0;
                intTaskID = NSC_DI.UTIL.SQL.GetValue<int>(strSQLTask);

                string strSQLDescription = @"Select isnull((Select T0.DSCRIPTION 
                                    from PMG1 T0
                                    inner join PMC2 T1 on T1.StageID = T0.StageID
                                    inner join PMC6 T2 on T0.Task = t2.TaskID
                                    where T0.AbsEntry = " + pAbsEntry + " and T2.Name ='" + pTask + "' and T0.DSCRIPTION Like'" + pDescription + "'),'0') as [Desc]";

                string strDesc = null;
                strDesc = NSC_DI.UTIL.SQL.GetValue<string>(strSQLDescription);

                SAPbobsCOM.PM_StageData stage = null;
                if (intStageID > 0 && intTaskID > 0 && strDesc == pDescription)
                {
                    //AddProdToProject(pmgService, projectToUpdateParam, intStageID, pDocEntry, pDocNum);
                    pmgService.UpdateProject(project);
                    return 1;
                }
                else
                {
                    intStageID = NSC_DI.UTIL.SQL.GetValue<int>("Select distinct T1.StageID from PMC2 T1 where T1.Name = '" + pStagetype + "'");
                    intTaskID = NSC_DI.UTIL.SQL.GetValue<int>("Select distinct T2.TaskID from PMC6 T2 where T2.Name = '" + pTask + "'");
                    stage = stagesCollection.Add();
                    stage.StageType = intStageID;//1;
                    stage.StartDate = DateTime.Now;
                    //stage.CloseDate = stage.StartDate.AddDays(30);
                    stage.Task = intTaskID;//1;
                    stage.Description = pDescription;
                    stage.ExpectedCosts = pExpectedCost;// 150;
                    stage.PercentualCompletness = pPercentComplete;
                    stage.IsFinished = SAPbobsCOM.BoYesNoEnum.tNO;
                    //stage.StageOwner = 5;
                    //stage.AttachmentEntry = 1;
                    //stage = stagesCollection.Add();
                    //stage.StageType = 2;
                    //stage.StartDate = DateTime.Now.AddMonths(1);
                    //stage.CloseDate = stage.StartDate.AddDays(30);
                    //stage.Task = 2;
                    //stage.Description = "StageWithDocByDI_02";
                    //stage.ExpectedCosts = 250;
                    //stage.PercentualCompletness = 8;
                    //stage.IsFinished = SAPbobsCOM.BoYesNoEnum.tNO;
                    //stage.StageOwner = 5;
                    //stage.DependsOnStage1 = 1;
                    //stage.StageDependency1Type = SAPbobsCOM.StageDepTypeEnum.sdt_Project;
                    //stage.DependsOnStageID1 = 1;
                    pmgService.UpdateProject(project);
                    //AddProdToProject(pmgService, projectToUpdateParam, intStageID, pDocEntry, pDocNum);


                }
            }

            catch (Exception ex)
            {
                Globals.oApp.MessageBox(ex.Message);
            }

            finally
            {
                if (pmgService != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(pmgService);
                }
            }

            return -1;

        }
        public int GetProj_AbsEntry(string pDocNum)
        {
            int intHolder =NSC_DI.UTIL.SQL.GetValue<int>("Select T0.AbsEntry from OPMG T0 where T0.DocNum=" + pDocNum, 0);
            return intHolder;
        }
        public int GetStageID(string pDocNum)
        {
            int intHolder = NSC_DI.UTIL.SQL.GetValue<int>("Select T0.StageID from PMG1 T0 where T0.UniqueID='" + pDocNum+"'", 0);
            return intHolder;
        }
        public string GetFinProj(string pDocNum)
        {
            string strFinProj = NSC_DI.UTIL.SQL.GetValue<string>("Select T0.FIPROJECT from OPMG T0 where T0.DocNum=" + pDocNum, "");
            return strFinProj;
        }
        public string GetCropName(int pCropDocNum)
        {
            string strCropName = NSC_DI.UTIL.SQL.GetValue<string>("Select T0.NAME from OPMG T0 where T0.DocNum=" + pCropDocNum, "");
            return strCropName;
        }

        public void AddDocToProject_Single(SAPbobsCOM.PMDocumentTypeEnum pDocType, string pDocEntry, string pCropID,string pStageID)
        {
            SAPbobsCOM.CompanyService oCompServ = (SAPbobsCOM.CompanyService)Globals.oCompany.GetCompanyService();
            SAPbobsCOM.ProjectManagementService pmgService = (SAPbobsCOM.ProjectManagementService)oCompServ.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectManagementService);
            SAPbobsCOM.PM_ProjectDocumentParams projectToUpdateParam = pmgService.GetDataInterface(SAPbobsCOM.ProjectManagementServiceDataInterfaces.pmsPM_ProjectDocumentParams);



            try
            {

                projectToUpdateParam.AbsEntry = GetProj_AbsEntry(pCropID);

                SAPbobsCOM.PM_ProjectDocumentData project = pmgService.GetProject(projectToUpdateParam);
                SAPbobsCOM.PM_DocumentsCollection documentsCollection = project.PM_DocumentsCollection;

                SAPbobsCOM.PM_DocumentData document = documentsCollection.Add();
                document.StageID = 1;   // GetStageID(pStageID);    does not work - tried the 1st 4 columns
                document.DocEntry = Convert.ToInt32(pDocEntry);
                document.DocType = pDocType; // SAPbobsCOM.PMDocumentTypeEnum.pmdt_APCreditMemo;
                document.LineNumber = 0;
                pmgService.UpdateProject(project);

               
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

            finally
            {
                if (pmgService != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(pmgService);
                }
            }
        }
        public void AddDocToProject_Issue(SAPbobsCOM.PMDocumentTypeEnum pDocType, string pDocEntry, string pCropID, string pStageID)
        {
            SAPbobsCOM.CompanyService oCompServ = (SAPbobsCOM.CompanyService)Globals.oCompany.GetCompanyService();
            SAPbobsCOM.ProjectManagementService pmgService = (SAPbobsCOM.ProjectManagementService)oCompServ.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectManagementService);
            SAPbobsCOM.PM_ProjectDocumentParams projectToUpdateParam = pmgService.GetDataInterface(SAPbobsCOM.ProjectManagementServiceDataInterfaces.pmsPM_ProjectDocumentParams);

            SAPbobsCOM.Recordset oRecSet = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(string.Format(@" Select  T0.LineNum from IGE1 T0
                                                                                        inner join OITM T1 on T1.ItemCode = T0.ItemCode
                                                                                        where T0.docentry = {0}  and IssueMthd != 'B'", pDocEntry));

            try
            {
                int intCrop = 0;
                intCrop = GetProj_AbsEntry(pCropID);

                for (int i = 0; i < oRecSet.RecordCount; i++)
                {
                    if (i == 0)
                    { oRecSet.MoveFirst(); }

                    if (intCrop > 0)
                    {
                        projectToUpdateParam.AbsEntry = intCrop;
                        SAPbobsCOM.PM_ProjectDocumentData project = pmgService.GetProject(projectToUpdateParam);
                        SAPbobsCOM.PM_DocumentsCollection documentsCollection = project.PM_DocumentsCollection;

                        SAPbobsCOM.PM_DocumentData document = documentsCollection.Add();
                        document.StageID = GetStageID(pStageID);// 1;
                        document.DocEntry = Convert.ToInt32(pDocEntry);
                        document.DocType = pDocType; // SAPbobsCOM.PMDocumentTypeEnum.pmdt_APCreditMemo;
                        document.LineNumber = oRecSet.Fields.Item(0).Value;
                        pmgService.UpdateProject(project);
                    }

                    oRecSet.MoveNext();
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(ex.Message);
            }

            finally
            {
                if (pmgService != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(pmgService);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecSet);
                }
            }
        }
        public void AddDocToProject_Receipts(SAPbobsCOM.PMDocumentTypeEnum pDocType, string pDocEntry, string pCropID, string pStageID)
        {
            SAPbobsCOM.CompanyService oCompServ = (SAPbobsCOM.CompanyService)Globals.oCompany.GetCompanyService();
            SAPbobsCOM.ProjectManagementService pmgService = (SAPbobsCOM.ProjectManagementService)oCompServ.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectManagementService);
            SAPbobsCOM.PM_ProjectDocumentParams projectToUpdateParam = pmgService.GetDataInterface(SAPbobsCOM.ProjectManagementServiceDataInterfaces.pmsPM_ProjectDocumentParams);

            SAPbobsCOM.Recordset oRecSet = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(string.Format(@" Select  T0.LineNum from IGN1 T0
                                                                                        inner join OITM T1 on T1.ItemCode = T0.ItemCode
                                                                                        where T0.docentry = {0} ", pDocEntry));

            try
            {
                int intCrop = 0;
                intCrop = GetProj_AbsEntry(pCropID);

                for (int i = 0; i < oRecSet.RecordCount; i++)
                {
                    if (i == 0)
                    { oRecSet.MoveFirst(); }

                    if (intCrop > 0)
                    {
                        projectToUpdateParam.AbsEntry = intCrop;
                        SAPbobsCOM.PM_ProjectDocumentData project = pmgService.GetProject(projectToUpdateParam);
                        SAPbobsCOM.PM_DocumentsCollection documentsCollection = project.PM_DocumentsCollection;

                        SAPbobsCOM.PM_DocumentData document = documentsCollection.Add();
                        document.StageID = GetStageID(pStageID);// 1;
                        document.DocEntry = Convert.ToInt32(pDocEntry);
                        document.DocType = pDocType; // SAPbobsCOM.PMDocumentTypeEnum.pmdt_APCreditMemo;
                        document.LineNumber = oRecSet.Fields.Item(0).Value;
                        pmgService.UpdateProject(project);
                    }

                    oRecSet.MoveNext();
                }
            }
            catch (Exception ex)
            {
                //Globals.oApp.MessageBox(ex.Message);
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

            finally
            {
                if (pmgService != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(pmgService);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecSet);
                }
            }
        }

        public void AddDocToProject_Backflush(SAPbobsCOM.PMDocumentTypeEnum pDocType, string pDocEntry,string pCropID, string pStageID )
        {
            SAPbobsCOM.CompanyService oCompServ = (SAPbobsCOM.CompanyService)Globals.oCompany.GetCompanyService();
            SAPbobsCOM.ProjectManagementService pmgService = (SAPbobsCOM.ProjectManagementService)oCompServ.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectManagementService);
            SAPbobsCOM.PM_ProjectDocumentParams projectToUpdateParam = pmgService.GetDataInterface(SAPbobsCOM.ProjectManagementServiceDataInterfaces.pmsPM_ProjectDocumentParams);

            SAPbobsCOM.Recordset oRecSet = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(string.Format(@" Select  T0.LineNum from IGE1 T0
                                                                                        inner join OITM T1 on T1.ItemCode = T0.ItemCode
                                                                                        where T0.docentry = {0} and IssueMthd = 'B'", pDocEntry));

            try
            {
                int intCrop = 0;
                intCrop = GetProj_AbsEntry(pCropID);

                for (int i = 0; i < oRecSet.RecordCount; i++)
                {
                    if(i==0)
                    { oRecSet.MoveFirst();}
            
                    if (intCrop > 0)
                    {
                        projectToUpdateParam.AbsEntry = intCrop;
                        SAPbobsCOM.PM_ProjectDocumentData project = pmgService.GetProject(projectToUpdateParam);
                        SAPbobsCOM.PM_DocumentsCollection documentsCollection = project.PM_DocumentsCollection;

                        SAPbobsCOM.PM_DocumentData document = documentsCollection.Add();
                        document.StageID = GetStageID(pStageID);// 1;
                        document.DocEntry = Convert.ToInt32(pDocEntry);
                        document.DocType = pDocType; // SAPbobsCOM.PMDocumentTypeEnum.pmdt_APCreditMemo;
                        document.LineNumber = oRecSet.Fields.Item(0).Value;
                        pmgService.UpdateProject(project);
                    }

                    oRecSet.MoveNext();
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(ex.Message);
            }

            finally
            {
                if (pmgService != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(pmgService);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecSet);
                }
            }
        }

        public void AddDocToProject(SAPbobsCOM.PMDocumentTypeEnum pDocType, string pDocEntry, DataTable pDataTable)
        {
            SAPbobsCOM.CompanyService oCompServ = (SAPbobsCOM.CompanyService)Globals.oCompany.GetCompanyService();
            SAPbobsCOM.ProjectManagementService pmgService = (SAPbobsCOM.ProjectManagementService)oCompServ.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectManagementService);
            SAPbobsCOM.PM_ProjectDocumentParams projectToUpdateParam = pmgService.GetDataInterface(SAPbobsCOM.ProjectManagementServiceDataInterfaces.pmsPM_ProjectDocumentParams);

            try
            {
                for(int i =0; i<pDataTable.Rows.Count;i++)
                {
                    string strCrop = "";
                    try
                    {
                        strCrop = GetProj_AbsEntry(pDataTable.GetValue("CropID", i));
                    }
                    catch
                    {
                        //do nothing the item in the dt has no crop cycle so continue
                    }
                    if (strCrop.Length > 0 )
                    {
                        projectToUpdateParam.AbsEntry = GetProj_AbsEntry(pDataTable.GetValue("CropID", i));
                        SAPbobsCOM.PM_ProjectDocumentData project = pmgService.GetProject(projectToUpdateParam);
                        SAPbobsCOM.PM_DocumentsCollection documentsCollection = project.PM_DocumentsCollection;

                        SAPbobsCOM.PM_DocumentData document = documentsCollection.Add();
                        document.StageID = GetStageID(pDataTable.GetValue("StageID", i));// 1;
                        document.DocEntry = Convert.ToInt32(pDocEntry);
                        document.DocType = pDocType; // SAPbobsCOM.PMDocumentTypeEnum.pmdt_APCreditMemo;
                        document.LineNumber = i;
                        pmgService.UpdateProject(project);
                    }

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(ex.Message);
            }

            finally
            {
                if (pmgService != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(pmgService);
                }
            }
        }

        public void AddProdToProject( int pDocEntry, int pDocNum,string ProjDocNum, string pStageID)
        {
            SAPbobsCOM.CompanyService oCompServ = (SAPbobsCOM.CompanyService)Globals.oCompany.GetCompanyService();
            SAPbobsCOM.ProjectManagementService pmgService = (SAPbobsCOM.ProjectManagementService)oCompServ.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectManagementService);
            SAPbobsCOM.PM_ProjectDocumentParams projectToUpdateParam = pmgService.GetDataInterface(SAPbobsCOM.ProjectManagementServiceDataInterfaces.pmsPM_ProjectDocumentParams);

           

            try
            {
                projectToUpdateParam.AbsEntry = GetProj_AbsEntry(ProjDocNum);
                SAPbobsCOM.PM_ProjectDocumentData project = pmgService.GetProject(projectToUpdateParam);
                SAPbobsCOM.PM_WorkOrdersCollection prodCollection = project.PM_WorkOrdersCollection;
                SAPbobsCOM.PM_WorkOrderData ProdOrder = prodCollection.Add();
                                
                ProdOrder.DocEntry = pDocEntry;
                ProdOrder.DocNumber = pDocNum;
                ProdOrder.StageID = GetStageID(pStageID);
                pmgService.UpdateProject(project);


            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

            finally
            {
                if (pmgService != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(pmgService);
                }
            }

        }
        #endregion
    }
}
