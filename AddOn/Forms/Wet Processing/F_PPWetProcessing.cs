﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using System.Text;

namespace NavSol.Forms.Wet_Processing
{
    class F_PPWetProcessing : B1Event
    {
        private const string cItemType = "Wet Cannabis";
        private const string cFormID = "NSC_WET_PROC_PP";
        public static SAPbouiCOM.Form _SBO_Form;

        /// <summary>
        /// Better way to keep track of the Matrix Columns we currently have.
        /// </summary>
        private enum MatrixColumns { ItemCode, ItemName, OnHand, Quantity, WhsCode, BatchNum, StateID, GroupNum }
        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPressed(ItemEvent pVal)
        {
            var BubbleEvent = true;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;
            try
            {
                oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

                switch (pVal.ItemUID)
                {
                    case "BTN_LOOKUP":
                        BTN_LOOKUP_ITEM_PRESSED(oForm);
                        break;

                    case "BTN_RELE":
                        BTN_RELE_Click(oForm);
                        break;

                    case "MTX_ITEMS":
                        MTX_ITEMS_Click(pVal, oForm);
                        break;
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
            }
            return BubbleEvent;
        }

        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
        [B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = null;
            try
            {
                oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

                switch (pVal.ItemUID)
                {
                    case "MTX_ITEMS":
                        MAIN_MATRIX_LINK_PRESSED(pVal, oForm);
                        break;
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
            }
        }

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (ChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
        public virtual void OnAfterComboSelect(ItemEvent pVal)
        {
            //10823-2
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "CBX_WHSE") CMB_WHSE_Selected(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion

        // ---------------------------------- SUBS       --------------------------------------------------------

        private static void FormSetup(Form pForm)
        {
            try
            {
                pForm.Freeze(true);
                //CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_MAIN", @"curing-icon.bmp");
                //Need Wet Processing Image

                Load_Matrix(pForm);
                pForm.Items.Item("CMB_PORDER").Specific.Select(0, BoSearchKey.psk_Index);
                pForm.Items.Item("MTX_ITEMS").Specific.SelectionMode = BoMatrixSelect.ms_Auto;
                pForm.Freeze(false);
                pForm.VisibleEx = true;
                pForm.Items.Item("TXT_LOOKUP").Specific.Active = true;

                if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y")
                {
                    NavSol.CommonUI.CFL.CreateWH(pForm, "CFL_WH", "CBX_WHSE");
                    CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_NONE, "Inactive", BoConditionOperation.co_EQUAL, "N");
                    CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_AND, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, "CUR", 1);
                    CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_OR, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, "DRY");
                    CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_OR, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, "WET", 0, 1);
                    CommonUI.CFL.AddCon_Branches(pForm, "CFL_WH", BoConditionRelationship.cr_AND);    //10823
                }
                else
                    Load_Combobox_Warehouses(pForm);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {

            }
        }

        public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
        {
            Item oItm = null;
            try
            {
                var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);
                // create hidden field to save the calling form
                if (pCallingFormUID != null)
                {
                    oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
                    oItm.Top = -20;
                    oItm.Specific.Value = pCallingFormUID;
                }

                FormSetup(oForm);

                return oForm;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return null;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private void BTN_RELE_Click(Form pForm)
        {
            try
            {
                // Grab the textbox for days from the form
                ComboBox ComboBox_POrder = pForm.Items.Item("CMB_PORDER").Specific;
                //ComboBox CMB_PORDER = this.Helpers.Forms.GetControlFromForm(
                //    ItemType: Forms.FormControlTypes.ComboBox,
                //    ItemUID: "CMB_PORDER",
                //    FormToSearchIn: pForm);

                if (string.IsNullOrEmpty(ComboBox_POrder.Value))
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Please select how you'd like to group your production orders.");

                    // Focus the cursor in the offending input
                    ComboBox_POrder.Active = true;
                    return;
                }

                // Possibly need to validate that we cannot run an already grouped Harvest/Dry as Grouped again.. Would be confusing.

                switch (ComboBox_POrder.Value.ToString())
                {
                    case "Individual":
                        CreateIndividualProductionOrders(pForm);
                        break;
                    case "Grouped":
                        //This calles a disassembly order and that is bad. 
                        //Todo: Fix this ta allow grouping and cration of a standard production order.
                        // CreateGroupedProductionOrder(pForm);
                        // Send a message to the client
                        Globals.oApp.StatusBar.SetText("Please select Individual.");
                        break;
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static void CreateIndividualProductionOrders(Form pForm)
        {
            try
            {
                // Pull all data from the selected columns.
                ManicuredBudDataTransfer manicuredBudToTransfer = GetSelectedItemsFromMatrix(pForm);
                if (manicuredBudToTransfer == null)
                    return;


                bool AllWetBudPassed = true;
                double TotalProcessed = 0;

                if (manicuredBudToTransfer.RowsToTransfer.Count <= 0)
                {
                    // Send success message to the client
                    Globals.oApp.StatusBar.SetText("Failed to Release and Issued all Selected Items!");

                    return;
                }

                // Save error line items

                var sb = new StringBuilder();

                sb.AppendLine("One or more items failed:");

                foreach (ManicuredBudSelectedRow row in manicuredBudToTransfer.RowsToTransfer)
                {
                    try
                    {
                        //Create Production Order for Cured Cannabis

                        var strainID = NSC_DI.SAP.Items.GetStrainID(row.ItemCode);
                        string SourceItemCode = NSC_DI.UTIL.AutoStrain.GetAutoItemCodeValue("~WUFL", strainID);

                        SAPbouiCOM.Matrix Matrix_Items = _SBO_Form.Items.Item("MTX_ITEMS").Specific;
                        SAPbouiCOM.EditText oTotalg = Matrix_Items.GetCellSpecific("col_2", row.Mx_RowID);

                        int PdOKey = 0;

                        if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                        //Remarks
                        SAPbouiCOM.EditText oEdit = pForm.Items.Item("TXT_REM").Specific;
                        string Remarks = oEdit.Value;

                        string destWHName = pForm.Items.Item("CBX_WHSE").Specific.Value;  // #10824
                        string destWHCode = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT WhsCode FROM OWHS WHERE WhsName = '{destWHName}'");// 10824
                        PdOKey = NSC_DI.SAP.ProductionOrder.CreateFromBOM(SourceItemCode, Convert.ToDouble(oTotalg.Value), DateTime.Now, destWHCode, true, "N", "WET", Remarks);// 10824

                        if (PdOKey == 0)
                        {
                            sb.AppendLine($"Item Code: {row.ItemCode}  Batch No: {row.BatchNo}");
                            sb.AppendLine("Failed to create Production Order " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                            sb.AppendLine();

                            throw new System.ComponentModel.WarningException();
                        }

                        //NSC_DI.SAP.ProductionOrder.UpdateStatus(PdOKey, BoProductionOrderStatusEnum.boposReleased);

                        //Check the warehouse for the Clone(s) item(s) on tbe Production BOM and if needed adjust accordingly
                        //Get clone Warehouses for selected items on the Matrix by batchnumber 
                        bool bUpdatedProdO = false;
                        SAPbobsCOM.ProductionOrders oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
                        oPdO.GetByKey(PdOKey);
                        SAPbobsCOM.ProductionOrders_Lines oLines = oPdO.Lines;

                        //Update Destination Warehouse.
                        if (row.DestinationWarehouseCode != oPdO.Warehouse)
                        {
                            oPdO.Warehouse = row.DestinationWarehouseCode;
                            bUpdatedProdO = true;
                        }
                        //Update line Item warehouse
                        for (int i = 0; i <= oLines.Count - 1; i++)
                        {
                            oLines.SetCurrentLine(i);
                            if (row.ItemCode == oLines.ItemNo)
                            {
                                //string strWhsSourceHolder = NSC_DI.UTIL.SQL.GetValue<string>(@"select WhsCode from [OWHS] where OWHS.WhsName = '" + row.SourceWarehouseCode + "'", null);
                                if (row.SourceWarehouseCode != oLines.Warehouse && oLines.PlannedQuantity > 0)
                                {
                                    //Update warehouse
                                    oLines.Warehouse = row.SourceWarehouseCode;
                                    NSC_DI.SAP.Document.SetDefaultDistributionRules(oLines);
                                    bUpdatedProdO = true;
                                }
                            }
                        }
                        if (bUpdatedProdO == true && oPdO.Update() < 0)
                        {
                            NSC_DI.SAP.B1Exception.RaiseException();
                        }

                        //Create Issue to Production for Selected plants SN - Full Harvest


                        var PdOIssueKey = NSC_DI.SAP.ProductionOrder.Issue(PdOKey, "Wet Cannabis", row.Quantity, row.BatchNo, null);
                        if (PdOIssueKey == 0)
                        {

                            sb.AppendLine($"Item Code: {row.ItemCode}  Batch No: {row.BatchNo}");
                            sb.AppendLine("Failed to Issue to Production for selected Plants " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                            sb.AppendLine();

                            throw new System.ComponentModel.WarningException();
                        }

                        if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                    }
                    catch (System.ComponentModel.WarningException)
                    {
                        AllWetBudPassed = false;
                    }
                    catch (NSC_DI.SAP.B1Exception ex)
                    {
                        sb.AppendLine($"Item Code: {row.ItemCode}  Batch No: {row.BatchNo}");
                        sb.AppendLine(ex.Message);
                        sb.AppendLine();

                        AllWetBudPassed = false;
                    }
                    catch (Exception ex)
                    {

                        Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                        AllWetBudPassed = false;
                    }
                    finally
                    {
                        if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                    }
                }

                if (AllWetBudPassed)
                {
                    // Send success message to the client
                    Globals.oApp.StatusBar.SetText("Successfully Release and Issued the Selected Items!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                    //Remarks
                    SAPbouiCOM.EditText oEdit = pForm.Items.Item("TXT_REM").Specific;
                    oEdit.Value = "";
                }
                else
                {
                    if (TotalProcessed > 0)
                    {
                        // Send success message to the client
                        Globals.oApp.StatusBar.SetText("Failed to Release and Issued some of the Selected Items!");
                    }
                    else
                    {
                        // Send success message to the client
                        Globals.oApp.StatusBar.SetText("Failed to Release and Issued all Selected Items!");
                    }

                    // Display error line items

                    var errStr = sb.ToString();

                    Globals.oApp.MessageBox(errStr);
                }

                Load_Matrix(pForm);
            }

            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }
        private static void CreateGroupedProductionOrder(Form pForm)
        {
            try
            {
                // Pull all data from the selected columns.
                ManicuredBudDataTransfer manicuredBudToTransfer = GetSelectedItemsFromMatrix(pForm);
                if (manicuredBudToTransfer == null)
                    return;

                //double TotalProcessed = 0;

                if (manicuredBudToTransfer.RowsToTransfer.Count <= 0)
                {
                    // Send success message to the client
                    Globals.oApp.StatusBar.SetText("Failed to Release and Issued all Selected Items!");

                    return;
                }

                #region Create GoodsIssued and Disassembly Production Order Data Lists

                string ItemCode = "";
                string DestinationWarehouseCode = "";
                string SourceWarehouseCode = "";
                string ProjectedBatchNumber = "";
                int TotalQuantity = 0;

                // Prepare a list of Goods Issued Lines
                List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines> ListOfGoodsIssuedLines = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();

                // Prepare a list of line items for the "Production Order"
                List<NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem> components = new List<NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem>();

                foreach (ManicuredBudSelectedRow row in manicuredBudToTransfer.RowsToTransfer)
                {
                    // Add a new item to the list of Goods Issued Lines
                    ListOfGoodsIssuedLines.Add(
                        new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
                        {
                            ItemCode = row.ItemCode,
                            Quantity = row.Quantity,
                            WarehouseCode = row.SourceWarehouseCode,
                            BatchLineItems = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>()
                            {
                            new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches() {BatchNumber = row.BatchNo, Quantity = row.Quantity}
                            }
                        }
                    );

                    ItemCode = row.ItemCode;
                    DestinationWarehouseCode = row.DestinationWarehouseCode;
                    SourceWarehouseCode = row.SourceWarehouseCode;
                    ProjectedBatchNumber = row.BatchNo;
                    TotalQuantity += (int)row.Quantity;
                }


                foreach (var item in NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(ItemCode))
                {
                    components.Add(
                        new NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem()
                        {
                            ItemCodeForItemBeingCreated = item.ItemCode,
                            DestinationWarehouseCode = DestinationWarehouseCode,
                            BaseQuantity = TotalQuantity,
                            PlannedQuantity = TotalQuantity
                        }
                    );
                }

                #endregion
                // Create a Disassembly Production Order based on the form
                NSC_DI.SAP.ProductionOrder_OLD.CreateDisassemblyProductionOrder(ItemCode
                    , TotalQuantity
                    , SourceWarehouseCode
                    , DateTime.Now
                    , components
                    , ProjectedBatchNumber);


                // Change status to released
                NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(NSC_DI.SAP.ProductionOrder_OLD.NewlyCreatedKey, BoProductionOrderStatusEnum.boposReleased);


                // Create an "Issue From Production" document
                NSC_DI.SAP.IssueFromProduction.Create(NSC_DI.SAP.ProductionOrder_OLD.NewlyCreatedKey, DateTime.Now, ListOfGoodsIssuedLines
                );

                Load_Matrix(pForm);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }
        private static void BTN_LOOKUP_ITEM_PRESSED(Form pForm)
        {
            EditText EditText_Lookup = null;
            Matrix Matrix_Items = null;
            try
            {
                EditText_Lookup = pForm.Items.Item("TXT_LOOKUP").Specific;

                // If nothing was passed in the textbox, just return
                if (EditText_Lookup.Value.Length == 0)
                {
                    return;
                }

                Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;
                CheckBox oChkBox_Assign_StateID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_STID", _SBO_Form) as CheckBox;
                bool Add_State_ID = oChkBox_Assign_StateID.Checked;
                if (Add_State_ID == true)
                {
                    bool bRowIsSelected = false;
                    int intSelectedRowCount = 0;
                    int intLastRowIndex = 0;
                    //Add Code to Validate Single row selection
                    for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                    {
                        bRowIsSelected = Matrix_Items.IsRowSelected(i);
                        if (bRowIsSelected == true)
                        {
                            intLastRowIndex = i;
                            intSelectedRowCount++;
                            bRowIsSelected = false;
                        }
                    }
                    if (intSelectedRowCount > 1)
                    {
                        //Show error message to indicate you can only select one row when assigning StateId
                        Globals.oApp.StatusBar.SetText("You may only select one row when assigning a StateID");
                        return;
                    }
                    else
                    {
                        //Check if ID already exist --, and (assign ID or Overwrite Existing)
                        string StateID = ((EditText)Matrix_Items.Columns.Item((int)MatrixColumns.StateID).Cells.Item(intLastRowIndex).Specific).Value;
                        string DistNumber = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.BatchNum).Cells.Item(intLastRowIndex).Specific).Value;
                        SAPbobsCOM.BatchNumberDetail oBatchDetails = null;
                        CompanyService oCoService = Globals.oCompany.GetCompanyService() as CompanyService;
                        BatchNumberDetailsService oBatchService = oCoService.GetBusinessService(ServiceTypes.BatchNumberDetailsService);
                        BatchNumberDetailParams oParams = oBatchService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);

                        try
                        {
                            if (StateID.Trim().Length == 0)
                            {
                                // RH commented out the lines
                                ////Add field in DB
                                ////Get and Set the DocEntry
                                ////***To Do: Up date this so that it is pulling on the correct DistNumber once Randy adjust this you will need to
                                ////Uncomment the line below and delete the current select State used for testing. 
                                //int intIndexHolder = DistNumber.IndexOf('-');
                                //DistNumber = DistNumber.Remove(0, intIndexHolder+1);
                                //intIndexHolder = DistNumber.IndexOf('-');
                                //DistNumber = DistNumber.Remove(0, intIndexHolder + 1);
                                string strSQL = string.Format(@"Select AbsEntry From [OBTN] where DistNumber='{0}'", DistNumber);
                                int DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                                oParams.DocEntry = DocEntry;
                                oBatchDetails = oBatchService.Get(oParams);
                                oBatchDetails.BatchAttribute1 = EditText_Lookup.Value;
                                oBatchService.Update(oBatchDetails);
                                //Success Reload Matrix
                                _SBO_Form.Freeze(true);
                                Load_Matrix(_SBO_Form);
                                // Empty the barcode scanner field
                                EditText_Lookup.Value = "";
                                // Focus back into the barcode scanner's text field
                                EditText_Lookup.Active = true;
                                //Move to next Row if not last row
                                if (Matrix_Items.RowCount != intLastRowIndex)
                                {
                                    Matrix_Items.SelectRow(intLastRowIndex + 1, true, true);
                                }
                            }
                            else
                            {
                                //Update field in DB
                                int iReturnVal = Globals.oApp.MessageBox("A State ID already exists for this item would you like to replace it?", 2, "Cancel", "OK");
                                switch (iReturnVal)
                                {
                                    case 1:
                                        //Cancel was selected do nothing                                         
                                        break;
                                    case 2:
                                        //Update the field in DB
                                        //Get and Set the DocEntry
                                        //***To Do: Up date this so that it is pulling on the correct DistNumber once Randy adjust this you will need to
                                        //Uncomment the line below and delete the current select State used for testing. 
                                        //int intIndexHolder = DistNumber.IndexOf('-');
                                        //DistNumber = DistNumber.Remove(0, intIndexHolder + 1);
                                        //intIndexHolder = DistNumber.IndexOf('-');
                                        //DistNumber = DistNumber.Remove(0, intIndexHolder + 1);
                                        string strSQL = string.Format(@"Select AbsEntry From [OBTN] where DistNumber='{0}'", DistNumber);
                                        int DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                                        oParams.DocEntry = DocEntry;
                                        oBatchDetails = oBatchService.Get(oParams);
                                        oBatchDetails.BatchAttribute1 = EditText_Lookup.Value;
                                        oBatchService.Update(oBatchDetails);
                                        //Success Reload Matrix
                                        _SBO_Form.Freeze(true);
                                        Load_Matrix(_SBO_Form);
                                        // Empty the barcode scanner field
                                        EditText_Lookup.Value = "";
                                        // Focus back into the barcode scanner's text field
                                        EditText_Lookup.Active = true;
                                        //Move to next Row if not last row
                                        if (Matrix_Items.RowCount != intLastRowIndex)
                                        {
                                            Matrix_Items.SelectRow(intLastRowIndex + 1, true, true);
                                        }
                                        break;
                                }

                            }

                            //Update Matrix cell with data or Reload Matrix
                        }
                        catch (Exception ex)
                        {
                            Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                        }
                        finally
                        {
                            //Clean up the objects and Garbage Collect
                            _SBO_Form.Freeze(false);
                            NSC_DI.UTIL.Misc.KillObject(oBatchDetails);
                            NSC_DI.UTIL.Misc.KillObject(oCoService);
                            NSC_DI.UTIL.Misc.KillObject(oBatchService);

                            GC.Collect();
                        }
                    }

                }
                else
                {
                    //Process as normal 
                    // Keep track of the warehouse ID from the barcode (if passed)
                    string WarehouseID = "";

                    // If the first four characters of the barcode are "WHSE", then the rest of the barcode contains the warehouse code.
                    if (EditText_Lookup.Value.Length >= 4 && EditText_Lookup.Value.Substring(0, 4) == "WHSE")
                    {
                        WarehouseID = EditText_Lookup.Value.Replace("WHSE-", "");
                    }

                    // Keep track if anything was selected.  At the end, if nothing was selected, click on the tab for that warehouse and select the plants in that warehouse
                    //bool WasAnythingSelected = false;

                    // Clear out the current Selections
                    for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                    {
                        Matrix_Items.SelectRow(i, false, false);
                    }

                    // For each row already selected
                    for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                    {
                        // Grab the row's Plant ID column
                        //string plantID = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.MotherID).Cells.Item(i).Specific).Value;

                        // Grab the row's Warehouse Code column
                        string plantsWarehouseID = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.WhsCode).Cells.Item(i).Specific).Value;

                        // Grab the row's Stated ID column
                        string plantsStateID = ((EditText)Matrix_Items.Columns.Item((int)MatrixColumns.StateID).Cells.Item(i).Specific).Value;

                        // If the warehouse was scanned
                        if (WarehouseID != "")
                        {
                            // If the scanned warehouse code matches the row's warehouse code
                            if (WarehouseID == plantsWarehouseID)
                            {
                                // Select the row where the warehouse codes match
                                Matrix_Items.SelectRow(i, true, true);
                                //WasAnythingSelected = true;
                            }
                        }
                        // No warehouse code was passed, so we are trying to identify an individual plant
                        else
                        {
                            // If the plant's ID matches the scanned barcode
                            if (EditText_Lookup.Value == plantsStateID)
                            {
                                // Select the row where the plant ID's match
                                Matrix_Items.SelectRow(i, true, true);

                                //WasAnythingSelected = true;
                                ((EditText)Matrix_Items.Columns.Item(MatrixColumns.Quantity).Cells.Item(i).Specific).Active = true;
                            }
                        }
                    }

                    // Empty the barcode scanner field
                    EditText_Lookup.Value = "";

                    // Focus back into the barcode scanner's text field
                    EditText_Lookup.Active = true;
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                NSC_DI.UTIL.Misc.KillObject(EditText_Lookup);
                GC.Collect();
            }
        }

        #region Initial Form Load Methods
        private static void Load_Combobox_Warehouses(Form pForm)
        {
            // Prepare to run a SQL statement.
            SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            // Count how many current records exist within the database.
            string sql = $@"SELECT 
                [WhsCode]
                ,[WhsName]
                FROM [OWHS]
                WHERE
                [U_" + Globals.SAP_PartnerCode +
                     "_" + "WhrsType] in ('CUR','DRY','WET') AND [Inactive] = 'N'";
            if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})";                                       // #10823
            oRecordSet.DoQuery(sql);

            // Find the Combobox of Warehouses from the Form UI
            ComboBox ComboBox_Warehouse = null;
            try
            {
                ComboBox_Warehouse = pForm.Items.Item("CBX_WHSE").Specific;

                // If items already exist in the drop down
                if (ComboBox_Warehouse.ValidValues.Count > 0)
                {
                    // Remove all currently existing values from warehouse drop down
                    for (int i = ComboBox_Warehouse.ValidValues.Count; i-- > 0;)
                    {
                        ComboBox_Warehouse.ValidValues.Remove(i, BoSearchKey.psk_Index);
                    }
                }

                // If more than 1 warehouses exists
                if (oRecordSet.RecordCount > 1)
                {
                    // Create the first item as an empty item
                    ComboBox_Warehouse.ValidValues.Add("", "");

                    // Select the empty item (forcing the user to make a decision)
                    ComboBox_Warehouse.Select(0, BoSearchKey.psk_Index);
                }

                // Add allowed warehouses to the drop down
                for (int i = 0; i < oRecordSet.RecordCount; i++)
                {
                    try
                    {
                        ComboBox_Warehouse.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
                    }
                    catch { }
                    ComboBox_Warehouse.Item.Enabled = true;
                    oRecordSet.MoveNext();
                }

                // Auto select our warehouse if we only have one and Disable the control
                if (oRecordSet.RecordCount == 1)
                {
                    ComboBox_Warehouse.Active = false;
                    ComboBox_Warehouse.Item.Enabled = false;
                    ComboBox_Warehouse.Select(0, BoSearchKey.psk_Index);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(ComboBox_Warehouse);
                GC.Collect();
            }
        }

        private static void Load_Matrix(Form pForm)
        {
            string sqlQuery = string.Format(@"
SELECT
[OITM].[ItemCode]
,[OITM].[ItemName]
,[OBTQ].[Quantity] AS [OnHand]
,[OBTQ].[Quantity] AS [Quantity]
,[OBTQ].[WhsCode] AS [WhsCode]
,[OBTN].[DistNumber] AS [BatchNum]
,[OBTN].[U_NSC_MotherID] [MotherID]
,[OBTN].[U_NSC_GroupNum] AS [GroupNum]
,[OBTN].[MnfSerial]

FROM [OBTN] 
JOIN [OBTQ] ON [OBTQ].[ItemCode] = [OBTN].[ItemCode]
	AND [OBTQ].[SysNumber] = [OBTN].[SysNumber]
JOIN [OITM] ON [OITM].[ItemCode] = [OBTN].[ItemCode]
JOIN [OITB] ON [OITM].[ItmsGrpCod] = [OITB].[ItmsGrpCod]
JOIN [OWHS] ON [OBTQ].[WhsCode] = [OWHS].[WhsCode]
WHERE [OITB].[ItmsGrpNam] = '{0}'
	AND [OBTQ].[Quantity] > 0
", cItemType);
            var wh = CommonUI.Forms.GetField<string>(pForm, "CBX_WHSE", true);
            var branch = NSC_DI.SAP.Warehouse.GetBranch(wh);
            if (Globals.BranchDflt >= 0) sqlQuery += $" AND OWHS.BPLid IN ({branch})";     //10823-2
            // Load the matrix of tasks
            CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "OIBT", "MTX_ITEMS", new List<CommonUI.Matrix.MatrixColumn>() {
                            new CommonUI.Matrix.MatrixColumn(){ Caption="Item Code", ColumnWidth=40, ColumnName="ItemCode", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="Item Name", ColumnWidth=80, ColumnName="ItemName", ItemType = BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="Quantity", ColumnWidth=80, ColumnName="OnHand", ItemType = BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="Quantity", ColumnWidth=0, ColumnName="Quantity", ItemType = BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="Source Warehouse", ColumnWidth=0, ColumnName="WhsCode", ItemType = BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="Batch Number", ColumnWidth=80, ColumnName="BatchNum", ItemType = BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="State Id", ColumnWidth=80, ColumnName="MnfSerial", ItemType = BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="Group Number", ColumnWidth=30, ColumnName="GroupNum", ItemType = BoFormItemTypes.it_EDIT}
                        }, sqlQuery);
        }

        #endregion

        #region Matrix Click Events

        private void MTX_ITEMS_Click(ItemEvent pVal, Form pForm)
        {
            Matrix Matrix_Items = null;
            try
            {


                // Find the matrix in the form UI
                Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;

                string ItemCodeOfNewSelectedRow = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.ItemCode).Cells.Item(pVal.Row).Specific).Value;

                // Determine if this key press was of the same Item Group we had selected previously.
                bool deselectCurrent = false;
                List<int> rowsToReselect = new List<int>();
                for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                {
                    string ItemCodeToCompare = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.ItemCode).Cells.Item(i).Specific).Value;
                    // If the current row is now selected we can just continue looping.
                    if (!Matrix_Items.IsRowSelected(i) || i == pVal.Row)
                        continue;

                    rowsToReselect.Add(i);

                    if (!ItemCodeOfNewSelectedRow.Equals(ItemCodeToCompare))
                    {
                        deselectCurrent = true;
                    }
                }

                if (deselectCurrent)
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Item Code must be of the same type!", BoMessageTime.bmt_Short);

                    Matrix_Items.SelectRow(pVal.Row, false, false);
                    return;
                }

                // Grab the selected item in the Matrix
                int rowIndex = Matrix_Items.GetNextSelectedRow();
                SAPbouiCOM.EditText oEdit = Matrix_Items.GetCellSpecific("col_0", rowIndex);
                string selectedItemCode = oEdit.Value;

                // Prepare the SQL statement that will find the projected time via the item code
                string sql = @"
SELECT 
[STRAIN].[U_PrjctManicure] 
FROM
[OITM]
JOIN [@" + NSC_DI.Globals.tStrains + @"] AS [STRAIN] ON [OITM].[U_NSC_StrainID] = [STRAIN].[Code]
WHERE [OITM].[ItemCode] = '" + selectedItemCode + "'";


                // Grab the results from the SQL query
                Recordset ResultsFromSQL = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(sql);

                // Set the estimated days
                pForm.Items.Item("TXT_DAYS").Specific.Value = ResultsFromSQL.Fields.Item(0).Value.ToString();

                Matrix_Items.SetCellFocus(pVal.Row, 3);
            }
            catch (Exception ex)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    // there wasn't any error handling here before, so I'm just adding a message for debugging
                    var errMsg = NSC_DI.UTIL.Message.Format(ex);
                }
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                GC.Collect();
            }

        }

        private static void MAIN_MATRIX_LINK_PRESSED(ItemEvent pVal, Form pForm)
        {
            try
            {
                // Attempt to grab the selected row ID
                int SelectedRowID = pVal.Row;

                if (SelectedRowID > 0)
                {
                    // Which column stores the ID
                    int ColumnIDForIDOfItemSelected = 0;

                    // Get the ID of the note selected
                    string ItemSelected = pForm.Items.Item("MTX_ITEMS").Specific.Columns.Item(ColumnIDForIDOfItemSelected).Cells.Item(SelectedRowID).Specific.Value.ToString();

                    Form formProductionOrder = null;
                    try
                    {
                        formProductionOrder = Globals.oApp.Forms.GetForm("150", 0);
                        formProductionOrder.Select();
                    }
                    catch (Exception ex)
                    {
                        // there really shouldn't be branching logic here, so I'm adding this message here so we can see what is going on
                        if (System.Diagnostics.Debugger.IsAttached)
                        {
                            var errMsg = NSC_DI.UTIL.Message.Format(ex);
                            System.Diagnostics.Debugger.Break();
                        }
                        // Open up the Item Master Data form
                        Globals.oApp.ActivateMenuItem("3073");

                        // Grab the "Item Master" form from the UI
                        formProductionOrder = Globals.oApp.Forms.GetForm("150", 0);

                        // Freeze the "Item Master" form
                        formProductionOrder.Freeze(true);

                        // Change the "Item Master" form to find mode
                        formProductionOrder.Mode = BoFormMode.fm_FIND_MODE;

                        // Insert the Item Master ID in the appropriate text field
                        ((EditText)formProductionOrder.Items.Item("5").Specific).Value = ItemSelected;

                        // Click the "Find" button
                        ((Button)formProductionOrder.Items.Item("1").Specific).Item.Click();

                        // Un-Freeze the "Production Order" form
                        formProductionOrder.Freeze(false);
                    }
                    finally
                    {
                        NSC_DI.UTIL.Misc.KillObject(formProductionOrder);
                        GC.Collect();
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void CMB_WHSE_Selected(Form pForm)
        {
            //10823-2
            //
            // this method is required because if a method is called from an event handler,
            // the method must call MessageBox and not throw a new exception

            try
            {
                if (Globals.BranchDflt >= 0)
                    Load_Matrix(pForm);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
            }
        }

        private void CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            if (pVal.SelectedObjects == null) return;

            try
            {
                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_WH":
                        CommonUI.CFL.SetFieldsWH(pForm, pVal, "CBX_WHSE", "txtWH_CFL");
                        if (Globals.BranchDflt >= 0)//10823-2
                            Load_Matrix(pForm);
                        break;

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        #endregion

        #region Data Classes For Ease Of Use Of Multi-Selected Rows.

        private static ManicuredBudDataTransfer GetSelectedItemsFromMatrix(Form pForm)
        {
            ManicuredBudDataTransfer manicuredBudTransferData = new ManicuredBudDataTransfer();

            // Grab the matrix from the form ui
            Matrix Matrix_Items = null;
            EditText TextBox_Days = null;
            try
            {
                Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;

                // Grab the textbox for days from the form
                TextBox_Days = pForm.Items.Item("TXT_DAYS").Specific;

                int estimatedDays;
                if (!int.TryParse(TextBox_Days.Value, out estimatedDays))
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Please enter in a valid number for the days");

                    // Focus the cursor in the offending input
                    TextBox_Days.Active = true;
                    return null;
                }

                manicuredBudTransferData.DueDate = DateTime.Now.AddDays(estimatedDays);

                // For each row already selected
                for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                {
                    ManicuredBudSelectedRow newRow = new ManicuredBudSelectedRow();

                    ComboBox ComboBox_Warehouse = null;
                    try
                    {
                        // If the current row is now selected we can just continue looping.
                        if (!Matrix_Items.IsRowSelected(i))
                            continue;

                        newRow.ItemCode = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.ItemCode).Cells.Item(i).Specific).Value;
                        newRow.BatchNo = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.BatchNum).Cells.Item(i).Specific).Value;
                        newRow.SourceWarehouseCode = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.WhsCode).Cells.Item(i).Specific).Value;
                        newRow.Mx_RowID = i;
                        // Keep track of which warehouse was selected
                        // SelectedDestinationWarehouseID = "";

                        double parsedQuantity;
                        if (!double.TryParse(((EditText)Matrix_Items.Columns.Item(MatrixColumns.Quantity).Cells.Item(i).Specific).Value, out parsedQuantity))
                        {
                            // Send a message to the client
                            Globals.oApp.StatusBar.SetText("Please enter in a valid number for the quantity");

                            // Focus the cursor in the offending input
                            ((EditText)Matrix_Items.Columns.Item(MatrixColumns.Quantity).Cells.Item(i).Specific).Active = true;

                            return null;
                        }

                        newRow.Quantity = parsedQuantity;


                        // Find the ComboBox in the Form UI
                        ComboBox_Warehouse = pForm.Items.Item("CBX_WHSE").Specific;

                        // Grab the value from the ComboBox
                        newRow.DestinationWarehouseName = ComboBox_Warehouse.Value.ToString();

                        // Get the "description" from the ComboBox, where we are really storing the value
                        newRow.DestinationWarehouseCode = CommonUI.ComboBox.GetComboBoxValueOrDescription(ref pForm, "CBX_WHSE", null, newRow.DestinationWarehouseName);



                        // Add a row to our Transfer list.
                        manicuredBudTransferData.RowsToTransfer.Add(newRow);
                    }
                    catch (Exception ex1)
                    {

                    }
                    finally
                    {
                        NSC_DI.UTIL.Misc.KillObject(ComboBox_Warehouse);
                    }
                }
            }
            catch (Exception ex2)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex2));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(TextBox_Days);
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                GC.Collect();
            }
            return manicuredBudTransferData;
        }

        protected class ManicuredBudDataTransfer
        {
            public List<ManicuredBudSelectedRow> RowsToTransfer = new List<ManicuredBudSelectedRow>();
            public DateTime DueDate;
        }

        protected class ManicuredBudSelectedRow
        {
            public string ItemCode;
            public double Quantity;
            public string SourceWarehouseCode;
            public string BatchNo;
            public string DestinationWarehouseName;
            public string DestinationWarehouseCode;
            public int Mx_RowID;
        }

        #endregion
    }
}