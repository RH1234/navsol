﻿using System;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Xml;

namespace NavSol.Forms.Wet_Processing
{
    class F_WetProcessing_VM : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------
        private ProgressBar progressBar;
        const string cFormID                            = "NSC_WET_PROC_VM";
        const string cItemType                          = "Wet Unb Flower";
        const string cRawItemProp                       = "Wet Cannabis";
        private const string cProcess                   = "WET";
        private static System.Data.DataTable _dtByProd  = new System.Data.DataTable("ByProd");

        public static SAPbouiCOM.Form _SBO_Form;

        private enum MatrixColumns { DocNum, ItemName, ItemCode, PlannedQuantity, BatchID, StateID, HarvName, CreateDate, Warehouse, DestinationWarehouse, DocEnty, MotherID,
                                     GroupNum, Quantity, QtyID, TrimQty, TrimID, WasteQty, RcptItemCode
        }

        private static long _lastKeyDownPress = -1;

        public class WetBudDataTransfer
        {
            public List<WetBudSelectedRow> RowsToTransfer = new List<WetBudSelectedRow>();
        }

        public class WetBudSelectedRow
        {
            public int ProductionKey;
            public string ItemCode;
            public string MotherId;
            public string SourceWarehouseCode;
            public string DestinationWarehouseCode;
            public string FirstBillOfMaterialItemCode;

            public bool WasGrouped { get; set; }
            public string GroupNumber { get; set; }

            public double Qty;
            public string QtyID;
            public double TrimQty;
            public string TrimID;
            public double WasteQty;

            public List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> ListOfGoodsReceiptLines = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();
        }
        #endregion Vars
        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPressed(ItemEvent pVal)
        {
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            switch (pVal.ItemUID)
            {
                case "BTN_LOOKUP":
                    BTN_LOOKUP_ITEM_PRESSED(oForm);
                    break;

                case "BTN_APPLY":
                    BTN_APPLY_PRESSED_NEW_2(oForm);
                    //Clear any remarks
                    SAPbouiCOM.EditText oEdit = _SBO_Form.Items.Item("TXT_REM").Specific;
                    oEdit.Value = "";
                    break;

                case "MTX_ITEMS":
                    MTX_ITEMS_Click(oForm, pVal);
                    SetRemarks();
                    break;

                case "btnWaste":
                    BTN_ReportWaste(oForm);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return true;
        }
        #endregion BEFORE EVENT
        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
        [B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
        {
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "MTX_ITEMS":
                    MAIN_MATRIX_LINK_PRESSED(oForm, pVal);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        [B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
        public virtual void OnAfterFormResize(ItemEvent pVal)
        {
            Form oForm = null;
            Matrix Matrix_Items = null;
            try
            {
                oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
                Matrix_Items = oForm.Items.Item("MTX_ITEMS").Specific;

                Matrix_Items.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                // 
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
            }
        }
        #endregion

        #region SUBS

        public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
        {
            Item oItm = null;
            try
            {
                var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
                {
                    oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
                    oItm.Top = -20;
                    oItm.Specific.Value = pCallingFormUID;
                }
                FormSetup(oForm);

                return oForm;
            }
            catch (Exception ex)
            {
                  throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private static void FormSetup(Form pForm)
        {
            try
            {
                CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_MAIN", @"curing-icon.bmp");

                if (NSC_DI.UTIL.Settings.Version.GetCompliance() == "METRC")
                {
                    pForm.Items.Item("staWaste").Visible = true;
                    pForm.Items.Item("txtWaste").Visible = true;
                    pForm.Items.Item("cboWaste").Visible = true;
                    pForm.Items.Item("btnWaste").Visible = true;

                    NavSol.CommonUI.ComboBox.ComboBoxAdd_UDT_CN(pForm, "cboWaste", NSC_DI.Globals.tWasteTypes);
                }

                Load_Matrix(pForm);

                //Add Columns
                if (_dtByProd.Columns.Count == 0)
                {
                    _dtByProd.Columns.Add("ItemCode");
                    _dtByProd.Columns.Add("ItemName");
                    _dtByProd.Columns.Add("PlannedQty");
                    _dtByProd.Columns.Add("wareHouse");
                    _dtByProd.Columns.Add("LineNum");
                    _dtByProd.Columns.Add("BatchID");
                    _dtByProd.Columns.Add("StateID");

                }
                else
                {
                    _dtByProd.Clear();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                pForm.VisibleEx = true;
            }
        }

        private static void Load_AvailableActions(Form pForm)
        {
            ButtonCombo ButtonCombo_Action = null;
            try
            {
                ButtonCombo_Action = pForm.Items.Item("CBT_ACTION").Specific;

                // If items already exist in the drop down
                if (ButtonCombo_Action.ValidValues.Count > 0)
                {
                    // Remove all currently existing values from warehouse drop down
                    for (int i = ButtonCombo_Action.ValidValues.Count; i-- > 0;)
                    {
                        ButtonCombo_Action.ValidValues.Remove(i, BoSearchKey.psk_Index);
                    }
                }

                //CBT_ACTION.ValidValues.Add("RECEIPT", "Receipt Items");
                ButtonCombo_Action.ValidValues.Add("RECEIPT_CLOSE", "Receipt & Close");

                ButtonCombo_Action.Select("RECEIPT_CLOSE", BoSearchKey.psk_ByValue);

                //CBT_ACTION.Caption = "Actions";
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(ButtonCombo_Action);
                GC.Collect();
            }
        }

        private void SetRemarks()
        {
            try
            {
                //Get the Remarks Textbox
                SAPbouiCOM.EditText oEdit = _SBO_Form.Items.Item("TXT_REM").Specific;
                // Grab the Matrix from the form 
                SAPbouiCOM.Matrix matrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", _SBO_Form);
                //Get selected production order
                string ProdNumder = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Production Order");

                string Remarks = NSC_DI.UTIL.SQL.GetValue<string>(@"Select OWOR.Comments from OWOR where OWOR.DocNum=" + ProdNumder);
                oEdit.Value = Remarks;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox("Please Select The Production Order");
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void Load_Matrix(Form pForm)
        {
            try
            {
                // Prepare a list of columns to go into the 
                List<CommonUI.Matrix.MatrixColumn> ListOfMatrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
            {
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="DocNum", Caption="Production Order", ColumnWidth=30, ItemType = BoFormItemTypes.it_LINKED_BUTTON  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Item Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemCode", Caption="Item Code", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="PlannedQty", Caption="Planned", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="BatchID",    Caption="Batch",               ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="StateID",    Caption="State ID",            ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                new CommonUI.Matrix.MatrixColumn(){ Caption="Harvest Name",     ColumnWidth=80, ColumnName="HarvName", ItemType = BoFormItemTypes.it_EDIT},
                //new SAP_BusinessOne.Helpers.Matrix.MatrixColumn(){ ColumnName="CmpltQty", Caption="Completed", ColumnWidth=80, ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Created On", ColumnWidth=60, ItemType = BoFormItemTypes.it_EDIT  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="Warehouse", Caption="Warehouse ID", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="destinationWarehouse", Caption="Destination Warehouse", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="DocEntry", Caption="DocEntry", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="MotherID", Caption="MotherID", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="GroupNum", Caption="GroupNum", ColumnWidth=30, ItemType = BoFormItemTypes.it_EDIT  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="Quantity", Caption="Quantity", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable = true  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="QtyID", Caption="Quantity State ID", ColumnWidth=30, ItemType = BoFormItemTypes.it_EDIT, IsEditable = true  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="TrimQty", Caption="Wet Unbatched Trim", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable = true  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="TrimID", Caption="Trim State ID", ColumnWidth=30, ItemType = BoFormItemTypes.it_EDIT, IsEditable = true  },
                new CommonUI.Matrix.MatrixColumn(){ ColumnName="WasteQty", Caption="Waste", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable = true  },
                new CommonUI.Matrix.MatrixColumn(){ColumnName = "RcptItemCode", Caption = "Receipt Item Code", ColumnWidth = 0, ItemType = BoFormItemTypes.it_EDIT}
     };

                var itemNames = GetAutoItemsFromBOM();

                var wasteQry = "0.0";
                if ((NSC_DI.UTIL.Settings.Version.GetCompliance() == "METRC"))
                {
                    // get the total waste already recived
                    var property = NSC_DI.SAP.Items.Property.GetFieldName("Cannabis Waste");
                    wasteQry = $"(SELECT SUM(Quantity) FROM IGN1 INNER JOIN OITM ON IGN1.ItemCode = OITM.ItemCode WHERE IGN1.BaseEntry = [OWOR].[DocEntry] AND OITM." + property + " = 'Y')";
                }

                string sqlQuery = $@"
SELECT [OWOR].[DocEntry], [OWOR].[DocNum], [OITM].[ItemName], [OWOR].[ItemCode], [OWOR].[PlannedQty], [OWOR].[CreateDate], [OWOR].[Warehouse]
,(  SELECT TOP 1
    [WOR1].[wareHouse] 
    FROM [WOR1] 
    WHERE [WOR1].[DocEntry] = [OWOR].[DocNum] ) AS [destinationWarehouse]
,(  SELECT TOP 1 U_NSC_MotherID
    FROM OBTN 
    WHERE OBTN.ItemCode = [OWOR].ItemCode) as MotherID
,(  SELECT [OBTN].[U_NSC_GroupNum]
    FROM [OBTN]
    WHERE [OBTN].[DistNumber] = [OWOR].[U_NSC_PLN_BATCH] ) AS [GroupNum]
, '' as  [RightPadding]
, 0.0 AS [Quantity]
, SPACE(32) AS [QtyID]
, 0.0 AS [TrimQty]
, SPACE(32) AS [TrimID]
, {wasteQry} AS [WasteQty],
(SELECT TOP 1 OBTN.DistNumber
                		   FROM OBTN 
                           JOIN [ITL1] ON [OBTN].ItemCode = [ITL1].ItemCode AND [OBTN].SysNumber = [ITL1].SysNumber  
                           JOIN [OITL] ON [ITL1].[LogEntry] = [OITL].[LogEntry]
                          WHERE [OITL].BaseEntry = [OWOR].DocEntry AND [OITL].DocType = 60) AS BatchID,
                        (SELECT TOP 1 OBTN.ItemCode
                		   FROM OBTN 
                           JOIN [ITL1] ON [OBTN].ItemCode = [ITL1].ItemCode AND [OBTN].SysNumber = [ITL1].SysNumber  
                           JOIN [OITL] ON [ITL1].[LogEntry] = [OITL].[LogEntry]
                          WHERE [OITL].BaseEntry = [OWOR].DocEntry AND [OITL].DocType = 60) AS RcptItemCode,
 (SELECT TOP 1 OBTN.U_NSC_HarvestName
                		   FROM OBTN 
                           JOIN [ITL1] ON [OBTN].ItemCode = [ITL1].ItemCode AND [OBTN].SysNumber = [ITL1].SysNumber  
                           JOIN [OITL] ON [ITL1].[LogEntry] = [OITL].[LogEntry]
                          WHERE [OITL].BaseEntry = [OWOR].DocEntry AND [OITL].DocType = 60) AS [HarvName], 
                        (SELECT TOP 1 OBTN.MnfSerial 
                		   FROM OBTN 
                           JOIN [ITL1] ON [OBTN].ItemCode = [ITL1].ItemCode AND [OBTN].SysNumber = [ITL1].SysNumber  
                           JOIN [OITL] ON [ITL1].[LogEntry] = [OITL].[LogEntry]
                          WHERE [OITL].BaseEntry = [OWOR].DocEntry AND [OITL].DocType = 60) AS StateID
FROM [OWOR]
JOIN [OITM] ON [OITM].[ItemCode] = [OWOR].[ItemCode]
JOIN [OITB] ON [OITB].[ItmsGrpCod] = [OITM].[ItmsGrpCod]
JOIN [OWHS] ON [OWHS].[WhsCode] = [OWOR].[Warehouse]
WHERE [OWOR].[Status] = 'R'
AND [OITB].[ItmsGrpNam] = '{cItemType}'";
                if (Globals.BranchDflt >= 0) sqlQuery += $" AND OWHS.BPLid IN ({Globals.BranchList})"; //10823-2
                sqlQuery += @"ORDER BY CONVERT(int, [OWOR].[DocNum]) DESC";
                // Load "Dry Cannabis" disassembly production orders into the matrix
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(
                    pForm: pForm,
                    DatabaseTableName: "OWOR",
                    MatrixUID: "MTX_ITEMS",
                    ListOfColumns: ListOfMatrixColumns,
                SQLQuery: sqlQuery);

                if ((NSC_DI.UTIL.Settings.Version.GetCompliance() == "METRC"))
                {
                    pForm.Items.Item("MTX_ITEMS").Specific.Columns.Item(MatrixColumns.WasteQty).Editable = false;
                }
                else
                {
                    // NavSol.CommonUI.ComboBox.ComboBoxAdd_UDT_CN(pForm, "MTX_ITEMS", (int)MatrixColumns.WasteType, NSC_DI.Globals.tWasteTypes);
                    //  oRcv.Lines.UserFields.Fields.Item("U_NSC_WasteType").Value = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_CompCode FROM [@{NSC_DI.Globals.tWasteTypes}] WHERE Code = '{wasteTypeCode}'", "");
                }

                // Grab the matrix from the form UI
                pForm.Items.Item("MTX_ITEMS").Specific.SelectionMode = BoMatrixSelect.ms_Auto;

                // Update the combo button box options available actions are
                //Load_AvailableActions(pForm);
            }
            catch (WarningException ex) { }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static IEnumerable<string> GetAutoItemsFromBOM()
        {
            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("Use Template Items") == "Y")
                {
                    try
                    {
                        return NSC_DI.SAP.BillOfMaterials.GetByProductNames("~WUFL", false);
                    }
                    catch (WarningException ex) { throw ex; }
                    catch (NSC_DI.SAP.B1Exception ex)
                    {
                        Globals.oApp.MessageBox(ex.Message);
                        throw new WarningException();
                    }
                }
                else
                {
                    throw new NotImplementedException();
                    //return GetAutoItemsFromBOM_OLD().Select().Select(n => n["ItemName"] as string);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static System.Data.DataTable GetAutoItemsFromBOM_OLD()
        {
            try
            {
                string qry = @"Select REPLACE(REPLACE([Auto].[U_ItemName], '[StrainName] - ', ''),'~','') AS [ItemName]
FROM [@" + NSC_DI.Globals.tAutoItem + @"] AS [Auto]
WHERE [Auto].U_ItemGroupCode = 111  or [Auto].[U_ItemCode] IN (
    SELECT [Bom].[U_ItemCode]
    FROM [@" + NSC_DI.Globals.tAutoItemBOM + @"] AS [Bom]
    JOIN [@" + NSC_DI.Globals.tAutoItem + @"] AS [AUTO2] ON [AUTO2].[U_ItemCode] = [Bom].[U_ParentItem]
    JOIN [OITB] ON [OITB].[ItmsGrpCod] = [AUTO2].[U_ItemGroupCode]
    WHERE [OITB].[ItmsGrpNam] ='" + cItemType + @"')  and [Auto].U_ItemGroupCode !=110";
                System.Data.DataTable dt = NSC_DI.UTIL.SQL.DataTable(qry, "U_ItemCode");
                return dt;
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static void BTN_APPLY_PRESSED_NEW(Form pForm)
        {
            // Grab the ComboButton from the form UI
            Matrix oMat = null;
            SAPbobsCOM.ProductionOrders oPdO = null;
            SAPbobsCOM.Documents oRcv = null;

            try
            {
                //if (pForm.Items.Item("CBT_ACTION").Specific.Selected == null)
                //{
                //    Globals.oApp.StatusBar.SetText("Please select an action!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                //    return;
                //}

                // Grab the Matrix from the form 
                oMat = pForm.Items.Item("MTX_ITEMS").Specific;
                var lineNum = -1;
                var indx = oMat.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);
                if (indx < 0)
                {
                    Globals.oApp.StatusBar.SetText("Please select at least one row!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    throw new System.ComponentModel.WarningException();
                }


                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                while (indx >= 0)
                {
                    oPdO = NSC_DI.SAP.ProductionOrder.OpenPdO(Convert.ToInt32(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.DocNum).UniqueID, indx).Value));
                    double dblPlannedQty = oPdO.PlannedQuantity;
                    double dblTotalReceivedQty = 0.00;

                    //Inter-Company Check 
                    //string InterCoSubCode   = NSC_DI.UTIL.SQL.GetValue<string>($@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 where T0.WhsCode = '{oPdO.Warehouse}'");
                    //string InterCo          = NSC_DI.UTIL.SQL.GetValue<string>("select T0.U_Value from dbo.[@NSC_SETTINGS] T0 where T0.Name = 'Inter-Company'");

                    oRcv = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                    var rawMatlBatch = NSC_DI.SAP.ProductionOrder.GetBatchNumberFromIssue(oPdO.DocumentNumber, null, cRawItemProp);
                    var rawBatchRec  = NSC_DI.SAP.BatchItems.GetInfo(rawMatlBatch);

                    if (Globals.BranchDflt >= 0)
                        oRcv.BPL_IDAssignedToInvoice = NSC_DI.UTIL.SQL.GetValue<int>($"Select BPLId FROM OWHS WHERE WHSCODE = '{oPdO.Warehouse}'");// 10823 - 4

                    //-------------------------
                    // RECEIVE THE REMAINING MANUAL ITEMS.
                    //-------------------------
                    // PARENT
                    if (Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.Quantity).UniqueID, indx).Value) > 0)
                    {
                        //var subBatch = NSC_DI.UTIL.AutoStrain.NextBatch(oPdO.ItemNo, InterCoSubCode);
                        //subBatch     = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, subBatch);
                        var subBatch = NSC_DI.SAP.BatchItems.NextBatch(oPdO.ItemNo);
                        //try
                        //{
                        //    //Inter-Company Check 
                        //    if (InterCo == "Y" && InterCoSubCode.Trim().Length > 0)
                        //    {
                        //        subBatch = InterCoSubCode.Trim() + "-" + subBatch;
                        //    }

                       //}
                       //catch
                       //{
                       //    throw new Exception(NSC_DI.UTIL.Message.Format("Inter-Company Error: Please check your settings (Y or N) and make sure you have a subsidiary code on your warehouse."));
                       //}
                        oRcv.Lines.BaseType                              = 0;
                        oRcv.Lines.BaseEntry                             = oPdO.DocumentNumber;
                        oRcv.Lines.TransactionType                       = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                        oRcv.Lines.WarehouseCode                         = oPdO.Warehouse;
                        oRcv.Lines.Quantity                              = Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.Quantity).UniqueID, indx).Value);
                        dblTotalReceivedQty                              = oRcv.Lines.Quantity;//Qty Holder for validation that total is lessed than planned
                        oRcv.Lines.BatchNumbers.BatchNumber              = subBatch;
                        oRcv.Lines.BatchNumbers.Quantity                 = oRcv.Lines.Quantity;
                        oRcv.Lines.BatchNumbers.ManufacturerSerialNumber = oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.QtyID).UniqueID, indx).Value;

                        NSC_DI.SAP.Document.SetDefaultDistributionRules(oRcv.Lines);
                        // copy and set batch UDFs
                        NSC_DI.UTIL.UDO.CopyUDFs(oRcv.Lines.BatchNumbers, rawBatchRec);

                        var baseQty = oRcv.Lines.Quantity;
                    }
                    //var pByProdBatch = GetBatchNumber(oPdO.DocumentNumber, cRawItemProp);

                    //-------------------------
                    // TRIM
                    lineNum = NSC_DI.SAP.ProductionOrder.GetLineNum(oPdO.DocumentNumber, "Wet Trim");
                    if (lineNum >= 0)
                    {
                        oPdO.Lines.SetCurrentLine(lineNum);
                        if (Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.TrimQty).UniqueID, indx).Value) > 0)
                        {
                            if (Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.Quantity).UniqueID, indx).Value) > 0)
                            {
                                oRcv.Lines.Add();
                            }
                            oRcv.Lines.BaseType      = 0;
                            oRcv.Lines.BaseEntry     = oPdO.DocumentNumber;
                            oRcv.Lines.BaseLine      = lineNum;
                            oRcv.Lines.WarehouseCode = oPdO.Warehouse;
                            oRcv.Lines.Quantity      = -(oPdO.Lines.BaseQuantity * Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.TrimQty).UniqueID, indx).Value));
                            dblTotalReceivedQty     += oRcv.Lines.Quantity; //Qty Holder to validate against so planned is not greater than total received

                            //var subBatch             = NSC_DI.UTIL.AutoStrain.NextBatch(oPdO.Lines.ItemNo, InterCoSubCode);
                            //subBatch                 = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, subBatch);
                            var subBatch = NSC_DI.SAP.BatchItems.NextBatch(oPdO.Lines.ItemNo);

                            oRcv.Lines.BatchNumbers.BatchNumber              = subBatch;
                            oRcv.Lines.BatchNumbers.Quantity                 = oRcv.Lines.Quantity;
                            oRcv.Lines.BatchNumbers.ManufacturerSerialNumber = oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.TrimID).UniqueID, indx).Value;
                            //oRcv.Lines.TransactionType          = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete; 
                            NSC_DI.SAP.Document.SetDefaultDistributionRules(oRcv.Lines);
                            NSC_DI.UTIL.UDO.CopyUDFs(oRcv.Lines.BatchNumbers, rawBatchRec);
                        }
                    }

                    //-------------------------
                    // WASTE
                    lineNum = NSC_DI.SAP.ProductionOrder.GetLineNum(oPdO.DocumentNumber, "Cannabis Waste");
                    double dblWaste = 0.0;
                    if (lineNum >= 0 && NSC_DI.UTIL.Settings.Version.GetCompliance() != "METRC")
                    {
                        oPdO.Lines.SetCurrentLine(lineNum);
                        if (Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.WasteQty).UniqueID, indx).Value) > 0)
                        {
                            if (Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.Quantity).UniqueID, indx).Value) > 0 || Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.TrimQty).UniqueID, indx).Value) > 0)
                            {
                                oRcv.Lines.Add();
                            }
                            oRcv.Lines.BaseType  = 0;
                            oRcv.Lines.BaseEntry = oPdO.DocumentNumber;
                            oRcv.Lines.BaseLine  = lineNum;
                            //oRcv.Lines.TransactionType          = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                            //Commented out on 8/6/18 waste was being sent to drying warehouse instead of destruction (9999) 
                            //oRcv.Lines.WarehouseCode = oPdO.Warehouse;
                            //Updated the warehouse to point to what is set on the BOM for Waste only! Other items should go to the designated warehouses.
                            oRcv.Lines.Quantity      = -(oPdO.Lines.BaseQuantity * Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.WasteQty).UniqueID, indx).Value));
                            dblTotalReceivedQty     += oRcv.Lines.Quantity;//Qty Holder for validation that total is lessed than planned
                            dblWaste                 = oRcv.Lines.Quantity;
                            //var subBatch             = NSC_DI.UTIL.AutoStrain.NextBatch(oPdO.Lines.ItemNo, InterCoSubCode);
                            //subBatch                 = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, subBatch);
                            var subBatch = NSC_DI.SAP.BatchItems.NextBatch(oPdO.Lines.ItemNo);

                            oRcv.Lines.BatchNumbers.BatchNumber = subBatch;
                            oRcv.Lines.BatchNumbers.Quantity    = oRcv.Lines.Quantity;
                            NSC_DI.UTIL.UDO.CopyUDFs(oRcv.Lines.BatchNumbers, rawBatchRec);
                            NSC_DI.SAP.Document.SetDefaultDistributionRules(oRcv.Lines);
                            oRcv.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_QuarState").Value = "DEST";
                            oRcv.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_TimerQuar").Value = NSC_DI.UTIL.Dates.CurrentTimeStamp.ToString();
                            oRcv.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_WasteType").Value = pForm.Items.Item("cboWaste").Specific.Value;
                        }
                    }

                    //Validate the sum of products to create is not greater than the Planned amount
                    if (dblTotalReceivedQty > dblPlannedQty)
                    {
                        Globals.oApp.MessageBox("Your Quantity Received is Greater than Planned. Please adjust your quantities.");
                        return;//Rollback transaction.
                    }

                    // add the receipt
                    if (oRcv.Add() != 0)
                    {
                        Globals.oApp.MessageBox("Failed to Receipt from Production for batch " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                        throw new System.ComponentModel.WarningException();
                    }

                    NSC_DI.SAP.ProductionOrder.UpdateStatus(oPdO.DocumentNumber, BoProductionOrderStatusEnum.boposClosed);
                    
                    indx = oMat.GetNextSelectedRow(indx, BoOrderType.ot_RowOrder);
                }

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                Load_Matrix(pForm);
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                NSC_DI.UTIL.Misc.KillObject(oMat);
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                NSC_DI.UTIL.Misc.KillObject(oRcv);
                GC.Collect();
            }
        }

        private static void BTN_ReportWaste(Form pForm)
        {
            SAPbouiCOM.Matrix oMat = null;
            SAPbobsCOM.Documents oRcv = null;
            SAPbobsCOM.ProductionOrders oPdO = null;

            try
            {
                oMat = pForm.Items.Item("MTX_ITEMS").Specific;
                var row = oMat.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);
                var PdO = Convert.ToInt32(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.DocNum).UniqueID, row).Value);
                int WastelineNum = NSC_DI.SAP.ProductionOrder.GetLineNum(PdO, "Cannabis Waste");
                if (WastelineNum < 0)
                {
                    Globals.oApp.StatusBar.SetText("There is no waste row.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    return;
                }

                if (pForm.Items.Item("txtWaste").Specific.Value == "")
                {
                    Globals.oApp.StatusBar.SetText("You need to enter a quantity.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    return;
                }
                var wasteQty = -1D;
                if (!double.TryParse(pForm.Items.Item("txtWaste").Specific.Value, out wasteQty))
                {
                    Globals.oApp.StatusBar.SetText("Please validate that Quantity is a numeric value and try again.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    return;
                }
                if (wasteQty <= 0D)
                {
                    Globals.oApp.StatusBar.SetText("The Quantity must be greater than zero.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    return;
                }
                if (pForm.Items.Item("cboWaste").Specific.Value == "")
                {
                    Globals.oApp.StatusBar.SetText("You must specify a Waste Type.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    return;
                }                

                oPdO = NSC_DI.SAP.ProductionOrder.OpenPdO(PdO);
                oPdO.Lines.SetCurrentLine(WastelineNum);

                // get the total waste quantities and planned quantity
                var previousWasteQty = Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.WasteQty).UniqueID, row).Value);
                if (previousWasteQty + wasteQty > oPdO.PlannedQuantity)
                {
                    Globals.oApp.StatusBar.SetText("The total Quantity is more that the Planned Quantity.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    return;
                }

                //Inter-Company Check 
                // DELETE 12/30/2020  string InterCoSubCode = NSC_DI.UTIL.SQL.GetValue<string>($@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 where T0.WhsCode = '{oPdO.Warehouse}'");
                // DELETE 12/30/2020 string InterCo = NSC_DI.UTIL.SQL.GetValue<string>("select T0.U_Value from dbo.[@NSC_SETTINGS] T0 where T0.Name = 'Inter-Company'");

                oRcv = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                // OP10348 gets the employee id, if the returned val is null then the UDF is not set. Circumvents SAP setting the UDF to 0
                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null) oRcv.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                oRcv.Lines.BaseType      = 0;
                oRcv.Lines.BaseEntry     = PdO;
                oRcv.Lines.BaseLine      = WastelineNum;
                oRcv.Lines.WarehouseCode = oPdO.Lines.Warehouse;
                oRcv.Lines.Quantity      = wasteQty;

                //var subBatch = NSC_DI.UTIL.AutoStrain.NextBatch(oPdO.Lines.ItemNo, InterCoSubCode);
                //subBatch = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, subBatch);
                var subBatch = NSC_DI.SAP.BatchItems.NextBatch(oPdO.Lines.ItemNo);

                var rawMatlBatch = NSC_DI.SAP.ProductionOrder.GetBatchNumberFromIssue(oPdO.DocumentNumber, null, cRawItemProp);
                var rawBatchRec = NSC_DI.SAP.BatchItems.GetInfo(rawMatlBatch);

                oRcv.Lines.BatchNumbers.BatchNumber = subBatch;
                oRcv.Lines.BatchNumbers.Quantity = oRcv.Lines.Quantity;
                NSC_DI.UTIL.UDO.CopyUDFs(oRcv.Lines.BatchNumbers, rawBatchRec);
                oRcv.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_QuarState").Value = "DEST";
                oRcv.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_TimerQuar").Value = NSC_DI.UTIL.Dates.CurrentTimeStamp.ToString();
                oRcv.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_WasteType").Value = pForm.Items.Item("cboWaste").Specific.Value;
                if (Globals.BranchDflt >= 0)
                    oRcv.BPL_IDAssignedToInvoice = NSC_DI.UTIL.SQL.GetValue<int>($"Select BPLId FROM OWHS WHERE WHSCODE = '{oPdO.Lines.Warehouse}'");// 10823
                // should eventually remove this code and call productionorder.recieve

                if (oRcv.Add() != 0)
                {
                    Globals.oApp.MessageBox("Failed to Receipt from Production for batch " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                    throw new System.ComponentModel.WarningException();
                }

                Load_Matrix(pForm);

                Globals.oApp.StatusBar.SetText("Waste reportred successfully", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMat);
                NSC_DI.UTIL.Misc.KillObject(oRcv);
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                GC.Collect();
            }
        }

        private static void BTN_APPLY_PRESSED_OLD(Form pForm)
        {
            // Grab the ComboButton from the form UI
            ButtonCombo ButtonCombo_Action = null;
            Matrix Matrix_Items = null;
            var transStatus = BoWfTransOpt.wf_RollBack;
            try
            {
                ButtonCombo_Action = pForm.Items.Item("CBT_ACTION").Specific;

                if (ButtonCombo_Action.Selected == null)
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Please select an action!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    return;
                }

                // Grab the Matrix from the form 
                Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;

                WetBudDataTransfer curedBudToTransfer = GetSelectedItemsFromMatrix(pForm);

                if (curedBudToTransfer.RowsToTransfer.Count <= 0)
                {
                    //Send success message to the client
                    Globals.oApp.StatusBar.SetText(
                        Text: "Please select at least one row!",
                        Seconds: BoMessageTime.bmt_Medium,
                        Type: BoStatusBarMessageType.smt_Error);

                    throw new System.ComponentModel.WarningException();
                }

                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                foreach (WetBudSelectedRow row in curedBudToTransfer.RowsToTransfer)
                {

                    //Go through the MX and grab the wieghts the column name is the item. (Column is UD_"X")
                    SAPbouiCOM.Matrix oMX = _SBO_Form.Items.Item("MTX_ITEMS").Specific;
                    int j = 0;
                    SAPbouiCOM.EditText OEdit = null;
                    SAPbouiCOM.EditText OEditItem = null;
                    Dictionary<string, string> ByProductAndWeight = new Dictionary<string, string>();
                    var byProductUDFs = new Dictionary<string, Dictionary<string, string>>();
                    for (int i = 1; i <= oMX.RowCount; i++)
                    {
                        if (!oMX.IsRowSelected(i)) continue;
                        OEdit = oMX.GetCellSpecific("col_0", i);
                        //OEditItem = oMX.GetCellSpecific("col_1", i);
                        //string strItemName = OEditItem.Value;
                        //string[] strStrainID = strItemName.Split('-');

                        var itemCode = CommonUI.Forms.GetField<string>(pForm, "MTX_ITEMS", i, "col_2");
                        var strainName = NSC_DI.SAP.Items.GetStrainName(itemCode);


                        if (row.ProductionKey.ToString() == OEdit.Value)
                        {
                            for (j = 0; j < oMX.Columns.Count; j++)
                            {
                                if (oMX.Columns.Item(j).Title == "Quantity")
                                {
                                    // get the state ID for the Quantity 
                                    ByProductAndWeight.Add(oMX.GetCellSpecific(2, i).Value, oMX.GetCellSpecific(j, i).Value);
                                    var u = new Dictionary<string, string>() { { "ManufacturerSerialNumber", oMX.GetCellSpecific(j + 1, i).Value as string } };
                                    byProductUDFs.Add(oMX.GetCellSpecific(2, i).Value, u);
                                    j++;
                                    continue;
                                }
                                string strHolder = oMX.Columns.Item(j).DataBind.Alias;

                                if (strHolder.StartsWith("UDS_") == true)
                                {

                                    var colItemName = "~" + oMX.Columns.Item(j).Title;
                                    var fmtItemName = NSC_DI.UTIL.AutoStrain.GetAutoItemNameValue(colItemName, strainName);

                                    //Get the weight of the item and set the dictionary values
                                    OEdit = oMX.GetCellSpecific(j, i);
                                    //Get ItemCode
                                    string ItemCode = NSC_DI.UTIL.SQL.GetValue<string>($"Select ItemCode from OITM where ItemName = '{fmtItemName}'");
                                    //ByProductAndWeight.Add(strStrainID[0]+"-"+ oMX.Columns.Item(j).Title, OEdit.Value);
                                    ByProductAndWeight.Add(ItemCode, OEdit.Value);
                                    if (ItemCode.IndexOf("WA", StringComparison.InvariantCultureIgnoreCase) == -1)
                                    {
                                        var u = new Dictionary<string, string>() { { "ManufacturerSerialNumber", CommonUI.Forms.GetField<string>(pForm, "MTX_ITEMS", i, j + 1) } };
                                        byProductUDFs.Add(ItemCode, u);
                                        j++;
                                    }
                                }
                            }
                        }
                    }

                    // go get the batch info from the wet canabis issue for this PdO
                    var batch = NSC_DI.SAP.ProductionOrder.GetBatchNumberFromIssue(row.ProductionKey, null, "Wet Cannabis");
                    var UserDefinedFields = NSC_DI.SAP.BatchItems.GetInfo(batch);

                    var PdOReceiptKey = NSC_DI.SAP.ProductionOrder.ReceiveCuring(row.ProductionKey, "WET", cRawItemProp, ByProductAndWeight, UserDefinedFields, byProductUDFs);



                    if (PdOReceiptKey == 0)
                    {
                        Globals.oApp.MessageBox("Failed to Receipt from Production for batch " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                        throw new System.ComponentModel.WarningException();
                    }

                    NSC_DI.SAP.ProductionOrder.UpdateStatus(row.ProductionKey, BoProductionOrderStatusEnum.boposClosed);
                }

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                //_SBO_Form.Close();

                #region OldCode
                /*
                foreach (CuredBudSelectedRow row in curedBudToTransfer.RowsToTransfer)
                {
                // Attempt to create a "Receipt from Production" document
                NSC_DI.SAP.ReceiptFromProduction.Create(
                        productionOrderKey: row.ProductionKey,
                        documentDate: DateTime.Now,
                        listOfGoodsReceiptLines: row.ListOfGoodsReceiptLines,
                        CallingForm: "Curring"
                );

                if (!NSC_DI.SAP.ReceiptFromProduction.WasSuccessful)
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText(
                        Text: "Failed to create production order!",
                        Seconds: BoMessageTime.bmt_Medium,
                        Type: BoStatusBarMessageType.smt_Error);

                    AllCuredBudPassed = false;
                    continue;
                }

                TotalProcessed += 1;

                // Based off of the currently selected action
                if (ButtonCombo_Action.Selected.Value == "RECEIPT_CLOSE")
                {
                    // Change the status of the production order.
                    NSC_DI.SAP.ProductionOrder.UpdateStatus(row.ProductionKey, BoProductionOrderStatusEnum.boposClosed);
                }

                // API: Creating cured cannabis items
                try
                {
                    #region Washington State API

                    // Prepare a settings controller
                    VirSci_SAP.Controllers.Settings _VirSci_Controllers_Settings = new Controllers.Settings(SAPBusinessOne_Application: _SBO_Application, SAPBusinessOne_Company: _SBO_Company, SAPBusinessOne_Form: _SBO_Form);

                    // Prepare a connection to the state API
                    var TraceCon = new VirSci_SAP.Controllers.TraceabilityAPI(SAPBusinessOne_Application: _SBO_Application,
                        SAPBusinessOne_Company: _SBO_Company,
                        SAPBusinessOne_Form: _SBO_Form);

                    BioTrack.API btAPI = TraceCon.new_API_obj();

                    // Prepare a list of new plant weight items
                    List<BioTrack.Plant.PlantWeights> ListOfNewPlantsWeights = new List<BioTrack.Plant.PlantWeights>();

                    Dictionary<int, GroupSplitNeeded> DictOfPlantWeights = new Dictionary<int, GroupSplitNeeded>();

                    // Prepare a controller for "ItemGroups"
                    SAP_BusinessOne.Controllers.ItemGroups controllerItemGroup = new SAP_BusinessOne.Controllers.ItemGroups(
                        SAPBusinessOne_Application: _SBO_Application,
                        SAPBusinessOne_Company: _SBO_Company);

                    // For each item with entered weight from the user
                    foreach (var item in row.ListOfGoodsReceiptLines)
                    {
                        // Get State ID for item group of an item.
                        int ItemGroupStateID = Convert.ToInt32(controllerItemGroup.GetStateIDFromItemCode(ItemCode: item.ItemCode));

                        if (DictOfPlantWeights.ContainsKey(ItemGroupStateID))
                        {
                            DictOfPlantWeights[ItemGroupStateID].NeedsSplit = true;
                            DictOfPlantWeights[ItemGroupStateID].plantWeight.Amount += item.Quantity;
                            DictOfPlantWeights[ItemGroupStateID].IndividualWeights.Add(item.Quantity);

                        }
                        else
                        {
                            // Create a new entry
                            DictOfPlantWeights.Add(ItemGroupStateID, new GroupSplitNeeded()
                            {
                                NeedsSplit = false,
                                plantWeight = new BioTrack.Plant.PlantWeights()
                                {
                                    InvType = (BioTrack.Inventory.InvTypes)ItemGroupStateID,
                                    Amount = item.Quantity,
                                    UOM = BioTrack.API.UnitOfMeasurement.g
                                },
                                IndividualWeights = new List<double>() { item.Quantity }
                            });
                        }

                        // Add the BioCrap plant weight item
                        ListOfNewPlantsWeights.Add(
                            new BioTrack.Plant.PlantWeights()
                            {
                                InvType = (BioTrack.Inventory.InvTypes)ItemGroupStateID,
                                Amount = item.Quantity,
                                UOM = BioTrack.API.UnitOfMeasurement.g
                            }
                        );
                    }

                    if (row.WasGrouped)
                    {
                        SAP_BusinessOne.Controllers.ItemGroups controllerItemGroups = new SAP_BusinessOne.Controllers.ItemGroups(
                           SAPBusinessOne_Application: _SBO_Application,
                           SAPBusinessOne_Company: _SBO_Company);

                        // Prepare an SQL statement that will grab the current parent items state ID and current warehouse location.
                        //                        string groupStateIdsQuery = string.Format(@"
                        //SELECT 
                        //    [OBTN].[U_VSC_StateID] AS [StateId],[OBTN].[U_VSC_CollectAdd]
                        //, ( SELECT [OIBT].[WhsCode] 
                        //    FROM [OIBT] 
                        //    WHERE [OIBT].[BatchNum] = [IBT1].[BatchNum]) AS [Warehouse]
                        //FROM [OBTN]
                        //JOIN [IBT1] ON [IBT1].BatchNum = [OBTN].[DistNumber]
                        //JOIN [OITM] ON [OITM].[ItemCode] = [OBTN].[ItemCode]
                        //JOIN [OITB] ON [OITB].[ItmsGrpCod] = [OITM].[ItmsGrpCod]
                        //WHERE [OBTN].[U_VSC_GroupNum] = '{0}'
                        //    AND [OITB].ItmsGrpCod = '{1}'
                        //	AND [IBT1].[BaseType] = 60", row.GroupNumber, controllerItemGroups.GetSAPItemGroupCodeFromGroupType(wetCannabisType));

                        string groupStateIdsQuery = string.Format(@"SELECT
                [OWHS].WhsCode AS [Warehouse],OSRN.U_VSC_CollectAdd,OSRN.U_VSC_StateID as [StateId]
                FROM [OSRN]
                JOIN [OWHS]
                ON [OWHS].[WhsCode] = (SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber])
                where [OSRN].U_VSC_GroupNum='{0}'", row.GroupNumber);

                        // Prepare a new connection to the SQL server
                        SAP_BusinessOne.Helpers.SQL dataAccessLayer = new SAP_BusinessOne.Helpers.SQL(SAPBusinessOne_Company: _SBO_Company);
                        Recordset dbResults = dataAccessLayer.GetRecordSetFromSQLQuery(groupStateIdsQuery);

                        string warehouse = "";
                        string CollectAdditional = "";
                        List<string> barcodesFromGroup = new List<string>();
                        for (int i = 0; i < dbResults.RecordCount; i++)
                        {
                            string stateId = dbResults.Fields.Item("StateId").Value.ToString();
                            string warehouseCode = dbResults.Fields.Item("Warehouse").Value.ToString(); ;

                            if (string.IsNullOrEmpty(stateId))
                            {
                                dbResults.MoveNext();
                                continue;
                            }

                            barcodesFromGroup.Add(dbResults.Fields.Item("StateId").Value.ToString());
                            warehouse = dbResults.Fields.Item("Warehouse").Value.ToString();
                            CollectAdditional = dbResults.Fields.Item("U_VSC_CollectAdd").Value.ToString();
                            dbResults.MoveNext();
                        }

                        // Basically just combining types that are the same.. We will create new items but they will use the same stateId.
                        ListOfNewPlantsWeights = new List<BioTrack.Plant.PlantWeights>();
                        foreach (var item in DictOfPlantWeights.Values)
                        {
                            ListOfNewPlantsWeights.Add(item.plantWeight);
                        }

                        // Grab the XML for the API call.
                        if (CollectAdditional == "1")
                        {
                            BioTrack.Plant.Cure(
                                BioTrackAPI: ref btAPI
                                , BarcodeID: barcodesFromGroup.ToArray()
                                , weights: ListOfNewPlantsWeights.ToArray()
                                , CollectAdditional: true
                                , Room: Convert.ToInt32(warehouse)
                                , Location: _VirSci_Controllers_Settings.GetValueOfSetting("StateLocLic")
                            );
                        }
                        else
                        {
                            BioTrack.Plant.Cure(
                                BioTrackAPI: ref btAPI
                                , BarcodeID: barcodesFromGroup.ToArray()
                                , weights: ListOfNewPlantsWeights.ToArray()
                                , CollectAdditional: false
                                , Room: Convert.ToInt32(warehouse)
                                , Location: _VirSci_Controllers_Settings.GetValueOfSetting("StateLocLic")
                             );
                        }

                    }
                    else
                    {

                        // Prepare an SQL statement that will grab the current parent items state ID and current warehouse location.
                        string SQL_GetParentItemInformationFromProductionOrderID = @"
                SELECT
                [OBTN].[U_VSC_StateID]  AS [MotherPlant_StateID],[OBTN].[U_VSC_CollectAdd]
                , ( SELECT [OIBT].[WhsCode] 
                FROM [OIBT] 
                WHERE [OIBT].[BatchNum] = [IBT1].[BatchNum]) AS [MotherPlant_CurrentWarehouseStateID]
                FROM [OBTN]
                JOIN [IBT1] ON [IBT1].BatchNum = [OBTN].[DistNumber]
                WHERE [IBT1].[BaseType] = 60
                AND [IBT1].[BaseEntry] = (  SELECT [IGE1].[DocEntry] 
                            FROM [IGE1] 
                            WHERE [IGE1].[BaseRef] = '" + (new SqlParameter("BaseRef", row.ProductionKey)).Value.ToString() + "')";

                        // Prepare a new connection to the SQL server
                        SAP_BusinessOne.Helpers.SQL dataAccessLayer = new SAP_BusinessOne.Helpers.SQL(SAPBusinessOne_Company: _SBO_Company);

                        // Grab results from the SQL server
                        Fields ResultsFromSQL = dataAccessLayer.GetFieldsFromSQLQuery(SQLQuery: SQL_GetParentItemInformationFromProductionOrderID);
                        string MotherPlant_StateID = ResultsFromSQL.Item("MotherPlant_StateID").Value.ToString();
                        string MotherPlant_CurrentWarehouseStateID = ResultsFromSQL.Item("MotherPlant_CurrentWarehouseStateID").Value.ToString();
                        string CollectAdditional = ResultsFromSQL.Item("U_VSC_CollectAdd").Value.ToString();

                        // Grab the XML for the API call.
                        if (CollectAdditional == "1")
                        {
                            BioTrack.Plant.Cure(
                                BioTrackAPI: ref btAPI
                                , BarcodeID: new string[] { MotherPlant_StateID }
                                , weights: ListOfNewPlantsWeights.ToArray()
                                , CollectAdditional: true
                                , Room: Convert.ToInt32(MotherPlant_CurrentWarehouseStateID)
                                , Location: _VirSci_Controllers_Settings.GetValueOfSetting("StateLocLic")
                            );
                        }
                        else
                        {
                            BioTrack.Plant.Cure(
                                BioTrackAPI: ref btAPI
                                , BarcodeID: new string[] { MotherPlant_StateID }
                                , weights: ListOfNewPlantsWeights.ToArray()
                                , CollectAdditional: false
                                , Room: Convert.ToInt32(MotherPlant_CurrentWarehouseStateID)
                                , Location: _VirSci_Controllers_Settings.GetValueOfSetting("StateLocLic")
                            );
                        }

                    }

                    // Prepare a new controller for "Compliance"
                    VirSci_SAP.Controllers.Compliance controllerCompliance = new Controllers.Compliance(SAPBusinessOne_Application: _SBO_Application, SAPBusinessOne_Company: _SBO_Company);

                    // Create a new line item for compliance
                    string newlyCreatedComplianceID = controllerCompliance.CreateComplianceLineItem(
                         Reason: Models.Compliance.ComplianceReasons.Plant_Cure
                         , API_Call: btAPI.XmlApiRequest.ToString()
                     );

                    // API: Run "Plant Cure" during the curing phase.
                    btAPI.PostToApi();

                    if (btAPI.WasSuccesful)
                    {
                        controllerCompliance.UpdateComliancy(
                            ComplianceID: newlyCreatedComplianceID,
                            ResponseXML: btAPI.xDocFromResponse.ToString(),
                            Status: Controllers.Compliance.ComplianceStatus.Success);

                        XmlDocument reponseDocument = btAPI.xmlDocFromResponse;

                        // Prepare a new SQL helper.
                        SAP_BusinessOne.Helpers.SQL helperSQL = new SAP_BusinessOne.Helpers.SQL(SAPBusinessOne_Company: _SBO_Company);

                        // For each batch item we are creating.
                        foreach (var goodsReceiptLineItem in row.ListOfGoodsReceiptLines)
                        {
                            // Prepare a SQL parameter for the item code
                            SqlParameter sqlParameter_ItemCode = new SqlParameter("ItemCode", value: goodsReceiptLineItem.ItemCode);

                            // SQL query that will return the state ID for an item group given an item code.
                            string sql_query_getItemGroupStateIDFromAnItemCode = @"
                SELECT [OITB].[U_VSC_StateID]
                FROM [OITB]
                JOIN [OITM] ON [OITM].[ItmsGrpCod] = [OITB].[ItmsGrpCod]
                WHERE [OITM].[ItemCode] = '" + sqlParameter_ItemCode.Value.ToString() + @"'";

                            // Get the item groups state ID from the SQL query
                            string itemGroupStateCode = helperSQL.GetFieldsFromSQLQuery(SQLQuery: sql_query_getItemGroupStateIDFromAnItemCode).Item(Index: 0).Value.ToString();

                            // Find the newly created state barcode ID by matching the state item group ID

                            int indexOfBarCode = 0;
                            foreach (XmlNode responce in reponseDocument.DocumentElement.GetElementsByTagName("barcode_type"))
                            {
                                string stateCode = responce.InnerXml;
                                if (stateCode == itemGroupStateCode)
                                {
                                    break;
                                }
                                indexOfBarCode++;
                            }

                            string newStateID = btAPI.xmlDocFromResponse.DocumentElement.GetElementsByTagName("barcode_id")[indexOfBarCode].InnerXml;

                            // Prepare a SQL parameter for the state ID
                            SqlParameter sqlParameter_StateID = new SqlParameter("StateID", newStateID);

                            // Prepare a SQL parameter for the batch number
                            SqlParameter sqlParameter_BatchNum = new SqlParameter("BatchNum", value: goodsReceiptLineItem.ListOfBatchLineItems[0].BatchNumber);

                            // Prepare a SQL statement to update a batch with the newly created ID.
                            string sql_query_UpdateBatchWithStateID = @"
                UPDATE [OIBT] 
                SET [OIBT].[U_VSC_StateID] = '" + sqlParameter_StateID.Value.ToString() + @"'
                WHERE [OIBT].[BatchNum] = '" + sqlParameter_BatchNum.Value.ToString() + @"'";

                            // Update the batch with the newly created state barcode ID
                            helperSQL.RunSQLQuery(SQLQuery: sql_query_UpdateBatchWithStateID);
                        }
                    }
                    // The post to the state API failed!
                    else
                    {
                        controllerCompliance.UpdateComliancy(
                            ComplianceID: newlyCreatedComplianceID,
                            ResponseXML: btAPI.xDocFromResponse.ToString(),
                            Status: Controllers.Compliance.ComplianceStatus.Failed);

                        // Send failure message to the client
                        _SBO_Application.StatusBar.SetText(
                            Text: "Failed to post to the state!",
                            Seconds: BoMessageTime.bmt_Medium,
                            Type: BoStatusBarMessageType.smt_Error);

                        this.Load_Matrix();

                        return;
                    }
                    #endregion
                }
                catch { }

                }

                if (AllCuredBudPassed)
                {
                // Send success message to the client
                Globals.oApp.StatusBar.SetText(
                    Text: "Successfully Receipt the Selected Items!",
                    Seconds: BoMessageTime.bmt_Short,
                    Type: BoStatusBarMessageType.smt_Success);
                }
                else
                {
                if (TotalProcessed > 0)
                {
                    // Send success message to the client
                    Globals.oApp.StatusBar.SetText(
                            Text: "Failed to Receipt some of the Selected Items!",
                        Seconds: BoMessageTime.bmt_Medium,
                            Type: BoStatusBarMessageType.smt_Error);
                }
                else
                {
                    // Send success message to the client
                    _SBO_Application.StatusBar.SetText(
                        Text: "Failed to Receipt all Selected Items!",
                        Seconds: BoMessageTime.bmt_Medium,
                        Type: BoStatusBarMessageType.smt_Error);
                }
                }
                */
                #endregion

                Load_Matrix(pForm);


            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                NSC_DI.UTIL.Misc.KillObject(ButtonCombo_Action);
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                GC.Collect();
            }
        }

        private static void BTN_APPLY_PRESSED_NEW_2(Form pForm)
        {
            SAPbouiCOM.Matrix oMat = null;

            try
            {
                //Check if Byproducts Exist
                oMat = pForm.Items.Item("MTX_ITEMS").Specific;
                var row = oMat.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);
                if (row < 0)
                {
                    Globals.oApp.StatusBar.SetText("Select a row.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    throw new System.ComponentModel.WarningException();
                }
                var PdO             = Convert.ToInt32(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.DocNum).UniqueID, row).Value);
                int WetlineNum      = NSC_DI.SAP.ProductionOrder.GetLineNum(PdO, "Wet Trim");
                int WastelineNum    = NSC_DI.SAP.ProductionOrder.GetLineNum(PdO, "Cannabis Waste");
                int NoOfByProducts  = NSC_DI.UTIL.SQL.GetValue<int>("SELECT COUNT(WOR1.ItemCode) FROM WOR1 WHERE WOR1.DocEntry = " + PdO + " AND WOR1.PlannedQty < 0 AND WOR1.IssueType='M' AND WOR1.LineNum NOT IN ('" + WetlineNum + "','" + WastelineNum + "')");

                if (NoOfByProducts > 0)
                {
                    if (Globals.oApp.MessageBox($"There are ByProducts to Report for Production Order {PdO}.\n What would you like to do?", 2, " Assign Values ", " Cancel ") == 2) return; // continue;
                }
                else
                {
                    // no byproducts
                    AddReceipt(pForm);
                    return;
                }

                var pFormID = _SBO_Form.UniqueID; // Save unique id to get form in callback
                SAPbouiCOM.DataTable dtByProd = null;

                try
                {
                    if (_SBO_Form.DataSources.DataTables.Item("BySrc").IsEmpty == false)
                    {
                        _SBO_Form.DataSources.DataTables.Item("BySrc").Clear();
                        dtByProd = _SBO_Form.DataSources.DataTables.Item("BySrc");
                    }
                }
                catch
                {
                    dtByProd = _SBO_Form.DataSources.DataTables.Add("BySrc");
                }

                string strSQL = @"SELECT WOR1.ItemCode,OITM.ItemName, WOR1.PlannedQty,SPACE(10) AS Warehouse,WOR1.LineNum
                                           ,'                                                         ' as [BatchID]
                                           ,'                                                         ' as [StateID] 
                                from WOR1 WITH (NOLOCK)
                                                    join OITM WITH (NOLOCK) on WOR1.ItemCode = OITM.ItemCode
                                                    where WOR1.DocEntry = " + PdO + " and WOR1.PlannedQty < 0 " +
                                    " and WOR1.IssueType='M' and WOR1.LineNum NOT IN ('" + WetlineNum + "','" + WastelineNum + "')";

                dtByProd.ExecuteQuery(strSQL);

                var sql = $"SELECT Warehouse FROM OWOR WHERE DocEntry = '{PdO}'";
                var wh = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
                string InterCoSubCode = NSC_DI.SAP.Warehouse.Get_InterCoCode(wh);

                // open the form; ByProduct Planned Quantity must be set
                F_ByProdSelect.FormCreate(pFormID, dtByProd);

                F_ByProdSelect.ByProdsCB = delegate (string callingFormUid, System.Data.DataTable pdtByProd)
                {
                    if (pdtByProd == null || pdtByProd.Rows.Count < 1) return;
                    _dtByProd.Clear();
                    foreach (System.Data.DataRow dr in pdtByProd.Rows)
                    {
                        if (double.Parse(dr["PlannedQty"].ToString()) <= 0) continue;
                        var batch = NSC_DI.SAP.BatchItems.NextBatch(dr["ItemCode"].ToString(), "BP", InterCoSubCode);

                        _dtByProd.Rows.Add(dr["ItemCode"], dr["ItemName"], dr["PlannedQty"], dr["Warehouse"], dr["LineNum"],
                                           batch,
                                           dr["StateID"]);
                    }

                    Form oForm = B1Connections.theAppl.Forms.GetForm(cFormID, 0);
                    //oForm.Select();
                    AddReceipt(oForm);
                    return;
                };
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }

        private static void AddReceipt(Form pForm)
        {
            // Grab the ComboButton from the form UI
            SAPbouiCOM.Matrix oMat           = null;
            SAPbobsCOM.ProductionOrders oPdO = null;
            SAPbobsCOM.Documents oRcv        = null;

            try
            {

                // Grab the Matrix from the form 
                oMat = pForm.Items.Item("MTX_ITEMS").Specific;
                var lineNum = -2;
                var visLine = -2;
                var indx = oMat.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);

                oPdO = NSC_DI.SAP.ProductionOrder.OpenPdO(Convert.ToInt32(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.DocNum).UniqueID, indx).Value));

                var batchNum     = oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.BatchID).UniqueID, indx).Value;
                var rcptItemCode = oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.RcptItemCode).UniqueID, indx).Value;
                var batchNumDet  = NSC_DI.SAP.BatchItems.GetInfo(batchNum, rcptItemCode);

                double dblPlannedQty        = oPdO.PlannedQuantity;
                double dblTotalReceivedQty  = 0.00;

                //Check if Byproducts Exist
                int DrylineNum      = NSC_DI.SAP.ProductionOrder.GetLineNum(oPdO.DocumentNumber, "Dry Unbatched Trim");
                int WastelineNum    = NSC_DI.SAP.ProductionOrder.GetLineNum(oPdO.DocumentNumber, "Cannabis Waste");
                int NoOfByProducts  = NSC_DI.UTIL.SQL.GetValue<int>("select Count(WOR1.ItemCode) from WOR1 where WOR1.DocEntry = " + oPdO.DocumentNumber + " and WOR1.PlannedQty < 0 and WOR1.IssueType='M' and WOR1.LineNum NOT IN ('" + DrylineNum + "','" + WastelineNum + "')");

                //Inter-Company Check 
                string InterCoSubCode = NSC_DI.SAP.Warehouse.Get_InterCoCode(oPdO.Warehouse);


                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                oRcv = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                var rawMatlBatch = NSC_DI.SAP.ProductionOrder.GetBatchNumberFromIssue(oPdO.DocumentNumber, null, cRawItemProp);
                var rawBatchRec  = NSC_DI.SAP.BatchItems.GetInfo(rawMatlBatch);

                //-------------------------
                // RECEIVE THE REMAINING MANUAL ITEMS.
                //-------------------------
                // PARENT
                if (Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.Quantity).UniqueID, indx).Value) > 0)
                {
                    //var subBatch = NSC_DI.UTIL.AutoStrain.NextBatch(oPdO.ItemNo, InterCoSubCode);
                    //subBatch = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, subBatch);
                    var subBatch = NSC_DI.SAP.BatchItems.NextBatch(oPdO.ItemNo);

                    oRcv.Lines.BaseType = 0;
                    oRcv.Lines.BaseEntry = oPdO.DocumentNumber;
                    //oRcv.Lines.TransactionType                       = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                    oRcv.Lines.WarehouseCode = oPdO.Warehouse;
                    oRcv.Lines.Quantity = Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.Quantity).UniqueID, indx).Value);
                    dblTotalReceivedQty = oRcv.Lines.Quantity;//Qty Holder for validation that total is lessed than planned
                    oRcv.Lines.BatchNumbers.BatchNumber = subBatch;
                    oRcv.Lines.BatchNumbers.Quantity = oRcv.Lines.Quantity;
                    oRcv.Lines.BatchNumbers.ManufacturerSerialNumber = oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.QtyID).UniqueID, indx).Value;
                    // copy and set batch UDFs
                    NSC_DI.UTIL.UDO.CopyUDFs(oRcv.Lines.BatchNumbers, rawBatchRec);

                    var baseQty = oRcv.Lines.Quantity;
                }

                //-------------------------
                // TRIM
                lineNum = NSC_DI.SAP.ProductionOrder.GetLineNum(oPdO.DocumentNumber, "Dry Unbatched Trim");
                visLine = NSC_DI.SAP.ProductionOrder.GetLineNum(oPdO.DocumentNumber, "Dry Unbatched Trim");
                if (lineNum >= 0)
                {
                    oPdO.Lines.SetCurrentLine(visLine);
                    if (Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.TrimQty).UniqueID, indx).Value) > 0)
                    {
                        if (Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.Quantity).UniqueID, indx).Value) > 0) oRcv.Lines.Add();

                        oRcv.Lines.BaseType = 0;
                        oRcv.Lines.BaseEntry = oPdO.DocumentNumber;
                        oRcv.Lines.BaseLine = lineNum;
                        //oRcv.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                        oRcv.Lines.WarehouseCode = oPdO.Warehouse;
                        oRcv.Lines.Quantity = -(oPdO.Lines.BaseQuantity * Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.TrimQty).UniqueID, indx).Value));
                        dblTotalReceivedQty += oRcv.Lines.Quantity;//Qty Holder to validate against so planned is not greater than total received
                        //var subBatch             = NSC_DI.UTIL.AutoStrain.NextBatch(oPdO.Lines.ItemNo, InterCoSubCode);
                        //subBatch = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, subBatch);
                        var subBatch = NSC_DI.SAP.BatchItems.NextBatch(oPdO.Lines.ItemNo);

                        oRcv.Lines.BatchNumbers.BatchNumber = subBatch;
                        oRcv.Lines.BatchNumbers.Quantity = oRcv.Lines.Quantity;
                        oRcv.Lines.BatchNumbers.ManufacturerSerialNumber = oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.TrimID).UniqueID, indx).Value;
                        NSC_DI.UTIL.UDO.CopyUDFs(oRcv.Lines.BatchNumbers, rawBatchRec);
                    }
                    string harvestNameSQL = NSC_DI.UTIL.SQL.GetValue<string>($"Select U_NSC_HarvestName from OBTN where itemCode = '{rcptItemCode}' and DistNumber ='{batchNum}'");
                    string harvestDateSQL = NSC_DI.UTIL.SQL.GetValue<string>($"Select U_NSC_HarvestDate from OBTN where itemCode = '{rcptItemCode}' and DistNumber ='{batchNum}'");
                    if (!string.IsNullOrEmpty(harvestNameSQL?.Trim()))// Whitespace-Change (!NSC_DI.UTIL.Strings.Empty(harvestNameSQL))
                        oRcv.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_HarvestName").Value = harvestNameSQL;
                    if (!string.IsNullOrEmpty(harvestDateSQL?.Trim()))// Whitespace-Change !NSC_DI.UTIL.Strings.Empty(harvestDateSQL))
                        oRcv.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_HarvestDate").Value = harvestDateSQL;
                }

                //-------------------------
                // WASTE
                lineNum = NSC_DI.SAP.ProductionOrder.GetLineNum(oPdO.DocumentNumber, "Cannabis Waste");
                visLine = NSC_DI.SAP.ProductionOrder.GetVisOrder(oPdO.DocumentNumber, "Cannabis Waste");
                double dblWaste = 0.0;
                if (lineNum >= 0)
                {
                    oPdO.Lines.SetCurrentLine(visLine);
                    dblWaste = Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.WasteQty).UniqueID, indx).Value);
                    if (NSC_DI.UTIL.Settings.Version.GetCompliance() == "METRC")
                    {
                        dblTotalReceivedQty += dblWaste;
                    }
                    else
                    {
                        if (dblWaste > 0)
                        {
                            if (Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.Quantity).UniqueID, indx).Value) > 0 ||
                                Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.TrimQty).UniqueID, indx).Value) > 0) oRcv.Lines.Add();

                            //oRcv.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;                            
                            oRcv.Lines.BaseType = 0;
                            oRcv.Lines.BaseEntry = oPdO.DocumentNumber;
                            oRcv.Lines.BaseLine = lineNum;
                            oRcv.Lines.WarehouseCode = oPdO.Lines.Warehouse;
                            oRcv.Lines.Quantity = -(oPdO.Lines.BaseQuantity * Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.WasteQty).UniqueID, indx).Value));
                            dblTotalReceivedQty += oRcv.Lines.Quantity;//Qty Holder for validation that total is lessed than planned
                            dblWaste = oRcv.Lines.Quantity;
                            //var subBatch             = NSC_DI.UTIL.AutoStrain.NextBatch(oPdO.Lines.ItemNo, InterCoSubCode);
                            //subBatch                 = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, subBatch);
                            var subBatch = NSC_DI.SAP.BatchItems.NextBatch(oPdO.Lines.ItemNo);

                            oRcv.Lines.BatchNumbers.BatchNumber = subBatch;
                            oRcv.Lines.BatchNumbers.Quantity = oRcv.Lines.Quantity;
                            NSC_DI.UTIL.UDO.CopyUDFs(oRcv.Lines.BatchNumbers, rawBatchRec);
                            oRcv.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_QuarState").Value = "DEST";
                            oRcv.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_TimerQuar").Value = NSC_DI.UTIL.Dates.CurrentTimeStamp.ToString();

                        }
                    }
                }

                // set the Branch
                var br = NSC_DI.SAP.Branch.Get(oRcv.Lines.WarehouseCode);
                if (br > 0) oRcv.BPL_IDAssignedToInvoice = br;

                dblTotalReceivedQty += NSC_DI.SAP.ProductionOrder.ReceiveByProducts(oPdO.DocumentNumber, oRcv, _dtByProd, batchNumDet);

                //Validate the sum of products to create is not greater than the Planned amount
                //if (Math.Round(dblTotalReceivedQty, 2) > Math.Round(dblPlannedQty, 2))
                if (dblTotalReceivedQty > dblPlannedQty)
                {
                    Globals.oApp.MessageBox("Your Quantity Received is Greater than Planned. Please adjust your quantities." + Environment.NewLine + $"Received: {dblTotalReceivedQty}     Planned: {dblPlannedQty}");
                    return;
                }

                if (oRcv.Add() != 0)
                {
                    Globals.oApp.MessageBox("Failed to Receipt from Production for batch " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                    throw new System.ComponentModel.WarningException();
                }

                //NSC_DI.SAP.ProductionOrder.ReceiveByProducts(oPdO.DocumentNumber, _dtByProd);
                NSC_DI.SAP.ProductionOrder.UpdateStatus(oPdO.DocumentNumber, BoProductionOrderStatusEnum.boposClosed);

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                Load_Matrix(pForm);
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                NSC_DI.UTIL.Misc.KillObject(oMat);
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                NSC_DI.UTIL.Misc.KillObject(oRcv);
                GC.Collect();
            }
        }

        private static WetBudDataTransfer GetSelectedItemsFromMatrix(Form pForm)
        {
            WetBudDataTransfer driedBudTransferData = new WetBudDataTransfer();
            Matrix Matrix_Items = null;
            try
            {
                // Grab the matrix from the form ui
                Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;

                int incrementBatch = 0;
                // For each row already selected
                for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                {
                    try
                    {
                        // If the current row is now selected we can just continue looping.
                        if (!Matrix_Items.IsRowSelected(i))
                            continue;
                        WetBudSelectedRow newRow = new WetBudSelectedRow();
                        newRow.ProductionKey = Int32.Parse(((EditText)Matrix_Items.Columns.Item(MatrixColumns.DocNum).Cells.Item(i).Specific).Value);
                        newRow.ItemCode = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.ItemCode).Cells.Item(i).Specific).Value;
                        newRow.MotherId = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.MotherID).Cells.Item(i).Specific).Value;
                        newRow.SourceWarehouseCode = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.Warehouse).Cells.Item(i).Specific).Value;
                        newRow.DestinationWarehouseCode = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.DestinationWarehouse).Cells.Item(i).Specific).Value;
                        //newRow.FirstBillOfMaterialItemCode = NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(itemCode: newRow.ItemCode).FirstOrDefault().ItemCode.ToString();

                        string groupNumber = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.GroupNum).Cells.Item(i).Specific).Value;
                        newRow.WasGrouped = !string.IsNullOrEmpty(groupNumber);
                        newRow.GroupNumber = groupNumber;

                        newRow.Qty = double.Parse(((EditText)Matrix_Items.Columns.Item(MatrixColumns.Quantity).Cells.Item(i).Specific).Value);
                        newRow.MotherId = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.QtyID).Cells.Item(i).Specific).Value;
                        newRow.TrimQty = double.Parse(((EditText)Matrix_Items.Columns.Item(MatrixColumns.TrimQty).Cells.Item(i).Specific).Value);
                        newRow.TrimID = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.TrimID).Cells.Item(i).Specific).Value;
                        newRow.WasteQty = double.Parse(((EditText)Matrix_Items.Columns.Item(MatrixColumns.WasteQty).Cells.Item(i).Specific).Value);

                        // Add a row to our Transfer list.
                        driedBudTransferData.RowsToTransfer.Add(newRow);
                    }
                    catch (Exception e) { }
                }

                return driedBudTransferData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                GC.Collect();
            }
        }

        private static void BTN_LOOKUP_ITEM_PRESSED(Form pForm)
        {
            EditText Text_Lookup = null;
            Matrix Matrix_Items = null;
            try
            {
                Text_Lookup = pForm.Items.Item("TXT_LOOKUP").Specific;

                // If nothing was passed in the textbox, just return
                if (Text_Lookup.Value.Length == 0)
                {
                    return;
                }

                // Grab the matrix from the form UI
                Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;

                // Keep track of the warehouse ID from the barcode (if passed)
                string WarehouseID = "";

                // If the first four characters of the barcode are "WHSE", then the rest of the barcode contains the warehouse code.
                if (Text_Lookup.Value.Length >= 4 && Text_Lookup.Value.Substring(0, 4) == "WHSE")
                {
                    WarehouseID = Text_Lookup.Value.Replace("WHSE-", "");
                }

                // Keep track if anything was selected.  At the end, if nothing was selected, click on the tab for that warehouse and select the plants in that warehouse
                bool WasAnythingSelected = false;

                // Clear out the current Selections
                for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                {
                    Matrix_Items.SelectRow(i, false, false);
                }

                // For each row already selected
                for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                {
                    // Grab the row's Plant ID column
                    string plantID = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.MotherID).Cells.Item(i).Specific).Value;

                    // Grab the row's Warehouse Code column
                    string plantsWarehouseID = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.Warehouse).Cells.Item(i).Specific).Value;

                    // Grab the row's Stated ID column
                    string plantsStateID = "";//((SAPbouiCOM.EditText)MTX_PLANTS.Columns.Item(6).Cells.Item(i).Specific).Value;

                    // If the warehouse was scanned
                    if (WarehouseID != "")
                    {
                        // If the scanned warehouse code matches the row's warehouse code
                        if (WarehouseID == plantsWarehouseID)
                        {
                            // Select the row where the warehouse codes match
                            Matrix_Items.SelectRow(i, true, true);
                            WasAnythingSelected = true;
                        }
                    }
                    // No warehouse code was passed, so we are trying to identify an individual plant
                    else
                    {
                        // If the plant's ID matches the scanned barcode
                        if (Text_Lookup.Value == plantID || Text_Lookup.Value == plantsStateID)
                        {
                            // Select the row where the plant ID's match
                            Matrix_Items.SelectRow(i, true, true);

                            WasAnythingSelected = true;
                        }
                    }
                }

                // Empty the barcode scanner field
                Text_Lookup.Value = "";

                if (!WasAnythingSelected)
                {

                }

                // Focus back into the barcode scanner's text field
                Text_Lookup.Active = true;
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                NSC_DI.UTIL.Misc.KillObject(Text_Lookup);
                GC.Collect();
            }
        }

        private void UpdateProgressBar(string Message, int ProgressToIncreaseBy = 0)
        {
            try
            {
                // Set progress bar message
                progressBar.Text = Message;

                // If we are increasing the progress bar size
                if (ProgressToIncreaseBy > 0)
                {
                    // Calculate the new progress
                    int NewProgressBarValue = progressBar.Value + ProgressToIncreaseBy;

                    // Make sure our new progress amount is not greater than the maxium allowed
                    if (NewProgressBarValue <= progressBar.Maximum)
                    {
                        progressBar.Value = NewProgressBarValue;
                    }
                }
            }
            catch { }

        }

        #endregion

        #region Matrix Click Events

        private void MTX_ITEMS_Click(Form pForm, ItemEvent SAP_UI_ItemEvent)
        {
            // Find the matrix in the form UI
            Matrix Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;

            try
            {
                string ItemCodeOfNewSelectedRow = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.ItemCode).Cells.Item(SAP_UI_ItemEvent.Row).Specific).Value;

                // Determine if this key press was of the same Item Group we had selected previously.
                bool deselectCurrent = false;
                List<int> rowsToReselect = new List<int>();
                for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                {
                    string ItemCodeToCompare = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.ItemCode).Cells.Item(i).Specific).Value;
                    // If the current row is now selected we can just continue looping.
                    if (!Matrix_Items.IsRowSelected(i) || i == SAP_UI_ItemEvent.Row)
                        continue;

                    rowsToReselect.Add(i);

                    if (!ItemCodeOfNewSelectedRow.Equals(ItemCodeToCompare))
                    {
                        deselectCurrent = true;
                    }
                }

                if (deselectCurrent)
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText(
                        Text: "Item Code must be of the same type!",
                        Seconds: BoMessageTime.bmt_Short,
                        Type: BoStatusBarMessageType.smt_Error);

                    Matrix_Items.SelectRow(SAP_UI_ItemEvent.Row, false, false);
                    return;
                }
            }
            catch
            {

            }
        }

        List<NSC_DI.SAP.Strain.CSV_Record_Item> StoredListOfItems = null;
        private void MAIN_MATRIX_KeyDown(ItemEvent SAP_UI_ItemEvent)
        {
            // Attempt to grab the selected row ID
            //int SelectedRowID = SAP_UI_ItemEvent.Row;

            //if (SelectedRowID > 0)
            //{
            //    // Find the matrix in the form UI
            //    Matrix MTX_ITEMS = _VirSci_Helper_Form.GetControlFromForm(
            //        ItemType: SAP_BusinessOne.Helpers.Forms.FormControlTypes.Matrix, ItemUID: "MTX_ITEMS", FormToSearchIn: _SBO_Form);

            //    double totalWeight = 0;
            //    List<VirSci_SAP.Controllers.Strain.CSV_Record_Item> ListOfItems = new List<Strain.CSV_Record_Item>();

            //    if (StoredListOfItems == null || StoredListOfItems.Count <= 0)
            //    {
            //        ListOfItems = ListOfAutoCreatedItems();
            //    }
            //    else
            //    {
            //        ListOfItems = StoredListOfItems;
            //    }

            //    // Foreach "bill of material" "item" created from the disassembly of "dry cannabis" 
            //    foreach (VirSci_SAP.Controllers.Strain.CSV_Record_BillOfMaterial bomItem in ListOfBillOfMaterialsForMainItemInProductionOrder(ListOfItems))
            //    {
            //        // Grab the Item Name for the BoM item
            //        string ItemName = ((VirSci_SAP.Controllers.Strain.CSV_Record_Item)ListOfItems.Where(t => t.ItemCode == bomItem.ItemCode).First()).ItemName;

            //        // Remove the "Strain Name" replacement from the CSV.
            //        ItemName = ItemName.Replace("[StrainName] - ", "");

            //        // Add an entry tying the column name to the user entered weight
            //        int columnIndex = MTX_ITEMS.GetColumnIndex(columnTitle: ItemName);
            //        double quantityOfColumn = 0;
            //        try
            //        {
            //            quantityOfColumn = Convert.ToDouble(((EditText)MTX_ITEMS.Columns.Item(columnIndex).Cells.Item(SelectedRowID).Specific).Value);
            //        }
            //        catch (Exception ex)
            //        {
            //            EditText columnToSelect = MTX_ITEMS.Columns.Item(columnIndex).Cells.Item(SelectedRowID) as EditText;
            //            columnToSelect.Active = true;

            //            // Send a message to the client
            //            _SBO_Application.StatusBar.SetText(
            //                Text: "Please enter a valid number for this column.",
            //                Seconds: BoMessageTime.bmt_Medium,
            //                Type: BoStatusBarMessageType.smt_Error);

            //            return;
            //        }

            //        totalWeight += quantityOfColumn;
            //    }

            //    EditText txtLineSum = _VirSci_Helper_Form.GetControlFromForm(
            //        ItemType: SAP_BusinessOne.Helpers.Forms.FormControlTypes.EditText, ItemUID: "txtLineSum", FormToSearchIn: _SBO_Form);

            //    txtLineSum.Value = totalWeight.ToString();
            //}
        }

        private static void MAIN_MATRIX_LINK_PRESSED(Form pForm, ItemEvent pItemEvent)
        {
            Form Form_ProductionOrder = null;
            try
            {
                // Attempt to grab the selected row ID
                int SelectedRowID = pItemEvent.Row;

                if (SelectedRowID > 0)
                {
                    // Which column stores the ID
                    int ColumnIDForIDOfItemSelected = 0;

                    // Get the ID of the note selected
                    string ItemSelected = pForm.Items.Item("MTX_ITEMS").Specific.Columns.Item(ColumnIDForIDOfItemSelected).Cells.Item(SelectedRowID).Specific.Value.ToString();

                    try
                    {
                        Form_ProductionOrder = Globals.oApp.Forms.GetForm("65211", 0);
                        Form_ProductionOrder.Select();
                    }
                    catch (Exception e)
                    {
                        // Open up the Production Order Master Data form
                        Globals.oApp.ActivateMenuItem("4369");

                        // Grab the "Production Order" form from the UI
                        Form_ProductionOrder = Globals.oApp.Forms.GetForm("65211", 0);
                    }

                    // Freeze the "Production Order" form
                    Form_ProductionOrder.Freeze(true);

                    // Change the "Production Order" form to find mode
                    Form_ProductionOrder.Mode = BoFormMode.fm_FIND_MODE;

                    // Insert the Production Order ID in the appropriate text field
                    ((EditText)Form_ProductionOrder.Items.Item("18").Specific).Value = ItemSelected;

                    // Click the "Find" button
                    ((Button)Form_ProductionOrder.Items.Item("1").Specific).Item.Click(BoCellClickType.ct_Regular);

                    // Un-Freeze the "Production Order" form
                    Form_ProductionOrder.Freeze(false);
                }
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(Form_ProductionOrder);
                GC.Collect();
            }
        }

        #endregion SUBS

    }
}
