﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using Matrix = NavSol.CommonUI.Matrix;

namespace NavSol.Forms
{
	class F_Quarantine : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "NSC_QUARANTINE";

		//DO NOT CHANGE.. Or stuff may break.
		private enum MatrixColumns { UniqueID, StateID, ItemName, Weight, UoM, WarehouseCode, WarehouseName, CreateDate, ItemCode, SysNumber, Timer }

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{

                case "1":
                    if(oForm.Mode == BoFormMode.fm_UPDATE_MODE) BubbleEvent = btnUpdate_Click(oForm);
                    break;

                case "TAB_1":
					// Load data about plants into the matrix
					Load_Matrx_Main(oForm, NSC_DI.Globals.QuarantineStates.SALE);
					break;

				case "TAB_2":
					// Load data about plants into the matrix
					Load_Matrx_Main(oForm, NSC_DI.Globals.QuarantineStates.SICK);
					break;

				case "TAB_3":
					// Load data about plants into the matrix
					Load_Matrx_Main(oForm, NSC_DI.Globals.QuarantineStates.DEST);
					break;

				case "TAB_4":
					// Load data about plants into the matrix
					Load_Matrx_Main(oForm, NSC_DI.Globals.QuarantineStates.DEST, true);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "BTN_LOOKUP":
					BTN_LOOKUP(oForm);
					break;

                case "MTX_PLANTS":
                    MatrixSelected(oForm);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "MTX_PLANTS":
					MTX_PLANTS_Link_Pressed(oForm, pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

        //--------------------------------------------------------------------------------------- et_COMBO_SELECT
        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
        public virtual void OnAfterComboSelect(ItemEvent pVal)
        {
            if (pVal.InnerEvent == true) return;
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            ComboSelected(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        private static void FormSetup(Form pForm)
		{
			try
			{
				pForm.Freeze(true);

				// Set the main image
				CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_Main", @"quarantine-icon.bmp");  //  gov-icon

                // set the Transfer Type Valid Values
                NavSol.CommonUI.ComboBox.AddVal(pForm, "cboTransTy", "Garden", "Transfer back to population");
                NavSol.CommonUI.ComboBox.AddVal(pForm, "cboTransTy", "Destroy", "Transfer to quarantine warehouse");
                NavSol.CommonUI.ComboBox.AddVal(pForm, "cboTransTy", "", "");
                pForm.Items.Item("cboTransTy").DisplayDesc = false;

                NavSol.CommonUI.ComboBox.ComboBoxAdd_UDT_CN(pForm, "cboDestroy", NSC_DI.Globals.tQuarantineType);
                NavSol.CommonUI.ComboBox.ComboBoxAdd_UDT_CN(pForm, "cboMixMat", NSC_DI.Globals.tMixMats);
                NavSol.CommonUI.ComboBox.ComboBoxAdd_UDT_CN(pForm, "cboWstMth", NSC_DI.Globals.tWasteMeth);

                pForm.Items.Item("TAB_2").Specific.Select();
                pForm.Items.Item("TAB_1").Visible = false;


                Load_TabCount(pForm);

				pForm.VisibleEx = true;

                pForm.Items.Item("LBL_ACTION").Visible = false;
                pForm.Items.Item("cboDestroy").Visible = false;
            }
            catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		private static void Load_Matrx_Main(Form pForm, NSC_DI.Globals.QuarantineStates QuarantineState, bool loadTimerColumn = false)
        {

            try
            {
                if (!int.TryParse(NSC_DI.UTIL.Options.Value.GetSingle("QuarantineTimer"), out int duration)) duration = 72;

                // Freeze the Form UI
                pForm.Freeze(true);

                string WarehouseType = "";
              
                //Button BTN_DEST = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "BTN_DEST", pForm);
                //BTN_DEST.Item.Visible = false;

                switch (QuarantineState)
                {
                    case NSC_DI.Globals.QuarantineStates.DEST:
                        WarehouseType = "QND";
                        if (!loadTimerColumn)
                        {
                            //BTN_DEST.Item.Visible = true;
                        }
                        break;

                    case NSC_DI.Globals.QuarantineStates.SALE:
                        WarehouseType = "QNS";
                        break;

                    case NSC_DI.Globals.QuarantineStates.SICK:
                        WarehouseType = "QNX";
                        break;
                }

                List<Matrix.MatrixColumn> matrixColumnCreation = new List<Matrix.MatrixColumn>()
            {
                new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.UniqueID.ToString("F"), Caption="Plant ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_LINKED_BUTTON  }
                ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.StateID.ToString("F"), Caption="State ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.ItemName.ToString("F"), Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.Weight.ToString("F"), Caption="Weight", ColumnWidth=80,ItemType=BoFormItemTypes.it_EDIT }
                ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.UoM.ToString(), Caption="UoM", ColumnWidth=80, ItemType=BoFormItemTypes.it_COMBO_BOX}
                ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.WarehouseCode.ToString("F"), Caption="Warehouse ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.WarehouseName.ToString("F"), Caption="Warehouse", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.CreateDate.ToString("F"), Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.ItemCode.ToString("F"), Caption="Item Code", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                ,new Matrix.MatrixColumn(){ ColumnName=MatrixColumns.SysNumber.ToString("F"), Caption="Sys Number", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
            };

                string additionalColumnToQuery = "";
                string additionalClauseToQuery = "";

                if (QuarantineState == NSC_DI.Globals.QuarantineStates.DEST)
                {
                    double destroyTimestamp = NSC_DI.UTIL.Dates.Quarantine.DestroyTimeStamp;


                    if (loadTimerColumn)
                    {
                        additionalColumnToQuery = ",[OSRN].[U_NSC_TimerQuar] AS [Timer]";
                        additionalClauseToQuery = string.Format(" AND CONVERT(int, ISNULL([OSRN].[U_NSC_TimerQuar], 0)) > CONVERT(int, ISNULL({0}, 0)) ", destroyTimestamp);
                        matrixColumnCreation.Add(new Matrix.MatrixColumn()
                        {
                            ColumnName = MatrixColumns.Timer.ToString("F"),
                            Caption = "Timer",
                            ColumnWidth = 80,
                            ItemType = BoFormItemTypes.it_EDIT
                        });
                    }
                    else
                    {
                        additionalClauseToQuery = string.Format(" AND CONVERT(int, ISNULL([OSRN].[U_NSC_TimerQuar], 0)) <= CONVERT(int, ISNULL({0}, 0)) ", destroyTimestamp);
                    }

                }
                // #10823 --->
                // limit the list to items in ths pecific branch
                var defBranch = Globals.BranchDflt;         //  -1 is no branches, 0 is no default branch
                var brList = Globals.BranchList;
                var destWH = pForm.Items.Item("cboWH").Specific.Value;
                if (destWH != "" && defBranch >= 0)
                {
                    defBranch = NSC_DI.SAP.Branch.Get(destWH);
                    brList = NSC_DI.SAP.Branch.GetAll_User_Str(defBranch);
                }
                // #10823 <---
                string sqlQuery = $@"
SELECT DISTINCT 
       OSRN.DistNumber AS [UniqueID], [OSRI].[WhsCode] AS [WarehouseCode], [OWHS].[WhsName] AS [WarehouseName], [OSRN].[CreateDate] AS [CreateDate], [OSRN].[itemName] AS [ItemName], 
       OSRN.[itemCode] AS [ItemCode], [OSRN].[SysNumber] AS [SysNumber], [OSRN].[MnfSerial] AS [StateID], 0.00 AS [Weight], SPACE(32) AS [UoM]
{additionalColumnToQuery}
  FROM [OSRN]
  JOIN [OSRI] ON [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]
  JOIN [OWHS] ON [OWHS].[WhsCode] = (SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber])
  JOIN [OITM] ON [OSRI].[ItemCode] = [OITM].[ItemCode] 
 WHERE  [OSRI].[Status] = 0 --[OITM].[U_NSC_IsMother] = 'N' AND
   AND [OWHS].[U_{Globals.SAP_PartnerCode}_WhrsType] = '{WarehouseType}' {additionalClauseToQuery}";
                if (defBranch >= 0) sqlQuery += $" AND OWHS.BPLid IN ({brList})";        // #10823
                //                string sqlQuery = $@"
                //SELECT Distinct
                //OSRN.DistNumber AS [UniqueID]
                //,(SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) AS [WarehouseCode]
                //,[OWHS].[WhsName] AS [WarehouseName]
                //,[OSRN].[CreateDate] AS [CreateDate]
                //,[OSRN].[itemName] AS [ItemName]
                //,[OSRN].[itemCode] AS [ItemCode]
                //,[OSRN].[SysNumber] AS [SysNumber]
                //,[OSRN].[U_{Globals.SAP_PartnerCode}_StateID] AS [StateID]
                //{additionalColumnToQuery}
                //FROM [OSRN]
                //JOIN [OWHS]
                //ON [OWHS].[WhsCode] = (SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber])
                //WHERE
                //[OWHS].[U_{Globals.SAP_PartnerCode}_WhrsType] = '{WarehouseType}'
                //AND (SELECT [OSRI].[Status] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) = 0
                //{additionalClauseToQuery}";

               

                Matrix.LoadDatabaseDataIntoMatrix(pForm, "OSRI", "MTX_PLANTS", matrixColumnCreation, sqlQuery);
                // Grab the matrix from the form UI
                SAPbouiCOM.Matrix MTX_PLANTS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLANTS", pForm) as SAPbouiCOM.Matrix;              
               
                // Allow multi-selection on the Matrix
                MTX_PLANTS.SelectionMode = BoMatrixSelect.ms_Auto;

                if (loadTimerColumn)
                {
                    // Get and Clear out our DataTable so we can populate with our new selected data.
                    DataTable RouteReadyMatrixDataTable = pForm.DataSources.DataTables.Item("OSRI");

                    for (int i = 1; i < (MTX_PLANTS.RowCount + 1); i++) //TODO FIX THE DAMN FOR LOOP
                    {
                        try
                        {
                            // Stored the Ticks of time so we can just parse the ticks right into DateTime.
                            var timeSpan = long.Parse(MTX_PLANTS.Columns.Item(MatrixColumns.Timer).Cells.Item(i).Specific.Value);
                            
                            DateTime timerStartTime = NSC_DI.UTIL.Dates.FromTimeStamp(timeSpan); // COMPLIANCE
                            TimeSpan span = timerStartTime - NSC_DI.UTIL.Dates.Quarantine.DestroyDate;
                            //string formatted = NSC_DI.UTIL.Dates.GetFormattedTimespan(timerStartTime, NSC_DI.UTIL.Dates.Quarantine.DestroyDate);  // #1150
                            string formatted = Math.Round(span.TotalHours, 1).ToString() + " hours";    // #1150
                            RouteReadyMatrixDataTable.SetValue("Timer", i - 1, formatted);
                        }
                        catch (Exception ex)
                        {
                            // Send a message to the client
                            Globals.oApp.StatusBar.SetText("Failed handle matrix load events!", BoMessageTime.bmt_Short);
                        }
                    }
                }

                // Reload from our datasource.
                MTX_PLANTS.LoadFromDataSource();

                Column weightCol = MTX_PLANTS.Columns.Item(MatrixColumns.Weight);
                Column UoMCol = MTX_PLANTS.Columns.Item(MatrixColumns.UoM);

                if (CurrentlySelectedTab(pForm) == 3)
                {
                    weightCol.Visible = true;
                    weightCol.Editable = true;
                    UoMCol.Visible = true;
                    UoMCol.Editable = true;
                    CommonUI.ComboBox.ComboBoxAdd_UFD1(pForm, "MTX_PLANTS", "OIGE", "NSC_WasteUoM", MatrixColumns.UoM);
                    CommonUI.ComboBox.SetVal(pForm, "MTX_PLANTS", "Grams", BoSearchKey.psk_ByValue, MatrixColumns.UoM);
                }
                else
                {
                    weightCol.Visible = false;
                    UoMCol.Visible = false;
                }


                MTX_PLANTS.AutoResizeColumns();

                pForm.Mode = BoFormMode.fm_OK_MODE;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            { 
                // Un-Freeze the Form UI
			    pForm.Freeze(false);
            }
        }

		private static void Load_TabCount(Form pForm)
        {
            try
            {
                if (!int.TryParse(NSC_DI.UTIL.Options.Value.GetSingle("QuarantineTimer"), out int duration)) duration = 72;

                double destroyTimestamp = NSC_DI.UTIL.Dates.Quarantine.DestroyTimeStamp;

                //                string sqlQuery = $@"SELECT
                //(SELECT COUNT(*) 
                //   FROM [OSRI] 
                //   JOIN [OITM] ON [OSRI].[ItemCode] = [OITM].[ItemCode] 
                //  WHERE [OITM].[U_NSC_IsMother] = 'N'  AND [OSRI].[Status] = 0 AND [OSRI].[U_NSC_QuarState] = 'SALE') AS SALE,
                //(SELECT COUNT(*)
                //   FROM [OSRI]
                //   JOIN [OITM] ON [OSRI].[ItemCode] = [OITM].[ItemCode]
                //  WHERE [OITM].[U_NSC_IsMother] = 'N'  AND [OSRI].[Status] = 0 AND [OSRI].[U_NSC_QuarState] = 'SICK') AS SICK,
                //(SELECT COUNT(*) 
                //   FROM [OSRI] 
                //   JOIN [OITM] ON [OSRI].[ItemCode] = [OITM].[ItemCode] 
                //  WHERE [OITM].[U_NSC_IsMother] = 'N' AND [OSRI].[Status] = 0 AND CONVERT(int, ISNULL([OSRI].[U_NSC_TimerQuar], 0)) <= CONVERT(int, ISNULL({destroyTimestamp}, 0))
                //    AND [OSRI].[U_NSC_QuarState] = 'DEST') AS DEST,
                //(SELECT COUNT(*)
                //   FROM [OSRI] 
                //   JOIN [OITM] ON [OSRI].[ItemCode] = [OITM].[ItemCode] 
                //  WHERE [OITM].[U_NSC_IsMother] = 'N' AND CONVERT(int, ISNULL([OSRI].[U_NSC_TimerQuar], 0)) > CONVERT(int, ISNULL({destroyTimestamp}, 0))
                //    AND [OSRI].[Status] = 0 AND [OSRI].[U_NSC_QuarState] = 'DEST') AS AWAIT";

                // #10823
                var branchWhere = "";
                if (Globals.BranchDflt >= 0) branchWhere += $" AND OWHS.BPLid IN ({Globals.BranchList})";        

                string sqlQuery = $@"SELECT
(SELECT COUNT(*) 
   FROM [OSRI] 
   JOIN [OITM] ON [OSRI].[ItemCode] = [OITM].[ItemCode] 
   JOIN [OWHS] ON [OWHS].[WhsCode]  = [OSRI].[WhsCode]
  WHERE  [OSRI].[Status] = 0 AND [OWHS].[U_{Globals.SAP_PartnerCode}_WhrsType] = 'QNS' {branchWhere}) AS SALE, --[OITM].[U_NSC_IsMother] = 'N'  AND
(SELECT COUNT(*)
   FROM [OSRI]
   JOIN [OITM] ON [OSRI].[ItemCode] = [OITM].[ItemCode]
   JOIN [OWHS] ON [OWHS].[WhsCode]  = [OSRI].[WhsCode]
  WHERE [OSRI].[Status] = 0 AND [OWHS].[U_{Globals.SAP_PartnerCode}_WhrsType] = 'QNX' {branchWhere}) AS SICK, --[OITM].[U_NSC_IsMother] = 'N'  AND 
(SELECT COUNT(*) 
   FROM [OSRI] 
   JOIN [OITM] ON [OSRI].[ItemCode] = [OITM].[ItemCode] 
   JOIN [OWHS] ON [OWHS].[WhsCode]  = [OSRI].[WhsCode]
  WHERE [OSRI].[Status] = 0 AND CONVERT(int, ISNULL([OSRI].[U_NSC_TimerQuar], 0)) <= CONVERT(int, ISNULL({destroyTimestamp}, 0)) --[OITM].[U_NSC_IsMother] = 'N' AND 
    AND [OWHS].[U_{Globals.SAP_PartnerCode}_WhrsType] = 'QND' {branchWhere}) AS DEST,
(SELECT COUNT(*)
   FROM [OSRI] 
   JOIN [OWHS] ON [OWHS].[WhsCode]  = [OSRI].[WhsCode]
   JOIN [OITM] ON [OSRI].[ItemCode] = [OITM].[ItemCode] 
  WHERE CONVERT(int, ISNULL([OSRI].[U_NSC_TimerQuar], 0)) > CONVERT(int, ISNULL({destroyTimestamp}, 0)) --[OITM].[U_NSC_IsMother] = 'N' AND 
    AND [OSRI].[Status] = 0 AND [OWHS].[U_{Globals.SAP_PartnerCode}_WhrsType] = 'QND' {branchWhere}) AS AWAIT";

                var dt = NSC_DI.UTIL.SQL.DataTable(sqlQuery);
                var dr = dt.Rows[0];

                (pForm.Items.Item("TAB_1").Specific as SAPbouiCOM.Folder).Caption = "Sales (" + dr["SALE"] + ")";
                (pForm.Items.Item("TAB_2").Specific as SAPbouiCOM.Folder).Caption = "Sick (" + dr["SICK"] + ")";
                (pForm.Items.Item("TAB_3").Specific as SAPbouiCOM.Folder).Caption = "Destroy (" +dr["DEST"] + ")";
                (pForm.Items.Item("TAB_4").Specific as SAPbouiCOM.Folder).Caption = "Awaiting Timer (" + dr["AWAIT"] + ")";
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
            }
        }

        private static int CurrentlySelectedTab(Form pForm)
        {
            try
            {
                for (int i = 1; i < 7; i++)
                {
                    if (((Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_" + i, pForm)).Selected)
                    {
                        return i;
                    }
                }

                return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
            }
        }

        private List<string> SelectedPlantIDs
        {
            get
            {
                // Keep track of the selected Plant ID's
                List<string> SelectedPlantIDs = new List<string>();

                // Grab the matrix from the form UI
	            SAPbouiCOM.Matrix MTX_PLANTS = null;
				// RHH - will have to fix if this list is ever used
				//MTX_PLANTS = Helpers.Forms.GetControlFromForm(Helpers.Forms.FormControlTypes.Matrix, "MTX_PLANTS", pForm) as Matrix;

                // For each row already selected
                for (int i = 1; i < (MTX_PLANTS.RowCount + 1); i++)
                {
                    if (MTX_PLANTS.IsRowSelected(i))
                    {
                        // Grab the selected row's Plant ID column
                        string plantID = ((EditText)MTX_PLANTS.Columns.Item(0).Cells.Item(i).Specific).Value;
                        // Add the selected row plant ID to list
                        SelectedPlantIDs.Add(plantID);
                    }
                }

                return SelectedPlantIDs;
            }
            set
            {
                return;
            }
        }

		private static bool btnUpdate_Click(Form pForm)
		{
            try
            {
                if (CurrentlySelectedTab(pForm) == 2) Update_SickTab(pForm);
                if (CurrentlySelectedTab(pForm) == 3) Update_DestroyTab(pForm);
                return true;
            }
            catch (System.ComponentModel.WarningException)
            {
                return false;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return false;
            }
            finally
            {                
                //NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
            }
        }

        private static void MatrixSelected(Form pForm)
        {
            // if a row has been selected and the current tab is "Destroy", change the form mode to update
            try
            {
                if (CurrentlySelectedTab(pForm) != 3) return;
                if (pForm.Items.Item("MTX_PLANTS").Specific.GetNextSelectedRow(0, BoOrderType.ot_RowOrder) > 0)
                    pForm.Mode = BoFormMode.fm_UPDATE_MODE;
                else
                    pForm.Mode = BoFormMode.fm_OK_MODE;

                pForm.Items.Item("1").Specific.Caption = "Destroy";

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
            }
        }

        private static void Update_SickTab(Form pForm)
        {
            SAPbobsCOM.StockTransfer oDoc = null;
            SAPbouiCOM.Matrix        oMat = null;

            try
            {
                oMat = pForm.Items.Item("MTX_PLANTS").Specific;
                var row = oMat.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);
                if(row < 1)
                {
                    Globals.oApp.SetStatusBarMessage("No Plants were selected.", BoMessageTime.bmt_Medium, true);
                    return;
                }

                var toWH = CommonUI.Forms.GetField<string>(pForm, "cboWH");

                oDoc = Globals.oCompany.GetBusinessObject(BoObjectTypes.oStockTransfer) as StockTransfer;
                oDoc.DocDate  = DateTime.Now;
                oDoc.TaxDate  = DateTime.Now;                
                oDoc.Comments = "Return Quarantined Plants to Population";
                oDoc.UserFields.Fields.Item("U_NSC_InvXFerType").Value      = "0";   // set to "Other"
                oDoc.UserFields.Fields.Item("U_NSC_InvXFerResn").Value      = oDoc.Comments;
                oDoc.UserFields.Fields.Item("U_NSC_StateSyncStatus").Value  = "R";
                var fromWH = "";
                while (row > 0)
                {
                    if (oDoc.Lines.ItemCode != "") oDoc.Lines.Add();

                    oDoc.Lines.ItemCode                         = oMat.GetCellSpecific(MatrixColumns.ItemCode, row).Value;
                    oDoc.Lines.FromWarehouseCode                = oMat.GetCellSpecific(MatrixColumns.WarehouseCode, row).Value;
                    oDoc.Lines.WarehouseCode                    = toWH;
                    oDoc.Lines.Quantity = 1;
                   
                    oDoc.Lines.SerialNumbers.SystemSerialNumber = Convert.ToInt32(oMat.GetCellSpecific(MatrixColumns.SysNumber, row).Value);
                    fromWH = oDoc.Lines.FromWarehouseCode;
                    row = oMat.GetNextSelectedRow(row, BoOrderType.ot_RowOrder);
                }

                oDoc.FromWarehouse = fromWH;
                oDoc.ToWarehouse = toWH;
                NSC_DI.SAP.Document.Add(oDoc);
                Load_TabCount(pForm);
                Load_Matrx_Main(pForm, NSC_DI.Globals.QuarantineStates.SICK);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDoc);
                NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }

        private static void Update_DestroyTab(Form pForm)
        {
            try
            {
                if (Globals.oApp.MessageBox("Are you sure you want to destroy the selected?", 1, "Yes", "No") != 1) return;

                // Grab the matrix from the form UI
                SAPbouiCOM.Matrix MTX_PLANTS = pForm.Items.Item("MTX_PLANTS").Specific;

                // Prepare a list of items to issue in the "Goods Issued" document.
                List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines> ListOfGoodsIssuedItems = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();

                List<string> Barcodes = new List<string>();
                // For each row already selected
                for (int i = 1; i < (MTX_PLANTS.RowCount + 1); i++)
                {
                    // If the row was selected
                    if (MTX_PLANTS.IsRowSelected(i))
                    {
                        //var weight = ((EditText)MTX_PLANTS.Columns.Item(MatrixColumns.Weight).Cells.Item(i).Specific).Value;
                        //if (string.IsNullOrEmpty(weight?.Trim()))
                        //{
                        //    Globals.oApp.SetStatusBarMessage("You must have a weight entered", BoMessageTime.bmt_Medium, true);
                        //    return;
                        //}
                        string stateID = ((EditText)MTX_PLANTS.Columns.Item(MatrixColumns.StateID).Cells.Item(i).Specific).Value;
                        Barcodes.Add(stateID);

                        // Grab the serial ID, item code, and warehouse code from the matrix of the selected item.
                        int SerialNumber = Convert.ToInt32(((EditText)MTX_PLANTS.Columns.Item(MatrixColumns.SysNumber).Cells.Item(i).Specific).Value.ToString());
                        string DistNumber = ((EditText)MTX_PLANTS.Columns.Item(MatrixColumns.UniqueID).Cells.Item(i).Specific).Value.ToString();
                        string ItemCode = ((EditText)MTX_PLANTS.Columns.Item(MatrixColumns.ItemCode).Cells.Item(i).Specific).Value;
                        string WarehouseCode = ((EditText)MTX_PLANTS.Columns.Item(MatrixColumns.WarehouseCode).Cells.Item(i).Specific).Value;
                        string wasteWght = ((EditText)MTX_PLANTS.Columns.Item(MatrixColumns.Weight).Cells.Item(i).Specific).Value; //13144
                        string wasteUoM = ((ComboBox)MTX_PLANTS.Columns.Item(MatrixColumns.UoM).Cells.Item(i).Specific).Value;//13144 grabs new vals in the matrix on the destroy tab

                        string mixMatCode = CommonUI.Forms.GetField<string>(pForm, "cboMixMat");
                        string wasteMethCode = CommonUI.Forms.GetField<string>(pForm, "cboWstMth");
                        string mixMatName = "";
                        string wasteMethName = "";

                        // 12903 creates a new udf dictionary and adds to it if there are values in the combo boxes on the destroy tab--------------------
                        Dictionary<string, string> UserDefinedFields = new Dictionary<string, string>();
                       
                        if (double.Parse(wasteWght) <= 0.00)//13144 checks to see if the user entered in a val in Waste Weight
                        {                            
                            Globals.oApp.SetStatusBarMessage("Please enter a valid weight for the selected plants. E.G. higher than 0.00", BoMessageTime.bmt_Medium, true);
                            throw new System.ComponentModel.WarningException();
                        }

                        if (!string.IsNullOrEmpty(mixMatCode?.Trim()))
                        {
                            mixMatName = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Name FROM [@{NSC_DI.Globals.tMixMats}] WHERE Code = '{mixMatCode}'");
                            UserDefinedFields.Add("U_NSC_MixMats", mixMatName);
                        }
                        if (!string.IsNullOrEmpty(wasteMethCode?.Trim()))
                        {
                            wasteMethName = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Name FROM [@{NSC_DI.Globals.tWasteMeth}] WHERE Code = '{wasteMethCode}'");
                            UserDefinedFields.Add("U_NSC_InvXFerType", wasteMethName);
                        }
                        UserDefinedFields.Add("U_NSC_WasteUoM", wasteUoM);
                        //UserDefinedFields.Add("U_NSC_WasteWgt", wasteWght);//13144 adds the new waste weight and UOM to the UDFs for the goods issue

                        // Add a new item to our Goods Issued document line items
                        ListOfGoodsIssuedItems.Add(new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
                        {
                            Quantity = 1,                           
                            SysSerialNumbers = new List<int>() { SerialNumber },
                            ItemCode = ItemCode,
                            WarehouseCode = WarehouseCode,
                            WasteWeight = double.Parse(wasteWght),
                            DistNum = DistNumber,
                            //UoMPer = double.Parse(weight),
                            UserDefinedFields = UserDefinedFields //12903, added it this way so that we can add to it easier if more udf's are req'd
                            
                        });
                    }
                }
                if (ListOfGoodsIssuedItems.Count == 0)
                {
                    Globals.oApp.StatusBar.SetText("No items selected");
                    throw new System.ComponentModel.WarningException();
                }
                // Attempt to create the "Goods Issued" document.
                NSC_DI.SAP.GoodsIssued.Create(DateTime.Now, ListOfGoodsIssuedItems);
                Load_TabCount(pForm);

                // Reload the matrix with the current quarantine for destruction plants.
                Load_Matrx_Main(pForm, NSC_DI.Globals.QuarantineStates.DEST);
            }
            catch (System.ComponentModel.WarningException)
            {
                throw new System.ComponentModel.WarningException();
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
            }
        }

        /*		// COMPLIANCE
                // Prepare a connection to the state API
                    var TraceCon = new NSC_DI.SAP.TraceabilityAPI(SAPBusinessOne_Application: Globals.oApp,
                        SAPBusinessOne_Company: Globals.oCompany,
                        SAPBusinessOne_Form: _SBO_Form);

                    BioTrack.API btAPI = TraceCon.new_API_obj();

                    BioTrack.Plant.Destroy(ref btAPI, BarcodeIDs: Barcodes.ToArray());

                    string compID = VirSci_Controllers_Compliance.CreateComplianceLineItem(
                        Reason: Models.Compliance.ComplianceReasons.Plant_Destroy,
                        API_Call: btAPI.XmlApiRequest.ToString()
                        );

                    // API: Destroy a plant.
                    btAPI.PostToApi();

                    if (btAPI.WasSuccesful)
                    {
                        VirSci_Controllers_Compliance.UpdateComliancy(
                            ComplianceID: compID,
                            ResponseXML: btAPI.xDocFromResponse.ToString(),
                            Status: Controllers.Compliance.ComplianceStatus.Success);
                    }
                    else
                    {
                        VirSci_Controllers_Compliance.UpdateComliancy(
                            ComplianceID: compID,
                            ResponseXML: btAPI.xDocFromResponse.ToString(),
                            Status: Controllers.Compliance.ComplianceStatus.Failed);
                    }
                }
         */

        private static void BTN_LOOKUP(Form pForm)
		{
			SAPbouiCOM.Matrix MTX_PLANTS = null;
			EditText TXT_LOOKUP = null;

			try
			{
				// Freeze the form UI
				pForm.Freeze(true);

				// Grab the matrix from the form UI
				MTX_PLANTS = pForm.Items.Item("MTX_PLANTS").Specific;

				// Find the textbox for scanner results in the form
				TXT_LOOKUP = pForm.Items.Item("TXT_LOOKUP").Specific;

				// For each row in the matrix
				for (var i = 1; i < (MTX_PLANTS.RowCount + 1); i++)
				{
					// Grab the selected row's Plant ID column
					string plantID = ((EditText)MTX_PLANTS.Columns.Item(0).Cells.Item(i).Specific).Value;

					if (TXT_LOOKUP.Value == plantID)
					{
						MTX_PLANTS.SelectRow(i, true, true);
						break;
					}
				}

				TXT_LOOKUP.Value = "";

				TXT_LOOKUP.Active = true;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
				NSC_DI.UTIL.Misc.KillObject(MTX_PLANTS);
				NSC_DI.UTIL.Misc.KillObject(TXT_LOOKUP);
				GC.Collect();
			}
		}

		private void MTX_PLANTS_Link_Pressed(Form pForm, ItemEvent pVal)
		{
			try
			{
				int SelectedRowID = pVal.Row;

				if (SelectedRowID > 0)
				{
					// Get the ID of the note selected
					string plantID = CommonUI.Forms.GetField<string>(pForm, "MTX_PLANTS", SelectedRowID, MatrixColumns.UniqueID);
                    string itemCode = CommonUI.Forms.GetField<string>(pForm, "MTX_PLANTS", SelectedRowID, MatrixColumns.ItemCode);

                    // Open the plant manager for the selected plant
                    NavSol.Forms.F_PlantDetails.FormCreate(plantID);//, pItemCode: itemCode
                }

			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
		}

        private static void ComboSelected(Form pForm, ItemEvent pVal)
        {
            var sql = "";

            try
            {
                if(pVal.ItemUID == "cboTransTy")
                {
                    var visible = pForm.Items.Item("cboTransTy").Specific.Value == "Destroy";
                    pForm.Items.Item("LBL_ACTION").Visible = visible;
                    pForm.Items.Item("cboDestroy").Visible = visible;

                    // clear the WH valid values
                    NavSol.CommonUI.ComboBox.DeleteVals(pForm, "cboWH");

                    string whSQLStrg = $"U_{Globals.SAP_PartnerCode}_WhrsType = ";
                    sql = $"SELECT WhsCode, WhsName FROM OWHS WHERE {whSQLStrg}";

                    if (pForm.Items.Item("cboTransTy").Specific.Value == "") return;
                    if (pForm.Items.Item("cboTransTy").Specific.Value == "Garden")  sql += $"'WOR' OR {whSQLStrg}'VEG' OR {whSQLStrg}'FLO'";
                    if (pForm.Items.Item("cboTransTy").Specific.Value == "Destroy") sql += "'QND'";
                    if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})";  // #10823

                    NavSol.CommonUI.ComboBox.LoadComboBoxSQL(pForm, "cboWH", sql);
                    pForm.Items.Item("cboWH").Specific.Select(1, BoSearchKey.psk_Index);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}