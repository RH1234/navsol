﻿using System;
using SAPbobsCOM;
using SAPbouiCOM;
using B1WizardBase;

namespace NavSol.Forms.Sales_and_Deliveries
{
	class F_133 : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "133"; // Delivery
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_FORM_LOAD
		[B1Listener(BoEventTypes.et_FORM_LOAD, true, new string[] { cFormID })]
		public virtual bool OnBeforeFormLoad(ItemEvent pVal)
		{
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            FormSetup(oForm);   // #3601 - put here because form is accessed via drill down.            

            if (oForm != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oForm);
			GC.Collect();

			return true;
		}

		//--------------------------------------------------------------------------------------- et_VALIDATE
		[B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID })]
		public virtual bool OnBeforeValidate(ItemEvent pVal)
		{
			var BubbleEvent = true;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
           
			if (pVal.FormMode == (int)BoFormMode.fm_OK_MODE && (pVal.ItemUID == "4" || pVal.ItemUID == "54")) BubbleEvent = ValidateBP(oForm);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_FORM_LOAD
		[B1Listener(BoEventTypes.et_FORM_LOAD, false, new string[] { cFormID })]
		public virtual void OnAfterFormLoad(ItemEvent pVal)
		{
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            
            NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_FORM_DATA_LOAD
		[B1Listener(BoEventTypes.et_FORM_DATA_LOAD, false, new string[] { cFormID })]
		public virtual void OnAfterFormDataLoad(BusinessObjectInfo pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            //EnableBtnComplete(oForm);

            if (oForm != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oForm);
			GC.Collect();
		}

        //--------------------------------------------------------------------------------------- et_FORM_DATA_ADD
        [B1Listener(BoEventTypes.et_FORM_DATA_ADD, false, new string[] { cFormID })]
        public virtual void OnAfterFormDataAdd(BusinessObjectInfo pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            //Compliance(oForm, pVal.ObjectKey);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            
            //if(pVal.ItemUID == "btnCompl") Complete(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------
		private static void FormSetup(Form pForm)
		{
            return;
			// add any additional fields, datasources, ect
			SAPbouiCOM.Item oItm = null;
           
            try
			{
                ////----------------------------    REMOVED IT - DOES NOT MAKE SENSE TO HAVE IT HERE
                //// add Complete Button
                //oItm = pForm.Items.Add("btnCompl", BoFormItemTypes.it_BUTTON);
                //oItm.Width		= pForm.Items.Item("2").Width;
                //oItm.Top		= pForm.Items.Item("2").Top;
                //oItm.Left		= pForm.Items.Item("2").Left + pForm.Items.Item("2").Width + 5;
                //oItm.Specific.Caption = "Complete";
            }
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		private static void EnableBtnComplete(Form pForm)
		{
			try
			{
				var status = CommonUI.Forms.Status(pForm);
				pForm.Items.Item("btnCompl").Enabled = (status != "Closed" && status != "Cancelled");
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private static bool ValidateBP(Form pForm)
		{
            bool BPValid = false;
			try
			{
                var status = CommonUI.Forms.Status(pForm);

                if (NSC_DI.UTIL.Settings.Value.Get("IsCannabis") != "Y")
                    BPValid = true;
				
				if(status != "Closed" && status != "Cancelled")
                    BPValid = true;

				// if there are no items, then ok
				if (pForm.Items.Item("38").Specific.RowCount == 0)
				{
					//CommonUI.Beep.Send();
                    //Globals.oApp.SetStatusBarMessage("The Customer must have a State ID.");
                    BPValid = true;
				}
				if (pForm.Items.Item("38").Specific.RowCount == 1 && CommonUI.Forms.GetField<string>(pForm, "38", 1, "1") == "")
				{
					//CommonUI.Beep.Send(); ...
                    //Globals.oApp.SetStatusBarMessage("The Customer must have a State ID.");
                    BPValid = true;
				}
                return BPValid;

                // get the BP Code
                //var bp = CommonUI.Forms.GetField<string>(pForm, "4");
				//if (bp.Trim() == "") return true;

				// make sure the there is a State ID
				//if (NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_NSC_StateUBI FROM OCRD WHERE CardCode = '{bp}'") != "") return true;

				//CommonUI.Beep.Send();
				//Globals.oApp.SetStatusBarMessage("The Customer must have a State ID.");
				
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return BPValid;
			}
			finally
			{
				GC.Collect();
			}
		}

		private static void Complete(Form pForm)
		{
			//var errs = "";
			//SAPbobsCOM.Documents oDoc = null;

			//try
			//{
			//	if(pForm.Mode != BoFormMode.fm_OK_MODE)
			//	{
			//		CommonUI.Beep.Send();
			//		Globals.oApp.SetStatusBarMessage("Save changes first.");
			//		return;
			//	}

			//	if (ValidateBP(pForm) == false) return;

			//	var docNum = CommonUI.Forms.GetField<string>(pForm, "8");
			//	var docEnt = NSC_DI.SAP.Document.GetDocEntry<int>("ORDR", docNum);

			//	NSC_DI.SAP.Document.GetByKey(out oDoc, BoObjectTypes.oOrders, docEnt);

			//	var dt = NSC_DI.SAP.BatchItems.SubBatch.DT_Load(oDoc, out errs);

			//	// if any errors, display message and return
			//	if (errs != "")
			//	{
			//		Globals.oApp.MessageBox(errs);
			//		return;
			//	}

   //             //-------------------------------
   //             if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();
   //             NSC_DI.SAP.BatchItems.SubBatch.DeAllocate(oDoc, dt);                    // replace the batches with nothing
   //             NSC_DI.SAP.BatchItems.SubBatch.Create(dt);                              // create sub-batches
			//	NSC_DI.SAP.BatchItems.SubBatch.Allocate(oDoc, dt);                      // replace the batches with the sub batches in the SO (do it in the SO so that the line numbers match up)
			//	oDoc = NSC_DI.SAP.Document.Copy(oDoc, BoObjectTypes.oDeliveryNotes);    // create Delivery
			//	NSC_DI.SAP.Document.Add(oDoc);                                          // add Delivery
   //             if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

			//	Globals.oApp.ActivateMenuItem(NavSol.Id.MenuBar.Refresh);
			//	Globals.oApp.StatusBar.SetText("Process successful.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
			//}
			//catch (Exception ex)
			//{
			//	if(Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
			//	Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			//}
			//finally
			//{
			//	GC.Collect();
			//}
		}

        private static void Compliance(Form pForm, string pObjectKey)
        {
            try
            {
                var key = NSC_DI.SAP.B1Object.GetKey(pObjectKey);
                //NSC_DI.API.Compliance.Sales.Post(key, SAPbobsCOM.BoObjectTypes.oInvoices);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oDoc);
                GC.Collect();
            }
        }       
    }
}