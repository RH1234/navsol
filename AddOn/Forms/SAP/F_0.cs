﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms.SAP
{
    class F_0 : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "0"; // modal dialog
        #endregion VARS
        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPRessed(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            FormSetup(oForm);

            if (pVal.ItemUID == "1") // Yes Clicked
            {
                YES_CLICKED(oForm);
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return true;
        }
        #endregion BEFORE EVENT

        //#region ---------------------------------- AFTER EVENT --------------------------------------------------------
        ////--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        //[B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        //public virtual void OnAfterItemPRessed(ItemEvent pVal)
        //{
        //    Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
        //    FormSetup(oForm);

        //    if (pVal.ItemUID == "1") // Yes Clicked
        //    {
        //        YES_CLICKED(oForm);
        //    }

        //    NSC_DI.UTIL.Misc.KillObject(oForm);
        //    GC.Collect();
        //}
        //#endregion AFTER EVENT

        private static void FormSetup(Form pForm)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void YES_CLICKED(Form pForm)
        {
            try
            {
                //var firstLineVal = CommonUI.Forms.GetField<string>(pForm, "7");
                
                //if (firstLineVal == "The database structure has been modified. In order to resume the process, all open" && !Globals.AddonInitialized)
                //{
                //    try
                //    {
                //        if (!IsAddonInitializing()) AddOn.InitializeComponents();
                //    }
                //    catch (Exception ex)
                //    {
                //        System.Windows.Forms.MessageBox.Show("Error in loading. Closing Add-On." + Environment.NewLine + ex.Message);
                //        System.Windows.Forms.Application.Exit();
                //    }
                //}
            }
            catch (Exception ex)
            {
                B1Connections.theAppl.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static bool IsAddonInitializing()
        {
            bool formInitializing = false;
            ProgressBar pbTest = null;

            try
            {
                pbTest = B1Connections.theAppl.StatusBar.CreateProgressBar("test", 1, true);
            }
            catch
            {
                formInitializing = true;
            }
            finally
            {
                if (pbTest != null) pbTest.Stop();
                NSC_DI.UTIL.Misc.KillObject(pbTest);
                GC.Collect();
            }

            return formInitializing;
        }
    }
}
