﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms.SAP
{

    class F_1320000702 : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "1320000702"; // inventory transfer
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_FORM_LOAD
        [B1Listener(BoEventTypes.et_FORM_LOAD, true, new string[] { cFormID })]
        public virtual bool OnBeforeFormLoad(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            if (oForm != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oForm);
            GC.Collect();

            return true;
        }

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeValidate(ItemEvent pVal)
        {
            var BubbleEvent = true;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPRessed(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            FormSetup(oForm);

            bool bubbleEvent = true;

            if (pVal.ItemUID == "1320000004")
            {
                Set_Default_Clicked(oForm);
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return bubbleEvent;
        }

        [B1Listener(BoEventTypes.et_DOUBLE_CLICK, true, new string[] { cFormID })]
        public virtual bool OnBeforeDoubleClick(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            FormSetup(oForm);

            bool bubbleEvent = true;

            if (pVal.ItemUID == "1320000003")
            {
                Set_Default_Clicked(oForm);
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return bubbleEvent;
        }
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_FORM_LOAD
        [B1Listener(BoEventTypes.et_FORM_LOAD, false, new string[] { cFormID })]
        public virtual void OnAfterFormLoad(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            FormSetup(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

       
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------
        private static void FormSetup(Form pForm)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static bool Set_Default_Clicked(Form pForm)
        {
            Matrix oMatrix = null;
            bool bubbleEvent = true;
            try
            {
                oMatrix = (SAPbouiCOM.Matrix)pForm.Items.Item("1320000003").Specific;
                var nextRow = oMatrix.GetNextSelectedRow();
                Globals.BranchDflt = int.Parse(oMatrix.GetCellSpecific(0, nextRow).Value);
                Globals.BranchList = NSC_DI.SAP.Branch.GetAll_User_Str(Globals.BranchDflt);               
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMatrix);
                GC.Collect();
            }
            return bubbleEvent;
        }
    }
}
