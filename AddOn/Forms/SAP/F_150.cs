﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms.SAP
{
    class F_150 : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "150"; // Item Master Data
        private const string cFormID_UDF = "-150"; // Production Order UDFs
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPRessed(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            FormSetup(oForm);

            bool bubbleEvent = true;

            if (pVal.ItemUID == "1" && pVal.FormMode == (int)BoFormMode.fm_ADD_MODE) // Add Clicked
            {
                ADD_CLICKED(oForm);
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return bubbleEvent;
        }

        //--------------------------------------------------------------------------------------- et_FORM_DATA_UPDATE
        [B1Listener(BoEventTypes.et_FORM_DATA_UPDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeFormDataUpdate(BusinessObjectInfo pVal)
        {
            bool bubbleEvent = true;

            //Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            //NSC_DI.UTIL.Misc.KillObject(oForm);
            //GC.Collect();

            return bubbleEvent;
        }
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_FORM_DATA_UPDATE
        [B1Listener(BoEventTypes.et_FORM_DATA_UPDATE, false, new string[] { cFormID })]
        public virtual void OnAfterFormDataUpdate(BusinessObjectInfo pVal)
        {
            //if (pVal.ActionSuccess == false) return;
            //Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            //NSC_DI.UTIL.Misc.KillObject(oForm);
            //GC.Collect();
        }

        // ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_FORM_DATA_LOAD
        [B1Listener(BoEventTypes.et_FORM_DATA_LOAD, false, new string[] { cFormID })]
        public virtual void OnAfterFormDataLoad(BusinessObjectInfo pVal)
        {
            //if (pVal.ActionSuccess == false) return;
            //Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            //if (oForm != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oForm);
            //GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_COMBO_SELECT *** UDF ***
        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID_UDF })]
        public virtual void OnAfterComboSelectUDF(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            if (pVal.InnerEvent == true) return;

            var oFormUDF = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            SetFieldsUDF(oFormUDF, pVal);

            NSC_DI.UTIL.Misc.KillObject(oFormUDF);
            GC.Collect();
        }
        #endregion AFTER EVENT

        private static void FormSetup(Form pForm)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static bool ADD_CLICKED(Form pForm)
        {
            bool bubbleEvent = true;
            try
            {
                //  disable adding template items 

                // var itemCode = CommonUI.Forms.GetField<string>(pForm, "5");

                //var qry = $"SELECT COUNT(*) FROM [@{NSC_DI.Globals.tAutoItem} WHERE U_ItemCode = '{itemCode}'";

                //if (itemCode.StartsWith("~") && NSC_DI.UTIL.SQL.GetValue<int>(qry) == 0)
                //{

                //    bubbleEvent = false;
                //}

            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            return bubbleEvent;
        }

        private static void SetFieldsUDF(Form pForm, ItemEvent pVal)
        {
            // set field values and / or access
            try
            {
                //var itemCode = pForm.Items.Item("5").Specific.Value;

                //--------------
                // IsMother HAS BEEN REMOVED

                //if (pVal.ItemChanged && pVal.ItemUID == "U_NSC_IsMother")
                //{
                //    //var dbVal = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_NSC_IsMother FROM OITM WHERE ItemCode = '{itemCode}'", null);
                //    pForm.Items.Item("U_NSC_MotherStatusChg").Specific.Select("Y", SAPbouiCOM.BoSearchKey.psk_ByValue);
                //}            
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
