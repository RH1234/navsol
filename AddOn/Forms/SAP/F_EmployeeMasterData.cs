﻿using System;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms.SAP
{
	class EmployeeMasterData : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "60100";

		private Tuple<string, string, string, string, string> employee_info;
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			var BubbleEvent = true;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			// The main "Add", "OK", "Find" button was clicked.
			if (pVal.ItemUID == "2" && oForm.Mode == BoFormMode.fm_ADD_MODE) BubbleEvent = btn_Add_Before(oForm);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			if (pVal.ItemUID == "2" && oForm.Mode == BoFormMode.fm_ADD_MODE) btn_Add_After(oForm);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private bool btn_Add_Before(Form pForm)
		{
			try
			{
				employee_info = new Tuple<string, string, string, string, string>(
					pForm.Items.Item("38").Specific.Value,
					pForm.Items.Item("37").Specific.Value,
					pForm.Items.Item("84").Specific.Value,
					pForm.Items.Item("113").Specific.Value,
					pForm.Items.Item("33").Specific.Value);

				if (String.IsNullOrEmpty(employee_info.Item1)
				    || String.IsNullOrEmpty(employee_info.Item2)
				    || String.IsNullOrEmpty(employee_info.Item3)
				    || String.IsNullOrEmpty(employee_info.Item4))
				{
					Globals.oApp.StatusBar.SetText("Please fill out first and last name, date of birth and hire date!");
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return false;
			}
			finally
			{
				GC.Collect();
			}
		}

		private void btn_Add_After(Form pForm)
		{
			try
			{
				string NewlyCreatedEmployeeID;
				Globals.oCompany.GetNewObjectCode(out NewlyCreatedEmployeeID);

				// Grab the data from the newly created employee since it has been wiped away from the form UI.                                                                             

				string employee_name = employee_info.Item1 + " " + employee_info.Item2;

				string employee_id = employee_info.Item5;

				string birth_month = employee_info.Item4.Substring(startIndex: 4, length: 2);
				string birth_day = employee_info.Item4.Substring(startIndex: 6, length: 2);
				string birth_year = employee_info.Item4.Substring(startIndex: 0, length: 4);

				string hire_month = employee_info.Item3.Substring(startIndex: 4, length: 2);
				string hire_day = employee_info.Item3.Substring(startIndex: 6, length: 2);
				string hire_year = employee_info.Item3.Substring(startIndex: 0, length: 4);

 /*	// COMPLIANCE
				// Prepare a connection to the state API
				var TraceCon = new VirSci_SAP.Controllers.TraceabilityAPI(SAPBusinessOne_Application: _SBO_Application,
					SAPBusinessOne_Company: _SBO_Company,
					SAPBusinessOne_Form: _SBO_Form);

				BioTrack.API btAPI = TraceCon.new_API_obj();

				// Run the "employee_new" function.
				BioTrack.Employee.Add(
					BioTrackAPI: ref btAPI
					, Employee_Name: employee_name
					, Employee_ID: employee_id
					, BirthDay: Convert.ToDateTime(birth_month + "/" + birth_day + "/" + birth_year)
					, HireDay: Convert.ToDateTime(hire_month + "/" + hire_day + "/" + hire_year)
				);

				// Prepare a new controller for "Compliance"
				VirSci_SAP.Controllers.Compliance controllerCompliance = new Controllers.Compliance(SAPBusinessOne_Application: _SBO_Application, SAPBusinessOne_Company: _SBO_Company);

				// Create a new line item for compliance
				string compID = controllerCompliance.CreateComplianceLineItem(
					Reason: Models.Compliance.ComplianceReasons.Employee_New
					, API_Call: btAPI.XmlApiRequest.ToString()
				);

				// API: Create a new employee
				btAPI.PostToApi();

				if (btAPI.WasSuccesful)
				{
					//codes are "0" not run, "1" successful, "2" error
					controllerCompliance.UpdateComliancy(ComplianceID: compID,
						ResponseXML: btAPI.xDocFromResponse.ToString(),
						Status: Controllers.Compliance.ComplianceStatus.Success);
				}
				else
				{
					//codes are "0" not run, "1" successful, "2" error
					controllerCompliance.UpdateComliancy(
						ComplianceID: compID,
						ResponseXML: btAPI.xDocFromResponse.ToString(),
						Status: Controllers.Compliance.ComplianceStatus.Failed);
				}
*/
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}
	}
}
