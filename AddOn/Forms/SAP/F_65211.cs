﻿using System;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms.SAP
{

    class F_65211 : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID     = "65211";  // Production Order
        private const string cFormID_UDF = "-65211"; // Production Order UDFs
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_FORM_DATA_ADD
        [B1Listener(BoEventTypes.et_FORM_DATA_ADD, true, new string[] { cFormID })]
        public virtual bool OnBeforeFormDataAdd(BusinessObjectInfo pVal)
        {
            var BubbleEvent = true;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        //--------------------------------------------------------------------------------------- et_FORM_DATA_UPDATE
        [B1Listener(BoEventTypes.et_FORM_DATA_UPDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeFormDataUpdate(BusinessObjectInfo pVal)
        {
            var BubbleEvent = true;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            Validate(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        //--------------------------------------------------------------------------------------- et_FORM_LOAD
        [B1Listener(BoEventTypes.et_FORM_LOAD, true, new string[] { cFormID })]
        public virtual bool OnBeforeFormLoad(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            if (oForm != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oForm);
            GC.Collect();

            return true;
        }

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeValidate(ItemEvent pVal)
        {
            var BubbleEvent = true;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_FORM_LOAD
        [B1Listener(BoEventTypes.et_FORM_LOAD, false, new string[] { cFormID })]
        public virtual void OnAfterFormLoad(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            FormSetup(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_FORM_DATA_LOAD
        [B1Listener(BoEventTypes.et_FORM_DATA_LOAD, false, new string[] { cFormID })]
        public virtual void OnAfterFormDataAdd(BusinessObjectInfo pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            SetFields(oForm);
            

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, false, new string[] { cFormID })]
        public virtual void OnAfterValidate(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            if (pVal.InnerEvent == true) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            
            // 9731 runs SetProcess if the item id is 6 (product number) and the the form is in add mode
            if (pVal.ItemUID == "6" && oForm.Mode == BoFormMode.fm_ADD_MODE) 
                SetProcess(oForm);

            if (pVal.ItemUID == "37" && pVal.ColUID == "4") Set_Project(oForm, pVal.Row);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_COMBO_SELECT *** UDF ***
        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID_UDF })]
        public virtual void OnAfterComboSelectUDF(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            if (pVal.InnerEvent == true) return;

            var oFormUDF = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            SetFieldsUDF(oFormUDF, pVal.ItemUID);
            

            NSC_DI.UTIL.Misc.KillObject(oFormUDF);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------
        private static void FormSetup(Form pForm)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
       
        private static void SetProcess(Form pForm)
        {
            // 9731 Sets the Pdo Process UDF with the Process UDF from the BOM, if the Process from the UDF field has a value
            Form oUDFForm = null;
            
            try
            {
                var productNumber = pForm.Items.Item("6").Specific.Value;
                if (productNumber == "")
                    return;

                oUDFForm = B1Connections.theAppl.Forms.Item(pForm.UDFFormUID);
                
                var SQL = $"SELECT U_NSC_DefProcess FROM OITT WHERE Code = '{productNumber}'";
                var processFromBOM = NSC_DI.UTIL.SQL.GetValue<string>(SQL, "");

                // 9731 sets pDo process field to the BOM default process IF default process is not null
                if (processFromBOM != "")
                    oUDFForm.Items.Item("U_NSC_Process").Specific.Value = processFromBOM;                
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDFForm);
                GC.Collect();
            }
        }

        private static void SetFieldsUDF(Form pForm, string pFieldID = null)
        {
            // THIS CODE STARTED CAUSING PROBLEMS FOR BUCKEYE,
            // SO JUST COMMENTED IT OUT.
            return;
            // set filed values and / or access - if null, then process all fields
            try
            {
                // isProductionBatch
                if (pFieldID == "U_NSC_isProductionBatch" || pFieldID == null)
                {
                    if (pForm.Items.Item("U_NSC_isProductionBatch").Specific.Value == "Y")
                    {
                        pForm.Items.Item("U_NSC_ProductionBatchID").Enabled = true;
                    }

                    else
                    {
                        pForm.Items.Item("U_NSC_ProductionBatchID").Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void SetFields(Form pForm, string pFieldID = null)
        {
            // set field values and / or access - if null, then process all fields
            Form oUDFForm = null;

            try
            {
                if (pForm.Items.Item("10").Specific.Value == "L") return;   // order is closed. can't change anything

                // UDFs
                oUDFForm = B1Connections.theAppl.Forms.Item(pForm.UDFFormUID);
                SetFieldsUDF(oUDFForm, pFieldID);
                //SetUDFFields(oUDFForm, pFieldID);

                // Main

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDFForm);
                GC.Collect();
            }
        }

        private static void Set_Project(Form pForm, int pRow)
        {
            // if a project is specified, make sure all lines have a project
            SAPbouiCOM.Matrix oMat = null;
            try
            {
                oMat = pForm.Items.Item("37").Specific;
                var proj = pForm.Items.Item("540000153").Specific.Value;
                if (string.IsNullOrEmpty(proj?.Trim())) return;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(proj))
                if (oMat.Columns.Item("4").Cells.Item(pRow).Specific.Value == "") return;
                if (oMat.Columns.Item("540000037").Cells.Item(pRow).Specific.Value != "") return;
                oMat.Columns.Item("540000037").Cells.Item(pRow).Specific.Value = proj;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }

        private static bool Validate(Form pForm)
        {
            // if a project is specified, make sure all lines have a project
            SAPbouiCOM.Matrix oMat = null;
            try
            {
                // CAN'T GET THE DATASOURCE TO WORK AND WILL NOT GO DOWN THE MATRIX - COULD BE LARGE

                oMat = pForm.Items.Item("37").Specific;
                var s = pForm.DataSources.DBDataSources.Count;
                for (var i = 0; i < s; i++)
                {
                    var x = pForm.DataSources.DBDataSources.Item(i);
                    var h = x.TableName;
                }
                //var s = pForm.DataSources.DBDataSources.Item("WOR1");   // .GetValue("Project", 1);
                //var a = s.Size;
                //for (var i = 0; i < oTransfer.Lines.Count; i++)
                //{
                //}
                return true;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return false;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }
    }
}
