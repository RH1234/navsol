﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms.SAP
{

    class F_940 : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "940"; // inventory transfer
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_FORM_LOAD
        [B1Listener(BoEventTypes.et_FORM_LOAD, true, new string[] { cFormID })]
        public virtual bool OnBeforeFormLoad(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            if (oForm != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oForm);
            GC.Collect();

            return true;
        }

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeValidate(ItemEvent pVal)
        {
            var BubbleEvent = true;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_FORM_LOAD
        [B1Listener(BoEventTypes.et_FORM_LOAD, false, new string[] { cFormID })]
        public virtual void OnAfterFormLoad(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            FormSetup(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_FORM_DATA_ADD
        [B1Listener(BoEventTypes.et_FORM_DATA_ADD, false, new string[] { cFormID })]
        public virtual void OnAfterFormDataAdd(BusinessObjectInfo pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            UpdatePlantJournal(oForm, pVal.ObjectKey);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------
        private static void FormSetup(Form pForm)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void UpdatePlantJournal(Form pForm, string pObjectKey)
        {
            SAPbobsCOM.StockTransfer oTransfer = null;
            try
            {
                var key = NSC_DI.SAP.B1Object.GetKey(pObjectKey);

                oTransfer = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oStockTransfer);

                if (!oTransfer.GetByKey(key)) throw new Exception(Globals.oCompany.GetLastErrorDescription());

                for (var i = 0; i < oTransfer.Lines.Count; i++)
                {
                    oTransfer.Lines.SetCurrentLine(i);

                    var itemCode = oTransfer.Lines.ItemCode;
                    if (NSC_DI.SAP.Items.IsPlant(itemCode))
                    {
                        var fromWrhsCode = oTransfer.Lines.FromWarehouseCode;

                        var fromWrhsName = NSC_DI.SAP.Warehouse.GetName(fromWrhsCode);

                        var wrhsCode = oTransfer.Lines.WarehouseCode;

                        var wrhsName = NSC_DI.SAP.Warehouse.GetName(wrhsCode);

                        for (var j = 0; j < oTransfer.Lines.SerialNumbers.Count; j++)
                        {
                            oTransfer.Lines.SerialNumbers.SetCurrentLine(j);
                            var plantID = oTransfer.Lines.SerialNumbers.InternalSerialNumber;

                            NSC_DI.SAP.PlantJournal.AddPlantJournalEntry(plantID, NSC_DI.Globals.JournalEntryType.Transfer,
                                $"Plant moved from warehouse #{fromWrhsName} to warehouse #{wrhsName}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oTransfer);
                GC.Collect();
            }
        }
       
    }

}
