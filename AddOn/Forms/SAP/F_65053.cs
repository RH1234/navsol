﻿using System;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms.SAP
{

    class F_65053 : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID     = "65053";  // Batch Details
        private const string cFormID_UDF = "-65053"; // UDFs
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_FORM_DATA_ADD
        [B1Listener(BoEventTypes.et_FORM_DATA_ADD, true, new string[] { cFormID })]
        public virtual bool OnBeforeFormDataAdd(BusinessObjectInfo pVal)
        {
            var BubbleEvent = true;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_FORM_LOAD
        [B1Listener(BoEventTypes.et_FORM_LOAD, false, new string[] { cFormID })]
        public virtual void OnAfterFormLoad(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            FormSetup(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if(pVal.ItemUID == "btnTestRlt") ShowTestResults(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------
        private static void FormSetup(Form pForm)
        {
            SAPbouiCOM.Item oItm = null;
            try
            {
                oItm = pForm.Items.Item("51");  // the quantities button

                oItm = CommonUI.Forms.Create.Item(pForm, "btnTestRlt", SAPbouiCOM.BoFormItemTypes.it_BUTTON, oItm.Top, oItm.Left - 73, oItm.Height, 65, 0, 0);
                oItm.Specific.Caption = "Test Results";
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private static void ShowTestResults(Form pForm)
        {
            try
            {              
                string batchNum = pForm.Items.Item("62").Specific.Value;
                string SourceStateID = pForm.Items.Item("63").Specific.Value;
                var oForm = new NavSol.Forms.Lab_Controls.F_TestResults();
                oForm.Form_Load(SourceStateID, batchNum);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }
    }
}
