﻿using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.SAP
{
    class WarehouseMasterData
    {
 /*	// COMPLIANCE
        private string warehouseID;
        private string warehouseName;
        public WarehouseMasterData(Application SAPBusinessOne_Application, Company SAPBusinessOne_Company)
        {
            this.FormType = "62";

            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
            _VirSci_Helper_Form = new SAP_BusinessOne.Helpers.Forms(SAPBusinessOne_Application: _SBO_Application);
        }

        override public void ItemEventHandler(string FormUID, ref ItemEvent pVal, out bool BubbleEvent)
        {
            // Prepare a new form helper
            SAP_BusinessOne.Helpers.Forms helperForm = new SAP_BusinessOne.Helpers.Forms(SAPBusinessOne_Application: _SBO_Application);

            // Grab the form that fired this call
            this._SBO_Form = helperForm.GetFormFromUID(pVal.FormUID);

            // Keep track of whether or not we want SAP to continue after running our code.
            bool _BubbleEvent = true;

            // Based off of the event type that fired the event
            switch (pVal.EventType)
            {
                case BoEventTypes.et_ITEM_PRESSED:

                    switch (pVal.ItemUID)
                    {
                        // The main "Add", "OK", "Find" button was clicked.
                        case "1":

                            // If we are in "Add" mode
                            if (pVal.FormMode == 3)
                            {


                                switch (pVal.BeforeAction)
                                {
                                    // Fire before SAP has fired code
                                    case true:

                                        warehouseID = ((EditText)_VirSci_Helper_Form.GetControlFromForm(
                                            ItemType: VERSCI.Forms.FormControlTypes.EditText
                                            , ItemUID: "5"
                                            , FormToSearchIn: this._SBO_Form
                                        )).Value.ToString();

                                        warehouseName = ((EditText)_VirSci_Helper_Form.GetControlFromForm(
                                            ItemType: VERSCI.Forms.FormControlTypes.EditText
                                            , ItemUID: "6"
                                            , FormToSearchIn: this._SBO_Form
                                        )).Value.ToString();
                                        // Run any validation code needed
                                        int foobar;
                                        if (!int.TryParse(warehouseID, out foobar))
                                        {
                                            // ID is not a int!
                                            BubbleEvent = false;

                                            _SBO_Application.StatusBar.SetText(Text: "Warehouse Code must be a number!",
                                                    Seconds: BoMessageTime.bmt_Medium,
                                                    Type: BoStatusBarMessageType.smt_Error);
                                            return;
                                        }

                                        break;

                                    // Fire after SAP has fired code
                                    case false:

                                        // After the addition of the warehouse was successful
                                        if (pVal.Action_Success)
                                        {
                                            string SQLQuery = string.Format(@"SELECT [U_{0}_WhrsType],[U_{0}_State] FROM OWHS WHERE WhsCode = '{1}'",
                                                                            SAP_BusinessOne.Global.SAP_PartnerCode, warehouseID);
                                            SAPbobsCOM.Recordset oRecordSet = (Recordset)_SBO_Company.GetBusinessObject(BoObjectTypes.BoRecordset);

                                            oRecordSet.DoQuery(SQLQuery);

                                            string warehouseType = oRecordSet.Fields.Item(0).Value; //Our types of inventory, to pre-pend name
                                            string warehouseState = oRecordSet.Fields.Item(1).Value; //state type: inventory (stock room) or cultivation (plant room)

                                            // Prepare a new controller for "Compliance"
                                            VirSci_SAP.Controllers.Compliance controllerCompliance = new Controllers.Compliance(SAPBusinessOne_Application: _SBO_Application, SAPBusinessOne_Company: _SBO_Company);

                                            VirSci_SAP.Controllers.Settings _VirSci_Controllers_Settings = new Controllers.Settings(SAPBusinessOne_Application: _SBO_Application,
                                                                                                                                    SAPBusinessOne_Company: _SBO_Company,
                                                                                                                                    SAPBusinessOne_Form: _SBO_Form);

                                            // Prepare a connection to the state API
                                            var TraceCon = new VirSci_SAP.Controllers.TraceabilityAPI(SAPBusinessOne_Application: _SBO_Application,
                                                SAPBusinessOne_Company: _SBO_Company,
                                                SAPBusinessOne_Form: _SBO_Form);

                                            BioTrack.API btAPI = TraceCon.new_API_obj();

                                            string compID = null;

                                            // we only do compliance stuff if it is of the correct type
                                            switch (warehouseState)
                                            {
                                                case "INV":                                                    
                                                    bool quarintine = false;
                                                    if (warehouseType.Equals("QND") || warehouseType.Equals("QNS"))
                                                    {
                                                        quarintine = true;
                                                    }
                                                    BioTrack.Room.Inventory.Add(ref btAPI,
                                                        warehouseType + " - " + warehouseName,
                                                        _VirSci_Controllers_Settings.GetValueOfSetting("StateLocLic"),
                                                        int.Parse(warehouseID),
                                                        quarintine);

                                                    // Create a new line item for compliance
                                                    compID = controllerCompliance.CreateComplianceLineItem(
                                                        Reason: Models.Compliance.ComplianceReasons.Inventory_Room_Add
                                                        , API_Call: btAPI.XmlApiRequest.ToString()
                                                    );

                                                    // API: Create a new inventory room
                                                    btAPI.PostToApi();
                                                    
                                                    if (btAPI.WasSuccesful)
                                                    {
                                                        controllerCompliance.UpdateComliancy(
                                                            ComplianceID: compID, 
                                                            ResponseXML: btAPI.xDocFromResponse.ToString(), 
                                                            Status: Controllers.Compliance.ComplianceStatus.Success);
                                                    }
                                                    else
                                                    {
                                                        controllerCompliance.UpdateComliancy(
                                                            ComplianceID: compID, 
                                                            ResponseXML: btAPI.xDocFromResponse.ToString(), 
                                                            Status: Controllers.Compliance.ComplianceStatus.Failed);
                                                    }
                                                    
                                                    break;

                                                case "CUL":

                                                    BioTrack.Room.Plant.Add(ref btAPI,
                                                        warehouseType + " - " + warehouseName,
                                                        _VirSci_Controllers_Settings.GetValueOfSetting("StateLocLic"),
                                                        int.Parse(warehouseID)
                                                    );

                                                    // Create a new line item for compliance
                                                    compID = controllerCompliance.CreateComplianceLineItem(
                                                        Reason: Models.Compliance.ComplianceReasons.Plant_Room_Add
                                                        , API_Call: btAPI.XmlApiRequest.ToString()
                                                    );

                                                    // API: Create a new cultivation room
                                                    btAPI.PostToApi();
                                                    
                                                    if (btAPI.WasSuccesful)
                                                    {
                                                        controllerCompliance.UpdateComliancy(
                                                            ComplianceID: compID, 
                                                            ResponseXML: btAPI.xDocFromResponse.ToString(), 
                                                            Status: Controllers.Compliance.ComplianceStatus.Success);
                                                    }
                                                    else
                                                    {
                                                        controllerCompliance.UpdateComliancy(
                                                            ComplianceID: compID, 
                                                            ResponseXML: btAPI.xDocFromResponse.ToString(), 
                                                            Status: Controllers.Compliance.ComplianceStatus.Failed);
                                                    }
                                                    break;
                                            }
                                        }

                                        break;
                                }
                            }

                            break;
                    }

                    break;
            }

            // Return whether or not SAP should continue after running our code.
            BubbleEvent = _BubbleEvent;
        }
  */
    }
}
