﻿using System;
using SAPbobsCOM;
using SAPbouiCOM;
using B1WizardBase;

namespace NavSol.Forms.SAP
{
    class F_169 : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "169"; // Main Menu
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_FORM_LOAD
        [B1Listener(BoEventTypes.et_FORM_LOAD, true, new string[] { cFormID })]
        public virtual bool OnBeforeFormLoad(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            if (oForm != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oForm);
            GC.Collect();

            return true;
        }

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeValidate(ItemEvent pVal)
        {
            var BubbleEvent = true;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_FORM_LOAD
        [B1Listener(BoEventTypes.et_FORM_LOAD, false, new string[] { cFormID })]
        public virtual void OnAfterFormLoad(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_FORM_DATA_LOAD
        [B1Listener(BoEventTypes.et_FORM_DATA_LOAD, false, new string[] { cFormID })]
        public virtual void OnAfterFormDataLoad(BusinessObjectInfo pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            if (oForm != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_FORM_DATA_ADD
        [B1Listener(BoEventTypes.et_FORM_DATA_ADD, false, new string[] { cFormID })]
        public virtual void OnAfterFormDataAdd(BusinessObjectInfo pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------
        public static int GetDefaultBranch()
        {
            // -1 is no branches, 0 is no default branch

            Form oForm = null;

            try
            {
                oForm = NavSol.CommonUI.Forms.GetForm(cFormID, 1);

                var infoStr = oForm.Items.Item("7").Specific.Caption;
                var pos = infoStr.IndexOf("Branch:");
                if (pos < 0) return -1;                 // branches are not used

                pos += 7;
                var branchName = infoStr.Substring(pos).Trim();
                if (branchName == "") return 0;         // no default branch

                // get the branch code
                var sql = $"SELECT BPLId FROM OBPL WHERE BPLName = '{branchName}'";
                var Branch = NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);

                return Branch;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
            }
        }
    }
}