﻿using System;
using SAPbobsCOM;
using SAPbouiCOM;
using B1WizardBase;
using System.Linq;

namespace NavSol.Forms.SAP
{
    class F_63 : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "63"; // Item Groups
        private const string cFormID_UDF = "-63"; 
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_FORM_LOAD

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID_UDF })]
        public virtual bool OnBeforeValidate(ItemEvent pVal)
            {
            var BubbleEvent = true;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            BubbleEvent = ValidateComplTestIds(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }
        #endregion BEFORE EVENT

        // Attempts to verify that the use input valid values, uint exists to ensure negative ints are not valid
        private static bool ValidateComplTestIds(Form pForm, ItemEvent pVal)
        {
            bool rtrnBool = true;
            try
            {
                if (pVal.ItemUID == "U_NSC_CmplTestID")
                {
                    rtrnBool = false;
                    string complTestIdStr = pForm.Items.Item("U_NSC_CmplTestID").Specific.Value;
                    try
                    {
                        uint[] array = complTestIdStr.Split(',').Select(str => uint.Parse(str)).ToArray();
                        rtrnBool = true;
                    }
                    catch
                    {
                        Globals.oApp.MessageBox("Pleae enter valid values");
                    }
                }
                return rtrnBool;                
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return rtrnBool;
            }
            finally
            {
            }
        }
    }
}
