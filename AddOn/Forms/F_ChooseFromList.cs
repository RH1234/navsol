﻿using System;
using System.Collections.Generic;
using System.Linq;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using Matrix = NavSol.CommonUI.Matrix;

namespace NavSol.Forms
{
    public class F_CFL : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------
        private const string cFormID = "NSC_CFL_NEW";

        private static string _SearchString = "";
        private static string _matrixColumnSelected = "";
        private static string _callingFormUid = String.Empty;
        public static List<Dictionary<string, string>> rowsSelected;
        public static ChooseFromListCallback CflCB = null;
        private static int Top;
        private static int Left;
        private static int Height;
        private static int Width;

        private static string Title;
        private static string Sql;
        private static bool MultiSelect;
        //private static int[] ColWidths;

        private static List<Matrix.MatrixColumn> MatrixColumns;
        private enum SelectionDirection
        {
            Up,
            Down
        }
        #endregion VARS

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "BTN_OK") BTN_CHOOSE_Click(oForm);

            if (pVal.Row == 0)
            {
                // Store the column that is selected.
                _matrixColumnSelected = pVal.ColUID;
                SearchMatrixFromText(oForm);
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_KEY_DOWN
        [B1Listener(BoEventTypes.et_KEY_DOWN, false, new string[] { cFormID })]
        public virtual void OnAfterKeyDown(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            // Capturing the Down Arrow Button Press.
            if (pVal.CharPressed == 40) MoveSelection(oForm, SelectionDirection.Down);
            if (pVal.CharPressed == 38) MoveSelection(oForm, SelectionDirection.Up);

            if (pVal.ItemUID == "TXT_FIND") SearchMatrixFromText(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_DOUBLE_CLICK
        [B1Listener(BoEventTypes.et_DOUBLE_CLICK, false, new string[] { cFormID })]
        public virtual void OnAfterDoubleClick(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "MTX_MAIN" && pVal.Row != 0) BTN_CHOOSE_Click(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_FORM_RESIZE
        [B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
        public virtual void OnAfterFormResize(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            FormReSize(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------
        private static string FormXML()
        {
            var xml = "";
            switch (Globals.oCompany.language)
            {
                case (BoSuppLangs.ln_English):
                    xml = @"<?xml version='1.0' encoding='UTF-16'?>
<Application>
  <forms>
    <action type='add'>
      <form appformnumber='(NSC_FORM)' FormType='(NSC_FORM)' type='0' BorderStyle='0' uid='(NSC_FORM)' title='CFL_Helper' visible='0' default_button='BTN_OK' pane='1' color='0' left='300' top='100' width='420' height='275' client_width='' client_height='' AutoManaged='0' SupportedModes='15' ObjectType='' mode='1'>
        <datasources>
          <DataTables />
          <dbdatasources>
            <action type='add' />
          </dbdatasources>
          <userdatasources>
            <action type='add' />
          </userdatasources>
        </datasources>
        <Menus />
        <items>
          <action type='add'>
            <item cellHeight='16' tab_order='0' titleHeight='20' top='29' left='16' width='600' height='279' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='' right_just='0' type='127' visible='1' uid='MTX_MAIN' IsAutoGenerated='0'>
              <specific layout='0' SelectionMode='0'>
                <columns>
                  <action type='add'>
                    <column backcolor='16777215' ChooseFromListIsAutoFill='0' font_size='-1' forecolor='-1' text_style='0' disp_desc='0' visible='1' AffectsFormMode='1' val_on='Y' IsAutoGenerated='0' val_off='N' title='#' width='25' editable='1' type='16' right_just='0' uid='#' sortable='0' />
                    <column backcolor='-1' ChooseFromListIsAutoFill='0' font_size='-1' forecolor='-1' text_style='0' disp_desc='0' visible='1' AffectsFormMode='1' val_on='Y' IsAutoGenerated='0' val_off='N' title='Col_0' width='50' editable='1' type='16' right_just='0' uid='Col_0' sortable='0' />
                  </action>
                </columns>
              </specific>
            </item>
            <item top='315' left='16' width='65' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='' right_just='0' type='4' visible='1' uid='BTN_OK' IsAutoGenerated='0'>
              <specific caption='Choose' />
            </item>
            <item top='315' left='90' width='65' height='20' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='' right_just='0' type='4' visible='1' uid='2' IsAutoGenerated='0'>
              <specific caption='Cancel' />
            </item>
            <item backcolor='-1' font_size='-1' forecolor='-1' tab_order='0' text_style='' top='8' left='113' width='169' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='' right_just='0' type='16' visible='1' uid='TXT_FIND' IsAutoGenerated='0'>
              <specific ChooseFromListAlias='' ChooseFromListIsAutoFill='0' ChooseFromListUID='' IsPassword='0' supp_zeros='0' />
            </item>
            <item top='8' left='25' width='82' height='14' AffectsFormMode='1' description='' disp_desc='0' enabled='1' from_pane='0' to_pane='0' linkto='TXT_FIND' right_just='0' type='8' visible='1' uid='LBL_FIND' IsAutoGenerated='0'>
              <specific caption='Find' />
            </item>
          </action>
        </items>
        <ChooseFromListCollection>
          <action type='add'>
            <ChooseFromList UniqueID='-1' ObjectType='-1' MultiSelection='0' IsSystem='1' />
          </action>
        </ChooseFromListCollection>
        <DataBrowser BrowseBy='' />
        <Settings MatrixUID='' Enabled='0' EnableRowFormat='0' />
      </form>
    </action>
  </forms>
</Application>";
                    break;

                case (BoSuppLangs.ln_Spanish_La):
                    xml = "";
                    break;
            }
            return xml;
        }

        private static void FormLoad(Form pForm)
        {

            // Freeze the Form UI
            pForm.Freeze(true);

            // Load the Matrix
            //Load_Matrix(pForm);

            try
            {
                // Storing original form size to properly set button locations.
                var formWidth = pForm.Width;
                var formHeight = pForm.Height;

                // Center the form on the screen
                pForm.Width = Width;
                pForm.ClientWidth = Width;

                pForm.Height = Height;
                pForm.ClientHeight = Height;

                if (Top != -1) pForm.Top = Top;
                if (Left != -1) pForm.Left = Left;

                SAPbouiCOM.Matrix oMatrix = pForm.Items.Item("MTX_MAIN").Specific;
                //oGrd.DataTable = pForm.DataSources.DataTables.Item("cfl");
                //pForm.DataSources.DataTables.Item("cfl").ExecuteQuery(Sql);

                // These are just numbers I found that worked best with the form.
                var widthDiff = 50;
                var heightDiff = 95;

                var percentageOfHeight = (oMatrix.Item.Height + oMatrix.Item.Top + 10) / formHeight;
                var percentageOfWidth = oMatrix.Item.Width / formWidth;

                // The 570 and 735 are the values the tab control initial height and width.
                //oMatrix.Item.Width = (int)(pForm.Width * percentageOfWidth);
                //oMatrix.Item.Height = (int)(pForm.Height * percentageOfHeight);

                var btnOk = pForm.Items.Item("BTN_OK");
                //if (btnOk != null)
                //{
                //    double percentageFromTop = (btnOk.Top + btnOk.Height + 5) / formHeight;
                //    btnOk.Top = (int)(pForm.Height * percentageFromTop);
                //}


                var btnCancel = (Item)pForm.Items.Item("2");
                //if (btnCancel != null)
                //{
                //    // Adding a 5 px margin
                //    double percentageFromTop = (btnCancel.Top + btnCancel.Height + 5) / formHeight;
                //    btnCancel.Top = (int)(pForm.Height * percentageFromTop);
                //}
                Load_Matrix(pForm);
                
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                pForm.VisibleEx = true;
            }
        }
        public delegate void ChooseFromListCallback(string callingFormUid, System.Data.DataTable values);
		public static Form FormCreate(string pTitle, string pCallingFormUID, string pSQL, List<Matrix.MatrixColumn> pMatrixColumns,
			bool pMultiSelect = false, int[] pColWidths = null, int pTop = -1, int pLeft = -1, int pHeight = -1, int pWidth = -1)
		{
			Item oItm = null;
			try
			{
				var formStr = FormXML();
				formStr = formStr.Replace("(NSC_FORM)", cFormID);
				var oForm = CommonUI.Forms.LoadXML_Modal(formStr);

                _callingFormUid = pCallingFormUID;

                oForm.Title = pTitle;

				var grdWidth = (pColWidths == null) ? -1 : pColWidths.Sum();

				Top = 40;
				Left = 380;
                //Height = (pHeight == -1) ? 150 : pHeight;
                //grdWidth = (grdWidth == -1) ? 200 : grdWidth;
                //Width = (pWidth == -1) ? grdWidth : pWidth;
                Height =  400;
                grdWidth = 400;
                Width = 700;

                Title = pTitle;
				Sql = pSQL;
				MultiSelect = pMultiSelect;
				MatrixColumns = pMatrixColumns;
				//ColWidths = pColWidths;

				FormLoad(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return null;
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		private void MoveSelection(Form pForm, SelectionDirection DirectionToMove)
		{
			SAPbouiCOM.Matrix MTX_MAIN = null;
			try
			{
				MTX_MAIN = pForm.Items.Item("MTX_MAIN").Specific;
				var currentSelectRow = -1;
				for (var i = 1; i < (MTX_MAIN.RowCount + 1); i++)
				{
					// If the current row is now selected we can just continue looping.
					if (!MTX_MAIN.IsRowSelected(i)) continue;

					currentSelectRow = i;
					break;
				}

				// If we didn't find the current row we will ignore this call.
				if (currentSelectRow == -1)
				{
					return;
				}

				// Move the selection down one.
				if (DirectionToMove == SelectionDirection.Down && currentSelectRow < MTX_MAIN.RowCount + 1)
				{
					MTX_MAIN.SelectRow(currentSelectRow + 1, true, false);
				}
				else if (DirectionToMove == SelectionDirection.Up && currentSelectRow > 1)
				{
					MTX_MAIN.SelectRow(currentSelectRow - 1, true, false);
				}
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(MTX_MAIN);
				GC.Collect();
			}
		}

		private void SearchMatrixFromText(Form pForm)
		{
			EditText TXT_FIND = null;
			SAPbouiCOM.Matrix MTX_MAIN = null;

			try
			{
				TXT_FIND = pForm.Items.Item("TXT_FIND").Specific;
				MTX_MAIN = pForm.Items.Item("MTX_MAIN").Specific;
			
				if (string.IsNullOrEmpty(_matrixColumnSelected))
				{
					Globals.oApp.StatusBar.SetText("Please select a column you wish to search through by clicking the top row column.", BoMessageTime.bmt_Short);
				}

				if (!TXT_FIND.Value.Equals(_SearchString))
				{
					if (MatrixColumns == null)
					{
						// The _CFL Populator is null for some reason. Force the user to reload the helper.
						pForm.Close();
						return;
					}

					_SearchString = TXT_FIND.Value;

					// We don't care to search the matrix if there is nothing to search.. 
					// Also adding a min of 2 characters to be typed before we check anything.
					if (string.IsNullOrEmpty(_SearchString) || _SearchString.Length < 2)
					{
						return;
					}

					DataTable dtChooseFromList = pForm.DataSources.DataTables.Item("Matrix");

					// Get the column we want to search through.
					var columnCounter = 0;
					foreach (var item in MatrixColumns)
					{
						var uniqueID = MTX_MAIN.Columns.Item(columnCounter).UniqueID.ToString();
						if (_matrixColumnSelected.Equals(uniqueID))
						{
							break;
						}
						columnCounter++;
					}

					// Loop over only the column that is selected.
					for (var i = 0; i <= (dtChooseFromList.Rows.Count); i++)
					{
						try
						{
							string value = dtChooseFromList.GetValue(columnCounter, i).ToString();
							if (!value.ToLower().Contains(_SearchString.ToLower())) continue;
							MTX_MAIN.SelectRow((i+1), true, false);
							return;
						}
						catch (Exception ex)
						{
							Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
						}
					}
				}
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(TXT_FIND);
				NSC_DI.UTIL.Misc.KillObject(MTX_MAIN);
				GC.Collect();
			}
		}

		private void BTN_CHOOSE_Click(Form pForm)
		{
			SAPbouiCOM.Matrix MTX_MAIN = null;

			try
			{
				if (MatrixColumns == null)
				{
					return; // We failed.
				}
                System.Data.DataTable selectedItems = new System.Data.DataTable();
                foreach (var col in MatrixColumns)
                {
                    System.Data.DataColumn dc = new System.Data.DataColumn(col.ColumnName, typeof(string));
                    selectedItems.Columns.Add(dc);
                }
                // Get the data of the row of the selected Matrix.
                var dataOftheSelectedRow = new Dictionary<string, string>();

				MTX_MAIN = pForm.Items.Item("MTX_MAIN").Specific;

				// Only check for selected rows here
				var NextRow = MTX_MAIN.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);

				while (NextRow != -1)
				{
					var columnCounter = 0;
                    System.Data.DataRow dr = selectedItems.NewRow();
					foreach (var item in MatrixColumns)
					{
						var value = MTX_MAIN.Columns.Item(columnCounter).Cells.Item(NextRow).Specific.Value;
						//dataOftheSelectedRow.Add(item.ColumnName, value);
                        dr[item.ColumnName] = value;
						columnCounter++;
					}
                    selectedItems.Rows.Add(dr);
					//rowsSelected.Add(dataOftheSelectedRow);
					//dataOftheSelectedRow = new Dictionary<string, string>();
					NextRow = MTX_MAIN.GetNextSelectedRow(NextRow, BoOrderType.ot_RowOrder);
				}
                pForm.Close();

                if (CflCB != null && selectedItems.Rows.Count > 0) CflCB(_callingFormUid, selectedItems);
            }
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(MTX_MAIN);
				GC.Collect();
			}
		}

		private void FormReSize(Form pForm)
		{
			SAPbouiCOM.Matrix MTX_MAIN = null;

			try
			{
				MTX_MAIN = pForm.Items.Item("MTX_MAIN").Specific;
                MTX_MAIN.AutoResizeColumns();
            }
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(MTX_MAIN);
				GC.Collect();
			}
		}
        private static void Load_Matrix(Form pForm)
        {
            SAPbouiCOM.Matrix oMatrix = null;

            try
            {
                Matrix.LoadDatabaseDataIntoMatrix(pForm, "Matrix", "MTX_MAIN", MatrixColumns, Sql);
                oMatrix = pForm.Items.Item("MTX_MAIN").Specific;

                oMatrix.SelectionMode = MultiSelect ? SAPbouiCOM.BoMatrixSelect.ms_Auto : SAPbouiCOM.BoMatrixSelect.ms_Single;

                if (oMatrix.RowCount > 0) oMatrix.SelectRow(1, true, false);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMatrix);
                GC.Collect();
            }
        }
	}
}
