﻿using System;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms
{
	public class F_Waiting : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_WAITING";

		private static int currentProgress = 1;
		private static int maxLoaderImages = 49;
		private static string loaderImage = "virLoadv1_00";

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------

		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------

		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			pForm.Freeze(true);

			try
			{
				pForm.Visible = false;
				pForm.Freeze(true);

				pForm.Left = Globals.oApp.Desktop.Width/3;
				pForm.Top = Globals.oApp.Desktop.Height/3;

				pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = true, string pCallingFormUID = null)
		{
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingFrm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		public static void SetProgressBar(Form pForm, int Width, string Message = null)
		{
			if (!string.IsNullOrEmpty(Message)) pForm.Items.Item("LBL_INFO").Specific.Caption = Message;
			MoveProgress(pForm, Width);
		}

		public static void MoveProgress(Form pForm, int Width)
		{
			var imageNumber = maxLoaderImages*(((double) Width)/100.0d);
			var imageProgressNumber = (int) (imageNumber);
			for (; currentProgress < imageProgressNumber; currentProgress++)
			{
				loaderImage = "virLoadv1_00";
				if (currentProgress < 10)
				{
					loaderImage += "0" + currentProgress + ".bmp";
				}
				else
				{
					loaderImage += currentProgress + ".bmp";
				}

				CommonUI.Forms.SetFieldvalue.Loader(pForm, "IMG_PROG", loaderImage);
			}

			// If we are at 100% delay the window a split second.
			if (currentProgress >= maxLoaderImages)
			{
				// Set it to our max image.
				loaderImage = "virLoadv1_0048.bmp";
				CommonUI.Forms.SetFieldvalue.Loader(pForm, "IMG_PROG", loaderImage);
			}
		}

		public static void Form_Close(Form pForm)
		{
            try
            {
                // Bring focus to the waiting form
                pForm.Select();

                // Close the waiting form
                pForm.Close();
            }
                catch { }
		}
}
}
