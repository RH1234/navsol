﻿using System;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms
{
	public class F_CFL_GRID : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "NSC_CFL_Grid";

        private static string _valueSeparator   = "";
        private static string _SearchString     = "";
		private static string _colSearch        = "1";
        private static string _returnField      = "";

        public static CFL_GRID_Callback CflCB = null;

        private enum SelectionDirection
		{
			Up,
			Down
		}

		//public List<Dictionary<string, string>> selections;

		//private static int Top;
		//private static int Left;
		//private static int Height;
		//private static int Width;

		//private static string Title;
		//private static string Sql;
		//private static bool		MultiSelect;
		//private static int[] ColWidths;


		#endregion VARS

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			if (pVal.ItemUID == "btnChoose") btnChoose(oForm);

			if (pVal.ItemUID == "grdData" && pVal.Row == -1) 
			{
				// Store the column that is selected.
				_colSearch = pVal.ColUID;
                grdSearch(oForm);
            }

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_KEY_DOWN
		[B1Listener(BoEventTypes.et_KEY_DOWN, false, new string[] {cFormID})]
		public virtual void OnAfterKeyDown(ItemEvent pVal)
		{
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            // Capturing the Down Arrow Button Press.
            if (pVal.CharPressed == 40) matMoveSelection(oForm, SelectionDirection.Down);
            if (pVal.CharPressed == 38) matMoveSelection(oForm, SelectionDirection.Up);

            if (pVal.ItemUID == "txtFind") grdSearch(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

		//--------------------------------------------------------------------------------------- et_DOUBLE_CLICK
		[B1Listener(BoEventTypes.et_DOUBLE_CLICK, false, new string[] { cFormID })]
		public virtual void OnAfterDoubleClick(ItemEvent pVal)
		{
			//if (pVal.ActionSuccess == false) return;
			//var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			//if (pVal.ItemUID == "MTX_MAIN" && pVal.Row != 0) btnChoose(oForm);

			//NSC_DI.UTIL.Misc.KillObject(oForm);
			//GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_FORM_RESIZE
		[B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
		public virtual void OnAfterFormResize(ItemEvent pVal)
		{
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            FormReSize(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public static Form FormCreate(string pTitle, string pSQL, string pCallingForm, string pCallingField, string pReturnField = null, bool pMultiSelect = false, string pValueSeparator = "^")
		{
            SAPbouiCOM.Form oForm = null;
			try
			{
                _valueSeparator = pValueSeparator;
                _returnField    = pReturnField;

                oForm = CommonUI.Forms.Load(cFormID, true);
                oForm.Title = pTitle;

                oForm.Items.Item("txtFormID").Specific.Value  = pCallingForm;
                oForm.Items.Item("txtFieldID").Specific.Value = pCallingField;

                // GRID
                oForm.DataSources.DataTables.Add("dtGrid");
                oForm.Items.Item("grdData").Specific.DataTable = oForm.DataSources.DataTables.Item("dtGrid");

                if (pMultiSelect)
                    oForm.Items.Item("grdData").Specific.SelectionMode = BoMatrixSelect.ms_Auto;
                else
                    oForm.Items.Item("grdData").Specific.SelectionMode = BoMatrixSelect.ms_Single;

                oForm.DataSources.DataTables.Item("dtGrid").ExecuteQuery(pSQL);
                oForm.Items.Item("grdData").Specific.AutoResizeColumns();

                _colSearch = oForm.Items.Item("grdData").Specific.Columns.Item(0).UniqueID;

                return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
                if(oForm != null) oForm.Visible = true;
                GC.Collect();
			}
		}

        private static void grdSearch(Form pForm)
        {
            Grid oGrd = null;

            try
            {
                //if (TXT_FIND.Value.Equals(_SearchString)) return;
                oGrd = pForm.Items.Item("grdData").Specific;

                _SearchString = pForm.Items.Item("txtFind").Specific.Value;

                if (string.IsNullOrEmpty(_SearchString)) return;

                var dt = pForm.DataSources.DataTables.Item("dtGrid");

                // Loop over only the column that is selected.
                for (var i = 0; i < (dt.Rows.Count); i++)
                {
                    string value = dt.GetValue(_colSearch, i).ToString();
                    if (value.ToLower().StartsWith(_SearchString.ToLower()))
                    {
                        oGrd.Rows.SelectedRows.Clear();
                        oGrd.Rows.SelectedRows.Add(i);
                        return;

                    }
                }
            }
            catch (Exception ex)
            {
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void matMoveSelection(Form pForm, SelectionDirection DirectionToMove)
		{
			var MTX_MAIN =
				 CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", pForm) as Matrix;

			var currentSelectRow = -1;
			for (var i = 1; i < (MTX_MAIN.RowCount + 1); i++)
			{
				try
				{
					// If the current row is now selected we can just continue looping.
					if (!MTX_MAIN.IsRowSelected(i)) continue;

					currentSelectRow = i;
					break;
				}
				catch (Exception ex)
				{
					// We don't care.. something happened in the move.
				}
			}

			// If we didn't find the current row we will ignore this call.
			if (currentSelectRow == -1)
			{
				return;
			}

			try
			{
				// Move the selection down one.
				if (DirectionToMove == SelectionDirection.Down && currentSelectRow < MTX_MAIN.RowCount + 1)
				{
					MTX_MAIN.SelectRow(currentSelectRow + 1, true, false);
				}
				else if (DirectionToMove == SelectionDirection.Up && currentSelectRow > 1)
				{
					MTX_MAIN.SelectRow(currentSelectRow - 1, true, false);
				}
			}
			catch (Exception ex)
			{
				// We don't care.. something happened in the move.
			}
		}

		private static void btnChoose(Form pForm)
		{
            Grid oGrd = null;

            try
            {
                oGrd = pForm.Items.Item("grdData").Specific;               
                if (string.IsNullOrEmpty(_returnField?.Trim())) _returnField = oGrd.Columns.Item(0).UniqueID;// Whitespace-Change NSC_DI.UTIL.Strings.Empty(_returnField)
                string callingForm   = pForm.Items.Item("txtFormID").Specific.Value.ToString();
                string callingField  = pForm.Items.Item("txtFieldID").Specific.Value.ToString();
                Form oCallingForm = Globals.oApp.Forms.Item(callingForm);
                var cnt = oGrd.Rows.SelectedRows.Count;
                //var val = "";

                //for (var i = 1; i <= cnt; i++)
                //{
                //    var row = oGrd.Rows.SelectedRows.Item(i-1, BoOrderType.ot_RowOrder);
                //    val += oGrd.DataTable.GetValue(_returnField, row);
                //    if (i < cnt) val += _valueSeparator;   
                //}
                //oCallingForm.Items.Item(callingField).Specific.Value = val;

                var UIfields = callingField.Split(',');
                var DBfields = _returnField.Split(',');

                for (var j = 0; j < UIfields.Length; j++)
                {
                    var val = "";
                    for (var i = 1; i <= cnt; i++)
                    {
                        var row = oGrd.Rows.SelectedRows.Item(i - 1, BoOrderType.ot_RowOrder);
                        val += oGrd.DataTable.GetValue(DBfields[j].Trim(), row);
                        if (i < cnt) val += _valueSeparator;
                    }
                    oCallingForm.Items.Item(UIfields[j].Trim()).Specific.Value = val;
                }

                if (CflCB != null) CflCB(callingForm);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
                pForm.Close();
            }
        }

        private static void FormReSize(Form pForm)
        {
            //Grid oGrd = null;

            try
            {
                //oGrd = pForm.Items.Item("grdData").Specific;
                pForm.Items.Item("grdData").Specific.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }

        public delegate void CFL_GRID_Callback(string callingFormUid);
    }
}