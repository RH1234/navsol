﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Enviromental_Controls
{
	internal class F_FilterMngt : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_FILTER_MNGT";

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------

		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "BTN_ADDLS":
					BTN_ADDLS_Click(oForm);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
		[B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] {cFormID})]
		public virtual void OnAfterChooseFromList(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				// Item ChooseFromList
				case "TXT_FILT":
					TXT_FILT_ChooseFromList_Selected(oForm, (SAPbouiCOM.IChooseFromListEvent) pVal);
					break;
				// Warehouse ChooseFromList
				case "TXT_WRHS":
					TXT_WRHS_ChooseFromList_Selected(oForm, (SAPbouiCOM.IChooseFromListEvent) pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			try
			{
				pForm.Freeze(true);

				pForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;

                // Set the Logo image
                CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_Main", "filter-icon.bmp");


				pForm.Items.Item("TXT_CDATE").Specific.Value = DateTime.UtcNow.ToString("yyyyMMdd");

				pForm.Items.Item("TXT_LOGID").Specific.Value =
					NSC_DI.UTIL.SQL.GetValue<string>("SELECT MAX(CONVERT(int,[Code])) + 1 FROM [@" + NSC_DI.Globals.tFilterLog + "]", "1");

				Load_Matrix_FilterLog(pForm);

				pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Form oForm = null;
			Item oItm = null;
			try
			{
                oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return null;
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		private static void BTN_ADDLS_Click(Form pForm)
		{
			try
			{
                var userName = Globals.oApp.Company.UserName;

                var filterItem = CommonUI.Forms.GetField<string>(pForm, "TXT_ITEMC");

                var quantity = CommonUI.Forms.GetField<string>(pForm, "TXT_QUANT");

                var replaceDate = CommonUI.Forms.GetField<string>(pForm, "TXT_REDATE");

                var warehouseCode = CommonUI.Forms.GetField<string>(pForm, "TXT_WRHSC");

                if (string.IsNullOrEmpty(filterItem))
				{
					Globals.oApp.StatusBar.SetText("Please select an item!", BoMessageTime.bmt_Short);
					return;
				}

				if (string.IsNullOrEmpty(warehouseCode))
				{
					Globals.oApp.StatusBar.SetText("Please select a warehouse!", BoMessageTime.bmt_Short);
					return;
				}

				if (string.IsNullOrEmpty(quantity))
				{
					Globals.oApp.StatusBar.SetText("Please enter a quantity!", BoMessageTime.bmt_Short);
					return;
				}

				if (string.IsNullOrEmpty(replaceDate))
				{
					Globals.oApp.StatusBar.SetText("Please enter a date!", BoMessageTime.bmt_Short);
					return;
				}

                // Attempt to create the FilterLog 
               
                NSC_DI.SAP.FilterMngtLog.CreateNewFilterLog(userName, filterItem, quantity, replaceDate, warehouseCode);

				Load_Matrix_FilterLog(pForm);

				Globals.oApp.StatusBar.SetText("Successfully added details to the Filter Log", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private static void TXT_FILT_ChooseFromList_Selected(Form pForm, IChooseFromListEvent pChooseFromListEvent)
		{
			try
			{
				DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

				string oItemCode = oDataTable.GetValue(0, 0).ToString();
				string oItemName = oDataTable.GetValue(1, 0).ToString();

				// Set the userdatasource for the name textbox
				pForm.DataSources.UserDataSources.Item("UDS_OITEM").ValueEx = Convert.ToString(oItemName);

				// Save the business partners code
				pForm.Items.Item("TXT_ITEMC").Specific.Value = oItemCode;
			}
			catch(Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
		}

		private static void TXT_WRHS_ChooseFromList_Selected(Form pForm, IChooseFromListEvent pChooseFromListEvent)
		{
			try
			{
				DataTable oDataTable = pChooseFromListEvent.SelectedObjects;
				string oWarehouseCode = oDataTable.GetValue(0, 0).ToString();
				string oWarehouseName = oDataTable.GetValue(1, 0).ToString();

				// Set the userdatasource for the name textbox
				pForm.DataSources.UserDataSources.Item("UDS_WRHS").ValueEx = Convert.ToString(oWarehouseName);

				// Save the business partners code
				pForm.Items.Item("TXT_WRHSC").Specific.Value = oWarehouseCode;

			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
		}

		private static void Load_Matrix_FilterLog(Form pForm)
		{
			try
			{
				// Load the matrix of logged air quality
				CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tFilterLog, "MTX_FLTLOG", new List<CommonUI.Matrix.MatrixColumn>()
					{
						//TODO: make item/warehouse links?
						new CommonUI.Matrix.MatrixColumn() {Caption = "ID", ColumnWidth = 40, ColumnName = "Code", ItemType = BoFormItemTypes.it_EDIT},
						new CommonUI.Matrix.MatrixColumn() {Caption = "Item", ColumnWidth = 40, ColumnName = "U_ItemCode", ItemType = BoFormItemTypes.it_EDIT},
						new CommonUI.Matrix.MatrixColumn() {Caption = "Created By", ColumnWidth = 40, ColumnName = "U_CreatedBy", ItemType = BoFormItemTypes.it_EDIT},
						new CommonUI.Matrix.MatrixColumn() {Caption = "Quantity", ColumnWidth = 80, ColumnName = "U_Quantity", ItemType = BoFormItemTypes.it_EDIT},
						new CommonUI.Matrix.MatrixColumn() {Caption = "Warehouse", ColumnWidth = 80, ColumnName = "U_Warehouse", ItemType = BoFormItemTypes.it_EDIT},
						new CommonUI.Matrix.MatrixColumn() {Caption = "Created On", ColumnWidth = 40, ColumnName = "U_CreatedOn", ItemType = BoFormItemTypes.it_EDIT},
						new CommonUI.Matrix.MatrixColumn() {Caption = "ReplaceDate", ColumnWidth = 80, ColumnName = "U_ReDate", ItemType = BoFormItemTypes.it_EDIT}
					}, "SELECT TOP 100 * FROM [@" + NSC_DI.Globals.tFilterLog + @"] ORDER BY [U_CreatedOn] DESC");
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

	}
}
