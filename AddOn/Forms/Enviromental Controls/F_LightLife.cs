﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using Matrix = NavSol.CommonUI.Matrix;

namespace NavSol.Forms.Enviromental_Controls
{
	internal class F_LightLife : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_LIGHTLIFE";

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------

		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "BTN_ADDLS":
					BTN_ADDLS_Click(oForm);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
		[B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] {cFormID})]
		public virtual void OnAfterChooseFromList(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				// Item ChooseFromList
				case "TXT_LTITEM":
					TXT_LTITEM_ChooseFromList_Selected(oForm, (SAPbouiCOM.IChooseFromListEvent) pVal);
					break;
				// Warehouse ChooseFromList
				case "TXT_WRHS":
					TXT_WRHS_ChooseFromList_Selected(oForm, (SAPbouiCOM.IChooseFromListEvent) pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			PictureBox IMG_Main = null;
			try
			{
				pForm.Freeze(true);

				pForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;

                // Set the Logo image
                CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_Main", "light-icon.bmp");

				Load_Matrix(pForm);

				pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
				NSC_DI.UTIL.Misc.KillObject(IMG_Main);
				GC.Collect();
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Form oForm = null;
			Item oItm = null;
			try
			{
				oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return null;
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		private static void Load_Matrix(Form pForm)
		{
			try
			{
				// Load the matrix of logged light life
				CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tLightLife, "MTX_LTLOG", new List<Matrix.MatrixColumn>()
				{
					//TODO: make item/warehouse links?
					new Matrix.MatrixColumn() {Caption = "ID", ColumnWidth = 40, ColumnName = "Code", ItemType = BoFormItemTypes.it_EDIT}
					,
					new Matrix.MatrixColumn() {Caption = "Item", ColumnWidth = 40, ColumnName = "U_LightItem", ItemType = BoFormItemTypes.it_EDIT}
					,
					new Matrix.MatrixColumn() {Caption = "Created By", ColumnWidth = 40, ColumnName = "U_CreatedBy", ItemType = BoFormItemTypes.it_EDIT}
					,
					new Matrix.MatrixColumn() {Caption = "Quantity", ColumnWidth = 80, ColumnName = "U_Quantity", ItemType = BoFormItemTypes.it_EDIT}
					,
					new Matrix.MatrixColumn() {Caption = "Warehouse", ColumnWidth = 80, ColumnName = "U_Warehouse", ItemType = BoFormItemTypes.it_EDIT}
					,
					new Matrix.MatrixColumn() {Caption = "Created On", ColumnWidth = 40, ColumnName = "U_CreatedOn", ItemType = BoFormItemTypes.it_EDIT}
					,
					new Matrix.MatrixColumn() {Caption = "ReplaceDate", ColumnWidth = 80, ColumnName = "U_ReplaceDate", ItemType = BoFormItemTypes.it_EDIT}
				}, "SELECT TOP 100 * FROM [@" + NSC_DI.Globals.tLightLife + "] ORDER BY [Code] DESC");
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
		}

		private static void BTN_ADDLS_Click(Form pForm)
		{
			try
			{
				EditText TXT_LTITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_LTITEM", pForm);

				// Validate that we have a Light item selected.
				EditText TXT_ITEMC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEMC", pForm);
				if (string.IsNullOrEmpty(TXT_ITEMC.Value))
				{
					Globals.oApp.StatusBar.SetText("Please select an item!", BoMessageTime.bmt_Short);
					return;
				}

				// Validate that we have a warehouse selected.
				EditText TXT_WRHSC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHSC", pForm);
				if (string.IsNullOrEmpty(TXT_WRHSC.Value))
				{
					Globals.oApp.StatusBar.SetText("Please select a warehouse!", BoMessageTime.bmt_Short);
					return;
				}

				// Validate that a quanitity is set.
				EditText TXT_QUANT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QUANT", pForm);
				if (string.IsNullOrEmpty(TXT_QUANT.Value))
				{
					Globals.oApp.StatusBar.SetText("Please enter a quantity!", BoMessageTime.bmt_Short);
					return;
				}

				// Validate that we have a date set.
				EditText TXT_REDATE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_REDATE", pForm);
				if (string.IsNullOrEmpty(TXT_REDATE.Value))
				{
					Globals.oApp.StatusBar.SetText("Please enter a date!", BoMessageTime.bmt_Short);
					return;
				}

				EditText TXT_WRHS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHS", pForm);

				// Attempt to create the Light Life Log 
				// Active is currently not hooked to anything.. We are holding off until we get more information on this.
				NSC_DI.SAP.LightLifeManagement.CreateNewLightLifeLog(Globals.oApp.Company.UserName, TXT_ITEMC.Value, TXT_QUANT.Value, TXT_REDATE.Value, TXT_WRHSC.Value);

				// Clear all the form data.

				TXT_LTITEM.Value = string.Empty;
				TXT_ITEMC.Value = string.Empty;
				TXT_QUANT.Value = string.Empty;
				TXT_REDATE.Value = string.Empty;
				TXT_WRHS.Value = string.Empty;
				TXT_WRHSC.Value = string.Empty;
				TXT_ITEMC.Active = true;

				Load_Matrix(pForm);
				Globals.oApp.StatusBar.SetText("Successfully added details to the Light Life Log", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				//NSC_DI.UTIL.Misc.KillObject(oBar);
				GC.Collect();
			}

		}

		private static void TXT_LTITEM_ChooseFromList_Selected(Form pForm, IChooseFromListEvent pChooseFromListEvent)
		{
			try
			{
				DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

				string oItemCode = oDataTable.GetValue(0, 0).ToString();
				string oItemName = oDataTable.GetValue(1, 0).ToString();

				// Set the userdatasource for the name textbox
				pForm.DataSources.UserDataSources.Item("UDS_OITEM").ValueEx = Convert.ToString(oItemName);

				// Save the business partners code
				((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEMC", pForm))
					.Value = oItemCode;

			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private static void TXT_WRHS_ChooseFromList_Selected(Form pForm, IChooseFromListEvent pChooseFromListEvent)
		{
			DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

			try
			{
				string oWarehouseCode = oDataTable.GetValue(0, 0).ToString();
				string oWarehouseName = oDataTable.GetValue(1, 0).ToString();

				// Set the userdatasource for the name textbox
				pForm.DataSources.UserDataSources.Item("UDS_WRHS").ValueEx = Convert.ToString(oWarehouseName);

				// Save the business partners code
				((EditText) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHSC", pForm))
					.Value = oWarehouseCode;

			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}


	}
}
