﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using Matrix = NavSol.CommonUI.Matrix;

namespace NavSol.Forms.Enviromental_Controls
{
	class F_AirQuality : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_AIR_QUALITY";

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "BTN_ADDLS":
					BTN_ADDLS_Click(oForm);
					break;

				// Filter Mngt form
				case "BTN_NEWFLT":
					F_FilterMngt.FormCreate();
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
		[B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
		public virtual void OnAfterChooseFromList(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				// Warehouse ChooseFromList
				case "TXT_WRHS":
					TXT_WRHS_ChooseFromList_Selected(oForm, (SAPbouiCOM.IChooseFromListEvent)pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			PictureBox IMG_Main = null;

			try
			{
				pForm.Freeze(true);

				pForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;

				// Set the Logo image
                CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_Main", "air-quality-icon.bmp");

				pForm.Items.Item("TXT_CDATE").Specific.Value = DateTime.UtcNow.ToString("yyyyMMdd");

				pForm.Items.Item("TXT_CRBY").Specific.Value =
					NSC_DI.UTIL.SQL.GetValue<string>("SELECT MAX(CONVERT(int,[Code])) + 1 FROM [@" + NSC_DI.Globals.tAirLog + "]", "1");

				Load_Matrix(pForm);

				pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
				NSC_DI.UTIL.Misc.KillObject(IMG_Main);
				GC.Collect();
			}
		}

		public static Form FormCreate(bool pSingleInstance = true, string pCallingFormUID = null)
		{
			Form oForm = null;
			Item oItm = null;
			try
			{
                oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return null;
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		private static void TXT_WRHS_ChooseFromList_Selected(Form pForm, IChooseFromListEvent pVal)
		{
			try
			{
                if (pVal.SelectedObjects == null) return;

                DataTable oDataTable = pVal.SelectedObjects;

				string oWarehouseCode = oDataTable.GetValue(0, 0).ToString();
				string oWarehouseName = oDataTable.GetValue(1, 0).ToString();

				// Set the userdatasource for the name textbox
				pForm.DataSources.UserDataSources.Item("UDS_WRHS").ValueEx = Convert.ToString(oWarehouseName);

				// Save the business partners code
				((EditText) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHSC", pForm))
					.Value = oWarehouseCode;

			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
		}

		private static void BTN_ADDLS_Click(Form pForm)
		{
			//EditText TXT_CO2 = null;
			//EditText TXT_HUMID = null;
			//EditText TXT_TEMP = null;
			//EditText CMB_TEMP = null;
			//EditText TXT_WRHS = null;
			//EditText TXT_CDATE = null;
			//EditText TXT_WRHSC = null;

			try
			{
				// Validate that we have a CO2 value set.
				EditText TXT_CO2 = (EditText) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_CO2", pForm);
                if (pForm.Items.Item("TXT_CO2").Specific.Value == "")
				{
					TXT_CO2.Value = "";
					TXT_CO2.Active = true;
					Globals.oApp.StatusBar.SetText("Please enter in a valid CO2 value.", BoMessageTime.bmt_Short);
					return;
				}

				// Validate that we have a Humidity value set.
				EditText TXT_HUMID = (EditText) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_HUMID", pForm);
				if (string.IsNullOrEmpty(TXT_HUMID.Value))
				{
					TXT_HUMID.Value = "";
					TXT_HUMID.Active = true;
					Globals.oApp.StatusBar.SetText("Please enter a valid humidity value.", BoMessageTime.bmt_Short);
					return;
				}

				// Validate that we have a Temperature value set.
				EditText TXT_TEMP = (EditText) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_TEMP", pForm);
				if (string.IsNullOrEmpty(TXT_TEMP.Value))
				{
					TXT_TEMP.Value = "";
					TXT_TEMP.Active = true;
					Globals.oApp.StatusBar.SetText("Please enter a valid temperature.", BoMessageTime.bmt_Short);
					return;
				}

				// Validate we have a Temperature type set.
				ComboBox CMB_TEMP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_TEMP", pForm);
				if (string.IsNullOrEmpty(CMB_TEMP.Value))
				{
					CMB_TEMP.Active = true;
					Globals.oApp.StatusBar.SetText("Please select a temperature type.", BoMessageTime.bmt_Short);
					return;
				}

				// Validate we have a Warehouse set.
				EditText TXT_WRHS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHS", pForm);
				if (string.IsNullOrEmpty(TXT_WRHS.Value))
				{
					TXT_WRHS.Value = "";
					TXT_WRHS.Active = true;
					Globals.oApp.StatusBar.SetText("Please enter a Warehouse", BoMessageTime.bmt_Short);
					return;
				}

				// Grab the Date from the form UI
				EditText TXT_CDATE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_CDATE", pForm) as EditText;

				// Warehouse code
				EditText TXT_WRHSC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHSC", pForm);

				// Attempt to create the AirQualityLog 
				NSC_DI.SAP.AirQualityLog.CreateNewAirQualityLog(Globals.oApp.Company.UserName, TXT_CDATE.Value, TXT_CO2.Value, TXT_HUMID.Value, TXT_TEMP.Value,
					CMB_TEMP.Value, TXT_WRHSC.Value);

				TXT_CO2.Value = string.Empty;
				TXT_HUMID.Value = string.Empty;
				TXT_TEMP.Value = string.Empty;
				CMB_TEMP.Select(0);
				TXT_WRHS.Value = string.Empty;
				TXT_WRHSC.Value = string.Empty;
				TXT_CO2.Active = true;

				Load_Matrix(pForm);
				Globals.oApp.StatusBar.SetText("Successfully added details to the Air Quality Log", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private static void Load_Matrix(Form pForm)
        {
            try
            {
                // Load the matrix of logged air quality
				CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tAirLog, "MTX_AIRLOG", new List<Matrix.MatrixColumn>() {
                    new Matrix.MatrixColumn(){ Caption="ID", ColumnWidth=40, ColumnName="Code", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                    ,new Matrix.MatrixColumn(){ Caption="Created By", ColumnWidth=80, ColumnName="U_CreatedBy", ItemType = BoFormItemTypes.it_EDIT}               
                    ,new Matrix.MatrixColumn(){ Caption="Created On", ColumnWidth=80, ColumnName="U_CreatedOn", ItemType = BoFormItemTypes.it_EDIT}
                    ,new Matrix.MatrixColumn(){ Caption="CO2 Level", ColumnWidth=80, ColumnName="U_CO2", ItemType = BoFormItemTypes.it_EDIT}
                    ,new Matrix.MatrixColumn(){ Caption="Humidity", ColumnWidth=80, ColumnName="U_Humidity", ItemType = BoFormItemTypes.it_EDIT}
                    ,new Matrix.MatrixColumn(){ Caption="Temp.", ColumnWidth=80, ColumnName="U_Temperature", ItemType = BoFormItemTypes.it_EDIT}
                    ,new Matrix.MatrixColumn(){ Caption="Degree Type", ColumnWidth=80, ColumnName="U_DegreeType", ItemType = BoFormItemTypes.it_EDIT}
                    ,new Matrix.MatrixColumn(){ Caption="Warehouse", ColumnWidth=80, ColumnName="U_Warehouse", ItemType = BoFormItemTypes.it_EDIT}
                }, "SELECT TOP 100 * FROM [@" + NSC_DI.Globals.tAirLog + "] ORDER BY [Code] DESC");
            }
            catch(Exception ex) 
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
        }

	}
}