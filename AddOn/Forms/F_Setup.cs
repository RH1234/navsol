﻿using System;
using B1WizardBase;
using SAPbouiCOM;
using SAPbobsCOM;
using System.Linq;

namespace NavSol.Forms
{
	public class F_Setup : B1Event
    {
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_SETUP";
        public delegate void FormSaveCallback();
        public static FormSaveCallback OnFormSave;
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPressed(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = null;

            try
            {
                oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
                if (pVal.ItemUID == "1") OK_BEFORECLICK(ref BubbleEvent, pVal, oForm);
                if (pVal.ItemUID == "2") CANCEL_BEFORECLICK(ref BubbleEvent, pVal, oForm);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            { 
                NSC_DI.UTIL.Misc.KillObject(oForm);
			    GC.Collect();
            }
            return BubbleEvent;
		}

        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, false, new string[] { cFormID })]
        public virtual void OnAfterValidate(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID != "txtPassW") return;
            if (pVal.InnerEvent == true) return;

            Validate_PW(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			//switch (pVal.ItemUID)
			//{
			//	case "1":
			//		Save(oForm);
			//		break;

			//	case "2":
			//		oForm.Close();
			//		break; 

			//	//case "BTN_IMPORT":
			//	//	BTN_IMPORT_Click();
			//	//	break;
			//}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			try
			{
				pForm.Freeze(true);

                var settingsLoaded = NSC_DI.UTIL.UDO.IsFieldInTable("@" + NSC_DI.Globals.tSettings, "Description");

                // Load the micro verticle check boxes
                pForm.DataSources.UserDataSources.Add("ds1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);
				pForm.Items.Item("chkGrow").Specific.DataBind.SetBound(true, "", "ds1");
                pForm.Items.Item("chkGrow").Specific.Checked = (settingsLoaded && NSC_DI.UTIL.Settings.Value.Get("MV_Grow") == "Y");

				pForm.DataSources.UserDataSources.Add("ds2", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);
				pForm.Items.Item("chkProcess").Specific.DataBind.SetBound(true, "", "ds2");
				pForm.Items.Item("chkProcess").Specific.Checked = (settingsLoaded && NSC_DI.UTIL.Settings.Value.Get("MV_Processor") == "Y");

				pForm.DataSources.UserDataSources.Add("ds3", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);
				pForm.Items.Item("chkDistrib").Specific.DataBind.SetBound(true, "", "ds3");
				pForm.Items.Item("chkDistrib").Specific.Checked = (settingsLoaded && NSC_DI.UTIL.Settings.Value.Get("MV_Distributor") == "Y");

				pForm.DataSources.UserDataSources.Add("ds4", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);
				pForm.Items.Item("chkRetail").Specific.DataBind.SetBound(true, "", "ds4");
				pForm.Items.Item("chkRetail").Specific.Checked = (settingsLoaded && NSC_DI.UTIL.Settings.Value.Get("MV_Retail") == "Y");

				pForm.DataSources.UserDataSources.Add("ds5", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);
				pForm.Items.Item("chkCanna").Specific.DataBind.SetBound(true, "", "ds5");
				pForm.Items.Item("chkCanna").Specific.Checked = settingsLoaded ? (NSC_DI.UTIL.Settings.Value.Get("IsCannabis") == "Y") : true;
                pForm.Items.Item("chkCanna").Enabled = false;

                pForm.DataSources.UserDataSources.Add("ds6", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);
                pForm.Items.Item("chkTemplat").Specific.DataBind.SetBound(true, "", "ds6");
                pForm.Items.Item("chkTemplat").Specific.Checked = false;    // settingsLoaded ? (NSC_DI.UTIL.Settings.Value.Get("AddTemplates") == "Y") : true;

                //pForm.DataSources.UserDataSources.Add("dsCompOpt", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);
                //pForm.Items.Item("optNone").Specific.DataBind.SetBound(true, "", "dsCompOpt");
                //pForm.Items.Item("optBioTr").Specific.GroupWith("optNone");
                //pForm.Items.Item("optBioTr").Specific.DataBind.SetBound(true, "", "dsCompOpt");
                //pForm.Items.Item("optMetrc").Specific.GroupWith("optBioTr");
                //pForm.Items.Item("optMetrc").Specific.DataBind.SetBound(true, "", "dsCompOpt");
                //pForm.Items.Item("optNone").Specific.Selected = true;
                pForm.Items.Item("cboStComp").Specific.ValidValues.Add("None", "None");
                pForm.Items.Item("cboStComp").Specific.ValidValues.Add("BioTrack", "BioTrack");
                pForm.Items.Item("cboStComp").Specific.ValidValues.Add("METRC", "METRC");
                if(settingsLoaded)
                    // don't want to a default value. A state compliance must be selected before continuing
                    //pForm.Items.Item("cboStComp").Specific.Select("None", BoSearchKey.psk_ByValue);
                //else
                    pForm.Items.Item("cboStComp").Specific.Select(NSC_DI.UTIL.Settings.Value.Get("State Compliance"), BoSearchKey.psk_ByValue);

                pForm.DataSources.DataTables.Add("dsGrid");

				if (settingsLoaded) Load_Grid(pForm);
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.VisibleEx = true;
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = true, string pCallingFormUID = null)
		{
			Item oItm = null;
			Form oForm = null;
			try
			{
                oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
                {
                    oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
                    oItm.Top = -20;
                    oItm.Specific.Value = pCallingFormUID;
                }

                FormSetup(oForm);

                return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		private static void Load_Grid(Form pForm)
		{
			SAPbouiCOM.Grid oGrd = null;

			try
			{
                var sql = $"SELECT * FROM [@{NSC_DI.Globals.tSettings}]";
				oGrd = pForm.Items.Item("grdSetting").Specific;
				oGrd.DataTable = pForm.DataSources.DataTables.Item("dsGrid");
				oGrd.DataTable.ExecuteQuery(sql);
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oGrd);
				GC.Collect();
			}
		}

		private static void OK_BEFORECLICK(ref bool BubbleEvent, ItemEvent pVal, Form pForm)
        {
            if (pVal.FormMode == (int)BoFormMode.fm_UPDATE_MODE)
            { 
                Save(pForm);
                try
                {
                    AddOn.InitializeComponents();
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show("Error in loading. Closing Add-On." + Environment.NewLine + NSC_DI.UTIL.Message.Format(ex.Message));
                    System.Windows.Forms.Application.Exit();
                }
            }
        }

        private static void CANCEL_BEFORECLICK(ref bool BubbleEvent, ItemEvent pVal, Form pForm)
        {
            if (pVal.FormMode == (int) BoFormMode.fm_UPDATE_MODE)
            {
                //BubbleEvent = false;
                //Globals.oApp.MessageBox("Cannot cancel setup because it hasn't yet been updated.");
                Globals.oApp.SetStatusBarMessage("Viridian setup canceled.", BoMessageTime.bmt_Long, true);
                System.Windows.Forms.Application.Exit();
            }
        }

        private static void Validate_PW(Form pForm)
        {
            try
            {
                var pw = pForm.Items.Item("txtPassW").Specific.Value;
                if (pw != "NavVerSci") return;

                pForm.Items.Item("1").Enabled = true;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void Save(Form pForm)
        {
            try
            {
                var grow        = pForm.Items.Item("chkGrow").Specific.Checked ? "Y" : "N";
                var processor   = pForm.Items.Item("chkProcess").Specific.Checked ? "Y" : "N";
                var distributor = pForm.Items.Item("chkDistrib").Specific.Checked ? "Y" : "N";
                var retail      = pForm.Items.Item("chkRetail").Specific.Checked ? "Y" : "N";
                var cannabis    = pForm.Items.Item("chkCanna").Specific.Checked ? "Y" : "N";

                var addTemplates = pForm.Items.Item("chkTemplat").Specific.Checked ? "Y" : "N";
                var compliance  = pForm.Items.Item("cboStComp").Specific.Value;

                AddOn.InitSettings();

                AddOn.WaitForSettingsToComplete();

                NSC_DI.UTIL.Settings.Value.Set("MV_Grow"        , grow);
                NSC_DI.UTIL.Settings.Value.Set("MV_Processor"   , processor);
                NSC_DI.UTIL.Settings.Value.Set("MV_Distributor" , distributor);
                NSC_DI.UTIL.Settings.Value.Set("MV_Retail"      , retail);
                NSC_DI.UTIL.Settings.Value.Set("IsCannabis"     , cannabis);
                NSC_DI.UTIL.Settings.Value.Set("AddTemplates"   , addTemplates);
                NSC_DI.UTIL.Settings.Value.Set("State Compliance", compliance, "State Compliance System");
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void mtx_plan_Matrix_Link_Pressed(Form pForm, ItemEvent pVal)
		{
			if (pVal.Row <= 0) return;

			SAPbouiCOM.Matrix oMat = null;

			try
			{
				oMat = pForm.Items.Item("mtx_plan").Specific;
				var val = oMat.GetCellSpecific(0, pVal.Row).Value.ToString();
				F_StrainInfo.FormCreate_Find(val);
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oMat);
				GC.Collect();
			}
		}

		private static string AskForFile()
		{
			try
			{
				var ofd = new System.Windows.Forms.OpenFileDialog();
				var y = ofd.ShowDialog();

				return ofd.FileName;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
		}

	}
}