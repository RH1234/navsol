﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using B1WizardBase;
using NavSol.CommonUI;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Drying_and_Curing
{
	class F_ProductionDryBudVM : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID        = "NSC_PVDRY";
        private const string cItemType      = "'Dry Cannabis'";                                 // #3600 - filter for NON wet processing
        private const string cItemTypeWet   = "'Dry Cannabis','Dry Unb Flower','Dry Unb Trim'"; // #3600 - filter for wet processing
        private const string cItemProperty  = "Dry Cannabis";

        private static System.Data.DataTable _dtByProd = new System.Data.DataTable("ByProd");

        private enum MatrixColumns { DocNum, ItemName, ItemCode, PlannedQuantity, CreateDate, Warehouse, DestinationWarehouse, DocEnty, MotherID, GroupNum, DryCannabis, StateID, HarvName, BatchID, RcptItemCode }
        public static SAPbouiCOM.Form _SBO_Form;

        protected class DriedBudDataTransfer
        {
            public List<DriedBudSelectedRow> RowsToTransfer = new List<DriedBudSelectedRow>();
        }

        protected class DriedBudSelectedRow
        {
            public int ProductionKey;
            public string ItemCode;
            public string MotherId;
            public string SourceWarehouseCode;
            public string DestinationWarehouseCode;
            public string FirstBillOfMaterialItemCode;

            public string GroupName { get; set; }
            public string Quantity { get; set; }


            public List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> ListOfGoodsReceiptLines = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();
        }
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            switch (pVal.ItemUID)
			{
				case "BTN_LOOKUP":
					BTN_LOOKUP_ITEM_PRESSED(oForm);
					break;

				case "BTN_APPLY":
                    BTN_APPLY_PRESSED_NEW(oForm);
                    SAPbouiCOM.EditText oEdit = _SBO_Form.Items.Item("TXT_REM").Specific;
                    oEdit.Value = "";
                    break;

				case "MTX_ITEMS":
					MTX_ITEMS_Click(oForm, pVal);
                    SetRemarks();
                    break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "MTX_ITEMS":
					MAIN_MATRIX_LINK_PRESSED(oForm, pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		#region ---------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			try
			{
				pForm.Freeze(true);

				CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_MAIN", @"drying-icon.bmp");
				
				Load_Matrix(pForm);

                // Center the form on the screen
                //pForm.Width			= Convert.ToInt32(Globals.oApp.Desktop.Width * .5m);
                //pForm.ClientWidth	= Convert.ToInt32(Globals.oApp.Desktop.Width * .5m);
                //pForm.Top			= Convert.ToInt32(Globals.oApp.Desktop.Height * .2m);
                //pForm.Left			= Convert.ToInt32(Globals.oApp.Desktop.Width * .25m);

                //pForm.Items.Item("MTX_ITEMS").Specific.SelectionMode = BoMatrixSelect.ms_Auto;
                //pForm.Items.Item("MTX_ITEMS").Width = Convert.ToInt32(Globals.oApp.Desktop.Width * .5m) - 100;

                pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);

				return oForm;
			}
			catch (Exception ex)
			{
                //Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                throw ex;
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}
        private void SetRemarks()
        {
            try
            {
                //Get the Remarks Textbox
                SAPbouiCOM.EditText oEdit = _SBO_Form.Items.Item("TXT_REM").Specific;
                // Grab the Matrix from the form 
                SAPbouiCOM.Matrix matrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", _SBO_Form);
                //Get selected production order
                string ProdNumder = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Production Order");

                string Remarks = NSC_DI.UTIL.SQL.GetValue<string>(@"Select OWOR.Comments from OWOR where OWOR.DocNum=" + ProdNumder);
                oEdit.Value = Remarks;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox("Please Select The Production Order");
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void Load_AvailableActions(Form pForm)
		{
			ButtonCombo ComboBox_Action = null;
			try
			{
				ComboBox_Action = pForm.Items.Item("CBT_ACTION").Specific;

				// If items already exist in the drop down
				if (ComboBox_Action.ValidValues.Count > 0)
				{
					// Remove all currently existing values from warehouse drop down
					for (int i = ComboBox_Action.ValidValues.Count; i-- > 0;)
					{
						ComboBox_Action.ValidValues.Remove(i, BoSearchKey.psk_Index);
					}
				}

				//CBT_ACTION.ValidValues.Add("RECEIPT", "Receipt Items");
				ComboBox_Action.ValidValues.Add("RECEIPT_CLOSE", "Receipt & Close");
				ComboBox_Action.Select("RECEIPT_CLOSE");

				//CBT_ACTION.Caption = "Actions";
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(ComboBox_Action);
				GC.Collect();
			}
		}

		private static void Load_Matrix(Form pForm)
		{
			try
            {
                pForm.Select();
                pForm.Freeze(true);
                // Prepare a list of columns to go into the 
                List<CommonUI.Matrix.MatrixColumn> ListOfMatrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
                {
                    new CommonUI.Matrix.MatrixColumn() {ColumnName = "DocNum", Caption = "Production Order", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
                    new CommonUI.Matrix.MatrixColumn() {ColumnName = "itemName", Caption = "Item Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                    new CommonUI.Matrix.MatrixColumn() {ColumnName = "itemCode", Caption = "Item Code", ColumnWidth = 0, ItemType = BoFormItemTypes.it_EDIT},
                    new CommonUI.Matrix.MatrixColumn() {ColumnName = "PlannedQty", Caption = "Original Wet Wgt", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                    //new CommonUI.Matrix.MatrixColumn() { ColumnName="CmpltQty", Caption="Completed", ColumnWidth=80, ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT  },
					new CommonUI.Matrix.MatrixColumn() {ColumnName = "CreateDate", Caption = "Began Drying", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                    new CommonUI.Matrix.MatrixColumn() {ColumnName = "Warehouse", Caption = "Warehouse ID", ColumnWidth = 0, ItemType = BoFormItemTypes.it_EDIT},
                    new CommonUI.Matrix.MatrixColumn() {ColumnName = "destinationWarehouse", Caption = "Destination Warehouse", ColumnWidth = 0, ItemType = BoFormItemTypes.it_EDIT},
                    new CommonUI.Matrix.MatrixColumn() {ColumnName = "DocEntry", Caption = "DocEntry", ColumnWidth = 0, ItemType = BoFormItemTypes.it_EDIT},
                    new CommonUI.Matrix.MatrixColumn() {ColumnName = "MotherID", Caption = "MotherID", ColumnWidth = 0, ItemType = BoFormItemTypes.it_EDIT},
                    new CommonUI.Matrix.MatrixColumn() {ColumnName = "GroupNum", Caption = "Harvest Group", ColumnWidth = 20, ItemType = BoFormItemTypes.it_EDIT},
                    new CommonUI.Matrix.MatrixColumn() {ColumnName = null, Caption = "Dry Cannabis", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT, IsEditable = true},
                    new CommonUI.Matrix.MatrixColumn(){ Caption="State Id",         ColumnWidth=80, ColumnName="StateID", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true},
                    new CommonUI.Matrix.MatrixColumn(){ Caption="Harvest Name", ColumnName="HarvName", ColumnWidth=80, ItemType=BoFormItemTypes.it_EDIT},
                    new CommonUI.Matrix.MatrixColumn() {ColumnName = "BatchID", Caption = "Batch", ColumnWidth = 40, ItemType = BoFormItemTypes.it_EDIT},
                    new CommonUI.Matrix.MatrixColumn(){ColumnName = "RcptItemCode", Caption = "Receipt Item Code", ColumnWidth = 0, ItemType = BoFormItemTypes.it_EDIT}

                };


                // do not get multiple columns - just hard code 1 column
                //IEnumerable<string> names = GetItemNames();

                //foreach (var name in names)
                //{
                //ListOfMatrixColumns.Add(new CommonUI.Matrix.MatrixColumn()
                //{
                //    ColumnName = null,
                //    Caption = "Dry Cannabis",    //name,
                //    ColumnWidth = 80,
                //    ItemType = BoFormItemTypes.it_EDIT,
                //    IsEditable = true
                    //});

                    //}

                    string filter = NSC_DI.UTIL.Options.Value.GetSingle("Wet Processing") == "Y" ? cItemTypeWet : cItemType;

                // Load "Dry Cannabis" disassembly production orders into the matrix
                //CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "OWOR", "MTX_ITEMS", ListOfMatrixColumns, @"
                //SELECT [OWOR].[DocEntry], [OWOR].[DocNum], [OITM].[ItemName], [OWOR].[ItemCode], [OWOR].[PlannedQty], [OWOR].[CmpltQty], [OWOR].[CreateDate], [OWOR].[Warehouse],0.0 AS [WasteQty],
                //       (SELECT TOP 1 [WOR1].[wareHouse] 
                //          FROM [WOR1] 
                //         WHERE [WOR1].[DocEntry] = [OWOR].[DocNum] ) AS [destinationWarehouse],
                //       (SELECT TOP 1 [OBTN].[U_NSC_MotherID]
                //          FROM [IBT1]
                //          JOIN [OBTN] ON [OBTN].[DistNumber] = [IBT1].[BatchNum] 
                //          JOIN [IGE1] ON [IGE1].[DocEntry] = [IBT1].[BaseEntry]
                //         WHERE [IBT1].[BaseType] = '60'
                //           AND [IGE1].[BaseRef] = [OWOR].[DocNum]) AS MotherID,
                //       (SELECT TOP 1 [OBTN].[U_NSC_GroupNum]
                //          FROM [IBT1]
                //          JOIN [OBTN] ON [OBTN].[DistNumber] = [IBT1].[BatchNum] 
                //          JOIN [IGE1] ON [IGE1].[DocEntry] = [IBT1].[BaseEntry]
                //         WHERE [IBT1].[BaseType] = '60'
                //           AND [IGE1].[BaseRef] = [OWOR].[DocNum]) AS [GroupNum],
                //         '                                                                       ' as [StateID]
                //  FROM [OWOR]
                //  JOIN [OITM] ON [OITM].[ItemCode] = [OWOR].[ItemCode]
                //  JOIN [OITB] ON [OITB].[ItmsGrpCod] = [OITM].[ItmsGrpCod]
                // WHERE [OWOR].[Status] = 'R'
                //   AND [OITB].[ItmsGrpNam] in (" + filter + @")
                // ORDER BY CONVERT(int, [OWOR].[DocNum]) DESC ");

//                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "OWOR", "MTX_ITEMS", ListOfMatrixColumns, @"
//                SELECT [OWOR].[DocEntry], [OWOR].[DocNum], [OITM].[ItemName], [OWOR].[ItemCode], [OWOR].[PlannedQty], [OWOR].[CmpltQty], [OWOR].[CreateDate], [OWOR].[Warehouse],0.0 AS [WasteQty],
//                       (SELECT TOP 1 [WOR1].[wareHouse] 
//                          FROM [WOR1] 
//                         WHERE [WOR1].[DocEntry] = [OWOR].[DocNum] ) AS [destinationWarehouse],
//                       (SELECT TOP 1 [OBTN].[U_NSC_MotherID]
//                          FROM [OBTN]
//                          JOIN [ITL1] ON [OBTN].ItemCode = [ITL1].ItemCode AND [OBTN].SysNumber = [ITL1].SysNumber  
//                          JOIN [OITL] ON [ITL1].[LogEntry] = [OITL].[LogEntry]
//                          JOIN [IGE1] ON [IGE1].[DocEntry] = [OITL].[BaseEntry]
//                         WHERE [OITL].[BaseType] = '60'
//                           AND [IGE1].[BaseRef] = [OWOR].[DocNum]) AS MotherID,
//                       (SELECT TOP 1 [OBTN].[U_NSC_GroupNum]
//                          FROM [OBTN]
//                          JOIN [ITL1] ON [OBTN].ItemCode = [ITL1].ItemCode AND [OBTN].SysNumber = [ITL1].SysNumber  
//                          JOIN [OITL] ON [ITL1].[LogEntry] = [OITL].[LogEntry]
//                          JOIN [IGE1] ON [IGE1].[DocEntry] = [OITL].[BaseEntry]
//                         WHERE [OITL].[BaseType] = '60'
//                           AND [IGE1].[BaseRef] = [OWOR].[DocNum]) AS [GroupNum],
//                         'MnfSerial' as [StateID],
//'     ' as BatchID
//                  FROM [OWOR]
//                  JOIN [OITM] ON [OITM].[ItemCode] = [OWOR].[ItemCode]
//                  JOIN [OITB] ON [OITB].[ItmsGrpCod] = [OITM].[ItmsGrpCod]
//                 WHERE [OWOR].[Status] = 'R'
//                   AND [OITB].[ItmsGrpNam] in (" + filter + @")
//                 ORDER BY CONVERT(int, [OWOR].[DocNum]) DESC ");

                var sql = $@"
SELECT OWOR.DocEntry, OWOR.DocNum, OITM.ItemName, OWOR.ItemCode, OWOR.PlannedQty, OWOR.CmpltQty, OWOR.CreateDate, OWOR.Warehouse, 0.0 AS WasteQty, 
       WOR1.wareHouse AS destinationWarehouse,
	   OBTN.U_NSC_MotherID AS [MotherID], OBTN.U_NSC_GroupNum AS [GroupNum], OBTN.MnfSerial AS [StateID], OBTN.DistNumber AS [BatchID], OBTN.ItemCode AS [RcptItemCode], OBTN.U_NSC_HarvestName AS [HarvName]
  FROM OWOR
  JOIN OITM ON OITM.ItemCode   = OWOR.ItemCode
  JOIN OITB ON OITB.ItmsGrpCod = OITM.ItmsGrpCod
  JOIN WOR1 ON OWOR.DocEntry   = WOR1.DocEntry AND WOR1.BaseQty > 0
  JOIN IGE1 ON OWOR.DocNum     = IGE1.BaseRef
  JOIN OITL ON IGE1.DocEntry   = OITL.DocEntry AND OITL.ApplyType = 60
  JOIN ITL1 ON OITL.LogEntry   = ITL1.LogEntry
  JOIN OBTN ON OBTN.ItemCode   = ITL1.ItemCode AND OBTN.SysNumber = ITL1.SysNumber
  JOIN [OWHS] ON [OWHS].[WhsCode] = [OWOR].[Warehouse]
 WHERE OWOR.Status = 'R' AND OITB.ItmsGrpNam IN ({filter})";
                if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})"; //10823-2                 

                sql += $@"ORDER BY CONVERT(int, OWOR.DocNum) DESC";
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "OWOR", "MTX_ITEMS", ListOfMatrixColumns, sql);

                pForm.Items.Item("MTX_ITEMS").Specific.AutoResizeColumns();

                pForm.Items.Item("MTX_ITEMS").Specific.SelectionMode = BoMatrixSelect.ms_Single;

                // Update the combo button box options available actions are
                Load_AvailableActions(pForm);
            }
            catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
                pForm.Freeze(false);

                GC.Collect();
			}
		}

        private static IEnumerable<string> GetItemNames()
        {
            if (NSC_DI.UTIL.Settings.Value.Get("Use Template Items") == "Y") return NSC_DI.SAP.Items.GetItemNames(cItemProperty);
            else return GetItemNamesOLD().Select().Select(n => n["ItemName"] as string);
        }

        private static System.Data.DataTable GetItemNamesOLD()
        {
            string filter = NSC_DI.UTIL.Options.Value.GetSingle("Wet Processing") == "Y" ? cItemTypeWet : cItemType;

            var sql = "";
            sql += "SELECT DISTINCT REPLACE(U_ItemName, '[StrainName] - ', '') AS ItemName"; 
            sql += "  FROM [@" + NSC_DI.Globals.tAutoItem + "]";
            //sql += "  JOIN [@" + NSC_DI.Globals.tAutoItemBOM + "] ON [@" + NSC_DI.Globals.tAutoItem + "].U_ItemCode = [@" + NSC_DI.Globals.tAutoItemBOM + "].U_ItemCode";
            //sql += " WHERE U_ItemGroupCode = " + NSC_DI.SAP.ItemGroups.GetSAPItemGroupCodeFromGroupType(itemGroupType);
            sql += " WHERE U_ItemCode IN (SELECT b.U_ItemCode";
            sql += " FROM  [@" + NSC_DI.Globals.tAutoItemBOM + "] AS b";
            sql += " JOIN [@" + NSC_DI.Globals.tAutoItem + "] AS i ON i.U_ItemCode = b.U_ParentItem";
            sql += " JOIN [OITB] AS o ON o.ItmsGrpCod = i.U_ItemGroupCode";
            sql += " WHERE o.ItmsGrpNam = '" + filter + "' AND b.U_ItemCode IS NOT NULL)";
            var dt = NSC_DI.UTIL.SQL.DataTable(sql);
            return dt;
        }

        private static void BTN_APPLY_PRESSED_NEW(Form pForm)
        {
            // Grab the ComboButton from the form UI
            SAPbouiCOM.Matrix oMat = null;
            SAPbobsCOM.ProductionOrders oPdO = null;
            SAPbobsCOM.Documents oRcv = null;

           try
            {
                
                if (pForm.Items.Item("CBT_ACTION").Specific.Selected == null)
                {
                    Globals.oApp.StatusBar.SetText("Please select an action!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    return;
                }

                // Grab the Matrix from the form 
                oMat = pForm.Items.Item("MTX_ITEMS").Specific;
                var lineNum = -1;
                var indx = oMat.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);
                if (indx < 0)
                {
                    Globals.oApp.StatusBar.SetText("Please select at least one row!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                    throw new System.ComponentModel.WarningException();
                }

                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();


                //Globals.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                oPdO = NSC_DI.SAP.ProductionOrder.OpenPdO(Convert.ToInt32(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.DocNum).UniqueID, indx).Value));
                //Set the Doc Num so it can be passed to the delegate. 
                int intDocNum = oPdO.DocumentNumber;

                //Set the Qty so it can be passed to the delegate.  
                double dblQty = Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.DryCannabis).UniqueID, indx).Value);

                //Set the StateID to pass tot he delegate 
                string strStateID  = Convert.ToString(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.StateID).UniqueID, indx).Value);

                //Inter-Company Check 
                //string InterCoSubCode = NSC_DI.UTIL.SQL.GetValue<string>($@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 where T0.WhsCode = '{oPdO.Warehouse}'"); DELETE 12/30/2020
                //string InterCo = NSC_DI.UTIL.SQL.GetValue<string>("select T0.U_Value from dbo.[@NSC_SETTINGS] T0 where T0.Name = 'Inter-Company'");

                //Check if Byproducts Exist
                var batchNum = oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.BatchID).UniqueID, indx).Value;
                var rcptItemCode = oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.RcptItemCode).UniqueID, indx).Value;

                int NoOfByProducts = NSC_DI.UTIL.SQL.GetValue<int>("select Count(WOR1.ItemCode) from WOR1 where WOR1.DocEntry = " + oPdO.DocumentNumber + " and WOR1.PlannedQty < 0 and WOR1.IssueType='M'");

                var batchNumDet = NSC_DI.SAP.BatchItems.GetInfo(batchNum, rcptItemCode);

                if (NoOfByProducts > 0)
                {
                    int iReturnVal = Globals.oApp.MessageBox("There are ByProducts to Report. What would you like to do?", 2, " Assign Values ", " Cancel ");

                    switch (iReturnVal)
                    {
                        case 1:
                            //set Results to DT 

                            //Add Columns
                            if (_dtByProd.Columns.Count == 0)
                            {
                                _dtByProd.Columns.Add("ItemCode");
                                _dtByProd.Columns.Add("ItemName");
                                _dtByProd.Columns.Add("PlannedQty");
                                _dtByProd.Columns.Add("wareHouse");
                                _dtByProd.Columns.Add("LineNum");
                                _dtByProd.Columns.Add("BatchID");
                                _dtByProd.Columns.Add("StateID");


                            }
                            else
                            {
                                _dtByProd.Clear();
                            }

                            var pFormID = _SBO_Form.UniqueID; // Save unique id to get form in callback
                            SAPbouiCOM.DataTable dtByProd = null;

                            try
                            {

                                if (_SBO_Form.DataSources.DataTables.Item("BySrc").IsEmpty == false)
                                {
                                    _SBO_Form.DataSources.DataTables.Item("BySrc").Clear();
                                    dtByProd = _SBO_Form.DataSources.DataTables.Item("BySrc");
                                }
                            }
                            catch
                            {
                                dtByProd = _SBO_Form.DataSources.DataTables.Add("BySrc");
                            }

                            string strSQL = @"select WOR1.ItemCode,OITM.ItemName, WOR1.PlannedQty,SPACE(10) AS Warehouse,WOR1.LineNum
                                                ,'                                                         ' as [BatchID]
                                                ,'                                                         ' as [StateID] 
                                                    from WOR1
                                                    join OITM on WOR1.ItemCode = OITM.ItemCode
                                                    where WOR1.DocEntry = " + oPdO.DocumentNumber + " and WOR1.PlannedQty < 0 " +
                                                " and WOR1.IssueType='M'";

                            dtByProd.ExecuteQuery(strSQL);

                            //Int to hold the production order number ofr delegate
                            int strProdOrderNumber = oPdO.DocumentNumber;
                            int byProductsSet = 0;
                            // open the form; ByProduct Planned Quantity must be set
                            F_ByProdSelect.FormCreate(pFormID, dtByProd);
                            string strDelegateState = "Fail";


                            F_ByProdSelect.ByProdsCB = delegate (string callingFormUid, System.Data.DataTable pdtByProd)
                            {
                                Form oForm = null;

                                oForm = Globals.oApp.Forms.Item(pFormID) as Form;
                                
                                //if (pdtByProd != null && pdtByProd.Rows.Count == 0)
                                //{
                                //    //Change the status of the production order.
                                //    while (indx >= 0)
                                //    {
                                //        oPdO = NSC_DI.SAP.ProductionOrder.OpenPdO(Convert.ToInt32(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.DocNum).UniqueID, indx).Value));
                                //        double dblPlannedQty = oPdO.PlannedQuantity;
                                //        double dblTotalReceivedQty = 0.00;

                                //        oRcv = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                                //        var rawMatlBatch = NSC_DI.SAP.ProductionOrder.GetBatchNumberFromIssue(oPdO.DocumentNumber, null, null);
                                //        var rawBatchRec = NSC_DI.SAP.BatchItems.GetInfo(rawMatlBatch);

                                //        //-------------------------
                                //        // RECEIVE THE REMAINING MANUAL ITEMS.
                                //        //-------------------------
                                       
                                //        // PARENT
                                //        var qty = Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.DryCannabis).UniqueID, indx).Value);
                                       
                                //        if (qty > 0)
                                //        {
                                //            //var subBatch = NSC_DI.UTIL.AutoStrain.NextBatch(oPdO.ItemNo, InterCoSubCode);
                                //            //subBatch = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, subBatch);
                                //            var subBatch = NSC_DI.SAP.BatchItems.NextBatch(oPdO.ItemNo);

                                //            oRcv.Lines.BaseType                              = 0;
                                //            oRcv.Lines.BaseEntry                             = oPdO.DocumentNumber;
                                //            oRcv.Lines.TransactionType                       = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                                //            oRcv.Lines.WarehouseCode                         = oPdO.Warehouse;
                                //            oRcv.Lines.Quantity                              = qty;
                                //            dblTotalReceivedQty                              = oRcv.Lines.Quantity;//Qty Holder for validation that total is lessed than planned
                                //            oRcv.Lines.BatchNumbers.BatchNumber              = subBatch;
                                //            oRcv.Lines.BatchNumbers.Quantity                 = oRcv.Lines.Quantity;
                                //            oRcv.Lines.BatchNumbers.ManufacturerSerialNumber = strStateID.Trim();
                                           
                                //            // copy and set batch UDFs
                                //            NSC_DI.UTIL.UDO.CopyUDFs(oRcv.Lines.BatchNumbers, rawBatchRec);

                                //            var baseQty = oRcv.Lines.Quantity;

                                //            // set the Branch
                                //            var br = NSC_DI.SAP.Branch.Get(oRcv.Lines.WarehouseCode);
                                //            if (br > 0) oRcv.BPL_IDAssignedToInvoice = br;
                                //        }

                                //        //Validate the sum of products to create is not greater than the Planned amount
                                //        if (dblTotalReceivedQty > dblPlannedQty)
                                //        {
                                //            Globals.oApp.MessageBox("Your Quantity Received is Greater than Planned. Please adjust your quantities.");
                                //            return;//Rollback transaction.
                                //        }

                                //        // add the receipt
                                //        if (oRcv.Add() != 0)
                                //        {
                                //            Globals.oApp.MessageBox("Failed to Receipt from Production for batch " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                                //            throw new System.ComponentModel.WarningException();
                                //        }


                                //        indx = oMat.GetNextSelectedRow(indx, BoOrderType.ot_RowOrder);
                                //    }

                                //    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                                //    NSC_DI.SAP.ProductionOrder.UpdateStatus(strProdOrderNumber, SAPbobsCOM.BoProductionOrderStatusEnum.boposClosed);
                                //    return;
                                //}
                                //else 
                                if (pdtByProd != null)// && pdtByProd.Rows.Count > 0)
                                {
                                    if (pdtByProd.Rows.Count > 0)
                                    {
                                        //Report Byproducts
                                        while (indx >= 0)
                                        {
                                            //oPdO = NSC_DI.SAP.ProductionOrder.OpenPdO(Convert.ToInt32(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.DocNum).UniqueID, indx).Value));
                                            oPdO = NSC_DI.SAP.ProductionOrder.OpenPdO(intDocNum);
                                            double dblPlannedQty = oPdO.PlannedQuantity;
                                            double dblTotalReceivedQty = 0.00;

                                            oRcv = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                                            var rawMatlBatch = NSC_DI.SAP.ProductionOrder.GetBatchNumberFromIssue(oPdO.DocumentNumber, null, null);
                                            var rawBatchRec = NSC_DI.SAP.BatchItems.GetInfo(rawMatlBatch);

                                            //-------------------------
                                            // RECEIVE THE REMAINING MANUAL ITEMS.
                                            //-------------------------
                                            // PARENT
                                            //var qty = Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.DryCannabis).UniqueID, indx).Value);

                                            if (dblQty > 0)
                                            {
                                                //var subBatch = NSC_DI.UTIL.AutoStrain.NextBatch(oPdO.ItemNo, InterCoSubCode);
                                                //subBatch = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, subBatch);
                                                var subBatch = NSC_DI.SAP.BatchItems.NextBatch(oPdO.ItemNo);

                                                oRcv.Lines.BaseType = 0;
                                                oRcv.Lines.BaseEntry = oPdO.DocumentNumber;
                                                oRcv.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                                                oRcv.Lines.WarehouseCode = oPdO.Warehouse;
                                                oRcv.Lines.Quantity = dblQty;
                                                dblTotalReceivedQty = oRcv.Lines.Quantity;//Qty Holder for validation that total is lessed than planned
                                                oRcv.Lines.BatchNumbers.BatchNumber = subBatch;
                                                oRcv.Lines.BatchNumbers.Quantity = oRcv.Lines.Quantity;
                                                oRcv.Lines.BatchNumbers.ManufacturerSerialNumber = strStateID.Trim();

                                                // copy and set batch UDFs
                                                NSC_DI.UTIL.UDO.CopyUDFs(oRcv.Lines.BatchNumbers, rawBatchRec);
                                                var baseQty = oRcv.Lines.Quantity;
                                            }

                                            //Validate the sum of products to create is not greater than the Planned amount
                                            if (dblTotalReceivedQty > dblPlannedQty)
                                            {
                                                Globals.oApp.MessageBox("Your Quantity Received is Greater than Planned. Please adjust your quantities.");
                                                return;//Rollback transaction.
                                            }

                                            // add the receipt
                                            if (oRcv.Add() != 0)
                                            {
                                                Globals.oApp.MessageBox("Failed to Receipt from Production for batch " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                                                throw new System.ComponentModel.WarningException();
                                            }


                                            indx = -1;
                                        }

                                        foreach (System.Data.DataRow dr in pdtByProd.Rows)
                                        {
                                            if (double.Parse(dr["PlannedQty"].ToString()) <= 0) continue;
                                            _dtByProd.Rows.Add(dr["ItemCode"], dr["ItemName"], dr["PlannedQty"], dr["Warehouse"], dr["LineNum"], NSC_DI.SAP.BatchItems.NextBatch(dr["ItemCode"].ToString()), dr["StateID"]);
                                        }

                                        NSC_DI.SAP.ProductionOrder.ReceiveByProducts(strProdOrderNumber, _dtByProd, batchNumDet);

                                        //Change the status of the production order.
                                        NSC_DI.SAP.ProductionOrder.UpdateStatus(strProdOrderNumber, SAPbobsCOM.BoProductionOrderStatusEnum.boposClosed);
                                    }
                                    var oFormCB = B1Connections.theAppl.Forms.GetForm(cFormID, 0);
                                    //oFormCB.Select();
                                    Load_Matrix(oFormCB);
                                    return;
                                }
                                else
                                {
                                    //Cancel button was hit on the delegate 
                                    Globals.oApp.MessageBox("Please report byproducts or remove them from the production order to continue.");
                                    return;
                                }
                            
                            };

                        break;
                        case 2:
                            if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                            Load_Matrix(pForm);
                            return;
                    }
                }
                else
                {
                    while (indx >= 0)
                    {
                        oPdO = NSC_DI.SAP.ProductionOrder.OpenPdO(Convert.ToInt32(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.DocNum).UniqueID, indx).Value));
                        double dblPlannedQty = oPdO.PlannedQuantity;
                        double dblTotalReceivedQty = 0.00;

                        oRcv = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);

                        var rawMatlBatch = NSC_DI.SAP.ProductionOrder.GetBatchNumberFromIssue(oPdO.DocumentNumber, null, null);
                        var rawBatchRec = NSC_DI.SAP.BatchItems.GetInfo(rawMatlBatch);

                        //-------------------------
                        // RECEIVE THE REMAINING MANUAL ITEMS.
                        //-------------------------
                        // PARENT
                        var qty = Convert.ToDouble(oMat.GetCellSpecific(oMat.Columns.Item(MatrixColumns.DryCannabis).UniqueID, indx).Value);
                        if (qty > 0)
                        {
                            //var subBatch = NSC_DI.UTIL.AutoStrain.NextBatch(oPdO.ItemNo, InterCoSubCode);
                            //subBatch = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, subBatch);
                            var subBatch = NSC_DI.SAP.BatchItems.NextBatch(oPdO.ItemNo);

                            oRcv.Lines.BaseType = 0;
                            oRcv.Lines.BaseEntry = oPdO.DocumentNumber;
                            oRcv.Lines.TransactionType = SAPbobsCOM.BoTransactionTypeEnum.botrntComplete;
                            oRcv.Lines.WarehouseCode = oPdO.Warehouse;
                            oRcv.Lines.Quantity = qty;
                            dblTotalReceivedQty = oRcv.Lines.Quantity;//Qty Holder for validation that total is lessed than planned
                            oRcv.Lines.BatchNumbers.BatchNumber = subBatch;
                            oRcv.Lines.BatchNumbers.Quantity = oRcv.Lines.Quantity;
                            oRcv.Lines.BatchNumbers.ManufacturerSerialNumber = strStateID.Trim();
                            // copy and set batch UDFs
                            NSC_DI.UTIL.UDO.CopyUDFs(oRcv.Lines.BatchNumbers, rawBatchRec);

                            var baseQty = oRcv.Lines.Quantity;

                            // set the Branch
                            var br = NSC_DI.SAP.Branch.Get(oRcv.Lines.WarehouseCode);
                            if (br > 0) oRcv.BPL_IDAssignedToInvoice = br;
                        }

                        //Validate the sum of products to create is not greater than the Planned amount
                        if (dblTotalReceivedQty > dblPlannedQty)
                        {
                            Globals.oApp.MessageBox("Your Quantity Received is Greater than Planned. Please adjust your quantities.");
                            return;//Rollback transaction.
                        }

                        // add the receipt
                        if (oRcv.Add() != 0)
                        {
                            Globals.oApp.MessageBox("Failed to Receipt from Production for batch " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                            throw new System.ComponentModel.WarningException();
                        }


                        indx = oMat.GetNextSelectedRow(indx, BoOrderType.ot_RowOrder);
                    }
                    NSC_DI.SAP.ProductionOrder.UpdateStatus(oPdO.DocumentNumber, SAPbobsCOM.BoProductionOrderStatusEnum.boposClosed);
                    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);

                    Load_Matrix(pForm);
                }
               
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack);
                NSC_DI.UTIL.Misc.KillObject(oMat);
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                NSC_DI.UTIL.Misc.KillObject(oRcv);
                GC.Collect();
            }
        }

        private static DriedBudDataTransfer GetSelectedItemsFromMatrix(Form pForm)
        {
            DriedBudDataTransfer driedBudTransferData = new DriedBudDataTransfer();

            // Grab the matrix from the form ui
            SAPbouiCOM.Matrix Matrix_Items = null;
            try
            { 
                Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;

                int incrementBatch = 0;
                // For each row already selected
                for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                {
                    DriedBudSelectedRow newRow = new DriedBudSelectedRow();
                    try
                    {
                        // If the current row is now selected we can just continue looping.
                        if (!Matrix_Items.IsRowSelected(i)) continue;


                        newRow.ProductionKey = Int32.Parse(((EditText)Matrix_Items.Columns.Item(MatrixColumns.DocNum).Cells.Item(i).Specific).Value);
                        newRow.ItemCode = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.ItemCode).Cells.Item(i).Specific).Value;
                        newRow.MotherId = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.MotherID).Cells.Item(i).Specific).Value;
                        newRow.SourceWarehouseCode = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.Warehouse).Cells.Item(i).Specific).Value;
                        newRow.DestinationWarehouseCode = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.DestinationWarehouse).Cells.Item(i).Specific).Value;
					    newRow.FirstBillOfMaterialItemCode = NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(newRow.ItemCode).FirstOrDefault().ItemCode.ToString();

                        newRow.GroupName = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.GroupNum).Cells.Item(i).Specific).Value;

                        // Get a list of items that are automatically created when a new strain is created.
                        //var dt_Items = GetItemNames();
                        //var itemsAndEnteredWeight = from System.Data.DataRow dr in dt_Items.Rows
                        //                            select new
                        //                            {
                        //                                ItemName = dr[0] as string,
                        //                                Quantity = Convert.ToDouble(Matrix_Items.GetCellSpecific(dr[0], i))
                        //                            };
                    
                        // Prepare a list of "Goods Receipt" line items
                        //List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> ListOfGoodsReceiptLines = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();

                        //// For each "Bill of Material" item from the main item in the "Production Order"
                        //foreach (var BomItemCode in NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(itemCode: newRow.ItemCode))
                        //{
                        //    // Get item name of item code
                        //    string ItemName = NSC_DI.SAP.Items.GetItemNameFromItemCode(ItemCode: BomItemCode.ItemCode);

                        //    foreach (var iw in itemsAndEnteredWeight)
                        //    {
                        //        // If our "Bill Of Material" item's name contains the term from the column
                        //        if (ItemName.Contains(iw.ItemName))
                        //        {
                        //            double quantity = iw.Quantity;
                        //            if (quantity > 0)
                        //            {
                        //                // Add a new Line Item to the Goods Receipt
                        //                ListOfGoodsReceiptLines.Add(new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines()
                        //                {
                        //                    ItemCode = BomItemCode.ItemCode,
                        //                    WarehouseCode = newRow.DestinationWarehouseCode,
                        //                    Quantity = quantity,
                        //                    ListOfBatchLineItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem>()
                        //                    {
                        //                        new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem()
                        //                        {
                        //                            BatchNumber = NSC_DI.SAP.BatchItems.NextBatch(ItemCode: BomItemCode.ItemCode, Prefix: "B", IncrementToBatchNumber: incrementBatch),
                        //                            Quantity = quantity,
                        //                            UserDefinedFields = new Dictionary<string, string>()
                        //                            {
                        //                                {"U_NSC_MotherID", newRow.MotherId},
                        //                                {"U_NSC_GroupNum", newRow.GroupName}
                        //                            }
                        //                        }
                        //                    },
                        //                    UserDefinedFields = new Dictionary<string, string>()
                        //                    {
                        //                        {"U_NSC_MotherID", newRow.MotherId},
                        //                        {"U_NSC_GroupNum", newRow.GroupName}
                        //                    }
                        //                });
                        //            }
                        //            break;
                        //        }
                        //    }
                        //}
                        //incrementBatch++;

                        //newRow.ListOfGoodsReceiptLines = ListOfGoodsReceiptLines;

                        // Add a row to our Transfer list.
                        driedBudTransferData.RowsToTransfer.Add(newRow);
                    }
                    catch (Exception e) { }
                }

                return driedBudTransferData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                GC.Collect();
            }
        }

        /// <summary>
        /// Barcode scan selection method.
        /// </summary>
        private void BTN_LOOKUP_ITEM_PRESSED(Form pForm)
        {
            EditText EditText_Lookup = null;
            SAPbouiCOM.Matrix Matrix_Items = null;

            //Process as normal 

            try
            {
                EditText_Lookup = pForm.Items.Item("TXT_LOOKUP").Specific;
                // If nothing was passed in the textbox, just return
                if (EditText_Lookup.Value.Length == 0)
                {
                    return;
                }

                Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;
                // Keep track of the warehouse ID from the barcode (if passed)
                string WarehouseID = "";

                // If the first four characters of the barcode are "WHSE", then the rest of the barcode contains the warehouse code.
                if (EditText_Lookup.Value.Length >= 4 && EditText_Lookup.Value.Substring(0, 4) == "WHSE")
                {
                    WarehouseID = EditText_Lookup.Value.Replace("WHSE-", "");
                }

                // Keep track if anything was selected.  At the end, if nothing was selected, click on the tab for that warehouse and select the plants in that warehouse
                bool WasAnythingSelected = false;

                // Clear out the current Selections
                for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                {
                    Matrix_Items.SelectRow(i, false, false);
                }

                int RowToCellFocusOn = -1;

                // For each row already selected
                for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                {
                    // Grab the row's Plant ID column
                    string plantID = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.MotherID).Cells.Item(i).Specific).Value;

                    // Grab the row's Warehouse Code column
                    string plantsWarehouseID = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.Warehouse).Cells.Item(i).Specific).Value;

                    // Grab the row's Stated ID column
                    string plantsStateID = "";//((SAPbouiCOM.EditText)MTX_PLANTS.Columns.Item(6).Cells.Item(i).Specific).Value;

                    // If the warehouse was scanned
                    if (WarehouseID != "")
                    {
                        // If the scanned warehouse code matches the row's warehouse code
                        if (WarehouseID == plantsWarehouseID)
                        {
                            // Select the row where the warehouse codes match
                            Matrix_Items.SelectRow(i, true, true);
                            WasAnythingSelected = true;
                        }
                    }
                    // No warehouse code was passed, so we are trying to identify an individual plant
                    else
                    {
                        // If the plant's ID matches the scanned barcode
                        if (EditText_Lookup.Value == plantID || EditText_Lookup.Value == plantsStateID)
                        {
                            // Select the row where the plant ID's match
                            Matrix_Items.SelectRow(i, true, true);


                            WasAnythingSelected = true;
                            RowToCellFocusOn = i;
                        }
                    }
                }

                // Empty the barcode scanner field
                EditText_Lookup.Value = "";

                if (WasAnythingSelected && RowToCellFocusOn != -1)
                {
                    int columnToFocus = 9;

                    var col = ((EditText)Matrix_Items.Columns.Item(columnToFocus).Cells.Item(RowToCellFocusOn).Specific);

                    col.Active = true;
                    col.ClickPicker();

                    //((SAPbouiCOM.EditText)MTX_ITEMS.Columns.Item(Index: columnToFocus).Cells.Item(RowToCellFocusOn).Specific).SuppressZeros = true;

                    //MTX_ITEMS.Columns.Item(Index: columnToFocus).Cells.Item(RowToCellFocusOn).Click(ClickType: SAPbouiCOM.BoCellClickType.ct_Regular);


                    //MTX_ITEMS.SetCellFocus(rowIndex: RowToCellFocusOn, ColumnIndex: columnToFocus);
                    //_SBO_Application.SendKeys("{RIGHT}{DELETE}{BACKSPACE}");

                }

                // Focus back into the barcode scanner's text field
                EditText_Lookup.Active = true;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(EditText_Lookup);
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                GC.Collect();
            }
            
        }

        private static void UpdateProgressBar(string Message, int ProgressToIncreaseBy = 0)
        {
            //try
            //{
            //    // Set progress bar message
            //    progressBar.Text = Message;

            //    // If we are increasing the progress bar size
            //    if (ProgressToIncreaseBy > 0)
            //    {
            //        // Calculate the new progress
            //        int NewProgressBarValue = progressBar.Value + ProgressToIncreaseBy;

            //        // Make sure our new progress amount is not greater than the maxium allowed
            //        if (NewProgressBarValue <= progressBar.Maximum)
            //        {
            //            progressBar.Value = NewProgressBarValue;
            //        }
            //    }
            //}
            //catch { }
        }

        private void MTX_ITEMS_Click(Form pForm, ItemEvent SAP_UI_ItemEvent)
        {
            // Find the matrix in the form UI
            SAPbouiCOM.Matrix Matrix_Items = null;
            try
            {
                Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;

                if (SAP_UI_ItemEvent.Row > Matrix_Items.RowCount || SAP_UI_ItemEvent.Row <= 0) return;

                var ItemGroupCodeOfNewSelectedRow = CommonUI.Forms.GetField<string>(pForm, "MTX_ITEMS", SAP_UI_ItemEvent.Row, "col_7");
                //((EditText)Matrix_Items.Columns.Item(F_Quarantine_Batch.MatrixColumns.ItemCode).Cells.Item(SAP_UI_ItemEvent.Row).Specific).Value;

                // Determine if this key press was of the same Item Group we had selectedd previously.
                bool deselectCurrent = false;
                List<int> rowsToReselect = new List<int>();
                for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                {
                    string ItemGroupCodeToCompare = CommonUI.Forms.GetField<string>(pForm, "MTX_ITEMS", i, "col_7");
                    // If the current row is now selected we can just continue looping.
                    if (!Matrix_Items.IsRowSelected(i) || i == SAP_UI_ItemEvent.Row)
                        continue;

                    rowsToReselect.Add(i);

                    deselectCurrent |= !string.Equals(ItemGroupCodeOfNewSelectedRow, ItemGroupCodeToCompare, StringComparison.InvariantCulture);
              
                }

                if (deselectCurrent)
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText(
                        Text: "Item Code must be of the same type!",
                        Seconds: BoMessageTime.bmt_Short,
                        Type: BoStatusBarMessageType.smt_Error);

                    Matrix_Items.SelectRow(SAP_UI_ItemEvent.Row, false, false);
                    throw new System.ComponentModel.WarningException();
                }
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                GC.Collect();
            }
        }

        private static void MAIN_MATRIX_LINK_PRESSED(Form pForm, ItemEvent SAP_UI_ItemEvent)
        {
            Form Form_ProductionOrder = null;
            try
            {
                // Attempt to grab the selected row ID
                int SelectedRowID = SAP_UI_ItemEvent.Row;

                if (SelectedRowID <= 0) throw new System.ComponentModel.WarningException();
                
                // Which column stores the ID
                int ColumnIDForIDOfItemSelected = 0;

                // Get the ID of the note selected
                SAPbouiCOM.Matrix oMX = pForm.Items.Item("MTX_ITEMS").Specific;
                SAPbouiCOM.EditText oEdit = oMX.GetCellSpecific("col_0", SelectedRowID);
                  

                try
                {
                    Form_ProductionOrder = Globals.oApp.Forms.GetForm("65211", 0);
                    Form_ProductionOrder.Select();
                }
                catch (Exception e)
                {
                    // Open up the Production Order Master Data form
                    Globals.oApp.ActivateMenuItem("4369");

                    // Grab the "Production Order" form from the UI
                    Form_ProductionOrder = Globals.oApp.Forms.GetForm("65211", 0);
                }

                // Freeze the "Production Order" form
                Form_ProductionOrder.Freeze(true);

                // Change the "Production Order" form to find mode
                Form_ProductionOrder.Mode = BoFormMode.fm_FIND_MODE;

                // Insert the Production Order ID in the appropriate text field
                ((EditText)Form_ProductionOrder.Items.Item("18").Specific).Value = oEdit.Value;

                // Click the "Find" button
                ((Button)Form_ProductionOrder.Items.Item("1").Specific).Item.Click();

                // Un-Freeze the "Production Order" form
                Form_ProductionOrder.Freeze(false);
                
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(Form_ProductionOrder);
                GC.Collect();
            }
        }
		#endregion SUBS
	}
}