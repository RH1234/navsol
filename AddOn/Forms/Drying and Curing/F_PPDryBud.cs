﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using System.Linq;
using System.Text;

namespace NavSol.Forms.Drying_and_Curing
{
	class F_PPDryBud : B1Event
	{

		/// <summary>
		/// Better way to keep track of the Matrix Columns we currently have.
		/// </summary>
		private enum MatrixColumns { ItemCode, ItemName, OnHand, Quantity, WhsCode, BatchNum, StateID, HarvName, GroupNum }
        private const string cItemType      = "Wet Cannabis";                                 // #3600 - filter for NON wet processing
        private const string cItemTypeWet   = "'Wet Cannabis','Wet Unb Flower','Wet Unb Trim'"; // #3600 - filter for wet processing

        public const string cFormID = "NSC_PLANDRYBUD";
        private static Form _SBO_Form;

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			var BubbleEvent = true;
			var oForm = null as Form;

            try
			{
				oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
                _SBO_Form = oForm;

                switch (pVal.ItemUID)
				{
					case "BTN_LOOKUP":
						BTN_LOOKUP_ITEM_PRESSED(oForm);
						break;

					case "BTN_RELE":
						BTN_RELE_Click(oForm);
						break;

					case "MTX_ITEMS":
						MTX_ITEMS_Click(oForm, pVal);
						break;
				}
			}
			catch (System.ComponentModel.WarningException) { }
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oForm);
				GC.Collect();
			}
			return BubbleEvent;
		}        
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
        [B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = null;
			try
			{
				oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
                _SBO_Form = oForm;

                switch (pVal.ItemUID)
				{
					case "MTX_ITEMS":
						MAIN_MATRIX_LINK_PRESSED(oForm, pVal);
						break;
				}
			}
			catch (System.ComponentModel.WarningException) { }
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oForm);
				GC.Collect();
			}
		}

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (ChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        // ---------------------------------- SUBS       --------------------------------------------------------

        public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}
				FormSetup(oForm);

				return oForm;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return null;
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		private static void FormSetup(Form pForm)
		{
			// Show the "Loading" form.
			Form Form_Waiting = null;
			try
			{
                CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_MAIN", @"drying-icon.bmp");

				Form_Waiting = F_Waiting.FormCreate();

				F_Waiting.SetProgressBar(Form_Waiting, 24);
                pForm.Select();
				Load_Matrix(pForm);
                
				F_Waiting.SetProgressBar(Form_Waiting, 55);
                // Fill the ComboBox of Warehouses
                if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y")
                {
                    NavSol.CommonUI.CFL.CreateWH(pForm, "CFL_WH", "CBX_WHSE");
                    NavSol.CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_NONE, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, "DRY");
                    NavSol.CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_AND, "Inactive", BoConditionOperation.co_EQUAL, "N");
                    CommonUI.CFL.AddCon_Branches(pForm, "CFL_WH", BoConditionRelationship.cr_AND);    //10823
                }
                else
                    Load_Combobox_Warehouses(pForm);

				// Preselect Individual Grouping of Production Orders.
				pForm.Items.Item("CMB_PORDER").Specific.Select(0, BoSearchKey.psk_Index);
               

				// Grab the matrix from the form ui
				pForm.Items.Item("MTX_ITEMS").Specific.SelectionMode = BoMatrixSelect.ms_Auto;

				F_Waiting.SetProgressBar(Form_Waiting, 100);
				F_Waiting.Form_Close(Form_Waiting);

                pForm.Items.Item("TXT_LOOKUP").Specific.Active = true;

                pForm.Items.Item("CMB_PORDER").Visible = false; // hide the Grouping option until we know what we want to do with it

                // Show the form
                pForm.VisibleEx = true;
                pForm.Items.Item("TXT_LOOKUP").Specific.Active = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(Form_Waiting);
				GC.Collect();
			}
		}

        private void CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            if (pVal.SelectedObjects == null) return;

            try
            {
                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_WH":
                        CommonUI.CFL.SetFieldsWH(pForm, pVal, "CBX_WHSE", "txtWH_CFL");
                        break;

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void Load_Combobox_Warehouses(Form pForm)
        {
            // Prepare to run a SQL statement.
            ComboBox ComboBox_Warehouse = null;

            SAPbobsCOM.Recordset oRecordSet = (Recordset)Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            // Count how many current records exist within the database.
            string sql = @"SELECT 
    [WhsCode]
,   [WhsName]
FROM [OWHS]
WHERE [Inactive] = 'N' AND [U_NSC_WhrsType] = 'DRY'";
            if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})";  // #10823
            System.Data.DataTable dt_Warehouse = NSC_DI.UTIL.SQL.DataTable(sql, "OWHS");
            //            oRecordSet.DoQuery(string.Format(@"
            //SELECT 
            //    [WhsCode]
            //,   [WhsName]
            //FROM [OWHS]
            //WHERE [U_NSC_WhrsType] = 'DRY'"));

            // Find the Combobox of Warehouses from the Form UI
            ComboBox_Warehouse = pForm.Items.Item("CBX_WHSE").Specific;

            // If items already exist in the drop down
            if (ComboBox_Warehouse.ValidValues.Count > 0)
            {
                // Remove all currently existing values from warehouse drop down
                for (int i = ComboBox_Warehouse.ValidValues.Count; i-- > 0;)
                {
                    ComboBox_Warehouse.ValidValues.Remove(i, BoSearchKey.psk_Index);
                }
            }

            // If more than 1 warehouses exists
            if (dt_Warehouse.Rows.Count > 1)
            {
                // Create the first item as an empty item
                ComboBox_Warehouse.ValidValues.Add("", "");

                // Select the empty item (forcing the user to make a decision)
                ComboBox_Warehouse.Select(0, BoSearchKey.psk_Index);
            }

            // Add allowed warehouses to the drop down
            foreach (System.Data.DataRow dr in dt_Warehouse.Rows)
            {
                try
                {
                    ComboBox_Warehouse.ValidValues.Add(dr["WhsName"].ToString(), dr["WhsCode"].ToString());

                }
                catch { }
                ComboBox_Warehouse.Item.Enabled = true;
            }

            // Auto select our warehouse if we only have one and Disable the control
            if (dt_Warehouse.Rows.Count == 1)
            {
                ComboBox_Warehouse.Active = false;
                ComboBox_Warehouse.Item.Enabled = false;
                ComboBox_Warehouse.Select(0, BoSearchKey.psk_Index);
            }
        }

        private static void Load_Matrix(Form pForm)
        {
            try
            {
                // #10823 --->
                // limit the list to items in ths pecific branch
                var defBranch = Globals.BranchDflt;         //  -1 is no branches, 0 is no default branch
                var brList = Globals.BranchList;
                var destWH = pForm.Items.Item("CBX_WHSE").Specific.Value;
                if (destWH != "" && defBranch >= 0)
                {
                    defBranch = NSC_DI.SAP.Branch.Get(destWH);
                    brList = NSC_DI.SAP.Branch.GetAll_User_Str(defBranch);
                }
                // #10823 <---
                // #3600 - filter for wet processing
                string filter = NSC_DI.UTIL.Options.Value.GetSingle("Wet Processing") == "Y" ? cItemTypeWet : "'" + cItemType + "'";
                string sqlQuery = $@"
SELECT	[OITM].[ItemCode]	AS [ItemCode]
,		[OITM].[ItemName]	AS [ItemName]
,		[OBTQ].[Quantity]	AS [Quantity]
,		[OBTQ].[Quantity]	AS [OnHand]
,		[OBTQ].[WhsCode]	AS [WhsCode]
,		[OWHS].[WhsName]	as [WhsName]
,		[OBTN].[DistNumber] AS [BatchNum]
,		[OBTN].[U_NSC_MotherID] [MotherID]
,		[OBTN].[U_NSC_GroupNum] AS [GroupNum]
,       [OBTN].[MnfSerial]
,       OBTN.U_NSC_HarvestName AS [HarvName]
FROM [OBTN] 
JOIN [OBTQ] ON [OBTQ].[ItemCode] = [OBTN].[ItemCode]
	AND [OBTQ].[SysNumber] = [OBTN].[SysNumber]
JOIN [OITM] ON [OITM].[ItemCode] = [OBTN].[ItemCode]
JOIN [OITB] ON [OITM].[ItmsGrpCod] = [OITB].[ItmsGrpCod]
JOIN [OWHS] On	[OBTQ].WhsCode = [OWHS].WhsCode
WHERE [OITB].[ItmsGrpNam] in ({filter})
AND [OBTQ].[Quantity] > 0 
AND [OWHS].Inactive = 'N'
";
                if (defBranch >= 0) sqlQuery += $" AND OWHS.BPLid IN ({brList})";        // #10823


                // Load the matrix of tasks
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "OIBT", "MTX_ITEMS", new List<CommonUI.Matrix.MatrixColumn>() {
                            new CommonUI.Matrix.MatrixColumn(){ Caption="Item Code",         ColumnWidth=40, ColumnName="ItemCode", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="Item Name",        ColumnWidth=80, ColumnName="ItemName", ItemType = BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="On Hand",          ColumnWidth=80, ColumnName="OnHand",   ItemType = BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="Quantity",         ColumnWidth=80, ColumnName="Quantity", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="Source Warehouse", ColumnWidth=80, ColumnName="WhsName",  ItemType = BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="Batch Number",     ColumnWidth=80, ColumnName="BatchNum", ItemType = BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="State Id",         ColumnWidth=80, ColumnName="MnfSerial", ItemType = BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="Harvest Name",     ColumnWidth=80, ColumnName="HarvName", ItemType = BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn(){ Caption="Group Number",     ColumnWidth=80, ColumnName="GroupNum", ItemType = BoFormItemTypes.it_EDIT}

                        }, sqlQuery);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

        }

        private static WetBudDataTransfer GetSelectedItemsFromMatrix(Form pForm)
        {
            EditText EditText_Days = null;
            Matrix Matrix_Items = null;
            try
            {
                WetBudDataTransfer wetBudTransferData = new WetBudDataTransfer();

                // Grab the matrix from the form ui
                Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;

                // Grab the textbox for days from the form
                EditText_Days = pForm.Items.Item("TXT_DAYS").Specific;

                int estimatedDays;
                if (!int.TryParse(EditText_Days.Value, out estimatedDays))
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Please enter in a valid number for the days", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);

                    // Focus the cursor in the offending input
                    try
                    {
                        EditText_Days.Active = true;
                    }
                    catch
                    {
                    }
					return null;
				}

                wetBudTransferData.DueDate = DateTime.Now.AddDays(estimatedDays);

                // For each row already selected
                for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                {
                    WetBudSelectedRow newRow = new WetBudSelectedRow();

                    try
                    {
                        // If the current row is now selected we can just continue looping.
                        if (!Matrix_Items.IsRowSelected(i)) continue;

                        newRow.ItemCode = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.ItemCode).Cells.Item(i).Specific).Value;
                        newRow.BatchNo = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.BatchNum).Cells.Item(i).Specific).Value;
                        newRow.SourceWarehouseCode = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.WhsCode).Cells.Item(i).Specific).Value;
                        newRow.GroupName = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.GroupNum).Cells.Item(i).Specific).Value;

                        // Keep track of which warehouse was selected
                        string SelectedDestinationWarehouseID = "";

                        double parsedQuantity;
                        if (!double.TryParse(((EditText)Matrix_Items.Columns.Item(MatrixColumns.Quantity).Cells.Item(i).Specific).Value, out parsedQuantity))
                        {
                            // Send a message to the client
                            Globals.oApp.StatusBar.SetText("Please enter in a number for the quantity!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);

                            // Focus the cursor in the offending input
                            ((EditText)Matrix_Items.Columns.Item(MatrixColumns.Quantity).Cells.Item(i).Specific).Active = true;

                            throw new System.ComponentModel.WarningException("Please enter in a valid number for the quantity");
                        }

                        newRow.Quantity = parsedQuantity;

                        // Grab the value from the ComboBox
                        newRow.DestinationWarehouseName = pForm.Items.Item("CBX_WHSE").Specific.Value.ToString();

                        // Get the "description" from the ComboBox, where we are really storing the value
                        newRow.DestinationWarehouseCode = CommonUI.Forms.GetField<string>(pForm, "CBX_WHSE", true);

                        // Add a row to our Transfer list.
                        wetBudTransferData.RowsToTransfer.Add(newRow);
                    }
                    catch (Exception ex1)
                    {
                        System.Diagnostics.Debugger.Break();
                    }
                }

                return wetBudTransferData;
            }
            catch (Exception ex2)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex2), ex2);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(EditText_Days);
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                GC.Collect();
            }
        }

        /// <summary>
        /// Handle our button click event for Release and Issue.
        /// </summary>
        private static void BTN_RELE_Click(Form pForm)
        {
            ComboBox ComboBox_ProductionOrder = null;
            Matrix Matrix_Items = null;
            Form oForm_Waiting = null;
            try
            {
                oForm_Waiting = F_Waiting.FormCreate(pCallingFormUID: pForm.UniqueID);
                F_Waiting.SetProgressBar(oForm_Waiting, 24, "Please wait while we gather data for you...");

                // Grab the textbox for days from the form
                ComboBox_ProductionOrder = pForm.Items.Item("CMB_PORDER").Specific;

                if (string.IsNullOrEmpty(ComboBox_ProductionOrder.Value))
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Please select how you'd like to group your production orders.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);

                    // Focus the cursor in the offending input
                    ComboBox_ProductionOrder.Active = true;
                    F_Waiting.Form_Close(oForm_Waiting);
                    throw new System.ComponentModel.WarningException("Please select how you'd like to group your production orders.");
                }

                // Grab the matrix from the form ui
                Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;

                // Keep track of the first group we find selected
                string firstGroupIDSelected = "";

                // Keep track of the number of items selected in the group
                int groupSelectionCount = 0;

                // Determine if multiple groups were selected
                bool multipleGroupsSelected = false;

                // Loop through selected rows
                int NextRow = Matrix_Items.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);
                while (NextRow != -1)
                {
                    // Get the "Group Number" from the selected row
                    string groupNumber = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.GroupNum).Cells.Item(NextRow).Specific).Value;

                    // If this is the first "Group Number" we've seen
                    if (string.IsNullOrEmpty(firstGroupIDSelected))
                    {
                        firstGroupIDSelected = groupNumber;
                        groupSelectionCount = 1;
                    }
                    // We've already seen a "Group Number" in the past
                    else
                    {
                        // If the current "Group Number" does not match our first "Group Number" find
                        if (firstGroupIDSelected != groupNumber)
                        {
                            multipleGroupsSelected = true;

                            // Break out of the loop through selected items
                            break;
                        }
                        // Current "Group Number" matches our first "Group Number" find
                        else
                        {
                            groupSelectionCount++;
                        }
                    }

                    NextRow = Matrix_Items.GetNextSelectedRow(NextRow, BoOrderType.ot_RowOrder);
                }

                // If the user selected items across multiple groups
                if (multipleGroupsSelected)
                {
                    // Loop through selected rows to de-select them
                    NextRow = Matrix_Items.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);
                    while (NextRow != -1)
                    {
                        // Get the "Group Number" from the selected row
                        string groupNumber = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.GroupNum).Cells.Item(NextRow).Specific).Value;

                        // If the items "Group Number" does not match our first "Group Number" found
                        if (firstGroupIDSelected != groupNumber)
                        {
                            // De-select the selected row
                            Matrix_Items.SelectRow(NextRow, false, true);
                        }

                        NextRow = Matrix_Items.GetNextSelectedRow(NextRow, BoOrderType.ot_RowOrder);
                    }
                }

                // Use SQL to calculate total available batches in that group
                if (!string.IsNullOrEmpty(firstGroupIDSelected))
                {
                    string SQL_Query_GroupBatchCount = @"
SELECT COUNT(*) FROM [OBTN]
  JOIN [OITM] ON [OITM].[ItemCode] = [OBTN].[ItemCode]
  JOIN [OITB] ON [OITM].[ItmsGrpCod] = [OITB].[ItmsGrpCod]
 WHERE [OBTN].[U_NSC_GroupNum] = '" + firstGroupIDSelected + @"'AND [OITB].[ItmsGrpNam] = '" + cItemType + @"'
   AND (SELECT [OIBT].[WhsCode] FROM [OIBT] WHERE [OIBT].[BatchNum] = [OBTN].[DistNumber] AND [OIBT].[Quantity] > 0) > 0
";

                    // Retrieve the count of items in the group
                    int totalAvailableBatchesInGroup = NSC_DI.UTIL.SQL.GetValue<int>(SQL_Query_GroupBatchCount);

                    // 
                    if (totalAvailableBatchesInGroup != groupSelectionCount)
                    {
                        // TODO: Select all grouped items for the user

                        Globals.oApp.StatusBar.SetText("Please select all " + totalAvailableBatchesInGroup + " batches in group " + firstGroupIDSelected + "!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error
                        );
                        F_Waiting.Form_Close(oForm_Waiting);
                        throw new System.ComponentModel.WarningException("Please select all " + totalAvailableBatchesInGroup + " batches in group " + firstGroupIDSelected + "!");
                    }
                }

                CreateIndividualProductionOrders(pForm);
                //switch (ComboBox_ProductionOrder.Value.ToString())
                //{
                //    case "Individual":
                //        CreateIndividualProductionOrders(pForm);
                //        break;
                //    case "Grouped":
                //        CreateGroupedProductionOrder(pForm);
                //        break;
                //}

                F_Waiting.Form_Close(oForm_Waiting);
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oForm_Waiting);
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                NSC_DI.UTIL.Misc.KillObject(ComboBox_ProductionOrder);
                GC.Collect();
            }
        }

        private static void CreateIndividualProductionOrders(Form pForm)
        {
            // Pull all data from the selected columns.
            WetBudDataTransfer wetBudToTransfer = GetSelectedItemsFromMatrix(pForm);
            if (wetBudToTransfer == null) return;


            bool AllWetBudPassed = true;
            double TotalProcessed = 0;

            if (wetBudToTransfer.RowsToTransfer.Count <= 0)
            {
                // Send success message to the client
                Globals.oApp.StatusBar.SetText("Please select at least one row!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
				return;
			}

            bool bUpdatedProdO = false;

            // Save error line items

            var sb = new StringBuilder();

            sb.AppendLine("One or more items failed:");

            foreach (WetBudSelectedRow row in wetBudToTransfer.RowsToTransfer)
            {
                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                try
                {
                    var strainID = NSC_DI.SAP.Items.Batches.GetStrainID(row.BatchNo);
                    //determine item to create
                    var parentItem="";
                    // Wet Unbatched Trim
                    if (row.ItemCode.Contains("WUTR"))
                    {
                        parentItem = NSC_DI.UTIL.AutoStrain.GetAutoItemCodeValue("~DUTR", strainID);
                    }
                    //Wet Cannabis 
                    if (row.ItemCode.Contains("WC"))
                    {
                        parentItem = NSC_DI.UTIL.AutoStrain.GetAutoItemCodeValue("~DC", strainID);
                    } 
                    //Wet Unbatched Flower
                    if(row.ItemCode.Contains("WUFL"))
                    {
                        parentItem = NSC_DI.UTIL.AutoStrain.GetAutoItemCodeValue("~DUFL", strainID);
                    }
                    //Larf
                    if (row.ItemCode.Contains("WUL"))
                    {
                        parentItem = NSC_DI.UTIL.AutoStrain.GetAutoItemCodeValue("~DUL", strainID);
                    }
                    //Wet Extract Flower
                    if (row.ItemCode.Contains("WUEF"))
                    {
                        parentItem = NSC_DI.UTIL.AutoStrain.GetAutoItemCodeValue("~DUEF", strainID);
                    }
                    //Wet Granulated Flower
                    if (row.ItemCode.Contains("WUGN"))
                    {
                        parentItem = NSC_DI.UTIL.AutoStrain.GetAutoItemCodeValue("~DUGN", strainID);
                    }
                    //Wet Premium Flower
                    if (row.ItemCode.Contains("WPFL"))
                    {
                        parentItem = NSC_DI.UTIL.AutoStrain.GetAutoItemCodeValue("~DPFL", strainID);
                    }

                    var plannedQuantity = row.Quantity;
                    //Remarks
                    SAPbouiCOM.EditText oEdit = pForm.Items.Item("TXT_REM").Specific;
                    string Remarks = oEdit.Value;

                    string destWHName = pForm.Items.Item("CBX_WHSE").Specific.Value;  // #10824
                    string destWHCode = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT WhsCode FROM OWHS WHERE WhsName = '{destWHName}'");// 10824
                    var PdOKey = NSC_DI.SAP.ProductionOrder.CreateFromBOM(parentItem, plannedQuantity, DateTime.Now, destWHCode, true, "N", "WET", Remarks); // 10824

                    if (PdOKey == 0)
                    {
                        sb.AppendLine($"Item Code: {row.ItemCode} Batch No: {row.BatchNo}");
                        sb.AppendLine("Failed to create Production Order " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                        sb.AppendLine();
                        throw new System.ComponentModel.WarningException();
                    }

                    //Check the warehouse for the item(s) on tbe Production BOM and if needed adjust accordingly
                    //Get clone Warehouses for selected items on the Matrix by batchnumber 
                    SAPbobsCOM.ProductionOrders oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
                    oPdO.GetByKey(PdOKey);
                    SAPbobsCOM.ProductionOrders_Lines oLines = oPdO.Lines;

                    //Update Destination Warehouse.
                    if (row.DestinationWarehouseCode != oPdO.Warehouse)
                    {
                        oPdO.Warehouse = row.DestinationWarehouseCode;
                        NSC_DI.SAP.Document.SetDefaultDistributionRules(oLines);
                        bUpdatedProdO = true;
                    }
                    //Update line Item warehouse
                    for (int i = 0; i <= oLines.Count - 1; i++)
                    {
                        oLines.SetCurrentLine(i);
                        if (row.ItemCode == oLines.ItemNo)
                        {
                            string strWhsSourceHolder = NSC_DI.UTIL.SQL.GetValue<string>(@"select WhsCode from [OWHS] where OWHS.WhsName = '" + row.SourceWarehouseCode + "'", null);
                            if (strWhsSourceHolder != oLines.Warehouse && oLines.PlannedQuantity > 0)
                            {
                                //Update warehouse
                                oLines.Warehouse = strWhsSourceHolder;
                                NSC_DI.SAP.Document.SetDefaultDistributionRules(oLines);
                                bUpdatedProdO = true;
                            }
                        }
                    }
                    if (bUpdatedProdO == true)
                    {
                        int res = oPdO.Update();
                        if (res < 0) NSC_DI.SAP.B1Exception.RaiseException();
                    }

                    string filter = NSC_DI.UTIL.Options.Value.GetSingle("Wet Processing") == "Y" ? cItemTypeWet : cItemType;    // #6355
                    var poIssueKey = NSC_DI.SAP.ProductionOrder.Issue(PdOKey, filter, row.Quantity, row.BatchNo, null);

                    if (poIssueKey == 0)
                    {
                        sb.AppendLine($"Item Code: {row.ItemCode} Batch No: {row.BatchNo}");
                        sb.AppendLine("Failed to Issue to Production for selected Wet Cannabis " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                        sb.AppendLine();
                        throw new System.ComponentModel.WarningException();
                    }

                    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                    TotalProcessed += row.Quantity;
                }
                catch (System.ComponentModel.WarningException)
                {
                    AllWetBudPassed = false;
                }
                catch (NSC_DI.SAP.B1Exception ex)
                {
                    sb.AppendLine($"Item Code: {row.ItemCode} Batch No: {row.BatchNo}");
                    sb.AppendLine(ex.Message);
                    sb.AppendLine();
                    AllWetBudPassed = false;
                }
                catch (Exception ex)
                {
                    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                    sb.AppendLine($"Item Code: {row.ItemCode} Batch No: {row.BatchNo}");
                    sb.AppendLine(ex.Message);
                    sb.AppendLine();
                    AllWetBudPassed = false;
                }
                finally
                {
                    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
            }

            if (AllWetBudPassed)
            {
                // Send success message to the client
                Globals.oApp.StatusBar.SetText("Successfully Release and Issued the Selected Items!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                SAPbouiCOM.EditText oEdit = pForm.Items.Item("TXT_REM").Specific;
                oEdit.Value = "";
            }
            else
            {
                if (TotalProcessed > 0)
                {
                    // Send success message to the client
                    Globals.oApp.StatusBar.SetText("Failed to Release and Issued some of the Selected Items!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                }
                else
                {
                    // Send success message to the client
                    Globals.oApp.StatusBar.SetText("Failed to Release and Issued all Selected Items!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                }

                var errMsg = sb.ToString();
                Globals.oApp.MessageBox(errMsg);
            }

            Load_Matrix(pForm);
        }

        private static void CreateGroupedProductionOrder(Form pForm)
        {
            // Pull all data from the selected columns.
            WetBudDataTransfer wetBudToTransfer = GetSelectedItemsFromMatrix(pForm);
            if (wetBudToTransfer == null)
                return;


            double TotalProcessed = 0;

            if (wetBudToTransfer.RowsToTransfer.Count <= 0)
            {
                // Send success message to the client
                Globals.oApp.StatusBar.SetText("Failed to Release and Issued all Selected Items!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);

                throw new System.ComponentModel.WarningException("Failed to Release and Issued all Selected Items!");
            }

            #region Create GoodsIssued and Disassembly Production Order Data Lists

            string ItemCode = "";
            string DestinationWarehouseCode = "";
            string SourceWarehouseCode = "";
            string ProjectedBatchNumber = "";
            int TotalQuantity = 0;

            // Prepare a list of Goods Issued Lines
            List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines> ListOfGoodsIssuedLines = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();

            // Prepare a list of line items for the "Production Order"
            List<NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem> components = new List<NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem>();

            foreach (WetBudSelectedRow row in wetBudToTransfer.RowsToTransfer)
            {
                // Add a new item to the list of Goods Issued Lines
                ListOfGoodsIssuedLines.Add(
                    new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
                    {
                        ItemCode = row.ItemCode,
                        Quantity = row.Quantity,
                        WarehouseCode = row.SourceWarehouseCode,
                        BatchLineItems = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>()
                        {
                            new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches()
                            {
                                BatchNumber = row.BatchNo,
                                Quantity = row.Quantity
                            }
                        },
                        UserDefinedFields = new Dictionary<string, string>()
                        {
                           {"U_NSC_GroupNum", row.GroupName}
                        }
                    }
                );

                ItemCode = row.ItemCode;
                DestinationWarehouseCode = row.DestinationWarehouseCode;
                SourceWarehouseCode = row.SourceWarehouseCode;
                ProjectedBatchNumber = row.BatchNo;
                TotalQuantity += (int)row.Quantity;
            }


            foreach (var item in NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(ItemCode))
            {
                components.Add(
                    new NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem()
                    {
                        ItemCodeForItemBeingCreated = item.ItemCode,
                        DestinationWarehouseCode = DestinationWarehouseCode,
                        BaseQuantity = TotalQuantity,
                        PlannedQuantity = TotalQuantity
                    }
                );
            }

            #endregion

            // Create a Disassembly Production Order based on the form
            try
            {
                NSC_DI.SAP.ProductionOrder_OLD.CreateDisassemblyProductionOrder(ItemCode
                    , TotalQuantity
                    , SourceWarehouseCode
                    , DateTime.Now
                    , components
                    , ProjectedBatchNumber);
            }
            catch
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Failed to create production order!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
            }

            // Change status to released
            NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(NSC_DI.SAP.ProductionOrder_OLD.NewlyCreatedKey, SAPbobsCOM.BoProductionOrderStatusEnum.boposReleased);

            try
            {
                // Create an "Issue From Production" document
                NSC_DI.SAP.IssueFromProduction.Create(NSC_DI.SAP.ProductionOrder_OLD.NewlyCreatedKey, DateTime.Now, ListOfGoodsIssuedLines
                );

                Globals.oApp.StatusBar.SetText("Successfully Released and Issued the Selected Items!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch
            {
                Globals.oApp.StatusBar.SetText("Failed to Release and Issue all Selected Items!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
            }

            Load_Matrix(pForm);
        }

        private static void BTN_LOOKUP_ITEM_PRESSED(Form pForm)
        {
            // Grad the textbox for the barcode scanner from the form UI
            EditText EditText_Lookup = null;
            Matrix Matrix_Items = null;
            try
            {
                EditText_Lookup = pForm.Items.Item("TXT_LOOKUP").Specific;

                // If nothing was passed in the textbox, just return
                if (EditText_Lookup.Value.Length == 0)
                {
                    Globals.oApp.StatusBar.SetText("No Data To Scan! - Please Enter A Valid Value.");
                    return;
                }

                // Grab the matrix from the form UI
                Matrix_Items = pForm.Items.Item("MTX_ITEMS").Specific;
                CheckBox oChkBox_Assign_StateID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_STID", _SBO_Form) as CheckBox;
                bool Add_State_ID = oChkBox_Assign_StateID.Checked;
                if (Add_State_ID == true)
                {
                    bool bRowIsSelected = false;
                    int intSelectedRowCount = 0;
                    int intLastRowIndex = 0;
                    //Add Code to Validate Single row selection
                    for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                    {
                        bRowIsSelected = Matrix_Items.IsRowSelected(i);
                        if (bRowIsSelected == true)
                        {
                            intLastRowIndex = i;
                            intSelectedRowCount++;
                            bRowIsSelected = false;
                        }
                    }
                    if (intSelectedRowCount > 1)
                    {
                        //Show error message to indicate you can only select one row when assigning StateId
                        Globals.oApp.StatusBar.SetText("You may only select one row when assigning a StateID");
                        return;
                    }
                    else
                    {
                        //Check if ID already exist --, and (assign ID or Overwrite Existing)
                        string StateID = ((EditText)Matrix_Items.Columns.Item((int)MatrixColumns.StateID).Cells.Item(intLastRowIndex).Specific).Value;
                        string DistNumber = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.BatchNum).Cells.Item(intLastRowIndex).Specific).Value;
                        SAPbobsCOM.BatchNumberDetail oBatchDetails = null;
                        CompanyService oCoService = Globals.oCompany.GetCompanyService() as CompanyService;
                        BatchNumberDetailsService oBatchService = oCoService.GetBusinessService(ServiceTypes.BatchNumberDetailsService);
                        BatchNumberDetailParams oParams = oBatchService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);

                        try
                        {
                            if (StateID.Trim().Length == 0)
                            {
                                // RH commented out the lines
                                ////Add field in DB
                                ////Get and Set the DocEntry
                                ////***To Do: Up date this so that it is pulling on the correct DistNumber once Randy adjust this you will need to
                                ////Uncomment the line below and delete the current select State used for testing. 
                                //int intIndexHolder = DistNumber.IndexOf('-');
                                //DistNumber = DistNumber.Remove(0, intIndexHolder+1);
                                //intIndexHolder = DistNumber.IndexOf('-');
                                //DistNumber = DistNumber.Remove(0, intIndexHolder + 1);
                                string strSQL = string.Format(@"Select AbsEntry From [OBTN] where DistNumber='{0}'", DistNumber);
                                int DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                                oParams.DocEntry = DocEntry;
                                oBatchDetails = oBatchService.Get(oParams);
                                oBatchDetails.BatchAttribute1 = EditText_Lookup.Value;
                                oBatchService.Update(oBatchDetails);
                                //Success Reload Matrix
                                _SBO_Form.Freeze(true);
                                Load_Matrix(_SBO_Form);
                                // Empty the barcode scanner field
                                EditText_Lookup.Value = "";
                                // Focus back into the barcode scanner's text field
                                EditText_Lookup.Active = true;
                                //Move to next Row if not last row
                                if (Matrix_Items.RowCount != intLastRowIndex)
                                {
                                    Matrix_Items.SelectRow(intLastRowIndex + 1, true, true);
                                }
                            }
                            else
                            {
                                //Update field in DB
                                int iReturnVal = Globals.oApp.MessageBox("A State ID already exists for this item would you like to replace it?", 2, "Cancel", "OK");
                                switch (iReturnVal)
                                {
                                    case 1:
                                        //Cancel was selected do nothing                                         
                                        break;
                                    case 2:
                                        //Update the field in DB
                                        //Get and Set the DocEntry
                                        //***To Do: Up date this so that it is pulling on the correct DistNumber once Randy adjust this you will need to
                                        //Uncomment the line below and delete the current select State used for testing. 
                                        //int intIndexHolder = DistNumber.IndexOf('-');
                                        //DistNumber = DistNumber.Remove(0, intIndexHolder + 1);
                                        //intIndexHolder = DistNumber.IndexOf('-');
                                        //DistNumber = DistNumber.Remove(0, intIndexHolder + 1);
                                        string strSQL = string.Format(@"Select AbsEntry From [OBTN] where DistNumber='{0}'", DistNumber);
                                        int DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                                        oParams.DocEntry = DocEntry;
                                        oBatchDetails = oBatchService.Get(oParams);
                                        oBatchDetails.BatchAttribute1 = EditText_Lookup.Value;
                                        oBatchService.Update(oBatchDetails);
                                        //Success Reload Matrix
                                        _SBO_Form.Freeze(true);
                                        Load_Matrix(_SBO_Form);
                                        // Empty the barcode scanner field
                                        EditText_Lookup.Value = "";
                                        // Focus back into the barcode scanner's text field
                                        EditText_Lookup.Active = true;
                                        //Move to next Row if not last row
                                        if (Matrix_Items.RowCount != intLastRowIndex)
                                        {
                                            Matrix_Items.SelectRow(intLastRowIndex + 1, true, true);
                                        }
                                        break;
                                }

                            }

                            //Update Matrix cell with data or Reload Matrix
                        }
                        catch (Exception ex)
                        {
                            Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                        }
                        finally
                        {
                            //Clean up the objects and Garbage Collect
                            _SBO_Form.Freeze(false);
                            NSC_DI.UTIL.Misc.KillObject(oBatchDetails);
                            NSC_DI.UTIL.Misc.KillObject(oCoService);
                            NSC_DI.UTIL.Misc.KillObject(oBatchService);

                            GC.Collect();
                        }
                    }

                }
                else
                {
                    //Process as normal 

                    // Keep track of the warehouse ID from the barcode (if passed)
                    string WarehouseID = "";

                    // If the first four characters of the barcode are "WHSE", then the rest of the barcode contains the warehouse code.
                    if (EditText_Lookup.Value.Length >= 4 && EditText_Lookup.Value.Substring(0, 4) == "WHSE")
                    {
                        WarehouseID = EditText_Lookup.Value.Replace("WHSE-", "");
                    }

                    // Keep track if anything was selected.  At the end, if nothing was selected, click on the tab for that warehouse and select the plants in that warehouse
                    bool WasAnythingSelected = false;

                    // Clear out the current Selections
                    for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                    {
                        Matrix_Items.SelectRow(i, false, false);
                    }

                    // For each row already selected
                    for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                    {
                        // Grab the row's Plant ID column
                        //string plantID = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.MotherID).Cells.Item(i).Specific).Value;

                        // Grab the row's Warehouse Code column
                        string plantsWarehouseID = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.WhsCode).Cells.Item(i).Specific).Value;

                        // Grab the row's Stated ID column
                        string plantsStateID = ((EditText)Matrix_Items.Columns.Item((int)MatrixColumns.StateID).Cells.Item(i).Specific).Value;

                        // If the warehouse was scanned
                        if (WarehouseID != "")
                        {
                            // If the scanned warehouse code matches the row's warehouse code
                            if (WarehouseID == plantsWarehouseID)
                            {
                                // Select the row where the warehouse codes match
                                Matrix_Items.SelectRow(i, true, true);
                                WasAnythingSelected = true;
                            }
                        }
                        // No warehouse code was passed, so we are trying to identify an individual plant
                        else
                        {
                            // If the plant's ID matches the scanned barcode
                            if (EditText_Lookup.Value == plantsStateID)
                            {
                                // Select the row where the plant ID's match
                                Matrix_Items.SelectRow(i, true, true);

                                WasAnythingSelected = true;
                                ((EditText)Matrix_Items.Columns.Item(MatrixColumns.Quantity).Cells.Item(i).Specific).Active = true;
                            }
                        }
                    }

                    // Empty the barcode scanner field
                    EditText_Lookup.Value = "";

                    if (!WasAnythingSelected)
                    {

                    }

                    // Focus back into the barcode scanner's text field
                    EditText_Lookup.Active = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(EditText_Lookup);
                NSC_DI.UTIL.Misc.KillObject(Matrix_Items);
                GC.Collect();
            }

        }

        private static void MTX_ITEMS_Click(Form pForm, ItemEvent pItemEvent)
        {

            Matrix Matrix_Main = null;
            try
            {
                Matrix_Main = pForm.Items.Item("MTX_ITEMS").Specific;
                if (Matrix_Main.GetNextSelectedRow() == -1 || pItemEvent.Row > Matrix_Main.RowCount) return;
                string ItemCodeOfNewSelectedRow = CommonUI.Forms.GetField<string>(pForm, "MTX_ITEMS", pItemEvent.Row, 0);

                // Determine if this key press was of the same Item Group we had selected previously.
                bool deselectCurrent = false;
                List<int> rowsToReselect = new List<int>();

                int NextRow = Matrix_Main.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);

                while (NextRow != -1)
                {
                    string ItemCodeToCompare = ((EditText)Matrix_Main.Columns.Item(MatrixColumns.ItemCode).Cells.Item(NextRow).Specific).Value;

                    if (!ItemCodeOfNewSelectedRow.Equals(ItemCodeToCompare))
                    {
                        deselectCurrent = true;
                        NextRow = -1;
                    }
                    else
                    {
                        rowsToReselect.Add(NextRow);
                        NextRow = Matrix_Main.GetNextSelectedRow(NextRow, BoOrderType.ot_RowOrder);
                    }
                }
                
                if (deselectCurrent)
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Item Code must be of the same type!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);

                    Matrix_Main.SelectRow(pItemEvent.Row, false, false);
                    throw new System.ComponentModel.WarningException("Item Code must be of the same type!");
                }

                // Grab the selected item in the Matrix
                int idx = Matrix_Main.GetNextSelectedRow(0, BoOrderType.ot_SelectionOrder);
                string selectedItemCode = CommonUI.Forms.GetField<string>(pForm, "MTX_ITEMS", idx, 0);

                // Prepare the SQL statement that will find the projected time via the item code
                string sql = @"
SELECT 
[Strain].[U_PrjctDrying] 
FROM
[OITM]
JOIN
[@" + NSC_DI.Globals.tStrains + @"] as [Strain]
ON
[OITM].[U_NSC_StrainID] = [Strain].[Code]
WHERE
[OITM].[ItemCode] = '" + selectedItemCode + "'";
                
                // Set the estimated days
                pForm.Items.Item("TXT_DAYS").Specific.Value = NSC_DI.UTIL.SQL.GetValue<string>(sql);

                Matrix_Main.SetCellFocus(pItemEvent.Row, 3);
            }
            catch (System.ComponentModel.WarningException ex1)
            {
                // throw ex1;
            }
            catch (Exception ex2)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex2));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(Matrix_Main);
                GC.Collect();
            }
        }

        private void MAIN_MATRIX_LINK_PRESSED(Form pForm, ItemEvent pItemEvent)
        {
            Form Form_ProductionOrder = null;
            try
            {
                // Attempt to grab the selected row ID
                int SelectedRowID = pItemEvent.Row;

                if (SelectedRowID > 0)
                {
                    // Which column stores the ID
                    int ColumnIDForIDOfItemSelected = 0;



                    // Get the ID of the note selected
                    string ItemSelected = pForm.Items.Item("MTX_ITEMS").Specific.Columns.Item(ColumnIDForIDOfItemSelected).Cells.Item(SelectedRowID).Specific.Value.ToString();


                    try
                    {
                        Form_ProductionOrder = Globals.oApp.Forms.GetForm("150", 0);
                        Form_ProductionOrder.Select();
                    }
                    catch (Exception e)
                    {
                        // Open up the Item Master Data form
                        Globals.oApp.ActivateMenuItem("3073");

                        // Grab the "Item Master" form from the UI
                        Form_ProductionOrder = Globals.oApp.Forms.GetForm("150", 0);

                        // Freeze the "Item Master" form
                        Form_ProductionOrder.Freeze(true);

                        // Change the "Item Master" form to find mode
                        Form_ProductionOrder.Mode = BoFormMode.fm_FIND_MODE;

                        // Insert the Item Master ID in the appropriate text field
                        ((EditText)Form_ProductionOrder.Items.Item("5").Specific).Value = ItemSelected;

                        // Click the "Find" button
                        ((Button)Form_ProductionOrder.Items.Item("1").Specific).Item.Click(BoCellClickType.ct_Regular);

                        // Un-Freeze the "Production Order" form
                        Form_ProductionOrder.Freeze(false);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(Form_ProductionOrder);
                GC.Collect();
            }
        }

        #region Data Classes For Ease Of Use Of Multi-Selected Rows.

        internal class WetBudDataTransfer
        {
            public List<WetBudSelectedRow> RowsToTransfer = new List<WetBudSelectedRow>();
            public DateTime DueDate;

            /// <summary>
            /// We currently are not using this functionality. Since we wanted to be able to track down how much loss happens from each batch of Wet to Dry.
            /// </summary>
            /// <returns></returns>
            public Dictionary<string, WetBudGroup> GroupByItemCodeAndWarehouse()
            {
                Dictionary<string, WetBudGroup> aggregation = new Dictionary<string, WetBudGroup>();
                foreach (WetBudSelectedRow row in RowsToTransfer)
                {
                    string dictKey = row.ItemCode + row.SourceWarehouseCode;
                    if (aggregation.ContainsKey(dictKey))
                    {
                        // Dictionary key already exists. Add our row to the group and quantity total.
                        aggregation[dictKey].TotalQuantity += row.Quantity;
                        aggregation[dictKey].GoodsIssuedLines.Add(new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches() { BatchNumber = row.BatchNo, Quantity = row.Quantity });
                    }
                    else
                    {
                        // Add a new entry to our Dictionary of WetBudGroups.
                        WetBudGroup newGroup = new WetBudGroup();
                        newGroup.ItemCode = row.ItemCode;
                        newGroup.WarehouseCode = row.SourceWarehouseCode;
                        newGroup.TotalQuantity = row.Quantity;
                        newGroup.DestinationWarehouseCode = row.DestinationWarehouseCode;
                        newGroup.GoodsIssuedLines.Add(new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches() { BatchNumber = row.BatchNo, Quantity = row.Quantity });
                        aggregation.Add(dictKey, newGroup);
                    }
                }

                return aggregation;
            }
        }

        // We currently are not using this functionality. Since we wanted to be able to track down how much loss happens from each batch of Wet to Dry.
        internal class WetBudGroup
        {
            public List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches> GoodsIssuedLines = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>();
            public double TotalQuantity;
            public string ItemCode;
            public string WarehouseCode;
            public string DestinationWarehouseCode;
        }

        internal class WetBudSelectedRow
        {
            public string ItemCode;
            public double Quantity;
            public string SourceWarehouseCode;
            public string BatchNo;
            public string DestinationWarehouseName;
            public string DestinationWarehouseCode;

            public string GroupName { get; set; }
        }

        #endregion

    }
}

