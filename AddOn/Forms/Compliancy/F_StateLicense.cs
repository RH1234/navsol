﻿using SAPbouiCOM;

namespace NavSol.Forms.Compliancy
{
    class F_StateLicense 
    {  
 /*	// COMPLIANCE
        public StateLicense(Application SAPBusinessOne_Application, Company SAPBusinessOne_Company)
        {
            // this.FormUID = "VSC_STATELIC";
            this.MenuText = "State License and Credentials";
            this.SubMenu = SubMenus.MENU_COMP;
            this.MenuPosition = 1;

            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
            Helpers.Forms = new Helpers.Forms(SAPBusinessOne_Application: Globals.oApp);
        }

        override public void ItemEventHandler(string FormUID, ref ItemEvent pVal, out bool BubbleEvent)
        {
            // Prepare the private form field
            _SBO_Form = Helpers.Forms.GetFormFromUID(cFormID);

            // Assume we want to complete the bubble event unless otherwise noted
            BubbleEvent = true;

            switch (pVal.BeforeAction)
            {
                // After SAP Events have fired
                case false:
                    switch (pVal.EventType)
                    {
                        // An item has been pressed
                        case BoEventTypes.et_ITEM_PRESSED:

                            // Based off of the item that fired the event
                            switch (pVal.ItemUID)
                            {
                                case "BTN_SAVE":
                                    BTN_SAVE_Click();
                                    break;
                            }

                            break;

                    }
                    break;
            }
        }

        override public void Form_Load()
        {
            // Load form from XML
            Helpers.Forms.ShowFormFromXMLFile(FormXMLLocation);

            // If the form displayed properly, or is already in view
            if (Helpers.Forms.WasSuccessful || Helpers.Forms.LastErrorMessage.Contains("[66000-11]"))
            {
                // Set our private form field
                _SBO_Form = Helpers.Forms.GetFormFromUID(cFormID);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                // Prepare a settings controller
                NSC_DI.SAP.Settings _VirSci_Controllers_Settings = new Controllers.Settings(SAPBusinessOne_Application: Globals.oApp, SAPBusinessOne_Company: Globals.oCompany, SAPBusinessOne_Form: _SBO_Form);

                // Set the textboxes to their settings value from the database
                

                EditText TXT_USER = Helpers.Forms.GetControlFromForm(ItemType: Helpers.Forms.FormControlTypes.EditText, ItemUID: "TXT_USER", FormToSearchIn: _SBO_Form);
                string stateUser = _VirSci_Controllers_Settings.GetValueOfSetting("StateUser");
                TXT_USER.Value = stateUser;

                EditText TXT_PASS = Helpers.Forms.GetControlFromForm(ItemType: Helpers.Forms.FormControlTypes.EditText, ItemUID: "TXT_PASS", FormToSearchIn: _SBO_Form);
                string statePass = _VirSci_Controllers_Settings.GetValueOfSetting("StatePass");
                TXT_PASS.Value =statePass;
                
                EditText TXT_UBI = Helpers.Forms.GetControlFromForm(ItemType: Helpers.Forms.FormControlTypes.EditText, ItemUID: "TXT_UBI", FormToSearchIn: _SBO_Form);
                string stateUBI = _VirSci_Controllers_Settings.GetValueOfSetting("StateLic");
                TXT_UBI.Value = stateUBI;

                EditText TXT_LICN = Helpers.Forms.GetControlFromForm(ItemType: Helpers.Forms.FormControlTypes.EditText, ItemUID: "TXT_LICN", FormToSearchIn: _SBO_Form);
                string stateLICN=_VirSci_Controllers_Settings.GetValueOfSetting("StateLocLic");
                TXT_LICN.Value = stateLICN;

                CheckBox CHK_TRAIN = Helpers.Forms.GetControlFromForm(
                ItemType: Helpers.Forms.FormControlTypes.CheckBox,
                ItemUID: "CHK_TRAIN",
                FormToSearchIn: _SBO_Form);

                string StateTrainMode = _VirSci_Controllers_Settings.GetValueOfSetting("StateTrainMode");
                if (StateTrainMode != null)
                {
                    StateTrainMode = StateTrainMode.ToLower();
                    if (StateTrainMode == "y" || StateTrainMode == "n")
                    {
                        CHK_TRAIN.Checked = StateTrainMode.Equals("y");
                    }
                }


                // Set the main image
                ((PictureBox)Helpers.Forms.GetControlFromForm(Helpers.Forms.FormControlTypes.PictureBox, "IMG_Main", FormToSearchIn: _SBO_Form)).Picture = Globals.pathToImg + @"\Icons\" + "gov-icon.bmp";

                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                // Show the form
                _SBO_Form.VisibleEx = true;
            }
        }

        private void BTN_SAVE_Click()
        {
            NSC_DI.SAP.Settings _VirSci_Controllers_Settings = new Controllers.Settings(SAPBusinessOne_Application: Globals.oApp, SAPBusinessOne_Company: Globals.oCompany, SAPBusinessOne_Form: _SBO_Form);

            // Save the settings in the textbox to the database
            _VirSci_Controllers_Settings.SetSettingValue("StateUser", ((EditText)Helpers.Forms.GetControlFromForm(ItemType: Helpers.Forms.FormControlTypes.EditText, ItemUID: "TXT_USER", FormToSearchIn: _SBO_Form)).Value);
            _VirSci_Controllers_Settings.SetSettingValue("StatePass", ((EditText)Helpers.Forms.GetControlFromForm(ItemType: Helpers.Forms.FormControlTypes.EditText, ItemUID: "TXT_PASS", FormToSearchIn: _SBO_Form)).Value);
            _VirSci_Controllers_Settings.SetSettingValue("StateLic", ((EditText)Helpers.Forms.GetControlFromForm(ItemType: Helpers.Forms.FormControlTypes.EditText, ItemUID: "TXT_UBI", FormToSearchIn: _SBO_Form)).Value);
            _VirSci_Controllers_Settings.SetSettingValue("StateLocLic", ((EditText)Helpers.Forms.GetControlFromForm(ItemType: Helpers.Forms.FormControlTypes.EditText, ItemUID: "TXT_LICN", FormToSearchIn: _SBO_Form)).Value);


            CheckBox CHK_TRAIN = Helpers.Forms.GetControlFromForm(
                ItemType: Helpers.Forms.FormControlTypes.CheckBox,
                ItemUID: "CHK_TRAIN",
                FormToSearchIn: _SBO_Form);
            string CHK_TRAIN_CHKD = "y"; //default to training mode
            if (CHK_TRAIN.Checked) { CHK_TRAIN_CHKD = "y"; }
            else { CHK_TRAIN_CHKD = "n"; }
            _VirSci_Controllers_Settings.SetSettingValue("StateTrainMode", CHK_TRAIN_CHKD);



            Globals.oApp.StatusBar.SetText("Successfully saved state licensing information!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
        }
*/
    }
}
