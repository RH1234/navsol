﻿using System;
using System.Collections.Generic;
using System.Drawing;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms
{
    public class F_ComplianceMonitor : B1Event
    {
        #region ---------------------------------- VARS, CLASSES ---------------------------------
        private const string cFormID = "NSC_COMPLIANCEMON";

        private static string sTmp = "";
        private static string sql = "";

        public static string XMLResponse { get; set; }
        public static string XMLRequest { get; set; }
        public static string CompliancyID { get; set; }

        #endregion VARS

        #region ---------------------------------- BEFORE EVENT ----------------------------------
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  ----------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "mtx_comp":
                    if (pVal.Row > 0) Mtx_Comp(oForm, pVal.Row);
                    break;

                //case "btnResolve":
                //    ResendCompliancy(oForm);
                //    break;

                case "btnLoad":
                    LoadMatrix_Compliance(oForm);
                    break;

                case "btnFailed":
                    LoadMatrix_Compliance_Failed(oForm);
                    break;

                //case "btnCStatus":
                //    SetCompliancyAsResolved(oForm);
                //    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        private static void FormSetup(Form pForm)
        {
            SAPbouiCOM.EditText oFld = null;
            try
            {
                pForm.DataSources.UserDataSources.Add("DS_FromD", SAPbouiCOM.BoDataType.dt_DATE, 254);
                oFld = pForm.Items.Item("txtFromD").Specific;
                oFld.DataBind.SetBound(true, "", "DS_FromD");
                oFld.Value = NSC_DI.UTIL.Dates.GetForUI(DateTime.Today.AddDays(-14));

                pForm.DataSources.UserDataSources.Add("DS_ToD", SAPbouiCOM.BoDataType.dt_DATE, 254);
                oFld = pForm.Items.Item("txtToD").Specific;
                oFld.DataBind.SetBound(true, "", "DS_ToD");
                oFld.Value = NSC_DI.UTIL.Dates.GetForUI(DateTime.Today);
                pForm.Items.Item("btnLoad").Visible = false;

                LoadMatrix_Compliance(pForm);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                pForm.VisibleEx = true;
                NSC_DI.UTIL.Misc.KillObject(oFld);
                GC.Collect();
            }
        }

        public static Form FormCreate(bool pSingleInstance = true, string pCallingFormUID = null)
        {
            Item oItm = null;
            try
            {
                var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
                {
                    oItm = oForm.Items.Add("txtPare", BoFormItemTypes.it_EDIT);
                    oItm.Top = -20;
                    oItm.Specific.Value = pCallingFormUID;
                }

                FormSetup(oForm);
                return oForm;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private static void LoadMatrix_Compliance(Form pForm)
        {
            string matrix_uid = "mtx_comp";
            Matrix oMat = null;

            try
            {
                var sql = $@"
SELECT Code, U_TransID, U_DateCreated, U_Status, U_Action, U_CompKey, U_KeyType, U_ItemCode, U_DocNum,
	CASE 
	WHEN SUBSTRING(U_DocNum, 1, 1) = 'P' THEN 'PdO'
	WHEN SUBSTRING(U_DocNum, 1, 1) = 'R' THEN 'Goods Receipt'
	WHEN SUBSTRING(U_DocNum, 1, 1) = 'T' THEN 'Inventory Transfer'
	ELSE ''
	END AS Doctype,
U_APICall, U_Response
FROM [@{NSC_DI.Globals.tCompliance}] 
WHERE U_DateCreated BETWEEN '{pForm.Items.Item("txtFromD").Specific.Value.ToString()}' AND '{pForm.Items.Item("txtToD").Specific.Value.ToString()}'
 ORDER BY CONVERT(int,[Code]) ASC";

                // Load the matrix of tasks
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "COMP"
                , matrix_uid
                , new List<CommonUI.Matrix.MatrixColumn>() {
                         new CommonUI.Matrix.MatrixColumn(){ Caption="ID",          ColumnWidth=40, ColumnName="Code",          ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Transaction ID",  ColumnWidth=80, ColumnName="U_TransID", ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Sync Date",  ColumnWidth=80, ColumnName="U_DateCreated", ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Status",      ColumnWidth=80, ColumnName="U_Status",      ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Action",      ColumnWidth=40, ColumnName="U_Action",      ItemType = BoFormItemTypes.it_EDIT} // post, put, update, delete, get
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Key",         ColumnWidth=80, ColumnName="U_CompKey",     ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="KeyType",     ColumnWidth=80, ColumnName="U_KeyType",     ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="ItemCode",    ColumnWidth=80, ColumnName="U_ItemCode",    ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Doc Num",     ColumnWidth=80, ColumnName="U_DocNum",      ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Doc Type",    ColumnWidth=80, ColumnName="DocType",     ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="API Call",    ColumnWidth=80, ColumnName="U_APICall",     ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Response",    ColumnWidth=80, ColumnName="U_Response",    ItemType = BoFormItemTypes.it_EDIT}
                    }
                , sql
            );

                // Find the matrix in the form
                oMat = pForm.Items.Item(matrix_uid).Specific;

                // Change row color of items in matrix based off of the compliance status
                CommonSetting mtx_comp_settings = oMat.CommonSetting;
                for (int i = 1; i < (oMat.RowCount + 1); i++)
                {
                    EditText oItem = oMat.Columns.Item(2).Cells.Item(i).Specific as EditText;

                    // Load row color based off of status
                    switch (oItem.Value.ToString())
                    {
                        // only 2 status
                        //case "O": // Open
                        //    mtx_comp_settings.SetRowBackColor(i, ColorTranslator.ToWin32(Color.LightYellow));
                        //    break;

                        case "S": // Success
                            mtx_comp_settings.SetRowBackColor(i, ColorTranslator.ToWin32(Color.LightGreen));
                            break;

                        case "F": // Failed
                            mtx_comp_settings.SetRowBackColor(i, ColorTranslator.ToWin32(Color.LightPink));
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }

        private static void LoadMatrix_Compliance_Failed(Form pForm)
        {
            string matrix_uid = "mtx_comp";
            Matrix oMat = null;

            try
            {
                var sql = $@"
SELECT Code, U_TransID, U_DateCreated, U_Status, U_Action, U_CompKey, U_KeyType, U_ItemCode, U_DocNum,
	CASE 
	WHEN SUBSTRING(U_DocNum, 1, 1) = 'P' THEN 'PdO'
	WHEN SUBSTRING(U_DocNum, 1, 1) = 'R' THEN 'Goods Receipt'
	WHEN SUBSTRING(U_DocNum, 1, 1) = 'T' THEN 'Inventory Transfer'
	ELSE ''
	END AS Doctype,
U_APICall, U_Response
FROM [@NSC_COMPLIANCE_LOG] 
 WHERE U_DateCreated BETWEEN '{pForm.Items.Item("txtFromD").Specific.Value.ToString()}' AND '{pForm.Items.Item("txtToD").Specific.Value.ToString()}'
   AND [U_Status] = 'F'
 ORDER BY CONVERT(int,[Code]) DESC";

                // Load the matrix of tasks
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "COMP"
                , matrix_uid
                , new List<CommonUI.Matrix.MatrixColumn>() {
                         new CommonUI.Matrix.MatrixColumn(){ Caption="ID",          ColumnWidth=40, ColumnName="Code",          ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Created On",  ColumnWidth=80, ColumnName="U_DateCreated", ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Status",      ColumnWidth=80, ColumnName="U_Status",      ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Action",      ColumnWidth=40, ColumnName="U_Action",      ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Key",         ColumnWidth=80, ColumnName="U_CompKey",     ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="KeyType",     ColumnWidth=80, ColumnName="U_KeyType",     ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="ItemCode",    ColumnWidth=80, ColumnName="U_ItemCode",    ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Doc Num",     ColumnWidth=80, ColumnName="U_DocNum",      ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Doc Type",    ColumnWidth=80, ColumnName="DocType",       ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="API Call",    ColumnWidth=80, ColumnName="U_APICall",     ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Response",    ColumnWidth=80, ColumnName="U_Response",    ItemType = BoFormItemTypes.it_EDIT}
                    }
                , sql
            );

                // Find the matrix in the form
                oMat = pForm.Items.Item(matrix_uid).Specific;

                // Change row color of items in matrix based off of the compliance status
                CommonSetting mtx_comp_settings = oMat.CommonSetting;
                for (int i = 1; i < (oMat.RowCount + 1); i++)
                {
                    EditText oItem = oMat.Columns.Item(2).Cells.Item(i).Specific as EditText;

                    // Load row color based off of status
                    switch (oItem.Value.ToString())
                    {
                        // only 2 status
                        //case "O": // Open
                        //    mtx_comp_settings.SetRowBackColor(i, ColorTranslator.ToWin32(Color.LightYellow));
                        //    break;

                        case "S": // Success
                            mtx_comp_settings.SetRowBackColor(i, ColorTranslator.ToWin32(Color.LightGreen));
                            break;

                        case "F": // Failed
                            mtx_comp_settings.SetRowBackColor(i, ColorTranslator.ToWin32(Color.LightPink));
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }

        //private static void ResendCompliancy(Form pForm)
        //{
        //    if (string.IsNullOrEmpty(XMLRequest))
        //    {
        //        Globals.oApp.StatusBar.SetText("Request is not specified. Please select the item again!", BoMessageTime.bmt_Short);
        //        return;
        //    }

        //    /*	// COMPLIANCE
        //                            BioTrack.API btAPI = new Controllers.TraceabilityAPI(
        //                                SAPBusinessOne_Application: _SBO_Application,
        //                                SAPBusinessOne_Company: _SBO_Company).new_API_obj();

        //                            XElement xmlRequest = XElement.Parse(XMLRequest);

        //                            btAPI.XmlApiRequest = xmlRequest;

        //                            SetComplaincyIdOnSalesOrder(salesOrderId: salesOrderId, complaincyId: compID);

        //                            btAPI.PostToApi();
        //                            if (btAPI.WasSuccesful)
        //                            {
        //                                CompCon.UpdateComliancy(
        //                                    ComplianceID: CompliancyID,
        //                                    ResponseXML: btAPI.xDocFromResponse.ToString(),
        //                                    Status: Controllers.Compliance.ComplianceStatus.Success);

        //                                Globals.oApp.StatusBar.SetText("Compliancy call was successful!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

        //                                LoadMatrix_Compliance_Failed(pForm);
        //                            }
        //                            else
        //                            {
        //                                Controllers.Compliance.UpdateComliancy(CompliancyID, btAPI.xDocFromResponse.ToString(), Controllers.Compliance.ComplianceStatus.Failed);

        //                                Globals.oApp.StatusBar.SetText("Compliancy call failed!",BoMessageTime.bmt_Short);
        //                            }
        //    */
        //}

        //private static void SetCompliancyAsResolved(Form pForm)
        //{
        //    EditText txtReason = null;

        //    txtReason = pForm.Items.Item("txtReason").Specific;
        //    if (txtReason == null || string.IsNullOrEmpty(txtReason.Value))
        //    {
        //        Globals.oApp.StatusBar.SetText("Please supply a reason!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
        //        return;
        //    }

        //    //NSC_DI.API.Compliance.Log.UpdateComliancy(CompliancyID, txtReason.Value, NSC_DI.Globals.ComplianceStatus.Success, txtReason.Value);

        //    Globals.oApp.StatusBar.SetText("Set complinacy line status to passed", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

        //    LoadMatrix_Compliance_Failed(pForm);
        //}

        public static void SetComplaincyIdOnSalesOrder(int salesOrderId, string complaincyId)
        {
            Documents salesOrderDocument = Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders) as Documents;

            // Attempt to get the production order by its key
            if (salesOrderDocument.GetByKey(salesOrderId))
            {
                try
                {
                    salesOrderDocument.GetByKey(salesOrderId);

                    salesOrderDocument.UserFields.Fields.Item("U_NSC_CompID").Value = complaincyId;

                    if (salesOrderDocument.Update() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }
                catch (Exception ex)
                {
                    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }
            }
        }

        private static void Mtx_Comp(Form pForm, int pRow)
        {
            Matrix mtx_comp = null;
           // Button btnResolve = null;
           // Button btnCStatus = null;
           // EditText txtReason = null;

            try
            {
                mtx_comp = pForm.Items.Item("mtx_comp").Specific;
                //btnResolve = pForm.Items.Item("btnResolve").Specific;
                //btnCStatus = pForm.Items.Item("btnCStatus").Specific;

                // User clicked 
                if (mtx_comp == null || mtx_comp.RowCount < pRow)
                {
                    return;
                }

                EditText txtCompliancy = ((EditText)mtx_comp.Columns.Item(0).Cells.Item(pRow).Specific);
                EditText txtStatus = ((EditText)mtx_comp.Columns.Item(2).Cells.Item(pRow).Specific);
                EditText mtxReason = ((EditText)mtx_comp.Columns.Item(1).Cells.Item(pRow).Specific);

                if (txtCompliancy == null || string.IsNullOrEmpty(txtCompliancy.Value))
                {
                    return;
                }

                if (txtStatus == null || string.IsNullOrEmpty(txtStatus.Value))
                {
                    return;
                }

                if (mtxReason == null || string.IsNullOrEmpty(mtxReason.Value))
                {
                    return;
                }

                //txtReason = pForm.Items.Item("txtReason").Specific;

                // Get the ID of the Effect selected
                XMLResponse = mtxReason.Value;
                CompliancyID = txtCompliancy.Value;

                bool isFailedComplaince = txtStatus.Value == "1" ? false : true;

                if (isFailedComplaince)
                {
                    mtx_comp.SelectionMode = BoMatrixSelect.ms_Single;
                    mtx_comp.SelectRow(pRow, true, false);

                    //btnCStatus.Item.Enabled = true;
                    //btnResolve.Item.Enabled = true;
                    XMLRequest = ((EditText)mtx_comp.Columns.Item(4).Cells.Item(pRow).Specific).Value.ToString();
                    //txtReason.Item.Enabled = true;
                }
                else
                {
                    //btnCStatus.Item.Enabled = false;
                    //btnResolve.Item.Enabled = false;
                    XMLRequest = string.Empty;
                    CompliancyID = string.Empty;

                    //txtReason.Item.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }
    }
}