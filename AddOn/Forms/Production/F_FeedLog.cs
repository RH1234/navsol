﻿using System;
using System.Collections.Generic;
using SAPbouiCOM;
using SAPbobsCOM;
using B1WizardBase;

namespace NavSol.Forms.Production
{
    class F_FeedLog : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "NSC_FEED_LOG";
        private const string cItemType = "";

        // FOR OLD CODE
        public VERSCI.Forms _VirSci_Helper_Form;
        public SAPbouiCOM.Application _SBO_Application;
        public SAPbobsCOM.Company _SBO_Company;
        public SAPbouiCOM.Form _SBO_Form;
        private SAPbouiCOM.ProgressBar oProgBar;

        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------


        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------


        //--------------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
        [B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "MTX_MAIN":

                    int SelectedRowID = pVal.Row;

                    if (SelectedRowID > 0)
                    {
                        // Which column stores the ID
                        int ColumnIDForIDOfItemSelected = 0;

                        // Grab the matrix from the Form UI
                        Matrix MTX_MAIN = (Matrix)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

                        // Get the ID of the note selected
                        string ItemSelected = ((EditText)MTX_MAIN.Columns.Item(ColumnIDForIDOfItemSelected).Cells.Item(SelectedRowID).Specific).Value.ToString();
                        
                        F_PlantDetails.FormCreate(pPlantID: ItemSelected);
                    }
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT
        public F_FeedLog() : this(Globals.oApp, Globals.oCompany) { }

        private F_FeedLog(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            // this.FormUID = "VSC_FEED_LOG";
           // this.MenuText = "Feed Log";
            //this.SubMenu = SubMenus.MENU_PROD;
            //this.MenuPosition = 3;

            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

        public void Form_Load()
        {
            _SBO_Form = CommonUI.Forms.Load(cFormID, true);

            _SBO_Form.Freeze(true);
            
            // Load the matrix of water events
            CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm: _SBO_Form,
                DatabaseTableName: "@" + NSC_DI.Globals.tFeedLog,
                MatrixUID: "MTX_MAIN",
                ListOfColumns: new List<CommonUI.Matrix.MatrixColumn>() {
                    new CommonUI.Matrix.MatrixColumn(){ Caption="Plant ID", ColumnWidth=40, ColumnName="U_"+ Globals.SAP_PartnerCode +"_PlantID", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                    ,new CommonUI.Matrix.MatrixColumn(){ Caption="Log Date", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_DateCreated", ItemType = BoFormItemTypes.it_EDIT}      
                    ,new CommonUI.Matrix.MatrixColumn(){ Caption="Item Code", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_ItemCode", ItemType = BoFormItemTypes.it_EDIT}   
                    ,new CommonUI.Matrix.MatrixColumn(){ Caption="Amount", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_Amount", ItemType = BoFormItemTypes.it_EDIT}  
                    ,new CommonUI.Matrix.MatrixColumn(){ Caption="PPM", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_ADPPM", ItemType = BoFormItemTypes.it_EDIT}
                    ,new CommonUI.Matrix.MatrixColumn(){ Caption="TDS", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_ADTDS", ItemType = BoFormItemTypes.it_EDIT}
                    ,new CommonUI.Matrix.MatrixColumn(){ Caption="EC", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_ADEC", ItemType = BoFormItemTypes.it_EDIT}
                    ,new CommonUI.Matrix.MatrixColumn(){ Caption="Temperature", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_TEMP", ItemType = BoFormItemTypes.it_EDIT}
                    ,new CommonUI.Matrix.MatrixColumn(){ Caption="PH", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_PH", ItemType = BoFormItemTypes.it_EDIT}
                }
            );

            // Set the main image
            ((PictureBox)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.PictureBox, "IMG_MAIN", _SBO_Form)).Picture = Globals.pathToImg + @"\Icons\" + "feed-icon.bmp";

            // Grab the matrix from the form ui
            Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

            // Variables to calculate the total and today's total.
            double total = 0;
            double total_today = 0;

            // We are specifying columns we want to grab. Since title is the Caption I've put the exact same caption here.
            string amountColumnName = "Amount";
            string dateCreatedColumnName = "Log Date";

            // Created a Matrix Column Collector helper to get the columns that I specify by Caption Name.
            CommonUI.MatrixColumnCollector helperCollector = new CommonUI.MatrixColumnCollector(new List<string>() { amountColumnName, dateCreatedColumnName }, MTX_MAIN);

            // Adding in a ColumnSortOption for Log Date.  We use the CaptionName. Hopefully this can be changed to something more defining?
            CommonUI.MatrixSortHelper.AddColumnSortOption(new CommonUI.MatrixSortHelper.ColumnSortOption("Log Date", true, BoGridSortType.gst_Descending
                ));

            // Apply the Sort on the Matrix.
            CommonUI.MatrixSortHelper.ApplySort(MTX_MAIN);


            // Validate that the Indexs of the Columns we need are valid.
            int amountColumnIndex = helperCollector.GetIndexOfColumn(amountColumnName);
            int dateCreatedColumnIndex = helperCollector.GetIndexOfColumn(dateCreatedColumnName);

            // If our index's are -1 we don't want to use them since we didn't find a column we will be using for calculations.
            if (amountColumnIndex == -1 || dateCreatedColumnIndex == -1)
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Failed to calculate today's and total feed useage."
                );
                return;
            }

            // For each row in the matrix
            for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
            {
                // Grab the amount and date from the Matrix.
                double amount = Convert.ToDouble(((EditText)MTX_MAIN.Columns.Item(amountColumnIndex).Cells.Item(i).Specific).Value.ToString());
                string date_string = ((EditText)MTX_MAIN.Columns.Item(1).Cells.Item(dateCreatedColumnIndex).Specific).Value.ToString();

                // Get the date parts from the SAP string
                string year = date_string.Substring(0, 4);
                string month = date_string.Substring(4, 2);
                string day = date_string.Substring(6, 2);

                // Removed conversion's currently. Not sure if we want to support this later?
                double amount_to_add = amount;

                // Add to total amount
                total += amount_to_add;

                // If the amount was added today, keep track
                if (Convert.ToDateTime(month + "/" + day + "/" + year).Date == DateTime.Now.Date)
                {
                    total_today += amount_to_add;
                }
            }

            // Set the textbox for the total amount of water used.
            ((EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_TOTAL", _SBO_Form)).Value =  total.ToString("0.0");
            ((EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_TODAY", _SBO_Form)).Value = total_today.ToString("0.0");

            // Un-Freeze the Form UI
            _SBO_Form.Freeze(false);

            // Show the form
            _SBO_Form.VisibleEx = true;
            
        }
    }
}