﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using Matrix = NavSol.VERSCI.Matrix;

namespace NavSol.Forms.Production
{
    class F_FeedWizard : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "NSC_FEED_WIZ";
        //Used for CA batch management and general hemp to determine which production order to send it to
        public static string CurrentPhase { get; set; }
        private static bool prntFrmMthrPltMgr = false;

        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			if (pVal.ItemUID == "BTN_OK") BTN_OK_Pressed(oForm);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
		[B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
		public virtual void OnAfterChooseFromList(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            switch (pVal.ItemUID)
            {

                // Item ChooseFromList
                case "TXT_ADITEM":
                    TXT_ADITEM_ChooseFromList_Selected(oForm, (IChooseFromListEvent)pVal);
                    break;

            } 
            //if (pVal.ItemUID == "TXT_ADITEM") TXT_ADITEM_ChooseFromList_Selected(oForm, (IChooseFromListEvent)pVal);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			try
			{
				pForm.Freeze(true);

				pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

        public static void Form_Load(bool isMotherPlantMngr, string[] ListOfPlantIDsToLoad, string pCurrentPhase = null)
        { 
            // Set our private form field
            Form oForm = FormCreate(true);
            prntFrmMthrPltMgr = isMotherPlantMngr;
            // Prepare the SQL statement that will load the selected plants data into the matrix
            string sql = null;
            // Prepare a Matrix form helper
            Matrix oMatrixHelper = new Matrix(Globals.oApp, oForm);

            if ((prntFrmMthrPltMgr) || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && pCurrentPhase == "FLO"))
            {
                CurrentPhase = pCurrentPhase;
                sql = @"
SELECT DISTINCT
(CONVERT(nvarchar,[OSRN].[DistNumber])) AS [UniqueID]
,(SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) AS [WhsCode]
,[OSRN].[CreateDate], [OSRN].[itemName],OSRN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]
FROM [OSRN]
left Join OPMG on OPMG.AbsEntry = OSRN.U_NSC_CropID
WHERE (SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) IN 
(SELECT [OWHS].[WhsCode] FROM [OWHS] WHERE ";

                foreach (string PlantID in ListOfPlantIDsToLoad)
                {
                    sql += " (CONVERT(nvarchar,[OSRN].[DistNumber]))  = '" + PlantID + "' OR";
                }

                // Remove the last "OR" in the SQL query and close off the subquery
                sql = sql.Substring(0, sql.Length - 2) + ")";
 
                // Load data about selected plants into the matrix
                oMatrixHelper.LoadDatabaseDataIntoMatrix("OSRN", "MTX_PLAN", new List<Matrix.MatrixColumn>() {
                        new Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new Matrix.MatrixColumn(){ ColumnName="CropID",               Caption="Crop ID",               ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new Matrix.MatrixColumn(){ ColumnName="CropName",             Caption="Crop Name",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new Matrix.MatrixColumn(){ ColumnName="WhsCode",             Caption="Warehouse",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                    }, sql
                    );
            }
            else
            {
                CurrentPhase = pCurrentPhase;
                sql = @"
SELECT DISTINCT
(CONVERT(nvarchar,[OBTN].[DistNumber])) AS [UniqueID]
--,(SELECT [OIBT].[WhsCode] FROM [OIBT] WHERE [OIBT].[ItemCode] = [OBTN].[ItemCode] AND [OIBT].BatchNum= [OBTN].DistNumber) AS [WhsCode]
,[OIBT].WhsCode
,[OBTN].[CreateDate], [OBTN].[itemName],OBTN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]
FROM [OBTN]
join OIBT on OIBT.BatchNum = OBTN.DistNumber  and [OIBT].[ItemCode] = [OBTN].[ItemCode]
left Join OPMG on OPMG.AbsEntry = OBTN.U_NSC_CropID
inner join OWHS on OIBT.WhsCode = OWHS.WhsCode 
inner join [@NSC_PDO_PHASES] T0 on T0.U_CurrentPhase = OWHS.U_NSC_WhrsType 
WHERE (SELECT [OIBT].[WhsCode] FROM [OIBT] WHERE [OIBT].[ItemCode] = [OBTN].[ItemCode] AND [OIBT].BatchNum = [OBTN].DistNumber  and [OIBT].Quantity>0 ) IN 
(SELECT [OWHS].[WhsCode] FROM [OWHS] WHERE ";

                foreach (string PlantID in ListOfPlantIDsToLoad)
                {
                    sql += " (CONVERT(nvarchar,[OBTN].[DistNumber]))  = '" + PlantID + "' OR";
                }

                // Remove the last "OR" in the SQL query and close off the subquery
                sql = sql.Substring(0, sql.Length - 2) + ")  and [OIBT].Quantity>0 and [OIBT].WhsCode!='9999' ";
                // Load data about selected plants into the matrix
                oMatrixHelper.LoadDatabaseDataIntoMatrix("OBTN", "MTX_PLAN", new List<Matrix.MatrixColumn>() {
                        new Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new Matrix.MatrixColumn(){ ColumnName="CropID",               Caption="Crop ID",               ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new Matrix.MatrixColumn(){ ColumnName="CropName",             Caption="Crop Name",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new Matrix.MatrixColumn(){ ColumnName="WhsCode",             Caption="Warehouse",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                    }, sql
                );
            }

            // Fill the ChooseFromList of Additive Items Only.
            //TODO: change to use our magic CFL of awesome, SAP CFL can only do so much :/
            Load_ChooseFromList_AdditiveItems(oForm);

            // Set the main image
            CommonUI.Forms.SetFieldvalue.Icon(oForm, "IMG_Main", "feed-icon.bmp");

                // Grab the textbox for the PPM of additive from the form UI
				EditText TXT_ADPPM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ADPPM", oForm);

                // Grab the textbox for the TDS of additive from the form UI
				EditText TXT_ADTDS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ADTDS", oForm);

                // Grab the textbox for the TDS of additive from the form UI
				EditText TXT_ADEC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ADEC", oForm);

                // If we have only one plant selected we automatically set the selected distribution to Per plant.
                if (ListOfPlantIDsToLoad.Length == 1)
                {
                    //Only do the below if the plant is serialized if it is batched managed they may have one line but many plants in the given batch
                    if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && pCurrentPhase == "FLO"))
                    {

                        try
                        {
                            // Grab the ComboBox list for additive distribution from the UI
                            ComboBox CMB_ADIST = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_ADIST", oForm);

                            // Remove all ValidValues from our list of selections.
                            if (CMB_ADIST.ValidValues.Count > 0)
                            {
                                foreach (SAPbouiCOM.ValidValue value in CMB_ADIST.ValidValues)
                                {
                                    CMB_ADIST.ValidValues.Remove(value.Value);
                                }

                                // Set the only option for the user to Per plant.
                                CMB_ADIST.ValidValues.Add("Per", "Per plant");
                                CMB_ADIST.Select("Per");

                                // Let go of focus and set editable to false.
                                CMB_ADIST.Active = false;
                                CMB_ADIST.Item.Enabled = false;
                            }
                        }
                        catch (Exception ex)
                        {
                        throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                    }
                }
                }

                // Try to disable warehouse dropdown.
                try
                {
                    // Find the Combobox of Warehouses from the Form UI
                    ComboBox CBX_WHSE = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WHRS", oForm) as ComboBox;

                    // Set Warehouse dropdown to editable = false.
                    CBX_WHSE.Active = false;
                    CBX_WHSE.Item.Enabled = false;

                }
                catch (Exception e) {/* Log the error? */}

                TXT_ADPPM.Value = "0";
                TXT_ADTDS.Value = "0";
                TXT_ADEC.Value = "0";

                // Grab the textbox for the amount of additive from the form UI
				EditText TXT_ADAMNT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ADAMNT", oForm);
                TXT_ADAMNT.Active = true;

                // Un-Freeze the Form UI
				oForm.Freeze(false);

                // Show the form
				oForm.VisibleEx = true;
            }

		private static void Load_ChooseFromList_AdditiveItems(Form pForm)
        {
            // Find the ChooseFromList of the Additive Items we want to filter.
            // Filter Logic: WHERE ItemGrpCode = @ItemGroupCode AND OnHand > 0
			SAPbouiCOM.ChooseFromList CFL_ADITEM = pForm.ChooseFromLists.Item("CFL_ADITEM");

            // Creating condition(s) to filter the list from.
            Condition oCon = default(Condition);
            Conditions oCons = default(Conditions);

            // Removing all conditions.
            CFL_ADITEM.SetConditions(null);


            // Getting the null conditions?
            oCons = CFL_ADITEM.GetConditions();

            // Setting/Getting a new condition for our ChooseFromList.
            oCon = oCons.Add();
            oCon.BracketOpenNum = 2;
            oCon.Alias = "ItmsGrpCod";
            oCon.Operation = BoConditionOperation.co_EQUAL;
            // Apply the ItemGroup to the filter Nutrients            
            oCon.CondVal = NSC_DI.SAP.ItemGroups.GetCode<string>("Nutrients");
            oCon.Relationship = BoConditionRelationship.cr_AND;

            // Also filter by OnHand > 0 since we only care to display items that are in stock.                
            oCon = oCons.Add();
            oCon.Alias = "OnHand";
            oCon.Operation = BoConditionOperation.co_GRATER_THAN;
            oCon.CondVal = "0";
            oCon.BracketCloseNum = 1;
            oCon.Relationship = BoConditionRelationship.cr_OR;

            // Apply the ItemGroup to the filter Fungisides
            oCon = oCons.Add();
            oCon.BracketOpenNum = 1;
            oCon.Alias = "ItmsGrpCod";
            oCon.Operation = BoConditionOperation.co_EQUAL;
            oCon.CondVal = NSC_DI.SAP.ItemGroups.GetCode<string>("Additives");
            oCon.Relationship = BoConditionRelationship.cr_AND;

            // Also filter by OnHand > 0 since we only care to display items that are in stock.                
            oCon = oCons.Add();
            oCon.Alias = "OnHand";
            oCon.Operation = BoConditionOperation.co_GRATER_THAN;
            oCon.CondVal = "0";
            oCon.BracketCloseNum = 2;

            CFL_ADITEM.SetConditions(oCons);
        }

        private void LoadWarehousesByItemSelected(Form pForm, string ItemCode)
        {
            // Prepare to run a SQL statement.
            SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            // Count how many current records exist within the database.
            string sql = $@"SELECT DISTINCT [OITW].[WhsCode], [OWHS].[WhsName]
                FROM [OITW]
                JOIN [OWHS] on [OWHS].[WhsCode] = [OITW].[WhsCode]
                WHERE [OITW].ItemCode = '" + ItemCode + "'"
                   + "AND [OITW].OnHand > 0";
            if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})"; // #10823 
            oRecordSet.DoQuery(sql);          

            // Find the Combobox of Warehouses from the Form UI
            ComboBox CBX_WHSE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WHRS", pForm) as ComboBox;

            // If items already exist in the drop down
            if (CBX_WHSE.ValidValues.Count > 0)
            {
                // Remove all currently existing values from warehouse drop down
                for (int i = CBX_WHSE.ValidValues.Count; i-- > 0; )
                {
                    CBX_WHSE.ValidValues.Remove(i, BoSearchKey.psk_Index);
                }
            }

            // If more than 1 warehouses exists
            if (oRecordSet.RecordCount > 1)
            {
                // Create the first item as an empty item
                CBX_WHSE.ValidValues.Add("", "");

                // Select the empty item (forcing the user to make a decision)
                CBX_WHSE.Select(0, BoSearchKey.psk_Index);
            }

            // Add allowed warehouses to the drop down
            for (int i = 0; i < oRecordSet.RecordCount; i++)
            {
                try
                {
                    CBX_WHSE.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
                }
                catch { }
                CBX_WHSE.Item.Enabled = true;
                oRecordSet.MoveNext();
            }

            // If we only have exactly one warehouse that contains our item simply select it and disable the field.
            if (CBX_WHSE.ValidValues.Count == 1)
            {
                CBX_WHSE.Select(0, BoSearchKey.psk_Index);

                // Would disable but currently throws an error when attempting.
            }

        }

        private void TXT_ADITEM_ChooseFromList_Selected(Form pForm, IChooseFromListEvent pChooseFromListEvent)
        {
			SAPbouiCOM.ChooseFromList oChooseFromList = pForm.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

            if (pChooseFromListEvent.BeforeAction == false && pChooseFromListEvent.SelectedObjects != null && !pChooseFromListEvent.SelectedObjects.IsEmpty)
            {
                DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

                try
                {
                    string oItemCode = oDataTable.GetValue(0, 0).ToString();
                    string oItemName = oDataTable.GetValue(1, 0).ToString();

                    //if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y")
                    //{
                    //    NavSol.CommonUI.CFL.CreateWH(pForm, "CFL_WH", "CBX_WHSE");
                    //    NavSol.CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_NONE, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, firstPhase.CurrentPhase);
                    //    NavSol.CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_AND, "Inactive", BoConditionOperation.co_EQUAL, "N");
                    //}
                    //else
                        LoadWarehousesByItemSelected(pForm, oItemCode);

                    // Set the userdatasource for the name textbox
					pForm.DataSources.UserDataSources.Item("UDS_ADITEM").ValueEx = Convert.ToString(oItemName);

                    // Save the business partners code
					((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_AITEMC", pForm))
                    .Value = oItemCode;

                }
                catch (Exception ex)
                {
					Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				}
            }
        }

		private void BTN_OK_Pressed(Form pForm)
		{
			//Variables for when validating thingies
			decimal try_parse;
			bool is_parsed = false;

			try
			{
				// Grab the textbox for the amount of additive from the form UI
				EditText TXT_ADAMNT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ADAMNT", pForm);

				// Make sure a additive amount was passed
				if (string.IsNullOrEmpty(TXT_ADAMNT.Value))
				{
					Globals.oApp.StatusBar.SetText("Please enter in an amount of additive!");

					TXT_ADAMNT.Active = true;
					return;
				}

				// Make sure the amount of additive passed is an integer
				try
				{
					decimal amount = Convert.ToDecimal(TXT_ADAMNT.Value);
				}
				catch
				{
					Globals.oApp.StatusBar.SetText("Please enter a numeric value for additive amount!");

					TXT_ADAMNT.Value = "";
					TXT_ADAMNT.Active = true;
					return;
				}

				// Find the ChooseFromList of the Additive Items
				EditText TXT_ADITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ADITEM", pForm);

				// Validate that we do have a Additive Item selected.
				if (string.IsNullOrEmpty(TXT_ADITEM.Value))
				{
					Globals.oApp.StatusBar.SetText("Please enter an Additive Item!");

					TXT_ADITEM.Active = true;
					return;
				}

				// Get the ItemCode of the Additive that was selected.
				EditText TXT_AITEMC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_AITEMC", pForm);

				if (string.IsNullOrEmpty(TXT_AITEMC.Value))
				{
					Globals.oApp.StatusBar.SetText("Please enter an Additive Item!");

					TXT_AITEMC.Active = true;
					return;
				}

				// Grab the ComboBox list for warehouse from the UI
				ComboBox CMB_WHRS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WHRS", pForm);

				// Make sure a warehouse was selected
				if (string.IsNullOrEmpty(CMB_WHRS.Value))
				{
					Globals.oApp.StatusBar.SetText("Please select a warehouse where the additive is located!");

					CMB_WHRS.Active = true;
					return;
				}

				// Get the "description" from the ComboBox, where we are really storing the value
				string SelectedDestinationWarehouseID = CommonUI.ComboBox.GetComboBoxValueOrDescription(ref pForm, "CMB_WHRS", null, CMB_WHRS.Value);

				// Grab the textbox for the PPM of additive from the form UI
				EditText TXT_ADPPM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ADPPM", pForm);

				// Make sure a additive PPM was passed
				if (string.IsNullOrEmpty(TXT_ADPPM.Value))
				{
					Globals.oApp.StatusBar.SetText("Please enter in a PPM amount!");

					TXT_ADPPM.Active = true;
					return;
				}

				// Grab the textbox for the TDS of additive from the form UI
				EditText TXT_ADTDS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ADTDS", pForm);

				// Make sure a additive TDS was passed
				if (string.IsNullOrEmpty(TXT_ADTDS.Value))
				{
					Globals.oApp.StatusBar.SetText("Please enter in a TDS amount!");

					TXT_ADTDS.Active = true;
					return;
				}

				// Grab the textbox for the TDS of additive from the form UI
				EditText TXT_ADEC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ADEC", pForm);

				// Make sure a additive EC was passed
				if (string.IsNullOrEmpty(TXT_ADEC.Value))
				{
					Globals.oApp.StatusBar.SetText("Please enter in an EC amount!");

					TXT_ADEC.Active = true;
					return;
				}

				// Grab the matrix of selected plants from the form UI
				SAPbouiCOM.Matrix MTX_PLAN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLAN", pForm);

				// Keep track of the selected Plant ID's
				List<string> SelectedPlantIDs = new List<string>();

				// For each row already selected
				for (int i = 1; i < (MTX_PLAN.RowCount + 1); i++)
				{
					// Grab the selected row's Plant ID column
					string plantID = ((EditText) MTX_PLAN.Columns.Item(0).Cells.Item(i).Specific).Value;

					// Add the selected row plant ID to list
					SelectedPlantIDs.Add(plantID);
				}

				// Grab the amount of additive we are applying to the plants
				double AdditiveToApply = Convert.ToDouble(TXT_ADAMNT.Value);

				// Grab the ComboBox list for additive distribution from the UI
				ComboBox CMB_ADIST = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_ADIST", pForm);

				// Validate that we have a distribution selected.
				if (string.IsNullOrEmpty(CMB_ADIST.Value))
				{
					Globals.oApp.StatusBar.SetText("Please enter a distribution type!");

					CMB_ADIST.Active = true;
					return;
				}

                //Get Temperature of additive
                EditText TXT_TEMP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_TEMP", pForm);
                
				if (!string.IsNullOrEmpty(TXT_TEMP.Value?.Trim()))// Whitespace-Change (!NSC_DI.UTIL.Strings.Empty(TXT_TEMP.Value))
                {
					//Validate Values are sane
					is_parsed = decimal.TryParse(TXT_TEMP.Value, out try_parse);
					if (!is_parsed || try_parse < -10 || try_parse > 125)
					{
						//is PH a decimal numner, and in range 0 > ph > 14?
						int popret = Globals.oApp.MessageBox("Temp is out of normal values, are you sure the data as entered is correct?", 2, "&Yes", "&No");
						if (popret != 1)
						{
							//"no", bail out!
							Globals.oApp.StatusBar.SetText("Please enter a valid temperature (must be a decimal!)!");
							TXT_TEMP.Active = true;
							return;
						}
					}
				}

				//Get PH of additive
				EditText TXT_PH = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_PH", pForm);

				if (!string.IsNullOrEmpty(TXT_PH.Value?.Trim()))// Whitespace-Change !NSC_DI.UTIL.Strings.Empty(TXT_PH.Value)
                {

					//Validate Values are sane
					is_parsed = decimal.TryParse(TXT_PH.Value, out try_parse);
					if (!is_parsed || try_parse < 0 || try_parse > 14)
					{
						//is PH a decimal numner, and in range 0 < ph < 14?
						int popret = Globals.oApp.MessageBox("PH is out of normal values, are you sure the data as entered is correct?", 2, "&Yes", "&No");
						if (popret != 1)
						{
							Globals.oApp.StatusBar.SetText("Please enter a valid PH (must be a decimal, and in range 0 < PH < 14!)!");
							//"no", bail out!
							TXT_PH.Active = true;
							return;
						}
					}
				}


				// If the additive distribution is Per we want to multiply the Additive amount by the number of Plants.
				if (CMB_ADIST.Value == "Per")
				{
                    if (prntFrmMthrPltMgr || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase == "FLO"))
                    {
                        AdditiveToApply = AdditiveToApply * SelectedPlantIDs.Count;
                    }
                    else
                    {
                        double AdditiveToApply_Counter = 0.0;
                        //Batch Managed Plants - We will have to account for the number of plants in each batch  
                        for (int i = 0; i < SelectedPlantIDs.Count; i++)
                        {
                            string strBatchID = SelectedPlantIDs[i].ToString();
                            int BatchQty_Holder = NSC_DI.UTIL.SQL.GetValue<int>(@"Select OBTQ.Quantity
                                                                                    from OBTN 
                                                                                    inner join OBTQ on OBTN.AbsEntry = OBTQ.MdAbsEntry and OBTN.ItemCode = OBTQ.ItemCode
                                                                                    inner join OWHS on OBTQ.WhsCode = OWHS.WhsCode 
                                                                                    inner join [@NSC_PDO_PHASES] T0 on T0.U_CurrentPhase = OWHS.U_NSC_WhrsType 
                                                                                    where OBTN.DistNumber = '" + strBatchID + "'  and  OBTQ.Quantity>0 and OBTQ.WhsCode!='9999'");
                            if (i == 0)
                            {
                                AdditiveToApply_Counter = AdditiveToApply * BatchQty_Holder;
                            }
                            else
                            {
                                AdditiveToApply_Counter += AdditiveToApply * BatchQty_Holder;
                            }
                        }
                        AdditiveToApply = AdditiveToApply_Counter;
                    }
                }

                string First_CropID = null;
                string WhsHolder = null;
                string CropIDHolder = null;
                int intFail = 0;
                
                //Get property to determine if cropCycle is used
                string Crop = NSC_DI.UTIL.SQL.GetValue<string>("Select U_Value1 from [@NSC_USER_OPTIONS] where name='Crop Cycle Management'");
                
                if (Crop=="Y")
                {

                    //Check For all the same crop. ToDo:in future allow for multiple crops to be on one order. Issue: how much to apply to each crop (Solvable but no time right now)
                    for (int i = 1; i < (MTX_PLAN.RowCount + 1); i++)
                    {
                        // Grab the selected row's Plant ID column
                        string plantID = ((EditText)MTX_PLAN.Columns.Item(0).Cells.Item(i).Specific).Value;
                        if (i == 1)
                        {
                            First_CropID = ((EditText)MTX_PLAN.Columns.Item(3).Cells.Item(i).Specific).Value;
                            WhsHolder = ((EditText)MTX_PLAN.Columns.Item(5).Cells.Item(i).Specific).Value;
                        }
                        else
                        {
                            CropIDHolder = ((EditText)MTX_PLAN.Columns.Item(3).Cells.Item(i).Specific).Value;
                            if (First_CropID != CropIDHolder)
                            {
                                intFail = 1;
                            }
                        }
                    }
                    if (intFail == 1)
                    {
                        // test what happens here
                        Globals.oApp.MessageBox("Please cancel and select all plants from the same crop.");
                        return;
                    }
                    int Check_First_CropID = First_CropID.Length == 0 ? Check_First_CropID = 0 : Check_First_CropID = int.Parse(First_CropID);
                    if (Check_First_CropID > 0 && !prntFrmMthrPltMgr)
                    {
                        string Prd_StageID = null;
                        string Prd_Key = null;
                        //Get Active Plant Stage
                        string strStage = NSC_DI.UTIL.SQL.GetValue<string>("Select U_NSC_WhrsType from OWHS where WhsCode='" + WhsHolder + "'");
                            if (strStage != "FLO")
                            {
                                Prd_StageID = "VEG_FEED_" + First_CropID;
                            }
                            else
                            {
                                Prd_StageID = "FLOW_FEED_" + First_CropID;
                            }
                        //Get production orders
                        Prd_Key = NSC_DI.UTIL.SQL.GetValue<string>("Select DocEntry from OWOR where U_NSC_CropStageID='" + Prd_StageID + "'");
                        //Update line on production order and Issue the Items
                        Forms.Crop.CropCycleManagement oCrop = new Forms.Crop.CropCycleManagement();
                        string strCropFinProjID = oCrop.GetFinProj(First_CropID);
                        int IssueDocNum = NSC_DI.SAP.ProductionOrder.UpdateGEN_CropProdLine(Convert.ToInt32(Prd_Key), TXT_AITEMC.Value, AdditiveToApply, SelectedDestinationWarehouseID, strCropFinProjID);
                        oCrop.AddDocToProject_Single(PMDocumentTypeEnum.pmdt_GoodsIssue, IssueDocNum.ToString(), First_CropID, Prd_StageID);
                    }
                }
                else
                {
                    // Create a GoodsIssued receipt
                    NSC_DI.SAP.GoodsIssued.Create(DateTime.Now, new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>()
				    {
					    new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
					    {
						    ItemCode = TXT_AITEMC.Value,
						    Quantity = AdditiveToApply,
						    WarehouseCode = SelectedDestinationWarehouseID
					    }
				    });
                }
                

                // We always want to take the AdditiveToApply and divide it by the number of Plants.
				// Above we did (Amount * CountOfPlants) = AmountToApply if the Additive Amount is applied per.
				// Now we want to get that Individual amount only per plant.
				// If the Distribution type was equal the amount would still need to be divided by the CountOfPlants.
				double actualAdditiveToApply = AdditiveToApply/SelectedPlantIDs.Count;
                
                // 6183 instantiates the prog bar here so that it has the scope for the finally at the bottom
                ProgressBar oBar = null;
                oBar = Globals.oApp.StatusBar.CreateProgressBar("Feeding Plants", SelectedPlantIDs.Count, false);
                oBar.Value = 0; //6183 prog bar starts here
                foreach (string PlantID in SelectedPlantIDs)
				{
					// For each plant we have in our selected list we create a new FeedLog for each.
					NSC_DI.SAP.FeedLog.AddFeedLog(PlantID, TXT_AITEMC.Value, actualAdditiveToApply, TXT_ADPPM.Value, TXT_ADTDS.Value, TXT_ADEC.Value, TXT_TEMP.Value, TXT_PH.Value
						);
                    oBar.Value++; // //6183 iterates prog bar here
                }

                //6183 if the code runs successfully ends prog bar here and then displays success msg. I did this because the msg was not working in the finally and it would disappear immediately when the prog bar is ended.
                if (oBar != null)
                    oBar.Stop();
                Globals.oApp.StatusBar.SetText("Successfully fed " + SelectedPlantIDs.Count + " plants!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

				// Close the feeding wizard
				pForm.Close();
                try
                {
                    //calling form is plant details
                    F_PlantDetails.Load_TabCount(Globals.oApp.Forms.ActiveForm);
                    F_PlantDetails.Load_Matrix(Globals.oApp.Forms.ActiveForm, "FEED");
                }
                catch
                {
                    //Calling form is not Production Garden no need to update anything
                }
                finally
                {
                    // 6183 ensures that the prog bar is ended. If code runs successfully this should not be triggered
                    if (oBar != null)
                        oBar.Stop();
                }

            }
            catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
                //NSC_DI.UTIL.Misc.KillObject(oBar);

                GC.Collect();
			}

		}

	}
}
