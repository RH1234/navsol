﻿using System;
using System.Collections.Generic;
using SAPbouiCOM;
using SAPbobsCOM;
using B1WizardBase;
using System.ComponentModel;
//using Matrix = NavSol.CommonUI.Matrix;

namespace NavSol.Forms.Production
{
	internal class F_WasteWizard : B1Event
	{
		private const string cFormID = "NSC_WASTE_WIZ";


		// FOR OLD CODE
		//public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		public string FormUID { get; set; }
        public static string CurrentPhase { get; set; }
        private static bool prntFrmMthrPltMgr = false;
        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //----------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				// "Apply" button was clicked
				case "btn_ok":
					if (pVal.BeforeAction == false)
					{
						BTN_OK_Click(_SBO_Form);
					}
					break;

            }

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		public F_WasteWizard(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
		{
			// this.FormUID = "VSC_WASTE_WIZ";

			_SBO_Application = SAPBusinessOne_Application;
			_SBO_Company = SAPBusinessOne_Company;
		}

		public F_WasteWizard() : this(Globals.oApp, Globals.oCompany)
		{
		}

		public void Form_Load(bool isMotherPlantMngr, string[] ListOfPlantIDsToLoad, string pCurrentPhase = null)
		{
			// If the form displayed properly, or is already in view
			try
			{
				// Set our private form field
				_SBO_Form = CommonUI.Forms.Load(cFormID);

				// Freeze the Form UI
				_SBO_Form.Freeze(true);

                CurrentPhase = pCurrentPhase;
                // Prepare the SQL statement that will load the selected plants data into the matrix
                // Prepare the SQL statement that will load the selected plants data into the matrix
                string sql = null;
                prntFrmMthrPltMgr = isMotherPlantMngr;
                if ((prntFrmMthrPltMgr) || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && pCurrentPhase == "FLO"))
                {

                    sql = @"
SELECT DISTINCT
(CONVERT(nvarchar,[OSRN].[DistNumber])) AS [UniqueID]
,(SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) AS [WhsCode]
,[OSRN].[CreateDate]
,[OSRN].[itemName],OSRN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]
FROM [OSRN]
left Join OPMG on OPMG.AbsEntry = OSRN.U_NSC_CropID
WHERE
(SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) IN 
(
SELECT 
[OWHS].[WhsCode]
FROM 
[OWHS]
WHERE
";
                    foreach (string PlantID in ListOfPlantIDsToLoad)
                    {
                        sql += " (CONVERT(nvarchar,[OSRN].[DistNumber]))  = '" + PlantID + "' OR";
                    }

                    // Remove the last "OR" in the SQL query and closes the subquery.
                    sql = sql.Substring(0, sql.Length - 2) + ")";

                    // Load data about selected plants into the matrix
                    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(
                                       pForm: _SBO_Form,
                                       DatabaseTableName: "OSRN",
                                       MatrixUID: "MTX_PLAN",
                                       ListOfColumns: new List<CommonUI.Matrix.MatrixColumn>() {
                        new CommonUI.Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropID",               Caption="Crop ID",               ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropName",             Caption="Crop Name",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="WhsCode",             Caption="Warehouse",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }

                                       },
                                       SQLQuery: sql
                                   );
                }
                else
                {
                    sql = @"
SELECT DISTINCT
(CONVERT(nvarchar,[OBTN].[DistNumber])) AS [UniqueID]
--,(SELECT [OIBT].[WhsCode] FROM [OIBT] WHERE [OIBT].[ItemCode] = [OBTN].[ItemCode] AND [OIBT].BatchNum = [OBTN].DistNumber) AS [WhsCode]
,[OIBT].WhsCode
,[OBTN].[CreateDate]
,[OBTN].[itemName],OBTN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]
FROM [OBTN]
join OIBT on OIBT.BatchNum = OBTN.DistNumber
left Join OPMG on OPMG.AbsEntry = OBTN.U_NSC_CropID
WHERE
(SELECT [OIBT].[WhsCode] FROM [OIBT] WHERE [OIBT].[ItemCode] = [OBTN].[ItemCode] AND [OIBT].BatchNum = [OBTN].DistNumber  and [OIBT].Quantity>0 ) IN 
(
SELECT 
[OWHS].[WhsCode]
FROM 
[OWHS]
WHERE
";


                    foreach (string PlantID in ListOfPlantIDsToLoad)
                    {
                        sql += " (CONVERT(nvarchar,[OBTN].[DistNumber]))  = '" + PlantID + "' OR";
                    }

                    // Remove the last "OR" in the SQL query and closes the subquery.
                    sql = sql.Substring(0, sql.Length - 2) + ")  and [OIBT].Quantity>0 ";

                    // Load data about selected plants into the matrix
                    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(
                                       pForm: _SBO_Form,
                                       DatabaseTableName: "OBTN",
                                       MatrixUID: "MTX_PLAN",
                                       ListOfColumns: new List<CommonUI.Matrix.MatrixColumn>() {
                        new CommonUI.Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropID",               Caption="Crop ID",               ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropName",             Caption="Crop Name",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="WhsCode",             Caption="Warehouse",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                                       },
                                       SQLQuery: sql
                                   );
                }

                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_Main", "bin-icon.bmp");                                                                                                                             

				// Un-Freeze the Form UI
				_SBO_Form.Freeze(false);

				// Show the form
				_SBO_Form.VisibleEx = true;

				// If we have only one plant selected we automatically set the selected distribution to Per plant.
				if (ListOfPlantIDsToLoad.Length == 1)
				{
					try
					{
						// Grab the ComboBox list for waste distribution from the UI
						ComboBox CMB_dist = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_dist", _SBO_Form);

						// Remove all ValidValues from our list of selections.
						if (CMB_dist.ValidValues.Count > 0)
						{
							foreach (SAPbouiCOM.ValidValue value in CMB_dist.ValidValues)
							{
								CMB_dist.ValidValues.Remove(value.Value);
							}

							// Set the only option for the user to Per plant.
							CMB_dist.ValidValues.Add("Per", "Per plant");
							CMB_dist.Select("Per");

							// Let go of focus and set editable to false.
							CMB_dist.Active = false;
							CMB_dist.Item.Enabled = false;
						}
					}
					catch (Exception e)
					{
						// We don't care right now
					}
				}
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				//NSC_DI.UTIL.Misc.KillObject(oBar);
				GC.Collect();
			}
		}

        private void BTN_OK_Click(Form pForm)
        {
            Matrix mtx_plan = null;
            //Validate Data
            // Grab the textbox for the amount of waste from the form UI
            EditText TXT_amnt = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_amnt", pForm);

            // Make sure a waste amount was passed
            if (TXT_amnt.Value == "")
            {
                Globals.oApp.StatusBar.SetText("Please enter in an amount of waste!");
                TXT_amnt.Active = true;
                return;
            }

            // Make sure the amount of waste passed is an integer
            try
            {
                decimal amount = Convert.ToDecimal(TXT_amnt.Value);
            }
            catch
            {
                Globals.oApp.StatusBar.SetText("Please enter a numeric value for waste amount!");
                TXT_amnt.Active = true;
                return;
            }

            // Grab the ComboBox list for unit of measurement from the UI
            ComboBox CMB_Meas = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.ComboBox, "CMB_Meas", pForm);

            // Make sure a unit of measurement was selected
            if (CMB_Meas.Value == "")
            {
                Globals.oApp.StatusBar.SetText("Please select a unit of measerment for the waste!");
                CMB_Meas.Active = true;
                return;
            }

            // Grab the matrix of selected plants from the form UI
            mtx_plan = pForm.Items.Item("MTX_PLAN").Specific;
            //mtx_plan = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLAN", pForm);


            // Keep track of the selected Plant ID's
            List<string> SelectedPlantIDs = new List<string>();

            // For each row already selected
            for (int i = 1; i < (mtx_plan.RowCount + 1); i++)
            {
                // Grab the selected row's Plant ID column
                string plantID = ((EditText)mtx_plan.Columns.Item(0).Cells.Item(i).Specific).Value;
                
                // Add the selected row plant ID to list
                SelectedPlantIDs.Add(plantID);
            }

            // Grab the amount of waste we are applying to the plants
            double WasteToApply = Convert.ToDouble(TXT_amnt.Value);

            // Grab the ComboBox list for waste distribution from the UI
            ComboBox CMB_dist = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.ComboBox, "CMB_dist", pForm);

            // Make sure a distribution method was selected.
            if (CMB_dist.Value == "")
            {
                Globals.oApp.StatusBar.SetText("Please select a distribution method for the waste!");
                CMB_dist.Active = true;
                return;
            }

            // If waste is eveny distributed, calculate the amount of waste applied.
            if (CMB_dist.Value == "Evenly")
            {
                WasteToApply = WasteToApply / SelectedPlantIDs.Count;
            }

            // Grab the ComboBox list for waste reason from the UI
            ComboBox CMB_Reas = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.ComboBox, "CMB_Reas", pForm);

            // Make sure a reason was selected
            if (CMB_Reas.Value == "")
            {
                Globals.oApp.StatusBar.SetText("Please select a distribution method for the waste!");
                CMB_Reas.Active = true;
                return;
            }
               
            Dictionary<string, PlantWasteGroup> PlantWasteByItemCode = new Dictionary<string, PlantWasteGroup>();

            foreach (string PlantID in SelectedPlantIDs)
            {

                string ItemCode = null;
                if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase == "FLO"))
                {
                    ItemCode = NSC_DI.SAP.Items.SNs.GetItemCode(PlantID);
                }
                else
                {
                    ItemCode = NSC_DI.SAP.Items.Batches.GetItemCode(PlantID);
                }
                    

                if (PlantWasteByItemCode.ContainsKey(ItemCode))
                {
                    PlantWasteByItemCode[ItemCode].Quantity += WasteToApply;
                }
                else
                {
                    PlantWasteGroup newGroup = new PlantWasteGroup();
                    newGroup.ItemCode = ItemCode;
                    newGroup.Quantity = WasteToApply;
                    if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase == "FLO"))
                    {
                        newGroup.StateID = NSC_DI.UTIL.SQL.GetValue<string>(@"Select MnfSerial from OSRN where DistNumber ='" + PlantID + "'");
                    }
                    else
                    {
                        newGroup.StateID = NSC_DI.UTIL.SQL.GetValue<string>(@"Select MnfSerial from OBTN where DistNumber ='" + PlantID + "'");
                    }

                    PlantWasteByItemCode.Add(ItemCode, newGroup);
                }
            }

            #region Creating Required Controllers for API Creation and Storing


            #endregion

            try
            {
                // Keep track of the selected Plant ID's
                //List<string> SelectedPlantIDs = new List<string>();


                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                try
                {
                    string warehouseCodeQuery = string.Format(@"SELECT TOP 1 [OWHS].[WhsCode] from [OWHS] WHERE [OWHS].[U_NSC_WhrsType] = 'QND'");

                    string warehouseCode = NSC_DI.UTIL.SQL.GetValue<string>(warehouseCodeQuery, 0);

                    if (SelectedPlantIDs.Count == 0)
                    {
                        Globals.oApp.MessageBox("There must be at least one plant");
                        throw new WarningException();
                    }

                   
                    //get the detail notes
                    string strDetails = ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txt_det", _SBO_Form)).Value;


                    string First_CropID = null;
                    string WhsHolder = null;
                    string CropIDHolder = null;
                    int intFail = 0;

                    //Get property to determine if cropCycle is used
                    string Crop = NSC_DI.UTIL.SQL.GetValue<string>("Select U_Value1 from [@NSC_USER_OPTIONS] where name='Crop Cycle Management'");
                    if (Crop == "Y")
                    {
                        //Check For all the same crop. ToDo:in future allow for multiple crops to be on one order. Issue: how much to apply to each crop (Solvable but no time right now)
                        for (int i = 1; i < (mtx_plan.RowCount + 1); i++)
                        {
                            // Grab the selected row's Plant ID column
                            string plantID = ((EditText)mtx_plan.Columns.Item(0).Cells.Item(i).Specific).Value;
                            if (i == 1)
                            {
                                First_CropID = ((EditText)mtx_plan.Columns.Item(3).Cells.Item(i).Specific).Value;
                                WhsHolder = ((EditText)mtx_plan.Columns.Item(5).Cells.Item(i).Specific).Value;
                            }
                            else
                            {
                                CropIDHolder = ((EditText)mtx_plan.Columns.Item(3).Cells.Item(i).Specific).Value;
                                if (First_CropID != CropIDHolder)
                                {
                                    intFail = 1;
                                }
                            }
                        }
                        if (intFail == 1)
                        {
                            Globals.oApp.MessageBox("Please cancle and select all plants from the same crop.");
                            return;
                        }
                        int Check_First_CropID = First_CropID.Length == 0 ? Check_First_CropID = 0 : Check_First_CropID = int.Parse(First_CropID); 
                        if (Check_First_CropID > 0 && !prntFrmMthrPltMgr)
                        {
                            string Prd_StageID = null;
                            string Prd_Key = null;
                            //Get Active Plant Stage
                            string strStage = NSC_DI.UTIL.SQL.GetValue<string>("Select U_NSC_WhrsType from OWHS where WhsCode='" + WhsHolder + "'");
                            if (strStage != "FLO")
                            {
                                Prd_StageID = "VEG_PRUNE_" + First_CropID;
                            }
                            else
                            {
                                Prd_StageID = "FLOW_PRUNE_" + First_CropID;
                            }
                            //Get production orders
                            Prd_Key = NSC_DI.UTIL.SQL.GetValue<string>("Select DocEntry from OWOR where U_NSC_CropStageID='" + Prd_StageID + "'");
                            //Update line on production order and Issue the Items
                            Forms.Crop.CropCycleManagement oCrop = new Forms.Crop.CropCycleManagement();
                            string strCropFinProjID = oCrop.GetFinProj(First_CropID);

                            // For each ItemCode we grouped them and now we need to create a Waste item for it.
                            foreach (var dictPair in PlantWasteByItemCode)
                            {
                                PlantWasteGroup wasteGroup = dictPair.Value;

                                var strainID = NSC_DI.SAP.Items.GetStrainID(wasteGroup.ItemCode);

                                var itemCode = NSC_DI.UTIL.AutoStrain.GetAutoItemCodeValue("~WA", strainID);
                                int RecptDoNum = NSC_DI.SAP.ProductionOrder.UpdateGEN_CropProdLine_Receipt(Convert.ToInt32(Prd_Key), itemCode, wasteGroup.Quantity, warehouseCode, strCropFinProjID);
                                oCrop.AddDocToProject_Single(PMDocumentTypeEnum.pmdt_GoodsReceipt, RecptDoNum.ToString(), First_CropID, Prd_StageID);
                            }                            
                        }
                    }
                    else
                    {
                        //Add Good Receipt line items
                        List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> listOfGoodsReceiptLines = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();

                        // For each ItemCode we grouped them and now we need to create a Waste item for it.
                        foreach (var dictPair in PlantWasteByItemCode)
                        {
                            PlantWasteGroup wasteGroup = dictPair.Value;

                            var strainID = NSC_DI.SAP.Items.GetStrainID(wasteGroup.ItemCode);

                            var itemCode = NSC_DI.UTIL.AutoStrain.GetAutoItemCodeValue("~WA", strainID);

                            string batchNumber = NSC_DI.SAP.BatchItems.NextBatch(itemCode);


                            NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines newLine = new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines()
                            {
                                ItemCode = itemCode,
                                Quantity = wasteGroup.Quantity,
                                WarehouseCode = warehouseCode,
                                ListOfBatchLineItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem>()
                            {
                                new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem()
                                {
                                    BatchNumber = batchNumber,
                                    Quantity = wasteGroup.Quantity,
                                    UserDefinedFields = new Dictionary<string, string>()
                                    {
                                        {"U_NSC_StateID", wasteGroup.StateID},
                                        {"U_NSC_QuarState", "DEST" },
                                        {"U_NSC_TimerQuar", NSC_DI.UTIL.Dates.CurrentTimeStamp.ToString()}
                                    }
                                }
                            }
                            };

                            listOfGoodsReceiptLines.Add(newLine);
                        }

                        Dictionary<string, string> UserDefinedFields = new Dictionary<string, string>()
                        {
                            { "U_NSC_ComplianceType","plant_waste_weigh"},
                            { "U_NBS_EmpID", "" }
                        };

                        //Add The Good Receipt 
                        NSC_DI.SAP.GoodsReceipt.Create(DateTime.Now, listOfGoodsReceiptLines, strDetails, UserDefinedFields);
                    }


                    // 6183 instantiates the prog bar here so that it has the scope for the finally at the bottom
                    ProgressBar oBar = null;
                    oBar = Globals.oApp.StatusBar.CreateProgressBar("Feeding Plants", SelectedPlantIDs.Count, false);
                    oBar.Value = 0; //6183 prog bar starts here
                    foreach (string PlantID in SelectedPlantIDs)
                    {
                        // Add Treatment line item 
                        NSC_DI.SAP.WasteLog.GetWasteFromPlant(
                            PlantID: PlantID,
                            AmountOfWasteRemoved: WasteToApply,
                            UnitOfMeasurement: CMB_Meas.Value,
                            Reason: CMB_Reas.Value,
                            Details: strDetails
                        );
                        oBar.Value++; // //6183 iterates prog bar here
                    }
                    //6183 if the code runs successfully ends prog bar here and then displays success msg. I did this because the msg was not working in the finally and it would disappear immediately when the prog bar is ended.
                    if (oBar != null)
                        oBar.Stop();
                    Globals.oApp.StatusBar.SetText("Successfully removed " + WasteToApply + " " + CMB_Meas.Value + " from " + SelectedPlantIDs.Count + " plants!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

                    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                    // Close the treatment wizard
                    pForm.Close();
                    try
                    {
                        //calling form is plant details
                        F_PlantDetails.Load_TabCount(Globals.oApp.Forms.ActiveForm);
                        F_PlantDetails.Load_Matrix(Globals.oApp.Forms.ActiveForm, "WASTE");
                    }
                    catch { }
                    finally
                    {
                        // 6183 ensures that the prog bar is ended. If code runs successfully this should not be triggered
                        if (oBar != null)
                            oBar.Stop();
                    }

                }
                catch (NSC_DI.SAP.B1Exception ex)
                {
                    Globals.oApp.StatusBar.SetText("Error occured when submitting a goods receipt: " + ex.Message);
                }
                catch (WarningException) { }
                catch (Exception ex)
                {
                    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                }
                finally
                {
                    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                }
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
                /*
                string warehouseCodeQuery = string.Format(@"
    SELECT TOP 1 [OWHS].[WhsCode]
    from [OWHS]
    WHERE [OWHS].[U_VSC_WhrsType] = 'QND' 
        OR [OWHS].[U_VSC_WhrsType] = 'QNX' 
        OR [OWHS].[U_VSC_WhrsType] = 'QNS' ");

                dbCommuncation.DoQuery(warehouseCodeQuery);

                string warehouseCode = dbCommuncation.Fields.Item(0).Value.ToString();

                // Creating a new Waste Item in our Inventory.
                Controllers.GoodsReceipt controllerGoodsReciept = new Controllers.GoodsReceipt(
                    SAPBusinessOne_Application: Globals.oApp, 
                    SAPBusinessOne_Company: Globals.oCompany);

                // Used to get a new batch item for the waste.
                Controllers.BatchItems controllerBatchItems = new Controllers.BatchItems(
                    SAPBusinessOne_Application: Globals.oApp, 
                    SAPBusinessOne_Company: Globals.oCompany);

                List<Controllers.GoodsReceipt.GoodsReceipt_Lines> listOfGoodsReceiptLines = new List<Controllers.GoodsReceipt.GoodsReceipt_Lines>();

                // For each ItemCode we grouped them and now we need to create a Waste item for it.
                foreach (var dictPair in PlantWasteByItemCode)
                {
                   PlantWasteGroup wasteGroup = dictPair.Value;
                    string batchNumber = controllerBatchItems.GetNextAvailableBatchID("WA-" + wasteGroup.ItemCode, "B");

                    Controllers.GoodsReceipt.GoodsReceipt_Lines newLine = new Controllers.GoodsReceipt.GoodsReceipt_Lines()
                    {
                        ItemCode = "WA-"+wasteGroup.ItemCode,
                        Quantity = wasteGroup.Quantity,
                        WarehouseCode = warehouseCode,
                        ListOfBatchLineItems = new List<Controllers.GoodsReceipt.GoodsReceipt_Lines.BatchItem>()
                        {
                            new Controllers.GoodsReceipt.GoodsReceipt_Lines.BatchItem()
                            {
                                BatchNumber = batchNumber,
                                Quantity = wasteGroup.Quantity,
                                UserDefinedFields = new Dictionary<string, string>()
                                {
                                    {"U_VSC_StateID", wasteGroup.StateID}
                                }
                            }
                        }                    
                    };

                    listOfGoodsReceiptLines.Add(newLine);
                }

                if (controllerGoodsReciept.Create(DateTime.Now, listOfGoodsReceiptLines))
                {

                    foreach (string PlantID in SelectedPlantIDs)
                    {
                         Add Waste Log line item
                        VirSci_Controller_WasteLog.GetWasteFromPlant(
                            PlantID: PlantID,
                            AmountOfWasteRemoved: WasteToApply,
                            UnitOfMeasurement: CMB_Meas.Value,
                            Reason: CMB_Reas.Value,
                            Details: ((EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txt_det", _SBO_Form)).Value
                        );
                    }

                    Globals.oApp.StatusBar.SetText(
                        "Successfully removed " + WasteToApply + " " + CMB_Meas.Value + " from " + SelectedPlantIDs.Count + " plants!", 
                        BoMessageTime.bmt_Medium, 
                        BoStatusBarMessageType.smt_Success);
                }
                else
                {
                    Globals.oApp.StatusBar.SetText(
                       "Failed to Create Waste Item From Plants!");
                }

                */
                // Close the watse wizard

        }

        private class PlantWasteGroup
        {
            public string ItemCode { get; set; }
            public string StateID { get; set; }
            public double Quantity { get; set; }
        }
    }
}
