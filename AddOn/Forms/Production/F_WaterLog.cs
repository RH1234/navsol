﻿using System;
using System.Collections.Generic;
using SAPbouiCOM;
using B1WizardBase;
using System.Runtime.InteropServices;

namespace NavSol.Forms.Production
{
    class F_WaterLog : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "NSC_WATER_LOG";

        // FOR OLD CODE
        public VERSCI.Forms _VirSci_Helper_Form;
        public SAPbouiCOM.Application _SBO_Application;
        public SAPbobsCOM.Company _SBO_Company;
        public SAPbouiCOM.Form _SBO_Form;
        public string FormUID { get; set; }
        private SAPbouiCOM.ProgressBar oProgBar;
        #endregion VARS

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
        [B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "MTX_MAIN":
                    Matrix_Main_Click(pVal);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        #endregion AFTER EVENT

        private F_WaterLog(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            // this.FormUID = "VSC_WATER_LOG";
            //  this.MenuText = "Water Log";
            //this.SubMenu = SubMenus.MENU_PROD;
            //this.MenuPosition = 2;

            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

        public F_WaterLog() : this(Globals.oApp, Globals.oCompany) { }

         public void Form_Load()
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);
                // Load the matrix of water events
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(
                    pForm: _SBO_Form,
                    DatabaseTableName: "@" + NSC_DI.Globals.tWaterLog,
                    MatrixUID: "MTX_MAIN",
                    ListOfColumns: new List<CommonUI.Matrix.MatrixColumn>() {
                        new CommonUI.Matrix.MatrixColumn(){ Caption="ID", ColumnWidth=40, ColumnName="U_"+ Globals.SAP_PartnerCode +"_PlantID", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Watered On", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_DateCreated", ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Amount", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_Amount", ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Measurement", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_Measure", ItemType = BoFormItemTypes.it_EDIT}
                         ,new CommonUI.Matrix.MatrixColumn(){ Caption="PPM", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_PPM", ItemType = BoFormItemTypes.it_EDIT}
                          ,new CommonUI.Matrix.MatrixColumn(){ Caption="PHLevel", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_PHLEVEL", ItemType = BoFormItemTypes.it_EDIT}
                           ,new CommonUI.Matrix.MatrixColumn(){ Caption="Dionized", ColumnWidth=80, ColumnName="U_"+ Globals.SAP_PartnerCode +"_DIONIZED", ItemType = BoFormItemTypes.it_EDIT}

                    }
                );

                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_MAIN", "watercan-icon.bmp");

                // Grab the matrix from the form ui
                Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

                double total = 0;
                double total_today = 0;

                // For each row in the matrix
                for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
                {
                    // Grab the measurement, amount and date from the matrix
                    string measurement = ((EditText)MTX_MAIN.Columns.Item(3).Cells.Item(i).Specific).Value.ToString();
                    double amount = Convert.ToDouble(((EditText)MTX_MAIN.Columns.Item(2).Cells.Item(i).Specific).Value.ToString());
                    string date_string = ((EditText)MTX_MAIN.Columns.Item(1).Cells.Item(i).Specific).Value.ToString();

                    // Get the date parts from the SAP string
                    string year = date_string.Substring(0, 4);
                    string month = date_string.Substring(4, 2);
                    string day = date_string.Substring(6, 2);

                    // Get the amount of water as milliliters for conversion
                    double amount_to_add = NSC_DI.SAP.WaterLog.GetMillilitersFromAmountOfLiquid(NSC_DI.SAP.WaterLog.GetLiquidMeasurementFromString(measurement), Amount: amount);

                    // Add to total amount
                    total += amount_to_add;

                    // If the amount was added today, keep track
                    if (Convert.ToDateTime(month + "/" + day + "/" + year).Date == DateTime.Now.Date)
                    {
                        total_today += amount_to_add;
                    }
                }

                // Set the textbox for the total amount of water used.
				((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_TOTAL", _SBO_Form)).Value = NSC_DI.SAP.WaterLog.GetAmountFromMilliliters(NSC_DI.Globals.LiquidMeasurements.gal, total).ToString("0.0") + " gallons";
				((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_TODAY", _SBO_Form)).Value = NSC_DI.SAP.WaterLog.GetAmountFromMilliliters(NSC_DI.Globals.LiquidMeasurements.gal, total_today).ToString("0.0") + " gallons";

                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                // Show the form
                _SBO_Form.VisibleEx = true;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void Matrix_Main_Click(ItemEvent pVal)
        {
            int SelectedRowID = pVal.Row;

            if (SelectedRowID > 0)
            {
                // Which column stores the ID
                int ColumnIDForIDOfItemSelected = 0;

                // Grab the matrix from the Form UI
                Matrix MTX_MAIN = (Matrix)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

                // Get the ID of the note selected
                string ItemSelected = ((EditText)MTX_MAIN.Columns.Item(ColumnIDForIDOfItemSelected).Cells.Item(SelectedRowID).Specific).Value.ToString();

                // Open the plant manager for the selected plant
                F_PlantDetails.FormCreate(pPlantID: ItemSelected);
            }
        }
    }
}