﻿using System;
using System.Collections.Generic;
using SAPbobsCOM;
using SAPbouiCOM;
using B1WizardBase;

namespace NavSol.Forms.Production
{
    class F_TreatWizard : B1Event
    {


        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "NSC_TREAT_WIZ";


        // FOR OLD CODE
        public VERSCI.Forms _VirSci_Helper_Form;
        public SAPbouiCOM.Application _SBO_Application;
        public SAPbobsCOM.Company _SBO_Company;
        public SAPbouiCOM.Form _SBO_Form;
        private SAPbouiCOM.ProgressBar oProgBar;
        //Used for CA batch management and general hemp to determine which production order to send it to
        public static string CurrentPhase { get; set; }
        private static bool prntFrmMthrPltMgr = false;


        #endregion VARS


        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            Form oFormHolder = null;
            Folder TAB_TREAT = null;
            EditText TXT_ITEM = null;
            switch (pVal.ItemUID)
            {
                // "Apply" button was clicked
                case "BTN_OK":

                    if (pVal.BeforeAction == false)
                    {
                        BTN_OK_Click(oForm);
                        try
                        {

                            Globals.oApp.Forms.Item("NSC_PLANT_DET").Select();
                            oFormHolder = Globals.oApp.Forms.ActiveForm;
                            TAB_TREAT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_TREAT", oFormHolder);
                            //Plant ID
                            TXT_ITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEM", oFormHolder);

                            // Count how many current records exist within the database.
                            string strSQL = @"
SELECT
(SELECT COUNT(*) FROM [@NSC_TreatLog] WHERE [U_PlantID] = '" + TXT_ITEM.Value + "')";

                            //Set caption value on the water tab
                            TAB_TREAT.Caption = "Treat (" + NSC_DI.UTIL.SQL.GetValue<string>(strSQL, 0) + ")";
                            TAB_TREAT.Select();


                            NavSol.CommonUI.Matrix.LoadDatabaseDataIntoMatrix(oFormHolder, "@" + NSC_DI.Globals.tTreatLog, "mtx_plan", new List<NavSol.CommonUI.Matrix.MatrixColumn>() {
                                            new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "Code", Caption = "ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_DateCreated", Caption = "Date Created", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_Details", Caption = "Journal Entry", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_PSPPM", Caption = "PPM(s)", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_PSTDS", Caption = "TDS", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_PSEC", Caption = "EC", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }

                        },
     @"Select T0.Code, T0.U_DateCreated, 'Treated ' + left(cast(T0.U_Amount as varchar(9)), 5) + ' of ' + OITM.ItemName as U_Details, T0.U_PSPPM, T0.U_PSTDS,T0.U_PSEC
 from [@" + NSC_DI.Globals.tTreatLog + "] T0  join OITM on T0.U_ItemCode = OITM.ItemCode WHERE T0.[U_PlantID] = '" + TXT_ITEM.Value + "'");

                        }
                        catch (Exception ex)
                        {
                            //Calling form is Plant Production (Garden) no need to do anything
                        }
                        finally
                        {
                            NSC_DI.UTIL.Misc.KillObject(oFormHolder);
                            NSC_DI.UTIL.Misc.KillObject(TAB_TREAT);
                            NSC_DI.UTIL.Misc.KillObject(TXT_ITEM);
                            NSC_DI.UTIL.Misc.KillObject(oForm);
                            GC.Collect();
                        }
                    }
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {

                // Item ChooseFromList
                case "TXT_PSITEM":
                    TXT_PSITEM_ChooseFromList_Selected(oForm,(IChooseFromListEvent)pVal);
                    break;

            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT


		public F_TreatWizard() : this(Globals.oApp, Globals.oCompany) { }

		public F_TreatWizard(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            // this.FormUID = "VSC_TREAT_WIZ";

            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

        public void Form_Load(string[] ListOfPlantIDsToLoad, string pCurrentPhase = null, bool pIsMother = false)
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);


                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                string sql = null;
                prntFrmMthrPltMgr = pIsMother;
                if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") 
                 || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && pCurrentPhase == "FLO")
                 || prntFrmMthrPltMgr)
                {
                    CurrentPhase = pCurrentPhase;

                    sql = @"
SELECT DISTINCT
(CONVERT(nvarchar,[OSRN].[DistNumber])) AS [UniqueID]
,(SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) AS [WhsCode]
,[OSRN].[CreateDate]
,[OSRN].[itemName],OSRN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]
FROM [OSRN]
left Join OPMG on OPMG.AbsEntry = OSRN.U_NSC_CropID
WHERE
  (SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) IN 
    (SELECT [OWHS].[WhsCode] FROM [OWHS]
      WHERE ";

                    foreach (string PlantID in ListOfPlantIDsToLoad)
                    {
                        sql += " (CONVERT(nvarchar,[OSRN].[DistNumber])) = '" + PlantID + "' OR";
                    }

                    // Remove the last "OR" in the SQL query and close off the subquery
                    sql = sql.Substring(0, sql.Length - 2) + ")";


                    // Load data about selected plants into the matrix
                    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(
                        pForm: _SBO_Form,
                        DatabaseTableName: "OSRN",
                        MatrixUID: "MTX_PLAN",
                        ListOfColumns: new List<CommonUI.Matrix.MatrixColumn>() {
                        new CommonUI.Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropID",               Caption="Crop ID",               ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropName",             Caption="Crop Name",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="WhsCode",             Caption="Warehouse",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        },
                        SQLQuery: sql
                    );
                }
                else
                {
                    CurrentPhase = pCurrentPhase;
                    sql = @"
SELECT DISTINCT
(CONVERT(nvarchar,[OBTN].[DistNumber])) AS [UniqueID]
--,(SELECT [OIBT].[WhsCode] FROM [OIBT] WHERE [OIBT].[ItemCode] = [OBTN].[ItemCode] AND [OIBT].BatchNum = [OBTN].DistNumber) AS [WhsCode]
,[OIBT].WhsCode
,[OBTN].[CreateDate]
,[OBTN].[itemName],OBTN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]
FROM [OBTN]
join OIBT on OIBT.BatchNum = OBTN.DistNumber  and OIBT.ItemCode = OBTN.ItemCode
left Join OPMG on OPMG.AbsEntry = OBTN.U_NSC_CropID
inner join OWHS on OIBT.WhsCode = OWHS.WhsCode 
inner join [@NSC_PDO_PHASES] T0 on T0.U_CurrentPhase = OWHS.U_NSC_WhrsType 
WHERE
   (SELECT [OIBT].[WhsCode] FROM [OIBT] WHERE [OIBT].[ItemCode] = [OBTN].[ItemCode] AND [OIBT].BatchNum = [OBTN].DistNumber and [OIBT].Quantity>0) IN 
     (SELECT [OWHS].[WhsCode] FROM [OWHS]
       WHERE ";

                    foreach (string PlantID in ListOfPlantIDsToLoad)
                    {
                        sql += " (CONVERT(nvarchar,[OBTN].[DistNumber])) = '" + PlantID + "' OR";
                    }

                    // Remove the last "OR" in the SQL query and close off the subquery
                    sql = sql.Substring(0, sql.Length - 2) + ")  and [OIBT].Quantity>0  and [OIBT].WhsCode!='9999'";


                    // Load data about selected plants into the matrix
                    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(
                        pForm: _SBO_Form,
                        DatabaseTableName: "OBTN",
                        MatrixUID: "MTX_PLAN",
                        ListOfColumns: new List<CommonUI.Matrix.MatrixColumn>() {
                        new CommonUI.Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropID",               Caption="Crop ID",               ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropName",             Caption="Crop Name",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="WhsCode",             Caption="Warehouse",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        },
                        SQLQuery: sql
                    );

                }

                // Filter ChooseFromList by Pesticide Items.
                Load_ChooseFromList_PesticideItems();

                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_Main", "treat-icon.bmp");

                // Grab the textbox for the PPM of additive from the form UI
                EditText TXT_PSPPM = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_PSPPM", _SBO_Form);

                // Grab the textbox for the TDS of additive from the form UI
                EditText TXT_PSTDS = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_PSTDS", _SBO_Form);

                // Grab the textbox for the TDS of additive from the form UI
                EditText TXT_PSEC = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_PSEC", _SBO_Form);

                TXT_PSPPM.Value = "0";
                TXT_PSTDS.Value = "0";
                TXT_PSEC.Value = "0";

                // If we have only one plant selected we automatically set the selected distribution to Per plant.
                if (ListOfPlantIDsToLoad.Length == 1)
                {
                    if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && pCurrentPhase == "FLO"))
                    {
                        try
                        {

                            // Grab the ComboBox list for additive distribution from the UI
                            ComboBox CMB_PDIST = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_PDIST", _SBO_Form);

                            // Remove all ValidValues from our list of selections.
                            if (CMB_PDIST.ValidValues.Count > 0)
                            {
                                foreach (SAPbouiCOM.ValidValue value in CMB_PDIST.ValidValues)
                                {
                                    CMB_PDIST.ValidValues.Remove(value.Value);
                                }

                                // Set the only option for the user to Per plant.
                                CMB_PDIST.ValidValues.Add("Per", "Per plant");
                                CMB_PDIST.Select("Per");

                                // Let go of focus and set editable to false.
                                CMB_PDIST.Active = false;
                                CMB_PDIST.Item.Enabled = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                        }
                    }
                }

                // Grab the textbox for the amount of pesticide and set it to the Active Field.
                EditText TXT_PSAMNT = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_PSAMNT", _SBO_Form);

                TXT_PSAMNT.Active = true;

                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                // Show the form
                _SBO_Form.VisibleEx = true;

                // Grab the ComboBox list for warehouse.
                ComboBox CMB_WHSE = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WHSE", _SBO_Form);

                // Disable the Warehouse dropdown.
                CMB_WHSE.Active = false;
                CMB_WHSE.Item.Enabled = false;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public void Form_Load()
        {

        }

        private void TXT_PSITEM_ChooseFromList_Selected(Form pForm, IChooseFromListEvent pChooseFromListEvent)
        {
            try
            {

                SAPbouiCOM.ChooseFromList oChooseFromList = pForm.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

                if (pChooseFromListEvent.BeforeAction == false)
                {
                    DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

                    try
                    {
                        string oItemCode = oDataTable.GetValue(0, 0).ToString();
                        string oItemName = oDataTable.GetValue(1, 0).ToString();

                        LoadWarehousesByItemSelected(pForm, oItemCode);

                        // Set the userdatasource for the name textbox
                        pForm.DataSources.UserDataSources.Item("UDS_PSITEM").ValueEx = Convert.ToString(oItemName);

                        // Save the business partners code
                        ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_PITEMC", pForm)).Value = oItemCode;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        /// <summary>
        /// Filters the ChooseFromList of items only of a certain type.
        /// </summary>
        private void Load_ChooseFromList_PesticideItems()
        {

            try
            {
                // Find the ChooseFromList of the Additive Items we want to filter.
                // Filter Logic: WHERE ItemGrpCode = @ItemGroupCode AND OnHand > 0
                SAPbouiCOM.ChooseFromList CFL_PSITEM = _SBO_Form.ChooseFromLists.Item("CFL_PSITEM");

                // Creating condition(s) to filter the list from.
                Condition oCon = default(Condition);
                Conditions oCons = default(Conditions);

                // Removing all conditions.
                CFL_PSITEM.SetConditions(null);

                // Getting the null conditions?
                oCons = CFL_PSITEM.GetConditions();

                // Setting/Getting a new condition for our ChooseFromList.
                oCon = oCons.Add();
                oCon.BracketOpenNum = 2;
                oCon.Alias = "ItmsGrpCod";
                oCon.Operation = BoConditionOperation.co_EQUAL;
                // Apply the ItemGroup to the filter Pesticides

                oCon.CondVal = NSC_DI.SAP.ItemGroups.GetCode<string>("Pesticides");
                oCon.Relationship = BoConditionRelationship.cr_AND;
               
                // Also filter by OnHand > 0 since we only care to display items that are in stock.                
                oCon = oCons.Add();                
                oCon.Alias = "OnHand";
                oCon.Operation = BoConditionOperation.co_GRATER_THAN;
                oCon.CondVal = "0";
                oCon.BracketCloseNum = 1;
                oCon.Relationship = BoConditionRelationship.cr_OR;

                // Apply the ItemGroup to the filter Fungisides
                oCon = oCons.Add();
                oCon.BracketOpenNum = 1;
                oCon.Alias = "ItmsGrpCod";
                oCon.Operation = BoConditionOperation.co_EQUAL;
                oCon.CondVal = NSC_DI.SAP.ItemGroups.GetCode<string>("Fungicides");
                oCon.Relationship = BoConditionRelationship.cr_AND;
                
                // Also filter by OnHand > 0 since we only care to display items that are in stock.                
                oCon = oCons.Add();                
                oCon.Alias = "OnHand";
                oCon.Operation = BoConditionOperation.co_GRATER_THAN;
                oCon.CondVal = "0";
                oCon.BracketCloseNum = 2;
                
                CFL_PSITEM.SetConditions(oCons);
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void LoadWarehousesByItemSelected(Form pForm, string ItemCode)
        {
            try
            {
                // Prepare to run a SQL statement.
                SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                // Count how many current records exist within the database.
                string sql = $@"SELECT DISTINCT [OITW].[WhsCode], [OWHS].[WhsName]
                FROM [OITW]
                JOIN [OWHS] on [OWHS].[WhsCode] = [OITW].[WhsCode]
                WHERE [OITW].ItemCode = '" + ItemCode + "'"
                       + "AND [OITW].OnHand > 0";
                if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})"; // #10823

                oRecordSet.DoQuery(sql);

                // Find the Combobox of Warehouses from the Form UI
                ComboBox CMB_WHSE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WHSE", pForm) as ComboBox;

                // If items already exist in the drop down
                if (CMB_WHSE.ValidValues.Count > 0)
                {
                    // Remove all currently existing values from warehouse drop down
                    for (int i = CMB_WHSE.ValidValues.Count; i-- > 0;)
                    {
                        CMB_WHSE.ValidValues.Remove(i, BoSearchKey.psk_Index);
                    }
                }

                // If more than 1 warehouses exists
                if (oRecordSet.RecordCount > 1)
                {
                    // Create the first item as an empty item
                    CMB_WHSE.ValidValues.Add("", "");

                    // Select the empty item (forcing the user to make a decision)
                    CMB_WHSE.Select(0, BoSearchKey.psk_Index);
                }

                // Add allowed warehouses to the drop down
                for (int i = 0; i < oRecordSet.RecordCount; i++)
                {
                    try
                    {
                        CMB_WHSE.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
                    }
                    catch { }
                    CMB_WHSE.Item.Enabled = true;
                    oRecordSet.MoveNext();
                }

                // If we only have exactly one warehouse that contains our item simply select it and disable the field.
                if (CMB_WHSE.ValidValues.Count == 1)
                {
                    CMB_WHSE.Select(0, BoSearchKey.psk_Index);

                    // Would disable but currently throws an error when attempting.
                }
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }


        private void BTN_OK_Click(Form pForm)
        {
            try
            {
                ;
                // Grab the textbox for the amount of pesticide from the form UI
                EditText TXT_PSAMNT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_PSAMNT", pForm);

                // Make sure a additive amount was passed
                if (string.IsNullOrEmpty(TXT_PSAMNT.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter in an amount of pesticide!");

                    TXT_PSAMNT.Active = true;
                    throw new System.ComponentModel.WarningException();
                }

                // Make sure the amount of pesticide passed is an integer
                try
                {
                    decimal amount = Convert.ToDecimal(TXT_PSAMNT.Value);
                }
                catch
                {
                    Globals.oApp.StatusBar.SetText("Please enter a numeric value for pesticide amount!");

                    TXT_PSAMNT.Active = true;
                    throw new System.ComponentModel.WarningException();
                }

                // Find the ChooseFromList of the Pesticide Items
                EditText TXT_PSITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_PSITEM", pForm);

                // Validate that we do have a Pesticide Item selected.
                if (string.IsNullOrEmpty(TXT_PSITEM.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter an Additive Item!");

                    TXT_PSITEM.Active = true;
                    throw new System.ComponentModel.WarningException();
                }

                // Get the ItemCode of the Additive that was selected.
                EditText TXT_PITEMC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_PITEMC", pForm);

                if (string.IsNullOrEmpty(TXT_PITEMC.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter an Additive Item!");

                    TXT_PITEMC.Active = true;
                    throw new System.ComponentModel.WarningException();
                }


                // Grab the ComboBox list for warehouse.
                ComboBox CMB_WHSE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WHSE", pForm);

                // Make sure a warehouse
                if (string.IsNullOrEmpty(CMB_WHSE.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please select a warehouse where the pesticide exists!");

                    CMB_WHSE.Active = true;
                    return;
                }


                // Get the "description" from the ComboBox, where we are really storing the value
                string SelectedDestinationWarehouseID = CommonUI.ComboBox.GetComboBoxValueOrDescription(
                    pForm: ref pForm,
                    pComboBoxUID: "CMB_WHSE",
                    pComboBoxItemDescription: null,
                    pComboBoxItemValue: CMB_WHSE.Value);


                // Grab the textbox for the PPM of additive from the form UI
                EditText TXT_PSPPM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_PSPPM", pForm);

                // Make sure a additive PPM was passed
                if (string.IsNullOrEmpty(TXT_PSPPM.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter in a PPM amount!");

                    TXT_PSPPM.Active = true;
                    return;
                }

                // Grab the textbox for the TDS of additive from the form UI
                EditText TXT_PSTDS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_PSTDS", pForm);

                // Make sure a additive TDS was passed
                if (string.IsNullOrEmpty(TXT_PSTDS.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter in a TDS amount!");
                    TXT_PSTDS.Active = true;
                    return;
                }

                // Grab the textbox for the TDS of additive from the form UI
                EditText TXT_PSEC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_PSEC", pForm);


                // Make sure a additive EC was passed
                if (string.IsNullOrEmpty(TXT_PSEC.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter in an EC amount!");

                    TXT_PSEC.Active = true;
                    return;
                }

                // Grab the matrix of selected plants from the form UI
                Matrix MTX_PLAN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLAN", pForm);

                // Keep track of the selected Plant ID's
                List<string> SelectedPlantIDs = new List<string>();

                // For each row already selected
                for (int i = 1; i < (MTX_PLAN.RowCount + 1); i++)
                {
                    // Grab the selected row's Plant ID column
                    string plantID = ((EditText)MTX_PLAN.Columns.Item(0).Cells.Item(i).Specific).Value;

                    // Add the selected row plant ID to list
                    SelectedPlantIDs.Add(plantID);
                }

                // Grab the amount of additive we are applying to the plants
                double TreatmentToApply = Convert.ToDouble(TXT_PSAMNT.Value);

                // Grab the ComboBox list for additive distribution from the UI
                ComboBox CMB_PDIST = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_PDIST", pForm);

                // Validate that we have a value for the Treament Distribution.
                if (string.IsNullOrEmpty(CMB_PDIST.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please select a treament distribution!");

                    CMB_PDIST.Active = true;
                    return;
                }

                // If distributed type is Per, calculate the amount of additive applied.
                if (CMB_PDIST.Value == "Per")
                {
                    if (prntFrmMthrPltMgr || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase == "FLO"))
                    {
                        TreatmentToApply = TreatmentToApply * SelectedPlantIDs.Count;
                    }
                    else
                    {
                        double TreatmentToApply_Counter = 0.0;
                        //Batch Managed Plants - We will have to account for the number of plants in each batch  
                        for (int i = 0; i < SelectedPlantIDs.Count; i++)
                        {
                            string strBatchID = SelectedPlantIDs[i].ToString();
                            int BatchQty_Holder = NSC_DI.UTIL.SQL.GetValue<int>(@"Select OBTQ.Quantity
                                                                                    from OBTN 
                                                                                    inner join OBTQ on OBTN.AbsEntry = OBTQ.MdAbsEntry and OBTN.ItemCode = OBTQ.ItemCode
                                                                                    inner join OWHS on OBTQ.WhsCode = OWHS.WhsCode 
                                                                                    inner join [@NSC_PDO_PHASES] T0 on T0.U_CurrentPhase = OWHS.U_NSC_WhrsType 
                                                                                    where OBTN.DistNumber = '" + strBatchID + "'  and  OBTQ.Quantity>0 and OBTQ.WhsCode!='9999'");
                            if (i == 0)
                            {
                                TreatmentToApply_Counter = TreatmentToApply * BatchQty_Holder;
                            }
                            else
                            {
                                TreatmentToApply_Counter += TreatmentToApply * BatchQty_Holder;
                            }
                        }
                        TreatmentToApply = TreatmentToApply_Counter;
                    }
                }


                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();
                // 6183 instantiates the prog bar here so that it has the scope for the finally at the bottom
                ProgressBar oBar = null;
                oBar = Globals.oApp.StatusBar.CreateProgressBar("Feeding Plants", SelectedPlantIDs.Count, false);
                try
                {
                    string First_CropID = null;
                    string WhsHolder = null;
                    string CropIDHolder = null;
                    int intFail = 0;

                    string Crop = NSC_DI.UTIL.SQL.GetValue<string>("Select U_Value1 from [@NSC_USER_OPTIONS] where name='Crop Cycle Management'");
                    if (Crop == "Y")
                    {
                        //Check For all the same crop. ToDo:in future allow for multiple crops to be on one order. Issue: how much to apply to each crop (Solvable but no time right now)
                        for (int i = 1; i < (MTX_PLAN.RowCount + 1); i++)
                        {
                            // Grab the selected row's Plant ID column
                            string plantID = ((EditText)MTX_PLAN.Columns.Item(0).Cells.Item(i).Specific).Value;
                            if (i == 1)
                            {
                                First_CropID = ((EditText)MTX_PLAN.Columns.Item(3).Cells.Item(i).Specific).Value;
                                WhsHolder = ((EditText)MTX_PLAN.Columns.Item(5).Cells.Item(i).Specific).Value;
                            }
                            else
                            {
                                CropIDHolder = ((EditText)MTX_PLAN.Columns.Item(3).Cells.Item(i).Specific).Value;
                                if (First_CropID != CropIDHolder)
                                {
                                    intFail = 1;
                                }
                            }
                        }
                        if (intFail == 1)
                        {
                            Globals.oApp.MessageBox("Please cancle and select all plants from the same crop.");
                            return;
                        }
                        int Check_First_CropID = First_CropID.Length == 0 ? Check_First_CropID = 0 : Check_First_CropID = int.Parse(First_CropID);
                        if (Check_First_CropID > 0 && !prntFrmMthrPltMgr)
                        {
                            string Prd_StageID = null;
                            string Prd_Key = null;
                            //Get Active Plant Stage
                            string strStage = NSC_DI.UTIL.SQL.GetValue<string>("Select U_NSC_WhrsType from OWHS where WhsCode='" + WhsHolder + "'");
                            if (strStage != "FLO")
                            {
                                Prd_StageID = "VEG_TREAT_" + First_CropID;
                            }
                            else
                            {
                                Prd_StageID = "FLOW_TREAT_" + First_CropID;
                            }
                            //Get production orders
                            Prd_Key = NSC_DI.UTIL.SQL.GetValue<string>("Select DocEntry from OWOR where U_NSC_CropStageID='" + Prd_StageID + "'");
                            //Update line on production order and Issue the Items
                            Forms.Crop.CropCycleManagement oCrop = new Forms.Crop.CropCycleManagement();
                            string strCropFinProjID = oCrop.GetFinProj(First_CropID);
                            int IssueDocNum = NSC_DI.SAP.ProductionOrder.UpdateGEN_CropProdLine(Convert.ToInt32(Prd_Key), TXT_PITEMC.Value, TreatmentToApply, SelectedDestinationWarehouseID, strCropFinProjID);
                            oCrop.AddDocToProject_Single(PMDocumentTypeEnum.pmdt_GoodsIssue, IssueDocNum.ToString(), First_CropID, Prd_StageID);
                        }
                    }
                    else
                    {
                        NSC_DI.SAP.GoodsIssued.Create(DateTime.Now, new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>()
                        {
                            new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
                            {
                                ItemCode = TXT_PITEMC.Value,
                                Quantity = TreatmentToApply,
                                WarehouseCode = SelectedDestinationWarehouseID
                            }
                        });
                    }


                    if (SelectedPlantIDs.Count == 0)
                        throw new Exception("There must be at least one plant");
                
                    // We always want to take the TreatmentToApply and divide it by the number of Plants.
                    // Above we did (Amount * CountOfPlants) = AmountToApply if the Treament Amount is applied per.
                    // Now we want to get that Individual amount only per plant.
                    // If the Distribution type was equal the amount would still need to be divided by the CountOfPlants.
                    double actualTreamentToApply = TreatmentToApply / SelectedPlantIDs.Count;

                    oBar.Value = 0; //6183 prog bar starts here
                    foreach (string PlantID in SelectedPlantIDs)
                    {
                        // Add Treatment line item
                        NSC_DI.SAP.TreatmentLog.AddTreatmentLog(
                            PlantID: PlantID,
                            ItemCode: TXT_PITEMC.Value,
                            AmountOfAdditiveApplied: actualTreamentToApply,
                            WarehouseCode: CMB_WHSE.Value,
                            PPM: TXT_PSPPM.Value,
                            TDS: TXT_PSTDS.Value,
                            EC: TXT_PSEC.Value
                        );
                        oBar.Value++;
                    }
                    //6183 if the code runs successfully ends prog bar here and then displays success msg. I did this because the msg was not working in the finally and it would disappear immediately when the prog bar is ended.
                    if (oBar != null)
                        oBar.Stop();
                    Globals.oApp.StatusBar.SetText("Successfully treated " + SelectedPlantIDs.Count + " plants!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    // Close the treatment wizard
                    pForm.Close();
                }
                catch (Exception ex)
                {
                    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                    Globals.oApp.StatusBar.SetText("Error occured when submitting a goods receipt: " + ex.Message);
                }
                finally
                {
                    // 6183 ensures that the prog bar is ended. If code runs successfully this should not be triggered
                    if (oBar != null)
                        oBar.Stop();
                }
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }


    }
}
