﻿using System;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms
{
    public class F_ProdOrderUpdate : B1Event
    {
        // ---------------------------------- VARS, CLASSES --------------------------------------------------------
        const string cFormID = "NSC_PROD_ORDER_UPDATE";

        private static string _callingFormUid   = String.Empty;

        // ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPressed(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "1") UpdateFields(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        // ---------------------------------- AFTER EVENT  --------------------------------------------------------

        [B1Listener(BoEventTypes.et_LOST_FOCUS, false, new string[] { cFormID })]
        public virtual void OnAfterLostFocus(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "txtProdOrF") LoadFields(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            //if (pVal.ActionSuccess == false) return;
            //var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            //NSC_DI.UTIL.Misc.KillObject(oForm);
            //GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (IChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public static Form FormCreate(bool pSingleInstance = true, string pCallingFormUID = null)
        {
            Item oItm = null;
            try
            {
                var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);
                FormSetup(oForm);
                return oForm;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private static void FormSetup(Form pForm)
        {
            try
            {
                CommonUI.Forms.CreateUserDataSource(pForm, "txtProdOrF", "dsProdF", BoDataType.dt_SHORT_TEXT, 50);
                CommonUI.CFL.Create(pForm, "CFL_ProdF", BoLinkedObject.lf_ProductionOrder);
                CommonUI.CFL.AddCon(pForm, "CFL_ProdF", BoConditionRelationship.cr_NONE, "Status", BoConditionOperation.co_EQUAL, "L");
                pForm.Items.Item("txtProdOrF").Specific.ChooseFromListUID = "CFL_ProdF";
                pForm.Items.Item("txtProdOrF").Specific.ChooseFromListAlias = "DocNum";

                CommonUI.Forms.CreateUserDataSource(pForm, "txtProdOrT", "dsProdT", BoDataType.dt_SHORT_TEXT, 50);
                CommonUI.CFL.Create(pForm, "CFL_ProdT", BoLinkedObject.lf_ProductionOrder);
                CommonUI.CFL.AddCon(pForm, "CFL_ProdT", BoConditionRelationship.cr_NONE, "Status", BoConditionOperation.co_EQUAL, "L");
                pForm.Items.Item("txtProdOrT").Specific.ChooseFromListUID = "CFL_ProdT";
                pForm.Items.Item("txtProdOrT").Specific.ChooseFromListAlias = "DocNum";
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.VisibleEx = true;
            }
        }

        private void CFL_Selected(Form pForm, IChooseFromListEvent pVal)
        {
            //if (pVal.InnerEvent) return;
            if (pVal.SelectedObjects == null) return;
            SAPbouiCOM.DataTable oDT = null;

            try
            {
                pForm.ChooseFromLists.Item(pVal.ChooseFromListUID);
                oDT = pVal.SelectedObjects;
                var docNum = "";

                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_ProdF":
                        docNum = oDT.GetValue("DocNum", 0).ToString();
                        pForm.DataSources.UserDataSources.Item("dsProdF").ValueEx = docNum;
                        pForm.Mode = BoFormMode.fm_OK_MODE;

                        CommonUI.CFL.AddCon(pForm, "CFL_ProdT", BoConditionRelationship.cr_NONE, "Status", BoConditionOperation.co_EQUAL, "L");
                        CommonUI.CFL.AddCon(pForm, "CFL_ProdT", BoConditionRelationship.cr_AND, "DocNum", BoConditionOperation.co_GRATER_EQUAL, docNum);

                        // populate the fields available to change
                        //LoadFields(pForm, docNum);
                        break;

                    case "CFL_ProdT":
                        docNum = oDT.GetValue("DocNum", 0).ToString();
                        pForm.DataSources.UserDataSources.Item("dsProdT").ValueEx = docNum;
                        pForm.Mode = BoFormMode.fm_OK_MODE;

                        // populate the fields available to change
                        //LoadFields(pForm, docNum);
                        break;
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }

        private void LoadFields(Form pForm, string pPdO = null)
        {
            SAPbobsCOM.ProductionOrders oPdO = null;

            try
            {
                //if (pPdO == null) pPdO = pForm.Items.Item("txtProdOrF").Specific.Value;
                //if (pPdO == "") return;
                //    int pdo = Convert.ToInt32(pPdO);
                ////oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                //oPdO = NSC_DI.SAP.ProductionOrder.OpenPdO(pdo);

                //pForm.Items.Item("txtProcess").Specific.Value = oPdO.UserFields.Fields.Item("U_NSC_Process").Value;

                pForm.Items.Item("txtProcess").Specific.Value = "";

                pForm.Mode = BoFormMode.fm_OK_MODE;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                GC.Collect();
            }
        }

        private void UpdateFields(Form pForm)
        {
            try
            {
                if (pForm.Mode != BoFormMode.fm_UPDATE_MODE) return;

                dynamic PdOf = pForm.Items.Item("txtProdOrF").Specific.Value;
                dynamic PdOt = pForm.Items.Item("txtProdOrT").Specific.Value;
                if (PdOt.Trim() == "") PdOt = PdOf;
                PdOf = Convert.ToInt32(PdOf);
                PdOt = Convert.ToInt32(PdOt);

                for (int doc = PdOf; doc <= PdOt; doc++)
                {
                    var val = NSC_DI.UTIL.SQL.FixSQL("'" + pForm.Items.Item("txtProcess").Specific.Value + "'");
                    NSC_DI.UTIL.SQL.RunSQLQuery($"EXEC('UPDATE OWOR SET U_NSC_Process = {val} WHERE DocEntry ={doc} AND Status = ''L'' ')");
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                GC.Collect();
            }
        }
    }
}