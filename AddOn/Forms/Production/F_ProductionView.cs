﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAPbobsCOM;
using SAPbouiCOM;
using B1WizardBase;

namespace NavSol.Forms.Production
{
    class F_ProductionView : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        const string cFormID = "NSC_PRODUCTION";
        const string cItemType = "Cannabis Plant";
        
        // FOR OLD CODE
        private static ProgressBar _progressBar;
        private static Form _SBO_Form;

        private enum Matrix_Filter_Columns { Select, WarehouseName, WarehousePlantCount }
        private enum Matrix_Info_Columns { Select, StrainName, WarehousePlantCount, WarehouseName, WarehouseCode }
        private enum Matrix_Plants_Columns { PlantID, ItemName, WarehouseName, AdmissionDate, BinLev1, BinLev2, BinLev3, BinLev4, DaysIn, StateID, LotNumber, HarvestName, Quantity, isMother, CropID, CropName, ItemCode}

        private class PlantType
        {
            internal string Plant { get; set; }
            internal string WH { get; set; }
            internal double Qty { get; set; }
            internal string ManBy { get; set; }
            internal string ItemCode { get; set; }
        }

        #endregion

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------


        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPressed(ItemEvent pVal)
        {
            if (pVal.InnerEvent == true)
            {
                return true;
            }

            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID.Contains("TAB_"))
            {
				_SBO_Form = oForm;
				TAB_PRESSED(pVal);
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            ActivateTextLookup(pVal);

            return BubbleEvent;
        }
        //--------------------------------------------------------------------------------------- et_KEY_DOWN
        [B1Listener(BoEventTypes.et_KEY_DOWN, true, new string[] { cFormID })]
        public virtual bool OnBeforeKeyDown(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

            if (pVal.ItemUID == "TXT_LOOKUP" && (pVal.CharPressed == 13 || pVal.CharPressed == 9))
            {
                BTN_LOOKUP_ITEM_PRESSED();

                ActivateTextLookup(pVal);
            }


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
            return BubbleEvent;
        }

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeValidate(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            if (pVal.InnerEvent) { return BubbleEvent; }

            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
            return BubbleEvent;
        }

        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.InnerEvent == true) return;
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;
            Phase selectedPhase = CurrentlySelectedPhase();

            switch (pVal.ItemUID)
            {
                case "BTN_LOOKUP":
                    BTN_LOOKUP_ITEM_PRESSED();
                    break;

                case "BTN_MOVE":
                    if (System.Diagnostics.Debugger.IsAttached)
                        BTN_MOVE_ITEM_PRESSED_OLD();
                       // BTN_MOVE_ITEM_PRESSED();
                    else
                        BTN_MOVE_ITEM_PRESSED_OLD();

                    break;

                case "CBT_ACTION":
                    CBT_ACTION_SELECT();
                    break;

                case "MTX_INFO":
                    MTX_INFO_MATRIX_PRESSED(pVal);
                    break;

                case "MTX_FILTER":
                    MTX_FILTER_MATRIX_PRESSED(pVal);
                    break;

                case "MTX_PLANTS":
                    MTX_PLANTS_MATRIX_PRESSED(pVal);
                    break;

                case "BTN_FILTER":
                    BTN_FILTER_ITEM_PRESSED();
                    break;

                case "BTN_ALL":
                    BTN_ALL_PRESSED();
                    break;

                case "IMG_MAIN":
                    IMG_MAIN_PRESSED();
                    break;

                case "btnLocCFL":
                    CFL_Location(oForm, pVal);
                    break;

                case "btnLocCFL2":
                    SAPbouiCOM.EditText oEdit = _SBO_Form.Items.Item("txtLocCFL2").Specific;
                    oEdit.Value = "";
                    CurrentWarehouseType = selectedPhase.CurrentPhase;
                    CFL_Location2(oForm, pVal, CurrentWarehouseType);
                    break;

                case "btnFilter":
                    SAPbouiCOM.EditText oEditFilter = _SBO_Form.Items.Item("txtLocCFL2").Specific;
                    string strFilterLoc = oEditFilter.Value;

                    if (strFilterLoc.Trim().Length > 0)
                    {
                        CurrentWarehouseType = selectedPhase.CurrentPhase;
                        this.Load_Filter_Matrix(selectedPhase.CurrentPhase, strFilterLoc);
                    }
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            ActivateTextLookup(pVal);
        }


        //--------------------------------------------------------------------------------------- et_COMBO_SELECT
        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
        public virtual void OnAfterComboSelect(ItemEvent pVal)
        {
            if (pVal.InnerEvent == true) return;
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;
			switch (pVal.ItemUID)
            {
                case "CBT_ACTION":
                    CBT_ACTION_SELECT();
                    break;

                case "CBX_WHSE":
                    break;
            }
            ActivateTextLookup(pVal);
            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

        }

        //--------------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
        [B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
            {
                case "MTX_PLANTS":
					MTX_PLANTS_MATRIX_LINK_PRESSED(oForm, pVal);
                    break;
            }

            //****I am not sure why this is being called here as it is no longer the 
            ActivateTextLookup(pVal);
        }

        //--------------------------------------------------------------------------------------- et_FORM_RESIZE
        [B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
        public virtual void OnAfterFormResize(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            Matrix Matrix_Main = null;
            Matrix Matrix_Filter = null;
            Matrix Matrix_Info = null;
            Matrix Matrix_Plants = null;
            Item filterMatrix = null;
            Item infoMatrix = null;
            try
            {
                // Double Try Catch!? What..
                try
                {
                    // Attempt to auto expand the tabs with the form..
                    // TODO: Extract this into a better feature. So tab controls expand with the form.
                    // Chase would appreciate this feature.
                    Item tabGroup = (Item)_SBO_Form.Items.Item("TAB_1");

                    if (tabGroup != null)
                    {
                        // These are just numbers I found that worked best with the form.
                        int widthDiff = 160;
                        int heightDiff = 120;

                        // These are the values of the tab controls initial height and width.
                        tabGroup.Height = Math.Max(515, _SBO_Form.Height - heightDiff);
                        tabGroup.Width = Math.Max(645, _SBO_Form.Width - widthDiff);
                    }

                    // Make the Matrix's not expand..
                    Matrix_Filter = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_FILTER", _SBO_Form
                    );

                     filterMatrix = (Item)_SBO_Form.Items.Item("MTX_FILTER");

                    // Lock the Width and Height.
                    if (filterMatrix != null)
                    {
                        // These are the values of the tab controls initial height and width.
                        filterMatrix.Height = 121;
                        filterMatrix.Width = 301;
                        Matrix_Filter.AutoResizeColumns();
                    }

                    Matrix_Info = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_INFO", _SBO_Form
                   );

                     infoMatrix = (Item)_SBO_Form.Items.Item("MTX_INFO");

                    // Lock the Width and Height.
                    if (infoMatrix != null)
                    {
                        // These are the values of the tab controls initial height and width.
                        infoMatrix.Height = 121;
                        Matrix_Info.AutoResizeColumns();
                    }
                }
                catch (Exception ex)
                {
                    // I could care less about your problems!
                }

                Matrix_Plants = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLANTS", _SBO_Form
                );

                Matrix_Plants.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                // 
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(Matrix_Main);
                NSC_DI.UTIL.Misc.KillObject(Matrix_Filter);
                NSC_DI.UTIL.Misc.KillObject(Matrix_Info);
                NSC_DI.UTIL.Misc.KillObject(Matrix_Plants);
                NSC_DI.UTIL.Misc.KillObject(filterMatrix);
                NSC_DI.UTIL.Misc.KillObject(infoMatrix);
                GC.Collect();
            }
            ActivateTextLookup(pVal);
        }

        //--------------------------------------------------------------------------------------- et_KEY_DOWN
        [B1Listener(BoEventTypes.et_KEY_DOWN, false, new string[] { cFormID })]
        public virtual void OnAfterKeyDown(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			//ActivateTextLookup(pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (ChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, false, new string[] { cFormID })]
        public virtual void OnAfterValidate(ItemEvent pVal)
        {
            if (pVal.InnerEvent == true) return;
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            if (pVal.ItemUID == "txtLocCFL") CFL_Location(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            ActivateTextLookup(pVal);
        }
        #endregion AFTER EVENT

        private void ActivateTextLookup(ItemEvent pVal=null)
        {
            return; // not sure this sub is necessary
            EditText TXT_LOOKUP = null;
            try
            {
                if (pVal.FormUID != "NSC_PRODUCTION") return;

                if(pVal.ItemUID == "txtWH_CFL") return;

                //****This line was causing application to crash as _SBO_Form was never defined -->Thus I created an optional variable for the pVal
                //TXT_LOOKUP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", _SBO_Form);
                TXT_LOOKUP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", B1Connections.theAppl.Forms.Item(pVal.FormUID));
                TXT_LOOKUP.Value = "";
                TXT_LOOKUP.Active = true;
            }
            catch (Exception ex)
            {
                //throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(TXT_LOOKUP);
                GC.Collect();
            }
        }

		public void Form_Load()
        {
            Form Form_Waiting = null;
            Item oItem = null;
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                _SBO_Form.DataSources.DataTables.Add("dtBins");

                CommonUI.Forms.CreateUserDataSource(_SBO_Form, "TAB_1", "TAB_1", BoDataType.dt_SHORT_TEXT, 50);

                // Location CFL   
                // have to use the custom CFL form
                //CommonUI.Forms.CreateUserDataSource(_SBO_Form, "txtLocCFL", "dsLoc", BoDataType.dt_SHORT_TEXT, 50);
                //CommonUI.CFL.Create(_SBO_Form, "CFL_LOC", BoLinkedObject.lf_Warehouses);
                //_SBO_Form.Items.Item("txtLocCFL").Specific.ChooseFromListUID = "CFL_LOC";
                //_SBO_Form.Items.Item("txtLocCFL").Specific.ChooseFromListAlias = "Location";
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "btnLocCFL", "CFL-icon.BMP");
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "btnLocCFL2", "CFL-icon.BMP");

                // Wh CFL   
                if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y") NavSol.CommonUI.CFL.CreateWH(_SBO_Form, "CFL_WH", "CBX_WHSE");
                //CommonUI.Forms.CreateUserDataSource(_SBO_Form, "txtWH_CFL", "dsWH", BoDataType.dt_SHORT_TEXT, 50);
                //CommonUI.CFL.Create(_SBO_Form, "CFL_WH", BoLinkedObject.lf_Warehouses);
                //_SBO_Form.Items.Item("txtWH_CFL").Specific.ChooseFromListUID = "CFL_WH";
                //_SBO_Form.Items.Item("txtWH_CFL").Specific.ChooseFromListAlias = "WhsCode";

                // Show the "Loading" form.
                F_Waiting vm_Waiting = new F_Waiting();
                Form_Waiting = F_Waiting.FormCreate();
            

                F_Waiting.SetProgressBar(pForm: Form_Waiting, Width: 24, Message: "Loading configured phases.");

                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_MAIN", "production-icon.bmp");

                //Item oItem;
                Folder oFolder;

                int current = 1;
                var sqlQuery = @"SELECT [Name] FROM [@" + NSC_DI.Globals.tPDOPhase + @"] ORDER BY Code";
                var dt_Phase = NSC_DI.UTIL.SQL.DataTable(sqlQuery, "Phase");
                
                foreach (System.Data.DataRow dr in dt_Phase.Rows)
                {
                    var phaseName = dr["Name"] as string;
                    if (current == 1)
                    {
                        ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_1", _SBO_Form)).Caption = phaseName;
                    }
                    else
                    {
                        try
                        {
                            // Make new Tab
                            oItem = _SBO_Form.Items.Add("TAB_" + current, BoFormItemTypes.it_FOLDER);
                            oItem.Top = 10;
                            oItem.Left = (120 + (95 * (current - 1)));
                            oItem.Height = 20;
                            oItem.Width = 95;
                            oItem.Visible = true;

                            oFolder = oItem.Specific;
                            oFolder.Caption = phaseName;
                            oFolder.DataBind.SetBound(true, "", "TAB_1");
                            oFolder.GroupWith("TAB_1");
                            oFolder.Pane = 0;
                        }
                        catch { }
                    }
                    current++;
                }

                F_Waiting.SetProgressBar(pForm: Form_Waiting, Width: 55, Message: "Loading initial warehouse information.");

                Load_TabCount();

                F_Waiting.SetProgressBar(pForm: Form_Waiting, Width: 75);

                F_Waiting.SetProgressBar(pForm: Form_Waiting, Width: 100, Message: "All complete loading form");

                F_Waiting.Form_Close(Form_Waiting);

                //Check if "Scan" State Id is selected and set checkbox accordingly

                _SBO_Form.DataSources.DataTables.Add("dtBinSrc");


                string StateIDMethod = NSC_DI.UTIL.Settings.Value.Get("State Plant ID Source");

                if (StateIDMethod.ToUpper()!="SCAN")
                {
                    //Hide the assign State ID Checkbox
                    ((CheckBox)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_STID", _SBO_Form)).Item.Visible = false;

                }
                // Select the first tab by default
                ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_1", _SBO_Form)).Select();

                LoadTab(1);

                // Show the form
                _SBO_Form.VisibleEx = true;
            }
            catch (Exception ex)
            {
                _SBO_Form.Close();
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItem);
                NSC_DI.UTIL.Misc.KillObject(Form_Waiting);
                GC.Collect();
            }
        }

        /// <summary>
        /// Loads available actions in the combobutton based off of a phase passed to it
        /// </summary>
        /// <param name="Phase"></param>
        private void Load_AvailablActions(Phase Phase)
        {
            try
            {
                ButtonCombo CBT_ACTION = _SBO_Form.Items.Item("CBT_ACTION").Specific as ButtonCombo;

                // If items already exist in the drop down
                if (CBT_ACTION.ValidValues.Count > 0)
                {
                    // Remove all currently existing values from warehouse drop down
                    for (int i = CBT_ACTION.ValidValues.Count; i-- > 0;)
                    {
                        CBT_ACTION.ValidValues.Remove(i, BoSearchKey.psk_Index);
                    }
                }

                if (Phase.CanWater)
                {
                    CBT_ACTION.ValidValues.Add("Water", "1");
                }

                if (Phase.CanFeed)
                {
                    CBT_ACTION.ValidValues.Add("Feed", "2");
                }

                if (Phase.CanPrune)
                {
                    CBT_ACTION.ValidValues.Add("Prune", "3");
                }

                if (Phase.CanTreat)
                {
                    CBT_ACTION.ValidValues.Add("Treat", "4");
                }

                if (Phase.CanQuarantine)
                {
                    CBT_ACTION.ValidValues.Add("Quarantine", "5");
                }

                //if (Phase.CanChangeMother)
                //{
                CBT_ACTION.ValidValues.Add("Set To Mother", "6");
                //}
            }
            catch
            { }
        }

        public class Phase
        {
            public string PhaseName { get; set; }
            public string CurrentPhase { get; set; }
            public string NextPhase { get; set; }
            public bool CanWater { get; set; }
            public bool CanFeed { get; set; }
            public bool CanTreat { get; set; }
            public bool CanPrune { get; set; }
            public bool CanQuarantine { get; set; }

            //FIXME: load from CSV if this phase can change mother state, until then, all phases can
            //public bool CanChangeMother { get; set; }
        }

        /// <summary>
        /// Returns the currently selected phase, based off of the selected tab in the UI
        /// </summary>
        /// <returns></returns>
        public Phase CurrentlySelectedPhase()
        {
            var sqlQuery = @"SELECT * FROM [@" + NSC_DI.Globals.tPDOPhase + @"] ORDER BY [Code]";
            System.Data.DataTable dt_Phase = NSC_DI.UTIL.SQL.DataTable(sqlQuery, "Phase");
            System.Data.DataRow dr = dt_Phase.Rows[CurrentlySelectedTab() - 1];

            return new Phase()
            {
                PhaseName       = dr["Name"] as string,
                CurrentPhase    = dr["U_CurrentPhase"] as string,
                NextPhase       = dr["U_NextPhase"] as string,
                CanWater        = string.Equals(dr["U_CanWater"].ToString(), "Y", StringComparison.InvariantCulture),
                CanFeed         = string.Equals(dr["U_CanFeed"].ToString(), "Y", StringComparison.InvariantCulture),
                CanTreat        = string.Equals(dr["U_CanTreat"].ToString(), "Y", StringComparison.InvariantCulture),
                CanPrune        = string.Equals(dr["U_CanPrune"].ToString(), "Y", StringComparison.InvariantCulture),
                CanQuarantine   = string.Equals(dr["U_CanQuarantine"].ToString(), "Y", StringComparison.InvariantCulture)
            };
        }

        private void Load_TabCount()
        {
            var sqlQuery = @"SELECT * FROM [@" + NSC_DI.Globals.tPDOPhase + @"] ORDER BY [Code]";
            var dt_Phase = NSC_DI.UTIL.SQL.DataTable(sqlQuery, "Phase");

            int itemCount = dt_Phase.Rows.Count;

            //foreach (CSV_Phase phase in ListOfPhases)
            //{
            //                SQL += @"
            //(
            //SELECT COUNT(*) 
            //FROM [OSRN] 
            //JOIN [OWHS] 
            //ON [OWHS].[WhsCode] = (SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) 
            //WHERE [OSRN].[U_VSC_IsMother] = 0 
            //AND [OWHS].[U_VSC_WhrsType] = '" + phase.CurrentPhase + @"'
            //AND 
            //(
            //	SELECT [OSRI].[Status] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]
            //)
            //= 0),";
            //itemCount++;
            //}

            //            // Clear the last comma from the SQL query
            //            sql = sql.Substring(0, sql.Length - 1);

            //            // Prepare to run a SQL statement.
            //            SAPbobsCOM.SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            //            // Count how many current records exist within the database.
            //            oRecordSet.DoQuery(SQL);

            for (int i = 1; i < itemCount; i++)
            {
                // Grab the "Tab" from the form UI
                Folder TAB = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_" + i.ToString(), _SBO_Form);

                if (TAB != null)
                {
                    // Set the caption of the tab to the count of items
                    TAB.Caption = dt_Phase.Rows[i - 1]["Name"] as string;
                }
            }

            //oRecordSet = null;
        }

        private void CFL_Location2(Form pForm, ItemEvent pVal, string pWhrsType)
        {
            try
            {
                if (pVal.EventType == BoEventTypes.et_VALIDATE)
                {
                    //if (pForm.Items.Item("txtLocCFL").Specific.Value != "")
                    //    Load_Warehouses(pForm, NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_NextPhase FROM [@{NSC_DI.Globals.tPDOPhase}] ORDER BY [Code]", ""));
                    //else
                        F_CFL_GRID.FormCreate("Location Choose From List", @"SELECT Distinct OLCT.Location, OLCT.Code FROM OLCT join OWHS on OLCT.Code=OWHS.Location where OWHS.U_NSC_WhrsType = '" + pWhrsType + "'", pForm.UniqueID, "txtLocCFL2", "Location");
                }

                if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnLocCFL2")
                    F_CFL_GRID.FormCreate("Location Choose From List", @"SELECT Distinct OLCT.Location, OLCT.Code FROM OLCT join OWHS on OLCT.Code=OWHS.Location where OWHS.U_NSC_WhrsType = '" + pWhrsType + "'", pForm.UniqueID, "txtLocCFL2", "Location");
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void CFL_Location(Form pForm, ItemEvent pVal)
        {
            try
            {
                if (pVal.EventType == BoEventTypes.et_VALIDATE)
                {
                    if (pForm.Items.Item("txtLocCFL").Specific.Value != "")
                        Load_Warehouses(pForm, NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_NextPhase FROM [@{NSC_DI.Globals.tPDOPhase}] ORDER BY [Code]", ""));
                    else
                        F_CFL_GRID.FormCreate("Location Choose From List", "SELECT Location, Code FROM OLCT", pForm.UniqueID, "txtLocCFL", "Location");
                }

                if (pVal.EventType == BoEventTypes.et_ITEM_PRESSED && pVal.ItemUID == "btnLocCFL")
                    F_CFL_GRID.FormCreate("Location Choose From List", "SELECT Location, Code FROM OLCT", pForm.UniqueID, "txtLocCFL", "Location");
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            if (pVal.SelectedObjects == null) return;

            try
            {
                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_WH":
                        CommonUI.CFL.SetVal(pForm, pVal, "WhsName");
                        break;

                    //case "CFL_LOC": // OLCT
                    //    pForm.Items.Item("txtLoc").Specific.Value = "";
                    //    pForm.Items.Item("txtLocCFL").Specific.Value = "";
                    //    var loc = oDT.GetValue("Location", 0).ToString();
                    //    //loc = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Location FROM OLCT WHERE Code = {loc}","");
                    //    pForm.DataSources.UserDataSources.Item("dsLoc").ValueEx = loc;
                    //    Load_Warehouses(pForm, NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_NextPhase FROM [@{NSC_DI.Globals.tPDOPhase}] ORDER BY [Code]",""));
                    //    break;
                }
            }
            catch (Exception ex) 
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void Load_Warehouses(Form pForm, string WarehouseType)
        {
            try
            {
                if (pForm.TypeEx != cFormID) return;

                if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y") 
                {
                    NavSol.CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_NONE, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, WarehouseType);
                    NavSol.CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND, "Inactive", BoConditionOperation.co_EQUAL, "N");
                    NavSol.CommonUI.CFL.AddCon_Branches(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND); // 10823
                    var loc = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Code FROM OLCT WHERE Location = '{pForm.Items.Item("txtLocCFL").Specific.Value}'", "");
                    if (pForm.Items.Item("txtWH_CFL").Visible && loc != "")
                        CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND, "Location", BoConditionOperation.co_EQUAL, loc);
                }
                else
                {
                    //pForm.Items.Item("txtWH_CFL").Visible    = false;
                    //pForm.Items.Item("CBX_WHSE").Width       = pForm.Items.Item("LBL_WRHS").Width;
                    //pForm.Items.Item("CBX_WHSE").Visible     = true;
                    Load_Combobox_Warehouses(pForm, WarehouseType, pForm.Items.Item("CBX_WHSE").Specific);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
            }
        }

        private void Load_Combobox_Warehouses(Form pForm, string WarehouseType, ComboBox ComboBoxControl)
        {
            //SAPbouiCOM.Form oFrom = CommonUI.Forms.GetActiveForm();

            try
            {
                
                pForm.Freeze(true);
                // Prepare to run a SQL statement.
                SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                // Count how many current records exist within the database.
                var sql = $@"SELECT [WhsCode], [WhsName] FROM [OWHS] LEFT JOIN OBPL ON OWHS.BPLid = OBPL.BPLid WHERE [U_{Globals.SAP_PartnerCode}_WhrsType] = '{WarehouseType.ToString()}' AND [Inactive] ='N'"; //10823 is the join
                var loc = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Code FROM OLCT WHERE Location = '{pForm.Items.Item("txtLocCFL").Specific.Value}'", "");
                if (Globals.BranchDflt >= 0) sql += $" AND OBPL.BPLid IN ({Globals.BranchList})"; // #10823 
                if (loc != "") sql += $" AND Location = {loc}";
 
                 oRecordSet.DoQuery(sql);

                // Disable the combobox until we know we've filled it with some data
                // CBX_WHSE.Item.Enabled = false;

                // Disable the "Move" button, no where to move to
                Button BTN_MOVE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "BTN_MOVE", pForm);

                BTN_MOVE.Item.Enabled = true;

                // If items already exist in the drop down
                if (ComboBoxControl.ValidValues.Count > 0)
                {
                    
                    // Remove all currently existing values from warehouse drop down
                    for (int i = ComboBoxControl.ValidValues.Count; i-- > 0;)
                    {
                        ComboBoxControl.ValidValues.Remove(i, BoSearchKey.psk_Index);
                    }
                }

                // If more than 1 warehouses exists
                if (oRecordSet.RecordCount > 1)
                {
                    // Create the first item as an empty item
                    ComboBoxControl.ValidValues.Add("", "");

                    // Select the empty item (forcing the user to make a decision)
                    ComboBoxControl.Select(0, BoSearchKey.psk_Index);
                }

                oRecordSet.MoveFirst();

                // Add allowed warehouses to the drop down
                try
                {
                    for (int i = 0; i < oRecordSet.RecordCount; i++)
                    {

                        ComboBoxControl.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());                        
                        oRecordSet.MoveNext();
                    }
                }
                catch { }
                finally { ComboBoxControl.Item.Enabled = true; }


                // If only one warehouse is available, disable the control.
                if (ComboBoxControl.ValidValues.Count == 0)
                {
                    try
                    {
                        // Disable the select box
                        ComboBoxControl.Item.Enabled = false;

                        // Disable the "Move" button, no where to move to
                        ((Button)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "BTN_Move", pForm))
                            .Item.Enabled = false;

                        // Create the first item as an empty item
                        ComboBoxControl.ValidValues.Add("", "");

                        // Select the empty item (forcing the user to make a decision)
                        ComboBoxControl.Select(0, BoSearchKey.psk_Index);
                    }
                    catch { }
                }

                // If a single value is present, select it
                if (ComboBoxControl.ValidValues.Count == 1)
                {
                    // Select the first item in the combobox
                    var formUID = _SBO_Form.UniqueID;
                    ComboBoxControl.Select(0, BoSearchKey.psk_Index);
                    _SBO_Form = Globals.oApp.Forms.Item(formUID);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
            }

        }

        private void Load_Filter_Matrix(string WarehouseType,string pLocation="")
        {

            try
            {
                // Get the phase from the CSV
                Phase selectedPhase = CurrentlySelectedPhase();

                string sqlQuery = null;
                if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && selectedPhase.CurrentPhase == "FLO"))
                {

                    sqlQuery = $@"SELECT 'select' as [Select], [OWHS].[WhsCode] AS[WarehouseCode], [OWHS].[WhsName] AS[WarehouseName]
                                    , (
                                        SELECT COUNT(*)
                                        FROM[OSRN] WITH (NOLOCK)

                                        JOIN[OSRQ] ON[OSRQ].[ItemCode] = [OSRN].[ItemCode]
                                            AND[OSRQ].[SysNumber] = [OSRN].[SysNumber]
                                            JOIN[OITM] ON[OITM].[ItemCode] = [OSRN].[ItemCode]
                                            JOIN[OITB] ON[OITM].[ItmsGrpCod] = [OITB].[ItmsGrpCod]
                                            WHERE[OSRQ].[WhsCode] = [OWHS].[WhsCode]
                                            AND[OSRQ].[Quantity] > 0
	                                    AND[OITB].[ItmsGrpNam] = '{cItemType} '
	                                    ) AS[WarehousePlantCount]
                                    FROM
                                    [OWHS] WITH (NOLOCK)
                                    LEFT outer join [OLCT] on [OWHS].Location = [OLCT].Code
                                    WHERE
                                    [OWHS].[U_NSC_WhrsType] ='{WarehouseType}' AND OWHS.Inactive = 'N'";
                                    if (Globals.BranchDflt >= 0) sqlQuery += $" AND OWHS.BPLid IN ({Globals.BranchList})";                                       // #10823
                    if (pLocation.Trim().Length > 0)
                    {
                        sqlQuery += " and [OLCT].[Location]='" + pLocation + "'";
                        
                    }

                    // Load plants into the matrix
                    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(_SBO_Form,
                                  "MTX_FILTER",
                                  "MTX_FILTER",
                                  new List<CommonUI.Matrix.MatrixColumn>() {
                              new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Filter_Columns.Select.ToString("F"), Caption="", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    //new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Filter_Columns.WarehouseCode.ToString("F"), Caption="#", ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Filter_Columns.WarehouseName.ToString("F"), Caption="Warehouse Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Filter_Columns.WarehousePlantCount.ToString("F"), Caption="# of Plants", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                                  },
                              SQLQuery: sqlQuery);
                }
                else
                {
                    sqlQuery = $@"SELECT 'select' as [Select], [OWHS].[WhsCode] AS[WarehouseCode], [OWHS].[WhsName] AS[WarehouseName]
                                    , (
                                        SELECT COUNT(*)
                                        FROM[OBTN] WITH (NOLOCK)

                                        JOIN[OBTQ] ON[OBTQ].[ItemCode] = [OBTN].[ItemCode]
                                            AND[OBTQ].[SysNumber] = [OBTN].[SysNumber]
                                            JOIN[OITM] ON[OITM].[ItemCode] = [OBTN].[ItemCode]
                                            JOIN[OITB] ON[OITM].[ItmsGrpCod] = [OITB].[ItmsGrpCod]
                                            WHERE[OBTQ].[WhsCode] = [OWHS].[WhsCode]
                                            AND[OBTQ].[Quantity] > 0
	                                    AND [OITB].[ItmsGrpNam] = '{cItemType}'
	                                    ) AS [WarehousePlantCount]
                                    FROM
                                    [OWHS] WITH (NOLOCK)
                                    left outer join [OLCT] on [OWHS].Location = [OLCT].Code
                                    WHERE
                                    [OWHS].[U_NSC_WhrsType] ='{WarehouseType}' AND OWHS.Inactive = 'N'";
                    if (Globals.BranchDflt >= 0) sqlQuery += $" AND OWHS.BPLid IN ({Globals.BranchList})";                                       // #10823
                    if (pLocation.Trim().Length > 0)
                    {
                        sqlQuery += " and [OLCT].[Location]='" + pLocation + "'";

                    }
                    // Load plants into the matrix
                    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(_SBO_Form,
                                  "MTX_FILTER",
                                  "MTX_FILTER",
                                  new List<CommonUI.Matrix.MatrixColumn>() {
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Filter_Columns.Select.ToString("F"), Caption="", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    //new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Filter_Columns.WarehouseCode.ToString("F"), Caption="#", ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Filter_Columns.WarehouseName.ToString("F"), Caption="Warehouse Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Filter_Columns.WarehousePlantCount.ToString("F"), Caption="# of Plant Lots", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                                  },
                              SQLQuery: sqlQuery);
                }

                // Grab the matrix from the form UI
                Matrix MTX_FILTER = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_FILTER", _SBO_Form) as Matrix;

                // Allow single selection only.
                MTX_FILTER.SelectionMode = BoMatrixSelect.ms_Auto;

                //Load_Info_Matrix();
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private int SetFilterWarehouseToLowestPlantCount()
        {
            // Grab the matrix from the form UI
            Matrix MTX_FILTER = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_FILTER", _SBO_Form) as Matrix;

            // Setting stupidly high number of plants.
            int lowestPlantCount = 1000000000;
            int indexToSelect = 1;
            for (int i = 1; i < MTX_FILTER.RowCount + 1; i++)
            {
                // Get the ID of the note selected
                int plantNumber = Convert.ToInt32(MTX_FILTER.Columns.Item((int)Matrix_Filter_Columns.WarehousePlantCount).Cells.Item(i).Specific.Value.ToString());
                if (plantNumber < lowestPlantCount && plantNumber > 0)
                {
                    lowestPlantCount = plantNumber;
                    indexToSelect = i;
                }
            }

            MTX_FILTER.SelectRow(indexToSelect, true, false);
            return indexToSelect;
        }

        private void Load_Info_Matrix()

        {
            #region Generate Warehouse Selection Filter

            List<string> filterWarehouseCodes = GetWarehouseCodesOfSelected();

            string warehouseFilterClause = "";

            foreach (var warehouseCode in filterWarehouseCodes)
            {
                if (string.IsNullOrEmpty(warehouseFilterClause))
                {
                    warehouseFilterClause += string.Format("AND ( [OWHS].[WhsName] = '{0}' ", warehouseCode);
                }
                else
                {
                    warehouseFilterClause += string.Format(" OR [OWHS].[WhsName] = '{0}'", warehouseCode);
                }
            }

            if (warehouseFilterClause != "")
            {
                // Close up the Warehouse Filter Query OR Clause.
                warehouseFilterClause += ")";
            }

            #endregion
            // Get the phase from the CSV
            Phase selectedPhase = CurrentlySelectedPhase();

            string sqlQuery = null;
            if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && selectedPhase.CurrentPhase == "FLO"))
            {

                sqlQuery = string.Format(@"

SELECT 'select' AS [Select],
[STRAIN].[Name]                AS [StrainName]
	, Count([STRAIN].[Name])   AS [WarehousePlantCount]
	, [WhsName]                                 AS [WarehouseName]
    , [OSRQ].[WhsCode]                          AS [WarehouseCode]
FROM [OSRQ]
JOIN [OWHS] ON [OWHS].[WhsCode] = [OSRQ].[WhsCode] 

JOIN [OITM] ON [OITM].[ItemCode] = [OSRQ].[ItemCode]
JOIN [@" + NSC_DI.Globals.tStrains + @"] AS [STRAIN] ON [OITM].[U_NSC_StrainID] = [STRAIN].[Code]

WHERE [OSRQ].[Quantity] > 0

{0} -- clause generated from above

GROUP BY [STRAIN].[Name], [WhsName], [OSRQ].[WhsCode]
", warehouseFilterClause);

                // Load plants into the matrix
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(_SBO_Form,
                              "INFO",
                              "MTX_INFO",
                              new List<CommonUI.Matrix.MatrixColumn>() {
                               new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Filter_Columns.Select.ToString("F"), Caption="", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Info_Columns.StrainName.ToString("F"), Caption="Strain", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Info_Columns.WarehousePlantCount.ToString("F"), Caption="# of Plants", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Info_Columns.WarehouseName.ToString("F"), Caption="Warehouse Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                                    //new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Info_Columns.WarehouseCode.ToString("F"), Caption="Warehouse #", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                              },
                          SQLQuery: sqlQuery);

            }
            else
            {
                sqlQuery = string.Format(@"

SELECT 'select' AS [Select],
[STRAIN].[Name]                AS [StrainName]
	, Count([STRAIN].[Name])   AS [WarehousePlantCount]
	, [WhsName]                                 AS [WarehouseName]
    , [OBTQ].[WhsCode]                          AS [WarehouseCode]
FROM [OBTQ]
JOIN [OWHS] ON [OWHS].[WhsCode] = [OBTQ].[WhsCode] 

JOIN [OITM] ON [OITM].[ItemCode] = [OBTQ].[ItemCode]
JOIN [@" + NSC_DI.Globals.tStrains + @"] AS [STRAIN] ON [OITM].[U_NSC_StrainID] = [STRAIN].[Code]

WHERE [OBTQ].[Quantity] > 0

{0} -- clause generated from above

GROUP BY [STRAIN].[Name], [WhsName], [OBTQ].[WhsCode]
", warehouseFilterClause);

                // Load plants into the matrix
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(_SBO_Form,
                              "INFO",
                              "MTX_INFO",
                              new List<CommonUI.Matrix.MatrixColumn>() {
                               new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Filter_Columns.Select.ToString("F"), Caption="", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Info_Columns.StrainName.ToString("F"), Caption="Strain", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Info_Columns.WarehousePlantCount.ToString("F"), Caption="# of Plant Lots", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Info_Columns.WarehouseName.ToString("F"), Caption="Warehouse Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                                    //new CommonUI.Matrix.MatrixColumn(){ ColumnName=Matrix_Info_Columns.WarehouseCode.ToString("F"), Caption="Warehouse #", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                              },
                          SQLQuery: sqlQuery);

            }

            // Grab the matrix from the form UI
            Matrix MTX_INFO = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_INFO", _SBO_Form) as Matrix;

            //string str = Matrix_Info_Columns.WarehousePlantCount.ToString("F");

            // Allow single selection only.
            MTX_INFO.SelectionMode = BoMatrixSelect.ms_Single;
        }

        /// <summary>
        /// Loads the Matrix with data given a warehouse type
        /// </summary>
        /// <param name="WarehouseType"></param>
        /// 

        private void Load_Matrix(string WarehouseType)
		{
			Matrix oPlants = _SBO_Form.Items.Item("MTX_PLANTS").Specific;

            try
            {
                Form Form_Waiting = null;

                try
                {
                    Form_Waiting = F_Waiting.FormCreate();

                    F_Waiting.SetProgressBar(pForm: Form_Waiting, Width: 24);
                }
                catch { }

                List<string> filterWarehouseCodes = GetWarehouseCodesOfSelected();
                List<StrainClause> filterByStrain = GetStrainNamesOfSelected();

                #region Generate Strain Selection Filter

                string strainFilterClause = "";

                foreach (var strainCaluse in filterByStrain)
                {
                    if (string.IsNullOrEmpty(strainFilterClause))
                    {
                        strainFilterClause += string.Format(" AND ( ( [@" + NSC_DI.Globals.tStrains + "].[Name]  = '{0}' AND [OWHS].[WhsName] = '{1}' ) ",
                            strainCaluse.StrainName.Replace("'", "''"), strainCaluse.WarehouseCode);
                    }
                    else
                    {
                        strainFilterClause += string.Format(" OR ( [@" + NSC_DI.Globals.tStrains + "].[Name]  = '{0}' AND [OWHS].[WhsName] = '{1}' ) ",
                            strainCaluse.StrainName.Replace("'", "''"), strainCaluse.WarehouseCode);
                    }
                }

                // Close up the Strain Name Filter Query OR Clause.
                if (!string.IsNullOrEmpty(strainFilterClause))
                {
                    strainFilterClause += ")";
                }

                #endregion

                string warehouseFilterClause = "";

                if (string.IsNullOrEmpty(strainFilterClause))
                {
                    #region Generate Warehouse Selection Filter

                    foreach (var warehouseCode in filterWarehouseCodes)
                    {
                        if (string.IsNullOrEmpty(warehouseFilterClause))
                        {
                            warehouseFilterClause += string.Format(" AND ( [OWHS].[WhsName] = '{0}' ", warehouseCode);
                        }
                        else
                        {
                            warehouseFilterClause += string.Format(" OR [OWHS].[WhsName] = '{0}'", warehouseCode);
                        }
                    }

                    // Close up the Warehouse Filter Query OR Clause.
                    if (!string.IsNullOrEmpty(warehouseFilterClause))
                    {
                        warehouseFilterClause += ")";
                    }
                    #endregion
                }

                //10182 Creates a filter clause around the crop name that the user can input in TXT_CROP. The user can just use partial names for the search with because the clause uses LIKE
                string cropNameFilterClause = "";
                string cropName = _SBO_Form.Items.Item("TXT_CROP").Specific.Value.Trim();

                if (cropName != "")
                {
                    cropNameFilterClause += string.Format($" AND [OPMG].[NAME] LIKE '%{cropName}%'");
                }

                var dt = NSC_DI.UTIL.SQL.DataTable("SELECT CASE WHEN Activated = 'Y' THEN DispName ELSE '' END AS DispName FROM OBFC WHERE FldType = 'S'");
                string[] BinLevNames = dt.Select().Select(n => n["DispName"] as string).ToArray();

                string sqlQueryForMatrixPopulating = null;
                // Get the phase from the CSV
                Phase selectedPhase = CurrentlySelectedPhase();
                string strPlantTable = "OSRN";

                if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && selectedPhase.CurrentPhase == "FLO"))
                {
                    // 10182 CHANGED CREATE DATE TO ADMISSION DATE AND NOW USE INDATE IN THE SQL
                    sqlQueryForMatrixPopulating = $@"
SELECT 
	--(CONVERT(nvarchar,[OSRQ].[ItemCode])+'-'+CONVERT(nvarchar,[OSRN].[DistNumber]))  AS [UniqueID]
CONVERT(nvarchar,[OSRN].[DistNumber])  AS [UniqueID]
,	[OSRQ].[WhsCode]
,	[OWHS].[WhsName]
,	[OSRN].[InDate] as 'AdmissionDate'
,	[OSRN].[itemName]
,   [OSRN].[U_NSC_IsMother] as 'isMother'
,	[OSRN].[MnfSerial]
,   [OSRN].[LotNumber]
,	[OSRN].[U_NSC_HarvestName] AS [HarvestName]
,	[OSRN].[SysNumber]
,   CONVERT(INT,[@NSC_STRAIN].U_PrjctFlower)
    ,DATEDIFF(day,[OSRN].[CreateDate],CURRENT_TIMESTAMP) as 'DaysIn'
,   CONVERT(INT,[@NSC_STRAIN].U_PrjctVeg) -DATEDIFF(day,[OSRN].[CreateDate],CURRENT_TIMESTAMP) as 'DaysRemainingVeg'	
,   CASE WHEN OBIN.SysBin = 'Y' THEN NULL ELSE OBIN.SL1Code END  AS BinLev1, SL2Code AS BinLev2, SL3Code AS BinLev3, SL4Code AS BinLev4
,   CONVERT(INT,[@NSC_STRAIN].U_PrjctVeg) + CONVERT(INT,[@NSC_STRAIN].U_PrjctFlower)-DATEDIFF(day,[OSRN].[CreateDate],CURRENT_TIMESTAMP) as 'DaysRemainingFlo'
,   [OSRQ].[Quantity]
,   OSRN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]
,   [OITM].[ItemCode]
FROM [OSRQ]
JOIN [OSRN] ON [OSRN].[ItemCode] = [OSRQ].[ItemCode] AND [OSRN].[SysNumber] = [OSRQ].[SysNumber]
JOIN [OWHS] ON [OWHS].[WhsCode] = [OSRQ].[WhsCode]
JOIN [OITM] ON [OITM].[ItemCode] = [OSRQ].[ItemCode]
JOIN [@{NSC_DI.Globals.tStrains}] ON [OITM].[U_NSC_StrainID] = [@{NSC_DI.Globals.tStrains}].[Code]
LEFT JOIN OSBQ ON OSRN.AbsEntry  = OSBQ.SnBMDAbs AND OSBQ.OnHandQty > 0
LEFT JOIN OBIN ON OSBQ.BinAbs = OBIN.AbsEntry
left Join OPMG on OPMG.AbsEntry = OSRN.U_NSC_CropID
WHERE [OSRQ].[Quantity] > 0
{warehouseFilterClause}
{strainFilterClause}
{cropNameFilterClause}
ORDER BY [OSRN].[ItemCode],[OSRN].[SysNumber] ASC
";
    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(
        _SBO_Form,
        strPlantTable,
        "MTX_PLANTS",
        new List<CommonUI.Matrix.MatrixColumn>() {
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="Plant ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_LINKED_BUTTON  },
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="WhsName", Caption="Warehouse", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="AdmissionDate", Caption="Admission Date", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="DaysRemainingVeg", Caption="Est. Days Left", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable=false  },
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="BinLev1", Caption=BinLevNames[0], ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable=false  },
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="BinLev2", Caption=BinLevNames[1], ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable=false  },
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="BinLev3", Caption=BinLevNames[2], ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable=false  },
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="BinLev4", Caption=BinLevNames[3], ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable=false  },
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="MnfSerial", Caption="State ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
            new CommonUI.Matrix.MatrixColumn(){ColumnName="LotNumber", Caption="Lot Number", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT },
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="HarvestName", Caption="Harvest Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="Quantity", Caption="Quantity", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
            new CommonUI.Matrix.MatrixColumn(){ ColumnName="isMother", Caption="Is Mother", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
            ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropID",               Caption="Crop ID",               ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
            ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropName",             Caption="Crop Name",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
            ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="ItemCode", Caption="ItemCode", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
        },
     SQLQuery: sqlQueryForMatrixPopulating);

                }
                else
                {
                    strPlantTable = "OBTN";

                    sqlQueryForMatrixPopulating = $@"
                SELECT 
	                --(CONVERT(nvarchar,[OBTQ].[ItemCode])+'-'+CONVERT(nvarchar,[OBTN].[DistNumber]))  AS [UniqueID]
                CONVERT(nvarchar,[OBTN].[DistNumber])  AS [UniqueID]
                ,	[OBTQ].[WhsCode]
                ,	[OWHS].[WhsName]
                ,	[OBTN].[InDate] as 'AdmissionDate'
                ,	[OBTN].[itemName]
                ,   [OBTN].[U_NSC_IsMother] as 'isMother'
                ,	[OBTN].[MnfSerial]
                ,   [OBTN].[LotNumber]
                ,	[OBTN].[U_NSC_HarvestName] as [HarvestName]
                ,	[OBTN].[SysNumber]
                ,   CONVERT(INT,[@NSC_STRAIN].U_PrjctFlower) as 'Strain'
                ,   DATEDIFF(day,[OBTN].[CreateDate],CURRENT_TIMESTAMP) as 'DaysIn'
                ,   CONVERT(INT,[@NSC_STRAIN].U_PrjctVeg) -DATEDIFF(day,[OBTN].[CreateDate],CURRENT_TIMESTAMP) as 'DaysRemainingVeg'	
                ,   CASE WHEN OBIN.SysBin = 'Y' THEN NULL ELSE OBIN.SL1Code END  AS BinLev1, SL2Code AS BinLev2, SL3Code AS BinLev3, SL4Code AS BinLev4
                ,   CONVERT(INT,[@NSC_STRAIN].U_PrjctVeg) + CONVERT(INT,[@NSC_STRAIN].U_PrjctFlower)-DATEDIFF(day,[OBTN].[CreateDate],CURRENT_TIMESTAMP) as 'DaysRemainingFlo'
                ,   [OBTQ].[Quantity]
                ,   OBTN.U_NSC_CropID as [CropID], OPMG.NAME as 'CropName'
                ,   [OITM].[ItemCode]
                FROM [OBTQ]
                JOIN [OBTN] ON [OBTN].[ItemCode] = [OBTQ].[ItemCode] AND [OBTN].[SysNumber] = [OBTQ].[SysNumber]
                JOIN [OWHS] ON [OWHS].[WhsCode] = [OBTQ].[WhsCode]
                JOIN [OITM] ON [OITM].[ItemCode] = [OBTQ].[ItemCode]
                JOIN [@{NSC_DI.Globals.tStrains}] ON [OITM].[U_NSC_StrainID] = [@{NSC_DI.Globals.tStrains}].[Code]
                LEFT JOIN OBBQ ON OWHS.WhsCode = OBBQ.WhsCode AND OBTN.AbsEntry = OBBQ.SnBMDAbs AND OBBQ.OnHandQty > 0
                LEFT JOIN OBIN ON OBBQ.BinAbs = OBIN.AbsEntry
                left Join OPMG on OPMG.AbsEntry = OBTN.U_NSC_CropID
                WHERE [OBTQ].[Quantity] > 0  
                {warehouseFilterClause}
                {strainFilterClause}
                {cropNameFilterClause}
                ORDER BY [OBTN].[ItemCode],[OBTN].[SysNumber] ASC";

                    //GetBatchSQLString(warehouseFilterClause, strainFilterClause);

                    // 10182 CHANGED CREATE DATE TO ADMISSION DATE AND NOW USE INDATE IN THE SQL
                    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(
                            _SBO_Form,
                            strPlantTable,
                            "MTX_PLANTS",
                            new List<CommonUI.Matrix.MatrixColumn>() {
                                 new CommonUI.Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="Plant ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_LINKED_BUTTON  },
                                new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                new CommonUI.Matrix.MatrixColumn(){ ColumnName="WhsName", Caption="Warehouse", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                new CommonUI.Matrix.MatrixColumn(){ ColumnName="AdmissionDate", Caption="Admission Date", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                new CommonUI.Matrix.MatrixColumn(){ ColumnName="DaysRemainingVeg", Caption="Est. Days Left", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable=false  },
                                new CommonUI.Matrix.MatrixColumn(){ ColumnName="BinLev1", Caption=BinLevNames[0], ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable=false  },
                                new CommonUI.Matrix.MatrixColumn(){ ColumnName="BinLev2", Caption=BinLevNames[1], ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable=false  },
                                new CommonUI.Matrix.MatrixColumn(){ ColumnName="BinLev3", Caption=BinLevNames[2], ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable=false  },
                                new CommonUI.Matrix.MatrixColumn(){ ColumnName="BinLev4", Caption=BinLevNames[3], ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable=false  },
                                new CommonUI.Matrix.MatrixColumn(){ ColumnName="MnfSerial", Caption="State ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                new CommonUI.Matrix.MatrixColumn(){ColumnName="LotNumber", Caption="Lot Number", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT },
                                new CommonUI.Matrix.MatrixColumn(){ ColumnName="HarvestName", Caption="Harvest Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                new CommonUI.Matrix.MatrixColumn(){ ColumnName="Quantity", Caption="Quantity", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable = true  },
                                new CommonUI.Matrix.MatrixColumn(){ ColumnName="isMother", Caption="Is Mother", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                                ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropID",               Caption="Crop ID",               ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                                ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropName",             Caption="Crop Name",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                                ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="ItemCode", Caption="ItemCode", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                            },
                         SQLQuery: sqlQueryForMatrixPopulating);

                }

				// Grab the matrix from the form UI
				Matrix MTX_PLANTS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLANTS", _SBO_Form) as Matrix;

                try { F_Waiting.SetProgressBar(Form_Waiting, Width: 55); } catch { }

                //CommonUI.Matrix.LoadDatabaseDataIntoMatrix( 
                //              _SBO_Form,
                //              "OSRN",
                //              "MTX_PLANTS",
                //              new List<CommonUI.Matrix.MatrixColumn>() { 
                //                        new CommonUI.Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="Plant ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_LINKED_BUTTON  },
                //                        new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                //                        new CommonUI.Matrix.MatrixColumn(){ ColumnName="WhsName", Caption="Warehouse", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                //                        new CommonUI.Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                //                        new CommonUI.Matrix.MatrixColumn(){ ColumnName="DaysRemainingVeg", Caption="DaysIn", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable=false  },
                //                        new CommonUI.Matrix.MatrixColumn(){ ColumnName="MnfSerial", Caption="State ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                //              },
                //          SQLQuery: sqlQueryForMatrixPopulating);

                oPlants.Columns.Item(Matrix_Plants_Columns.BinLev1).Visible = (BinLevNames[0] != "");
                oPlants.Columns.Item(Matrix_Plants_Columns.BinLev2).Visible = (BinLevNames[1] != "");
                oPlants.Columns.Item(Matrix_Plants_Columns.BinLev3).Visible = (BinLevNames[2] != "");
                oPlants.Columns.Item(Matrix_Plants_Columns.BinLev4).Visible = (BinLevNames[3] != "");
                //oPlants.Columns.Item(14).Visible = false;

                oPlants.AutoResizeColumns();

				try {F_Waiting.SetProgressBar(pForm: Form_Waiting, Width: 76); } catch { }


                // Allow multi-selection on the Matrix
                MTX_PLANTS.SelectionMode = BoMatrixSelect.ms_Auto;

				try {F_Waiting.SetProgressBar(pForm: Form_Waiting, Width: 100); } catch { }

                // Enable the button by default
                ((Button)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "BTN_MOVE", _SBO_Form))
					.Item.Enabled = true;

				try {F_Waiting.Form_Close(Form_Waiting); } catch { }
            }
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oPlants);
				GC.Collect();
			}
		}

        public List<string> GetWarehouseCodesOfSelected()
        {
            List<string> warehouseCodes = new List<string>();
            // Grab the matrix from the form UI
            Matrix MTX_FILTER = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_FILTER", _SBO_Form) as Matrix;
            string warehouseCode = "";

            // Which column stores the ID
            int warehouseCodeColumn = (int)Matrix_Filter_Columns.WarehouseName;

            for (int i = 1; i <= MTX_FILTER.RowCount; i++)
            {
                
                if (MTX_FILTER.IsRowSelected(i))
                {
                    // Get the ID of the note selected                    
                    warehouseCode = MTX_FILTER.Columns.Item(warehouseCodeColumn).Cells.Item(i).Specific.Value.ToString();
                    warehouseCodes.Add(warehouseCode);
                }
            }

            if (warehouseCodes.Count == 0 && MTX_FILTER.RowCount > 0)
            {
                int intIndexOfSelected = SetFilterWarehouseToLowestPlantCount();
                warehouseCode = MTX_FILTER.Columns.Item(warehouseCodeColumn).Cells.Item(intIndexOfSelected).Specific.Value.ToString();
                warehouseCodes.Add(warehouseCode);
                //warehouseCodes = new List<string>() { SetFilterWarehouseToLowestPlantCount().ToString() };
            }
            return warehouseCodes;
        }

        public class StrainClause
        {
            public string StrainName { get; set; }
            public string WarehouseCode { get; set; }
        }

        public List<StrainClause> GetStrainNamesOfSelected()
        {
            List<StrainClause> strainClauseList = new List<StrainClause>();
            // Grab the matrix from the form UI
            Matrix MTX_INFO = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_INFO", _SBO_Form) as Matrix;
            StrainClause strainClause;

            // Which column stores the ID
            int strainNameColumn = (int)Matrix_Info_Columns.StrainName;

            for (int i = 1; i < MTX_INFO.RowCount + 1; i++)
            {
                if (MTX_INFO.IsRowSelected(i))
                {
                    strainClause = new StrainClause();
                    // Get the ID of the note selected
                    strainClause.StrainName = MTX_INFO.Columns.Item(Matrix_Info_Columns.StrainName).Cells.Item(i).Specific.Value.ToString();
                    strainClause.WarehouseCode = MTX_INFO.Columns.Item(Matrix_Info_Columns.WarehouseName).Cells.Item(i).Specific.Value.ToString();

                    strainClauseList.Add(strainClause);
                }
            }

            return strainClauseList;
        }

        public string CurrentWarehouseType { get; set; }

        public void RefreshTabCount()
        {
            Load_TabCount();
        }

        private int CurrentlySelectedTab()
        {
            int count = GetPhaseCount();
            for (int i = 1; i <= count; i++)
            {
                if (((Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_" + i, _SBO_Form)).Selected)
                {
                    return i;
                }
            }

            return 1;
        }

        private static int GetPhaseCount()
        {
            var sqlQuery = @"SELECT COUNT(*) FROM [@" + NSC_DI.Globals.tPDOPhase + @"]";
            var count = NSC_DI.UTIL.SQL.GetValue<int>(sqlQuery, 0);
            return count;
        }

        private void UpdateProgressBar(string Message, int ProgressToIncreaseBy = 0)
        {
            try
            {
                // Set progress bar message
                _progressBar.Text = Message;

                // If we are increasing the progress bar size
                if (ProgressToIncreaseBy > 0)
                {
                    // Calculate the new progress
                    int NewProgressBarValue = _progressBar.Value + ProgressToIncreaseBy;

                    // Make sure our new progress amount is not greater than the maxium allowed
                    if (NewProgressBarValue <= _progressBar.Maximum)
                    {
                        _progressBar.Value = NewProgressBarValue;
                    }
                }
            }
            catch { }
        }

        private List<string> SelectedPlantIDs
        {
            get
            {
                // Keep track of the selected Plant ID's
                List<string> SelectedPlantIDs = new List<string>();

                // Grab the matrix from the form UI
                Matrix MTX_PLANTS = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLANTS", _SBO_Form) as Matrix;

                // For each row already selected
                for (int i = 1; i < (MTX_PLANTS.RowCount + 1); i++)
                {
                    if (MTX_PLANTS.IsRowSelected(i))
                    {
                        // Grab the selected row's Plant ID column
                        string plantID = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.PlantID).Cells.Item(i).Specific).Value;
                        // Add the selected row plant ID to list
                        SelectedPlantIDs.Add(plantID);
                    }
                }

                return SelectedPlantIDs;
            }
            set
            {
                return;
            }
        }

        private Dictionary<string, string> SelectedPlantIDAndWarehouses
        {
            get
            {
                // Keep track of the selected Plant ID's
                Dictionary<string, string> SelectedPlantAndWarehouseIDs = new Dictionary<string, string>();

                // Grab the matrix from the form UI
                Matrix MTX_PLANTS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLANTS", _SBO_Form) as Matrix;
                // For each row already selected
                for (int i = 1; i < (MTX_PLANTS.RowCount + 1); i++)
                {
                    if (MTX_PLANTS.IsRowSelected(i))
                    {
                        // Grab the selected row's Plant ID column
                        string plantID = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.PlantID).Cells.Item(i).Specific).Value;
                        string warehouseName = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.WarehouseName).Cells.Item(i).Specific).Value;
                        string warehouseCode = NSC_DI.SAP.Warehouse.GetCode(warehouseName);

                        // Add the selected row plant ID to list
                        SelectedPlantAndWarehouseIDs.Add(plantID, warehouseCode);
                    }
                }

                return SelectedPlantAndWarehouseIDs;
            }
            set
            {
                return;
            }
        }

        private void CBT_ACTION_SELECT()
        {
            try
            {

                ButtonCombo CBT_ACTION = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ButtonCombo, "CBT_ACTION", _SBO_Form);

                if (CBT_ACTION.Selected != null)
                {
                    List<string> SelectedPlants = this.SelectedPlantIDs;

                    if (SelectedPlants.Count > 0)
                    {
                        Phase selectedPhase = CurrentlySelectedPhase();
                        switch (CBT_ACTION.Selected.Description)
                        {
                            case "1":
                                //TODO: change to use same parent form instead of new ones every time
                                //(NavSol_AddOn.GetFormFromID("VSC_WATER_WIZ") as WaterWizard).Form_Load(ListOfPlantIDsToLoad: SelectedPlants.ToArray());
                                
                                (new F_WaterWizard()).Form_Load(false, SelectedPlants.ToArray(), selectedPhase.CurrentPhase);
                                break;

                            case "2":
                                F_FeedWizard.Form_Load(false, SelectedPlants.ToArray(),selectedPhase.CurrentPhase);
                                break;

                            case "3":
                                (new F_WasteWizard()).Form_Load(false, SelectedPlants.ToArray(), selectedPhase.CurrentPhase);
                                break;

                            case "4":
                                (new F_TreatWizard()).Form_Load(SelectedPlants.ToArray(), selectedPhase.CurrentPhase);
                                break;

                            case "5":
                                 //Dictionary<string, string> NewPlantHolder = this.SelectedPlantIDAndWarehouses;
                                F_QuarantineWizard.FormCreate(SelectedPlantIDAndWarehouses, false, null, selectedPhase.CurrentPhase);
                                break;
                            case "6":

                                // Find out which tab is selected
                                Phase CurrentPhase = CurrentlySelectedPhase();

                                if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase.CurrentPhase == "FLO"))
                                {
                                    this.SetMotherState(this.SelectedPlantIDs);
                                }
                                else
                                {
                                    //ToDo: Create a single Mother plant (serialized Plant) from a batch of plants. 
                                    Dictionary<string, string> NewPlantHolder = this.SelectedPlantIDAndWarehouses;

                                    List<int> PrdOrdList = CreateIndividualProductionOrdersMother(NewPlantHolder);

                                    //Create Receipt from Production                                    
                                    List<IEnumerable<string>> SNs_List = ReceiveMotherPlants(PrdOrdList);

                                    //Get a list of Serialized plants that were just created. 
                                    List<string> SNs = new List<string>();
                                    foreach (var SN in SNs_List)
                                    {
                                        List<string> strChecker = SN.ToList<string>();
                                        foreach(var ID in strChecker)
                                        {
                                            SNs.Add(ID);
                                        }
                                        
                                    }

                                    this.SetMotherState(SNs);

                                }
                                break;
                        }
                    }
                    else
                    {
                        Globals.oApp.StatusBar.SetText("Please select some plants first!");
                    }
                }

                BTN_FILTER_ITEM_PRESSED(false);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void IMG_MAIN_PRESSED()
        {
            // Grab the matrix from the form UI
            Matrix MTX_FILTER = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_FILTER", _SBO_Form) as Matrix;
            int FirstSelectedRow = MTX_FILTER.GetNextSelectedRow(0, BoOrderType.ot_SelectionOrder);
            string WhrsName = ((EditText)MTX_FILTER.Columns.Item(1).Cells.Item(FirstSelectedRow).Specific).Value;

            Globals.oApp.StatusBar.SetText("Opening your Garden View-"+ WhrsName.Trim(), BoMessageTime.bmt_Medium,BoStatusBarMessageType.smt_Success);

            //Open a Viridian Crystal Report For the Given Room 
            string strReportMenuID = NSC_DI.UTIL.SQL.GetValue<string>("SELECT MenuUID FROM OCMN WHERE Name = 'Garden View-"+ WhrsName.Trim() + "' AND Type = 'C'", "");
            Globals.oApp.Menus.Item(strReportMenuID).Activate();

            Form oForm = Globals.oApp.Forms.ActiveForm;
            //EditText oEdit = oForm.Items.Item("1000003").Specific;
            //oEdit.Value = "12";
            oForm.Items.Item("1").Click();
            oForm.Items.Item("1").Visible = false;
            oForm.Items.Item("2").Click();
        }

        private void SetMotherState(List<string> SetStatePlantIDs)
        {
            foreach (string PlantID in SetStatePlantIDs)
            {
                //"I-1005-5" == "$TCODE-TNUM-SysSerial"
                
                string ItemCode = NSC_DI.SAP.Items.SNs.GetItemCode(PlantID);
                
                string SysNumber = PlantID;

                Globals.oApp.StatusBar.SetText("Setting mother state to YES for '" + PlantID + "'", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                NSC_DI.SAP.Plant.ChangeMotherStatus(ItemCode: ItemCode, DistNumber: SysNumber, NewPlantMotherState: "Y");
            }
            Globals.oApp.StatusBar.SetText("Setting mother state(s) succeeded", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
        }
        
        #region Matrix Click Events

        private void MTX_PLANTS_MATRIX_LINK_PRESSED(Form pForm, ItemEvent SAP_UI_ItemEvent)
        {
            try
            {
                int SelectedRowID = SAP_UI_ItemEvent.Row;

                if (SelectedRowID > 0)
                {
                    // Which column stores the ID
                    int ColumnIDForIDOfItemSelected = 0;

                    // Get the ID of the note selected
					//string ItemSelected = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLANTS", _SBO_Form).Columns.Item(ColumnIDForIDOfItemSelected).Cells.Item(SelectedRowID).Specific.Value.ToString();
					string ItemSelected = pForm.Items.Item("MTX_PLANTS").Specific.Columns.Item(ColumnIDForIDOfItemSelected).Cells.Item(SelectedRowID).Specific.Value.ToString();

                    // Open the plant manager for the selected plant
                    Phase selectedPhase = CurrentlySelectedPhase();
                    F_PlantDetails.FormCreate(ItemSelected,false ,null, selectedPhase.CurrentPhase);
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void MTX_INFO_MATRIX_PRESSED(ItemEvent SAP_UI_ItemEvent)
        {
            try
            {
                int SelectedRowID = SAP_UI_ItemEvent.Row;

                if (SelectedRowID > 0)
                {
                    try
                    {
                        Matrix MTX_INFO = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_INFO", _SBO_Form) as Matrix;

                        // Which column stores the ID
                        int plantCountColumn = (int)Matrix_Filter_Columns.WarehousePlantCount;

                        int plantCount = Convert.ToInt32(MTX_INFO.Columns.Item(plantCountColumn).Cells.Item(SelectedRowID).Specific.Value.ToString());

                        // Do additional filtering? Possible to allow them to select an item in this Matrix and only select that strain type.
                    }
                    catch (Exception ex) { }

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void MTX_FILTER_MATRIX_PRESSED(ItemEvent SAP_UI_ItemEvent)
        {
            try
            {
                CommonUI.Matrix.SelectRow(_SBO_Form, SAP_UI_ItemEvent, BoMatrixSelect.ms_Single);

                int SelectedRowID = SAP_UI_ItemEvent.Row;

                if (SelectedRowID > 0)
                {
                    try
                    {
                        Matrix MTX_FILTER = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_FILTER", _SBO_Form) as Matrix;

                        // Which column stores the ID
                        int plantCountColumn = (int)Matrix_Filter_Columns.WarehousePlantCount;

                        int plantCount = Convert.ToInt32(MTX_FILTER.Columns.Item(plantCountColumn).Cells.Item(SelectedRowID).Specific.Value.ToString());

                        Load_Info_Matrix();
                    }
                    catch (Exception ex) { }
                }
            } catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void MTX_PLANTS_MATRIX_PRESSED(ItemEvent SAP_UI_ItemEvent)
        {
            try
            {
                int SelectedRowID = SAP_UI_ItemEvent.Row;

                if (SelectedRowID > 0)
                {
                    try
                    {
                        Matrix MTX_PLANTS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLANTS", _SBO_Form) as Matrix;

                        // Which column stores the ID
                        string StateID = MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.StateID).Cells.Item(SelectedRowID).Specific.Value.ToString();

                        if(StateID.Trim().Length>0)
                        {
                            //State ID Already Exists - Do not
                        }
                        else
                        {

                        }

                    }
                    catch (Exception ex) { }
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }
        #endregion Matrix Click Events

        #region Button Click Events

        private void TAB_PRESSED(ItemEvent SAP_UI_ItemEvent)
        {

            Console.WriteLine("Tab press");
            try
            {
                int tabNumber = Convert.ToInt32(SAP_UI_ItemEvent.ItemUID.Replace("TAB_", ""));
                LoadTab(tabNumber);
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }
        private void LoadTab(int tabNumber)
        {
            SAPbouiCOM.Form oForm = CommonUI.Forms.GetActiveForm();
            try
            {
                Form Form_Waiting = null;
                var formUID = _SBO_Form.UniqueID;
                Form_Waiting = F_Waiting.FormCreate();
                F_Waiting.SetProgressBar(Form_Waiting, 24);

                // Get the phase from the CSV
                Phase selectedPhase = CurrentlySelectedPhase();

                CurrentWarehouseType = selectedPhase.CurrentPhase;
                // 10182 need to make batch field and chk visable or not here. need to check for focus
                this.Load_Filter_Matrix(selectedPhase.CurrentPhase);
                F_Waiting.SetProgressBar(Form_Waiting, 55, "Loading Plant data into the matrix.");

                Load_Info_Matrix();
                F_Waiting.SetProgressBar(Form_Waiting, 76);
                try
                {
                    // Clear out the Matrix.
                    Matrix MTX_PLANTS = _SBO_Form.Items.Item("MTX_PLANTS").Specific;
                    MTX_PLANTS.Clear();

                }
                catch (Exception ex)
                {

                }
                // Find the Combobox of Warehouses from the Form UI
                StaticText lblWarehouse = _SBO_Form.Items.Item("LBL_WRHS").Specific;

                // Find the Combobox of Warehouses from the Form UI
                ComboBox CBX_WHSE = _SBO_Form.Items.Item("CBX_WHSE").Specific;

                // Find the Action Button from the Form UI so we can set the text.
                Button btnAction = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "BTN_MOVE", _SBO_Form);

                if (selectedPhase.NextPhase != "WET")
                {
                    // Load the next phases warehouses in the combobox
                    //this.Load_Combobox_Warehouses(selectedPhase.NextPhase, CBX_WHSE);
                    Load_Warehouses(_SBO_Form, selectedPhase.NextPhase);

                    try
                    {
                        // To prevent event from firing at the beginning, let's only call this when neccesary
                        //if (!CBX_WHSE.Item.Visible)
                        if (btnAction.Item.Specific.caption == "Harvest")
                        {
                            lblWarehouse.Item.Visible = true;

                            // had to replace the following lines by setting the width.
                            // for some reason the old code prevents the field from functioning correctly
                            //CBX_WHSE.Item.Visible = true;
                            //CBX_WHSE.Item.Enabled = true;
                            CBX_WHSE.Item.Width = btnAction.Item.Width;
                            btnAction.Item.Specific.caption = "Move";

                            CBX_WHSE.Active = true;
                        }

                    }
                    catch (Exception e) { }
                }
                else
                {
                    //// If items already exist in the drop down
                    //if (CBX_WHSE.ValidValues.Count > 0)
                    //{
                    //    // Remove all currently existing values from warehouse drop down
                    //    for (int i = CBX_WHSE.ValidValues.Count; i-- > 0;)
                    //    {
                    //        CBX_WHSE.ValidValues.Remove(i, BoSearchKey.psk_Index);
                    //    }
                    //}

                    try
                    {
                        _SBO_Form.VisibleEx = true;

                        (_SBO_Form.Items.Item("TXT_LOOKUP").Specific as EditText).Active = true;

                        // had to replace the following lines by setting the width.
                        // for some reason the old code prevents the field from functioning correctly
                        //CBX_WHSE.Item.Enabled = false;
                        //CBX_WHSE.Item.Visible = false;
                        CBX_WHSE.Item.Width = -1;

                        lblWarehouse.Item.Visible = false;

                        btnAction.Item.Specific.caption = "Harvest";
                    }
                    catch (Exception ex) { }
                }

                // Load the available actions
                this.Load_AvailablActions(selectedPhase);

                F_Waiting.SetProgressBar(pForm: Form_Waiting, Width: 100, Message: "Loaded the available actions.  All done!");

                F_Waiting.Form_Close(Form_Waiting);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oForm.Freeze(false);
            }
        }

        private void BTN_FILTER_ITEM_PRESSED(bool rootCallingObject = true)
        {
            try
            {
                // Load the matrix with data from the selected tab
                this.Load_Matrix(CurrentWarehouseType);
            }
             catch (Exception ex)
            {
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
        }

        private void BTN_LOOKUP_ITEM_PRESSED()
        {
            try
            { 
                // Grad the textbox for the barcode scanner from the form UI
                EditText TXT_LOOKUP = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", _SBO_Form);

                // If nothing was passed in the textbox, just return
                if (TXT_LOOKUP.Value.Length == 0)
                {
                        Globals.oApp.StatusBar.SetText("No Data To Scan! - Please Enter A Valid Value.");
                        return;
                }

                // Grab the matrix from the form UI
                Matrix MTX_PLANTS = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLANTS", _SBO_Form) as Matrix;
                CheckBox oChkBox_Assign_StateID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_STID", _SBO_Form) as CheckBox;
                bool Add_State_ID = oChkBox_Assign_StateID.Checked;
                if (Add_State_ID == true)
                {
                    bool bRowIsSelected = false;
                    int intSelectedRowCount = 0;
                    int intLastRowIndex = 0;
                    //Add Code to Validate Single row selection
                    for (int i = 1; i < (MTX_PLANTS.RowCount + 1); i++)
                    {
                        bRowIsSelected = MTX_PLANTS.IsRowSelected(i);
                        if (bRowIsSelected == true)
                        {
                            intLastRowIndex = i;
                            intSelectedRowCount++;
                            bRowIsSelected = false;
                        }
                    }
                    if (intSelectedRowCount > 1)
                    {
                        //Show error message to indicate you can only select one row when assigning StateId
                        Globals.oApp.StatusBar.SetText("You may only select one row when assigning a StateID");
                        return;
                    }
                    //Check if ID already exist --, and (assign ID or Overwrite Existing)
                    string StateID = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.StateID).Cells.Item(intLastRowIndex).Specific).Value;
                    string DistNumber = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.PlantID).Cells.Item(intLastRowIndex).Specific).Value;
                    string ItemCode = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.ItemCode).Cells.Item(intLastRowIndex).Specific).Value;
                    SAPbobsCOM.SerialNumberDetail oSerialDetails = null;
                    CompanyService oCoService = Globals.oCompany.GetCompanyService() as CompanyService;
                    SerialNumberDetailsService oSerialService = oCoService.GetBusinessService(ServiceTypes.SerialNumberDetailsService);
                    SerialNumberDetailParams oParams = oSerialService.GetDataInterface(SerialNumberDetailsServiceDataInterfaces.sndsSerialNumberDetailParams);

                    try
                    {
                        //if (StateID.Trim().Length == 0)
                        //{
                        //    // RH commented out the lines
                        //    ////Add field in DB
                        //    ////Get and Set the DocEntry
                        //    ////***To Do: Up date this so that it is pulling on the correct DistNumber once Randy adjust this you will need to
                        //    ////Uncomment the line below and delete the current select State used for testing. 
                        //    //int intIndexHolder = DistNumber.IndexOf('-');
                        //    //DistNumber = DistNumber.Remove(0, intIndexHolder+1);
                        //    //intIndexHolder = DistNumber.IndexOf('-');
                        //    //DistNumber = DistNumber.Remove(0, intIndexHolder + 1);
                        //    string strSQL = string.Format(@"Select AbsEntry From [OSRN] where DistNumber='{0}'", DistNumber);
                        //    int DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                        //    oParams.DocEntry = DocEntry;
                        //    oSerialDetails = oSerialService.Get(oParams);
                        //    oSerialDetails.MfrSerialNo = TXT_LOOKUP.Value;
                        //    oSerialService.Update(oSerialDetails);
                        //    //Success Reload Matrix
                        //    _SBO_Form.Freeze(true);
                        //    Phase CurrentPhase = CurrentlySelectedPhase();
                        //    Load_Matrix(CurrentPhase.ToString());
                        //    // Empty the barcode scanner field
                        //    TXT_LOOKUP.Value = "";
                        //    // Focus back into the barcode scanner's text field
                        //    TXT_LOOKUP.Active = true;
                        //    //Move to next Row if not last row
                        //    if (MTX_PLANTS.RowCount != intLastRowIndex)
                        //    {
                        //        MTX_PLANTS.SelectRow(intLastRowIndex + 1, true, true);
                        //    }
                        //}
                        //else
                        //{
                        //    //Update field in DB
                        //    int iReturnVal = Globals.oApp.MessageBox("A State ID already exists for this item would you like to replace it?", 2, "Cancel", "OK");
                        //    switch (iReturnVal)
                        //    {
                        //        case 1:
                        //            //Cancel was selected do nothing                                         
                        //            break;
                        //        case 2:
                        //            //Update the field in DB
                        //            //Get and Set the DocEntry                                 
                        //            string strSQL = string.Format(@"Select [OSRN].[AbsEntry] From [OSRN] where [OSRN].[DistNumber]='{0}'", DistNumber);
                        //            int DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                        //            oParams.DocEntry = DocEntry;
                        //            oSerialDetails = oSerialService.Get(oParams);
                        //            oSerialDetails.MfrSerialNo = TXT_LOOKUP.Value;
                        //            oSerialService.Update(oSerialDetails);
                        //            //Success Reload Matrix
                        //            _SBO_Form.Freeze(true);
                        //            Phase CurrentPhase = CurrentlySelectedPhase();
                        //            Load_Matrix(CurrentPhase.ToString());
                        //            // Empty the barcode scanner field
                        //            TXT_LOOKUP.Value = "";
                        //            // Focus back into the barcode scanner's text field
                        //            TXT_LOOKUP.Active = true;
                        //            //Move to next Row if not last row
                        //            if (MTX_PLANTS.RowCount != intLastRowIndex)
                        //            {
                        //                MTX_PLANTS.SelectRow(intLastRowIndex + 1, true, true);
                        //            }
                        //            break;
                        //    }
                        if (StateID.Trim().Length > 0 && Globals.oApp.MessageBox("A State ID already exists for this item would you like to replace it?", 2, "Cancel", "OK") == 1) return;
                        switch (NSC_DI.SAP.Items.ManagedBySB(DistNumber))
                        {
                            case "S":
                                NSC_DI.SAP.SerialItems.SetStateID(DistNumber, ItemCode, TXT_LOOKUP.Value);
                                break;

                            case "B":
                                NSC_DI.SAP.BatchItems.SetStateID(DistNumber, ItemCode, TXT_LOOKUP.Value);
                                break;

                            default:
                                break;
                        }
                        //Success Reload Matrix
                        _SBO_Form.Freeze(true);
                        Phase CurrentPhase = CurrentlySelectedPhase();
                        Load_Matrix(CurrentPhase.ToString());
                        // Empty the barcode scanner field
                        TXT_LOOKUP.Value = "";
                        // Focus back into the barcode scanner's text field
                        TXT_LOOKUP.Active = true;
                        //Move to next Row if not last row
                        if (MTX_PLANTS.RowCount != intLastRowIndex)
                        {
                            MTX_PLANTS.SelectRow(intLastRowIndex + 1, true, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                    }
                    finally
                    {
                        //Clean up the objects and Garbage Collect
                        _SBO_Form.Freeze(false);
                        NSC_DI.UTIL.Misc.KillObject(oSerialDetails);
                        NSC_DI.UTIL.Misc.KillObject(oCoService);
                        NSC_DI.UTIL.Misc.KillObject(oSerialService);

                        GC.Collect();
                    }
                }
                else
                {
                    //Process as normal 

                    // Keep track of the warehouse ID from the barcode (if passed)
                    string WarehouseID = "";

                    // If the first four characters of the barcode are "WHSE", then the rest of the barcode contains the warehouse code.
                    if (TXT_LOOKUP.Value.Length >= 4 && TXT_LOOKUP.Value.Substring(0, 4) == "WHSE")
                    {
                        WarehouseID = TXT_LOOKUP.Value.Replace("WHSE-", "");
                    }

                    // Keep track if anything was selected.  At the end, if nothing was selected, click on the tab for that warehouse and select the plants in that warehouse
                    bool WasAnythingSelected = false;

                    List<string> newIds = new List<string>();
                    var itemCode = "";

                    // For each row already selected
                    for (int i = 1; i < (MTX_PLANTS.RowCount + 1); i++)
                    {
                        // Grab the row's Plant ID column
                        string plantID = MTX_PLANTS.GetCellSpecific(Matrix_Plants_Columns.PlantID, i).Value.ToString();
                        
                        // Grab the row's Warehouse Code column
                        string plantsWarehouseID = MTX_PLANTS.GetCellSpecific(Matrix_Plants_Columns.WarehouseName, i).Value.ToString();
                        
                        // Grab the row's Stated ID column                    
                        string plantsStateID = MTX_PLANTS.GetCellSpecific(Matrix_Plants_Columns.StateID, i).Value.ToString();
                        
                        // If the warehouse was scanned
                        if (WarehouseID != "")
                        {
                            // If the scanned warehouse code matches the row's warehouse code
                            if (WarehouseID == plantsWarehouseID)
                            {
                                // Select the row where the warehouse codes match
                                MTX_PLANTS.SelectRow(i, true, true);
                                WasAnythingSelected = true;

                                newIds.Add(plantID);
                            }
                        }
                        // No warehouse code was passed, so we are trying to identify an individual plant
                        else
                        {
                            // If the plant's ID matches the scanned barcode
                            if (TXT_LOOKUP.Value == plantID)
                            {
                                // Select the row where the plant ID's match
                                MTX_PLANTS.SelectRow(i, true, true);

                                WasAnythingSelected = true;
                                newIds.Add(plantID);

                                // Break out of the code since we are done finding our selection
                                break;
                            }

                            if (TXT_LOOKUP.Value == plantsStateID)
                            {
                                // Select the row where the warehouse codes match
                                MTX_PLANTS.SelectRow(i, true, true);
                                WasAnythingSelected = true;
                                newIds.Add(plantID);
                                break;
                            }
                        }
                    }

                    // Empty the barcode scanner field
                    TXT_LOOKUP.Value = "";

                    // Focus back into the barcode scanner's text field
                    TXT_LOOKUP.Active = true;

                    if (WasAnythingSelected)
                    {
                        // TODO: Select the warehouse's tab and all plants in the warehouse
                        if (this.CurrentlySelectedTab() == GetPhaseCount())
                        {
                            // Check to see if the Production Form is open. If it is get the reference.
                            foreach (Form form in Globals.oApp.Forms)
                            {
                                // Check to see if we have the Production form open.
                                if (form.UniqueID == Globals.SAP_PartnerCode + "_HARVEST_WIZ")
                                {

                                    F_HarvestWizard harvestWizardForm = new F_HarvestWizard();

                                    // Attempt to get any current plants being harvested.
                                    List<string> plantIdsToHarvest = harvestWizardForm.PlantsToBeHarvested();
                                    if (plantIdsToHarvest.Count == 0)
                                    {
                                        plantIdsToHarvest = new List<string>();
                                    }

                                    // Add our newly selected plants.
                                    if (newIds.Count > 0)
                                    {
                                        plantIdsToHarvest.AddRange(newIds);
                                    }

                                    harvestWizardForm.Form_Load(itemCode, plantIdsToHarvest.ToArray());
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void BTN_MOVE_ITEM_PRESSED()
        {
            try
            {
                // Prepare a new progress bar
                _progressBar = Globals.oApp.StatusBar.CreateProgressBar("Starting to transfer plants!", 10, false);

                this.UpdateProgressBar("Finding your selected plants.", 1);


                // Keep track of the selected Plant ID's
                List<string> SelectedSNs = new List<string>();

                // Keep track of the selected plants using a ValueTuple
                //ValueTuple<(string Plant, string WH, double Qty, string ManBy)> vtRec;
                //Type T = vtRec.GetType();
                //List<T> PlantList = new List;
                //List<(string Plant, string WH, double Qty, string ManBy)> PlantList = new List<(string Plant, string WH, double Qty, string ManBy)>();
                List<PlantType> PlantList = new List<PlantType>();

                // Grab the matrix from the form UI
                Matrix MTX_PLANTS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLANTS", _SBO_Form) as Matrix;

                // Find out which tab is selected
                Phase CurrentPhase = CurrentlySelectedPhase();

                DateTime StartTime = DateTime.Now;

                int NextRow = MTX_PLANTS.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);
                Double QtyTotal = 0D;
                while (NextRow != -1)
                {
                    // Add the selected row plant ID to list
                    var plantRec = new PlantType();

                    // Grab the selected row's Plant ID column
                    plantRec.Plant      = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.PlantID).Cells.Item(NextRow).Specific).Value;
                    plantRec.WH         = NSC_DI.SAP.Warehouse.GetCode(((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.WarehouseName).Cells.Item(NextRow).Specific).Value);
                    plantRec.Qty        = Convert.ToDouble(((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.Quantity).Cells.Item(NextRow).Specific).Value);
                    plantRec.ItemCode   = MTX_PLANTS.Columns.Item(MTX_PLANTS.Columns.Count-1).Cells.Item(NextRow).Specific.Value;   // the last column is the item code
                    QtyTotal            += plantRec.Qty;

                    try
                    {
                        plantRec.ManBy = NSC_DI.SAP.Items.ManagedBySB(plantRec.Plant);
                        //if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" &&
                        //     NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase.CurrentPhase == "FLO")) plantRec.ManBy = "S";

                        //vtRec = ValueTuple<(Plant: plantID, WH: warehouseCode, Qty: Qty, ManBy: manBy)>;
                        //vtRec = ((Plant: plantID, WH: warehouseCode, Qty: Qty, ManBy: manBy));
                        //PlantList.Add((Plant:plantID, WH:warehouseCode, Qty:Qty, ManBy:manBy));

                        PlantList.Add(plantRec);
                        SelectedSNs.Add(plantRec.Plant);
                    }
                    catch { }

                    NextRow = MTX_PLANTS.GetNextSelectedRow(NextRow, BoOrderType.ot_RowOrder);
                }

                this.UpdateProgressBar("Found your selected plants.", 1);

                // Make sure some plants were selected
                if (SelectedSNs.Count == 0)
                {
                    _progressBar.Stop();

                    Globals.oApp.StatusBar.SetText("Please select some plants first!");

                    return;
                }

                this.UpdateProgressBar("Validated your selected plants.", 1);

                if (CurrentPhase.NextPhase == null || CurrentPhase.NextPhase == "WET")
                {
                    try
                    {
                        _progressBar.Stop();
                    }
                    catch { }

                    (new F_HarvestWizard()).Form_Load(null,SelectedSNs.ToArray());
                    return;
                }

                // Keep track of which warehouse was selected
                string SelectedDestinationWarehouseID = "";
                string SelectedWarehouseDescription = "";

                // Make sure a warehouse was selected
                try
                {

                    // Grab the value from the ComboBox
                    if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y")
                        SelectedWarehouseDescription = _SBO_Form.Items.Item("txtWH_CFL").Specific.Value.ToString();
                    else
                        SelectedWarehouseDescription = _SBO_Form.Items.Item("CBX_WHSE").Specific.Value.ToString();


                    // Get the "description" from the ComboBox, where we are really storing the value
                    //SelectedDestinationWarehouseID = CommonUI.ComboBox.GetComboBoxValueOrDescription(pForm: ref _SBO_Form, pComboBoxUID: "CBX_WHSE", pComboBoxItemDescription: null, pComboBoxItemValue: SelectedWarehouseDescription);
                    SelectedDestinationWarehouseID = NSC_DI.SAP.Warehouse.GetCode(SelectedWarehouseDescription);

                }
                catch (Exception ex)
                {
                    // Stop the progress bar
                    _progressBar.Stop();

                    // Alert the user
                    Globals.oApp.StatusBar.SetText("Please select a destination warehouse first!");

                    return;
                }

                this.UpdateProgressBar("Discovered your selected destination warehouse.", 1);

                this.UpdateProgressBar("Determined destination plant state.", 1);

                this.UpdateProgressBar("Preparing to move plants to new warehouse.", 1);

                SAPbouiCOM.DataTable dtBinIn = _SBO_Form.DataSources.DataTables.Item("dtBins");
                NavSol.Forms.F_BinSelect.dtCreateIn(dtBinIn);

                // Foreach list of plants within a "From Warehouse"
                foreach (var rec in PlantList)
                {
                    var PlantID = rec.Plant;
                    var ItemCode = "";
                    var BinCode = "";

                    if (rec.ManBy == "S")
                    {
                        ItemCode = NSC_DI.SAP.Items.SNs.GetItemCode(PlantID);
                        BinCode  = NSC_DI.SAP.Bins.GetCodeFromSN(PlantID);
                    }

                    if (rec.ManBy == "B")
                    {
                        ItemCode = NSC_DI.SAP.Items.Batches.GetItemCode(PlantID);
                        BinCode  = NSC_DI.SAP.Bins.GetCodeFromBatch(PlantID);
                    }

                    //----------------------------------------
                    // populate the Bin datatable

                    dtBinIn.Rows.Add(1);

                    dtBinIn.SetValue("ItemCode",        dtBinIn.Rows.Count - 1, ItemCode);
                    dtBinIn.SetValue("Name",            dtBinIn.Rows.Count - 1, NSC_DI.SAP.Items.GetItemNameFromItemCode(ItemCode));
                    dtBinIn.SetValue("SrcWhCode",       dtBinIn.Rows.Count - 1, rec.WH);
                    dtBinIn.SetValue("From WH",         dtBinIn.Rows.Count - 1, NSC_DI.SAP.Warehouse.GetName(rec.WH));
                    dtBinIn.SetValue("DstWhCode",       dtBinIn.Rows.Count - 1, SelectedDestinationWarehouseID);
                    dtBinIn.SetValue("To WH",           dtBinIn.Rows.Count - 1, NSC_DI.SAP.Warehouse.GetName(SelectedDestinationWarehouseID));
                    dtBinIn.SetValue("Serial\\Batch",   dtBinIn.Rows.Count - 1, PlantID);
                    dtBinIn.SetValue("BinCode",         dtBinIn.Rows.Count - 1, BinCode);
                    dtBinIn.SetValue("LevelID1",        dtBinIn.Rows.Count - 1, NSC_DI.SAP.Bins.GetLevelCode(BinCode, 1));
                    dtBinIn.SetValue("LevelID2",        dtBinIn.Rows.Count - 1, NSC_DI.SAP.Bins.GetLevelCode(BinCode, 2));
                    dtBinIn.SetValue("LevelID3",        dtBinIn.Rows.Count - 1, NSC_DI.SAP.Bins.GetLevelCode(BinCode, 3));
                    dtBinIn.SetValue("LevelID4",        dtBinIn.Rows.Count - 1, NSC_DI.SAP.Bins.GetLevelCode(BinCode, 4));
                    dtBinIn.SetValue("XFer Qty",        dtBinIn.Rows.Count - 1, rec.Qty);
                    dtBinIn.SetValue("Bin Qty",         dtBinIn.Rows.Count - 1, rec.Qty);
                    dtBinIn.SetValue("Old Bin Qty",     dtBinIn.Rows.Count - 1, rec.Qty);
                    dtBinIn.SetValue("LinkRecNo",       dtBinIn.Rows.Count - 1, -1);
                    dtBinIn.SetValue("ManBySBN",        dtBinIn.Rows.Count - 1, rec.ManBy);
                }

                if (NSC_DI.SAP.Bins.IsManaged(SelectedDestinationWarehouseID))
                {
                    var pformID = _SBO_Form.UniqueID;
                    NavSol.Forms.F_BinSelect.FormCreate(_SBO_Form.UniqueID, dtBinIn, Convert.ToInt32(QtyTotal), dtBinIn.GetValue("SrcWhCode", dtBinIn.Rows.Count - 1), "", false);
                    F_BinSelect.BinsCB = delegate (string callingFormUid, System.Data.DataTable dtBins)
                    {
                        Form oForm = Globals.oApp.Forms.Item(pformID) as Form;

                        // Transfer plants
                        Process(oForm, SelectedDestinationWarehouseID, dtBins);
                    };
                    return;
                }
                else
                {
                    var dt = NavSol.Forms.F_BinSelect.dtCreateOut("");
                    for (var i = 0; i < dtBinIn.Rows.Count; i++)
                    {
                        var row = dt.NewRow();
                        row["Order"]         = i;
                        row["BinCode"]       = "";
                        row["BinAbs"]        = "";
                        row["Quantity"]      = dtBinIn.GetValue("XFer Qty", i);
                        row["ItemCode"]      = dtBinIn.GetValue("ItemCode", i);
                        row["Serial\\Batch"] = dtBinIn.GetValue("Serial\\Batch", i);
                        row["DestWH"]        = dtBinIn.GetValue("DstWhCode", i);
                        row["FromWH"]        = dtBinIn.GetValue("SrcWhCode", i);
                        row["FromBin"]       = dtBinIn.GetValue("BinCode", i);
                        row["ManBySBN"]      = dtBinIn.GetValue("ManBySBN", i);
                        dt.Rows.Add(row);
                    }
                    Process(_SBO_Form, SelectedDestinationWarehouseID, dt);
                }

                //if (((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG")) || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase.CurrentPhase == "FLO"))
                //{

                //    // Transfer Serialized plants
                //    NSC_DI.SAP.Plant.TransferPlants(
                //    ListOfSelectedPlants: new HashSet<string>(dictionaryOfWarehousesAndItems[rec]),
                //    NextPlantPhase: CurrentPhase.NextPhase,
                //    DestinationWarehouseCode: SelectedDestinationWarehouseID,
                //    pFromWarehouseName: rec);
                //}
                //else
                //{

                //    //Transfer Batch Managed plants
                //    NSC_DI.SAP.Plant.TransferPlantsBatch(
                //        Dictionary_Of_Plants_Batch_WarehouseAndQty,
                //        DestinationWarehouseCode: SelectedDestinationWarehouseID,
                //        NextPlantPhase: CurrentPhase.NextPhase
                //       );
                //}
                if (((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG")) || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase.CurrentPhase == "FLO"))
                {
                    //Serial - Handled above or flower

                }
                else
                {
                    //if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "N" || CurrentPhase.NextPhase != "FLO")
                    //{
                    //    //Transfer Batch Managed plants
                    //    NSC_DI.SAP.Plant.TransferPlantsBatch(
                    //    Dictionary_Of_Plants_Batch_WarehouseAndQty,
                    //    DestinationWarehouseCode: SelectedDestinationWarehouseID,
                    //    NextPlantPhase: CurrentPhase.NextPhase
                    //   );
                    //}
                    //else
                    //{
                    //    //Plant is batch managed, and needs to be serialized when moving to flower
                    //    //This is the flower stage(i.e., CA Batch Adjustment)
                    //    SerializeAndMovePlants(Dictionary_Of_Plants_Batch_WarehouseAndQty, SelectedDestinationWarehouseID);

                    //}

                }


                    // Reload the matrix
                    _progressBar.Stop();
                    //Load_Matrix(CurrentPhase.CurrentPhase);

                    // Notify user everything went successfully
                    Globals.oApp.StatusBar.SetText("Successfully moved " + SelectedSNs.Count + " plants to warehouse #" + SelectedDestinationWarehouseID, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                //}
                //catch (Exception ex)
                //{
                //    //Globals.oApp.StatusBar.SetText("Failed to move " + SelectedPlantIDs.Count + " plants to warehouse #" + SelectedDestinationWarehouseID);
                //    _progressBar.Stop();
                //    Globals.oApp.MessageBox("Failed to move " + SelectedSNs.Count + " plants to warehouse #" + SelectedDestinationWarehouseID + Environment.NewLine + NSC_DI.UTIL.Message.Format(ex));
                //}
                //try
                //{
                //    _progressBar.Stop();
                //}
                //catch { }

                //Load_TabCount();

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                try
                {
                    _progressBar.Stop();
                }
                catch { }
            }
        }

        private void Process(SAPbouiCOM.Form pForm, string pToWH, System.Data.DataTable dtBins = null)
        {
            try
            {
                _SBO_Form = pForm;

                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                NSC_DI.SAP.Plant.Transfer(dtBins);

                Phase CurrentPhase = CurrentlySelectedPhase();
                //if (NSC_DI.SAP.Phase.IsSerialize(CurrentPhase.CurrentPhase)
                //{
                //    var SNs = NSC_DI.UTIL.AutoStrain.NextSN(ItemCode, "Serial Plant", Qty);
                //    NSC_DI.SAP.ProductionOrder.Receive(prodOrder, cItemType, Qty, strBatchNum, SNs, "Clone", UserDefinedFields, null, dtBins);
                //}
                //if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "N"))
                //{
                //    strBatchNum = NSC_DI.SAP.BatchItems.NextBatch(ItemCode);
                //    NSC_DI.SAP.ProductionOrder.Receive(prodOrder, cItemType, Qty, strBatchNum, null, "Clone", UserDefinedFields, null, dtBins);
                //}


                //if (pForm.Items.Item("CBT_ACTION").Specific.Selected.Value == "RECEIPT_CLOSE")
                //{
                //    // Based off of the currently selected action
                //    //Change the status of the production order.
                //    NSC_DI.SAP.ProductionOrder.UpdateStatus(prodOrder, BoProductionOrderStatusEnum.boposClosed);
                //}

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                Load_Matrix(CurrentPhase.CurrentPhase);
                Load_TabCount();

                Globals.oApp.StatusBar.SetText("Successfully moved items.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (NSC_DI.SAP.B1Exception ex)
            {
                Globals.oApp.StatusBar.SetText(ex.Message);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                NSC_DI.UTIL.Misc.KillObject(pForm);
                GC.Collect();
            }
        }

        private void BTN_MOVE_ITEM_PRESSED_OLD()
        {
            try
            {
                // Prepare a new progress bar
                _progressBar = Globals.oApp.StatusBar.CreateProgressBar("Starting to transfer plants!", 10, false);

                this.UpdateProgressBar("Finding your selected plants.", 1);


                // Keep track of the selected Plant ID's
                List<string> SelectedPlantIDs = new List<string>();

                //For GenAg to track plant quantity
                var dicPlantWhrseQty = new Dictionary<string, KeyValuePair<string, double>>();

                // Keep track of the selected plants and their "from warehouse" (if transferring)
                Dictionary<string, string> Dictionary_Of_PlantsandWarehouses = new Dictionary<string, string>();

                //For Batch Managed Plants
                var Dictionary_Of_Plants_Batch_WarehouseAndQty = new Dictionary<string, Dictionary<string, double>>();

                // Grab the matrix from the form UI
                Matrix MTX_PLANTS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLANTS", _SBO_Form) as Matrix;

                // need to pass in the sn and itemcode
                //var harvestCnt = NavSol.CommonUI.Matrix.getSelected_MXRow(ref MTX_PLANTS);
                //string[,] harvestIDs = new string[harvestCnt, 2]; 
                //harvestCnt = 0;
                var itemCode = "";

                // Find out which tab is selected
                Phase CurrentPhase = CurrentlySelectedPhase();

                DateTime StartTime = DateTime.Now;
                var harvestName = "";    // all harvest names must be the same or blank

               int NextRow = MTX_PLANTS.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);
                if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase.CurrentPhase == "FLO"))
                {
                    while (NextRow != -1)
                    {
                        // Add the selected row plant ID to list

                        // Grab the selected row's Plant ID column
                        string plantID = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.PlantID).Cells.Item(NextRow).Specific).Value;
                        string warehouseCode = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.WarehouseName).Cells.Item(NextRow).Specific).Value;
                        
                        // all harvest names must be the same or blank
                        var rowHarvestName = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.HarvestName).Cells.Item(NextRow).Specific).Value;
                        if (harvestName == "") harvestName = rowHarvestName;
                        if (harvestName != "" && rowHarvestName != "" && harvestName != rowHarvestName)
                        {
                            Globals.oApp.MessageBox("All Harvest names must be the same or blank.");
                            return;
                        }
                            try
                        {
                            var test = MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.ItemCode).Cells.Item(NextRow).Specific.Value;
                            if (itemCode == "") itemCode = MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.ItemCode).Cells.Item(NextRow).Specific.Value;    // the last column is the item code
                            if (itemCode == MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.ItemCode).Cells.Item(NextRow).Specific.Value)                   // make sure all itemcodes are same
                            {
                                Dictionary_Of_PlantsandWarehouses.Add(plantID, warehouseCode);
                                SelectedPlantIDs.Add(plantID);
                            }
                        }
                        catch { }

                        NextRow = MTX_PLANTS.GetNextSelectedRow(NextRow, BoOrderType.ot_RowOrder);
                    }
                }
                else
                {

                    while (NextRow != -1)
                    {
                        // GenAg

                        // Grab the selected row's Plant ID column
                        string plantID = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.PlantID).Cells.Item(NextRow).Specific).Value;
                        string warehouseName = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.WarehouseName).Cells.Item(NextRow).Specific).Value;
                        string warehouseCode = NSC_DI.UTIL.SQL.GetValue<string>(@"Select T0.WhsCode from OWHS T0 where T0.WhsName ='" + warehouseName + "'");
                        double Qty = Convert.ToDouble(((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.Quantity).Cells.Item(NextRow).Specific).Value);
                        string lotNumber = ((EditText)MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.LotNumber).Cells.Item(NextRow).Specific).Value;

                        try
                        {
                            // Add the selected row plant ID to list
                            if (itemCode == "") itemCode = MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.ItemCode).Cells.Item(NextRow).Specific.Value;    // the last column is the item code
                            if (itemCode == MTX_PLANTS.Columns.Item(Matrix_Plants_Columns.ItemCode).Cells.Item(NextRow).Specific.Value)                   // make sure all itemcodes are same
                            {
                                Dictionary_Of_PlantsandWarehouses.Add(plantID, warehouseCode);
                                SelectedPlantIDs.Add(plantID);
                                dicPlantWhrseQty.Add(plantID, new KeyValuePair<string, double>(warehouseCode, Qty));
                                Dictionary<string, double> t = new Dictionary<string, double>();
                                t.Add(warehouseCode, Qty);
                                Dictionary_Of_Plants_Batch_WarehouseAndQty.Add(plantID, t);
                            }
                        }
                        catch { }

                        NextRow = MTX_PLANTS.GetNextSelectedRow(NextRow, BoOrderType.ot_RowOrder);
                    }
                }
                this.UpdateProgressBar("Found your selected plants.", 1);

                // Make sure some plants were selected
                if (SelectedPlantIDs.Count == 0)
                {
                    _progressBar.Stop();

                    Globals.oApp.StatusBar.SetText("Please select some plants first!");

                    return;
                }

                this.UpdateProgressBar("Validated your selected plants.", 1);

                #region If there is a "next phase" these plants could move into
                if (CurrentPhase.NextPhase != null && CurrentPhase.NextPhase != "WET")
                {
                    //// Keep track of which warehouse was selected
                    //string SelectedDestinationWarehouseID = "";

                    //// Make sure a warehouse was selected
                    //try
                    //{
                    //    // Find the ComboBox in the Form UI
                    //    ComboBox CBX_WHSE = ((ComboBox)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CBX_WHSE", _SBO_Form));

                    //    // Grab the value from the ComboBox
                    //    string SelectedWarehouseDescription = CBX_WHSE.Value.ToString();

                    //    // Get the "description" from the ComboBox, where we are really storing the value
                    //    SelectedDestinationWarehouseID = CommonUI.ComboBox.GetComboBoxValueOrDescription(pForm: ref _SBO_Form, pComboBoxUID: "CBX_WHSE", pComboBoxItemDescription: null, pComboBoxItemValue: SelectedWarehouseDescription);

                    //}
                    //catch (Exception ex)
                    //{
                    //    // Stop the progress bar
                    //    _progressBar.Stop();

                    //    // Alert the user
                    //    Globals.oApp.StatusBar.SetText("Please select a destination warehouse first!");

                    //    return;
                    //}

                    // Keep track of which warehouse was selected
                    string SelectedDestinationWarehouseID = "";
                    string SelectedWarehouseDescription = "";

                    // Make sure a warehouse was selected
                    try
                    {

                        // Grab the value from the ComboBox
                        if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y")
                            SelectedWarehouseDescription = _SBO_Form.Items.Item("txtWH_CFL").Specific.Value.ToString();
                        else
                            SelectedWarehouseDescription = _SBO_Form.Items.Item("CBX_WHSE").Specific.Value.ToString();


                        // Get the "description" from the ComboBox, where we are really storing the value
                        //SelectedDestinationWarehouseID = CommonUI.ComboBox.GetComboBoxValueOrDescription(pForm: ref _SBO_Form, pComboBoxUID: "CBX_WHSE", pComboBoxItemDescription: null, pComboBoxItemValue: SelectedWarehouseDescription);
                        SelectedDestinationWarehouseID = NSC_DI.SAP.Warehouse.GetCode(SelectedWarehouseDescription);

                    }
                    catch (Exception ex)
                    {
                        // Stop the progress bar
                        _progressBar.Stop();

                        // Alert the user
                        Globals.oApp.StatusBar.SetText("Please select a destination warehouse first!");

                        return;
                    }

                    this.UpdateProgressBar("Discovered your selected destination warehouse.", 1);

                    this.UpdateProgressBar("Determined destination plant state.", 1);

                    // Group stock transfers by the "From Warehouse" fields
                    Dictionary<string, List<string>> dictionaryOfWarehousesAndItems = Dictionary_Of_PlantsandWarehouses.GroupBy(t => t.Value).ToDictionary(t => t.Key, t => t.Select(r => r.Key).ToList());

                    this.UpdateProgressBar("Preparing to move plants to new warehouse.", 1);

                    // Foreach list of plants within a "From Warehouse"
                    try
                    {
                        bool serialize = (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y");
                        string serStage = NSC_DI.UTIL.Settings.Value.Get("Serialize Stage");

                        foreach (string fromwarehouse in dictionaryOfWarehousesAndItems.Keys)
                        {

                            //----------------------------------------
                            // populate the Bin datatable
                            SAPbouiCOM.DataTable dtBinIn = _SBO_Form.DataSources.DataTables.Item("dtBins");
                            NavSol.Forms.F_BinSelect.dtCreateIn(dtBinIn);

                            var ListSNs = new HashSet<string>(dictionaryOfWarehousesAndItems[fromwarehouse]);
                            foreach (string PlantID in ListSNs)
                            {
                                dtBinIn.Rows.Add(1);
                                var ItemCode = NSC_DI.SAP.Items.SNs.GetItemCode(PlantID);
                                var BinCode = NSC_DI.SAP.Bins.GetCodeFromSN(PlantID);
                                dtBinIn.SetValue("ItemCode", dtBinIn.Rows.Count - 1, ItemCode);
                                dtBinIn.SetValue("Name", dtBinIn.Rows.Count - 1, NSC_DI.SAP.Items.GetItemNameFromItemCode(ItemCode));
                                dtBinIn.SetValue("SrcWhCode", dtBinIn.Rows.Count - 1, NSC_DI.SAP.Warehouse.GetCode(fromwarehouse));
                                dtBinIn.SetValue("From WH", dtBinIn.Rows.Count - 1, fromwarehouse);
                                dtBinIn.SetValue("DstWhCode", dtBinIn.Rows.Count - 1, SelectedDestinationWarehouseID);
                                dtBinIn.SetValue("To WH", dtBinIn.Rows.Count - 1, NSC_DI.SAP.Warehouse.GetName(SelectedDestinationWarehouseID));
                                dtBinIn.SetValue("Serial\\Batch", dtBinIn.Rows.Count - 1, PlantID);
                                dtBinIn.SetValue("BinCode", dtBinIn.Rows.Count - 1, BinCode);
                                dtBinIn.SetValue("LevelID1", dtBinIn.Rows.Count - 1, NSC_DI.SAP.Bins.GetLevelCode(BinCode, 1));
                                dtBinIn.SetValue("LevelID2", dtBinIn.Rows.Count - 1, NSC_DI.SAP.Bins.GetLevelCode(BinCode, 2));
                                dtBinIn.SetValue("LevelID3", dtBinIn.Rows.Count - 1, NSC_DI.SAP.Bins.GetLevelCode(BinCode, 3));
                                dtBinIn.SetValue("LevelID4", dtBinIn.Rows.Count - 1, NSC_DI.SAP.Bins.GetLevelCode(BinCode, 4));
                                dtBinIn.SetValue("XFer Qty", dtBinIn.Rows.Count - 1, 1);
                                dtBinIn.SetValue("Bin Qty", dtBinIn.Rows.Count - 1, 1);
                                dtBinIn.SetValue("Old Bin Qty", dtBinIn.Rows.Count - 1, 1);
                                dtBinIn.SetValue("LinkRecNo", dtBinIn.Rows.Count - 1, -1);
                                dtBinIn.SetValue("ManBySBN", dtBinIn.Rows.Count - 1, "S");
                            }

                            if (NSC_DI.SAP.Bins.IsManaged(SelectedDestinationWarehouseID))
                            {
                                var pformID = _SBO_Form.UniqueID;
                                NavSol.Forms.F_BinSelect.FormCreate(_SBO_Form.UniqueID, dtBinIn, -1, dtBinIn.GetValue("SrcWhCode", dtBinIn.Rows.Count - 1), "", true);
                                F_BinSelect.BinsCB = delegate (string callingFormUid, System.Data.DataTable dtBins)
                                {
                                    Form pForm = null;
                                    //return;
                                    pForm = Globals.oApp.Forms.Item(pformID) as Form;

                                    // Transfer plants
                                    NSC_DI.SAP.Plant.TransferPlants(
                                        ListOfSelectedPlants: new HashSet<string>(dictionaryOfWarehousesAndItems[fromwarehouse]),
                                        NextPlantPhase: CurrentPhase.NextPhase,
                                        DestinationWarehouseCode: SelectedDestinationWarehouseID,
                                        pFromWarehouseName: fromwarehouse,
                                        pBins: dtBins);

                                    _SBO_Form = pForm;
                                    this.Load_Matrix(CurrentPhase.CurrentPhase);
                                    Globals.oApp.StatusBar.SetText("Successfully moved " + SelectedPlantIDs.Count + " plants to warehouse #" + SelectedDestinationWarehouseID, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                                };
                                return;
                            }

                            if (((serialize && serStage == "VEG")) || (serialize && serStage == "FLO" && CurrentPhase.CurrentPhase == "FLO"))
                            {
                                // Transfer Serialized plants
                                NSC_DI.SAP.Plant.TransferPlants(
                                ListOfSelectedPlants: new HashSet<string>(dictionaryOfWarehousesAndItems[fromwarehouse]),
                                NextPlantPhase: CurrentPhase.NextPhase,
                                DestinationWarehouseCode: SelectedDestinationWarehouseID,
                                pFromWarehouseName: fromwarehouse);
                                continue;
                            }

                            //if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "N")
                            //{
                            //    continue;
                            //}

                            if (serialize && CurrentPhase.NextPhase == serStage)
                            {
                                //Plant is batch managed, and needs to be serialized when moving to flower
                                //This is the flower stage(i.e., CA Batch Adjustment)
                                SerializeAndMovePlants(Dictionary_Of_Plants_Batch_WarehouseAndQty, SelectedDestinationWarehouseID);
                                continue;
                            }
                            
                            //Transfer Batch Managed plants
                            NSC_DI.SAP.Plant.TransferPlantsBatch(
                            Dictionary_Of_Plants_Batch_WarehouseAndQty,
                            DestinationWarehouseCode: SelectedDestinationWarehouseID,
                            NextPlantPhase: CurrentPhase.NextPhase
                           );
                        }
                        //    if (((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG")) || 
                        //         (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase.CurrentPhase == "FLO"))
                        //    {

                        //        // Transfer Serialized plants
                        //        NSC_DI.SAP.Plant.TransferPlants(
                        //        ListOfSelectedPlants: new HashSet<string>(dictionaryOfWarehousesAndItems[fromwarehouse]),
                        //        NextPlantPhase: CurrentPhase.NextPhase,
                        //        DestinationWarehouseCode: SelectedDestinationWarehouseID,
                        //        pFromWarehouseName: fromwarehouse);
                        //    }
                        //    else
                        //    {

                        //        //Transfer Batch Managed plants
                        //        NSC_DI.SAP.Plant.TransferPlantsBatch(
                        //            Dictionary_Of_Plants_Batch_WarehouseAndQty,
                        //            DestinationWarehouseCode: SelectedDestinationWarehouseID,
                        //            NextPlantPhase: CurrentPhase.NextPhase
                        //           );
                        //    }
                        //}

                        //  RH 19SEP2019 - COMMENTED OUT. BATCHES WERE EXECUTING TWICE.  NOT SURE IF NEEDED
                        //if (((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG")) || 
                        //     (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase.CurrentPhase == "FLO"))
                        //{
                        //    //Serial - Handled above or flower

                        //}
                        //else
                        //{
                        //    if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "N" || CurrentPhase.NextPhase != "FLO")
                        //    {
                        //        //Transfer Batch Managed plants
                        //        NSC_DI.SAP.Plant.TransferPlantsBatch(
                        //        Dictionary_Of_Plants_Batch_WarehouseAndQty,
                        //        DestinationWarehouseCode: SelectedDestinationWarehouseID,
                        //        NextPlantPhase: CurrentPhase.NextPhase
                        //       );
                        //    }
                        //    else
                        //    {
                        //        //Plant is batch managed, and needs to be serialized when moving to flower
                        //        //This is the flower stage(i.e., CA Batch Adjustment)
                        //        SerializeAndMovePlants(Dictionary_Of_Plants_Batch_WarehouseAndQty, SelectedDestinationWarehouseID);

                        //    }

                        //}


                        // Reload the matrix
                        _progressBar.Stop();
                        this.Load_Matrix(CurrentPhase.CurrentPhase);

                        // Notify user everything went successfully
                        Globals.oApp.StatusBar.SetText("Successfully moved " + SelectedPlantIDs.Count + " plants to warehouse #" + SelectedDestinationWarehouseID, BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                    }
                    catch (Exception ex)
                    {
                        //Globals.oApp.StatusBar.SetText("Failed to move " + SelectedPlantIDs.Count + " plants to warehouse #" + SelectedDestinationWarehouseID);
                        _progressBar.Stop();
                        Globals.oApp.MessageBox("Failed to move " + SelectedPlantIDs.Count + " plants to warehouse #" + SelectedDestinationWarehouseID + Environment.NewLine + NSC_DI.UTIL.Message.Format(ex));
                    }
                    try
                    {
                        _progressBar.Stop();
                    }
                    catch { }

                    Load_TabCount();
                }
                #endregion
                else
                {
                    try
                    {
                        _progressBar.Stop();
                    }
                    catch { }
                    
                    (new F_HarvestWizard()).Form_Load(itemCode, SelectedPlantIDs.ToArray(),false, dicPlantWhrseQty, harvestName);
                }

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                try
                {
                    _progressBar.Stop();
                }
                catch { }
            }
        }

        private void BTN_ALL_PRESSED()
        {
            try
            {
                Matrix MTX_PLANTS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLANTS", _SBO_Form) as Matrix;

                _SBO_Form.Freeze(true);

                for (int i = 1; i < (MTX_PLANTS.RowCount + 1); i++)
                {
                    MTX_PLANTS.SelectRow(i, true, true);
                }

                _SBO_Form.Freeze(false);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void SerializeAndMovePlants(Dictionary <string,Dictionary<string,double>> PlantLots, string DestinationWarehouseCode=null)
        {
            try
            {
                _SBO_Form.Freeze(true);


                //Collect and Validate Inputs for crop cycle
                string CropCy = NSC_DI.UTIL.SQL.GetValue<string>("Select ISNULL(T1.U_Value1,'N') as [U_Value1] from [@NSC_USER_OPTIONS] T1 where T1.Name='Crop Cycle Management'");
                string CropID = null;
                if (CropCy == "Y")
                {
                    string strSQL = "select Count(Distinct ISNULL(T0.U_NSC_CropID,1)) as [CROPS] from OBTN T0 where T0.DistNumber in (";
                    int counter = 0;
                    string strBatchHolder = null;
                    foreach (var kvp in PlantLots)
                    {
                        if (counter == 0)
                        {
                            strSQL += " '" + kvp.Key + "' ";
                            strBatchHolder = kvp.Key;
                        }
                        else
                        {
                            strSQL += ", '" + kvp.Key + "' ";
                        }
                        counter++;
                    }
                    strSQL += ")";

                    int CropID_Counter = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);
                    if(CropID_Counter>1)
                    {
                        Globals.oApp.MessageBox("Please select plants from the same Crop Cycle.");
                        return;
                    }

                    CropID = NSC_DI.UTIL.SQL.GetValue<string>(@"select T0.U_NSC_CropID from OBTN T0 where T0.DistNumber ='" + strBatchHolder + "'");

                }

                if (CropID == "0") CropID = null;

                //Create Production order and Issue to Production
                List<int> PrdOrdList = CreateIndividualProductionOrders(PlantLots, DestinationWarehouseCode, CropID);

                //Create Receipt from Production
                ReceivePlants(PrdOrdList, DestinationWarehouseCode, CropID);


            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {

                _SBO_Form.Freeze(false);

            }

        }

        private List<int> CreateIndividualProductionOrdersMother(Dictionary<string,string> PlantLots, string DestinationWarehouseCode = null)
        {
            List<int> listPdoKeys = new List<int>();

            try
            {
                //CloneTransactionData cloneTransDetails = PullCloneDetailsFromForm();
                //if (cloneTransDetails == null) return;

                bool bUpdatedProdO = false;

                //foreach (var PlantID in PlantLots.Keys)
                foreach(KeyValuePair<string, string> entry in PlantLots)
                {
                    var PlantID = entry.Key;
                    var WH = entry.Value;

                    var PlantBatchItemCode = NSC_DI.SAP.Items.Batches.GetItemCode(PlantID);

                    //Serialized Flower Item - 
                    //ToDo:Base this off of BOM or Properties or combination of both 
                    string ItemCode = "M-" + PlantBatchItemCode ;


                    //DestinationWarehouseCode = NSC_DI.UTIL.SQL.GetValue<string>(@"Select T0.DfltWH from [OITM] T0 where T0.ItemCode='"+ ItemCode + "'"); // 10824 5
                    DestinationWarehouseCode = DestinationWarehouseCode ?? WH;

                    //--------------------------------------------
                    // PRODUCTION ORDER
                    int PdOKey = NSC_DI.SAP.ProductionOrder.CreateFromBOM(ItemCode, 1, DateTime.Now, DestinationWarehouseCode, true, "N", "");  
                    if (PdOKey == 0)
                    {
                        Globals.oApp.MessageBox("Failed to create Production Order for batch " + PlantID + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                        continue;
                    }
                    else
                    {
                        //add to list
                        listPdoKeys.Add(PdOKey);
                    }

                    //Check the warehouse for the Plant(s) item(s) on tbe Production BOM and if needed adjust accordingly
                    //Get Plant Warehouses for selected items on the Dictionaty by batchnumber 
                    SAPbobsCOM.ProductionOrders oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
                    oPdO.GetByKey(PdOKey);
                    SAPbobsCOM.ProductionOrders_Lines oLines = oPdO.Lines;

                    //Update Destination Warehouse.
                    if (DestinationWarehouseCode != oPdO.Warehouse)
                    {
                        oPdO.Warehouse = DestinationWarehouseCode;
                        bUpdatedProdO = true;
                    }
                    //Update line Item warehouse
                    for (int i = 0; i <= oLines.Count - 1; i++)
                    {
                        oLines.SetCurrentLine(i);
                        if (PlantBatchItemCode == oLines.ItemNo)
                        {
                            string strWhsSourceHolder = "";
                            PlantLots.TryGetValue(PlantID,out strWhsSourceHolder);

                            if (strWhsSourceHolder != oLines.Warehouse && oLines.PlannedQuantity > 0)
                            {
                                //Update warehouse
                                oLines.Warehouse = strWhsSourceHolder;
                                bUpdatedProdO = true;
                            }
                        }
                    }
                    if (bUpdatedProdO == true)
                    {
                        oPdO.Update();
                    }


                    //--------------------------------------------
                    // ISSUE TO PRODUCTION ORDER
                    var PdOIssueKey = NSC_DI.SAP.ProductionOrder.Issue(PdOKey, "Cannabis Plant", 1, PlantID, null);
                    if (PdOIssueKey == 0)
                    {
                        Globals.oApp.MessageBox("Failed to Issue to Production for batch " + PlantID + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                        continue;
                    }
                }

                Globals.oApp.StatusBar.SetText("Complete.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
                // if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

            return listPdoKeys;

        }
        private List<IEnumerable<string>> ReceiveMotherPlants(List<int> pPrdOrdList)
        {
            List<IEnumerable<string>> SNs_List = new List<IEnumerable<string>>();
            #region Validate User Input
            if (pPrdOrdList.Count == 0)
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Failed to creat production order!");
                return SNs_List;
            }

            #endregion

            #region Get Information and Create Controllers


            // Grab the destination warehouse code from the Production Order
            //string DestinationWarehouseCode = pDestinationWarehouseCode;

            // Create a List to hold items
            //List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> listOfGoodsReceiptLines = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();

            #endregion

            //Create an array of strings to hold newly generated SN numbers for plants being created. 
            // Attempt to create a "Receipt from Production" document
            // SERIAL NUMBER
           
            try
            {
               

                foreach (int PrdOrd in pPrdOrdList)
                {
                    // Grab the warehouse code from the Matrix in the form UI
                    string SourceWarehouseCode = NSC_DI.UTIL.SQL.GetValue<string>(@"
            Select T0.WhsCode
 from[IGE1] T0
join[OITM] T1 on T0.ItemCode = T1.ItemCode
 join[OITB] T2 on T2.ItmsGrpCod = T1.ItmsGrpCod
 where T0.[BaseRef] = " + PrdOrd + @" and T2.ItmsGrpNam ='Cannabis Plant'");

                    // Grab the Item Code of the Plant
                    string ItemCodeOfPlant = NSC_DI.UTIL.SQL.GetValue<string>(@"Select T0.ItemCode from OWOR T0 where T0.DocNum = " + PrdOrd.ToString()); ;
                    string ItemCode = ItemCodeOfPlant;

                    string strSQL = @"Select T0.Quantity
from[IGE1] T0
join[OITM] T1 on T0.ItemCode = T1.ItemCode
 join[OITB] T2 on T2.ItmsGrpCod = T1.ItmsGrpCod
 where T0.[BaseRef] = " + PrdOrd + @" and T2.ItmsGrpNam ='Cannabis Plant'";//??12121

                    int Qty = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);
                    //Convert.ToInt16(QuantityToAdd);
                    SAPbouiCOM.Form pForm = null;
                    int prodOrder = PrdOrd;
                    //System.Data.DataTable dtBins = null;
                    pForm = Globals.oApp.Forms.Item(_SBO_Form.UniqueID) as Form;

                    SAPbouiCOM.DataTable dtBinsS = _SBO_Form.DataSources.DataTables.Item("dtBinSrc");    // NavSol.Forms.F_BinSelect.CreateDT();
                    NavSol.Forms.F_BinSelect.dtCreateIn(dtBinsS);
                    var j = 0;
                    var sql = $@"
SELECT DISTINCT 
       OITM.ItemCode  AS [ItemCode], OITM.ItemName AS [Name], 
	   WOR1.warehouse AS [SrcWhCode], (SELECT WhsName FROM OWHS WHERE WhsCode = WOR1.wareHouse) AS [From WH],
	   OWOR.Warehouse AS [DstWhCode], (SELECT WhsName FROM OWHS WHERE WhsCode = OWOR.Warehouse) AS [To WH],
	   '' AS [Serial Number], '' AS [BinCode], 
       '' AS [LevelID1], '' AS [LevelID2], '' AS [LevelID3], '' AS [LevelID4], 
	   0.0 AS [XFer Qty], {Convert.ToDecimal(Qty)}.00 AS [Bin Qty],
       WOR1.IssuedQty AS [Old Bin Qty], -1 AS [LinkRecNo]
  FROM OWOR
  JOIN OITM ON OITM.ItemCode  = OWOR.ItemCode
  JOIN WOR1 ON WOR1.DocEntry  = OWOR.DocNum
  JOIN OWHS ON WOR1.wareHouse = OWHS.WhsCode
 WHERE OWOR.DocNum = {prodOrder} 
   AND OITM.QryGroup39 = 'Y' AND OWHS.U_NSC_WhrsType='CLO' AND WOR1.IssuedQty > 0";

                    // skip the BIN mangement form if the destination warehouse is NOT bin managed
                    var wh = NSC_DI.UTIL.SQL.GetValue<string>(string.Format(@"Select warehouse from OWOR where DocNum = {0}", prodOrder), "");
                    //if (NSC_DI.SAP.Bins.IsManaged(wh))
                    //{
                    //    NavSol.Forms.F_BinSelect.FormCreate(_SBO_Form.UniqueID, sql, Qty, wh, "", true);
                    //    F_BinSelect.BinsCB = delegate (string callingFormUid, System.Data.DataTable dtBins)
                    //    {
                    //        Process(pForm, prodOrder, ItemCode, Qty, dtBins);
                    //    };
                    //}
                    //else Process(pForm, prodOrder, ItemCode, Qty, null);
                    IEnumerable<string> SN = Process(pForm, prodOrder, ItemCode, Qty, null,null,"MOTHER");
                    SNs_List.Add(SN);


                }

            }
            catch (NSC_DI.SAP.B1Exception ex)
            {
                Globals.oApp.StatusBar.SetText(ex.Message);
            }

            return SNs_List;
        }

        private List<int> CreateIndividualProductionOrders(Dictionary<string, Dictionary<string, double>> PlantLots, string DestinationWarehouseCode = null, string CropID=null)
        {
            List<int> listPdoKeys = new List<int>();

            try
            {
                //CloneTransactionData cloneTransDetails = PullCloneDetailsFromForm();
                //if (cloneTransDetails == null) return;

                var PdO_Process = "";
                Phase CurrentPhase = CurrentlySelectedPhase();
                if (CurrentPhase.CurrentPhase == "VEG" && CurrentPhase.NextPhase == "FLO") PdO_Process = "Phase_Change";

                bool bUpdatedProdO = false;

                foreach (var PlantID in PlantLots.Keys)
                {
                    var PlantBatchItemCode = NSC_DI.SAP.Items.Batches.GetItemCode(PlantID);

                    //Serialized Flower Item - 
                    //ToDo:Base this off of BOM or Properties or combination of both 
                    string ItemCode = "F-" + PlantBatchItemCode;

                    Dictionary<string, double> NewDic = new Dictionary<string, double>();
                    PlantLots.TryGetValue(PlantID, out NewDic);

                    //--------------------------------------------
                    // PRODUCTION ORDER
                    int PdOKey = NSC_DI.SAP.ProductionOrder.CreateFromBOM(
                        ItemCode, NewDic.OrderBy(kvp => kvp.Key).First().Value, DateTime.Now, DestinationWarehouseCode, true, pProcess: PdO_Process);
                    if (PdOKey == 0)
                    {
                        Globals.oApp.MessageBox("Failed to create Production Order for batch " + PlantID + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                        continue;
                    }
                    else
                    {
                        //add to list
                        listPdoKeys.Add(PdOKey);
                    }

                    //Check the warehouse for the Plant(s) item(s) on tbe Production BOM and if needed adjust accordingly
                    //Get Plant Warehouses for selected items on the Dictionaty by batchnumber 
                    SAPbobsCOM.ProductionOrders oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
                    oPdO.GetByKey(PdOKey);
                    SAPbobsCOM.ProductionOrders_Lines oLines = oPdO.Lines;
                    
                    Forms.Crop.CropCycleManagement oCrop = new Crop.CropCycleManagement();

                    string strCropFinHolder = null;
                    if (CropID!=null)
                    {
                        bUpdatedProdO = true;
                        strCropFinHolder = oCrop.GetFinProj(CropID);
                        oPdO.Project = strCropFinHolder;

                    }
                    //Update Destination Warehouse.
                    if (DestinationWarehouseCode != oPdO.Warehouse)
                    {
                        oPdO.Warehouse = DestinationWarehouseCode; 
                        bUpdatedProdO = true;
                    }

                    //Update line Item warehouse
                    for (int i = 0; i <= oLines.Count - 1; i++)
                    {
                        oLines.SetCurrentLine(i);
                        if (PlantBatchItemCode == oLines.ItemNo)
                        {
                            string strWhsSourceHolder = NewDic.OrderBy(kvp => kvp.Key).First().Key;

                            if (strWhsSourceHolder != oLines.Warehouse && oLines.PlannedQuantity > 0)
                            {
                                //Update warehouse
                                oLines.Warehouse = strWhsSourceHolder;
                                bUpdatedProdO = true;
                            }
                        }
                        if (CropID != null)
                        {
                            oLines.Project = strCropFinHolder;
                            bUpdatedProdO = true;
                        }
                    }

                    if (bUpdatedProdO == true)
                    {
                        oPdO.Update();
                    }


                    //--------------------------------------------
                    // ISSUE TO PRODUCTION ORDER
                    var PdOIssueKey = NSC_DI.SAP.ProductionOrder.Issue(PdOKey, "Cannabis Plant", NewDic.OrderBy(kvp => kvp.Key).First().Value, PlantID, null);
                    if (PdOIssueKey == 0)
                    {
                        Globals.oApp.MessageBox("Failed to Issue to Production for batch " + PlantID + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                        continue;
                    }

                    string strStageID = "FLOW_OTHER_" + CropID;
                    int intDocNum = NSC_DI.UTIL.SQL.GetValue<int>("Select DocNum from OWOR where DocEntry=" + PdOKey.ToString());
                    if (CropID!=null)
                    {
                        oCrop.AddProdToProject(PdOKey, intDocNum, CropID, strStageID);
                        oCrop.AddDocToProject_Single(PMDocumentTypeEnum.pmdt_GoodsIssue, PdOIssueKey.ToString(), CropID, strStageID);
                    }
                }

                Globals.oApp.StatusBar.SetText("Complete.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
            }
            catch (Exception ex)
            {
               // if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
           
            return listPdoKeys;
            
        }

        private void ReceivePlants(List<int> pPrdOrdList, string pDestinationWarehouseCode=null, string pCropID=null)
        {

            #region Validate User Input
            if (pPrdOrdList.Count == 0)
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Failed to creat production order!");
                return;
            }
            //// Grab the Matrix from the form 
            //Matrix matrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", _SBO_Form);

            //// Grab the textbox quantity
            //EditText TXT_QUANT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QUANT", _SBO_Form);

            //// Make sure the user entered a value for quantity
            //double QuantityToAdd;
            //if (TXT_QUANT.Value.ToString() == "" || !Double.TryParse(TXT_QUANT.Value.ToString(), out QuantityToAdd) || QuantityToAdd <= 0)
            //{
            //    // Send a message to the client
            //    Globals.oApp.StatusBar.SetText("Please enter a numerical quantity greater than 0!");
            //    return;
            //}

            //// Grab the ComboButton from the form UI
            //ButtonCombo CBT_ACTION = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ButtonCombo, "CBT_ACTION", _SBO_Form);

            //if (CBT_ACTION.Selected == null)
            //{
            //    // Send a message to the client
            //    Globals.oApp.StatusBar.SetText("Please select an action!");
            //    return;
            //}

            // Grab the Matrix from the form 
            //Matrix matrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", _SBO_Form);
            ////Check that the enteres amount is not greater than planned amount.
            //string strPlannedQty = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Planned");
            //if (!Double.TryParse(TXT_QUANT.Value.ToString(), out QuantityToAdd) || QuantityToAdd > Convert.ToDouble(strPlannedQty))
            //{
            //    // Send a message to the client
            //    Globals.oApp.StatusBar.SetText("Please enter a numerical quantity less than or equal to planned!");
            //    return;
            //}
            #endregion

            #region Get Information and Create Controllers


            // Grab the destination warehouse code from the Production Order
            string DestinationWarehouseCode = pDestinationWarehouseCode;

            // Create a List to hold items
            //List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> listOfGoodsReceiptLines = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();

            #endregion

            //Create an array of strings to hold newly generated SN numbers for plants being created. 
            // Attempt to create a "Receipt from Production" document
            // SERIAL NUMBER

            try
            {
                foreach (int PrdOrd in pPrdOrdList)
                {
                    // Grab the warehouse code from the Matrix in the form UI
                    string SourceWarehouseCode = NSC_DI.UTIL.SQL.GetValue<string>(@"
            Select T0.WhsCode
 from[IGE1] T0
join[OITM] T1 on T0.ItemCode = T1.ItemCode
 join[OITB] T2 on T2.ItmsGrpCod = T1.ItmsGrpCod
 where T0.[BaseRef] = "+ PrdOrd+ @" and T2.ItmsGrpNam ='Cannabis Plant'");

                    // Grab the Item Code of the Plant
                    string ItemCodeOfPlant = NSC_DI.UTIL.SQL.GetValue<string>(@"Select T0.ItemCode from OWOR T0 where T0.DocNum = " + PrdOrd.ToString()); ;
                    string ItemCode = ItemCodeOfPlant;

                    string strSQL = @"Select T0.Quantity
from[IGE1] T0
join[OITM] T1 on T0.ItemCode = T1.ItemCode
 join[OITB] T2 on T2.ItmsGrpCod = T1.ItmsGrpCod
 where T0.[BaseRef] = " + PrdOrd + @" and T2.ItmsGrpNam ='Cannabis Plant'";//??12121

                    int Qty = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);
                        //Convert.ToInt16(QuantityToAdd);
                    SAPbouiCOM.Form pForm = null;
                    int prodOrder = PrdOrd;
                    //System.Data.DataTable dtBins = null;
                    pForm = Globals.oApp.Forms.Item(_SBO_Form.UniqueID) as Form;

                    SAPbouiCOM.DataTable dtBinsS = _SBO_Form.DataSources.DataTables.Item("dtBinSrc");    // NavSol.Forms.F_BinSelect.CreateDT();
                    NavSol.Forms.F_BinSelect.dtCreateIn(dtBinsS);
                    var j = 0;
                    var sql = $@"
SELECT DISTINCT 
       OITM.ItemCode  AS [ItemCode], OITM.ItemName AS [Name], 
	   WOR1.warehouse AS [SrcWhCode], (SELECT WhsName FROM OWHS WHERE WhsCode = WOR1.wareHouse) AS [From WH],
	   OWOR.Warehouse AS [DstWhCode], (SELECT WhsName FROM OWHS WHERE WhsCode = OWOR.Warehouse) AS [To WH],
	   '' AS [Serial Number], '' AS [BinCode], 
       '' AS [LevelID1], '' AS [LevelID2], '' AS [LevelID3], '' AS [LevelID4], 
	   0.0 AS [XFer Qty], {Convert.ToDecimal(Qty)}.00 AS [Bin Qty],
       WOR1.IssuedQty AS [Old Bin Qty], -1 AS [LinkRecNo]
  FROM OWOR
  JOIN OITM ON OITM.ItemCode  = OWOR.ItemCode
  JOIN WOR1 ON WOR1.DocEntry  = OWOR.DocNum
  JOIN OWHS ON WOR1.wareHouse = OWHS.WhsCode
 WHERE OWOR.DocNum = {prodOrder} 
   AND OITM.QryGroup39 = 'Y' AND OWHS.U_NSC_WhrsType='CLO' AND WOR1.IssuedQty > 0";

                    // skip the BIN mangement form if the destination warehouse is NOT bin managed
                    var wh = NSC_DI.UTIL.SQL.GetValue<string>(string.Format(@"Select warehouse from OWOR where DocNum = {0}", prodOrder), "");
                    //if (NSC_DI.SAP.Bins.IsManaged(wh))
                    //{
                    //    NavSol.Forms.F_BinSelect.FormCreate(_SBO_Form.UniqueID, sql, Qty, wh, "", true);
                    //    F_BinSelect.BinsCB = delegate (string callingFormUid, System.Data.DataTable dtBins)
                    //    {
                    //        Process(pForm, prodOrder, ItemCode, Qty, dtBins);
                    //    };
                    //}
                    //else Process(pForm, prodOrder, ItemCode, Qty, null);
                    Process(pForm, prodOrder, ItemCode, Qty, null, pCropID);
                }
            }
            catch (NSC_DI.SAP.B1Exception ex)
            {
                Globals.oApp.StatusBar.SetText(ex.Message);
            }
        }
        private IEnumerable<string> Process(SAPbouiCOM.Form pForm, int prodOrder, string ItemCode, int Qty, System.Data.DataTable dtBins,string pCropID=null, string pMother="")
        {
            IEnumerable<string> SNs=null;
            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();


                //Get the Batch for the production order to create Byproducts if they exist
                string strSQL = string.Format(@"SELECT
    T2.[BatchNum],T4.U_NSC_MotherID
    FROM
    OWOR T0  INNER JOIN WOR1 T1 ON T0.DocEntry = T1.DocEntry left  join IBT1 T2 on T2.[BsDocEntry]  = t0.docentry and T2.[BsDocLine]  =  T1.[LineNum] 
    left  join IBT1 T3 on T3.[BsDocEntry]  = t0.docentry and T0.[ItemCode] = T3.[ItemCode]
    Join OBTN T4 on T4.DistNumber = T2.BatchNum 
    WHERE
    T2.[BsDocType]  = 202 and  (T0.[Status] = 'R' OR T0.[Status] = 'L' ) and T0.DocEntry={0}", prodOrder);


                SAPbobsCOM.Fields oRetunedValues = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(strSQL);
                //ToDo: Set user defined fields
                string strBatchNum = oRetunedValues.Item(0).Value;
                var UserDefinedFields = NSC_DI.SAP.BatchItems.GetInfo(strBatchNum);
                string strCropHolder = null;
                int intReceiveKey = 0;

                if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y")
                {
                    SNs = NSC_DI.UTIL.AutoStrain.NextSN(ItemCode, "Serial Plant", Qty);
                    if (pCropID!=null && pCropID.Length > 0)
                    {
                        strCropHolder = pCropID;
                        UserDefinedFields.UserFields.Item("U_NSC_CropStageID").Value = "FLOW_OTHER_" + pCropID;
                        UserDefinedFields.UserFields.Item("U_NSC_CropID").Value = pCropID;
                    }

                    intReceiveKey = NSC_DI.SAP.ProductionOrder.Receive(prodOrder, cItemType, Qty, strBatchNum, SNs, "Cannabis Plant", UserDefinedFields, null, dtBins,null,0,"", strCropHolder);
                }
               
                //Needed For General_Ag
                if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "N")
                {
                    if (pCropID != null && pCropID.Length > 0)
                    {
                        strCropHolder = pCropID;
                        UserDefinedFields.UserFields.Item("U_NSC_CropStageID").Value = "FLOW_OTHER_" + pCropID;
                        UserDefinedFields.UserFields.Item("U_NSC_CropID").Value = pCropID;
                    }

                    //If pMother ="MOTHER" then receiving mother for GenAg
                     if(pMother=="MOTHER")
                    {
                        //Receive GenAg Mother
                        SNs = NSC_DI.UTIL.AutoStrain.NextSN(ItemCode, "Serial Plant", Qty);
                        intReceiveKey = NSC_DI.SAP.ProductionOrder.Receive(prodOrder, cItemType, Qty, strBatchNum, SNs, "Cannabis Plant", UserDefinedFields, null, dtBins, null, 0, "", strCropHolder);

                    }
                    else
                    {
                        strBatchNum = NSC_DI.SAP.BatchItems.NextBatch(ItemCode);
                        intReceiveKey = NSC_DI.SAP.ProductionOrder.Receive(prodOrder, cItemType, Qty, strBatchNum, null, "Cannabis Plant", UserDefinedFields, null, dtBins,null,0,"", strCropHolder);

                    }

                }


                //Add Documents to the Crop Project 
                if (pCropID != null && pCropID.Length > 0)
                {
                    Forms.Crop.CropCycleManagement oCrop = new Forms.Crop.CropCycleManagement();
                    oCrop.AddDocToProject_Receipts(PMDocumentTypeEnum.pmdt_GoodsReceipt, intReceiveKey.ToString(), pCropID, "FLOW_OTHER_" + pCropID);
                    //Check for Backflush issue to production that needs to be set to the project. 
                    string strBackfluchProdOrd = null;
                    strBackfluchProdOrd = NSC_DI.UTIL.SQL.GetValue<string>(string.Format(@" Select Distinct ISNULL(T0.DocEntry,0) as DocEntry from IGE1 T0
                                                                                            inner join OITM T1 on T1.ItemCode = T0.ItemCode
                                                                                            where T0.BaseEntry = {0} and IssueMthd = 'B'", prodOrder.ToString()));
                    if (strBackfluchProdOrd !=null)
                    {
                        oCrop.AddDocToProject_Single(PMDocumentTypeEnum.pmdt_GoodsIssue, strBackfluchProdOrd, pCropID, "FLOW_OTHER_" + pCropID);

                    }
                }

                    //if (pForm.Items.Item("CBT_ACTION").Specific.Selected.Value == "RECEIPT_CLOSE")
                    //{
                    // Based off of the currently selected action
                    //Change the status of the production order.
                    NSC_DI.SAP.ProductionOrder.UpdateStatus(prodOrder, BoProductionOrderStatusEnum.boposClosed);
                //}

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                Globals.oApp.StatusBar.SetText("Successfully received item!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                BTN_FILTER_ITEM_PRESSED();

                // Set the main image
                //SAPbouiCOM.EditText oEdit = pForm.Items.Item("TXT_QUANT").Specific;
                //oEdit.Value = "";

            }
            catch (NSC_DI.SAP.B1Exception ex)
            {
                Globals.oApp.StatusBar.SetText(ex.Message);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                NSC_DI.UTIL.Misc.KillObject(pForm);
                GC.Collect();
            }
            return SNs;
        }
        #endregion

    }
}