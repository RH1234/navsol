﻿using B1WizardBase;
using SAPbouiCOM;
using System;
using System.Collections.Generic;

namespace NavSol.Forms.Production
{
    class F_WasteLog : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "NSC_WASTE_LOG";


        // FOR OLD CODE
        public VERSCI.Forms _VirSci_Helper_Form;
        public SAPbouiCOM.Application _SBO_Application;
        public SAPbobsCOM.Company _SBO_Company;
        public SAPbouiCOM.Form _SBO_Form;
        private SAPbouiCOM.ProgressBar oProgBar;

        #endregion VARS


        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
        [B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "MTX_MAIN":

                    MatrixMainPressed(pVal, oForm);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT


        public F_WasteLog(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            // this.FormUID = "VSC_WASTE_LOG";
            //  this.MenuText = "Waste Log";
            //this.SubMenu = SubMenus.MENU_PROD;
            //this.MenuPosition = 5;

            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

        public F_WasteLog() : this(Globals.oApp, Globals.oCompany) { }

        public void Form_Load()
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);
                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                // Load the matrix of water events
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm: _SBO_Form,
                    DatabaseTableName: "@" + NSC_DI.Globals.tWasteLog,
                    MatrixUID: "MTX_MAIN",
                    ListOfColumns: new List<CommonUI.Matrix.MatrixColumn>() {
                        new CommonUI.Matrix.MatrixColumn(){ Caption="ID", ColumnWidth=40, ColumnName="U_PlantID", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Taken On", ColumnWidth=80, ColumnName="U_DateCreated", ItemType = BoFormItemTypes.it_EDIT}               
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Amount", ColumnWidth=80, ColumnName="U_Amount", ItemType = BoFormItemTypes.it_EDIT}  
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Measurement", ColumnWidth=80, ColumnName="U_Measure", ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Reason", ColumnWidth=80, ColumnName="U_Reason", ItemType = BoFormItemTypes.it_EDIT}  
                        ,new CommonUI.Matrix.MatrixColumn(){ Caption="Details", ColumnWidth=80, ColumnName="U_Details", ItemType = BoFormItemTypes.it_EDIT}
                    }
                );

                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_MAIN", "bin-icon.bmp");

                // Grab the matrix from the form ui
                Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

                decimal total = 0;
                decimal total_today = 0;

                // TODO: Better calculate total amount for today and total waste

                //// For each row in the matrix
                //for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
                //{
                //    // Grab the measurement, amount and date from the matrix
                //    string measurement = ((SAPbouiCOM.EditText)MTX_MAIN.Columns.Item(3).Cells.Item(i).Specific).Value.ToString();
                //    decimal amount = Convert.ToDecimal(((SAPbouiCOM.EditText)MTX_MAIN.Columns.Item(2).Cells.Item(i).Specific).Value.ToString());
                //    string date_string = ((SAPbouiCOM.EditText)MTX_MAIN.Columns.Item(1).Cells.Item(i).Specific).Value.ToString();

                //    // Get the date parts from the SAP string
                //    string year = date_string.Substring(0, 4);
                //    string month = date_string.Substring(4, 2);
                //    string day = date_string.Substring(6, 2);

                //    decimal amount_to_add = 0;

                //    // Convert to milliliters from various liquid measurements
                //    switch (measurement)
                //    {
                //        case "grams":
                //            amount_to_add = amount;
                //            break;

                //        case "lbs":
                //            amount_to_add = (amount * 453.592m);
                //            break;

                //        case "oz":
                //            amount_to_add = (amount * 28.3495m);
                //            break;
                //    }

                //    // Add to total amount
                //    total += amount_to_add;

                //    // If the amount was added today, keep track
                //    if (Convert.ToDateTime(month + "/" + day + "/" + year).Date == DateTime.Now.Date)
                //    {
                //        total_today += amount_to_add;
                //    }
                //}

                // Set the textbox for the total amount of water used.
                //((SAPbouiCOM.EditText)Helpers.Forms.GetControlFromForm(Helpers.Forms.FormControlTypes.EditText, "TXT_TOTAL", _SBO_Form)).Value = (total / 453.592m).ToString("0.0") + " lbs";
                //((SAPbouiCOM.EditText)Helpers.Forms.GetControlFromForm(Helpers.Forms.FormControlTypes.EditText, "TXT_TODAY", _SBO_Form)).Value = (total_today / 453.592m).ToString("0.0") + " lbs";

                ((EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_TOTAL", _SBO_Form)).Item.Visible = false;
                ((EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_TODAY", _SBO_Form)).Item.Visible = false;

                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                // Show the form
                _SBO_Form.VisibleEx = true;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static void MatrixMainPressed(ItemEvent pVal, Form pForm)
        {
            try
            {
                int SelectedRowID = pVal.Row;

                if (SelectedRowID > 0)
                {
                    // Which column stores the ID
                    int ColumnIDForIDOfItemSelected = 0;

                    string ItemSelected = ((EditText)pForm.Items.Item("MTX_MAIN").Specific.Columns.Item(ColumnIDForIDOfItemSelected).Cells.Item(SelectedRowID).Specific).Value.ToString();

                    // Open the plant manager for the selected plant
                    F_PlantDetails.FormCreate(pPlantID: ItemSelected);
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }
    }
}