﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAPbobsCOM;
using SAPbouiCOM;
using Matrix = NavSol.CommonUI.Matrix;
using B1WizardBase;
using System.ComponentModel;

namespace NavSol.Forms.Production
{
    class F_HarvestWizard : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID   = "NSC_HARVEST_WIZ";
        private const string cItemType = "Wet Cannabis";
        private const string cProcess  = "HARVEST";

        // FOR OLD CODE
        public static VERSCI.Forms           _VirSci_Helper_Form;
        public static SAPbouiCOM.Application _SBO_Application;
        public static SAPbobsCOM.Company     _SBO_Company;
        public static SAPbouiCOM.Form        _SBO_Form;


        protected class HarvestComplaincy
        {
            public PlantHarvestData PlantHarvestInfo { get; set; }
            public List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> GoodsRecieptLines { get; set; }
        }

        protected class PlantHarvestData
        {
            public string plantId;
            public double wetWeight;
            public string fromWarehouseCode;
			public string SysNumber;
            public string plantSerialNumber;
            public string stateId;
            public string cropId;
            public string itemCode;

            public PlantHarvestData(string plantId, double wetWeight, string fromWarehouseCode, string SysNumber,string CropID=null, string itemCode="")
            {
                this.plantId = plantId;
                this.wetWeight = wetWeight;
                this.fromWarehouseCode = fromWarehouseCode;
				this.SysNumber = SysNumber;
                this.cropId = CropID;
                this.itemCode = itemCode;
            }
        }

        protected enum Matrix_Columns { UniqueID, ItemName, StateID, WhsCode, WetWeight, CreateDate, ItemCode, SysNumber, CropID, CropName, PlantCount };

        #endregion VARS

        // ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeValidate(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            BubbleEvent = Validate(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_KEY_DOWN
        [B1Listener(BoEventTypes.et_KEY_DOWN, false, new string[] { cFormID })]
        public virtual void OnAfterKeyDown(ItemEvent pVal)
        {
            //if (pVal.ActionSuccess == false || !m_FormLoaded) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			if (pVal.ItemUID == "TXT_TOTALG")
            {
                //text input in totals textbox
                //TXT_TOTAL_KEYEVENT();
            }
            // uses CMB_WHSE
            //if (pVal.ItemUID == "TXT_DWHSE")
            //{
            //    TXT_DWHSE_KEYEVENT(pVal);
            //}

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            //if (pVal.ActionSuccess == false || !m_FormLoaded) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

            switch (pVal.ItemUID)
            {
                // "Apply" button was clicked
                case "BTN_OK":
                    BTN_OK_Click();
                    break;
                case "CHK_EHRVST":
                    CheckBox CHK_EHRVST = _SBO_Form.Items.Item("CHK_EHRVST").Specific;
                    if(!CHK_EHRVST.Checked)
                    {
                        //Clear the total weight field                         
                        EditText TotalWeightTXT = _SBO_Form.Items.Item("TXT_TOTALG").Specific;
                        TotalWeightTXT.Value = "";
                        //TODO: Clear out the matrix values if any

                    }
                    CHK_EVENHARVEST(oForm);
                    break;

                case "BTN_LOOKUP":
                    BTN_LOOKUP_ITEM_PRESSED();
                    break;

                case "btnRSelct":
                    BTN_DELETE_ITEM_SELECTED();
                    break;

                case "btnRAll":
                    BTN_CLEAR_ALL();
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (ChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, false, new string[] { cFormID })]
        public virtual void OnAfterValidate(ItemEvent pVal)
        {

            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.InnerEvent == true) return;

            Validate(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_COMBO_SELECT
        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
        public virtual void OnAfterComboSelect(ItemEvent pVal)
        {

            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.InnerEvent == true) return;

            AfterComboSelect(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        #endregion AFTER EVENT


        // Constant for the Warehouse of type Wet. This should be changed to be more dynamic in the future.
        private const string WAREHOUSETYPE_COLUMN_NAME = "WET";

        public F_HarvestWizard(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

        public F_HarvestWizard() : this(Globals.oApp, Globals.oCompany) { }

       
        /// <summary>
        /// Cannot load the form without selected plants
        /// </summary>
         public void Form_Load()
         {
            
            try
            {

                var oForm = CommonUI.Forms.Load(cFormID);
                _SBO_Form = oForm;

                var formUID = _SBO_Form.UniqueID;

                // Freeze the Form UI

                _SBO_Form.Freeze(true);

                // Prepare the SQL statement that will load the selected plants data into the matrix
                string sql = "SELECT REPLICATE(' ', 36) AS UniqueID , REPLICATE(' ', 36) AS WhsCode," +
                    " REPLICATE(' ', 36) AS CreateDate ,REPLICATE(' ', 36) AS WetWeight, REPLICATE(' ', 36) AS CropID,REPLICATE(' ', 36) AS CropName, " +
                    "'    Begin Scanning To Populate Form   ' AS ItemName, REPLICATE(' ', 36) AS ItemCode, " + "REPLICATE(' ', 36) AS StateID" +
                    ", '                       ' AS SysNumber" +
                    ", '                       ' AS CropID" +
                    ", '                                                    ' AS CropName";

                // TODO: Dynamically add editable columns in the matrix, representing one each of the components of the cannabis - plant item group type in the auto item config CSV's.

                // Load data about selected plants into the matrix
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm: _SBO_Form,
                    DatabaseTableName: "OSRN",
                    MatrixUID: "MTX_PLAN",
                    ListOfColumns: new List<Matrix.MatrixColumn>() {
                    new Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT },
                    new Matrix.MatrixColumn(){ ColumnName="ItemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                    new Matrix.MatrixColumn(){ ColumnName="StateID", Caption="State ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT },
                    new Matrix.MatrixColumn(){ ColumnName="WhsCode", Caption="Warehouse", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                    new Matrix.MatrixColumn(){ ColumnName="WetWeight", Caption="Wet Weight (g)", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable = true  },
                    new Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                    new Matrix.MatrixColumn(){ ColumnName="ItemCode", Caption="ItemCode", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  }
                    ,new Matrix.MatrixColumn(){ ColumnName="SysNumber", Caption="SysNumber", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  }
                    ,new Matrix.MatrixColumn(){ ColumnName="CropID", Caption="Crop ID", ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                    ,new Matrix.MatrixColumn(){ ColumnName="CropName", Caption="Crop Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }

                    },
                    SQLQuery: sql
                );

        SAPbouiCOM.Matrix MTX_PLAN = _SBO_Form.Items.Item("MTX_PLAN").Specific;
                var wh = MTX_PLAN.GetCellSpecific(Matrix_Columns.WhsCode, 1).Value;

                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_Main", "harvest-icon.bmp");

                ComboBox CMB_WRHS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WRHS", _SBO_Form);
                if (CMB_WRHS == null)
                {
                    Globals.oApp.StatusBar.SetText("Warehouse combobox failed to load.");
                    return;
                }

                EditText TXT_HDATE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_HDATE", _SBO_Form);
                string DateHolder = DateTime.Now.ToString("yyyyMMdd");
                TXT_HDATE.Value = DateHolder;

                Load_HarvestActions();

                // Show the form
                _SBO_Form.VisibleEx = true;


                if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y" && Globals.BranchDflt == 0)  // if there are multiple branches, then do not use CFL
                {
                    NavSol.CommonUI.CFL.CreateWH(_SBO_Form, "CFL_WH", "CMB_WRHS");
                    var firstPhase = NSC_DI.SAP.Phase.GetPhases().First();
                    NavSol.CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_NONE, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, WAREHOUSETYPE_COLUMN_NAME);
                    NavSol.CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND, "Inactive", BoConditionOperation.co_EQUAL, "N");
                    NavSol.CommonUI.CFL.AddCon_Branches(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND);           // #10823
                }
                else
                    Load_Combobox_Warehouses(CMB_WRHS, wh);

                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                //set distributed harvest chkbox stuff
                CheckBox CHK_EHRVST = CommonUI.Forms.GetControlFromForm(
                        CommonUI.Forms.FormControlTypes.CheckBox,
                        "CHK_EHRVST", _SBO_Form
                    ) as CheckBox;

               
                CHK_EHRVST.Checked = false;
                _SBO_Form = Globals.oApp.Forms.Item(formUID);
                CHK_EVENHARVEST(oForm);

                if (NSC_DI.UTIL.Options.Value.GetSingle("Harvest Wizard Single Select Only") == "Y")
                {
                    MTX_PLAN.SelectionMode = BoMatrixSelect.ms_Single;
                }
                else
                {
                    MTX_PLAN.SelectionMode = BoMatrixSelect.ms_Auto;
                }

                EditText TXT_LOOKUP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", _SBO_Form
                );

                TXT_LOOKUP.Active = true;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                _SBO_Form.VisibleEx = true;
                _SBO_Form.Freeze(false);
            }
        }

        //public enum Matrix_Columns { UniqueID, WhsCode, CreateDate, WetWeight, ItemName, ItemCode }

        public List<string> PlantsToBeHarvested()
        {
            List<string> results = new List<string>();

            if (_SBO_Form == null)
            {
                // Set our private form field
                _SBO_Form = CommonUI.Forms.GetFormFromUID(cFormID);
            }

            // Grab the matrix of selected plants from the form UI
            SAPbouiCOM.Matrix MTX_PLAN = CommonUI.Forms.GetControlFromForm(
                CommonUI.Forms.FormControlTypes.Matrix,
                "MTX_PLAN", _SBO_Form);

            for (int i = 1; i < (MTX_PLAN.RowCount + 1); i++)
            {
                string plantID = ((EditText)MTX_PLAN.Columns.Item(Matrix_Columns.UniqueID).Cells.Item(i).Specific).Value;
                results.Add(plantID);
            }

            return results;
        }

        public void Form_Load(string pitemCode, string[] ListOfPlantIDsToLoad, bool loadFromScan = false, Dictionary<string, KeyValuePair<string, double>> pGenAgBatchesWhrsQty = null, string pHarvestName = "")
        {
            try
            {

                _SBO_Form = CommonUI.Forms.Load(cFormID);
                var uniqueID = _SBO_Form.UniqueID;
                // Freeze the Form UI
                _SBO_Form.Freeze(true);
                string sql = "";

                if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y")
                {
                    // Prepare the SQL statement that will load the selected serialized plants data into the matrix
                    sql = @"
SELECT DISTINCT
(CONVERT(nvarchar,[OSRN].[DistNumber])) AS [UniqueID]
,(SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) AS [WhsCode]
,[OSRN].[CreateDate]
,SPACE(50) as [WetWeight] --blarg sap datatables are strange
,[OSRN].[itemName] AS [ItemName]
, [OSRN].[MnfSerial] AS [StateID]
, [OSRN].[ItemCode] AS [ItemCode]
, [OSRN].[SysNumber] AS [SysNumber]
,OSRN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]
FROM [OSRN]
left Join OPMG on OPMG.AbsEntry = OSRN.U_NSC_CropID
WHERE
    ";
                //    (SELECT[OSRI].[WhsCode] FROM[OSRI] WHERE[OSRI].[ItemCode] = [OSRN].[ItemCode] AND[OSRI].[SysSerial] = [OSRN].[SysNumber]) IN
                //(
                //SELECT
                //[OWHS].[WhsCode]
                //FROM
                //[OWHS]
                //WHERE

                    foreach (string PlantID in ListOfPlantIDsToLoad)
                    {
                        sql += $" ([OSRN].[DistNumber] = '{PlantID}' AND OSRN.ItemCode = '{pitemCode}') OR";
                    }

                    // Remove the last "OR" in the SQL query.
                    sql = sql.Substring(0, sql.Length - 2);

                    //sql += " ) ";

                    // TODO: Dynamically add editable columns in the matrix, representing one each of the components of the cannabis - plant item group type in the auto item config CSV's.

                    // Load data about selected plants into the matrix
                    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm: _SBO_Form,
                        DatabaseTableName: "OSRN",
                        MatrixUID: "MTX_PLAN",
                        ListOfColumns: new List<Matrix.MatrixColumn>() {
                        new Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT },
                        new Matrix.MatrixColumn(){ ColumnName="ItemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="StateID", Caption="State ID", ColumnWidth=80, ItemType=BoFormItemTypes.it_EDIT },
                        new Matrix.MatrixColumn(){ ColumnName="WhsCode", Caption="Warehouse", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="WetWeight", Caption="Wet Weight (g)", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable = true  },
                        new Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="ItemCode", Caption="ItemCode", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="SysNumber", Caption="SysNumber", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="CropID", Caption="Crop ID", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new Matrix.MatrixColumn(){ ColumnName="CropName", Caption="Crop Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }

                        },
                        SQLQuery: sql
                    );
                }
                else
                {
                    //Prepare the SQL statement that will load the selected Batch Managed Plants data into the matrix - GenAg 

                    sql = @"  SELECT DISTINCT
(CONVERT(nvarchar,[OBTN].DistNumber)) AS[UniqueID]
--,(SELECT[OIBT].[WhsCode] FROM[OIBT] WHERE[OIBT].[ItemCode] = [OBTN].[ItemCode] AND[OIBT].SysNumber = [OBTN].[SysNumber]) AS[WhsCode]
,[OIBT].WhsCode
,[OBTN].[CreateDate]
,[OBTN].[ItemName], [OBTN].[MnfSerial] AS [StateID], OBTN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]
,SPACE(50) as [WetWeight] --blarg sap datatables are strange
,[OBTN].ItemCode
,[OBTN].SysNumber
,'                                      ' as [PlantCount] --blarg to hold count of plants
FROM[OBTN]
left Join OPMG on OPMG.AbsEntry = OBTN.U_NSC_CropID
join OIBT on OIBT.BatchNum = OBTN.DistNumber and OIBT.ItemCode = OBTN.ItemCode
inner join OWHS on OIBT.WhsCode = OWHS.WhsCode 
inner join [@NSC_PDO_PHASES] T0 on T0.U_CurrentPhase = OWHS.U_NSC_WhrsType 
WHERE
(
SELECT[OIBT].[WhsCode] FROM[OIBT] WHERE [OIBT].[ItemCode] = [OBTN].ItemCode AND[OIBT].BatchNum = [OBTN].DistNumber and [OIBT].Quantity>0 ) IN
(
SELECT
[OWHS].[WhsCode]
FROM
[OWHS]
Where ";
                    foreach (string PlantID in pGenAgBatchesWhrsQty.Keys)
                    {
                        sql += "(CONVERT(nvarchar,[OBTN].DistNumber)) = '" + PlantID + "' OR";
                    }

                    // Remove the last "OR" in the SQL query and closes the subquery.
                    sql = sql.Substring(0, sql.Length - 2) + ")  and [OIBT].Quantity>0 and [OIBT].WhsCode!='9999'";

                    // TODO: Dynamically add editable columns in the matrix, representing one each of the components of the cannabis - plant item group type in the auto item config CSV's.

                    // Load data about selected plants into the matrix
                    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm: _SBO_Form,
                        DatabaseTableName: "OBTN",
                        MatrixUID: "MTX_PLAN",
                        ListOfColumns: new List<Matrix.MatrixColumn>() {
                        new Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT },
                        new Matrix.MatrixColumn(){ ColumnName="ItemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="StateID", Caption="State ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT },
                        new Matrix.MatrixColumn(){ ColumnName="WhsCode", Caption="Warehouse", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="WetWeight", Caption="Wet Weight (g)", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable = true  },
                        new Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="ItemCode", Caption="ItemCode", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="SysNumber", Caption="SysNumber", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                        new Matrix.MatrixColumn(){ ColumnName="CropID", Caption="Crop ID", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new Matrix.MatrixColumn(){ ColumnName="CropName", Caption="Crop Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new Matrix.MatrixColumn(){ ColumnName="PlantCount", Caption="Plant Count", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT, IsEditable = false  }


                        },
                        SQLQuery: sql
                    );
                }

                //Add Plant count values to MX
                SAPbouiCOM.Matrix oMX = _SBO_Form.Items.Item("MTX_PLAN").Specific;
                SAPbouiCOM.EditText oEdit = null;
                int rowCounter = 1;
                KeyValuePair<string,double> Holder;

                foreach (var PlantID in pGenAgBatchesWhrsQty)
                {
                    oEdit = oMX.GetCellSpecific(Matrix_Columns.PlantCount, rowCounter);
                    Holder = PlantID.Value;
                    oEdit.Value = Holder.Value.ToString();
                    rowCounter += 1;
                }

                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_Main", "harvest-icon.bmp");

                ComboBox CMB_WRHS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WRHS", _SBO_Form);

                if (CMB_WRHS == null)
                {
                    Globals.oApp.StatusBar.SetText("Warehouse combobox failed to load.");
                    return;
                }

                EditText TXT_HDATE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_HDATE", _SBO_Form);
                string DateHolder = DateTime.Now.ToString("yyyyMMdd");
                TXT_HDATE.Value = DateHolder;

                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                // Show the form
                _SBO_Form.VisibleEx = true;

                Load_HarvestActions();

                var wh = oMX.GetCellSpecific(Matrix_Columns.WhsCode, 1).Value;
                Load_Combobox_Warehouses(CMB_WRHS, wh);

                //set distributed harvest chkbox stuff

                SAPbouiCOM.CheckBox CHK_EHRVST = _SBO_Form.Items.Item("CHK_EHRVST").Specific;

                SAPbouiCOM.EditText TotalWeightTXT = _SBO_Form.Items.Item("TXT_TOTALG").Specific;
               
                if (!loadFromScan)
                {
                    //force distributed harvesting for I502
                    CHK_EHRVST.Checked = false;
                    //CHK_EHRVST.Item.Enabled = false;
                    TotalWeightTXT.Item.Enabled = true; //ct switched these to force group harvest
                    //TotalWeightTXT.Active = true; //ct switched these to force group harvest
                   

                }
                _SBO_Form = Globals.oApp.Forms.Item(uniqueID);
                _SBO_Form.Items.Item("TXTHNAME").Specific.Value = pHarvestName;
                
                _SBO_Form.ActiveItem = "TXT_STID";
                if (pHarvestName != "") _SBO_Form.Items.Item("TXTHNAME").Enabled = false;

                SAPbouiCOM.Matrix MTX_PLAN = _SBO_Form.Items.Item("MTX_PLAN").Specific;

                if (NSC_DI.UTIL.Options.Value.GetSingle("Harvest Wizard Single Select Only") == "Y")
                {
                    MTX_PLAN.SelectionMode = BoMatrixSelect.ms_Single;
                }
                else
                {
                    MTX_PLAN.SelectionMode = BoMatrixSelect.ms_Auto;
                }

                if(!ForceSameItemCode())
                {
                    Globals.oApp.SetStatusBarMessage("Item codes must all match for harvesting. We match on the first row item code.", BoMessageTime.bmt_Short);
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                _SBO_Form.Freeze(false);
            }
        }

        private static bool Validate(Form pForm, ItemEvent pVal)
        {
            try
            {
                // ----------------------------------------------
                // HARVEST NAME
                if (pVal.ItemUID == "TXTHNAME")
                {
                    // skip if add-on is setting the value
                    if (pVal.InnerEvent) return true;

                    //  if this is the after event, disable the field.
                    //if (pVal.Before_Action == false)
                    //{
                    //    pForm.Items.Item("TXT_STID").Specific.Active = false;
                    //    pForm.Items.Item(pVal.ItemUID).Enabled = false;
                    //    return true;
                    //}

                    // if field is disabled, then the name was passed in, so all is good
                    if (pForm.Items.Item(pVal.ItemUID).Enabled == false) return true;

                    // see if name aready exists
                    var hName = pForm.Items.Item(pVal.ItemUID).Specific.Value;

                    // for the serial/batch tables - decided to use the Harvest table
                    //hName = NSC_DI.UTIL.Strings.Search(hName);    // remove all special characters
//                    var sql = $@"SELECT 
//(SELECT COUNT(U_NSC_HarvestName) FROM OSRN WHERE U_NSC_HarvestName = '{hName}' COLLATE SQL_Latin1_General_CP1_CI_AS) +
//(SELECT COUNT(U_NSC_HarvestName) FROM OBTN WHERE U_NSC_HarvestName = '{hName}' COLLATE SQL_Latin1_General_CP1_CI_AS)";

                    var sql = NSC_DI.UTIL.Strings.SearchSQLtHarvest(hName); 
                    if (NSC_DI.UTIL.SQL.GetValue<bool>(sql, false) == false) return true;

                    Globals.oApp.StatusBar.SetText("That Harvest Name is in use. Enter another.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return false;
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }

        private static void AfterComboSelect(Form pForm, ItemEvent pVal)
        {
            try
            {
                if (pForm.Items.Item(pVal.ItemUID).Specific.Value.Trim() == "") return;

                if (pVal.ItemUID == "cboHType")
                {
                    if (pForm.Items.Item(pVal.ItemUID).Specific.Value == "Full")
                    {
                        pForm.Items.Item("TXTHNAME").Enabled = true;
                        return;
                    }

                    if (pForm.Items.Item("TXTHNAME").Specific.Value.Trim() != "") pForm.Items.Item("TXTHNAME").Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }

        #region Custom Choose From Lists


        private void HandleDryWarehouseSelectionCFL()
        {
            // Not used currently

            try
            {
                var sql = @"SELECT [Code], [Name] FROM [OWHS] WHERE [U_NSC_WhrsType] = 'DRY'";

                var matrixColumns = new List<Matrix.MatrixColumn>()
            {
                 new Matrix.MatrixColumn(){ Caption="Code", ColumnWidth=80, ColumnName="Code", ItemType = BoFormItemTypes.it_EDIT}
                ,new Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=40, ColumnName="Name", ItemType= BoFormItemTypes.it_EDIT}
            };
                int cflWidth = Math.Max(450, matrixColumns.Count * 150);
                int cflHeight = 420;
                var width = Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
                var height = Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);
                var top = (Globals.oApp.Desktop.Height - height) / 2;
                var left = (Globals.oApp.Desktop.Width - width) / 2;
                F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pDt)
                {
                    _SBO_Form = B1Connections.theAppl.Forms.Item(pCallingFormUid);

                    DryWarehouseChooseFromListCallBack(pDt);
                };
                Form Form_CFL = F_CFL.FormCreate(
                    pTitle: "Drying Room Location",
                    pCallingFormUID: _SBO_Form.UniqueID,
                    pSQL: sql,
                    pMatrixColumns: matrixColumns,
                    pWidth: width,
                    pHeight: height,
                    pTop: top,
                    pLeft: left
                    );
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public void DryWarehouseChooseFromListCallBack(System.Data.DataTable pResults)
        {
            try
            {
                // Select our current form.
                _SBO_Form.Select();

                _SBO_Form.Items.Item("TXT_WHSC").Specific.Value = pResults.Rows[0]["Code"];
                _SBO_Form.Items.Item("TXT_DWHSE").Specific.Value = pResults.Rows[0]["Name"];
            }
            catch (Exception ex)
            {
                // What are you complaining about?
            }
        }

        #endregion

        private void Load_Combobox_Warehouses(ComboBox CMBControl, string pWHCode = "")
        {
            SAPbobsCOM.Recordset oRS = null;

            try
            {
                // If items already exist in the drop down
                if (CMBControl.ValidValues.Count > 0)
                {
                    // Remove all currently existing values from warehouse drop down
                    for (int i = CMBControl.ValidValues.Count; i-- > 0;)
                    {
                        CMBControl.ValidValues.Remove(i, BoSearchKey.psk_Index);
                    }
                }

                // validate the Branch
                var branch = BranchValidate(pWHCode);

                // Prepare to run a SQL statement.
                oRS = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                // Currently Hard Coded to use Warehouse Type of WTR. Might be pulled out at sometime.
                var sql = $@"
SELECT [WhsCode], [WhsName]
  FROM [OWHS]
 WHERE [OWHS].U_NSC_WhrsType = '" + WAREHOUSETYPE_COLUMN_NAME + "' AND [OWHS].Inactive = 'N'";
                if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({branch})";     //10823

                oRS.DoQuery(sql);


                // If more than 1 warehouses exists
                if (oRS.RecordCount > 1)
                {
                    // Create the first item as an empty item
                    CMBControl.ValidValues.Add("", "");

                    // Select the empty item (forcing the user to make a decision)
                    CMBControl.Select(0, BoSearchKey.psk_Index);
                }

                // Add allowed warehouses to the drop down
                for (int i = 0; i < oRS.RecordCount; i++)
                {
                    try
                    {
                        CMBControl.ValidValues.Add(oRS.Fields.Item(1).Value.ToString(), oRS.Fields.Item(0).Value.ToString());
                    }
                    catch { }
                    CMBControl.Item.Enabled = true;
                    oRS.MoveNext();
                }

                // If only one option exists. Set it to the option and disable the control.
                if (oRS.RecordCount == 1)
                {
                    CMBControl.Select(0, BoSearchKey.psk_Index);
                    CMBControl.Active = false;
                    CMBControl.Item.Enabled = false;
                }
            } catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oRS);
                GC.Collect();
            }
        }

        private void Load_HarvestActions()
        {
            //ButtonCombo CBT_ACTION = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.ButtonCombo, "CBT_ACTION", _SBO_Form);  // removed this control
            ComboBox CBT_ACTION = _SBO_Form.Items.Item("cboHType").Specific;

            // If items already exist in the drop down
            if (CBT_ACTION.ValidValues.Count > 0)
            {
                // Remove all currently existing values from warehouse drop down
                for (int i = CBT_ACTION.ValidValues.Count; i-- > 0; )
                {
                    CBT_ACTION.ValidValues.Remove(i, BoSearchKey.psk_Index);
                }
            }

            // Populating our options for Harvesting
            CBT_ACTION.ValidValues.Add("", "");
            CBT_ACTION.ValidValues.Add("Full", "Full Harvest");
            CBT_ACTION.ValidValues.Add("Partial", "Partial Harvest");

            CBT_ACTION.Select(0, BoSearchKey.psk_Index);
        }

        /// <summary>
        /// Disable wet weight column and clear it out, enable textbox for number input
        /// OR
        /// Enable wet weight column and disable textbox
        /// </summary>
        private void CHK_EVENHARVEST(Form pForm)
        {
	        try
	        {

                //FIXME: for now assume "disable" column mode

                CheckBox CHK_EHRVST = pForm.Items.Item("CHK_EHRVST").Specific;

		        SAPbouiCOM.Matrix MTX_PLAN = pForm.Items.Item("MTX_PLAN").Specific;

		        EditText TotalWeightTXT = pForm.Items.Item("TXT_TOTALG").Specific;

		        Column WetWeightCol = MTX_PLAN.Columns.Item(Matrix_Columns.WetWeight);

		        if (CHK_EHRVST.Checked)
		        {
			        if (ForceSameItemCode())
			        {
                        // checks to see if the user has entered a value into the "Distributed Harvest Totals" field
                        pForm.Freeze(true);
                        if (TotalWeightTXT.Value == "")
                        {
                            // unchecks the box and sets DHT to the active field
                            CHK_EHRVST.Checked = false;
                            TotalWeightTXT.Active = true;
                        }                            
                        else
                        {
                            // removes focus from DHT and disables it. makes the weight fields in the matrix columns uneditable
                            TotalWeightTXT.Active = false;
                            TotalWeightTXT.Item.Enabled = false;
                            TXT_TOTAL_KEYEVENT();
                            WetWeightCol.Editable = false;
                        }
                        //_SBO_Form.Freeze(false);
			        }
			        else
			        {
				        WetWeightCol.Editable = true;
				        TotalWeightTXT.Active = false;
				        //TotalWeightTXT.Item.Enabled = true;

				        CHK_EHRVST.Checked = false;

				        Globals.oApp.SetStatusBarMessage("You can only harvest 1 strain at a time", //must be same itemcode
					        BoMessageTime.bmt_Short);
			        }
		        }
		        else
		        {
			        //unchecked, meaning individual harvest, enable column, disable txt
			        WetWeightCol.Editable = true;
			        TotalWeightTXT.Active = false;
			        TotalWeightTXT.Item.Enabled = true;
		        }
	        }
	        catch (Exception ex)
	        {
		        Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
	        }
            finally
            {
                pForm.Freeze(false);
            }
        }

        public bool ForceSameItemCode()
        {
            try
            {
                bool wasSuccessful = true;

                // Grab the matrix of selected plants from the form UI
                SAPbouiCOM.Matrix MTX_PLAN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLAN", _SBO_Form);

                string firstItemCodeToMatch = string.Empty;
                if (MTX_PLAN.RowCount > 0)
                {
                    //firstItemCodeToMatch = ((EditText)MTX_PLAN.Columns.Item(Matrix_Columns.ItemCode).Cells.Item(1).Specific).Value;
                    firstItemCodeToMatch = (MTX_PLAN.GetCellSpecific(Matrix_Columns.ItemCode, 1) as EditText).Value;
                }

                // Decrement counter so we can properly delete the matrix items as needed.
                for (int i = MTX_PLAN.RowCount; i > 1; i--)
                {
                    // string itemCode = ((EditText)MTX_PLAN.Columns.Item(Matrix_Columns.ItemCode).Cells.Item(i).Specific).Value;
                    var itemCode = (MTX_PLAN.GetCellSpecific(Matrix_Columns.ItemCode, i) as EditText).Value;

                    if (!itemCode.Equals(firstItemCodeToMatch))
                    {
                        MTX_PLAN.DeleteRow(i);
                        wasSuccessful = false;
                    }
                }

                return wasSuccessful;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        /// <summary>
        /// Make sure that TXT_totalg is numbers only. update with this number the distributed weight into all columns
        /// </summary>
        /// <param name="pVal"></param>
        private void TXT_TOTAL_KEYEVENT()
        {
            try
            {
                SAPbouiCOM.Matrix MTX_PLAN = CommonUI.Forms.GetControlFromForm(
                             CommonUI.Forms.FormControlTypes.Matrix,
                            "MTX_PLAN", _SBO_Form
                        ) as SAPbouiCOM.Matrix;

                EditText TotalWeightTXT = CommonUI.Forms.GetControlFromForm(
                             CommonUI.Forms.FormControlTypes.EditText,
                            "TXT_TOTALG", _SBO_Form
                        ) as EditText;
                //sanity check to aviod red-bars when un-needed:
                if (string.IsNullOrEmpty(TotalWeightTXT.Value))
                {
                    return;
                }

                Column WetWeightCol = MTX_PLAN.Columns.Item(Matrix_Columns.WetWeight);
  

                if (!decimal.TryParse(TotalWeightTXT.Value, out decimal total_mass))
                {
                    Globals.oApp.StatusBar.SetText("Harvest Total must be a decimal number in grams!");

                    TotalWeightTXT.Value = "";
                    return;
                }
                //value is "good" spread the joy to all the rows!
                var split_mass = total_mass / WetWeightCol.Cells.Count;


                WetWeightCol.Editable = true;
                _SBO_Form.Freeze(true);

                DataTable dataTable = _SBO_Form.DataSources.DataTables.Item((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y")?"OSRN": "OBTN");
                MTX_PLAN.FlushToDataSource();

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    string val = split_mass.ToString("F");
                    dataTable.SetValue("WetWeight", i, val);
                }

                MTX_PLAN.LoadFromDataSource();

                _SBO_Form.Freeze(false);
                WetWeightCol.Editable = false;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                _SBO_Form.Freeze(false);
            }
        }

        private void BTN_OK_Click()
        {
            // Prepare a new progress bar
            ProgressBar oBar = null;
            Recordset   oRS  = null;
           
            try
            {
                oBar = Globals.oApp.StatusBar.CreateProgressBar("Starting to transfer plants!", 13, false);
                var harvestName = _SBO_Form.Items.Item("TXTHNAME").Specific;
                if (harvestName.Value == "")
                {
                    if (oBar != null) oBar.Stop();
                    Globals.oApp.StatusBar.SetText("Harvest Name is Required to proceed");
                    //harvestName.Item.Enabled = true;
                    harvestName.Active = true;
                    return;
                }
                oBar.Value++;

                SAPbouiCOM.EditText oHDATE = _SBO_Form.Items.Item("TXT_HDATE").Specific;

                var harvestSearch = NSC_DI.UTIL.Strings.Search(harvestName.Value);
                var sql = "";

                // Keep track of the selected Plant ID's
                List<PlantHarvestData> SelectedPlantIDs = new List<PlantHarvestData>();

                //GEnAg Counter
                int intPlantCOunt = 0;

                // Keep track if any of the plants fail
                bool AllPlantsPassed = true;

                #region Validate the form is filled out properly.. and some Control references.

                CheckBox CHK_EHRVST = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_EHRVST", _SBO_Form);

                EditText txtWarehouseCode = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WHSC", _SBO_Form);

                string groupName = string.Empty;
                Crop.CropCycleManagement oCrop = new Crop.CropCycleManagement();
                string strCropHolder = null;


                if (CHK_EHRVST.Checked)
                {
                    // Get a Group Identifier to assign to each new batch.

                    sql = $@"
Select DISTINCT ( COUNT([OIBT].[U_NSC_GroupNum]) ) AS [GroupNumber]
FROM [OIBT]
WHERE [OIBT].[U_NSC_GroupNum] IS NOT NULL";


                    // Get the results from the SQL query
                    groupName = "H-GRP-" + NSC_DI.UTIL.SQL.GetValue<int>(sql);

                }

                oBar.Value++;
                // Grab the matrix of selected plants from the form UI
                SAPbouiCOM.Matrix MTX_PLAN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLAN", _SBO_Form);

                // Get the destination warehouse code from the UI.
                ComboBox CMB_WRHS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WRHS", _SBO_Form);

                // Make sure a destination warehouse was selected
                if (string.IsNullOrEmpty(CMB_WRHS.Value))
                {
                    if (oBar != null) oBar.Stop();
                    Globals.oApp.StatusBar.SetText("Please supply a destination warehouse!");

                    CMB_WRHS.Active = true;
                    return;
                }
                oBar.Value++;
                // Grab the destination warehouse code that the user selected
                string destinationWarehouse = CMB_WRHS.Value;

                //Number of Unique CropIDHolder
                int intCropCount = 0;
                string UniqueCropHolder = "";
                // For each row already selected
                for (int i = 1; i < (MTX_PLAN.RowCount + 1); i++)
                {
                    // Check for entered wet weight
                    double wet_weight = 0;
                    if (!Double.TryParse(((EditText)MTX_PLAN.Columns.Item(Matrix_Columns.WetWeight).Cells.Item(i).Specific).Value, out wet_weight))
                    {
                        // Plant has no wet weight listed, it has failed
                        AllPlantsPassed = false;
                        continue;
                    }

                    // Validate wet weight of each plant. They must be more then 0.
                    if (wet_weight <= 0)
                    {
                        if (oBar != null) oBar.Stop();
                        Globals.oApp.StatusBar.SetText("You must enter in a value greater than 0 for Wet Weight!");

                        // Set the invalid column to active for the user.
                        ((EditText)MTX_PLAN.Columns.Item(Matrix_Columns.WhsCode).Cells.Item(i).Specific).Active = true;
                        return;
                    }

                    // Grab the selected row's Plant ID column
                    string plantID = ((EditText)MTX_PLAN.Columns.Item(Matrix_Columns.UniqueID).Cells.Item(i).Specific).Value;

                    // sys number
                    var SysNumber = default(string);
                    try
                    {
                        SysNumber = CommonUI.Forms.GetField<string>(_SBO_Form, "MTX_PLAN", i, Matrix_Columns.SysNumber); //((EditText)MTX_PLAN.GetCellSpecific(6, i)).Value;      // "SysNumber"
                    }
                    catch
                    {
                        SysNumber = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT SysNumber FROM OSRN WHERE DistNumber = '{plantID}'");
                    }
                    // Grab the warehouse code the plant is currently in
                    string FromWarehouseCode = ((EditText)MTX_PLAN.Columns.Item(Matrix_Columns.WhsCode).Cells.Item(i).Specific).Value;
                    string ItemCode = ((EditText)MTX_PLAN.Columns.Item(Matrix_Columns.ItemCode).Cells.Item(i).Specific).Value;

                    string CropID = ((EditText)MTX_PLAN.Columns.Item(Matrix_Columns.CropID).Cells.Item(i).Specific).Value;
                    if(UniqueCropHolder.Trim()!=CropID.Trim())
                    {
                        UniqueCropHolder = CropID;
                        intCropCount += 1;
                    }
                    // Add the selected row plant ID to list
                    if(CropID=="0") CropID = null;
                    SelectedPlantIDs.Add(new PlantHarvestData(plantID, wet_weight, FromWarehouseCode, SysNumber,CropID, ItemCode));
                }
                oBar.Value++;
                if (intCropCount > 1)
                {
                    if (oBar != null) oBar.Stop();
                    Globals.oApp.StatusBar.SetText("Please select plants with the same crop cycle ID!", BoMessageTime.bmt_Short);

                    return;
                }

                // Keep track of the total amount of wet weight we are recording.
                var TotalWetWeight = SelectedPlantIDs.Sum(n => n.wetWeight);

                // Grab the destination warehouse code from the UI form
                string DestinationWarehouseCode = CommonUI.Forms.GetField<string>(_SBO_Form, "CMB_WRHS", true);

                // control was replace with a combo box
                //ButtonCombo CBT_ACTION = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ButtonCombo, "CBT_ACTION", _SBO_Form);
                ComboBox CBT_ACTION = _SBO_Form.Items.Item("cboHType").Specific;
                if (CBT_ACTION.Selected.Value == "")
                {
                    if (oBar != null) oBar.Stop();
                    Globals.oApp.StatusBar.SetText("Please select either Partial or Full Harvest from the action dropdown!", BoMessageTime.bmt_Short);

                    return;
                }
                #endregion
                oBar.Value++;
                bool isPartialHarvest = CBT_ACTION.Selected.Value == "Partial" ? true : false;
                bool bUpdatedProdO = false;

                //************NEW CODE*********************

                //Create Production Order for Wet Cannabis
                PlantHarvestData Plant_Holder = SelectedPlantIDs.First();
                // Split the plant ID by hyphen -
                string PlantID_Holder = Plant_Holder.plantId;

                var PlantItemCode = NSC_DI.SAP.Items.SNs.GetItemCode(PlantID_Holder);

                string strainID = null;
                if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y")
                {
                    strainID = NSC_DI.SAP.Items.SNs.GetStrainID(PlantID_Holder);
                }
                else
                {
                    strainID = NSC_DI.SAP.Items.Batches.GetStrainID(PlantID_Holder);
                }
                oBar.Value++;
                var SourceItemCode = NSC_DI.UTIL.AutoStrain.GetAutoItemCodeValue("~WC", strainID);

                SAPbouiCOM.EditText oTotalg = _SBO_Form.Items.Item("TXT_TOTALG").Specific;
                SAPbouiCOM.EditText oREM = _SBO_Form.Items.Item("TXT_REM").Specific;

                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                // set the Harvest name on all rows
                foreach (PlantHarvestData row in SelectedPlantIDs)
                {
                    sql = $"SELECT U_NSC_HarvestName FROM [OSRN] WHERE ItemCode = '{row.itemCode}' AND DistNumber = '{row.plantId}'";
                    //var hn = NSC_DI.UTIL.SQL.GetValue<string>(sql, null);   // if the value is null then there is no entry and the item must be a batch item
                    oRS = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(sql);
                    var hn = "";
                    if (oRS.EoF) hn = null;
                    if (oRS.EoF == false && oRS.Fields.Item(0).IsNull() == BoYesNoEnum.tYES) hn = "";
                    if (oRS.EoF == false && oRS.Fields.Item(0).IsNull() == BoYesNoEnum.tNO) hn = oRS.Fields.Item(0).Value;
                    if (hn != null)
                    {
                        if (harvestName.Value != hn) NSC_DI.SAP.SerialItems.SetUDF(row.plantId, row.itemCode, "U_NSC_HarvestName", harvestName.Value);
                    }
                    else
                    {
                        sql = $"SELECT U_NSC_HarvestName FROM [OBTN] WHERE ItemCode = '{row.itemCode}' AND DistNumber = '{row.plantId}'";
                        hn = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
                        if (harvestName.Value != hn) NSC_DI.SAP.BatchItems.SetUDF(row.itemCode, row.plantId, "U_NSC_HarvestName", harvestName.Value);
                    }
                }
                oBar.Value++;
                string strProcess = cProcess;
                if (isPartialHarvest) { strProcess += "-PARTIAL"; }

                int PdOKey = 0;
                if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y")
                {
                    PdOKey = NSC_DI.SAP.ProductionOrder.CreateFromBOM(SourceItemCode, SelectedPlantIDs.Count, DateTime.Now, DestinationWarehouseCode, true, "N", strProcess, oREM.Value);
                }
                else
                {
                    //GenAg - Need total count of plants from all batches.                    
                    EditText editCount = null;
                    SAPbouiCOM.Matrix oMX = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLAN", _SBO_Form);
                    for (int i = 1; i <= oMX.RowCount; i++)
                    {
                        editCount = oMX.GetCellSpecific(Matrix_Columns.PlantCount, i);
                        intPlantCOunt += Convert.ToInt16(editCount.Value.ToString()); ;
                    }

                    PdOKey = NSC_DI.SAP.ProductionOrder.CreateFromBOM(SourceItemCode, intPlantCOunt, DateTime.Now, DestinationWarehouseCode, true, "N", strProcess, oREM.Value);
                }

                if (PdOKey == 0)
                {
                    if (oBar != null) oBar.Stop();
                    Globals.oApp.MessageBox("Failed to create Production Order " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                    throw new WarningException();
                }

                //-------------------------------------------
                //Check the warehouse for the item(s) on tbe Production BOM and if needed adjust accordingly
                //Get clone Warehouses for selected items on the Matrix by batchnumber 
                SAPbobsCOM.ProductionOrders oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
                oPdO.GetByKey(PdOKey);
                oBar.Value++;
                // 10381 removed an oLines obj 
                //CropCycle Update
                string strCropFinHolder = null;
                
                if (Plant_Holder.cropId != null && Plant_Holder.cropId.Length > 0)
                {
                    bUpdatedProdO = true;
                    strCropFinHolder = oCrop.GetFinProj(Plant_Holder.cropId.ToString());
                    oPdO.Project = strCropFinHolder;
                }
                for (int i = 0; i <= oPdO.Lines.Count - 1; i++)
                {
                    oPdO.Lines.SetCurrentLine(i);
                    if (Plant_Holder.cropId != null && Plant_Holder.cropId.Length > 0)
                    {
                        bUpdatedProdO = true;
                        oPdO.Lines.Project = oCrop.GetFinProj(Plant_Holder.cropId.ToString());
                    }
                    if (PlantItemCode == oPdO.Lines.ItemNo)
                    {
                        string strWhsSourceHolder = NSC_DI.UTIL.SQL.GetValue<string>(@"select WhsCode from [OWHS] where OWHS.WhsCode = '" + Plant_Holder.fromWarehouseCode + "'", null);
                        if (strWhsSourceHolder != oPdO.Lines.Warehouse && oPdO.Lines.PlannedQuantity > 0)
                        {
                            //Update warehouse
                            oPdO.Lines.Warehouse = strWhsSourceHolder;
                            bUpdatedProdO = true;
                        }
                    }
                }               
                if (DestinationWarehouseCode != oPdO.Warehouse)
                {
                    oPdO.Warehouse = DestinationWarehouseCode;
                    bUpdatedProdO = true;
                }                
                if (bUpdatedProdO == true)
                {
                    oPdO.Update();
                }
                oBar.Value++;
                int PdOIssueKey = 0;
                if (!isPartialHarvest)
                {
                    if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" )
                    {
                        //Create Issue to Production for Selected plants SN - Full Harvest
                        string[] SN_Plants = SelectedPlantIDs.Select(n => n.SysNumber).ToArray();   // get all of the SNs
                        PdOIssueKey = NSC_DI.SAP.ProductionOrder.Issue(PdOKey, "Cannabis Plant", (double)SelectedPlantIDs.Count, null, SN_Plants, strCropFinHolder, null, harvestName.Value);
                    }
                    else
                    {
                        //GEN_AG
                        SAPbouiCOM.Matrix oMX = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLAN", _SBO_Form);
                        Dictionary<string, double> dicPlants = new Dictionary<string, double>();
                        for (int i = 1; i <= oMX.RowCount; i++)
                        {
                            string strBatchID = CommonUI.Forms.GetField<string>(_SBO_Form, "MTX_PLAN", i, Matrix_Columns.UniqueID);
                            double intIndvPlantCountByBatch = CommonUI.Forms.GetField<double>(_SBO_Form, "MTX_PLAN", i, Matrix_Columns.PlantCount);
                            dicPlants.Add(strBatchID, intIndvPlantCountByBatch);

                        }
                        PdOIssueKey = NSC_DI.SAP.ProductionOrder.Issue(PdOKey, "Cannabis Plant", intPlantCOunt, null, null, strCropFinHolder, dicPlants, harvestName.Value);
                    }

                    if (PdOIssueKey == 0)
                    {
                        if (oBar != null) oBar.Stop();
                        Globals.oApp.MessageBox("Failed to Issue to Production for selected Plants " + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                        throw new WarningException();
                    }

                    foreach (var plantid in SelectedPlantIDs)
                    {
                        if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y")
                        {
                            NSC_DI.SAP.SerialItems.SetWetWeight(plantid.plantId, plantid.wetWeight);
                        }
                        else
                        {
                            //GenAg
                            NSC_DI.SAP.BatchItems.SetWetWeight(plantid.plantId, plantid.wetWeight);
                        }
                    }
                }
                oBar.Value++;
                //-------------------------------------------
                // get the batch number
                // DELETE - 20201228
                // if there is already a Harvest with that name, use that Batch ID, otherwise get a new one 
                //var NextBatch = "";
                //sql = $"SELECT TOP 1 U_DistNumber FROM [@{NSC_DI.Globals.tHarvest}] WHERE U_NameSearch = '{harvestSearch}'";
                //NextBatch = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
                //string InterCoSubCode = NSC_DI.SAP.Warehouse.Get_InterCoCode(DestinationWarehouseCode);
                ////if (NextBatch == "") 
                //var NextBatch = NSC_DI.UTIL.AutoStrain.NextBatch(SourceItemCode, InterCoSubCode);
                var NextBatch = NSC_DI.SAP.BatchItems.NextBatch(SourceItemCode);

                //-------------------------------------------
                // update the Harvest table with the SNs
                UserTable sboTable = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tHarvest);
                foreach (PlantHarvestData row in SelectedPlantIDs)
                {
                    sboTable.Name = harvestName.Value;
                    sboTable.UserFields.Fields.Item("U_PdONum").Value = PdOKey;
                    sboTable.UserFields.Fields.Item("U_HarvestType").Value = (isPartialHarvest) ? "P" : "F";
                    sboTable.UserFields.Fields.Item("U_CreatedOn").Value = DateTime.Today;
                    sboTable.UserFields.Fields.Item("U_ItemCode").Value = NSC_DI.SAP.Items.SNs.GetItemCode(row.plantId);
                    sboTable.UserFields.Fields.Item("U_ItemType").Value = "S";
                    sboTable.UserFields.Fields.Item("U_ID").Value = row.plantId;
                    sboTable.UserFields.Fields.Item("U_Quantity").Value = row.wetWeight;
                    sboTable.UserFields.Fields.Item("U_DistNumber").Value = NextBatch;
                    sboTable.UserFields.Fields.Item("U_NameSearch").Value = harvestSearch;
                    if (sboTable.Add() != 0)
                    {
                        if (oBar != null) oBar.Stop();
                        throw new Exception("ERROR adding entry to HARVEST log." + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                    }
                }

                //Create Receopt from production for Total Distributed Wieght
                // RECEIPT FROM PRODUCTION ORDER 
                var PdOReceiptKey =0;
                //Inter-Company Check 
                //string InterCo = NSC_DI.UTIL.SQL.GetValue<string>("select T0.U_Value from dbo.[@NSC_SETTINGS] T0 where T0.Name = 'Inter-Company'");
                //NextBatch = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, NextBatch);

                //Get and Set StateID
                EditText txtStateID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_STID", _SBO_Form);
                string strStateID = null;
                if (txtStateID.Value.Length > 0) strStateID = txtStateID.Value;

                //Check to see if there is any waste and if so do the receipt forom production. 
                EditText txtWaste_Total = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WAST", _SBO_Form);
                string strWeightHolder = txtWaste_Total.Value;
                string NextBatch_WAST = "";

                // create array of Batch UDFs and Values
                string[,] HarvestUDFs = new String[3, 2];
                HarvestUDFs[0, 0] = "U_NSC_HarvestPdO";  HarvestUDFs[0, 1] = PdOKey.ToString();
                HarvestUDFs[1, 0] = "U_NSC_HarvestDate"; HarvestUDFs[1, 1] = DateTime.ParseExact(oHDATE.Value.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy");
                HarvestUDFs[2, 0] = "U_NSC_WetWgt";      HarvestUDFs[2, 1] = TotalWetWeight.ToString();

                if (strWeightHolder.Trim().Length > 0)
                {
                    double TotalWeight_WAST = Convert.ToDouble(strWeightHolder);
                    //Isolate  item code and append waste to prfex to get strain specific waste that is batch managed. 
                    string[] arItemCode = SourceItemCode.Split('-');
                    string SourceItemCode_WAST = "WA-" + arItemCode[1].ToString() + "-" + arItemCode[2].ToString();
                    //NextBatch_WAST = NSC_DI.UTIL.AutoStrain.NextBatch(SourceItemCode_WAST, InterCoSubCode);
                    //NextBatch_WAST = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, NextBatch_WAST);
                    NextBatch_WAST = NSC_DI.SAP.BatchItems.NextBatch(SourceItemCode_WAST);

                    //--------------------------------------------
                    // RECEIPT FROM PRODUCTION ORDER - With Waste
                    PdOReceiptKey = NSC_DI.SAP.ProductionOrder.Receive(PdOKey, "105", TotalWetWeight, NextBatch, null, "HARVEST", null, null, null, strStateID, TotalWeight_WAST, NextBatch_WAST, strCropFinHolder, HarvestUDFs);

                }
                else
                {
                    //--------------------------------------------
                    // RECEIPT FROM PRODUCTION ORDER - No Waste
                    PdOReceiptKey = NSC_DI.SAP.ProductionOrder.Receive(PdOKey, "105", TotalWetWeight, NextBatch, null, "HARVEST",null,null,null, strStateID,0,"", strCropFinHolder, HarvestUDFs);

                }
                oBar.Value++;
                if (PdOReceiptKey == 0)
                {
                    if (oBar != null) oBar.Stop();
                    Globals.oApp.MessageBox("Failed to Receipt from Production for batch " + NextBatch + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                    throw new WarningException();
                }

                //Set the Harvest Date
                string[] Plant_IDs = SelectedPlantIDs.Select(n => n.plantId).ToArray();
               

                if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y")
                {
                    NSC_DI.SAP.SerialItems.SetHarvestInfo(Plant_IDs, PdOKey, oHDATE.Value.ToString());
                    var oSN = NSC_DI.SAP.SerialItems.GetInfo(PlantID_Holder);
                
                    NSC_DI.SAP.BatchItems.UpdateUDFsFromObj(NextBatch, oSN, new string[5] { "U_NSC_StateID", "U_NSC_QuarState", "U_NSC_HarvestPdO", "U_NSC_HarvestDate", "U_NSC_WetWgt" });
                    //Copy UDFs to Wast
                    if (strWeightHolder.Trim().Length > 0)
                    {
                        NSC_DI.SAP.BatchItems.UpdateUDFsFromObj(NextBatch_WAST, oSN, new string[2] { "U_NSC_StateID", "U_NSC_QuarState" });
                    
                        //ToDo: Add stage code to UDF on plants for crop cycle

                    }
                }
                else
                {
                    //Add to batch for Gen Ag Harvests
                    //NSC_DI.SAP.BatchItems.SetHarvestInfo(Plant_IDs, PdOKey, oHDATE.Value.ToString());  // DELETE - 20201228
                    var oBN = NSC_DI.SAP.BatchItems.GetInfo(PlantID_Holder);
                    NSC_DI.SAP.BatchItems.UpdateUDFsFromObj(NextBatch, oBN, new string[2] { "U_NSC_StateID", "U_NSC_CropStageID" });
                    //Update the receipt batch wet weight to the total wet weight of all batches. 
                    //NSC_DI.SAP.BatchItems.SetWetWeight(NextBatch, TotalWetWeight);                     // DELETE - 20201228


                    //Copy UDFs to Wast
                    if (strWeightHolder.Trim().Length > 0)
                    {
                        NSC_DI.SAP.BatchItems.UpdateUDFsFromObj(NextBatch_WAST, oBN, new string[5] { "U_NSC_StateID", "U_NSC_QuarState", "U_NSC_HarvestPdO", "U_NSC_HarvestDate", "U_NSC_WetWgt" });

                        //ToDo: Add stage code to UDF on plants for crop cycle

                    }
                }
                oBar.Value++;

                //AddDocs To Projects - Crop Cycle Managment
                if (strCropFinHolder!=null && UniqueCropHolder != null)
                {
                    if (UniqueCropHolder.Trim().Length > 0)
                    {
                        string strStageID = "HARV_OTHER_" + UniqueCropHolder.Trim();
                        int intDocNum = NSC_DI.UTIL.SQL.GetValue<int>("Select DocNum from OWOR where DocEntry=" + PdOKey.ToString());
                        //Add Production Order to Project
                        oCrop.AddProdToProject(PdOKey, intDocNum, UniqueCropHolder, strStageID);
                        //Add the issue to the Project 
                        if (!isPartialHarvest)
                        {
                            oCrop.AddDocToProject_Single(PMDocumentTypeEnum.pmdt_GoodsIssue, PdOIssueKey.ToString(), UniqueCropHolder.Trim(), strStageID);
                        }
                        //Add The Receipt from Production to the 
                        oCrop.AddDocToProject_Single(PMDocumentTypeEnum.pmdt_GoodsReceipt, PdOReceiptKey.ToString(), UniqueCropHolder.Trim(), strStageID);


                        NSC_DI.SAP.BatchItems.UpdateUDF(NextBatch, "CropStageID", strStageID);
                        

                    }
                }
                

                NSC_DI.SAP.ProductionOrder.UpdateStatus(PdOKey, BoProductionOrderStatusEnum.boposClosed);

                if (isPartialHarvest) // Add entries to plant journal since there was no plant issued 
                {
                    foreach (var plantid in SelectedPlantIDs)
                    {
                        NSC_DI.SAP.PlantJournal.AddPlantJournalEntry(plantid.plantId, NSC_DI.Globals.JournalEntryType.Manicure, $"Manicured {plantid.wetWeight} grams to {NextBatch}", plantid.wetWeight, NextBatch);
                    }
                }

                //update batch StageID
                //ToDo: Determine if it is Plants are Batch or Serial if Batch call code below if Serial build a function that will up date the serialized plants
                //if (pCropID.Length > 0)
                //{
                //    string strReceivedBatchNum = NSC_DI.SAP.BatchItems.GetBatchNumFromDoc("60", prodOrder, 0);
                //    string strStageID = "PROP_OTHER_" + strCropHolder;
                //    NSC_DI.SAP.BatchItems.SetCropStageID(strReceivedBatchNum, ItemCode, strStageID);
                //}


                 if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                oBar.Value++;

                if (oBar != null) oBar.Stop();
                Globals.oApp.StatusBar.SetText("Successfully harvested selected plants!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                _SBO_Form.Close();

                //get active form (Production View) and refresh filter
                try
                {
                    Globals.oApp.Forms.ActiveForm.State = BoFormStateEnum.fs_Restore;
                    Globals.oApp.Forms.ActiveForm.Items.Item("BTN_FILTER").Click();
                }
                catch { }
            }
            catch (WarningException) { }
            catch (NSC_DI.SAP.B1Exception ex)
            {
                if (oBar != null) oBar.Stop();
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            catch (Exception ex)
            {
                if (oBar != null) oBar.Stop();              
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                if (oBar != null) oBar.Stop();
                NSC_DI.UTIL.Misc.KillObject(oBar);
                NSC_DI.UTIL.Misc.KillObject(oRS);
                GC.Collect();
            }
        }

        private void BTN_LOOKUP_ITEM_PRESSED()
        {
            SAPbouiCOM.Matrix MTX_PLAN = null;
            try
            {
                var harvestName = CommonUI.Forms.GetField<string>(_SBO_Form, "TXTHNAME");
                if (harvestName == "")
                {
                    Globals.oApp.MessageBox("You must enter a Harvest Name.");
                    return;
                }

                _SBO_Form.Freeze(true);

                var lookupValue = CommonUI.Forms.GetField<string>(_SBO_Form, "TXT_LOOKUP");

                if (string.IsNullOrEmpty(lookupValue?.Trim())) return;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(lookupValue))

                MTX_PLAN = _SBO_Form.Items.Item("MTX_PLAN").Specific as SAPbouiCOM.Matrix;

                // Keep track of the warehouse ID from the barcode (if passed)
                var WarehouseID = string.Empty;

                // If the first four characters of the barcode are "WHSE", then the rest of the barcode contains the warehouse code.
                if (lookupValue.Length >= 4 && lookupValue.Substring(0, 4) == "WHSE")
                {
                    WarehouseID = lookupValue.Replace("WHSE-", "");
                }

                //string sqlScanCheck = "SELECT S.DistNumber AS UniqueID, W.WhsCode, S.CreateDate, REPLICATE(' ', 36) AS WetWeight, I.ItemName, I.ItemCode" + 
                //    " FROM OSRN AS S JOIN OSRI AS I ON I.SysSerial = S.SysNumber AND I.ItemCode = S.ItemCode JOIN OWHS AS W ON W.WhsCode = I.WhsCode" + 
                //    $" WHERE I.Status = 0 AND W.U_NSC_WhrsType IN ('WET', 'FLO') AND (W.WhsCode = '{WarehouseID}' OR S.DistNumber = '{lookupValue}')";
                //UniqueID, WhsCode, CreateDate, WetWeight, ItemName, ItemCode
                string sqlScanCheck = $@"
                    SELECT OSRN.DistNumber AS UniqueID, OWHS.WhsCode, OSRN.CreateDate, SPACE(50) AS WetWeight, OSRI.ItemName, OSRI.ItemCode
, OSRN.[SysNumber] AS[SysNumber], OSRN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName], OWHS.BPLid AS [Branch]
 FROM OSRN JOIN OSRI ON OSRI.SysSerial = OSRN.SysNumber AND OSRI.ItemCode = OSRN.ItemCode JOIN OWHS ON OWHS.WhsCode = OSRI.WhsCode
 left Join OPMG on OPMG.AbsEntry = OSRN.U_NSC_CropID
WHERE OSRI.Status = 0 AND OWHS.U_NSC_WhrsType IN ('WET', 'FLO') AND (OWHS.WhsCode = '' OR OSRN.DistNumber = '{lookupValue}' OR OSRN.MnfSerial = '{lookupValue}')"; //10926


                Recordset dbResults = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(sqlScanCheck);

                // Datatable that is bound to the Matrix.
                DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("OSRN");

                string requiredItemCode = string.Empty;
                if (DataTable.Rows.Count > 0)
                {
                    requiredItemCode = DataTable.GetValue(Matrix_Columns.ItemCode.ToString("F"), 0).ToString();
                }

                List<int> indexsToSelect = new List<int>();
                for (int i = 0; i < dbResults.RecordCount; i++)
                {
                    string uniqueId      = dbResults.Fields.Item("UniqueID").Value.ToString();
                    string warehouseCode = dbResults.Fields.Item("WhsCode").Value.ToString();
                    DateTime createDate  = !string.IsNullOrEmpty(dbResults.Fields.Item("CreateDate").Value.ToString()) ? // If the string isn't empty.. parse the date out of the string.
                        Convert.ToDateTime(dbResults.Fields.Item("CreateDate").Value.ToString()) : DateTime.Now;
                    createDate           = createDate.Date;
                    string wetWeight     = dbResults.Fields.Item("WetWeight").Value.ToString();
                    string itemName      = dbResults.Fields.Item("ItemName").Value.ToString();

                    string itemCode      = dbResults.Fields.Item("ItemCode").Value.ToString();

                    var SysNumber        = dbResults.Fields.Item("SysNumber").Value.ToString();
                    var CropID           = dbResults.Fields.Item("CropID").Value.ToString();
                    var CropName         = dbResults.Fields.Item("CropName").Value.ToString();
                    var branch           = dbResults.Fields.Item("Branch").Value;

                    // 10823 --------->
                    if (Globals.BranchDflt > 0 && branch != Globals.BranchDflt) throw new System.ComponentModel.WarningException($"You have not been assigned to branch {branch}.");
                    if (Globals.BranchDflt == 0)
                    {
                        NSC_DI.SAP.Branch.Assigned(Globals.oCompany.UserSignature, branch);
                        var wh = MTX_PLAN.GetCellSpecific(Matrix_Columns.WhsCode, 1).Value;
                        if (wh != "")
                        {
                            var MTXBranch = NSC_DI.SAP.Warehouse.GetBranch(wh);
                            if (branch != MTXBranch) throw new System.ComponentModel.WarningException($"Branch {branch} does not match previous Branch.");
                        }
                        if (MTX_PLAN.RowCount == 1)
                        {
                            ComboBox CMB_WRHS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WRHS", _SBO_Form);
                            Load_Combobox_Warehouses(CMB_WRHS, warehouseCode);
                        }
                    }

                    // requiredItemCode is set to the first row.. if it is not set. We set it to our first record found.
                    if (!string.IsNullOrEmpty(requiredItemCode) && !requiredItemCode.Equals(itemCode))
                    {
                        dbResults.MoveNext();
                        continue;
                    }
                    else
                    {
                        requiredItemCode = itemCode;
                    }

                    bool removeFirstRow = false;
                    bool existsInList = false;
                    for (int t = 0; t < DataTable.Rows.Count; t++)
                    {
                        string compareItemName      = DataTable.GetValue(Matrix_Columns.ItemName.ToString("F"), t).ToString();
                        if (compareItemName == "") continue;
                        string compareCreateDate    = DataTable.GetValue(Matrix_Columns.CreateDate.ToString("F"), t).ToString();
                        string compareUniqueId      = DataTable.GetValue(Matrix_Columns.UniqueID.ToString("F"), t).ToString();
                        string CompareWetWeight     = DataTable.GetValue(Matrix_Columns.WetWeight.ToString("F"), t).ToString();
                        string CompareWarehouseCode = DataTable.GetValue(Matrix_Columns.WhsCode.ToString("F"), t).ToString();

                        if (compareItemName.Equals("Begin Scanning To Populate Form"))
                        {
                            removeFirstRow = true;
                        }

                        if (uniqueId.Equals(compareUniqueId))
                        {
                            // Matrix row 0 is the header.
                            indexsToSelect.Add(t + 1);

                            // Skip this.. we already have it in our list.
                            existsInList = true;
                            break;
                        }
                    }

                    if (existsInList)
                    {
                        dbResults.MoveNext();
                        continue;
                    }

                    MTX_PLAN.FlushToDataSource();   // #3894 save the currently entered weights

                    if (removeFirstRow)
                    {
                        DataTable.Rows.Remove(0);
                        MTX_PLAN.LoadFromDataSource();
                    }

                    // Determine the next row number
                    int oNewRowNumber = MTX_PLAN.RowCount;
                    var ss = createDate.ToShortDateString();
                    DataTable.Rows.Add();
                    DataTable.SetValue(Matrix_Columns.UniqueID.ToString("F"), oNewRowNumber, uniqueId);
                    DataTable.SetValue(Matrix_Columns.CreateDate.ToString("F"), oNewRowNumber, createDate.ToShortDateString());

                    if (itemName.Length > 38) //10926, getting a datatable column length error for itemnames longer than 38 chars. Could not find source of issue so this is the stop gap.
                        itemName = itemName.Remove(38);

                    DataTable.SetValue(Matrix_Columns.ItemName.ToString("F"), oNewRowNumber, itemName);
                    DataTable.SetValue(Matrix_Columns.WetWeight.ToString("F"), oNewRowNumber, wetWeight);
                    DataTable.SetValue(Matrix_Columns.WhsCode.ToString("F"), oNewRowNumber, warehouseCode);
                    DataTable.SetValue(Matrix_Columns.ItemCode.ToString("F"), oNewRowNumber, itemCode);
                    DataTable.SetValue("SysNumber", oNewRowNumber, SysNumber);
                    DataTable.SetValue("CropID", oNewRowNumber, CropID);
                    DataTable.SetValue("CropName", oNewRowNumber, CropName);
                    // We need to reload the Matrix.. so it saves the changes.
                    MTX_PLAN.LoadFromDataSource();

                    dbResults.MoveNext();
                }

                MTX_PLAN.LoadFromDataSource();

                foreach (int i in indexsToSelect)
                {
                    MTX_PLAN.SelectRow(i, true, true);
                }

                // Recalculate the Totals.
                TXT_TOTAL_KEYEVENT();
                
                // clear and reselect barcode field so we can add more

                CommonUI.Forms.SetField(_SBO_Form, "TXT_LOOKUP", string.Empty);

                (_SBO_Form.Items.Item("TXT_LOOKUP").Specific as EditText).Active = true;

                if (!ForceSameItemCode())
                {
                    Globals.oApp.SetStatusBarMessage("Item codes must all match for harvesting. We match on the first row item code.", BoMessageTime.bmt_Short);
                }

            }
            catch (System.ComponentModel.WarningException ex)
            {
                Globals.oApp.MessageBox(ex.Message);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                _SBO_Form.Freeze(false);
            }
        }

        private void BTN_DELETE_ITEM_SELECTED()
        {
            try
            {
                _SBO_Form.Freeze(true);

                // Getting a DataTable of the DataSource.
                DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("OSRN");

                // Grab the matrix from the form UI
                SAPbouiCOM.Matrix MTX_PLAN = CommonUI.Forms.GetControlFromForm(
                    CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLAN", _SBO_Form) as SAPbouiCOM.Matrix;

                // Go backwards across the list to remove the correct lines..
                for (int i = MTX_PLAN.RowCount; i >= 1; i--)
                {
                    if (MTX_PLAN.IsRowSelected(i))
                    {
                        try
                        {
                            //Remove this row out of the datatable since its selected.
                            DataTable.Rows.Remove(i - 1);
                        }
                        catch (Exception ex)
                        { /*  */ }
                    }
                }
                MTX_PLAN.LoadFromDataSource();

                _SBO_Form.Freeze(false);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void BTN_CLEAR_ALL()
        {
            try
            {
                _SBO_Form.Freeze(true);

                // Getting a DataTable of the DataSource.
                DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("OSRN");

                // Grab the matrix from the form UI
                SAPbouiCOM.Matrix MTX_PLAN = CommonUI.Forms.GetControlFromForm(
                    CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLAN", _SBO_Form) as SAPbouiCOM.Matrix;

                for (int i = 0; i < DataTable.Rows.Count; i++)
                {
                    DataTable.Rows.Remove(i);
                }

                MTX_PLAN.LoadFromDataSource();

                _SBO_Form.Freeze(false);
            } catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }
        // uses CMB_WHSE
        //private void TXT_DWHSE_KEYEVENT(ItemEvent pVal)
        //{
        //    try
        //    {
        //        if (pVal.CharPressed == 9)
        //        {
        //            EditText txtDryingWarehouse = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DWHSE", _SBO_Form);

        //            // Only handle the tab event if the text field is empty.
        //            if (string.IsNullOrEmpty(txtDryingWarehouse.Value))
        //            {
        //                txtDryingWarehouse.Active = true;
        //                HandleDryWarehouseSelectionCFL();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
        //    }
        //}

        private void CreatePlantHarvestComplianceCall(List<HarvestComplaincy> plantHarvestData, bool isPartialHarvest, string destinationWarehouseToParse)
        {
            /* // COMPLIANCY

            try
            {
                Recordset dbCommunication = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                List<string> PlantStateIds = new List<string>();
                List<BioTrack.Plant.PlantWeights> PlantWeights = new List<BioTrack.Plant.PlantWeights>();

                foreach (HarvestComplaincy info in plantHarvestData)
                {
                    PlantHarvestData plantData = info.PlantHarvestInfo;
                    List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> NewItemBatches = info.GoodsRecieptLines;

                    BioTrack.Plant.PlantWeights apiPlantWeight = new BioTrack.Plant.PlantWeights();

                    apiPlantWeight.Amount = plantData.wetWeight;
                    apiPlantWeight.InvType = BioTrack.Inventory.InvTypes.Flower;
                    apiPlantWeight.UOM = BioTrack.API.UnitOfMeasurement.g;

                    PlantStateIds.Add(plantData.stateId);
                    PlantWeights.Add(apiPlantWeight);
                }

                #region Creating Required Controllers for API Creation and Storing

                // Add the API Call into our Compliance monitor.
                Controllers.Compliance controllerCompliance = new Controllers.Compliance(SAPBusinessOne_Application: Globals.oApp, SAPBusinessOne_Company: Globals.oCompany);

                // Prepare a connection to the state API
                var TraceCon = new NSCNSC_DI.SAP._DI.UTIL.Misc.KillObject(oForm);TraceabilityAPI(SAPBusinessOne_Application: Globals.oApp,
                    SAPBusinessOne_Company: Globals.oCompany,
                    SAPBusinessOne_Form: _SBO_Form);

                BioTrack.API bioCrapAPI = TraceCon.new_API_obj();
                // Create a Bio Track API Controller.

                #endregion

                int destinationWarehouse = Convert.ToInt16(destinationWarehouseToParse);

                BioTrack.Plant.Harvest_Schedule(
                    BioTrackAPI: ref bioCrapAPI,
                    BarcodeIDs: PlantStateIds.ToArray());

                string compID = controllerCompliance.CreateComplianceLineItem(
                    Reason: Compliance.ComplianceReasons.Plant_Harvest_Schedule,
                    API_Call: bioCrapAPI.XmlApiRequest.ToString());

                // API: Schedule harvesting of wet cannabis from a plant.
                bioCrapAPI.PostToApi();

                if (bioCrapAPI.WasSuccesful)
                {
                    //codes are "0" not run, "1" successful, "2" error
                    controllerCompliance.UpdateComliancy(ComplianceID: compID,
                        ResponseXML: bioCrapAPI.xDocFromResponse.ToString(),
                        Status: Controllers.Compliance.ComplianceStatus.Success);
                }
                else
                {
                    //codes are "0" not run, "1" successful, "2" error
                    controllerCompliance.UpdateComliancy(ComplianceID: compID,
                        ResponseXML: bioCrapAPI.xDocFromResponse.ToString(),
                        Status: Controllers.Compliance.ComplianceStatus.Failed);
                    //if we error we don't bail out just yet, we need to record the plant_harvest xml for its information too
                }

                // Clearing out the XML that was created from the previous call.
                bioCrapAPI = TraceCon.new_API_obj();

                //If an Item is a Plant we need to create an API call for a new Plant.
                BioTrack.Plant.Harvest(
                    BioTrackAPI: ref bioCrapAPI,
                    BarcodeID: PlantStateIds.ToArray(),
                    weights: PlantWeights.ToArray(),
                    CollectAdditional: isPartialHarvest,
                    NewRoom: destinationWarehouse,
                    CollectionTime: null);

                compID = controllerCompliance.CreateComplianceLineItem(
                    Reason: Compliance.ComplianceReasons.Plant_Harvest,
                    API_Call: bioCrapAPI.XmlApiRequest.ToString());

                // API: Complete a harvest of wet cannabis from a plant.
                bioCrapAPI.PostToApi();

                if (bioCrapAPI.WasSuccesful)
                {
                    controllerCompliance.UpdateComliancy(ComplianceID: compID,
                        ResponseXML: bioCrapAPI.xDocFromResponse.ToString(),
                        Status: Controllers.Compliance.ComplianceStatus.Success);
                }
                else
                {
                    controllerCompliance.UpdateComliancy(ComplianceID: compID,
                        ResponseXML: bioCrapAPI.xDocFromResponse.ToString(),
                        Status: Controllers.Compliance.ComplianceStatus.Failed);
                    return;
                }
            }
            catch (Exception e)
            {

            }
            */

        }

        private void CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            if (pVal.SelectedObjects == null) return;

            try
            {
                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_WH":
                        CommonUI.CFL.SetFieldsWH(pForm, pVal, "CMB_WRHS", "txtWH_CFL");
                        break;

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private int BranchValidate(string pWHCode = "")
        {
            try
            {
                // validate the Branch
                var branch = NSC_DI.SAP.Branch.Get(pWHCode);    // wharehouse form the 1st line of the matrix
                var branchAry = NSC_DI.SAP.Branch.GetAll_User_Ary(branch);
                if (Globals.BranchDflt >= 0)                                                    // branches are being used
                {
                    if (branch <= 0 && Globals.BranchDflt < 1) return 0;                        // no branch on the WH, and no default branch
                    if (branch <= 0 && Globals.BranchDflt > 0) branch = Globals.BranchDflt;     // no branch on the WH, set to default branch
                    if (Globals.BranchDflt > 0 && Globals.BranchDflt != branch) return 0;       // the WH branch does not match the default Branch
                    if (Globals.BranchDflt == 0 && branchAry.Length < 1) return 0;              // the WH branch is not in the list of Branches for the user
                }
                return branch;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oRS);
                GC.Collect();
            }
        }
    }
}