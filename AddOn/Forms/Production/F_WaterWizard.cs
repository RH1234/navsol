﻿using System;
using System.Collections.Generic;
using SAPbobsCOM;
using SAPbouiCOM;
using B1WizardBase;

namespace NavSol.Forms.Production
{
    class F_WaterWizard : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "NSC_WATER_WIZ";
        private const string WAREHOUSETYPE_COLUMN_NAME = "WTR";

        // FOR OLD CODE
        public VERSCI.Forms _VirSci_Helper_Form;
        public SAPbouiCOM.Application _SBO_Application;
        public SAPbobsCOM.Company _SBO_Company;
        public SAPbouiCOM.Form _SBO_Form;
        public string FormUID { get; set; }
        private SAPbouiCOM.ProgressBar oProgBar;
        //Used for CA batch management and general hemp to determine which production order to send it to
        public static string CurrentPhase { get; set; }
        private static bool prntFrmMthrPltMgr = false;

        #endregion VARS

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //----------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            Form oFormHolder = null;
            Folder TAB_WATER = null;
            EditText TXT_ITEM = null;
            switch (pVal.ItemUID)
            {
                // "Apply" button was clicked
                case "btn_ok":
                    BTN_OK_Click();
                    //Check to see if the active form is the "Plant deatails" form and if so update the count on the water tab   
                    try
                    {

                        Globals.oApp.Forms.Item("NSC_PLANT_DET").Select();
                        oFormHolder = Globals.oApp.Forms.ActiveForm; 
                        TAB_WATER = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_WATER", oFormHolder);
                        //Plant ID
                        TXT_ITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEM", oFormHolder);

                        // Count how many current records exist within the database.
                        string strSQL =@"
SELECT
(SELECT COUNT(*) FROM [@NSC_PLANTJOURNAL] WHERE [U_EntryType] = 'Water'  AND [U_PlantID] = '" + TXT_ITEM.Value + "')";

                        //Set caption value on the water tab
                        TAB_WATER.Caption = "Water (" + NSC_DI.UTIL.SQL.GetValue<string>(strSQL,0) + ")";
                        TAB_WATER.Select();


                        NavSol.CommonUI.Matrix.LoadDatabaseDataIntoMatrix(oFormHolder, "@" + NSC_DI.Globals.tWaterLog, "mtx_plan", new List<NavSol.CommonUI.Matrix.MatrixColumn>() {
                                            new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "Code", Caption = "ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_DateCreated", Caption = "Date Created", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_Details", Caption = "Journal Entry", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_PHLEVEL", Caption = "PH Level", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_PPM", Caption = "PPM(s)", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }

                        },
 @"Select T0.Code, T0.U_DateCreated, 'Watered ' + left(cast(T0.U_Amount as varchar(9)), 5) + ' ' + T0.U_Measure as U_Details, T0.U_PHLEVEL, T0.U_PPM
 from [@" + NSC_DI.Globals.tWaterLog + "] T0 WHERE T0.[U_PlantID] = '" + TXT_ITEM.Value + "'");

//@"SELECT [Code] ,[U_DateCreated] ,[U_Note] FROM [@" + NSC_DI.Globals.tPlantJournal + "] WHERE [U_EntryType] = 'WATER' AND [U_PlantID] = '" + TXT_ITEM.Value + "'");
                    }
                    catch (Exception ex)
                    {
                        //Calling form is Plant Production (Garden) no need to do anything
                    }
                    finally
                    {
                        NSC_DI.UTIL.Misc.KillObject(oFormHolder);
                        NSC_DI.UTIL.Misc.KillObject(TAB_WATER);
                        NSC_DI.UTIL.Misc.KillObject(TXT_ITEM);
                        NSC_DI.UTIL.Misc.KillObject(oForm);
                        GC.Collect();
                    }
                    
                    break;
            }
 
        }

        #endregion AFTER EVENT

        public F_WaterWizard(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            // this.FormUID = "VSC_WATER_WIZ";
           
            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }
        public F_WaterWizard() : this(Globals.oApp, Globals.oCompany) { }

        public void Form_Load(bool isMotherPlantMngr, string[] ListOfPlantIDsToLoad, string pCurrentPhase = null)//
        {
            Matrix oMX_Plan = null;
            try
            {
               _SBO_Form = CommonUI.Forms.Load(cFormID, true);
                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                // Prepare the SQL statement that will load the selected plants data into the matrix
                string sql = null;
                prntFrmMthrPltMgr = isMotherPlantMngr;
                if ((prntFrmMthrPltMgr) || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && pCurrentPhase == "FLO") )
                {
                    CurrentPhase = pCurrentPhase;

                       sql = @"
SELECT DISTINCT
(CONVERT(nvarchar,[OSRN].[DistNumber])) AS [UniqueID]
,(SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) AS [WhsCode]
,[OSRN].[CreateDate]
,[OSRN].[itemName],OSRN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]
FROM [OSRN]
left Join OPMG on OPMG.AbsEntry = OSRN.U_NSC_CropID
WHERE
(SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) IN 
(
SELECT 
[OWHS].[WhsCode]
FROM 
[OWHS]
WHERE
";
                    foreach (string PlantID in ListOfPlantIDsToLoad)
                    {
                        sql += " (CONVERT(nvarchar,[OSRN].[DistNumber])) = '" + PlantID + "' OR";
                    }

                    // Remove the last "OR" in the SQL query and closes the subquery.
                    sql = sql.Substring(0, sql.Length - 2) + ")";

                    // Load data about selected plants into the matrix
                    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(
                    pForm: _SBO_Form,
                    DatabaseTableName: "OSRN",
                    MatrixUID: "MTX_PLAN",
                    ListOfColumns: new List<CommonUI.Matrix.MatrixColumn>() {
                        new CommonUI.Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropID",               Caption="Crop ID",               ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropName",             Caption="Crop Name",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="WhsCode",             Caption="Warehouse",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                    },
                    SQLQuery: sql
                );
                }
                else
                {
                    CurrentPhase = pCurrentPhase;

                    sql = @"  SELECT DISTINCT
(CONVERT(nvarchar,[OBTN].DistNumber)) AS[UniqueID]
--,(SELECT[OIBT].[WhsCode] FROM[OIBT] WHERE[OIBT].[ItemCode] = [OBTN].[ItemCode] AND[OIBT].SysNumber = [OBTN].[SysNumber]) AS[WhsCode]
,[OIBT].WhsCode
,[OBTN].[CreateDate]
,[OBTN].[itemName],OBTN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]
        FROM[OBTN]
left Join OPMG on OPMG.AbsEntry = OBTN.U_NSC_CropID
join OIBT on OIBT.BatchNum = OBTN.DistNumber and OIBT.ItemCode = OBTN.ItemCode
inner join OWHS on OIBT.WhsCode = OWHS.WhsCode 
inner join [@NSC_PDO_PHASES] T0 on T0.U_CurrentPhase = OWHS.U_NSC_WhrsType 
WHERE
(
SELECT[OIBT].[WhsCode] FROM[OIBT] WHERE [OIBT].[ItemCode] = [OBTN].ItemCode AND[OIBT].BatchNum = [OBTN].DistNumber and [OIBT].Quantity>0 ) IN
(
SELECT
[OWHS].[WhsCode]
FROM
[OWHS]
Where ";
                    foreach (string PlantID in ListOfPlantIDsToLoad)
                    {
                        sql += "(CONVERT(nvarchar,[OBTN].DistNumber)) = '" + PlantID + "' OR";
                    }

                    // Remove the last "OR" in the SQL query and closes the subquery.
                    sql = sql.Substring(0, sql.Length - 2) + ")  and [OIBT].Quantity>0 and [OIBT].WhsCode!='9999'";

                    // Load data about selected plants into the matrix
                    CommonUI.Matrix.LoadDatabaseDataIntoMatrix(
                        pForm: _SBO_Form,
                        DatabaseTableName: "OBTN",
                        MatrixUID: "MTX_PLAN",
                        ListOfColumns: new List<CommonUI.Matrix.MatrixColumn>() {
                        new CommonUI.Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropID",               Caption="Crop ID",               ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropName",             Caption="Crop Name",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="WhsCode",             Caption="Warehouse",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                        },
                        SQLQuery: sql
                    );

                }

                // Color the selected grid
                //***This line was causing the application to crash as the GetControl From form could not resolve and caused an overflow
                //ColorRowsForMatrix(((Matrix)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "mtx_plan", _SBO_Form)), 4);
                oMX_Plan = _SBO_Form.Items.Item("MTX_PLAN").Specific;
                ColorRowsForMatrix(oMX_Plan, 4);
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_Main", "watercan-icon.bmp");

                // Find the Combobox of Warehouses from the Form UI
                ComboBox CMB_WTANK = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.ComboBox, "CBX_WTANK", _SBO_Form) as ComboBox;

                // Load in Warehouses that have water tanks only.
                Load_Combobox_Warehouses(CMB_WTANK);

                // set the default UOM
                try
                {
                    var uom = NSC_DI.SAP.Items.GetField<string>("H2O", "InvntryUom");
                    //_SBO_Form.Items.Item("CMB_Meas").Specific.Select(2, BoSearchKey.psk_Index);
                    _SBO_Form.Items.Item("CMB_Meas").Specific.Select(uom, BoSearchKey.psk_ByDescription); // this does not work.
                }
                catch { }

                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                // Show the form
                _SBO_Form.VisibleEx = true;

                // If we have only one plant selected we automatically set the selected distribution to Per plant.
                if (ListOfPlantIDsToLoad.Length == 1)
                {
                    //Only do the below if the plant is serialized if it is batched managed they may have one line but many plants in the given batch
                    if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && pCurrentPhase == "FLO"))
                    {
                        try
                        {
                            // Grab the ComboBox list for water distribution from the UI
                            ComboBox CMB_dist = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_dist", _SBO_Form);

                            // Remove all ValidValues from our list of selections.
                            if (CMB_dist.ValidValues.Count > 0)
                            {
                                foreach (SAPbouiCOM.ValidValue value in CMB_dist.ValidValues)
                                {
                                    CMB_dist.ValidValues.Remove(value.Value);
                                }

                                // Set the only option for the user to Per plant.
                                CMB_dist.ValidValues.Add("Per", "Per plant");
                                CMB_dist.Select("Per");

                                // Let go of focus and set editable to false.
                                CMB_dist.Active = false;
                                CMB_dist.Item.Enabled = false;
                            }
                        }

                        catch (Exception ex)
                        {
                            throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                        }
                    }
                }

                // If we only have one Water tank warehouse available we will automatically set it and then lock the dropdown.
                if (CMB_WTANK.ValidValues.Count == 1)
                {
                    CMB_WTANK.Select(0, BoSearchKey.psk_Index);
                    CMB_WTANK.Active = false;
                    CMB_WTANK.Item.Enabled = false;
                }

                // Grab the textbox for the amount of water from the form UI
                EditText TXT_amnt = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_amnt", _SBO_Form);

                // Grab the textbox for the PPM of water from the form UI
                EditText TXT_PPM = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_PPM", _SBO_Form);

                // Defaulting the Parts Per Million to 0.
                TXT_PPM.Value = "0";

                // Set the quantity to have focus.
                TXT_amnt.Active = true;
            }
            catch (Exception ex)
            {               
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));

            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMX_Plan);
                GC.Collect();
                
            }
        }

        private void Load_Combobox_Warehouses(ComboBox CMBControl)
        {
            // Prepare to run a SQL statement.
            SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            // Currently Hard Coded to use Warehouse Type of WTR. Might be pulled out at sometime.
            string sql = $@"SELECT 
                [WhsCode]
                ,[WhsName]
                FROM [OWHS]
                WHERE [OWHS].U_NSC_WhrsType ='{WAREHOUSETYPE_COLUMN_NAME}'";
            if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})"; // #10823 
            oRecordSet.DoQuery(sql);
               
            
            // If items already exist in the drop down
            if (CMBControl.ValidValues.Count > 0)
            {
                // Remove all currently existing values from warehouse drop down
                for (int i = CMBControl.ValidValues.Count; i-- > 0; )
                {
                    CMBControl.ValidValues.Remove(i, BoSearchKey.psk_Index);
                }
            }

            // If more than 1 warehouses exists
            if (oRecordSet.RecordCount > 1)
            {
                // Create the first item as an empty item
                CMBControl.ValidValues.Add("", "");

                // Select the empty item (forcing the user to make a decision)
                CMBControl.Select(0, BoSearchKey.psk_Index);
            }

            // Add allowed warehouses to the drop down
            for (int i = 0; i < oRecordSet.RecordCount; i++)
            {
                try
                {
                    CMBControl.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
                }
                catch { }
                CMBControl.Item.Enabled = true;
                oRecordSet.MoveNext();
            }

        }

        /// <summary>
        /// Colors the rows in a matrix based off of Plant Status column.
        /// </summary>
        /// <param name="MatrixToColor"></param>
        /// <param name="ColumnNumberForStatus"></param>
        private void ColorRowsForMatrix(Matrix MatrixToColor, int ColumnNumberForStatus)
        {
            //// Change row color of items in matrix based off of the compliance status
            //SAPbouiCOM.CommonSetting mtx_comp_settings = MatrixToColor.CommonSetting;

            //for (int i = 1; i < (MatrixToColor.RowCount + 1); i++)
            //{
            //    mtx_comp_settings.SetRowBackColor(i, NavSol.Models.Plant.GetColorFromStage(((SAPbouiCOM.EditText)MatrixToColor.Columns.Item(ColumnNumberForStatus).Cells.Item(i).Specific).Value));
            //}
        }

        private void BTN_OK_Click()
        {
            try
            {
                
                // Grab the textbox for the amount of water from the form UI
                _SBO_Form = Globals.oApp.Forms.ActiveForm;
                EditText TXT_amnt = _SBO_Form.Items.Item("TXT_amnt").Specific;
                //****This was causing a crash so I commented it out. 
                //CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_amnt", _SBO_Form);

                // Make sure a water amount was passed
                if (string.IsNullOrEmpty(TXT_amnt.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter in an amount of water!");
                    TXT_amnt.Active = true;
                    throw new System.ComponentModel.WarningException();
                }

                // Make sure the amount of water passed is an integer
                try
                {
                    decimal amount = Convert.ToDecimal(TXT_amnt.Value);
                }
                catch
                {
                    Globals.oApp.StatusBar.SetText("Please enter a numeric value for water amount!");
                    TXT_amnt.Active = true;
                    throw new System.ComponentModel.WarningException();

                }

                // Grab the ComboBox list for unit of measurement from the UI
                ComboBox CMB_Meas = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_Meas", _SBO_Form);

                // Make sure a unit of measurement was selected
                if (string.IsNullOrEmpty(CMB_Meas.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please select a unit of measerment for the water!");
                    CMB_Meas.Active = true;
                    throw new System.ComponentModel.WarningException();
                }

                // Find the Combobox of Warehouses from the Form UI
                ComboBox CMB_WTANK = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CBX_WTANK", _SBO_Form) as ComboBox;

                // Validate we have a Warehouse/Water Tank set.
                if (string.IsNullOrEmpty(CMB_WTANK.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please select a Warehouse with a water tank!");
                    CMB_WTANK.Active = true;
                    throw new System.ComponentModel.WarningException();
                }

                // Grab the textbox for the PPM of water from the form UI
                EditText TXT_PPM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_PPM", _SBO_Form);

                // Make sure a water PPM was passed
                if (string.IsNullOrEmpty(TXT_PPM.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter in a PPM amount!");
                    TXT_PPM.Active = true;
                    throw new System.ComponentModel.WarningException();
                }

                // Grab the textbox for the phLevel of water from the form UI
                EditText TXT_PHLVL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_PHLVL", _SBO_Form);

                // Make sure a water phLevel was passed
                if (string.IsNullOrEmpty(TXT_PHLVL.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter in a pH amount!");
                    TXT_PHLVL.Active = true;
                    throw new System.ComponentModel.WarningException();
                }

                // Make sure a water phLevel was passed
                //if (CHK_DIOWTR.Checked = true)
                //{
                //    Globals.oApp.StatusBar.SetText("Was this dionized water?", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                //    CHK_DIOWTR.Checked = true;
                //    return;
                //}

                // Grab the matrix of selected plants from the form UI
                Matrix mtx_plan = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PLAN", _SBO_Form);

                // Keep track of the selected Plant ID's
                List<string> SelectedPlantIDs = new List<string>();

                // For each row already selected
                for (int i = 1; i < (mtx_plan.RowCount + 1); i++)
                {                     
                    // Grab the selected row's Plant ID column
                    string plantID = ((EditText)mtx_plan.Columns.Item(0).Cells.Item(i).Specific).Value;

                    // Add the selected row plant ID to list
                    SelectedPlantIDs.Add(plantID);                    
                }
               
                // Grab the amount of water we are applying to the plants
                var WaterToApply = Convert.ToDouble(TXT_amnt.Value);

                // Grab the ComboBox list for water distribution from the UI
                ComboBox CMB_dist = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_dist", _SBO_Form);

                double WaterToApply_Counter = 0.0;
                // If distributed type is Per, calculate the amount of water applied.
                if (prntFrmMthrPltMgr ||
                    (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") ||
                    (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase == "FLO"))
                {
                    //WaterToApply = WaterToApply * SelectedPlantIDs.Count;
                    WaterToApply_Counter = SelectedPlantIDs.Count;
                }
                else
                {
                    //Batch Managed Plants - We will have to account for the number of plants in each batch  
                    for (int i = 0; i < SelectedPlantIDs.Count; i++)
                    {
                        string strBatchID = SelectedPlantIDs[i].ToString();
                        int BatchQty_Holder = NSC_DI.UTIL.SQL.GetValue<int>(@"Select OBTQ.Quantity
                                                                                from OBTN 
                                                                               inner join OBTQ on OBTN.AbsEntry = OBTQ.MdAbsEntry and OBTN.ItemCode = OBTQ.ItemCode
                                                                               inner join OWHS on OBTQ.WhsCode = OWHS.WhsCode 
                                                                               inner join [@NSC_PDO_PHASES] T0 on T0.U_CurrentPhase = OWHS.U_NSC_WhrsType 
                                                                               where OBTN.DistNumber = '" + strBatchID + "'  and  OBTQ.Quantity>0 and OBTQ.WhsCode!='9999'");
                        if (i == 0)
                        {
                            //WaterToApply_Counter = WaterToApply * BatchQty_Holder;
                            WaterToApply_Counter = BatchQty_Holder;
                        }
                        else
                        {
                            //WaterToApply_Counter += WaterToApply * BatchQty_Holder;
                            WaterToApply_Counter += BatchQty_Holder;
                        }
                    }
                    //WaterToApply = WaterToApply_Counter;
                }

                if (CMB_dist.Value == "Per")
                    WaterToApply = WaterToApply * WaterToApply_Counter;
                //else
                //    WaterToApply = WaterToApply / WaterToApply_Counter;


                // get the inventory UOM
                var invUOM = NSC_DI.SAP.Items.GetField<string>("H2O", "InvntryUom");

                WaterToApply = NSC_DI.UTIL.Units.ConvertLiquid(WaterToApply, CMB_Meas.Value, invUOM);

                //// Get the current Unit of Measure that we are applying.
                //NSC_DI.Globals.LiquidMeasurements LiquidMeasurement = NSC_DI.SAP.WaterLog.GetLiquidMeasurementFromString(CMB_Meas.Value);

                //// Convert the amount into mililiter
                //double milliliterAmount = NSC_DI.SAP.WaterLog.GetMillilitersFromAmountOfLiquid(LiquidMeasurement, Amount: WaterToApply);

                //// Convert the amount into gallons.
                //double GallonsOfWaterToReduceFromInventory = Convert.ToDouble(
                //    NSC_DI.SAP.WaterLog.GetAmountFromMilliliters(
                //        Measurement: NSC_DI.Globals.LiquidMeasurements.gal,
                //        MilliliterAmount: milliliterAmount));


                // Get the "description" from the ComboBox, where we are really storing the value
                string SelectedDestinationWarehouseID = CommonUI.ComboBox.GetComboBoxValueOrDescription(
                    pForm: ref _SBO_Form,
                    pComboBoxUID: "CBX_WTANK",
                    pComboBoxItemDescription: null,
                    pComboBoxItemValue: CMB_WTANK.Value
                    );

                //CheckBox oCKB = null;

                // 6183 instantiates the prog bar here so that it has the scope for the finally at the bottom
                ProgressBar oBar = null;
                oBar = Globals.oApp.StatusBar.CreateProgressBar("Watering Plants", mtx_plan.RowCount, false);
               
                try
                {
                    string First_CropID = null;
                    string WhsHolder = null;
                    string CropIDHolder = null;
                    int intFail = 0;

                    string Crop = NSC_DI.UTIL.SQL.GetValue<string>("Select U_Value1 from [@NSC_USER_OPTIONS] where name='Crop Cycle Management'");
                    if (Crop == "Y")
                    {
                        //Check For all the same crop. ToDo:in future allow for multiple crops to be on one order. Issue: how much to apply to each crop (Solvable but no time right now)
                        for (int i = 1; i < (mtx_plan.RowCount + 1); i++)
                        {
                            // Grab the selected row's Plant ID column
                            string plantID = ((EditText)mtx_plan.Columns.Item(0).Cells.Item(i).Specific).Value;
                            if (i == 1)
                            {
                                First_CropID = ((EditText)mtx_plan.Columns.Item(3).Cells.Item(i).Specific).Value;
                                WhsHolder = ((EditText)mtx_plan.Columns.Item(5).Cells.Item(i).Specific).Value;
                            }
                            else
                            {
                                CropIDHolder = ((EditText)mtx_plan.Columns.Item(3).Cells.Item(i).Specific).Value;
                                if (First_CropID != CropIDHolder)
                                {
                                    intFail = 1;
                                }
                            }
                        }
                        if (intFail == 1)
                        {
                            Globals.oApp.MessageBox("Please cancle and select all plants from the same crop.");
                            return;
                        }
                        int Check_First_CropID = First_CropID.Length == 0 ? Check_First_CropID = 0 : Check_First_CropID = int.Parse(First_CropID);
                        if (Check_First_CropID > 0 && !prntFrmMthrPltMgr)
                        {
                            string Prd_StageID = null;
                            string Prd_Key = null;
                            //Get Active Plant Stage
                            string strStage = NSC_DI.UTIL.SQL.GetValue<string>("Select U_NSC_WhrsType from OWHS where WhsCode='" + WhsHolder + "'");
                            if (strStage != "FLO")
                            {
                                Prd_StageID = "VEG_WATER_" + First_CropID;
                            }
                            else
                            {
                                Prd_StageID = "FLOW_WATER_" + First_CropID;
                            }
                            //Get production orders
                            Prd_Key = NSC_DI.UTIL.SQL.GetValue<string>("Select DocEntry from OWOR where U_NSC_CropStageID='" + Prd_StageID + "'");
                            //Update line on production order and Issue the Items. Then link to the project
                            Forms.Crop.CropCycleManagement oCrop = new Forms.Crop.CropCycleManagement();
                            string strCropFinProjID = oCrop.GetFinProj(First_CropID);
                            int IssueDocNum = NSC_DI.SAP.ProductionOrder.UpdateGEN_CropProdLine(Convert.ToInt32(Prd_Key), "H2O", WaterToApply, SelectedDestinationWarehouseID, strCropFinProjID);
                            oCrop.AddDocToProject_Single(PMDocumentTypeEnum.pmdt_GoodsIssue, IssueDocNum.ToString(), First_CropID, Prd_StageID);
                        }
                    }
                    else
                    {
                        NSC_DI.SAP.GoodsIssued.Create(DateTime.Now, new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>()
                        {
                            new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
                            {
                                ItemCode = "H2O",
                                Quantity = WaterToApply,
                                WarehouseCode = SelectedDestinationWarehouseID,
                            }
                        });
                    }


                    // We will always devide our amount by our plants used. We calcualted above the amount needed for all.
                    double amountToApplyPerPlant = WaterToApply / SelectedPlantIDs.Count;


                    //oCKB = this._SBO_Form.Items.Item("CHK_DIOWTR").Specific;
                    string strDionizedChecked = CommonUI.CheckBox.GetValue(this._SBO_Form, "CHK_DIOWTR");
                    
                    oBar.Value = 0; //6183 prog bar starts here
                    foreach (string PlantID in SelectedPlantIDs)
                    {
                        // Add a WaterLog to our database.
                        NSC_DI.SAP.WaterLog.AddWaterLog(
                            PlantID: PlantID,
                            AmountOfWaterApplied: amountToApplyPerPlant,
                            UnitOfMeasurement: NSC_DI.SAP.WaterLog.GetLiquidMeasurementFromString(invUOM), //  CMB_Meas.Value),
                            PPM: TXT_PPM.Value,
                            PHLevel: TXT_PHLVL.Value,
                            //Dionized: CommonUI.Forms.GetField<string>(this._SBO_Form, "CHK_DIOWTR", 0, 0, false)
                            Dionized: strDionizedChecked                           
                        );
                        oBar.Value++; // //6183 iterates prog bar here
                    }

                    //6183 if the code runs successfully ends prog bar here and then displays success msg. I did this because the msg was not working in the finally and it would disappear immediately when the prog bar is ended.
                    if (oBar != null) oBar.Stop();
                    Globals.oApp.StatusBar.SetText("Successfully watered " + SelectedPlantIDs.Count + " plants with " + WaterToApply + " " + CMB_Meas.Value + " of water!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

                    // Close the watering wizard
                    if (!System.Diagnostics.Debugger.IsAttached) _SBO_Form.Close();
                }
                catch (Exception ex)
                {
                    //Globals.oApp.StatusBar.SetText("Failed to create goods issued receipt: " + ex.Message.Substring(0, ex.Message.IndexOf(Environment.NewLine)));
                    //System.Media.SystemSounds.Beep.Play();
                    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                }
                finally
                {
                    // NSC_DI.UTIL.Misc.KillObject(oCKB);
                    // 6183 ensures that the prog bar is ended. If code runs successfully this should not be triggered
                    if (oBar != null) oBar.Stop();
                    GC.Collect();
                }
            } catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }
    }
}