﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Propagation
{
    class F_MotherPlant : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_MOTHERPLANT";

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		public string FormUID { get; set; }
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
	    [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
	    public virtual bool OnBeforeItemPressed(ItemEvent pVal)
	    {
		    bool BubbleEvent = true;
		    Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


		    NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();

		    return BubbleEvent;
	    }
	    #endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

            switch (pVal.ItemUID)
            {
                // Add to "Selected Plants"
                case "btn_Add":
                    btnAdd(oForm);
                    break;

                // Remove froWaterWizard Plants" button was clicked
                case "btn_Rem":
                    btnRem(oForm);
                    break;

                // Water
                case "btn_H2O":
                    btnH2O(oForm);
                    break;

                // Clone
                case "btn_Clone":
                    btnClone(oForm);
                    break;

                // Feed
                case "btn_Feed":
                    btnFeed(oForm);
                    break;

                // Prune
                case "btn_Prune":
                    btnPrune(oForm);
                    break;

                // Treat
                case "btn_Treat":
                    btnTreat(oForm);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        #endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		public F_MotherPlant() : this(Globals.oApp, Globals.oCompany) { }

		public F_MotherPlant(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            _SBO_Application = SAPBusinessOne_Application;
			_SBO_Company = SAPBusinessOne_Company;
        }

        public void Form_Load()
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                Load_Matrix_Main();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
				_SBO_Form.Freeze(false);
				_SBO_Form.VisibleEx = true;

				//NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
				GC.Collect();
            }
        }

        private void Load_Matrix_Main()
        {
            // Prepare a Matrix form helper
            VERSCI.Matrix oMatrixHelper = new VERSCI.Matrix(SAPBusinessOne_Application: Globals.oApp, FormToUse: _SBO_Form);
            string SQLQuery = @"
SELECT
(CONVERT(nvarchar,[OSRI].IntrSerial)) AS [UniqueID]
,[OSRI].[WhsCode]
,[OSRI].[InDate] AS [CreateDate]
,[OSRI].[itemName]
FROM [OSRI]
JOIN [OWHS] ON [OWHS].WhsCode = [OSRI].[WhsCode]
JOIN OSRQ ON OSRI.ItemCode = OSRQ.ItemCode AND OSRI.SysSerial = OSRQ.SysNumber
WHERE
[OSRI].[U_NSC_IsMother] = 'Y'
AND [OWHS].U_NSC_WhrsType NOT IN ('QNX', 'QND', 'QNS') 
AND OSRQ.Quantity > 0";
            if (Globals.BranchDflt >= 0) SQLQuery += $" AND OWHS.BPLid IN ({Globals.BranchList})";
            SQLQuery += @"ORDER BY[OSRI].[ItemCode],[OSRI].[SysSerial] ASC";
// Load data about plants into the matrix
oMatrixHelper.LoadDatabaseDataIntoMatrix("OSRN", "mtx_mplant", new List<VERSCI.Matrix.MatrixColumn>() { 
                    new VERSCI.Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                    new VERSCI.Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                    new VERSCI.Matrix.MatrixColumn(){ ColumnName="WhsCode", Caption="Warehouse", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                    new VERSCI.Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                }, SQLQuery);

            // Grab the matrix from the form UI
            Matrix mtx_mplant = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "mtx_mplant", _SBO_Form) as Matrix;

            // Allow multi-selection in the Matrix
            mtx_mplant.SelectionMode = BoMatrixSelect.ms_Single;
        }
              

        private static List<string> GetSelectedPlants(Form pForm, bool errorIfSelectionEmpty = true)
        {
            try
            {

                // Grab the matrixes from the UI
                SAPbouiCOM.Matrix mtx_mselc = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "mtx_mselc", pForm);

                // Keep track of the selected Plant ID's
                List<string> SelectedPlantIDs = new List<string>();
                var rowCount = ((SAPbouiCOM.Matrix)pForm.Items.Item("mtx_mselc").Specific).RowCount;
                // For each row already selected
                for (int i = 1; i <= rowCount; i++)
                {
                    // Grab the selected row's Plant ID column
                    string plantID = CommonUI.Forms.GetField<string>(pForm, "mtx_mselc", i, 0);

                    // Add the selected row plant ID to list
                    SelectedPlantIDs.Add(plantID);
                }

                if (SelectedPlantIDs.Count < 1 && errorIfSelectionEmpty)
                {
                    Globals.oApp.StatusBar.SetText("Please select some plants first!", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                    //throw new System.ComponentModel.WarningException();
                }

                return SelectedPlantIDs;
            }
			catch (System.ComponentModel.WarningException)
			{
				// message was already displayed
				throw new System.ComponentModel.WarningException();
			}
			catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                //NSC_DI.UTIL.Misc.KillObject(IMG_Main);
                GC.Collect();
            }
        }

        private void btnAdd(Form pForm)
        {
            try
            {
                // Grab the matrixes from the UI
                SAPbouiCOM.Matrix mtx_mplant = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "mtx_mplant", pForm);
                SAPbouiCOM.Matrix mtx_mselc = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "mtx_mselc", pForm);

                // Keep track of the selected Plant ID's
                var SelectedPlantIDs = GetSelectedPlants(pForm, false);
                
                // Keep track of each selected row number
                List<int> SelectedRows = new List<int>();

                // Find what rows were selected in the main matrix
                for (int i = 1; i <= (mtx_mplant.RowCount); i++)
                {
                    if (mtx_mplant.IsRowSelected(i))
                    {
                        // Add the selected row index to list
                        SelectedRows.Add(i);

                        // Grab the selected row's Plant ID column
                        string plantID = ((SAPbouiCOM.EditText)mtx_mplant.Columns.Item(0).Cells.Item(i).Specific).Value;

                        // Add the selected row plant ID to list
                        SelectedPlantIDs.Add(plantID);
                    }
                }

                // As we delete rows from the main matrix, the index of the rows will change.
                // We will keep track of how many rows we have deleted, so that we can buffer the deletion call so we delete the appropriate rows.
                int deletionBuffer = 0;
                // Remove the rows from the main matrix
                foreach (int row in SelectedRows)
                {
                    mtx_mplant.DeleteRow(row - deletionBuffer);
                    deletionBuffer++;
                }


                if (SelectedPlantIDs.Count > 0)
                {

                    // Prepare the SQL query for the selected plants
                    string SQL = @"
SELECT
(CONVERT(nvarchar,[OSRN].DistNumber)) AS [UniqueID]
,[OSRN].[LotNumber]
,[OITM].[CreateDate]
,[OITM].[itemName]
FROM [OSRN]
JOIN [OITM]
ON 
[OSRN].[ItemCode] = [OITM].[ItemCode]
WHERE
";

                    foreach (string PlantID in SelectedPlantIDs)
                    {
                        SQL += " (CONVERT(nvarchar,[OSRN].DistNumber))   = '" + PlantID + "' OR";
                    }

                    // Remove the last "OR" in the SQL query.
                    SQL = SQL.Substring(0, SQL.Length - 2);

                    // Prepare a Matrix form helper
                    VERSCI.Matrix oMatrixHelper = new VERSCI.Matrix(_SBO_Application, _SBO_Form);

                    // Load data about selected plants into the matrix
                    oMatrixHelper.LoadDatabaseDataIntoMatrix("OSRN", "mtx_mselc", new List<VERSCI.Matrix.MatrixColumn>() { 
                                            new VERSCI.Matrix.MatrixColumn(){ ColumnName="UniqueID", Caption="ID", ColumnWidth=80, ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT  },
                                            new VERSCI.Matrix.MatrixColumn(){ ColumnName="itemName", Caption="Name", ColumnWidth=80, ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT  },
                                            new VERSCI.Matrix.MatrixColumn(){ ColumnName="LotNumber", Caption="Lot", ColumnWidth=80, ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT  },
                                            new VERSCI.Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Date Created", ColumnWidth=80, ItemType = SAPbouiCOM.BoFormItemTypes.it_EDIT  }
                                        }, SQL
                    );

                }
            }
			catch (System.ComponentModel.WarningException)
			{
				// message was already displayed
			}
			catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                //NSC_DI.UTIL.Misc.KillObject(IMG_Main);
                GC.Collect();
            }
        }

        private void btnRem(Form pForm)
        {
            SAPbouiCOM.Matrix mtx_mselc = null;
            try
            {
                // Grab the matrixes from the UI
                mtx_mselc = pForm.Items.Item("mtx_mselc").Specific;

                // Clear the selections in the selection matrix
                mtx_mselc.Clear();

                // Reload the main matrix
                Load_Matrix_Main();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        private void btnH2O(Form pForm)
        {
            SAPbouiCOM.Matrix mtx_mselc = null;
            try
            {
                var SelectedPlantIDs = GetSelectedPlants(pForm);
                               
                // Open the Watering Can with the selected plants.
                (new Production.F_WaterWizard(_SBO_Application, _SBO_Company)).Form_Load(true, SelectedPlantIDs.ToArray());//.ToUpper(),
            }
			catch (System.ComponentModel.WarningException)
			{
				// message was already displayed
			}
			catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        private void btnClone(Form pForm)
        {
            SAPbouiCOM.Matrix mtx_mselc = null;
            try
            {
                // Open the Watering Can with the selected plants.
                (new F_CloningWiz(_SBO_Application, _SBO_Company)).Form_Load();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        private void btnFeed(Form pForm)
        {
            SAPbouiCOM.Matrix mtx_mselc = null;
            try
            {
                var SelectedPlantIDs = GetSelectedPlants(pForm);

                // Open the Watering Can with the selected plants.
                Production.F_FeedWizard.Form_Load(true, SelectedPlantIDs.ToArray());
            }
			catch (System.ComponentModel.WarningException)
			{
				// message was already displayed
			}
			catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        private void btnPrune(Form pForm)
        {
            SAPbouiCOM.Matrix mtx_mselc = null;
            try
            {
                var SelectedPlantIDs = GetSelectedPlants(pForm);

                // Open the Watering Can with the selected plants.
                (new Production.F_WasteWizard(_SBO_Application, _SBO_Company)).Form_Load(true, SelectedPlantIDs.ToArray());
            }
			catch (System.ComponentModel.WarningException)
			{
				// message was already displayed
			}
			catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        private void btnTreat(Form pForm)
        {
            SAPbouiCOM.Matrix mtx_mselc = null;
            try
            {
                var SelectedPlantIDs = GetSelectedPlants(pForm);

                // Open the Watering Can with the selected plants.
                (new Production.F_TreatWizard(_SBO_Application, _SBO_Company)).Form_Load(SelectedPlantIDs.ToArray(), null, true);
            }
			catch (System.ComponentModel.WarningException)
			{
				// message was already displayed
			}
			catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }
    }
}