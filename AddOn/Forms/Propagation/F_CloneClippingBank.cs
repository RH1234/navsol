﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Propagation
{
    public class F_CloneClippingBank : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID	 = "NSC_CLONEBANK";
        private const string cPdoProcess = "CLONE";
        //private const string cItmGrpNam  = "Clone";
        //private const string cCloneItmGrpCode = "102";
        //Item group code for "Mature Plant"
        //private const string cPlantItmGrpCode = "107";



        private Dictionary<string, int> MatrixColumns = new Dictionary<string, int>()
        {
            {"ItemCode", 0}, 
            {"ItemName", 1}, 
            {"OnHand",   2}, 
            {"Quantity", 3},
            {"ProdBatchID", 4},
            {"WhsCode",  5},
            {"BatchNum", 6},
            {"MnfSerial",7},
            {"Bin",      8},
            {"CropID",   9},
            {"CropName", 10}

        };

        protected class CloneTransactionData
        {
            public List<CloneSelectedRow> RowsToProcess = new List<CloneSelectedRow>();

            public DateTime dueDate;
            public string projectBatchNo;

            // Newly created Production key. Assinged after creating the Production Order
            public int productionOrderKey;
        }

        protected class CloneSelectedRow
        {
            public string ItemCode;
            public string BaseItem;
            public double OnHand;
            public double Quantity;
            public string ProdBatchID;
			public string SourceWarehouseCode;
            public string BatchNo;
            public string DestinationWarehouseName;
            public string DestinationWarehouseCode;
            public string CropName;
            public string CropProjID;
        }

        // FOR OLD CODE
        public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
	    [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
	    public virtual bool OnBeforeItemPressed(ItemEvent pVal)
	    {
		    bool BubbleEvent = true;
		    Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


		    NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();

		    return BubbleEvent;
	    }
	    #endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
	    [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
	    public virtual void OnAfterItemPressed(ItemEvent pVal)
	    {
		    if (pVal.ActionSuccess == false) return;

		    var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

		    switch (pVal.ItemUID)
		    {
			    case "BTN_RELE":
				    BTN_RELE_Click(oForm);
				    break;

			    case "MTX_MAIN":
				    MTX_MAIN_Click(pVal);
				    break;
                case "BTN_LOOKUP":
                    BTN_LOOKUP_ITEM_PRESSED();
                    break;

            }

		    NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();
	    }

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (ChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_COMBO_SELECT
        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
        public virtual void OnAfterComboSelect(ItemEvent pVal)
        {
            if (pVal.InnerEvent == true) return;
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            _SBO_Form = oForm;
            if (pVal.ItemUID == "CBX_WHSE" || pVal.ItemUID == "cboPakWH") Load_Matrix();

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public F_CloneClippingBank() : this(Globals.oApp, Globals.oCompany) { }

		public F_CloneClippingBank(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

		private static void FormSetup(Form pForm)
		{
			Matrix oMat = null;

			try
			{
				// hide Production Order Type label and field - there is no "Grouped" PdOs
				pForm.Items.Item("TXT_DAYS").Click();
				pForm.Items.Item("Item_1").Visible = false;
				pForm.Items.Item("CMB_PORDER").Visible = false;

                // link the Drill Down Column to the correct form
                oMat = pForm.Items.Item("MTX_MAIN").Specific;
                CommonUI.Matrix.SetLink(oMat, 0, SAPbouiCOM.BoLinkedObject.lf_Items);

                //            oMat.Item.Height = 400;
                //            try
                //            {
                //                pForm.Items.Item("LBL_REM").Visible = true;
                //                pForm.Items.Item("TXT_REM").Visible = true;
                //            }
                //            catch
                //            {
                //                //XML Did not load correcly so build the fields on the form
                //                pForm.Items.Add("LBL_REM", BoFormItemTypes.it_STATIC);
                //                pForm.Items.Item("LBL_REM").Left    = 122;
                //                pForm.Items.Item("LBL_REM").Height  = 20;
                //                pForm.Items.Item("LBL_REM").Top     = 415;
                //                pForm.Items.Item("LBL_REM").Width   = 150;
                //                pForm.Items.Item("LBL_REM").Visible = true;
                //                SAPbouiCOM.StaticText oStatic       = pForm.Items.Item("LBL_REM").Specific;
                //                oStatic.Caption = "Remarks:";
                //                //Remarks Textbox
                //                pForm.Items.Add("TXT_REM", BoFormItemTypes.it_EXTEDIT);
                //                pForm.Items.Item("TXT_REM").Left    = 122;
                //                pForm.Items.Item("TXT_REM").Height  = 150;
                //                pForm.Items.Item("TXT_REM").Top     = 435;
                //                pForm.Items.Item("TXT_REM").Width   = 660;
                //                pForm.Items.Item("TXT_REM").Visible = true;

                //            }
                //            finally
                //            {
                //            }

                // hide the Siate ID fields
                pForm.Items.Item("TXT_DAYS").Click();
                pForm.Items.Item("CHK_STID").Visible    = false;
                pForm.Items.Item("LBL_SID").Visible     = false;
                pForm.Items.Item("TXT_LOOKUP").Visible  = false;
                pForm.Items.Item("BTN_LOOKUP").Visible  = false;
                
                // check the compliance type. if not METRC, then hide the Clone package fields
                if (NSC_DI.UTIL.Settings.Version.GetCompliance() == "METRC")
                {
                    var sql = $"SELECT WhsCode, WhsName FROM OWHS WHERE [U_{Globals.SAP_PartnerCode}_WhrsType] IN ('PRO', 'FIN') AND [Inactive] = 'N'";
                    if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})";                                       // #10823
                    CommonUI.ComboBox.LoadComboBoxSQL(pForm, "cboPakWH", sql);
                }
                else
                {
                    pForm.Items.Item("staPackage").Visible = false;
                    pForm.Items.Item("staPakWH").Visible = false;
                    pForm.Items.Item("cboPakWH").Visible = false;
                    pForm.Items.Item("staPakID").Visible = false;
                    pForm.Items.Item("txtPakID").Visible = false;

                    //NavSol.CommonUI.ComboBox.ComboBoxAdd_UDT_CN(pForm, "cboWaste", NSC_DI.Globals.tWasteTypes);
                }



                //((SAPbouiCOM.LinkedButton)(SAPbouiCOM.Column)oMat.Columns.Item(1)).LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items;
                //((SAPbouiCOM.LinkedButton)(SAPbouiCOM.Column)oMat.Columns.Item(0).ExtendedObject).LinkedObject = SAPbouiCOM.BoLinkedObject.lf_Items;


            }
            catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oMat);
				GC.Collect();
			}
		}

		public void Form_Load()
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_MAIN", @"clone-icon.bmp");

                Matrix MTX_MAIN = (Matrix) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

                MTX_MAIN.SelectionMode = BoMatrixSelect.ms_Auto;

                // Loading all Matrices. Passing true loads all three.
                Load_Matrix();

                // comment out Production Order - no "Grouped" PdOs
                // Preselect Individual Grouping of Production Orders.
                //SAPbouiCOM.ComboBox CMB_PORDER = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_PORDER", _SBO_Form);
                //CMB_PORDER.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);

				// Fill the ComboBox of Warehouses

				FormSetup(_SBO_Form);
                _SBO_Form.VisibleEx = true;

                if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y")
                {
                    NavSol.CommonUI.CFL.CreateWH(_SBO_Form, "CFL_WH", "CBX_WHSE");
                    var firstPhase = NSC_DI.SAP.Phase.GetPhases().First();
                    NavSol.CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_NONE, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, firstPhase.CurrentPhase);
                    NavSol.CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND, "Inactive", BoConditionOperation.co_EQUAL, "N");

                    NavSol.CommonUI.CFL.AddCon_Branches(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND);   //10823
                }
                else
                    Load_Combobox_Warehouses();

                _SBO_Form.Freeze(false);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
				_SBO_Form.Freeze(false);
				_SBO_Form.VisibleEx = true;

				//NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
				GC.Collect();
            }
        }

        private void MTX_MAIN_Click(ItemEvent pVal)
        {
            int clickedRowId = pVal.Row;
            if (clickedRowId <= 0) return;

            
            // Find the matrix in the form UI
            Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

            if (clickedRowId > MTX_MAIN.RowCount) return;

            try
            {
                string ItemCodeOfNewSelectedRow = ((EditText)MTX_MAIN.Columns.Item(MatrixColumns["ItemCode"]).Cells.Item(clickedRowId).Specific).Value;

                //// Determine if this key press was of the same Item Group we had selected previously.
                //bool deselectCurrent = false;
                //List<int> rowsToReselect = new List<int>();
                //for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
                //{
                //    string ItemCodeToCompare = ((SAPbouiCOM.EditText)MTX_MAIN.Columns.Item(MatrixColumns["ItemCode"]).Cells.Item(i).Specific).Value;
                //    // If the current row is now selected we can just continue looping.
                //    if (!MTX_MAIN.IsRowSelected(i) || i == clickedRowId)
                //        continue;

                //    rowsToReselect.Add(i);

                //    if (!ItemCodeOfNewSelectedRow.Equals(ItemCodeToCompare))
                //    {
                //        deselectCurrent = true;
                //    }
                //}

                //if (deselectCurrent)
                //{
                //    // Send a message to the client
                //    Globals.oApp.StatusBar.SetText(
                //        Text: "Item Code must be of the same type!",
                //        Seconds: SAPbouiCOM.BoMessageTime.bmt_Short,
                //        Type: SAPbouiCOM.BoStatusBarMessageType.smt_Error);

                //    MTX_MAIN.SelectRow(clickedRowId, false, false);
                //    return;
                //}

                // Grab the selected item in the Matrix
				string selectedItemCode = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(MTX_MAIN, "Item Code");

                // Prepare the SQL statement that will find the projected time via the item code
                string sql = @"SELECT [U_PrjctClone] FROM [OITM] JOIN [@"+NSC_DI.Globals.tStrains + @"] ON [OITM].[U_NSC_StrainID] = [Code] WHERE [OITM].[ItemCode] = '" + selectedItemCode + "'";


                // Grab the results from the SQL query
				Recordset ResultsFromSQL = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(sql);

                // Find the textbox for estimated days, package id, and the package WH combobox
                EditText TXT_DAYS = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_DAYS", _SBO_Form );
                EditText txtPakID = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txtPakID", _SBO_Form );
                ComboBox cboPakWH = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.ComboBox, "cboPakWH", _SBO_Form );
                
                // Set the estimated days if not using package section of form
                if(txtPakID.Value == "" && cboPakWH.Value == "")
                    TXT_DAYS.Value = ResultsFromSQL.Fields.Item(0).Value.ToString();

                if (pVal.ColUID == "col_4")
                    MTX_MAIN.Columns.Item("col_4").Cells.Item(clickedRowId).Specific.Active = true; // not sure why this is needed, but the active field kept moving
                else
                    ((EditText)MTX_MAIN.Columns.Item(MatrixColumns["Quantity"]).Cells.Item(clickedRowId).Specific).Active = true;
            }
			catch (Exception ex)
            {
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
        }

	    private void Load_Combobox_Warehouses()
	    {
		    try
		    {
			    // Get the first phase of config CSV
			    var firstPhase = NSC_DI.SAP.Phase.GetPhases().First();

			    // Prepare to run a SQL statement.
			    SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                // Count how many current records exist within the database.
                var sql = @"SELECT WhsCode, WhsName FROM OWHS WHERE [U_NSC_WhrsType] = '" + (new SqlParameter("WhrsType", firstPhase.CurrentPhase)).Value.ToString() + "' AND [Inactive] = 'N'";
                if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})";                                       // #10823

                oRecordSet.DoQuery(sql);
            

			    // Find the Combobox of Warehouses from the Form UI
			    ComboBox CBX_WHSE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CBX_WHSE", _SBO_Form) as ComboBox;

                CBX_WHSE.Item.DisplayDesc = true;

                // If items already exist in the drop down
                if (CBX_WHSE.ValidValues.Count > 0)
			    {
				    // Remove all currently existing values from warehouse drop down
				    for (int i = CBX_WHSE.ValidValues.Count; i-- > 0;)
				    {
					    CBX_WHSE.ValidValues.Remove(i, BoSearchKey.psk_Index);
				    }
			    }

			    // If more than 1 warehouses exists
			    if (oRecordSet.RecordCount > 1)
			    {
				    // Create the first item as an empty item
				    CBX_WHSE.ValidValues.Add("", "");

				    // Select the empty item (forcing the user to make a decision)
				    CBX_WHSE.Select(0, BoSearchKey.psk_Index);
			    }

			    // Add allowed warehouses to the drop down
			    for (int i = 0; i < oRecordSet.RecordCount; i++)
			    {
					//try
					//{
					    CBX_WHSE.ValidValues.Add(oRecordSet.Fields.Item(0).Value.ToString(), oRecordSet.Fields.Item(1).Value.ToString());
					//}
					//catch
					//{
					//}
				    CBX_WHSE.Item.Enabled = true;
				    oRecordSet.MoveNext();
			    }

			    // Auto select our warehouse if we only have one and Disable the control
			    if (oRecordSet.RecordCount == 1)
			    {
				    CBX_WHSE.Active = false;
				    CBX_WHSE.Item.Enabled = false;
				    CBX_WHSE.Select(0, BoSearchKey.psk_Index);
			    }
            }
		    catch (Exception ex)
		    {
			    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    GC.Collect();
		    }
	    }

	    private void BTN_RELE_Click(Form pForm)
	    {
		    try
		    {
                // no "Grouped" PdOs
                //// Grab the textbox for days from the form
                //ComboBox CMB_PORDER = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_PORDER", pForm);

			    //if (string.IsNullOrEmpty(CMB_PORDER.Value))
			    //{
				   // // Send a message to the client
				   // Globals.oApp.StatusBar.SetText("Please select how you'd like to group your production orders.");

				   // // Focus the cursor in the offending input
				   // CMB_PORDER.Active = true;
				   // return;
			    //}

				if(pForm.Items.Item("MTX_MAIN").Specific.GetNextSelectedRow() < 0)
				{
					Globals.oApp.StatusBar.SetText("At least one row must be selected.");
					return;
				}

                var CBX_WHSE = pForm.Items.Item("CBX_WHSE").Specific.Value;
                var TXT_DAYS = pForm.Items.Item("TXT_DAYS").Specific.Value;
                var cboPakWH = pForm.Items.Item("cboPakWH").Specific.Value;
                var txtPakID = pForm.Items.Item("txtPakID").Specific.Value;
                var process = (cboPakWH != "" || txtPakID != "") ? "Package" : "Propagation";

                // have to enter values for either Propagation or Pagkage - not both
                if ((CBX_WHSE != "" || TXT_DAYS != "") && (cboPakWH != "" || txtPakID != ""))
                {
                    Globals.oApp.MessageBox("Enter data for either Propagation or Package. Not both.");
                    return;
                }

                if (process == "Package" && (cboPakWH == "" || txtPakID == ""))
                {
                    Globals.oApp.MessageBox("Enter data for both the Destination Warehouse and the Compliance Package ID.");
                    return;
                }

                if (process == "Propagation" && CBX_WHSE == "")
                {
                    Globals.oApp.MessageBox("Select a location.");
                    return;
                }

                SAPbouiCOM.EditText oEdit = pForm.Items.Item("TXT_REM").Specific;
                string Remarks = oEdit.Value;

                // no "Grouped" PdOs
       //         switch (CMB_PORDER.Value.ToString())
			    //{
				    //case "Individual":
					    CreateIndividualProductionOrders(pForm, cboPakWH, txtPakID, Remarks);
                        oEdit = pForm.Items.Item("TXT_REM").Specific;
                        oEdit.Value="";
       //                 break;
				   // case "Grouped":		// not currently used
					  //  CreateGroupedProductionOrder();
					  //  break;
			    //}
		    }
		    catch (System.ComponentModel.WarningException)
		    {
			    // message was already displayed
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    ////NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

		private void CreateIndividualProductionOrders(Form pForm, string pPakWH, string pPakID, string pRemarks)
		{
			try
			{
                CloneTransactionData cloneTransDetails = PullCloneDetailsFromForm((pPakID != ""));
				if (cloneTransDetails == null) return;
                Forms.Crop.CropCycleManagement oCrop = new Crop.CropCycleManagement();

				bool bUpdatedProdO = false;
                var errorMsg = "";
				foreach (CloneSelectedRow row in cloneTransDetails.RowsToProcess)
				{
					if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                    //--------------------------------------------
                    // PRODUCTION ORDER

                    // check for clone packages
                    var pdoProcess = cPdoProcess;
                    if (pPakID != "")
                    {                      
                        pdoProcess = "plantbatches_createpackages";
                        var strainID = NSC_DI.SAP.Items.GetStrainID(row.ItemCode);
                        var itemCodeSQL = $"SELECT ItemCode FROM OITM WHERE OITM.QryGroup37 = 'Y' AND U_NSC_StrainID = '{strainID}'";                        
                        row.ItemCode = NSC_DI.UTIL.SQL.GetValue<string>(itemCodeSQL, "");
                        if (row.ItemCode == "")
                        {
                            errorMsg += $"No Packaged Clone Item found for strain {strainID}, Check Item Property 37." + Environment.NewLine;
                            continue;
                        }
                    }

                    // get the destination WH
                    string destWH = GetDestWH(pForm);               // #10824
                    var PdOKey = NSC_DI.SAP.ProductionOrder.CreateFromBOM(row.ItemCode, row.Quantity, DateTime.Now, destWH, true, "N", pdoProcess, pRemarks, row.ProdBatchID);
					if (PdOKey == 0)
					{
						Globals.oApp.MessageBox("Failed to create Production Order for batch " + row.BatchNo + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
						continue;
					}

					//Check the warehouse for the Clone(s) item(s) on tbe Production BOM and if needed adjust accordingly
					//Get clone Warehouses for selected items on the Matrix by batchnumber 
					SAPbobsCOM.ProductionOrders oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
					oPdO.GetByKey(PdOKey);
					SAPbobsCOM.ProductionOrders_Lines oLines = oPdO.Lines;

					//Update Destination Warehouse.
					if (row.DestinationWarehouseCode != oPdO.Warehouse)
					{
						oPdO.Warehouse = row.DestinationWarehouseCode;
                        NSC_DI.SAP.Document.SetDefaultDistributionRules(oLines);
                        bUpdatedProdO = true;
					}
                    //Update line Item warehouse
                    string strCropFinHolder = null;
                    if (row.CropName.Length > 0)
                    {
                        bUpdatedProdO = true;
                        strCropFinHolder = oCrop.GetFinProj(row.CropProjID);
                        oPdO.Project = strCropFinHolder;

                    }
                    for (int i = 0; i <= oLines.Count - 1; i++)
					{
						oLines.SetCurrentLine(i);
                        if (row.BaseItem == oLines.ItemNo)
                        {
                            string strWhsSourceHolder = NSC_DI.UTIL.SQL.GetValue<string>(@"select WhsCode from [OWHS] where OWHS.WhsName = '" + row.SourceWarehouseCode + "'", null);
                            if (strWhsSourceHolder != oLines.Warehouse && oLines.PlannedQuantity > 0)
                            {
                                //Update warehouse
                                oLines.Warehouse = strWhsSourceHolder;
                                NSC_DI.SAP.Document.SetDefaultDistributionRules(oLines);
                                bUpdatedProdO = true;
                            }
                        }
                        if (row.CropName.Length > 0)
                        {
                            //oLines.StageID = oCrop.GetStageID("PROP_CLONE_" + row.CropName.ToString());
                            oLines.Project = strCropFinHolder;
                            bUpdatedProdO = true;
                        }
                    }

                    if (bUpdatedProdO == true)
					{
						oPdO.Update();
					}

                    //--------------------------------------------
                    // ISSUE TO PRODUCTION ORDER
                    //oCrop.GetFinProj(row.CropName)
                    string itemProp = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT QryGroup41 FROM OITM WHERE ItemCode = '{row.BaseItem}'").ToUpper() == "Y"? "Clone" : "Plant Tissue";
                    var PdOIssueKey = NSC_DI.SAP.ProductionOrder.Issue(PdOKey, itemProp, row.Quantity, row.BatchNo, null, strCropFinHolder);
					if (PdOIssueKey == 0)
					{
						Globals.oApp.MessageBox("Failed to Issue to Production for batch " + row.BatchNo + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
						continue;
					}

                    //--------------------------------------------
                    // RECEIVE FROM PRODUCTION ORDER - only for clone packages
                    var PdOReceiptKey = 0;
                    if (pPakID != "")
                    {
                        var batchID = NSC_DI.SAP.BatchItems.NextBatch(row.ItemCode);
                        PdOReceiptKey = NSC_DI.SAP.ProductionOrder.ReceiveParent(PdOKey, DateTime.Today, batchID, null, row.Quantity);                        
                        if (PdOReceiptKey == 0)
                        {
                            Globals.oApp.MessageBox("Failed to receive from Production for batch " + row.BatchNo + Environment.NewLine + Globals.oCompany.GetLastErrorDescription());
                            continue;
                        }
                        NSC_DI.SAP.BatchItems.setAttribute1(batchID, row.ItemCode, row.BatchNo);
                        NSC_DI.SAP.BatchItems.setAttribute2(batchID, row.ItemCode, pPakID);
                        NSC_DI.SAP.ProductionOrder.UpdateStatus(PdOKey, BoProductionOrderStatusEnum.boposClosed);
                    }
                    

                    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                    string strStageID = "PROP_CLONE_" + row.CropProjID.ToString();
                    int intDocNum = NSC_DI.UTIL.SQL.GetValue<int>("Select DocNum from OWOR where DocEntry=" + PdOKey.ToString());
                    if (row.CropProjID.ToString().Length > 0 && (pPakID == ""))  // clone packages are not supposed to have crop cycles
                    {
                        oCrop.AddProdToProject(PdOKey, intDocNum, row.CropProjID, strStageID);
                        oCrop.AddDocToProject_Issue(PMDocumentTypeEnum.pmdt_GoodsIssue, PdOIssueKey.ToString(), row.CropProjID, strStageID);
                        //oCrop.AddDocToProject_Receipts(PMDocumentTypeEnum.pmdt_GoodsReceipt, PdOReceiptKey.ToString(), row.CropProjID, strStageID);
                    }
                }
                if (errorMsg != "")
                    Globals.oApp.MessageBox(errorMsg);

                Load_Matrix();

				Globals.oApp.StatusBar.SetText("Complete.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
            finally
            {
				if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                GC.Collect();
            }
        }
    private void BTN_LOOKUP_ITEM_PRESSED()
        {
            try
            {
                // Grad the textbox for the barcode scanner from the form UI
                EditText TXT_LOOKUP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", _SBO_Form);

                // If nothing was passed in the textbox, just return
                if (TXT_LOOKUP.Value.Length == 0)
                {
                    Globals.oApp.StatusBar.SetText("No Data To Scan! - Please Enter A Valid Value.");
                    return;
                }

                // Grab the matrix from the form UI
                Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form) as Matrix;
                CheckBox oChkBox_Assign_StateID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_STID", _SBO_Form) as CheckBox;
                bool Add_State_ID = oChkBox_Assign_StateID.Checked;
                if (Add_State_ID == true)
                {
                    bool bRowIsSelected = false;
                    int intSelectedRowCount = 0;
                    int intLastRowIndex = 0;
                    //Add Code to Validate Single row selection
                    for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
                    {
                        bRowIsSelected = MTX_MAIN.IsRowSelected(i);
                        if (bRowIsSelected == true)
                        {
                            intLastRowIndex = i;
                            intSelectedRowCount++;
                            bRowIsSelected = false;
                        }
                    }
                    if (intSelectedRowCount > 1)
                    {
                        //Show error message to indicate you can only select one row when assigning StateId
                        Globals.oApp.StatusBar.SetText("You may only select one row when assigning a StateID");
                        return;
                    }
                    else if(intSelectedRowCount==0)
                    {
                        //Show error message to indicate you can only select one row when assigning StateId
                        Globals.oApp.StatusBar.SetText("You select a row when assigning a StateID");
                        return;
                    }
                    else
                    {
                        //Check if ID already exist --, and (assign ID or Overwrite Existing)
                        string StateID = ((EditText)MTX_MAIN.Columns.Item("col_"+ MatrixColumns["MnfSerial"].ToString()).Cells.Item(intLastRowIndex).Specific).Value;
                        string DistNumber = ((EditText)MTX_MAIN.Columns.Item("col_" + MatrixColumns["BatchNum"].ToString()).Cells.Item(intLastRowIndex).Specific).Value;
                        SAPbobsCOM.BatchNumberDetail oBatchDetails = null;
                        CompanyService oCoService = Globals.oCompany.GetCompanyService() as CompanyService;
                        BatchNumberDetailsService oBatchService = oCoService.GetBusinessService(ServiceTypes.BatchNumberDetailsService);
                        BatchNumberDetailParams oParams = oBatchService.GetDataInterface(BatchNumberDetailsServiceDataInterfaces.bndsBatchNumberDetailParams);

                        try
                        {
                            if (StateID.Trim().Length == 0)
                            {
                                // RH commented out the lines
                                ////Add field in DB
                                ////Get and Set the DocEntry
                                ////***To Do: Up date this so that it is pulling on the correct DistNumber once Randy adjust this you will need to
                                ////Uncomment the line below and delete the current select State used for testing. 
                                //int intIndexHolder = DistNumber.IndexOf('-');
                                //DistNumber = DistNumber.Remove(0, intIndexHolder+1);
                                //intIndexHolder = DistNumber.IndexOf('-');
                                //DistNumber = DistNumber.Remove(0, intIndexHolder + 1);
                                string strSQL = string.Format(@"Select AbsEntry From [OBTN] where DistNumber='{0}'", DistNumber);
                                int DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                                oParams.DocEntry = DocEntry;
                                oBatchDetails = oBatchService.Get(oParams);
                                oBatchDetails.BatchAttribute1 = TXT_LOOKUP.Value;
                                oBatchService.Update(oBatchDetails);
                                //Success Reload Matrix
                                _SBO_Form.Freeze(true);
                                Load_Matrix();
                                // Empty the barcode scanner field
                                TXT_LOOKUP.Value = "";
                                // Focus back into the barcode scanner's text field
                                TXT_LOOKUP.Active = true;
                                //Move to next Row if not last row
                                if (MTX_MAIN.RowCount != intLastRowIndex)
                                {
                                    MTX_MAIN.SelectRow(intLastRowIndex + 1, true, true);
                                }
                            }
                            else
                            {
                                //Update field in DB
                                int iReturnVal = Globals.oApp.MessageBox("A State ID already exists for this item would you like to replace it?", 2, "Cancel", "OK");
                                switch (iReturnVal)
                                {
                                    case 1:
                                        //Cancel was selected do nothing                                         
                                        break;
                                    case 2:
                                        //Update the field in DB
                                        //Get and Set the DocEntry
                                        //***To Do: Up date this so that it is pulling on the correct DistNumber once Randy adjust this you will need to
                                        //Uncomment the line below and delete the current select State used for testing. 
                                        //int intIndexHolder = DistNumber.IndexOf('-');
                                        //DistNumber = DistNumber.Remove(0, intIndexHolder + 1);
                                        //intIndexHolder = DistNumber.IndexOf('-');
                                        //DistNumber = DistNumber.Remove(0, intIndexHolder + 1);
                                        string strSQL = string.Format(@"Select AbsEntry From [OBTN] where DistNumber='{0}'", DistNumber);
                                        int DocEntry = NSC_DI.UTIL.SQL.GetValue<int>(strSQL);

                                        oParams.DocEntry = DocEntry;
                                        oBatchDetails = oBatchService.Get(oParams);
                                        oBatchDetails.BatchAttribute1 = TXT_LOOKUP.Value;
                                        oBatchService.Update(oBatchDetails);
                                        //Success Reload Matrix
                                        _SBO_Form.Freeze(true);
                                        Load_Matrix();
                                        // Empty the barcode scanner field
                                        TXT_LOOKUP.Value = "";
                                        // Focus back into the barcode scanner's text field
                                        TXT_LOOKUP.Active = true;
                                        //Move to next Row if not last row
                                        if (MTX_MAIN.RowCount != intLastRowIndex)
                                        {
                                            MTX_MAIN.SelectRow(intLastRowIndex + 1, true, true);
                                        }
                                        break;
                                }

                            }

                            //Update Matrix cell with data or Reload Matrix
                        }
                        catch (Exception ex)
                        {
                            Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                        }
                        finally
                        {
                            //Clean up the objects and Garbage Collect
                            _SBO_Form.Freeze(false);
                            NSC_DI.UTIL.Misc.KillObject(oBatchDetails);
                            NSC_DI.UTIL.Misc.KillObject(oCoService);
                            NSC_DI.UTIL.Misc.KillObject(oBatchService);

                            GC.Collect();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }
        private void CreateGroupedProductionOrder()
	    {
		    // Pull all the form data that is required.
		    CloneTransactionData cloneTransDetails = PullCloneDetailsFromForm();
		    if (cloneTransDetails == null)
			    return;

		    double TotalProcessed = 0;

		    if (cloneTransDetails.RowsToProcess.Count <= 0)
		    {
			    // Send success message to the client
			    Globals.oApp.StatusBar.SetText("Failed to Release and Issued all Selected Items!");

			    return;
		    }

		    #region Create GoodsIssued and Disassembly Production Order Data Lists

		    string ItemCode = "";
		    string DestinationWarehouseCode = "";
		    string SourceWarehouseCode = "";
		    string ProjectedBatchNumber = "";
		    int TotalQuantity = 0;
		    var newPDOKey = 0;

		    // Prepare a list of Goods Issued Lines
		    List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines> ListOfGoodsIssuedLines = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();

		    // Prepare a list of line items for the "Production Order"
		    List<NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem> components =
			    new List<NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem>();

		    foreach (CloneSelectedRow row in cloneTransDetails.RowsToProcess)
		    {
			    // Add a new item to the list of Goods Issued Lines
			    ListOfGoodsIssuedLines.Add(
			                               new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
			                               {
				                               ItemCode = row.ItemCode,
				                               Quantity = row.Quantity,
				                               WarehouseCode = row.SourceWarehouseCode,
				                               BatchLineItems = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>()
				                               {
					                               new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches() {BatchNumber = row.BatchNo, Quantity = row.Quantity}
				                               }
			                               }
				    );

			    ItemCode = row.ItemCode;
			    DestinationWarehouseCode = row.DestinationWarehouseCode;
			    SourceWarehouseCode = row.SourceWarehouseCode;
			    ProjectedBatchNumber = row.BatchNo;
			    TotalQuantity += (int) row.Quantity;
		    }


		    foreach (var item in NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(ItemCode))
		    {
			    components.Add(
			                   new NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem()
			                   {
				                   ItemCodeForItemBeingCreated = item.ItemCode,
				                   DestinationWarehouseCode = DestinationWarehouseCode,
				                   BaseQuantity = TotalQuantity,
				                   PlannedQuantity = TotalQuantity
			                   }
				    );
		    }

		    #endregion

		    // Create a Disassembly Production Order based on the form
		    newPDOKey = NSC_DI.SAP.ProductionOrder_OLD.CreateDisassemblyProductionOrder(ItemCode
			    , TotalQuantity
			    , SourceWarehouseCode
			    , DateTime.Now
			    , components
			    , ProjectedBatchNumber);

		    // Change status to released
		    NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(newPDOKey, BoProductionOrderStatusEnum.boposReleased);

		    // Create an "Issue From Production" document
		    NSC_DI.SAP.IssueFromProduction.Create(newPDOKey, DateTime.Now, ListOfGoodsIssuedLines);
		    // Send success message to the client
		    Globals.oApp.StatusBar.SetText("Successfully Release and Issued the Selected Items!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

		    this.Load_Matrix();
	    }

	    /// <summary>
        /// Get all data from the SeedBank form that is required.
        /// </summary>
        /// <returns>CloneTransactionData Object</returns>
        private CloneTransactionData PullCloneDetailsFromForm(bool pClonePackage = false)
        {
            CloneTransactionData cloneDetails = new CloneTransactionData();

            // Grab the matrix from the form ui
            Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

            // Grab the textbox for days from the form
			EditText TXT_DAYS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DAYS", _SBO_Form);

            int daysToGerminate;
            if (pClonePackage == false && !int.TryParse(TXT_DAYS.Value, out daysToGerminate))
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Please enter in a number for the days!");

                // Focus the cursor in the offending input
                TXT_DAYS.Active = true;
                return null;
            }

            // For each row already selected
            for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
            {
                CloneSelectedRow newRow = new CloneSelectedRow();

                try
                {
                    // If the current row is now selected we can just continue looping.
                    if (!MTX_MAIN.IsRowSelected(i))
                        continue;

                    //Remove the Leading "CL-" to get the the base item to be created in the new production order - The Cannabis Plant itself 

                    // fix the case where an item is created manually - example: they copied CL-H-3 to CL-PG. 
                    // commented out the change. If they create to correct Items, the chnge is not needed.
                    newRow.ItemCode = ((EditText)MTX_MAIN.Columns.Item(MatrixColumns["ItemCode"]).Cells.Item(i).Specific).Value.Remove(0,3);
                    //newRow.ItemCode = ((EditText)MTX_MAIN.Columns.Item(MatrixColumns["ItemCode"]).Cells.Item(i).Specific).Value;;
                    //if (NSC_DI.SAP.Items.GetField<string>(newRow.ItemCode, "U_NSC_StrainID") == newRow.ItemCode.Remove(0, 3)) newRow.ItemCode = newRow.ItemCode.Remove(0, 3);
                    newRow.BaseItem = ((EditText)MTX_MAIN.Columns.Item(MatrixColumns["ItemCode"]).Cells.Item(i).Specific).Value;
                    newRow.SourceWarehouseCode = ((EditText)MTX_MAIN.Columns.Item(MatrixColumns["WhsCode"]).Cells.Item(i).Specific).Value;
					newRow.OnHand = Convert.ToDouble(((EditText)MTX_MAIN.Columns.Item(MatrixColumns["OnHand"]).Cells.Item(i).Specific).Value);
                    newRow.BatchNo = ((EditText)MTX_MAIN.Columns.Item(MatrixColumns["BatchNum"]).Cells.Item(i).Specific).Value;
                    string strBin  =   ((EditText)MTX_MAIN.Columns.Item(MatrixColumns["Bin"]).Cells.Item(i).Specific).Value;
                    newRow.CropProjID = ((EditText)MTX_MAIN.Columns.Item(MatrixColumns["CropID"]).Cells.Item(i).Specific).Value;
                    newRow.CropName = ((EditText)MTX_MAIN.Columns.Item(MatrixColumns["CropName"]).Cells.Item(i).Specific).Value;

                    newRow.ProdBatchID = ((EditText)MTX_MAIN.Columns.Item(MatrixColumns["ProdBatchID"]).Cells.Item(i).Specific).Value;
                    if (NSC_DI.UTIL.Settings.Version.GetCompliance() == "METRC" && (string.IsNullOrEmpty(newRow.ProdBatchID?.Trim())))// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(newRow.ProdBatchID)))
                    {
                        Globals.oApp.MessageBox($"All selected rows require a value for 'Prod Batch ID'");
                        return null;
                    }

                    // Keep track of which warehouse was selected
                    string SelectedDestinationWarehouseID = "";

                    double parsedQuantity;
                    if (!double.TryParse(((EditText)MTX_MAIN.Columns.Item(MatrixColumns["Quantity"]).Cells.Item(i).Specific).Value, out parsedQuantity))
                    {
                        // Send a message to the client
                        Globals.oApp.StatusBar.SetText("Please enter in a number for the quantity!");

                        // Focus the cursor in the offending input
                        ((EditText)MTX_MAIN.Columns.Item(MatrixColumns["Quantity"]).Cells.Item(i).Specific).Active = true;

                        return null;
                    }

                    newRow.Quantity = parsedQuantity;

                    // Prepare a ComboBox helper
					VERSCI.ComboBox ComboBox_Helper = new VERSCI.ComboBox(Globals.oApp, Globals.oCompany, _SBO_Form);

                    // Find the ComboBox in the Form UI
                    //ComboBox CBX_WHSE = ((ComboBox)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CBX_WHSE", _SBO_Form));

                    // Grab the value from the ComboBox
                    //newRow.DestinationWarehouseName = CBX_WHSE.Value.ToString();

                    //// Get the "description" from the ComboBox, where we are really storing the value
                    newRow.DestinationWarehouseCode = CommonUI.Forms.GetField<string>(_SBO_Form, "CBX_WHSE");
                    newRow.DestinationWarehouseName = NSC_DI.SAP.Warehouse.GetName(newRow.DestinationWarehouseCode);

                    // Add a row to our Transfer list.
                    cloneDetails.RowsToProcess.Add(newRow);
                }
				catch (Exception ex)
				{
					throw new Exception(NSC_DI.UTIL.Message.Format(ex));
				}
			}

            return cloneDetails;
        }

		private void Load_Matrix()
		{
            // #10823 --->
            // limit the list to items in ths pecific branch
            var defBranch   = Globals.BranchDflt;         //  -1 is no branches, 0 is no default branch
            var brList      = Globals.BranchList;          
            var destWH      = GetDestWH(_SBO_Form);
            if (destWH != "" && defBranch >= 0)
            {
                defBranch = NSC_DI.SAP.Branch.Get(destWH);
                brList = NSC_DI.SAP.Branch.GetAll_User_Str(defBranch);
            }
            // #10823 <---


            //            var sql = $@"
            //SELECT T0.ItemCode,T0.itemName,T2.Quantity as OnHand, T2.Quantity, T3.WhsCode,T3.WhsName,T0.DistNumber as BatchNum, T0.MnfSerial from OBTN T0
            //                    join OBTW T1
            //                    on T0.AbsEntry=T1.MdAbsEntry

            //                    join OBTQ T2
            //                    on T0.AbsEntry=T2.MdAbsEntry and T1.AbsEntry=T2.AbsEntry

            //                    Join OWHS T3
            //                    on T2.WhsCode=T3.WhsCode

            //                    where T3.U_NSC_WhrsType = 'CLO'
            //                    AND 
            //                    T2.Quantity>0";

            //            var sql = $@"
            //SELECT T0.ItemCode,T0.ItemName,T2.Quantity AS OnHand, OBBQ.OnHandQty AS Quantity, T3.WhsCode,T3.WhsName,T0.DistNumber AS BatchNum, T0.MnfSerial
            //       ,BinCode AS Bin
            //  FROM OBTN T0
            //  JOIN OBTW T1 ON T0.AbsEntry=T1.MdAbsEntry
            //  JOIN OBTQ T2 ON T0.AbsEntry=T2.MdAbsEntry AND T1.AbsEntry=T2.AbsEntry
            //  JOIN OWHS T3 ON T2.WhsCode=T3.WhsCode
            //  LEFT JOIN OBBQ ON OBBQ.SnBMDAbs = T2.MdAbsEntry
            //  LEFT JOIN OBIN ON OBIN.AbsEntry = OBBQ.BinAbs
            // WHERE T3.U_NSC_WhrsType = 'CLO' AND T2.Quantity > 0";

            var sql = $@"
SELECT OBTN.ItemCode, OBTN.itemName, OBTQ.Quantity AS OnHand, ISNULL(OBBQ.OnHandQty, OBTQ.Quantity) AS Quantity, SPACE(50) AS [ProdBatchID],
       OWHS.WhsCode, OWHS.WhsName, 
       OBTN.DistNumber AS BatchNum, OBTN.MnfSerial, OBIN.BinCode AS Bin, OBTN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]  
  FROM OBTQ 
 INNER JOIN OBTN ON OBTQ.MdAbsEntry = OBTN.AbsEntry 
 INNER JOIN OWHS ON OBTQ.WhsCode = OWHS.WhsCode
 LEFT JOIN OITM ON OBTN.ItemCode = OITM.ItemCode
  LEFT JOIN OBBQ ON OBTQ.ItemCode = OBBQ.ItemCode AND OBTQ.WhsCode = OBBQ.WhsCode AND OBTQ.MdAbsEntry = OBBQ.SnBMDAbs
  LEFT JOIN OBIN ON OBIN.AbsEntry = OBBQ.BinAbs
  LEFT Join OPMG on OPMG.AbsEntry = OBTN.U_NSC_CropID
  --LEFT JOIN OBPL ON OWHS.BPLid = OBPL.BPLid                                  -- // #10823
 WHERE OWHS.U_NSC_WhrsType = 'CLO' AND OBTQ.Quantity > 0 AND OITM.QryGroup37 = 'N'";
            if (defBranch >= 0) sql += $" AND OWHS.BPLid IN ({brList})";        // #10823

            // Prepare a Matrix helper
            VERSCI.Matrix MatrixHelper = new VERSCI.Matrix(SAPBusinessOne_Application: Globals.oApp, FormToUse: _SBO_Form);

            // Load the matrix of tasks
            MatrixHelper.LoadDatabaseDataIntoMatrix(DatabaseTableName: "OIBT", MatrixUID: "MTX_MAIN",
                ListOfColumns:
                    new List<VERSCI.Matrix.MatrixColumn>() {
                             new VERSCI.Matrix.MatrixColumn(){ Caption="Item Code",         ColumnWidth=40, ColumnName="ItemCode", ItemType= BoFormItemTypes.it_LINKED_BUTTON }
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Item Name",         ColumnWidth=80, ColumnName="ItemName", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="On Hand",           ColumnWidth=80, ColumnName="OnHand", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Quantity to Use",   ColumnWidth=80, ColumnName="Quantity", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Prod Batch ID",     ColumnWidth=80, ColumnName="ProdBatchID", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}
                            //,new VERSCI.Matrix.MatrixColumn(){ Caption="Source Warehouse",  ColumnWidth=0, ColumnName="WhsCode", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Source Warehouse",  ColumnWidth=80, ColumnName="WhsName", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Batch Number",      ColumnWidth=80, ColumnName="BatchNum", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ ColumnName="MnfSerial",      Caption="State ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                            ,new VERSCI.Matrix.MatrixColumn(){ ColumnName="Bin", Caption="Bin", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                            ,new VERSCI.Matrix.MatrixColumn(){ ColumnName="CropID", Caption="Crop ID", ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                            ,new VERSCI.Matrix.MatrixColumn(){ ColumnName="CropName", Caption="Crop Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },

                    },

                    SQLQuery: sql);

            // TODO: **** POSSIBLE ERROR IN SQL T3.U_NSC_WhrsType = 'CLO' ^ . NEEDS TO BE IN A TABLE

            // Find the matrix in the form UI
            Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

            //MTX_MAIN.Columns.Item("WhsCode").Visible = false;

            // check compliance system - only revelant for METRC
            MTX_MAIN.Columns.Item(4).Visible = (NSC_DI.UTIL.Settings.Version.GetCompliance() == "METRC");

            MTX_MAIN.SelectionMode = BoMatrixSelect.ms_Auto;
            //MTX_MAIN.AddRow();        // causes the last row to duplicate

            MTX_MAIN.AutoResizeColumns();
        }

        private void CreateIndividualProductionOrders_DISASSEMBLY()
		{
			var newPDOKey = 0;

			// Pull all data from the selected columns.
			CloneTransactionData cloneTransDetails = PullCloneDetailsFromForm();
			if (cloneTransDetails == null) return;

			bool AllWetBudPassed = true;
			double TotalProcessed = 0;

			if (cloneTransDetails.RowsToProcess.Count <= 0)
			{
				// Send success message to the client
				Globals.oApp.StatusBar.SetText("Failed to Release and Issued all Selected Items!");
				throw new System.ComponentModel.WarningException();
			}

			foreach (CloneSelectedRow row in cloneTransDetails.RowsToProcess)
			{
				// Prepare a list of line items for the "Production Order"
				List<NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem> components = new List<NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem>();

				foreach (var item in NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(row.ItemCode))
				{
					components.Add(
						new NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem()
						{
							ItemCodeForItemBeingCreated = item.ItemCode,
							DestinationWarehouseCode = row.DestinationWarehouseCode,
							BaseQuantity = item.Quantity,
							PlannedQuantity = row.Quantity
						}
					);
				}

				try
				{
					// Create a Disassembly Production Order based on the form
					newPDOKey = NSC_DI.SAP.ProductionOrder_OLD.CreateDisassemblyProductionOrder(row.ItemCode
						, row.Quantity
						, row.SourceWarehouseCode
						, DateTime.Now
						, components
						, row.BatchNo);

				}
				catch (Exception ex)
				{
					// Send a message to the client
					Globals.oApp.StatusBar.SetText("Failed to create production order!");
					AllWetBudPassed = false;
					continue;
				}

				// Change status to released
				NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(newPDOKey, BoProductionOrderStatusEnum.boposReleased);

				// Prepare a list of Goods Issued Lines
				List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines> ListOfGoodsIssuedLines = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();

				// Add a new item to the list of Goods Issued Lines
				ListOfGoodsIssuedLines.Add(
					new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
					{
						ItemCode = row.ItemCode,
						Quantity = row.Quantity,
						WarehouseCode = row.SourceWarehouseCode,
						BatchLineItems = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>()
						{
							new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches() {BatchNumber = row.BatchNo, Quantity = row.Quantity}
						}
					}
				);

				try
				{
					// Create an "Issue From Production" document
					NSC_DI.SAP.IssueFromProduction.Create(newPDOKey, DateTime.Now, ListOfGoodsIssuedLines);
					TotalProcessed += row.Quantity;
				}
				catch (Exception ex)
				{
					AllWetBudPassed = false;
				}
			}

			if (AllWetBudPassed)
			{
				// Send success message to the client
				Globals.oApp.StatusBar.SetText("Successfully Release and Issued the Selected Items!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
			}
			else
			{
				if (TotalProcessed > 0)
				{
					// Send success message to the client
					Globals.oApp.StatusBar.SetText("Failed to Release and Issued some of the Selected Items!");
				}
				else
				{
					// Send success message to the client
					Globals.oApp.StatusBar.SetText("Failed to Release and Issued all Selected Items!");
				}
			}
			this.Load_Matrix();
		}

		private void Load_Matrix_OLD()
        {
            // Prepare a Matrix helper
			VERSCI.Matrix MatrixHelper = new VERSCI.Matrix(SAPBusinessOne_Application: Globals.oApp, FormToUse: _SBO_Form);

            // Load the matrix of tasks
            MatrixHelper.LoadDatabaseDataIntoMatrix(
               DatabaseTableName: "OIBT",
                MatrixUID: "MTX_MAIN",
                ListOfColumns:
					new List<VERSCI.Matrix.MatrixColumn>() {
                            new VERSCI.Matrix.MatrixColumn(){ Caption="Item Code", ColumnWidth=40, ColumnName="ItemCode", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Item Name", ColumnWidth=80, ColumnName="ItemName", ItemType = BoFormItemTypes.it_EDIT}               
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="On Hand", ColumnWidth=80, ColumnName="OnHand", ItemType = BoFormItemTypes.it_EDIT}   
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Quantity to Use", ColumnWidth=80, ColumnName="Quantity", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true} 
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Source Warehouse", ColumnWidth=0, ColumnName="WhsCode", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Batch Number", ColumnWidth=80, ColumnName="BatchNum", ItemType = BoFormItemTypes.it_EDIT}
                        },
                    SQLQuery: @"Select T0.ItemCode,T0.itemName,T2.Quantity as OnHand, T2.Quantity, T3.WhsCode,T3.WhsName,T0.DistNumber as BatchNum from OBTN T0
                    join OBTW T1
                    on T0.AbsEntry=T1.MdAbsEntry

                    join OBTQ T2
                    on T0.AbsEntry=T2.MdAbsEntry and T1.AbsEntry=T2.AbsEntry

                    Join OWHS T3
                    on T2.WhsCode=T3.WhsCode

                    where T3.U_NSC_WhrsType = 'CLO'
                    AND 
                    T2.Quantity>0");
			// TODO: **** POSSIBLE ERROR IN SQL T3.U_NSC_WhrsType = 'CLO' ^ . NEEDS TO BE IN A TABLE

            // Find the matrix in the form UI
            Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);
            MTX_MAIN.SelectionMode = BoMatrixSelect.ms_Auto;
        }

        private void CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            if (pVal.SelectedObjects == null) return;

            try
            {
                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_WH":
                        CommonUI.CFL.SetFieldsWH(pForm, pVal, "CBX_WHSE", "txtWH_CFL");   
                        break;

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static string GetDestWH(Form pForm)     // #10823       // #10824
        {
            try
            {
                string destWH = pForm.Items.Item("CBX_WHSE").Specific.Value;
                if (destWH == "") destWH = pForm.Items.Item("CBX_WHSE").Specific.Value;
                return destWH;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}