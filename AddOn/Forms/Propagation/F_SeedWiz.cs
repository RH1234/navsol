﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Propagation
{
    class F_SeedWiz : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "NSC_SEED_WIZ";
        private const string cItemType = "Seed";
		public const string SEED_WAREHOUSE_TYPE = "GER";

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
	    [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
	    public virtual bool OnBeforeItemPressed(ItemEvent pVal)
	    {
		    var BubbleEvent = true;
		    var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

		    switch (pVal.ItemUID)
		    {
			    case "TAB_3":
				    BubbleEvent = MotherPlantSelected();
				    break;

			    case "TAB_4":
				    BubbleEvent = ValidateForTabFour();
				    break;
		    }

		    NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();

		    return BubbleEvent;
	    }

        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "BTN_FINISH":
					BTN_FINISH_Click();
					break;

				case "BTN_NEXT":
					BTN_NEXT_Click();
					break;

				case "BTN_BACK":
					BTN_BACK_Click();
					break;

                case "BTN_PLANT": // 10823-3
                    CFL_Item_Branches(_SBO_Form);
                    break;

                case "TAB_1":
					TAB_1_Click();
					break;

				case "TAB_2":
					TAB_2_Click();
					break;

				case "TAB_3":
					TAB_3_Click();
					break;

				case "TAB_4":
					TAB_4_Click();
					break;
            }

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            switch (pVal.ItemUID)
            {
                // MotherPlant ChooseFromList
                case "TXT_MOTHR":
                    TXT_PLANT_ChooseFromList_Selected((ChooseFromListEvent)pVal);
                    break;

                // Warehouse ChooseFromList 
                case "TXT_WRHS":
                    TXT_WRHS_ChooseFromList_Selected((ChooseFromListEvent)pVal);
                    break;

                // Warehouse ChooseFromList 
                case "txtWH_CFL":
                    CFL_Selected(oForm, (ChooseFromListEvent)pVal);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        [B1Listener(BoEventTypes.et_KEY_DOWN, false, new string[] { cFormID })]
        public virtual void OnAfterKeyDown(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            _SBO_Form = oForm;

            if (pVal.ItemUID == "TXT_MOTHR" && pVal.CharPressed == 9) CFL_Item_Branches(oForm);  // 10823-3  , pVal

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

       

        public F_SeedWiz() : this(Globals.oApp, Globals.oCompany) { }

		public F_SeedWiz(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

        public void Form_Load()
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                // Set the main image
				CommonUI.Forms.SetFieldvalue.Logo(_SBO_Form, "IMG_HEADER", @"WizardHeader.bmp");

				// Find the header label
				var LBL_HEADER = ((SAPbouiCOM.StaticText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.StaticText, "LBL_HEADER", _SBO_Form));

				// Increase the size of the header label
				LBL_HEADER.Item.FontSize = 12;

				// Set the header label to a bold style
				LBL_HEADER.Item.TextStyle = 1;

				// Set the due date to todays date.
				SAPbouiCOM.EditText TXT_DATE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DATE", _SBO_Form);

				if (TXT_DATE != null)
				{
					TXT_DATE.Value = DateTime.Now.ToString("yyyyMMdd");
				}

                if (Globals.BranchDflt < 0) // 10823-3
                {
                    _SBO_Form.Items.Item("BTN_PLANT").Visible = false;              
                    _SBO_Form.Items.Item("TXT_MOTHR").Specific.ChooseFromListUID = "CFL_PLANT";                   
                    //FilterItemChooseFromList(_SBO_Form); -- dont think I need this
                }

                ////Select a Plant
                var CFL_PLANT = _SBO_Form.ChooseFromLists.Item("CFL_PLANT");

				PopulatePlantChooseFromList(CFL_PLANT);

				var CMB_SeedWarehouse = ((SAPbouiCOM.ComboBox)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_CWRHS", _SBO_Form));

				// TODO: Removing for the time being. We may add this later with the option to skip the seed bank.
				//SAPbouiCOM.ComboBox CMB_PlantWarehouse = ((SAPbouiCOM.ComboBox)_VirSci_Helper_Form.GetControlFromForm(
				//   ItemType: SAP_BusinessOne.Helpers.Forms.FormControlTypes.ComboBox,
				//   ItemUID: "CMB_PWRHS",
				//   FormToSearchIn: _SBO_Form));

				// TODO: Removing for the time being. We may add this later with the option to skip the seed bank.
				//PopulatePlantWarehouseDropDown(CMB_PlantWarehouse);

				// Select the first tab by default
				((SAPbouiCOM.Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_1", _SBO_Form)).Select();

				// Un-Freeze the Form UI
				_SBO_Form.Freeze(false);

				// Show the form
				_SBO_Form.VisibleEx = true;

                // Need to display the form before we can set things to disabled/enabled
                // Populate the Seed Warehouse dropdown
                if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y")
                {
                    NavSol.CommonUI.CFL.CreateWH(_SBO_Form, "CFL_WH", "CMB_CWRHS");
                    NavSol.CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_NONE, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, SEED_WAREHOUSE_TYPE);
                    NavSol.CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND, "Inactive", BoConditionOperation.co_EQUAL, "N");
                }
                else
                    PopulateSeedWarehouseDropdown(CMB_SeedWarehouse);

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        private void CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            if (pVal.SelectedObjects == null) return;

            try
            {
                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_WH":
                        CommonUI.CFL.SetFieldsWH(pForm, pVal, "CMB_CWRHS", "txtWH_CFL");
                        break;

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void PopulatePlantChooseFromList(SAPbouiCOM.ChooseFromList CFL_PLANT)
        {
            // We need to query the database for what kind of items we are taking in..
            Recordset sqlResults = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            var objConditions = CFL_PLANT.GetConditions();

            // Make sure the plant item is a mother plant
            var objCondition = objConditions.Add();
            objCondition.Alias = "U_NSC_IsMother";
            objCondition.Operation = BoConditionOperation.co_EQUAL;
            objCondition.CondVal = "Y";

            CFL_PLANT.SetConditions(objConditions);
        }

        private void PopulateSeedWarehouseDropdown(ComboBox CMBControl, string pPlantBPLid = null)
        {
            // Prepare to run a SQL statement.
            SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            if (pPlantBPLid == null && Globals.BranchDflt >= 0)
                pPlantBPLid = Globals.BranchList;

            // Count how many current records exist within the database.
            var sql = $@"
SELECT[OWHS].[WhsCode], [OWHS].[WhsName]
FROM[OWHS]
LEFT JOIN OBPL ON OWHS.BPLid = OBPL.BPLid        --//10823
WHERE[OWHS].[U_{Globals.SAP_PartnerCode}_WhrsType] = '{SEED_WAREHOUSE_TYPE}' AND[OWHS].[Inactive] = 'N'";
            if (Globals.BranchDflt >= 0) sql += $" AND OBPL.BPLid IN ({pPlantBPLid})";     //10823
            oRecordSet.DoQuery(sql);

            // If items already exist in the drop down
            if (CMBControl.ValidValues.Count > 0)
            {
                // Remove all currently existing values from warehouse drop down
                for (var i = CMBControl.ValidValues.Count; i-- > 0; )
                {
                    CMBControl.ValidValues.Remove(i, BoSearchKey.psk_Index);
                }
            }

            // If more than 1 warehouses exists
            if (oRecordSet.RecordCount > 1)
            {
                // Create the first item as an empty item
                CMBControl.ValidValues.Add("", "");

                // Select the empty item (forcing the user to make a decision)
                CMBControl.Select(0, BoSearchKey.psk_Index);
            }

            // Add allowed warehouses to the drop down
            for (var i = 0; i < oRecordSet.RecordCount; i++)
            {
                try
                {
                    CMBControl.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
                }
                catch { }
                CMBControl.Item.Enabled = true;
                oRecordSet.MoveNext();
            }

            // If we only have exactly one warehouse that contains our item simply select it and disable the field.
            if (CMBControl.ValidValues.Count == 1)
            {
                CMBControl.Select(0, BoSearchKey.psk_Index);
                CMBControl.Item.Enabled = false;
            }
        }

        private void PopulatePlantWarehouseDropDown(ComboBox CMBControl)
        {
            // Prepare to run a SQL statement.
            SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            // Count how many current records exist within the database.
            oRecordSet.DoQuery(
                string.Format(@"
                SELECT DISTINCT [OITW].[WhsCode], [OWHS].[WhsName]
                FROM [OITW]
                JOIN [OWHS] on [OWHS].[WhsCode] = [OITW].[WhsCode]"));

            //WHERE [OWHS].U_{0}_WhrsType IS NOT NULL", Globals.SAP_PartnerCode));

            // If items already exist in the drop down
            if (CMBControl.ValidValues.Count > 0)
            {
                // Remove all currently existing values from warehouse drop down
                for (var i = CMBControl.ValidValues.Count; i-- > 0; )
                {
                    CMBControl.ValidValues.Remove(i, BoSearchKey.psk_Index);
                }
            }

            // If more than 1 warehouses exists
            if (oRecordSet.RecordCount > 1)
            {
                // Create the first item as an empty item
                CMBControl.ValidValues.Add("", "");

                // Select the empty item (forcing the user to make a decision)
                CMBControl.Select(0, BoSearchKey.psk_Index);
            }

            // Add allowed warehouses to the drop down
            for (var i = 0; i < oRecordSet.RecordCount; i++)
            {
                try
                {
                    CMBControl.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
                }
                catch { }
                CMBControl.Item.Enabled = true;
                oRecordSet.MoveNext();
            }

            // If we only have exactly one warehouse that contains our item simply select it and disable the field.
            if (CMBControl.ValidValues.Count == 1)
            {
                CMBControl.Select(0, BoSearchKey.psk_Index);
            }
        }

        private void TXT_PLANT_ChooseFromList_Selected(IChooseFromListEvent pChooseFromListEvent)
        {
            var oChooseFromList = _SBO_Form.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

            if (pChooseFromListEvent.BeforeAction == false)
            {
                var oDataTable = pChooseFromListEvent.SelectedObjects;

                try
                {
                    string oItemCode = oDataTable.GetValue(0, 0).ToString();
                    string oItemSysSerial = oDataTable.GetValue(1, 0).ToString();

                    string itemName = oDataTable.GetValue("itemName", 0).ToString();

                    // Set the userdatasource for the name textbox
                    _SBO_Form.DataSources.UserDataSources.Item("UDS_PLANT").ValueEx = Convert.ToString(itemName);

                    // Save the Mother Plant code
                    ((EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_MTHRC", _SBO_Form))
                    .Value = oItemSysSerial;

                    ((EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_MTHRI", _SBO_Form))
                    .Value = oItemCode;
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void CFL_Item_Branches(Form pForm)//, ItemEvent pVal) // 10823-3
        {
            try
            {
                if (Globals.BranchDflt < 0) return;

                string sql = $@"
                SELECT  OSRN.itemName, OSRN.ItemCode, OSRN.SysNumber, OSRN.DistNumber, OSRQ.WhsCode, OWHS.BPLid
                FROM OSRN 
                INNER JOIN OSRQ ON OSRN.AbsEntry = OSRQ.MdAbsEntry 
                INNER JOIN OWHS ON OSRQ.WhsCode = OWHS.WhsCode
                WHERE U_NSC_IsMother = 'Y' AND OWHS.BPLid IN ({Globals.BranchList}) AND OSRQ.Quantity > 0";

                F_CFL_GRID.CflCB = delegate (string pCallingFormUid)
                {
                    pForm = CommonUI.Forms.GetFormFromUID(pCallingFormUid);
                    string[] splitSerial = pForm.Items.Item("TXT_MTHRC").Specific.Value.ToUpper().Split(new string[] { "-P-" }, StringSplitOptions.None);
                    pForm.Items.Item("TXT_MTHRC").Specific.Value = splitSerial[1];
                    var CMB_SeedWarehouse = ((SAPbouiCOM.ComboBox)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_CWRHS", pForm));
                    PopulateSeedWarehouseDropdown(CMB_SeedWarehouse, pForm.Items.Item("TXT_WHSE").Specific.Value);
                };

                F_CFL_GRID.FormCreate("List of Serial Number Master Data", sql, pForm.UniqueID, "TXT_MOTHR,TXT_MTHRI,TXT_MTHRC,TXT_WHSE", "itemName,ItemCode,DistNumber,BPLid");                
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void TXT_WRHS_ChooseFromList_Selected(IChooseFromListEvent pChooseFromListEvent)
        {
            var oChooseFromList = _SBO_Form.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

            if (pChooseFromListEvent.BeforeAction == false)
            {
                var oDataTable = pChooseFromListEvent.SelectedObjects;

                try
                {
                    string oWarehouseCode = oDataTable.GetValue(0, 0).ToString();
                    string oWarehouseName = oDataTable.GetValue(1, 0).ToString();

                    // Set the userdatasource for the name textbox
                    _SBO_Form.DataSources.UserDataSources.Item("UDS_WRHS").ValueEx = Convert.ToString(oWarehouseName);

                    // Save the Warehouse code
                    ((EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHSC", _SBO_Form))
                    .Value = oWarehouseCode;

                }
                catch (Exception ex)
                {

                }
            }
        }

        private void BTN_FINISH_Click()
        {
            try
            {
                var MotherPlantItemCode = ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_MTHRI", _SBO_Form)).Value.ToString();
                // Grab the ID of the mother plant that was selected by the user in the form
                var MotherPlantSerialId = MotherPlantItemCode + "-P-"
                        +
                        ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_MTHRC", _SBO_Form)).Value.ToString();

                //  Get strain ID from mother plant
                var SQL_GetStrainIDFromMotherPlant = @"
SELECT [OITM].[U_NSC_StrainID], (SELECT [Name] FROM [@" + NSC_DI.Globals.tStrains + @"] WHERE [Code] = [OITM].[U_NSC_StrainID]) AS [StrainName]
FROM [OITM] 
WHERE [OITM].[ItemCode] = '" + MotherPlantItemCode + "'";

                // Get results from the SQL helper
                SAPbobsCOM.Fields resultFromSQLQuery = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(SQL_GetStrainIDFromMotherPlant);

                string StrainIDFromSelectedMotherPlant = resultFromSQLQuery.Item(0).Value.ToString();
                string StrainNameFromSelectedMotherPlant = resultFromSQLQuery.Item(1).Value.ToString();
                // Get the quantity from the form UI
                var QuantityOfSeedsBeingCreated = Convert.ToInt32(((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QUANT", _SBO_Form)).Value.ToString());

                // Get selected warehouse ID from the form UI
                var CMB_CWRHS = ((ComboBox)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_CWRHS", _SBO_Form));

                // Prepare a ComboBox helper
                VERSCI.ComboBox ComboBox_Helper = new VERSCI.ComboBox(Globals.oApp, Globals.oCompany, _SBO_Form);

                // Get the "description" from the ComboBox, where we are really storing the value
                string DestinationWarehouseIdForSeeds = ComboBox_Helper.GetComboBoxValueOrDescription(cFormID, "CMB_CWRHS", null, CMB_CWRHS.Value);


                // Find what item is the "seeds" item for this strain that comes from the selected mother
                var SQL_GetItemCodeOfSeedFromStrainID = @"
SELECT [OITM].[ItemCode]
FROM [OITM]
WHERE [OITM].[U_" + Globals.SAP_PartnerCode + @"_StrainID] = '" + StrainIDFromSelectedMotherPlant + @"'
AND [OITM].[ItmsGrpCod] IN
(SELECT [OITB].[ItmsGrpCod] FROM [OITB] WHERE
[OITB].[ItmsGrpNam] = '" + cItemType + @"')";

                // Get results from SQL server
                resultFromSQLQuery = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(SQL_GetItemCodeOfSeedFromStrainID);
                if (resultFromSQLQuery == null)
                {
                    Globals.oApp.MessageBox("There are no items defined as SEED for the Strain.");
                    return;
                }

                string ItemCodeOfASeedClipping = resultFromSQLQuery.Item(0).Value.ToString();


                // Prepare a new list of items to be added in the "Goods Receipt"
                List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> ListOfGoodsReceiptItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();

                // If the warehouse code is only one digit, add a leading zero.
                if (DestinationWarehouseIdForSeeds.Length == 1)
                {
                    DestinationWarehouseIdForSeeds = "0" + DestinationWarehouseIdForSeeds;
                }

                // Get the new batch ID
                //Inter-Company Check 
                // DELETE 12/30/2020 string InterCoSubCode = NSC_DI.UTIL.SQL.GetValue<string>($@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 where T0.WhsCode = '{DestinationWarehouseIdForSeeds}'");

                string BatchIDOfSeedClippings = NSC_DI.SAP.BatchItems.NextBatch(ItemCodeOfASeedClipping, "B");

                // Add a "Seed" line item to the "Goods Receipt" document
                ListOfGoodsReceiptItems.Add(new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines()
                {
                    ItemCode = ItemCodeOfASeedClipping,
                    Quantity = QuantityOfSeedsBeingCreated,
                    WarehouseCode = DestinationWarehouseIdForSeeds,
                    ListOfBatchLineItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem>()
                {
                    new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem()
                    {
                        BatchNumber = BatchIDOfSeedClippings,
                        Quantity = QuantityOfSeedsBeingCreated,
                        UserDefinedFields = new Dictionary<string, string>()
                        {
                            {"U_"+Globals.SAP_PartnerCode+"_MotherID", MotherPlantSerialId}
                        }
                    }
                }
                });

                // Make sure the "Goods Receipt" was created successfully
                if (NSC_DI.SAP.GoodsReceipt.Create(DateTime.Now, ListOfGoodsReceiptItems) < 1)
                {
                    // Let the user know the creation of the "Goods Receipt" document failed
                    Globals.oApp.StatusBar.SetText("Failed to create the \"Goods Receipt\"!");
                    return;
                }

                #region Washington State API
                /*	// COMPLIANCE
                           try
                           {

                               #region Creating Required Controllers for API Creation and Storing

                               // Get our Settings controller for the State Information for Complain calls.
                               NSC_DI.UTIL.Settings controllerSettings = new Controllers.Settings(
                                   SAPBusinessOne_Application: Globals.oApp,
                                   SAPBusinessOne_Company: Globals.oCompany,
                                   SAPBusinessOne_Form: _SBO_Form);

                               // Add the API Call into our compliance monitor.
                               Controllers.Compliance controllerCompliance = new Controllers.Compliance(SAPBusinessOne_Application: Globals.oApp, SAPBusinessOne_Company: Globals.oCompany);

                               // Prepare a connection to the state API
                               var TraceCon = new NSCNSC_DI.SAP._DI.UTIL.Misc.KillObject(oForm);TraceabilityAPI(SAPBusinessOne_Application: Globals.oApp,
                                   SAPBusinessOne_Company: Globals.oCompany,
                                   SAPBusinessOne_Form: _SBO_Form);

                               BioTrack.API bioCrapAPI = TraceCon.new_API_obj();
                               // Create a Bio Track API Controller.

                               #endregion

                               List<BioTrack.Inventory.InvNewData> newInventoryData = new List<BioTrack.Inventory.InvNewData>()
                               {
                                   new BioTrack.Inventory.InvNewData()
                                   {
                                       InvType = BioTrack.Inventory.InvTypes.Seed,
                                       quantity = QuantityOfSeedsBeingCreated,
                                       strain = StrainNameFromSelectedMotherPlant,
                                       Source_ID = MotherPlantSerialId
                                   }
                               };

                               BioTrack.Inventory.New(
                                       BioTrackAPI: ref bioCrapAPI
                                       , Location: controllerSettings.GetValueOfSetting("StateLocLic").ToString()
                                       , Data: newInventoryData.ToArray()

                               );

                               string newComplianceID = controllerCompliance.CreateComplianceLineItem(
                                   Reason: Models.Compliance.ComplianceReasons.Inventory_New,
                                    API_Call: bioCrapAPI.XmlApiRequest.ToString()
                               );

                               // API: Create seeds from a mother plant
                               bioCrapAPI.PostToApi();

                               if (bioCrapAPI.WasSuccesful)
                               {
                                   controllerCompliance.UpdateComliancy(
                                       ComplianceID: newComplianceID,
                                       ResponseXML: bioCrapAPI.xDocFromResponse.ToString(),
                                       Status: Controllers.Compliance.ComplianceStatus.Success);

                                   // Update the newly created batch with the newly created State ID
                                   string newlyCreatedStateID = bioCrapAPI.xmlDocFromResponse.DocumentElement.GetElementsByTagName("barcode_id")[0].InnerXml.ToString();

                                   // Prepare a SQL query that will update a batch with the newly created State ID
                                   var sql_query_UpdateBatchWithNewlyCreatedStateID = @"
               UPDATE
               [OBTN]
               SET
               [U_VSC_StateID] = '" + (new SqlParameter("StateID", newlyCreatedStateID)).Value.ToString() + @"'
               WHERE 
               [OBTN].[DistNumber] = '" + (new SqlParameter("DistNumber", BatchIDOfSeedClippings)).Value.ToString() + "'";

                                   // Prepare a SQL helper
                                   SAP_BusinessOne.Helpers.SQL helperSQL_UpdateBatch = new SAP_BusinessOne.Helpers.SQL(SAPBusinessOne_Company: Globals.oCompany);

                                   // Run the SQL query
                                   helperSQL_UpdateBatch.RunSQLQuery(SQLQuery: sql_query_UpdateBatchWithNewlyCreatedStateID);

                                   // Tell the user things went as expected
                                   Globals.oApp.StatusBar.SetText("Successfully received clones in inventory!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success
                                   );
                               }
                               else
                               {
                                   controllerCompliance.UpdateComliancy(
                                       ComplianceID: newComplianceID,
                                       ResponseXML: bioCrapAPI.xDocFromResponse.ToString(),
                                       Status: Controllers.Compliance.ComplianceStatus.Failed);

                                   // Send failure message to the client
                                   Globals.oApp.StatusBar.SetText("Failed to post to the state!");
                               }
                           }
                           catch (Exception ex)
                           {
                               // API-SAP-TODO: API Log error. Since this is a Compliance call with return value handling, we would have to handle this error more urgently.
                           }
               */
                #endregion

                // Close the form, user is done!
                _SBO_Form.Close();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                GC.Collect();
            }
        }

        private void BTN_NEXT_Click()
        {
            switch (CurrentlySelectedTab())
            {
                case 1:
                    ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_2", _SBO_Form))
                    .Select();
                    break;

                case 2:
                    ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_3", _SBO_Form))
                    .Select();
                    break;

                case 3:
                    ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_4", _SBO_Form))
                    .Select();
                    break;
            }
        }

        private int CurrentlySelectedTab()
        {
            for (var i = 1; i <= 4; i++)
            {
                Folder tab = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_" + i.ToString(), _SBO_Form);
                if (tab.Selected)
                {
                    return i;
                }
            }

            return 0;
        }

        private void BTN_BACK_Click()
        {
            switch (CurrentlySelectedTab())
            {
                case 2:
                    ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_1", _SBO_Form))
                    .Select();
                    break;

                case 3:
                    ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_2", _SBO_Form))
                    .Select();
                    break;

                case 4:
                    ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_3", _SBO_Form))
                    .Select();
                    break;
            }
        }

        private void TAB_1_Click()
        {
            // Disable Back Button
            Button BTN_BACK = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_BACK", _SBO_Form);
            BTN_BACK.Item.Enabled = false;

            // Disable Back Button
            Button BTN_FINISH = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_FINISH", _SBO_Form);
            BTN_FINISH.Item.Enabled = false;
        }

        private void TAB_2_Click()
        {
            // Enable Back Button
            Button BTN_BACK = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_BACK", _SBO_Form);
            BTN_BACK.Item.Enabled = true;

            // Select the Mother Plant textbox
            //((SAPbouiCOM.EditText)Helpers.Forms.GetControlFromForm(ItemType: Helpers.Forms.FormControlTypes.EditText, ItemUID: "TXT_PLANT", FormToSearchIn: _SBO_Form))
            //.Active = true;
        }

        private void TAB_3_Click()
        {
            // Disable Finish Button
            Button BTN_FINISH = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_FINISH", _SBO_Form);
            BTN_FINISH.Item.Enabled = false;

            // Disable Next Button
            Button BTN_NEXT = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_NEXT", _SBO_Form);
            BTN_NEXT.Item.Enabled = true;

            // Select the Quantity Textbox/ChooseFromList by default
            EditText TXT_QUANT = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_QUANT", _SBO_Form);
            TXT_QUANT.Active = true;
        }

        private void TAB_4_Click()
        {
            // Enable Finish Button
            Button BTN_FINISH = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_FINISH", _SBO_Form);
            BTN_FINISH.Item.Enabled = true;

            // Disable Next Button
            Button BTN_NEXT = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_NEXT", _SBO_Form);
            BTN_NEXT.Item.Enabled = false;
        }

	    private bool MotherPlantSelected()
	    {
		    try
		    {
			    // Make sure a Mother plant is selected.
			    if (((SAPbouiCOM.EditText) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_MTHRC", _SBO_Form)).Value != "") return true;

			    ((SAPbouiCOM.Folder) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_2", _SBO_Form)).Select();
			    Globals.oApp.StatusBar.SetText("Please select a Mother Plant first!", SAPbouiCOM.BoMessageTime.bmt_Short);
			    return false;
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return false;
			}
		    finally
		    {
			    //NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

	    private bool ValidateForTabFour()
        {
            var _BubbleEvent = true;

            var TXT_QTY = (EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_QUANT", _SBO_Form);

            var CMB_CWRHS = (ComboBox)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.ComboBox, "CMB_CWRHS", _SBO_Form);

            // TODO: Removing for the time being. We may add this later with the option to skip the seed bank.
            //SAPbouiCOM.ComboBox CMB_PWRHS = (SAPbouiCOM.ComboBox)Helpers.Forms.GetControlFromForm(
            //    ItemType: Helpers.Forms.FormControlTypes.ComboBox,
            //    ItemUID: "CMB_PWRHS",
            //    FormToSearchIn: _SBO_Form);

            if (string.IsNullOrEmpty(TXT_QTY.Value))
            {
                // Don't allow SAP to continue
                _BubbleEvent = false;

                Globals.oApp.StatusBar.SetText("Please enter a quantity of Seeds!", BoMessageTime.bmt_Short);

                TXT_QTY.Active = true;
            }

            if (string.IsNullOrEmpty(CMB_CWRHS.Value))
            {
                // Don't allow SAP to continue
                _BubbleEvent = false;

                Globals.oApp.StatusBar.SetText("Please select a destination warehouse for the Seed!", BoMessageTime.bmt_Short);

                CMB_CWRHS.Active = true;
            }

            //// TODO: Removing for the time being. We may add this later with the option to skip the seed bank.
            //if (string.IsNullOrEmpty(CMB_PWRHS.Value))
            //{
            //    // Don't allow SAP to continue
            //    _BubbleEvent = false;

            //    Globals.oApp.StatusBar.SetText(
            //        Text: "Please select a destination warehouse for the Plant!",
            //        Seconds: SAPbouiCOM.BoMessageTime.bmt_Short,
            //        Type: SAPbouiCOM.BoStatusBarMessageType.smt_Error);

            //    CMB_PWRHS.Active = true;
            //}

            if (!_BubbleEvent)
            {
                ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_3", _SBO_Form))
                .Select();
            }

            return _BubbleEvent;
        }
    }
}