﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Propagation
{
    class F_PVCloneVM : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID     = "NSC_PRODCLONE";
        private const string cItemType   = "Cannabis Plant";
        private const string cPdoProcess = "CLONE";

        public class BatchInfo
		{
			public string ItemCode { get; set; }
			public double Quantity { get; set; }
			public string StateID { get; set; }
			public string UniqueID { get; set; }
			public string SourceWarehouseCode { get; set; }

			public string DestinationWarehouseCode { get; set; }
		}

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
	    [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
	    public virtual bool OnBeforeItemPressed(ItemEvent pVal)
	    {
		    bool BubbleEvent = true;
		    Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();

		    return BubbleEvent;
	    }
	    #endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "BTN_APPLY":
					BTN_APPLY_PRESSED_NEW();
					break;
                case "MTX_ITEMS":
                    if(pVal.Row != 0)//10927 if the row is 0 the setremarks method fails at MatrixExtensionMethods.GetStringOfSelectedRow(). The reason will be obvious if you dig into this method              
                        SetRemarks();
                    break;
            }

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "MTX_ITEMS":
					MAIN_MATRIX_LINK_PRESSED(pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		public F_PVCloneVM() : this(Globals.oApp, Globals.oCompany) { }

        public F_PVCloneVM(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

        public void Form_Load()
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                _SBO_Form.DataSources.DataTables.Add("dtBinSrc");

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                // Set the main image
				CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_MAIN", @"clone-icon.bmp");

				Load_Matrix();

                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                // Show the form
                _SBO_Form.VisibleEx = true;

                _SBO_Form.Items.Item("TXT_QUANT").Specific.Active = true;
			}
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

		private void BTN_APPLY_PRESSED_NEW()
		{

			#region Validate User Input
			if (this.CurrentlySelectedProductionOrder() == 0)
			{
				// Send a message to the client
				Globals.oApp.StatusBar.SetText("Please select a production order!");
				return;
			}

			// Grab the textbox quantity
			EditText TXT_QUANT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QUANT", _SBO_Form);

			// Make sure the user entered a value for quantity
			double QuantityToAdd;
			if (TXT_QUANT.Value.ToString() == "" || !Double.TryParse(TXT_QUANT.Value.ToString(), out QuantityToAdd) || QuantityToAdd < 0)
			{
				// Send a message to the client
				Globals.oApp.StatusBar.SetText("Please enter a numerical quantity of 0 or higher!");
				return;
			}

			// Grab the ComboButton from the form UI
			ButtonCombo CBT_ACTION = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ButtonCombo, "CBT_ACTION", _SBO_Form);

			if (CBT_ACTION.Selected == null)
			{
				// Send a message to the client
				Globals.oApp.StatusBar.SetText("Please select an action!");
				return;
			}

			// Grab the Matrix from the form 
			Matrix matrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", _SBO_Form);
			//Check that the enteres amount is not greater than planned amount.
			string strPlannedQty = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Planned");
            string strCropID = null;
            strCropID = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Crop ID");
            if (!Double.TryParse(TXT_QUANT.Value.ToString(), out QuantityToAdd) || QuantityToAdd > Convert.ToDouble(strPlannedQty))
			{
				// Send a message to the client
				Globals.oApp.StatusBar.SetText("Please enter a numerical quantity less than or equal to planned!");
				return;
			}
			#endregion

			#region Get Information and Create Controllers
			// Grab the Item Code from the matrix in the form UI
			string ItemCode = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Item Code");

            // Grab the Item Code of the Plant
            //string ItemCodeOfPlant = NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(ItemCode)[0].ItemCode.ToString();
            string ItemCodeOfPlant = ItemCode;

            // Grab the warehouse code from the Matrix in the form UI
            string SourceWarehouseCode = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Warehouse ID");

			// Grab the destination warehouse code from the Production Order
			string DestinationWarehouseCode = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Destination Warehouse");

            // Create a List to hold items
            //List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> listOfGoodsReceiptLines = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();

            #endregion

            //listOfGoodsReceiptLines.Add(new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines()
            //{
            //	ItemCode = ItemCodeOfPlant,
            //	Quantity = QuantityToAdd,
            //	WarehouseCode = DestinationWarehouseCode,
            //	SerialNumbers = NSC_DI.SAP.Plant.GetNextAvailablePlantSerialID(ItemCodeOfPlant, QuantityToAdd)
            //});

            //Create an array of strings to hold newly generated SN numbers for plants being created. 
            // Attempt to create a "Receipt from Production" document
            // SERIAL NUMBER

            try
            { 
                int Qty = Convert.ToInt16(QuantityToAdd);
                SAPbouiCOM.Form pForm = null;
                int prodOrder = this.CurrentlySelectedProductionOrder();
                //System.Data.DataTable dtBins = null;
                pForm = Globals.oApp.Forms.Item(_SBO_Form.UniqueID) as Form;

                SAPbouiCOM.DataTable dtBinsS = _SBO_Form.DataSources.DataTables.Item("dtBinSrc");    // NavSol.Forms.F_BinSelect.CreateDT();
                NavSol.Forms.F_BinSelect.dtCreateIn(dtBinsS);
                var j = 0;
                var sql = $@"
SELECT DISTINCT 
       OITM.ItemCode  AS [ItemCode], OITM.ItemName AS [Name], 
	   WOR1.warehouse AS [SrcWhCode], (SELECT WhsName FROM OWHS WHERE WhsCode = WOR1.wareHouse) AS [From WH],
	   OWOR.Warehouse AS [DstWhCode], (SELECT WhsName FROM OWHS WHERE WhsCode = OWOR.Warehouse) AS [To WH],
	   '' AS [Serial\Batch], '' AS [BinCode], 
       '' AS [LevelID1], '' AS [LevelID2], '' AS [LevelID3], '' AS [LevelID4], 
	   0.0 AS [XFer Qty], {Convert.ToDecimal(Qty)}.00 AS [Bin Qty],
       WOR1.IssuedQty AS [Old Bin Qty], -1 AS [LinkRecNo],
       CASE WHEN OITM.ManSerNum = 'Y' THEN 'S' WHEN OITM.ManBtchNum = 'Y' THEN 'B' ELSE 'N' END AS [ManBySBN]
  FROM OWOR
  JOIN OITM ON OITM.ItemCode  = OWOR.ItemCode
  JOIN WOR1 ON WOR1.DocEntry  = OWOR.DocNum
  JOIN OWHS ON WOR1.wareHouse = OWHS.WhsCode
 WHERE OWOR.DocNum = {prodOrder} 
   AND OITM.QryGroup39 = 'Y' AND OWHS.U_NSC_WhrsType='CLO' AND WOR1.IssuedQty > 0";

                // skip the BIN mangement form if the destination warehouse is NOT bin managed
                var wh = NSC_DI.UTIL.SQL.GetValue<string>(string.Format(@"Select warehouse from OWOR where DocNum = {0}", prodOrder), "");
                if (NSC_DI.SAP.Bins.IsManaged(wh))
                {
                    NavSol.Forms.F_BinSelect.FormCreate(_SBO_Form.UniqueID, sql, Qty, wh, "", true, NSC_DI.SAP.Phase.IsSerialize("VEG"));
                    F_BinSelect.BinsCB = delegate (string callingFormUid, System.Data.DataTable dtBins)
                    {
                        Process(pForm, prodOrder, ItemCode, Qty, dtBins, strCropID);
                    };
                }
                else Process(pForm, prodOrder, ItemCode, Qty, null, strCropID);
            }
            catch (NSC_DI.SAP.B1Exception ex)
            {
                Globals.oApp.StatusBar.SetText(ex.Message);
            }
        }

        private void Process(SAPbouiCOM.Form pForm, int prodOrder, string ItemCode, int Qty, System.Data.DataTable dtBins,string pCropID=null)
        {
            try
            {
                NSC_DI.SAP.ProductionOrder.CheckProject(prodOrder);

                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();


                //Get the Batch for the production order to create Byproducts if they exist
                string strSQL = string.Format(@"SELECT
    T2.[BatchNum],T4.U_NSC_MotherID
    FROM
    OWOR T0  INNER JOIN WOR1 T1 ON T0.DocEntry = T1.DocEntry left  join IBT1 T2 on T2.[BsDocEntry]  = t0.docentry and T2.[BsDocLine]  =  T1.[LineNum] 
    left  join IBT1 T3 on T3.[BsDocEntry]  = t0.docentry and T0.[ItemCode] = T3.[ItemCode]
    Join OBTN T4 on T4.DistNumber = T2.BatchNum 
    WHERE
    T2.[BsDocType]  = 202 and  (T0.[Status] = 'R' OR T0.[Status] = 'L' ) and T0.DocEntry={0}", prodOrder);


                SAPbobsCOM.Fields oRetunedValues = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(strSQL);
                //ToDo: Set user defined fields
                string strBatchNum = oRetunedValues.Item(0).Value;
                string cloneBatchNum = strBatchNum;
                var UserDefinedFields = NSC_DI.SAP.BatchItems.GetInfo(strBatchNum);

                string strCropHolder = null;

                int intReceiveKey = 0;

                if(NSC_DI.UTIL.Settings.Value.Get("Serialize Plants")=="Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG")
                {
                    var SNs = NSC_DI.UTIL.AutoStrain.NextSN(ItemCode, "Serial Plant", Qty);
                    //Inter-Company Check
                    // I left this one in place because settings.concatbatchnum doesnt have the ability to handle the else yet, revisit in the future, Eric
                    string InterCoSubCode = NSC_DI.UTIL.SQL.GetValue<string>(@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 join OWOR T1 on T0.WhsCode = T1.Warehouse where T1.DocNum = " + prodOrder);
                    try
                    {

                        string InterCo = NSC_DI.UTIL.SQL.GetValue<string>("select T0.U_Value from dbo.[@NSC_SETTINGS] T0 where T0.Name = 'Inter-Company'");
                        if (InterCo == "Y" && InterCoSubCode.Trim().Length > 0)
                        {
                            SNs = NSC_DI.UTIL.AutoStrain.NextSN(ItemCode, "Serial Plant", Qty, InterCoSubCode);
                        }
                        else
                        {
                            SNs = NSC_DI.UTIL.AutoStrain.NextSN(ItemCode, "Serial Plant", Qty);
                        }
                    }
                    catch
                    {
                        throw new Exception(NSC_DI.UTIL.Message.Format("Inter-Company Error: Please check your settings (Y or N) and make sure you have a subsidiary code on your warehouse."));
                    }

                    if (pCropID!=null && pCropID.Length > 0)
                    {
                        strCropHolder = pCropID;
                        UserDefinedFields.UserFields.Item("U_NSC_CropStageID").Value = "VEG_CLONE_" + pCropID;
                    }
                    intReceiveKey = NSC_DI.SAP.ProductionOrder.Receive(prodOrder, cItemType, Qty, strBatchNum, SNs, "Clone", UserDefinedFields, null, dtBins,null,0,"", strCropHolder);

                }
                if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "N"))
                {
                    if (pCropID!=null && pCropID.Length > 0)
                    {
                        strCropHolder = pCropID;
                        UserDefinedFields.UserFields.Item("U_NSC_CropStageID").Value = "VEG_CLONE_" + pCropID;
                    }
                    //Inter-Company Check
                    //string InterCoSubCode = NSC_DI.UTIL.SQL.GetValue<string>(@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 join OWOR T1 on T0.WhsCode = T1.Warehouse where T1.DocNum = " + prodOrder);
                    strBatchNum = NSC_DI.SAP.BatchItems.NextBatch(ItemCode);
                    //try
                    //{
                    //    string InterCo = NSC_DI.UTIL.SQL.GetValue<string>("select T0.U_Value from dbo.[@NSC_SETTINGS] T0 where T0.Name = 'Inter-Company'");
                    //    if (InterCo == "Y" && InterCoSubCode.Trim().Length > 0)
                    //    {
                    //        strBatchNum = InterCoSubCode + "-" + strBatchNum;
                    //    }
                    //}
                    //catch
                    //{
                    //    throw new Exception(NSC_DI.UTIL.Message.Format("Inter-Company Error: Please check your settings (Y or N) and make sure you have a subsidiary code on your warehouse."));
                    //}

                    intReceiveKey = NSC_DI.SAP.ProductionOrder.Receive(prodOrder, cItemType, Qty, strBatchNum,null , "Clone", UserDefinedFields, null, dtBins, null,0,"",strCropHolder);
                }

                if (pCropID != null && pCropID.Length > 0)
                {
                    Forms.Crop.CropCycleManagement oCrop = new Forms.Crop.CropCycleManagement();
                    oCrop.AddDocToProject_Receipts(PMDocumentTypeEnum.pmdt_GoodsReceipt, intReceiveKey.ToString(), pCropID, "VEG_CLONE_" + pCropID);

                    //Check for Backflush issue to production that needs to be set to the project. 
                    int intBackfluchProdOrd = 0;
                    var sql = $@"
SELECT DISTINCT IGE1.DocEntry FROM IGE1
 INNER JOIN OITM ON IGE1.ItemCode = OITM.ItemCode
 WHERE IGE1.BaseEntry = {prodOrder} AND OITM.IssueMthd = 'B'";
                    intBackfluchProdOrd = NSC_DI.UTIL.SQL.GetValue<int>(sql, 0);
                    //intBackfluchProdOrd = NSC_DI.UTIL.SQL.GetValue<int>(string.Format(@" Select Distinct ISNULL(T0.DocEntry,0) as DocEntry from IGE1 T0
                    //                                                                        inner join OITM T1 on T1.ItemCode = T0.ItemCode
                    //                                                                        where T0.BaseEntry = {0} and IssueMthd = 'B'", prodOrder.ToString()));
                    if (intBackfluchProdOrd > 0)
                    {
                        oCrop.AddDocToProject_Backflush(PMDocumentTypeEnum.pmdt_GoodsIssue, intBackfluchProdOrd.ToString(), pCropID, "VEG_CLONE_" + pCropID);
                    }
                }

                if (pForm.Items.Item("CBT_ACTION").Specific.Selected.Value == "RECEIPT_CLOSE")
                {
                    // Based off of the currently selected action
                    //Change the status of the production order.
                    NSC_DI.SAP.ProductionOrder.UpdateStatus(prodOrder, BoProductionOrderStatusEnum.boposClosed);
                }

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);



                //update batch StageID
                //ToDo: Determine if it is Plants are Batch or Serial if Batch call code below if Serial build a function that will up date the serialized plants
                //if (pCropID.Length > 0)
                //{
                //    string strReceivedBatchNum = NSC_DI.SAP.BatchItems.GetBatchNumFromDoc("60", prodOrder, 0);
                //    string strStageID = "PROP_OTHER_" + strCropHolder;
                //    NSC_DI.SAP.BatchItems.SetCropStageID(strReceivedBatchNum, ItemCode, strStageID);
                //}


                Globals.oApp.StatusBar.SetText("Successfully received item!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                Load_Matrix(pForm);

                // Set the main image
                SAPbouiCOM.EditText oEdit = pForm.Items.Item("TXT_QUANT").Specific;
                oEdit.Value = "";

            }
            catch (NSC_DI.SAP.B1Exception ex)
            {
                Globals.oApp.StatusBar.SetText(ex.Message);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                NSC_DI.UTIL.Misc.KillObject(pForm);
                GC.Collect();
            }
        }

        private void SetRemarks()
        {
            try
            {
                //Get the Remarks Textbox
                SAPbouiCOM.EditText oEdit = _SBO_Form.Items.Item("TXT_REM").Specific;
                // Grab the Matrix from the form 
                Matrix matrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", _SBO_Form);
                //Get selected production order
                string ProdNumder = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Production Order");

                string Remarks = NSC_DI.UTIL.SQL.GetValue<string>($"Select OWOR.Comments from OWOR where OWOR.DocNum={ProdNumder}");
                oEdit.Value = Remarks;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void BTN_APPLY_PRESSED()
        {

            #region Validate User Input
            if (this.CurrentlySelectedProductionOrder() == 0)
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Please select a production order!");
                return;
            }

            // Grab the textbox quantity
            EditText TXT_QUANT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QUANT", _SBO_Form);

            // Make sure the user entered a value for quantity
            double QuantityToAdd;
            if (TXT_QUANT.Value.ToString() == "" || !Double.TryParse(TXT_QUANT.Value.ToString(), out QuantityToAdd) || QuantityToAdd <= 0)
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Please enter a numerical quantity greater than 0!");
                return;
            }

            // Grab the ComboButton from the form UI
            ButtonCombo CBT_ACTION = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ButtonCombo, "CBT_ACTION", _SBO_Form);

            if (CBT_ACTION.Selected == null)
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Please select an action!");
                return;
            }

            // Grab the Matrix from the form 
            Matrix matrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", _SBO_Form);
            //Check that the enteres amount is not greater than planned amount.
            string strPlannedQty = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Planned");
            if (!Double.TryParse(TXT_QUANT.Value.ToString(), out QuantityToAdd) || QuantityToAdd > Convert.ToDouble(strPlannedQty))
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Please enter a numerical quantity less than or equal to planned!");
                return;
            }
            #endregion

            #region Get Information and Create Controllers
            // Grab the Item Code from the matrix in the form UI
            string ItemCode = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Item Code");

            // Grab the Item Code of the Plant
            //string ItemCodeOfPlant = NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(ItemCode)[0].ItemCode.ToString();
            string ItemCodeOfPlant = ItemCode;

            // Grab the warehouse code from the Matrix in the form UI
            string SourceWarehouseCode = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Warehouse ID");

            // Grab the destination warehouse code from the Production Order
            string DestinationWarehouseCode = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Destination Warehouse");

            // Create a List to hold items
            //List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> listOfGoodsReceiptLines = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();

            #endregion

            //listOfGoodsReceiptLines.Add(new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines()
            //{
            //	ItemCode = ItemCodeOfPlant,
            //	Quantity = QuantityToAdd,
            //	WarehouseCode = DestinationWarehouseCode,
            //	SerialNumbers = NSC_DI.SAP.Plant.GetNextAvailablePlantSerialID(ItemCodeOfPlant, QuantityToAdd)
            //});

            //Create an array of strings to hold newly generated SN numbers for plants being created. 
            // Attempt to create a "Receipt from Production" document
            // SERIAL NUMBER
            try
            {
                int Qty = Convert.ToInt16(QuantityToAdd);

                SAPbouiCOM.DataTable dtBinsS = _SBO_Form.DataSources.DataTables.Item("dtBinSrc");    // NavSol.Forms.F_BinSelect.CreateDT();
                NavSol.Forms.F_BinSelect.dtCreateIn(dtBinsS);
                var j = 0;
                //WOR1.IssuedQty 
                var sql =$@"
SELECT DISTINCT 
       OITM.ItemCode  AS [ItemCode], OITM.ItemName AS [Name], 
	   WOR1.warehouse AS [SrcWhCode], (SELECT WhsName FROM OWHS WHERE WhsCode = WOR1.wareHouse) AS [From WH],
	   OWOR.Warehouse AS [DstWhCode], (SELECT WhsName FROM OWHS WHERE WhsCode = OWOR.Warehouse) AS [To WH],
	   '' AS [Serial Number], '' AS [BinCode], 
       '' AS [LevelID1], '' AS [LevelID2], '' AS [LevelID3], '' AS [LevelID4], 
	   0.0 AS [XFer Qty], {Convert.ToDecimal(Qty)}.00 AS [Bin Qty],
        WOR1.IssuedQty AS [Old Bin Qty], -1 AS [LinkRecNo]
  FROM OWOR
  JOIN OITM ON OITM.ItemCode  = OWOR.ItemCode
  JOIN WOR1 ON WOR1.DocEntry  = OWOR.DocNum
  JOIN OWHS ON WOR1.wareHouse = OWHS.WhsCode
 WHERE OWOR.DocNum = {this.CurrentlySelectedProductionOrder()} 
   AND OITM.QryGroup39 = 'Y' AND OWHS.U_NSC_WhrsType='CLO' AND WOR1.IssuedQty > 0";

                var prodOrder = this.CurrentlySelectedProductionOrder();
                var pformID = _SBO_Form.UniqueID;

                //Check if the destination warehouse is BIN managed
                //If so skip the BIN mangement form. 
                string strBinActiveHolder = NSC_DI.UTIL.SQL.GetValue<string>(@"select BinActivat from OWHS where WhsCode = '" + DestinationWarehouseCode + "'");

                if (strBinActiveHolder == "Y")
                {
                    // RHH NavSol.Forms.F_BinSelect.FormCreate(_SBO_Form.UniqueID, sql, Qty, NSC_DI.UTIL.SQL.GetValue<string>(string.Format(@"Select warehouse from OWOR where DocNum = {0}", this.CurrentlySelectedProductionOrder()), ""), "", true);

                    // RHH F_BinSelect.BinsCB = delegate (string callingFormUid, System.Data.DataTable dtBins)
                    // RHH {
                    System.Data.DataTable dtBins = null;    // RHH
                    Form pForm = null;
                        //return;
                        try
                        {
                            pForm = Globals.oApp.Forms.Item(pformID) as Form;
                        if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                            var SNs = NSC_DI.UTIL.AutoStrain.NextSN(ItemCode, "Serial Plant", Qty);

                            //Get the Batch for the production order to create Byproducts if they exist
                            string strSQL = string.Format(@"SELECT
    T2.[BatchNum],T4.U_NSC_MotherID
    FROM
    OWOR T0  INNER JOIN WOR1 T1 ON T0.DocEntry = T1.DocEntry left  join IBT1 T2 on T2.[BsDocEntry]  = t0.docentry and T2.[BsDocLine]  =  T1.[LineNum] 
    left  join IBT1 T3 on T3.[BsDocEntry]  = t0.docentry and T0.[ItemCode] = T3.[ItemCode]
    Join OBTN T4 on T4.DistNumber = T2.BatchNum 
    WHERE
    T2.[BsDocType]  = 202 and  (T0.[Status] = 'R' OR T0.[Status] = 'L' ) and T0.DocEntry={0}", prodOrder);


                            SAPbobsCOM.Fields oRetunedValues = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(strSQL);
                            //ToDo: Set user defined fields
                            string strBatchNum = oRetunedValues.Item(0).Value;
                            var UserDefinedFields = NSC_DI.SAP.BatchItems.GetInfo(strBatchNum);

                            NSC_DI.SAP.ProductionOrder.Receive(prodOrder, cItemType, QuantityToAdd, strBatchNum, SNs, "Clone", UserDefinedFields, null, dtBins);

                            // Based off of the currently selected action
                            if (CBT_ACTION.Selected.Value == "RECEIPT_CLOSE")
                            {
                                // Change the status of the production order.
                                NSC_DI.SAP.ProductionOrder.UpdateStatus(prodOrder, BoProductionOrderStatusEnum.boposClosed);
                            }

                        if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                            Globals.oApp.StatusBar.SetText("Successfully received item!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                            Load_Matrix(pForm);
                        }
                        catch (NSC_DI.SAP.B1Exception ex)
                        {
                            Globals.oApp.StatusBar.SetText(ex.Message);
                        }
                        catch (Exception ex)
                        {
                            Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                        }
                        finally
                        {
                            if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                            NSC_DI.UTIL.Misc.KillObject(pForm);
                            GC.Collect();
                        }
                    // RHH };
                }
                else
                {
                    //Setup standard Processing without Bins --It should be the same as Bins but substituting general Warehouses for BINS
                    Form pForm = null;
                    try
                    {
                        pForm = Globals.oApp.Forms.Item(pformID) as Form;
                        if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                        var SNs = NSC_DI.UTIL.AutoStrain.NextSN(ItemCode, "Serial Plant", Qty);

                        //Get the Batch for the production order to create Byproducts if they exist
                        string strSQL = string.Format(@"SELECT
    T2.[BatchNum],T4.U_NSC_MotherID
    FROM
    OWOR T0  INNER JOIN WOR1 T1 ON T0.DocEntry = T1.DocEntry left  join IBT1 T2 on T2.[BsDocEntry]  = t0.docentry and T2.[BsDocLine]  =  T1.[LineNum] 
    left  join IBT1 T3 on T3.[BsDocEntry]  = t0.docentry and T0.[ItemCode] = T3.[ItemCode]
    Join OBTN T4 on T4.DistNumber = T2.BatchNum 
    WHERE
    T2.[BsDocType]  = 202 and  (T0.[Status] = 'R' OR T0.[Status] = 'L' ) and T0.DocEntry={0}", prodOrder);


                        SAPbobsCOM.Fields oRetunedValues = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(strSQL);
                        //ToDo: Set user defined fields
                        string strBatchNum = oRetunedValues.Item(0).Value;
                        var UserDefinedFields = NSC_DI.SAP.BatchItems.GetInfo(strBatchNum);

                        NSC_DI.SAP.ProductionOrder.Receive(prodOrder, cItemType, QuantityToAdd, strBatchNum, SNs, "Clone", UserDefinedFields, null, null);

                        // Based off of the currently selected action
                        if (CBT_ACTION.Selected.Value == "RECEIPT_CLOSE")
                        {
                            // Change the status of the production order.
                            NSC_DI.SAP.ProductionOrder.UpdateStatus(prodOrder, BoProductionOrderStatusEnum.boposClosed);
                        }

                        if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                        Globals.oApp.StatusBar.SetText("Successfully received item!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                        Load_Matrix(pForm);
                    }
                    catch (NSC_DI.SAP.B1Exception ex)
                    {
                        Globals.oApp.StatusBar.SetText(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                    }
                    finally
                    {
                        if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                        NSC_DI.UTIL.Misc.KillObject(pForm);
                        GC.Collect();
                    }
                }

            }
            catch (NSC_DI.SAP.B1Exception ex)
            {
                Globals.oApp.StatusBar.SetText(ex.Message);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                GC.Collect();
            }

            //#region Washington State API

            //			// API: Creating new plant(s) from a clone(s).
            //			try
            //			{
            //				#region Get all the source state IDs (and other stuff) given a production order for the API calls

            //				string sql_query = string.Format(@"
            //SELECT [IBT1].[BatchNum] AS [BatchNumber]
            //, [IBT1].[Quantity] AS [Quantity] -- DO NOT USE!
            //, [OBTN].[U_NSC_StateID] AS [StateID]
            //, ( SELECT [@" + NSC_DI.Globals.tStrains + @"].[U_StrainName] 
            //    FROM [@" + NSC_DI.Globals.tStrains + @"] 
            //    JOIN [OITM] ON [@" + NSC_DI.Globals.tStrains + @"].[Code] = [OITM].[U_NSC_StrainID] 
            //    WHERE [OITM].[ItemCode] = [IBT1].[ItemCode] )   AS [Strain]
            //, [IBT1].[WhsCode] AS [RoomID]
            //, [IBT1].[ItemCode] AS [ItemCode]
            //FROM [IBT1]
            //JOIN [IGE1] ON [IBT1].[BaseEntry] = [IGE1].[DocEntry]
            //    AND [IBT1].[ItemCode] = [IGE1].[ItemCode]
            //JOIN [OBTN] ON [OBTN].[DistNumber] = [IBT1].[BatchNum] 
            //WHERE [IGE1].[BaseRef] = '{0}' ", CurrentlySelectedProductionOrder());

            //				SAPbobsCOM.Fields sql_return = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(sql_query);

            //				string BatchNumber = sql_return.Item(0).Value.ToString();
            //				int QuantityOnProductOrder = Convert.ToInt32(sql_return.Item(1).Value.ToString());
            //				int QuantityToCreate = Convert.ToInt32(QuantityToAdd);
            //				string SourceID = sql_return.Item(2).Value.ToString();
            //				string StrainName = sql_return.Item(3).Value.ToString();
            //				string RoomID = sql_return.Item(4).Value.ToString(); //unused, grabbed from above

            //				// Prepare to run a SQL statement.
            //				SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            //				// Count how many current records exist within the database.
            //				oRecordSet.DoQuery("SELECT [WhsCode], [WhsName] FROM [OWHS] WHERE [U_NSC_WhrsType] = 'QND'");

            //				oRecordSet.MoveFirst();

            //				string DestructionDestinationCode = "";
            //				// Add allowed warehouses to the drop down
            //				for (int i = 0; i < oRecordSet.RecordCount; i++)
            //				{
            //					try
            //					{
            //						DestructionDestinationCode = oRecordSet.Fields.Item(0).Value.ToString();
            //					}
            //					catch
            //					{
            //					}
            //					oRecordSet.MoveNext();
            //				}

            //				double differceOfQuantity = QuantityOnProductOrder - QuantityToCreate;
            //				BatchInfo batchInfo = new BatchInfo()
            //				{
            //					DestinationWarehouseCode = DestructionDestinationCode,
            //					ItemCode = ItemCode,
            //					Quantity = differceOfQuantity,
            //					StateID = SourceID,
            //					SourceWarehouseCode = SourceWarehouseCode,
            //					UniqueID = BatchNumber
            //				};

            //				#endregion

            //				if (QuantityOnProductOrder > QuantityToCreate)
            //				{
            //					// We need to do an Inventory Split of the current batch.. And destory it.
            //					CreateBatchSplitAndScheduleDestruction(batchInfo);
            //				}

            //				#region State API biotrack calls
            /*	// COMPLIANCE
                                // Get our Settings controller for the State Information for Complain calls.
                                NSC_DI.UTIL.Settings controllerSettings = new Controllers.Settings(
                                    SAPBusinessOne_Application: Globals.oApp,
                                    SAPBusinessOne_Company: Globals.oCompany,
                                    SAPBusinessOne_Form: _SBO_Form);

                                // Add the API Call into our Compliance monitor.
                                Controllers.Compliance controllerCompliance = new Controllers.Compliance(SAPBusinessOne_Application: Globals.oApp,
                                    SAPBusinessOne_Company: Globals.oCompany);

                                // License_Number of this Facility
                                string LocationId = controllerSettings.GetValueOfSetting("StateLocLic");

                                // Prepare a connection to the state API
                                var TraceCon = new NSCNSC_DI.SAP._DI.UTIL.Misc.KillObject(oForm);
                                TraceabilityAPI(SAPBusinessOne_Application: Globals.oApp,
                                    SAPBusinessOne_Company: Globals.oCompany,
                                    SAPBusinessOne_Form: _SBO_Form);

                                BioTrack.API btAPI = TraceCon.new_API_obj();

                                //If an Item is a Plant we need to create an API call for a new Plant.
                                BioTrack.Plant.New(
                                                   BioTrackAPI: ref btAPI,
                                    Location: LocationId,
                                    Source: SourceID,
                                    Quantity: QuantityToCreate,
                                    Room: DestinationWarehouseCode,
                                    Strain: StrainName,
                                    Mother: 0);

                                string comp_code = controllerCompliance.CreateComplianceLineItem(
                                                                                                 Reason: Models.Compliance.ComplianceReasons.Plant_New,
                                    API_Call: btAPI.XmlApiRequest.ToString());

                                btAPI.PostToApi();

                                if (btAPI.WasSuccesful)
                                {
                                    controllerCompliance.UpdateComliancy(
                                                                         ComplianceID: comp_code,
                                        ResponseXML: btAPI.xDocFromResponse.ToString(),
                                        Status: Controllers.Compliance.ComplianceStatus.Success);

                                    #region Update newly created plants with newly created state ids

                                    XmlNodeList bcodeXML = btAPI.xmlDocFromResponse.SelectNodes("/xml/barcode_id");

                                    SAP_BusinessOne.Helpers.SQL sql_help2 = new SAP_BusinessOne.Helpers.SQL(Globals.oCompany);
                                    string sql_query2 = string.Format(@"
            SELECT
            [SRI1].[ItemCode]
            ,[SRI1].[SysSerial]
            FROM
            [SRI1]
            JOIN
            [IGN1]
            ON
            [SRI1].[BaseEntry] = [IGN1].[DocEntry]
            AND [SRI1].[ItemCode] = [IGN1].[ItemCode]
            JOIN
            [OSRN]
            ON [OSRN].[ItemCode] = [SRI1].[ItemCode]
            AND [OSRN].[SysNumber] = [SRI1].[SysSerial]
            WHERE
            [IGN1].[BaseRef] = '{0}'", CurrentlySelectedProductionOrder());
                                    Recordset sql_return2 = sql_help2.GetRecordSetFromSQLQuery(sql_query2);

                                    for (int i = 0; i < sql_return2.RecordCount; i++)
                                    {
                                        //
                                        string Plant_ItemCode = sql_return2.Fields.Item(0).Value.ToString();
                                        string Plant_Serial = sql_return2.Fields.Item(1).Value.ToString();

                                        #region get Doc entry from Plant_ItemCode and Plant_Serial

                                        string SQL_Query_GetDocEntryFromItemCodeAndSysNum = @"
            SELECT
            [AbsEntry]
            FROM
            [OSRN]
            WHERE
            [ItemCode] = '" + Plant_ItemCode + "' AND [SysNumber] = '" + Plant_Serial + "'";
                                        SAP_BusinessOne.Helpers.SQL VirSci_Helpers_SQL = new SAP_BusinessOne.Helpers.SQL(SAPBusinessOne_Company: Globals.oCompany);

                                        int DocEntry = Convert.ToInt32(VirSci_Helpers_SQL.GetFieldsFromSQLQuery(SQL_Query_GetDocEntryFromItemCodeAndSysNum).Item(0).Value.ToString());
                                        // Prepare a Serial Number Details Service
                                        SerialNumberDetailsService oSerialNumberDetailsService = (Gllobals.oCompany.GetCompanyService();
                                        obals).
                                        GetBusinessService(ServiceTypes.SerialNumberDetailsService) as SerialNumberDetailsService;

                                        // Prepare the parameters for the serial number details
                                        SerialNumberDetailParams oSerialNumberDetailParams =
                                            oSerialNumberDetailsService.GetDataInterface(SerialNumberDetailsServiceDataInterfaces.sndsSerialNumberDetailParams) as SerialNumberDetailParams;

                                        // Set the DocEntry to the number we found through SQL
                                        oSerialNumberDetailParams.DocEntry = DocEntry;

                                        // Locate the record we are attempting to edit
                                        SerialNumberDetail oSerialNumberDetail = oSerialNumberDetailsService.Get(oSerialNumberDetailParams);

                                        #endregion

                                        // Set the new plant state
                                        oSerialNumberDetail.UserFields.Item("U_" + Globals.SAP_PartnerCode + "_StateID").Value = bcodeXML[i].InnerText;

                                        // Attempt to update the serialized items information
                                        oSerialNumberDetailsService.Update(oSerialNumberDetail);

                                        sql_return2.MoveNext();
                                    }

                                    #endregion
                                }
                                else
                                {
                                    controllerCompliance.UpdateComliancy(
                                                                         ComplianceID: comp_code,
                                        ResponseXML: btAPI.xDocFromResponse.ToString(),
                                        Status: Controllers.Compliance.ComplianceStatus.Failed);

                                    // Send a message to the client about the fail of the document creation
                                    Globals.oApp.StatusBar.SetText("Failed to call state!"
                                        );
                                    return;
                                }
            */
            //#endregion

            //}
            //catch (Exception ex)
            //{
            //	// API-SAP-TODO: API Log error. Since this is a Compliance call with return value handling, we would have to handle this error more urgently.

            //	//Globals.oApp.StatusBar.SetText("Failed to call state!"
            //	//	);
            //}

            //#endregion
        }

        public void CreateBatchSplitAndScheduleDestruction_OLD(BatchInfo batchInfoToSplit)
        {
/*	// COMPLIANCE
			#region Create BioTrack Controllers

            // Get our Settings controller for the State Information for Complain calls.
            NSC_DI.UTIL.Settings controllerSettings = new Controllers.Settings(
                SAPBusinessOne_Application: Globals.oApp,
                SAPBusinessOne_Company: Globals.oCompany,
                SAPBusinessOne_Form: _SBO_Form);

            // Add the API Call into our Compliance monitor.
            Controllers.Compliance controllerCompliance = new Controllers.Compliance(SAPBusinessOne_Application: Globals.oApp, SAPBusinessOne_Company: Globals.oCompany);

            // License_Number of this Facility
            string LocationId = controllerSettings.GetValueOfSetting("StateLocLic");

            // Prepare a connection to the state API
            var TraceCon = new NSCNSC_DI.SAP._DI.UTIL.Misc.KillObject(oForm);TraceabilityAPI(SAPBusinessOne_Application: Globals.oApp,
                SAPBusinessOne_Company: Globals.oCompany,
                SAPBusinessOne_Form: _SBO_Form);

            BioTrack.API btAPI = TraceCon.new_API_obj();

            #endregion

            List<BioTrack.Inventory.InvLotCreate> inventoryLotsToSplit = new List<BioTrack.Inventory.InvLotCreate>();

            List<Controllers.GoodsReceipt.GoodsReceipt_Lines> goodsReceiptLines = new List<Controllers.GoodsReceipt.GoodsReceipt_Lines>();

            List<Controllers.GoodsIssued.GoodsIssued_Lines> goodsIssueLines = new List<Controllers.GoodsIssued.GoodsIssued_Lines>();

            // Prepare a "Batch Item" controller
            Controllers.BatchItems controllerBatchItems = new Controllers.BatchItems(SAPBusinessOne_Application: Globals.oApp, SAPBusinessOne_Company: Globals.oCompany);


            BioTrack.Inventory.InvLotCreate lotSplit = new BioTrack.Inventory.InvLotCreate()
            {
                BarcodeID = batchInfoToSplit.StateID,
                RemoveQuantity = batchInfoToSplit.Quantity,
                RemoveQuantityUOM = BioTrack.API.UnitOfMeasurement.g
            };

            inventoryLotsToSplit.Add(lotSplit);


            BioTrack.Inventory.Split(BioTrackAPI: ref btAPI, data: inventoryLotsToSplit.ToArray());

            string compID = controllerCompliance.CreateComplianceLineItem(
                   Reason: NavSol.Models.Compliance.ComplianceReasons.Inventory_Split,
                   API_Call: btAPI.XmlApiRequest.ToString());

            btAPI.PostToApi();

            // Get our Split Inventory List from BioTrack
            if (btAPI.WasSuccesful)
            {
                // Update the compliance monitor with a failure
                controllerCompliance.UpdateComliancy(
                    ComplianceID: compID,
                    ResponseXML: btAPI.xDocFromResponse.ToString(),
                    Status: Controllers.Compliance.ComplianceStatus.Success
                );

                XmlNodeList responseBarcodesXML = btAPI.xmlDocFromResponse.SelectNodes("/xml/barcode_id");

                BatchInfo newBatchItem = batchInfoToSplit;

                // For each state id we got back we need to associate it with the batch we are creating in sap for the seperation.
                for (int i = 0; i < responseBarcodesXML.Count; i++)
                {
                    string newStateId = responseBarcodesXML[i].InnerText;
                    string oldStateId = inventoryLotsToSplit[i].BarcodeID;

                    newBatchItem.UniqueID = controllerBatchItems.GetNextAvailableBatchID(ItemCode: newBatchItem.ItemCode, Prefix: "B");

                    //Update the Batched Item with its new State Id.
                    newBatchItem.StateID = newStateId;

                    // Should never be null.. but just in case.
                    if (newBatchItem == null)
                        continue;

                    newBatchItem.StateID = newStateId;

                    Controllers.GoodsReceipt.GoodsReceipt_Lines newGoodsReceipt = new Controllers.GoodsReceipt.GoodsReceipt_Lines()
                    {
                        ItemCode = newBatchItem.ItemCode,
                        Quantity = newBatchItem.Quantity,
                        WarehouseCode = newBatchItem.SourceWarehouseCode,
                        ListOfBatchLineItems = new List<Controllers.GoodsReceipt.GoodsReceipt_Lines.BatchItem>()
                        {
                            new Controllers.GoodsReceipt.GoodsReceipt_Lines.BatchItem()
                            {
                                BatchNumber = newBatchItem.UniqueID,
                                Quantity = newBatchItem.Quantity,
                                UserDefinedFields = new Dictionary<string, string>()
                                {
                                    {"U_VSC_StateID", newStateId}
                                }
                            }
                        } 
                    };

                    goodsReceiptLines.Add(newGoodsReceipt);
                }
           
                //Create the goods Receipt for the new Split items.
                Controllers.GoodsReceipt controllerGoodsReceipt = new Controllers.GoodsReceipt(
                    SAPBusinessOne_Application: Globals.oApp, SAPBusinessOne_Company: Globals.oCompany);

                controllerGoodsReceipt.Create(DateTime.Now, goodsReceiptLines);
                if (!controllerGoodsReceipt.WasSuccessful)
                {
                    Globals.oApp.StatusBar.SetText("Failed to split inventory! : Goods Receipt", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning
                    );

                    return;
                }


                ScheduleDestruction(newBatchItem);
            }
*/
        }

		public void CreateBatchSplitAndScheduleDestruction(BatchInfo batchInfoToSplit)
		{
			//Create the goods Receipt for the new Split items.
		}

		public void ScheduleDestruction(BatchInfo batchInfo)
        {
            #region Create BioTrack Controllers
/*	// COMPLIANCE
            // Get our Settings controller for the State Information for Complain calls.
            NSC_DI.UTIL.Settings controllerSettings = new Controllers.Settings(
                SAPBusinessOne_Application: Globals.oApp,
                SAPBusinessOne_Company: Globals.oCompany,
                SAPBusinessOne_Form: _SBO_Form);

            // Add the API Call into our Compliance monitor.
            Controllers.Compliance controllerCompliance = new Controllers.Compliance(SAPBusinessOne_Application: Globals.oApp, SAPBusinessOne_Company: Globals.oCompany);

            // License_Number of this Facility
            string LocationId = controllerSettings.GetValueOfSetting("StateLocLic");

            // Prepare a connection to the state API
            var TraceCon = new NSC_DI.SAP.TraceabilityAPI(SAPBusinessOne_Application: Globals.oApp,
                SAPBusinessOne_Company: Globals.oCompany,
                SAPBusinessOne_Form: _SBO_Form);

            BioTrack.API btAPI = TraceCon.new_API_obj();
*/
            #endregion

            // Grab data from the selected matrix row.
            string itemCode = batchInfo.ItemCode;
            double quantity = batchInfo.Quantity;
            string stateId = batchInfo.StateID;
            string uniqueId = batchInfo.UniqueID;
            string sourceWarehouseCode = batchInfo.SourceWarehouseCode;
            string destinationWarehouseCode = batchInfo.DestinationWarehouseCode;

			NSC_DI.SAP.BatchItems.StockTransferCreate stockTransferDocument = new NSC_DI.SAP.BatchItems.StockTransferCreate(sourceWarehouseCode, "Transfering Serial/Batch Items from " + sourceWarehouseCode + " to " + destinationWarehouseCode);

            stockTransferDocument.DocDate = DateTime.Now;
            stockTransferDocument.TaxDate = DateTime.Now;
            stockTransferDocument.FromWarehouse = sourceWarehouseCode;
			stockTransferDocument.StockTransferLines = new List<NSC_DI.SAP.BatchItems.StockTransferLine>();

			stockTransferDocument.StockTransferLines.Add(new NSC_DI.SAP.BatchItems.StockTransferLine()
            {
                BatchNumber = uniqueId,
                DestinationWarehouseCode = destinationWarehouseCode,
                ItemCode = itemCode,
                Quantity = quantity
            });

            // Transfer Inventory
			NSC_DI.SAP.BatchItems.CreateStockTransfer(stockTransferDocument);

            foreach (var transferLines in stockTransferDocument.StockTransferLines)
            {
				NSC_DI.SAP.BatchItems.ChangeQuarantineStatus(transferLines.BatchNumber, NSC_DI.Globals.QuarantineStates.DEST);
            }

            // Complaincy Call.
            string reasonForDestruction = "Clones that didn't make it";

            List<string> barcodes = new List<string>();
            barcodes.Add(stateId);
/*	// COMPLIANCE
            BioTrack.Inventory.Destroy_Schedule(ref btAPI, Reason: reasonForDestruction, BarcodeID: barcodes.ToArray());

            string compID = controllerCompliance.CreateComplianceLineItem(
                Reason: Models.Compliance.ComplianceReasons.Inventory_Destroy_Schedule,
                API_Call: btAPI.XmlApiRequest.ToString()
             );

            btAPI.PostToApi();

            if (btAPI.WasSuccesful)
            {
                this.UpdateProgressBar(string.Format("Complaincy call succeeded"), 1);
                controllerCompliance.UpdateComliancy(
                    ComplianceID: compID,
                    ResponseXML: btAPI.xDocFromResponse.ToString(),
                    Status: Controllers.Compliance.ComplianceStatus.Success);
            }
            else
            {
                this.UpdateProgressBar(string.Format("Complaincy call failed"), 1);
                controllerCompliance.UpdateComliancy(
                    ComplianceID: compID,
                    ResponseXML: btAPI.xDocFromResponse.ToString(),
                    Status: Controllers.Compliance.ComplianceStatus.Failed);
            }
*/
        }

		#region Does Not Change

		/// <summary>
		/// Loads the Matrix with data given a production order status
		/// </summary>
		/// <param name="status"></param>
		private void Load_Matrix()
		{
            Load_Matrix(_SBO_Form);

        }

        private static void Load_Matrix(Form pForm)
        {
            try
            {

                // OLD - uses item group
                //				var sql = @" 
                //SELECT [OWOR].[DocEntry], [OWOR].[DocNum], [OITM].[ItemName], [OWOR].[ItemCode], [OWOR].[PlannedQty], [OWOR].[CmpltQty], [OWOR].[CreateDate], [OWOR].[Warehouse], [OWOR].[Warehouse]  AS [destinationWarehouse]
                //FROM [OWOR]
                //JOIN [OITM] ON [OITM].[ItemCode] = [OWOR].[ItemCode]
                //JOIN [OITB] ON [OITB].[ItmsGrpCod] = [OITM].[ItmsGrpCod]
                //WHERE [OWOR].[Status] = 'R' AND [OITB].[ItmsGrpNam] = '" + cItemType + @"'
                //ORDER BY CONVERT(int, [OWOR].[DocNum]) DESC";

                // OLD - uses item property
                var propField = NSC_DI.SAP.Items.Property.GetFieldName(cItemType);

                // FIX ********** RETURNS DUPLICATE ROWS
                var sql = $@" 
SELECT DISTINCT [OWOR].[DocEntry], [OWOR].[DocNum], [OITM].[ItemName], [OWOR].[ItemCode], [OWOR].[PlannedQty], [OWOR].[CmpltQty], [OBTN].[InDate], [OWOR].[Warehouse], 
[OWOR].[Warehouse]  AS [destinationWarehouse],OBTN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName], OWOR.U_NSC_ProductionBatchID AS LotNum
,T1.WhsName  as [destWhsName]
,[@NSC_STRAIN].U_PrjctClone  - DATEDIFF(day,[OWOR].[CreateDate],CURRENT_TIMESTAMP) as 'DaysLeft'
,T1.WhsName  as [destWhsName]
,OWHS.WhsName  as [curLoc]
, [IBT1].[BatchNum] AS [BatchID], [OBTN].[MnfSerial] AS [StateID]
FROM [OWOR]
JOIN [OITM] ON [OITM].[ItemCode] = [OWOR].[ItemCode]
Join [WOR1] on [WOR1].[DocEntry] = [OWOR].[DocNum]   
Join [OWHS] on [WOR1].wareHouse =OWHS.WhsCode 
 join [OWHS] T1 on [OWOR].[Warehouse] = T1.WhsCode
 Join [@NSC_STRAIN] on [OWOR].ItemCode = [@NSC_STRAIN].Code
 JOIN [IBT1] ON IBT1.BsDocType = {Convert.ToInt32(SAPbobsCOM.BoObjectTypes.oProductionOrders).ToString()} AND IBT1.BsDocEntry = [OWOR].[DocEntry] AND IBT1.BsDocLine = [WOR1].[LineNum]
 JOIN OBTN ON OBTN.ItemCode = WOR1.ItemCode AND OBTN.DistNumber = IBT1.BatchNum
left Join OPMG on OPMG.AbsEntry = OBTN.U_NSC_CropID
WHERE [OWOR].[Status] = 'R' AND [OITM].[{propField}] = 'Y'
and [OWHS].U_NSC_WhrsType='CLO' and [WOR1].PlannedQty>0
AND U_NSC_Process = '{cPdoProcess}'";
                if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})";//10823-2

                sql +=  $@"ORDER BY [OWOR].[DocEntry], [OWOR].[DocNum], [OITM].[ItemName], [OWOR].[ItemCode], [OWOR].[PlannedQty], [OWOR].[CmpltQty], [OBTN].[InDate], [OWOR].[Warehouse], 
[destinationWarehouse],T1.WhsName,[DaysLeft],OWHS.WhsName,OBTN.U_NSC_CropID,OPMG.NAME";

                // Load plants into the matrix
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "OWOR", "MTX_ITEMS",
                              new List<CommonUI.Matrix.MatrixColumn>() {
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="DocNum",                Caption="Production Order",      ColumnWidth=80, ItemType = BoFormItemTypes.it_LINKED_BUTTON  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemName",              Caption="Item Name",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="curLoc",                Caption="Current Loc.",          ColumnWidth=40, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="itemCode",              Caption="Item Code",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="PlannedQty",            Caption="Planned",               ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="CmpltQty",              Caption="Completed",             ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="InDate",                Caption="Created On",            ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="DaysLeft",              Caption="Days Left",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="Warehouse",             Caption="Warehouse ID",          ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="destWhsName",           Caption="To Warehouse",          ColumnWidth=40, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="destinationWarehouse",  Caption="Destination Warehouse", ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="DocEntry",              Caption="DocEntry",              ColumnWidth=0, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="BatchID",               Caption="Batch",                 ColumnWidth=60, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="LotNum",                Caption ="Lot Number",           ColumnWidth=60, ItemType = BoFormItemTypes.it_EDIT},
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="StateID",               Caption="State ID",              ColumnWidth=60, ItemType = BoFormItemTypes.it_EDIT  }
                                    ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropID",               Caption="Crop ID",               ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                                    ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CropName",             Caption="Crop Name",             ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }


                              }, sql);


                //--,(SELECT [WOR1].[wareHouse] FROM [WOR1] WHERE [WOR1].[DocEntry] = [OWOR].[DocNum]) AS [destinationWarehouse]

                // Grab the matrix from the form UI
                Matrix matrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", pForm) as Matrix;

                // Allow multi-selection on the Matrix
                matrix.SelectionMode = BoMatrixSelect.ms_Single;

                // Update the combo button box options available actions are
                Load_AvailableActions(pForm);
            }
            catch (Exception ex)
            {
                pForm.Close();
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        private int CurrentlySelectedProductionOrder()
        {
            // Grab the Matrix from the form 
            Matrix matrix = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", _SBO_Form);

            // Prepare getting the key from the selected row
            int key;

            // Grab the production order key from the selected row
            int.TryParse(CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Production Order"), out key);

            return key;
        }

		private void MAIN_MATRIX_LINK_PRESSED(ItemEvent SAP_UI_ItemEvent)
		{
			try
			{
				// Attempt to grab the selected row ID
				int SelectedRowID = SAP_UI_ItemEvent.Row;
				if (SelectedRowID < 1) return;

				// Which column stores the ID
				int ColumnIDForIDOfItemSelected = 0;

				// Get the ID of the note selected
				string ItemSelected = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ITEMS", _SBO_Form).Columns.Item(ColumnIDForIDOfItemSelected).Cells.Item(SelectedRowID).Specific.Value.ToString();

				// Open up the Production Order Master Data form
				Globals.oApp.ActivateMenuItem("4369");

				// Grab the "Production Order" form from the UI
				Form formProductionOrder = Globals.oApp.Forms.GetForm("65211", 0);

				// Freeze the "Production Order" form
				formProductionOrder.Freeze(true);

				// Change the "Production Order" form to find mode
				formProductionOrder.Mode = BoFormMode.fm_FIND_MODE;

				// Insert the Production Order ID in the appropriate text field
				((EditText)formProductionOrder.Items.Item("18").Specific).Value = ItemSelected;

				// Click the "Find" button
				((Button)formProductionOrder.Items.Item("1").Specific).Item.Click();

				// Un-Freeze the "Production Order" form
				formProductionOrder.Freeze(false);
			}

			catch (Exception ex)
			{
				_SBO_Form.Close();
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				//NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
				GC.Collect();
			}
		}

        private void UpdateProgressBar(string Message, int ProgressToIncreaseBy = 0)
        {
            try
            {
				//// Set progress bar message
				//progressBar.Text = Message;

				//// If we are increasing the progress bar size
				//if (ProgressToIncreaseBy > 0)
				//{
				//	// Calculate the new progress
				//	int NewProgressBarValue = progressBar.Value + ProgressToIncreaseBy;

				//	// Make sure our new progress amount is not greater than the maxium allowed
				//	if (NewProgressBarValue <= progressBar.Maximum)
				//	{
				//		progressBar.Value = NewProgressBarValue;
				//	}
				//}
            }
            catch { }
        }

        /// <summary>
        /// Loads available actions in the combobutton based off of a tab view passed to it
        /// </summary>
        /// <param name="TabView"></param>
        /// CHANGE: Change the available actions on every form
        private void Load_AvailableActions()
        {
            Load_AvailableActions(_SBO_Form);
        }

        private static void Load_AvailableActions(Form pForm)
        {
            ButtonCombo CBT_ACTION = null;

            try
            {
                CBT_ACTION = pForm.Items.Item("CBT_ACTION").Specific as ButtonCombo;

                // If items already exist in the drop down
                if (CBT_ACTION.ValidValues.Count > 0)
                {
                    // Remove all currently existing values from warehouse drop down
                    for (int i = CBT_ACTION.ValidValues.Count; i-- > 0;)
                    {
                        CBT_ACTION.ValidValues.Remove(i, BoSearchKey.psk_Index);
                    }
                }

                //CBT_ACTION.ValidValues.Add("RECEIPT", "Receipt Items");
                CBT_ACTION.ValidValues.Add("RECEIPT_CLOSE", "Receipt & Close");
                CBT_ACTION.Select("RECEIPT_CLOSE");

                //CBT_ACTION.Caption = "Actions";
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(CBT_ACTION);
                GC.Collect();
            }
        }

        private static IEnumerable<string> GetSNs_and_Batch()
		{
			try
			{
                // ****** BE SURE TO USE A TRANSACTION AROUND ALL OF THE UPDATES
                // BECAUSE THE NEXT NUMBER SUBS ARE UPDATING A TABLE AND WE WANT THAT UPDATE TO ROLLBACK IN THE CASE OF AN ERROR.

                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

				// BATCH
				// single
				var batch = NSC_DI.SAP.BatchItems.NextBatch("H-1");
				//Console.WriteLine(batch.Single());

				// multiple
				//var batches = NSC_DI.SAP.BatchItems.NextBatch("H-1", "B", 10);
				//foreach (string bat in batches)
				//{
				//	Console.WriteLine(bat);
				//}

				// SERIAL NUMBER
				var SNs = NSC_DI.UTIL.AutoStrain.NextSN("H-5", "Serial Plant", 10);
				foreach(string sn in SNs)
				{
					Console.WriteLine(sn);
				}

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                return SNs;

            }
			catch (Exception ex)
			{
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				//NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
				GC.Collect();
			}
		}

		#endregion
	}
}