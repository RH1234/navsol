﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using System.Linq;

namespace NavSol.Forms.Propagation
{
    class F_CloningWiz : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_CLONING_WIZ";

        public const string CLONE_WAREHOUSE_TYPE = "CLO";			// TODO: **** POSSIBLE ERROR WITH 'CLO' . NEEDS TO BE IN A TABLE
		public const string ITEM_GROUP_NAME		 = "('Clone')";
        private bool usingPkgdClones = false; // global bool to see if the form is using the pkg clone grd or the clone/plant grid

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public Form _SBO_Form;
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
	    [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
	    public virtual bool OnBeforeItemPressed(ItemEvent pVal)
	    {
		    bool BubbleEvent = true;
		    Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
		    _SBO_Form = oForm;

		    switch (pVal.ItemUID)
		    {
                case "1":
                    if (pVal.FormMode == (int)BoFormMode.fm_UPDATE_MODE)
                    {
                        if (usingPkgdClones)
                            BTN_UPDATE_CLICK_PkgClne(oForm);
                        else
                            BTN_UPDATE_Click(oForm);
                    }
                    break;
                case "BTN_FLTR":// 11046
                    LoadGrid(oForm);
                    break;              
		    }

		    NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();

		    return BubbleEvent;
	    }
        
        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, true, new string[] { cFormID })]
        public virtual bool OnBeforeChooseFromList(ItemEvent pVal)
        {
            Grid oGrd = null;
            bool BubbleEvent = true;
            if (pVal.ActionSuccess == true) return false;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            var pFormID = oForm.UniqueID; // Save unique id to get form in callback
            try
            {
                switch (pVal.ItemUID)
                {
                    // Crop/Project
                    case "TXT_CROP":
                        string strCrop = NSC_DI.UTIL.SQL.GetValue<string>("Select T0.U_Value1 as [CropCycle] from [@NSC_USER_OPTIONS] T0 where T0.Name='Crop Cycle Management'");
                        if (strCrop == "Y")
                        {
                             SAPbouiCOM.DataTable dtCropProj = null;
                            try
                            {
                                if (oForm.DataSources.DataTables.Item("CropSrc").IsEmpty == false)
                                {
                                    oForm.DataSources.DataTables.Item("CropSrc").Clear();
                                    dtCropProj = oForm.DataSources.DataTables.Item("CropSrc");
                                }
                            }
                            catch
                            {
                                dtCropProj = oForm.DataSources.DataTables.Add("CropSrc");
                            }

                            dtCropProj.ExecuteQuery(@"Select T0.DocNum as [Crop ID],T0.NAME,T0.START as [Start Date],T0.DUEDATE [End Date] from [OPMG] T0 where T0.STATUS!='F'");

                            // open the form; 
                            Forms.Crop.CropCycleSelect.FormCreate(pFormID, dtCropProj);

                            Forms.Crop.CropCycleSelect.CropProjCB = delegate (string callingFormUid, System.Data.DataTable pdtCropProj)
                            {
                                Form oFormDel = null;

                                oFormDel = Globals.oApp.Forms.Item(pFormID) as Form;
                                if (pdtCropProj.Rows.Count == 0) return;

                                System.Data.DataRow row = pdtCropProj.Rows[0];
                                var strCropProjCode = row.ItemArray.GetValue(0);
                                var strCropProjName = row.ItemArray.GetValue(1);

                                oForm = Globals.oApp.Forms.Item(pFormID);
                                oForm.DataSources.UserDataSources.Item("UDS_CRO").ValueEx = strCropProjName.ToString(); ;

                                SAPbouiCOM.EditText oEdit = oForm.Items.Item("TXT_CROC").Specific;
                                oEdit.Value = strCropProjCode.ToString();

                                oEdit = oForm.Items.Item("TXT_CROF").Specific;
                                //string strFinHolder= NSC_DI.UTIL.SQL.GetValue<string>("Select T0.FIPROJECT from [OPMG] T0 where T0.DocNum =" + strCropProjCode);
                                oEdit.Value = NSC_DI.UTIL.SQL.GetValue<string>("Select T0.FIPROJECT from [OPMG] T0 where T0.DocNum =" + strCropProjCode);

                                //Show dropdown
                                ShowAndLoad_StageCMB(oForm, Convert.ToInt32(strCropProjCode));
                                LoadGrid_Stage(oForm);
                            };

                            BubbleEvent = false;
                        }
                        break;

                    // Crop/Project - GRID
                    case "grdMoms":
                        if (pVal.ColUID == "CropCycle" && NSC_DI.UTIL.Options.Value.GetSingle("Crop Cycle Management") == "Y")
                        {
                            SAPbouiCOM.DataTable dtCropProj = null;
                            try
                            {
                                if (oForm.DataSources.DataTables.Item("CropSrc").IsEmpty == false)
                                {
                                    oForm.DataSources.DataTables.Item("CropSrc").Clear();
                                    dtCropProj = oForm.DataSources.DataTables.Item("CropSrc");
                                }
                            }
                            catch
                            {
                                dtCropProj = oForm.DataSources.DataTables.Add("CropSrc");
                            }

                            dtCropProj.ExecuteQuery(@"Select T0.DocNum as [Crop ID],T0.NAME,T0.START as [Start Date],T0.DUEDATE [End Date] from [OPMG] T0 where T0.STATUS!='F'");

                            int iRow = pVal.Row;

                            // open the form; 
                            Forms.Crop.CropCycleSelect.FormCreate(pFormID, dtCropProj);

                            Forms.Crop.CropCycleSelect.CropProjCB = delegate (string callingFormUid, System.Data.DataTable pdtCropProj)
                            {
                                Form oFormDel = null;

                                oFormDel = Globals.oApp.Forms.Item(pFormID) as Form;
                                if (pdtCropProj.Rows.Count == 0) return;

                                System.Data.DataRow row = pdtCropProj.Rows[0];
                                var strCropProjCode = row.ItemArray.GetValue(0);
                                var strCropProjName = row.ItemArray.GetValue(1);

                                oForm = Globals.oApp.Forms.Item(pFormID);

                                //oForm.DataSources.UserDataSources.Item("UDS_CRO").ValueEx = strCropProjName.ToString(); ;
                                oGrd = oForm.Items.Item("grdMoms").Specific;
                                oGrd.DataTable.SetValue("CropCycle", iRow, strCropProjName.ToString());

                                //SAPbouiCOM.EditText oEdit = oForm.Items.Item("TXT_CROC").Specific;
                                //oEdit.Value = strCropProjCode.ToString();
                                oGrd.DataTable.SetValue("CROC", iRow, strCropProjCode.ToString());

                                //oEdit = oForm.Items.Item("TXT_CROF").Specific;
                                ////string strFinHolder= NSC_DI.UTIL.SQL.GetValue<string>("Select T0.FIPROJECT from [OPMG] T0 where T0.DocNum =" + strCropProjCode);
                                //oEdit.Value = NSC_DI.UTIL.SQL.GetValue<string>("Select T0.FIPROJECT from [OPMG] T0 where T0.DocNum =" + strCropProjCode);
                                var val = NSC_DI.UTIL.SQL.GetValue<string>("Select T0.FIPROJECT from [OPMG] T0 where T0.DocNum =" + strCropProjCode);
                                oGrd.DataTable.SetValue("CROF", iRow, val);

                                //Show dropdown
                                //LoadGrid_Stage(oForm);    // CAN'T HAVE DIFFERENT SET OF VALUES FOR EACH ROW
                            };

                            BubbleEvent = false;
                        }
                        break;
                }
                return BubbleEvent;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return false;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
            }
        }

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeValidate(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "grdMoms") BubbleEvent = Validate_Grid(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
	    public virtual void OnAfterItemPressed(ItemEvent pVal)
	    {
		    if (pVal.ActionSuccess == false) return;

		    var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

		    switch (pVal.ItemUID)
		    {
			    case "BTN_FINISH":
				    BTN_FINISH_Click(oForm);
				    break;

			    case "BTN_NEXT":
				    BTN_NEXT_Click(oForm);
				    break;

			    case "BTN_BACK":
				    BTN_BACK_Click(oForm);
				    break;

                case "CHK_MTHR":
                    Filter_Selection(_SBO_Form);
                    break;
                case "BTN_FLTR":
                    LoadGrid(_SBO_Form);
                    break;
            }

		    NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();
	    }

		//--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
		[B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] {cFormID})]
		public virtual void OnAfterChooseFromList(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				// MotherPlant ChooseFromList
				case "TXT_MOTHR":
					TXT_MOTHR_ChooseFromList_Selected((IChooseFromListEvent)pVal);
					break;

                // Warehouse ChooseFromList "txtWH_CFL"
                case "TXT_WRHS":
					TXT_WRHS_ChooseFromList_Selected((IChooseFromListEvent)pVal);
					break;

                // Clone Warehouse ChooseFromList 
                case "txtWH_CFL":
                    CFL_Selected_CloneWH(oForm, (ChooseFromListEvent)pVal);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
        public virtual void OnAfterComboSelect(ItemEvent pVal)
        {
            if (pVal.InnerEvent == true) return;
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            var test = oForm.Items.Item("cboType").Specific.Value == "Pkgd";
            if (pVal.ItemUID.ToUpper() == "CBOTYPE" && (oForm.Items.Item("cboType").Specific.Value == "Pkgd" || usingPkgdClones))
                LoadGrid(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_FORM_RESIZE
        [B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
        public virtual void OnAfterFormResize(ItemEvent pVal)
        {

            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            FormResize(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public F_CloningWiz() : this(Globals.oApp, Globals.oCompany) { }

		public F_CloningWiz(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
		{
			_SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

        public void Form_Load()
        {
            Form oForm = CommonUI.Forms.Load(cFormID, true);
            try
            {
                // Set the main image
                CommonUI.Forms.SetFieldvalue.Logo(oForm, "IMG_HEADER", @"WizardHeader.bmp");

                // Set the due date to todays date.
                EditText TXT_DATE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DATE", oForm);
                if (TXT_DATE != null) TXT_DATE.Value = DateTime.Now.ToString("yyyyMMdd");
                
                ComboBox CMB_CloneWarehouse = ((ComboBox) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_CWRHS", oForm));

                oForm.Items.Item("CHK_MTHR").Specific.Checked = true;
                //Filter_Selection(oForm);

                // TODO: Removing for the time being. We may add this later with the option to skip the clone bank.
                //SAPbouiCOM.ComboBox CMB_PlantWarehouse = ((SAPbouiCOM.ComboBox)_VirSci_Helper_Form.GetControlFromForm(
                //   ItemType: SAP_BusinessOne.Helpers.Forms.FormControlTypes.ComboBox,
                //   ItemUID: "CMB_PWRHS",
                //   FormToSearchIn: oForm));

                // TODO: Removing for the time being. We may add this later with the option to skip the clone bank.
                //PopulatePlantWarehouseDropDown(CMB_PlantWarehouse);
                oForm.VisibleEx = true; // has to be here so that objects can be dis-abled
                oForm.Freeze(true);


                FILL_FILTER_COMBOS(oForm);

                if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y")
                {
                    NavSol.CommonUI.CFL.CreateWH(oForm, "CFL_WH", "CMB_CWRHS");
                    NavSol.CommonUI.CFL.AddCon(oForm, "CFL_WH", BoConditionRelationship.cr_NONE, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, CLONE_WAREHOUSE_TYPE);
                    NavSol.CommonUI.CFL.AddCon(oForm, "CFL_WH", BoConditionRelationship.cr_AND, "Inactive", BoConditionOperation.co_EQUAL, "N");
                    NavSol.CommonUI.CFL.AddCon_Branches(oForm, "CFL_WH", BoConditionRelationship.cr_AND);           // #10823
                }
                else
                    // Populate the Clone Warehouse dropdown
                    PopulateCloneWarehouseDropdown(CMB_CloneWarehouse);

                var useCCM = (NSC_DI.UTIL.Options.Value.GetSingle("Crop Cycle Management") == "Y");
                oForm.Items.Item("TXT_DATE").Specific.Active = true;
                oForm.Items.Item("LBL_CROP").Visible  = useCCM;
                oForm.Items.Item("TXT_CROP").Visible  = useCCM;
                oForm.Items.Item("LBL_STAGE").Visible = useCCM;
                oForm.Items.Item("CBT_STAGE").Visible = useCCM;

                // load the grid
                oForm.DataSources.DataTables.Add("dsGrid");
                LoadGrid(oForm);

                oForm.Mode = BoFormMode.fm_OK_MODE;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oForm.Freeze(false);
                //NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        private static void FormResize(Form pForm)
        {
            try
            {
                pForm.Items.Item("IMG_HEADER").Width = pForm.ClientWidth;
                pForm.Items.Item("LBL_CTYPE").Left = 435;
                pForm.Items.Item("cboType").Left = 497;
                pForm.Items.Item("CHK_MTHR").Left = 435;
                pForm.Items.Item("BTN_FLTR").Left = 570;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }

        private static void ShowAndLoad_StageCMB(Form pForm, int pProjDocNum)
        {
            //Select T0.UniqueID as [Stage ID] from PMG1 T0 where T0.AbsEntry = 40 and T0.UniqueID like 'IN_%'
            //CBT_STAGE
            SAPbouiCOM.ComboBox oCMB = pForm.Items.Item("CBT_STAGE").Specific;
            SAPbouiCOM.DataTable oDT = pForm.DataSources.DataTables.Item("dtSTA");
            oDT.Clear();
            oDT.ExecuteQuery(@"Select T0.UniqueID as [Stage ID] from PMG1 T0 where T0.AbsEntry = " + pProjDocNum + " and T0.UniqueID like 'PROP_CLONE%'");
            //if (oCMB.ValidValues.Count > 0)
            //{
            //    for (int j = oCMB.ValidValues.Count; j <= oCMB.ValidValues.Count; j--)
            //    {
            //        if (j > 0)
            //        {
            //            oCMB.ValidValues.Remove(j - 1, BoSearchKey.psk_Index);
            //        }
            //        else
            //        {
            //            //Break out of loop
            //            j = 2;
            //        }

            //    }
            //    //string strHolder = oCMB.ValidValues.Item(j).Value;
            //    //    int RecAdj = j;
            //    //    if (oCMB.ValidValues.Item(j).Value == RecAdj.ToString())
            //    //    {

            //    //}

            //}
            NavSol.CommonUI.ComboBox.DeleteVals(pForm, "CBT_STAGE");
            for (int i = 0; i < oDT.Rows.Count; i++)
            {
                oCMB.ValidValues.Add(i.ToString(), oDT.GetValue(0, i));

            }

            //LBL_STAGE
            //SAPbouiCOM.StaticText oLBL = pForm.Items.Item("LBL_STAGE").Specific;
            //oLBL.Item.Visible = true;
            //oCMB.Item.Enabled = true;

        }

        private void PopulateCloneWarehouseDropdown(ComboBox CMBControl)
        {
            Recordset oRS = null;
            try
            {
                // Prepare to run a SQL statement.
                oRS = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                // Count how many current records exist within the database.
                var sql = $@"
SELECT[OWHS].[WhsCode], [OWHS].[WhsName]
FROM[OWHS]
WHERE[OWHS].[U_{Globals.SAP_PartnerCode}_WhrsType] = '{CLONE_WAREHOUSE_TYPE}' AND[OWHS].[Inactive] = 'N'";
                if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})";     //10823

                oRS.DoQuery(sql);

                // If items already exist in the drop down
                if (CMBControl.ValidValues.Count > 0)
                {
                    // Remove all currently existing values from warehouse drop down
                    for (int i = CMBControl.ValidValues.Count; i-- > 0;)
                    {
                        CMBControl.ValidValues.Remove(i, BoSearchKey.psk_Index);
                    }
                }

                // If more than 1 warehouses exists
                if (oRS.RecordCount > 1)
                {
                    // Create the first item as an empty item
                    CMBControl.ValidValues.Add("", "");

                    // Select the empty item (forcing the user to make a decision)
                    CMBControl.Select(0, BoSearchKey.psk_Index);
                }

                // Add allowed warehouses to the drop down
                for (int i = 0; i < oRS.RecordCount; i++)
                {
                    try
                    {
                        CMBControl.ValidValues.Add(oRS.Fields.Item(0).Value.ToString(), oRS.Fields.Item(1).Value.ToString());
                    }
                    catch { }
                    CMBControl.Item.Enabled = true;
                    oRS.MoveNext();
                }

                // If we only have exactly one warehouse that contains our item simply select it and disable the field.
                if (CMBControl.ValidValues.Count == 1)
                {
                    CMBControl.Select(0, BoSearchKey.psk_Index);
                    CMBControl.Active = false;
                    CMBControl.Item.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oRS);
                GC.Collect();
            }
        }

        private void PopulatePlantWarehouseDropDown(ComboBox CMBControl)
        {
            // Prepare to run a SQL statement.
            Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            // Count how many current records exist within the database.
            oRecordSet.DoQuery(
                string.Format(@"
                SELECT DISTINCT [OITW].[WhsCode], [OWHS].[WhsName]
                FROM [OITW]
                JOIN [OWHS] on [OWHS].[WhsCode] = [OITW].[WhsCode]"));

            //WHERE [OWHS].U_{0}_WhrsType IS NOT NULL", Globals.SAP_PartnerCode));

            // If items already exist in the drop down
            if (CMBControl.ValidValues.Count > 0)
            {
                // Remove all currently existing values from warehouse drop down
                for (int i = CMBControl.ValidValues.Count; i-- > 0; )
                {
                    CMBControl.ValidValues.Remove(i, BoSearchKey.psk_Index);
                }
            }

            // If more than 1 warehouses exists
            if (oRecordSet.RecordCount > 1)
            {
                // Create the first item as an empty item
                CMBControl.ValidValues.Add("", "");

                // Select the empty item (forcing the user to make a decision)
                CMBControl.Select(0, BoSearchKey.psk_Index);
            }

            // Add allowed warehouses to the drop down
            for (int i = 0; i < oRecordSet.RecordCount; i++)
            {
                try
                {
                    CMBControl.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
                }
                catch { }
                CMBControl.Item.Enabled = true;
                oRecordSet.MoveNext();
            }

            // If we only have exactly one warehouse that contains our item simply select it and disable the field.
            if (CMBControl.ValidValues.Count == 1)
            {
                CMBControl.Select(0, BoSearchKey.psk_Index);
            }
        }

        private void TXT_MOTHR_ChooseFromList_Selected(IChooseFromListEvent pChooseFromListEvent)
        {
            SAPbouiCOM.ChooseFromList oChooseFromList = _SBO_Form.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

            if (pChooseFromListEvent.BeforeAction == false)
            {
                DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

                try
                {
                    string oItemCode = oDataTable.GetValue(0, 0).ToString();
                    string oItemSysSerial = oDataTable.GetValue(2, 0).ToString();

                    string itemName = oDataTable.GetValue("itemName", 0).ToString();

                    // Set the userdatasource for the name textbox
                    _SBO_Form.DataSources.UserDataSources.Item("UDS_MOTHR").ValueEx = Convert.ToString(itemName);

                    // Save the Mother Plant code
                    ((EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_MTHRC", _SBO_Form))
                    .Value = oItemSysSerial;

                    ((EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_MTHRI", _SBO_Form))
                    .Value = oItemCode;

                }
                catch (Exception ex)
                {

                }
            }
        }

        private void TXT_WRHS_ChooseFromList_Selected(IChooseFromListEvent pChooseFromListEvent)
        {
            SAPbouiCOM.ChooseFromList oChooseFromList = _SBO_Form.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

            if (pChooseFromListEvent.BeforeAction == false)
            {
                DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

                try
                {
                    string oWarehouseCode = oDataTable.GetValue(0, 0).ToString();
                    string oWarehouseName = oDataTable.GetValue(1, 0).ToString();

                    // Set the userdatasource for the name textbox
                    _SBO_Form.DataSources.UserDataSources.Item("UDS_WRHS").ValueEx = Convert.ToString(oWarehouseName);

                    // Save the Warehouse code
                    ((EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHSC", _SBO_Form))
                    .Value = oWarehouseCode;

                }
                catch (Exception ex)
                {

                }
            }
        }

        private void CFL_Selected_CloneWH(Form pForm, ChooseFromListEvent pVal)
        {
            if (pVal.SelectedObjects == null) return;

            try
            {
                CommonUI.CFL.SetFieldsWH(pForm, pVal, "CMB_CWRHS", "txtWH_CFL");
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void LoadGridOld()
        {
            Grid oGrd = null;
            
            try
            {
                string sql = $@"
SELECT OSRN.ItemCode, OITM.ItemName, OSRN.DistNumber AS SerialNumber, OSRN.AbsEntry,
       0 AS Quantity, GETDATE() AS Intake, SPACE(10) AS WhCode, SPACE(100) AS Warehouse, 
       SPACE(50) AS CropCycle, SPACE(20) AS CROC, SPACE(20) AS CROF, SPACE(20) AS Stage
  FROM OSRN 
 INNER JOIN OITM ON OSRN.ItemCode = OITM.ItemCode
 --INNER JOIN OSRQ ON OSRN.AbsEntry = OSRQ.AbsEntry -- this is not correct - RH 21oct2020
 INNER JOIN OSRQ ON OSRN.AbsEntry = OSRQ.MdAbsEntry AND OSRQ.Quantity > 0
 INNER JOIN OWHS ON OSRQ.WhsCode = OWHS.WhsCode
 WHERE OSRN.U_NSC_IsMother = 'Y' ";             
                
                if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})"; // 10823
                sql += " ORDER BY ItemName ASC";

                oGrd = _SBO_Form.Items.Item("grdMoms").Specific;
                oGrd.DataTable = _SBO_Form.DataSources.DataTables.Item("dsGrid");
                oGrd.DataTable.ExecuteQuery(sql);

                // right justify
                oGrd.Columns.Item("Quantity").RightJustified = true;

                // hide some columns
                oGrd.Columns.Item("ItemCode").Visible    = false;
                oGrd.Columns.Item("AbsEntry").Visible    = false;
                oGrd.Columns.Item("WhCode").Visible      = false;
                oGrd.Columns.Item("CROC").Visible        = false;
                oGrd.Columns.Item("CROF").Visible        = false;
                var useCCM = (NSC_DI.UTIL.Options.Value.GetSingle("Crop Cycle Management") == "Y");
                oGrd.Columns.Item("CropCycle").Visible   = useCCM;
                oGrd.Columns.Item("Stage").Visible       = useCCM;

                // don't allow changes for now
                oGrd.Columns.Item("Intake").Editable     = false;
                oGrd.Columns.Item("CropCycle").Editable  = false;
                oGrd.Columns.Item("Stage").Editable      = false;

                // inactivate some columns
                oGrd.Columns.Item("ItemName").Editable      = false;
                oGrd.Columns.Item("SerialNumber").Editable  = false;

                //------------------------------------
                // configure Warehouse
                sql = $@"
SELECT [OWHS].[WhsCode], [OWHS].[WhsName]
 FROM[OWHS]
LEFT JOIN OBPL ON OWHS.BPLid = OBPL.BPLid        --//10823
 WHERE[OWHS].[U_{Globals.SAP_PartnerCode}_WhrsType] = '{CLONE_WAREHOUSE_TYPE}' AND[OWHS].[Inactive] = 'N'";
                if (Globals.BranchDflt >= 0) sql += $" AND OBPL.BPLid IN ({Globals.BranchList})"; // 10823
                oGrd.Columns.Item("Warehouse").Type = BoGridColumnType.gct_ComboBox;
                ((SAPbouiCOM.ComboBoxColumn)oGrd.Columns.Item("Warehouse")).DisplayType = BoComboDisplayType.cdt_Description;
                CommonUI.ComboBox.ComboBoxAddVals(_SBO_Form, "grdMoms", "Warehouse", sql, " ");

                //------------------------------------
                // configure Stage - COMMENT THIS OUT FOR NOW - CAN'T HAVE DIFFERENT VALID VALUES FOR EACH CELL ON A GRID - ONLY FOR MATRIX
                //oGrd.Columns.Item("Stage").Type = BoGridColumnType.gct_ComboBox;
                //((SAPbouiCOM.ComboBoxColumn)oGrd.Columns.Item("Stage")).DisplayType = BoComboDisplayType.cdt_Description;

                //------------------------------------
                // configure CFL
                _SBO_Form.DataSources.UserDataSources.Add("udsCrop", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                CommonUI.CFL.Create(_SBO_Form, "cflGrdCrop", BoLinkedObject.lf_ProjectCodes);
                ((SAPbouiCOM.EditTextColumn)oGrd.Columns.Item("CropCycle")).ChooseFromListUID = "cflGrdCrop";
                ((SAPbouiCOM.EditTextColumn)oGrd.Columns.Item("CropCycle")).ChooseFromListAlias = "PrjCode";


                oGrd.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }

        private void LoadGrid(Form pForm)
        {
            Grid oGrd = null;
            
            try
            {
                pForm.Freeze(true);
                string sql = "";
                if (!usingPkgdClones && pForm.Items.Item("cboType").Specific.Value == "Pkgd")
                {
                    // 11046 sets all filter selections to unenabled by triggering the chk_mthr event and then unenabling chk_mthr 
                    pForm.Items.Item("CHK_MTHR").Specific.Checked = true;
                    pForm.Items.Item("CHK_MTHR").Enabled = false;
                    
                    sql = $@"
SELECT obtn.ItemName, oitm.ItemCode, obtn.DistNumber AS BatchNumber, obtn.AbsEntry, OBTQ.Quantity AS Quantity, GETDATE() AS Intake, SPACE(32) as 'Lot Number',
SPACE(10) AS WhCode, SPACE(100) AS Warehouse, 
       SPACE(50) AS CropCycle, SPACE(20) AS CROC, SPACE(20) AS CROF, SPACE(20) AS Stage
From obtn
inner join OITM on OBTN.ItemCode = OITM.ItemCode 
inner join obtq on obtn.AbsEntry = obtq.MdAbsEntry AND OBTQ.Quantity > 0
INNER JOIN OWHS ON OBTQ.WhsCode = OWHS.WhsCode 
where oitm.QryGroup37 = 'Y' ";

                    if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})"; // 10823
                    sql += " ORDER BY ItemName ASC";

                    oGrd = pForm.Items.Item("grdMoms").Specific;
                    oGrd.DataTable = pForm.DataSources.DataTables.Item("dsGrid");
                    oGrd.DataTable.ExecuteQuery(sql);

                    // right justify
                    oGrd.Columns.Item("Quantity").RightJustified = true;

                    // hide some columns
                    oGrd.Columns.Item("ItemCode").Visible = false;
                    oGrd.Columns.Item("AbsEntry").Visible = false;
                    oGrd.Columns.Item("WhCode").Visible = false;
                    oGrd.Columns.Item("CROC").Visible = false;
                    oGrd.Columns.Item("CROF").Visible = false;
                    var useCCM = (NSC_DI.UTIL.Options.Value.GetSingle("Crop Cycle Management") == "Y");
                    oGrd.Columns.Item("CropCycle").Visible = useCCM;
                    oGrd.Columns.Item("Stage").Visible = useCCM;

                    // don't allow changes for now
                    oGrd.Columns.Item("Intake").Editable = false;
                    oGrd.Columns.Item("CropCycle").Editable = false;
                    oGrd.Columns.Item("Stage").Editable = false;

                    // inactivate some columns
                    oGrd.Columns.Item("ItemName").Editable = false;
                    oGrd.Columns.Item("BatchNumber").Editable = false;
                    oGrd.Columns.Item("Lot Number").Editable = true;
                    //oGrd.Columns.Item("Quantity").Editable = false;

                    usingPkgdClones = true;
                }
                else
                {
                   
                    // 11046 if chk_mthr is unenabled (because the form was set to packaged clones) it is reenabled
                    if (!pForm.Items.Item("CHK_MTHR").Enabled)
                        pForm.Items.Item("CHK_MTHR").Enabled = true;

                    // 11046 Gets the values of the filter fields
                   
                    string TXT_ADM = pForm.Items.Item("TXT_ADM").Specific.Value?.ToString().Trim();
                    string CBO_STRN = pForm.Items.Item("CBT_STRN").Specific.Value?.ToString().Trim();
                    string CBO_WRHS = pForm.Items.Item("CBT_WRHS").Specific.Value?.ToString().Trim();
                    string TXT_IDNUM = pForm.Items.Item("TXT_IDNUM").Specific.Value?.ToString().Trim();
                    bool mtherChk = pForm.Items.Item("CHK_MTHR").Specific.Checked;
                    string OSRNWhere = "";
                    string OBTNWhere = "";

                    // 11046 creates extra conditions for the where clauses based on branches and filter fields
                    if (mtherChk)
                        OSRNWhere += "WHERE OSRN.U_NSC_IsMother = 'Y'";
                    if (Globals.BranchDflt >= 0)
                    {
                        OSRNWhere += WhereBuilder(OSRNWhere, $" OWHS.BPLid IN ({Globals.BranchList})"); // 10823
                        OBTNWhere += $" AND OWHS.BPLid IN ({Globals.BranchList})";
                    }                                                       
                    if (!string.IsNullOrEmpty(CBO_STRN))
                    {
                        OSRNWhere += WhereBuilder(OSRNWhere, $" OITM.U_NSC_StrainID = '{CBO_STRN}'");// 11046
                        OBTNWhere += $" AND OITM.U_NSC_StrainID = '{CBO_STRN}'";
                    }                      
                    if (!string.IsNullOrEmpty(CBO_WRHS))
                    {
                        OSRNWhere += WhereBuilder(OSRNWhere, $" OWHS.WhsCode = '{CBO_WRHS}'");// 11046
                        OBTNWhere += $" AND OWHS.WhsCode = '{CBO_WRHS}'";
                    }                       
                    if (!string.IsNullOrEmpty(TXT_IDNUM))
                    {
                        OSRNWhere += WhereBuilder(OSRNWhere, $" (OSRN.MnfSerial = '{TXT_IDNUM}' OR OSRN.DistNumber = '{TXT_IDNUM}')");// 11046
                        OBTNWhere +=  $" AND (OBTN.MnfSerial = '{TXT_IDNUM}' OR OBTN.DistNumber = '{TXT_IDNUM}')";
                    }
                    if (!string.IsNullOrEmpty(TXT_ADM))
                    {
                        TXT_ADM = NSC_DI.UTIL.Dates.FromUI(TXT_ADM).ToString();
                        OSRNWhere += WhereBuilder(OSRNWhere, $" OSRN.InDate = '{TXT_ADM}'");// 11046
                        OBTNWhere +=$" AND OBTN.InDate = '{TXT_ADM}'";
                    }

                    sql = $@"
SELECT OSRN.ItemCode, OITM.ItemName, OSRN.DistNumber AS [Serial/Batch Number],OSRN.MnfSerial AS StateID, OSRN.AbsEntry,
       0 AS Quantity, GETDATE() AS Intake, SPACE(10) AS WhCode, SPACE(100) AS Warehouse, 
       SPACE(50) AS CropCycle, SPACE(20) AS CROC, SPACE(20) AS CROF, SPACE(20) AS Stage
  FROM OSRN 
 INNER JOIN OITM ON OSRN.ItemCode = OITM.ItemCode
 --INNER JOIN OSRQ ON OSRN.AbsEntry = OSRQ.AbsEntry -- this is not correct - RH 21oct2020
 INNER JOIN OSRQ ON OSRN.AbsEntry = OSRQ.MdAbsEntry AND OSRQ.Quantity > 0
 INNER JOIN OWHS ON OSRQ.WhsCode = OWHS.WhsCode
 {OSRNWhere}";
                    if(NSC_DI.UTIL.Options.Value.GetSingle("Clone Batches") == "Y" && !mtherChk)
 sql += $@"
 UNION
SELECT DISTINCT OBTN.ItemCode, OITM.ItemName, OBTN.DistNumber, OBTN.MnfSerial, OBTN.AbsEntry,
       0 AS Quantity, GETDATE() AS Intake, SPACE(10) AS WhCode, SPACE(100) AS Warehouse, 
       SPACE(50) AS CropCycle, SPACE(20) AS CROC, SPACE(20) AS CROF, SPACE(20) AS Stage
FROM OBTN
INNER JOIN OBTQ ON OBTN.AbsEntry = OBTQ.MdAbsEntry AND OBTQ.Quantity > 0
INNER JOIN OITM ON OBTN.ItemCode = OITM.ItemCode
INNER JOIN OWHS ON OBTQ.WhsCode = OWHS.WhsCode
WHERE (OITM.QryGroup39 = 'Y' OR OITM.QryGroup41 = 'Y' OR OITM.QryGroup54 = 'Y') {OBTNWhere}";

                    sql += " ORDER BY ItemName ASC";

                    oGrd = pForm.Items.Item("grdMoms").Specific;
                    oGrd.DataTable = pForm.DataSources.DataTables.Item("dsGrid");
                    oGrd.DataTable.ExecuteQuery(sql);             

                    // right justify
                    oGrd.Columns.Item("Quantity").RightJustified = true;

                    // hide some columns
                    oGrd.Columns.Item("ItemCode").Visible = false;
                    oGrd.Columns.Item("AbsEntry").Visible = false;
                    oGrd.Columns.Item("WhCode").Visible = false;
                    oGrd.Columns.Item("CROC").Visible = false;
                    oGrd.Columns.Item("CROF").Visible = false;
                    var useCCM = (NSC_DI.UTIL.Options.Value.GetSingle("Crop Cycle Management") == "Y");
                    oGrd.Columns.Item("CropCycle").Visible = useCCM;
                    oGrd.Columns.Item("Stage").Visible = useCCM;

                    // don't allow changes for now
                    oGrd.Columns.Item("Intake").Editable = false;
                    oGrd.Columns.Item("CropCycle").Editable = false;
                    oGrd.Columns.Item("Stage").Editable = false;
                    oGrd.Columns.Item("StateID").Editable = false;

                    // inactivate some columns
                    oGrd.Columns.Item("ItemName").Editable = false;
                    oGrd.Columns.Item("Serial/Batch Number").Editable = false;
                    oGrd.Columns.Item("Quantity").Editable = true;

                    usingPkgdClones = false;
                }
                try
                {
                    // This only needs to happen on initial load
                    //------------------------------------
                    // configure Warehouse
                    sql = $@"
SELECT [OWHS].[WhsCode], [OWHS].[WhsName]
 FROM[OWHS]
LEFT JOIN OBPL ON OWHS.BPLid = OBPL.BPLid        --//10823
 WHERE[OWHS].[U_{Globals.SAP_PartnerCode}_WhrsType] = '{CLONE_WAREHOUSE_TYPE}' AND[OWHS].[Inactive] = 'N'";
                    if (Globals.BranchDflt >= 0) sql += $" AND OBPL.BPLid IN ({Globals.BranchList})"; // 10823
                    oGrd.Columns.Item("Warehouse").Type = BoGridColumnType.gct_ComboBox;
                    ((SAPbouiCOM.ComboBoxColumn)oGrd.Columns.Item("Warehouse")).DisplayType = BoComboDisplayType.cdt_Description;
                    CommonUI.ComboBox.ComboBoxAddVals(pForm, "grdMoms", "Warehouse", sql, " ");

                    //------------------------------------
                    // configure CFL
                    _SBO_Form.DataSources.UserDataSources.Add("udsCrop", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);
                    CommonUI.CFL.Create(_SBO_Form, "cflGrdCrop", BoLinkedObject.lf_ProjectCodes);
                    ((SAPbouiCOM.EditTextColumn)oGrd.Columns.Item("CropCycle")).ChooseFromListUID = "cflGrdCrop";
                    ((SAPbouiCOM.EditTextColumn)oGrd.Columns.Item("CropCycle")).ChooseFromListAlias = "PrjCode";

                }
                catch {}              

                oGrd.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
                pForm.Freeze(false);
            }
           
        }

        private bool Validate_Grid(Form pForm, ItemEvent pVal)
        {
            Grid oGrd = null;

            try
            {
                if (pVal.ItemChanged == false) return true;

                oGrd = pForm.Items.Item("grdMoms").Specific;
                if(pVal.ColUID == "Quantity")
                {
                    pForm.Freeze(true);
                    double qty = oGrd.DataTable.GetValue("Quantity", pVal.Row);
                    if (qty < 0) return false;
                    if (qty == 0)
                    {
                        oGrd.DataTable.SetValue("Intake",    pVal.Row, "");
                        oGrd.DataTable.SetValue("Warehouse", pVal.Row, " ");
                        oGrd.DataTable.SetValue("CropCycle", pVal.Row, "");
                        oGrd.DataTable.SetValue("Stage",     pVal.Row, "");
                        oGrd.DataTable.SetValue("CROC",      pVal.Row, "");
                        oGrd.DataTable.SetValue("CROF",      pVal.Row, "");
                    }
                    if (qty > 0)
                    {
                        oGrd.DataTable.SetValue("Intake",    pVal.Row, pForm.Items.Item("TXT_DATE").Specific.Value);
                        oGrd.DataTable.SetValue("Warehouse", pVal.Row, pForm.Items.Item("CMB_CWRHS").Specific.Value);
                        oGrd.DataTable.SetValue("CropCycle", pVal.Row, pForm.Items.Item("TXT_CROP").Specific.Value);
                        oGrd.DataTable.SetValue("Stage",     pVal.Row, CommonUI.Forms.GetField<string>(pForm, "CBT_STAGE", true));
                        oGrd.DataTable.SetValue("CROC",      pVal.Row, pForm.Items.Item("TXT_CROC").Specific.Value);
                        oGrd.DataTable.SetValue("CROF",      pVal.Row, pForm.Items.Item("TXT_CROF").Specific.Value);
                        Disable_Fields(pForm);
                    }
                }

                // **********************  THIS IS DONE IN CFL EVENT
                //if (pVal.ColUID == "CropCycle")
                //{
                //    var val = pForm.Items.Item("TXT_CROC").Specific.Value;
                //    if (val != "")
                //    {
                //LoadGrid_Stage(pForm);
                //    }
                //}

                oGrd.AutoResizeColumns();

                return true;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return false;
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }

        private void Disable_Fields(Form pForm)
        {
            // have to disable header fields for now.

            try
            {
                if (pForm.Mode == BoFormMode.fm_UPDATE_MODE)
                {
                    pForm.Items.Item("TXT_DATE").Enabled = false;
                    pForm.Items.Item("TXT_CROP").Enabled = false;
                    pForm.Items.Item("CBT_STAGE").Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }

        private bool LoadGrid_Stage(Form pForm)
        {
            // NOT USING THIS NOW - CAN'T HAVE DIFFERENT VALID VALUES FOR EACH CELL ON A GRID - ONLY FOR MATRIX
            return true;

            try
            {
                var val = pForm.Items.Item("TXT_CROC").Specific.Value;
                CommonUI.ComboBox.ComboBoxAddVals(pForm, "grdMoms", "Stage", "", null); // clear current options
                if (val != "")
                {
                    var sql = $@"SELECT StageID, UniqueID from PMG1 WHERE AbsEntry = {val} AND UniqueID LIKE 'PROP_CLONE%'";
                    CommonUI.ComboBox.ComboBoxAddVals(pForm, "grdMoms", "Stage", sql, null);
                }

                return true;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return false;
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }

        private void BTN_UPDATE_CLICK_PkgClne(Form pForm)
        {
            SAPbouiCOM.DataTable oDT = null;
            try
            {
                string itemCode;
                double quantity;
                DateTime dueDate;
                string destWhse;
                string batchNum;
                string lotNum = null; //12533 added in lot num so that we can set production batch ID in the createfrombom method

                oDT = _SBO_Form.Items.Item("grdMoms").Specific.DataTable;

                for (var i = 0; i < oDT.Rows.Count; i++)
                {
                    if (oDT.GetValue("Warehouse", i) == "") continue;

                    itemCode = oDT.GetValue("ItemCode", i);
                    quantity = oDT.GetValue("Quantity", i);
                    destWhse = CommonUI.Forms.GetField<string>(_SBO_Form, "grdMoms", i, "Warehouse");
                    batchNum = oDT.GetValue("BatchNumber", i);
                    lotNum = oDT.GetValue("Lot Number", i);//12533

                    int PdOKey = 0;
                    if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();
                    PdOKey = NSC_DI.SAP.ProductionOrder.DisassemblyPdO.CreateFromBOM(itemCode, quantity, DateTime.Now, destWhse, true, "N", "CLONE_UNPACK", pProdBatchID: lotNum); 

                    if (PdOKey == 0)
                    {
                        throw new System.ComponentModel.WarningException();
                    }
                    int PdOIssueKey = NSC_DI.SAP.ProductionOrder.DisassemblyPdO.Issue(PdOKey, batchNum);

                    int PdOReceiptKey = NSC_DI.SAP.ProductionOrder.DisassemblyPdO.Receive(PdOKey);

                    NSC_DI.SAP.ProductionOrder.UpdateStatus(PdOKey, BoProductionOrderStatusEnum.boposClosed);
                    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);//12533 closes the PdO
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                GC.Collect();
            }
        }

        private void BTN_UPDATE_Click(Form pForm)
        {
            SAPbouiCOM.DataTable oDT = null;
            try
            {
                var CropID = "";
                var StageID = "";
                var addGR = false;
                string BatchIDOfCloneClippings = "";
                string tempStrainID = "";
                int batchCounter = 0;
                // Prepare a new list of items to be added in the "Goods Receipt"
                List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> ListOfGoodsReceiptItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();

                oDT = _SBO_Form.Items.Item("grdMoms").Specific.DataTable;
                for (var i = 0; i < oDT.Rows.Count; i++)
                {
                    if (oDT.GetValue("Quantity", i) < 1) continue;

                    string itemCode = oDT.GetValue("ItemCode", i);
                    //((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_MTHRI", _SBO_Form)).Value.ToString();

                    // Get strain ID from mother plant item code.
                    string SQL_GetStrainIDFromMotherPlant = @"SELECT [U_NSC_StrainID] FROM [OITM] WHERE [OITM].[ItemCode] = '" +
                                                            (new SqlParameter("ItemCode", itemCode)).Value.ToString() + "'";

                    // Get results from the SQL helper
                    //SAPbobsCOM.Fields resultFromSQLQuery = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(SQL_GetStrainIDFromMotherPlant);
                    //string StrainIDFromSelectedMotherPlant = resultFromSQLQuery.Item(0).Value.ToString();

                    var strainID = NSC_DI.UTIL.SQL.GetValue<string>(SQL_GetStrainIDFromMotherPlant, "");


                    if (strainID == "")
                    {
                        Globals.oApp.MessageBox($"There is no strain specified on the item master for item {itemCode}");

                        return;
                    }


                    // Get the quantity from the form UI
                    int QuantityOfClonesBeingCreated = oDT.GetValue("Quantity", i);
                    //Convert.ToInt32(((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QUANT", _SBO_Form)).Value.ToString());

                    // Get selected warehouse ID from the form UI
                    //ComboBox CMB_CWRHS = ((ComboBox)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_CWRHS", _SBO_Form));

                    // TODO: Removing for the time being. We may add this later with the option to skip the clone bank.
                    //SAPbouiCOM.ComboBox CMB_PWRHS = ((SAPbouiCOM.ComboBox)Helpers.Forms.GetControlFromForm(
                    //   ItemType: Helpers.Forms.FormControlTypes.ComboBox,
                    //   ItemUID: "CMB_PWRHS",
                    //   FormToSearchIn: _SBO_Form));

                    // Prepare a ComboBox helper
                    //VERSCI.ComboBox ComboBox_Helper = new VERSCI.ComboBox(Globals.oApp, Globals.oCompany, _SBO_Form);

                    // Get the "description" from the ComboBox, where we are really storing the value
                    //string DestinationWarehouseIdForClones = ComboBox_Helper.GetComboBoxValueOrDescription(cFormID, "CMB_CWRHS", null, CMB_CWRHS.Value);
                    string DestinationWarehouseIdForClones = CommonUI.Forms.GetField<string>(_SBO_Form, "grdMoms", i, "Warehouse");


                    //// TODO: Removing for the time being. We may add this later with the option to skip the clone bank.
                    //string DestinationWarehouseIdForPlants = ComboBox_Helper.GetComboBoxValueOrDescription(
                    //    pFormUID: cFormID,
                    //    pComboBoxUID: "CMB_PWRHS",
                    //    pComboBoxItemDescription: null,
                    //    pComboBoxItemValue: CMB_PWRHS.Value);


                    // Find what item is the "clone clipping" item for this strain that comes from the selected mother
                    //                    string sql = @"
                    //SELECT [ItemCode] FROM [OITM]
                    //WHERE [OITM].[U_" + Globals.SAP_PartnerCode + @"_StrainID] = '" + strainID + @"'
                    //  AND [OITM].[ItmsGrpCod] IN
                    //      (SELECT [ItmsGrpCod] FROM [OITB] WHERE [ItmSGrpNam] IN " + ITEM_GROUP_NAME + ")";

                    //                    // Get results from SQL server
                    //                    var resultFromSQLQuery = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(sql);

                    //                    string ItemCodeOfACloneClipping = resultFromSQLQuery.Item(0).Value.ToString();

                    // allow for plant tissue
                    var grpNo = "";
                    switch (pForm.Items.Item("cboType").Specific.Value)
                    {
                        case "Clone":
                            grpNo = "41";
                            break;
                        case "Plant":
                            grpNo = "54";
                            break;
                        //case "Pkgd":
                        //    grpNo = "37";
                        //    break;
                        default:
                            Globals.oApp.MessageBox("Select a Clone Type.");
                            return;
                    }

                    string sql = $@"
SELECT [ItemCode] FROM [OITM]
 WHERE [OITM].[U_{Globals.SAP_PartnerCode}_StrainID] = '{strainID}'
   AND [OITM].[QryGroup{grpNo}] = 'Y'";

                    string ItemCodeOfACloneClipping = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");
                    if (ItemCodeOfACloneClipping == "")
                    {
                        Globals.oApp.MessageBox("No item was found for the selected Clone Type.");
                        return;
                    }

                    //// Prepare a new list of items to be added in the "Goods Receipt"
                    //List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> ListOfGoodsReceiptItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();

                    // If the warehouse code is only one digit, add a leading zero.
                    if (DestinationWarehouseIdForClones.Length == 1)
                    {
                        DestinationWarehouseIdForClones = "0" + DestinationWarehouseIdForClones;
                    }

                    // Get the new batch ID
                    //Inter-Company Check 
                    //string InterCoSubCode = NSC_DI.UTIL.SQL.GetValue<string>($@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 where T0.WhsCode = '{DestinationWarehouseIdForClones}'");// DELETE 12/30/2020 
                    //string BatchIDOfCloneClippings = "CL-" + NSC_DI.SAP.BatchItems.GetNextAvailableBatchID(ItemCodeOfACloneClipping);
                    if (tempStrainID != strainID)
                    {
                        tempStrainID = strainID;
                        batchCounter = 0;
                    }
                    BatchIDOfCloneClippings = NSC_DI.SAP.BatchItems.NextBatch(ItemCodeOfACloneClipping, 1, "B", batchCounter).First();
                    batchCounter++;
                    // Grab the ID of the mother plant that was selected by the user in the form
                    string distNum = oDT.GetValue("Serial/Batch Number", i);
                    //((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_MTHRC", _SBO_Form)).Value.ToString();

                    // Get the quantity from the form UI
                    double dblPrice = 0.01;
                    //CropInfo
                    CropID = CommonUI.Forms.GetField<string>(_SBO_Form, "grdMoms", i, "CROC");
                    //((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_CROC", _SBO_Form)).Value.ToString();

                    StageID = CommonUI.Forms.GetField<string>(pForm, "CBT_STAGE", true);
                    string CropFinProj = CommonUI.Forms.GetField<string>(_SBO_Form, "grdMoms", i, "CROF");
                    //((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_CROF", _SBO_Form)).Value.ToString();

                    //string strSQL = @"Select T0.U_NSC_IsMedical from OSRN T0 where T0.DistNumber='" + MotherPlantSerialId + "'";

                    Dictionary<string, string> UDFs = new Dictionary<string, string>();

                    string strIsMedical = MedicalSQL(itemCode, distNum);//NSC_DI.UTIL.SQL.GetValue<string>(strSQL);

                    if (NSC_DI.UTIL.UDO.IsFieldInTable("OSRN", "U_VIR_Pheno")) // 9783 Added because this only matters for buckeyes dbs
                    {
                        string strSQLTwo = $"IF COL_LENGTH('OSRN', 'U_VIR_Pheno') IS NULL SELECT NULL ELSE EXEC('SELECT U_VIR_Pheno FROM OSRN WHERE DistNumber=''{distNum}''')"; // 9783 added to copy phenotype from serial info to the new batch details
                        string strPhenotype = NSC_DI.UTIL.SQL.GetValue<string>(strSQLTwo, null); // 9783 added to copy phenotype from serial info to the new batch details\
                        strPhenotype = (strPhenotype == "0") ? null : strPhenotype; // 9783 - SQL returns a NULL. B1 record set returns 0;
                        if (strPhenotype != null) UDFs.Add("U_VIR_Pheno", strPhenotype); // 9783 added to copy phenotype from serial info to the new batch details
                    }
                    Dictionary<string, string> GoodReceiptUDFs = new Dictionary<string, string>();
                    if (NSC_DI.UTIL.Settings.Version.GetCompliance() == "METRC")
                    {
                        if (grpNo == "41") GoodReceiptUDFs.Add($"U_{Globals.SAP_PartnerCode}_ComplianceType", "plantbatch_package");    // clone
                        if (grpNo == "54") GoodReceiptUDFs.Add($"U_{Globals.SAP_PartnerCode}_ComplianceType", "plantbatch_tissue");     // plant issue
                    }

                    string mnfSrlNum = NSC_DI.UTIL.SQL.GetValue<string>(@"Select MnfSerial from OSRN T0 where T0.DistNumber='" + distNum + "'");


                    UDFs.Add("U_" + Globals.SAP_PartnerCode + "_MotherID", distNum);
                    UDFs.Add("U_" + Globals.SAP_PartnerCode + "_IsMedical", strIsMedical);


                    if (CropID.Trim().Length > 0)
                    {
                        UDFs.Add("U_" + Globals.SAP_PartnerCode + "_CropStageID", StageID);
                        UDFs.Add("U_" + Globals.SAP_PartnerCode + "_CropID", CropID);
                    }
                    if (ListOfGoodsReceiptItems.Count > 0)
                        ListOfGoodsReceiptItems.Clear();
                    // Add a "Clone Clipping" line item to the "Goods Receipt" document
                    ListOfGoodsReceiptItems.Add(new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines()
                    {
                        ItemCode = ItemCodeOfACloneClipping,
                        Quantity = QuantityOfClonesBeingCreated,
                        Price = dblPrice,
                        WarehouseCode = DestinationWarehouseIdForClones,
                        ListOfBatchLineItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem>()
                    {
                        new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem()
                        {
                            BatchNumber = BatchIDOfCloneClippings,
                            Quantity = QuantityOfClonesBeingCreated,
                            UserDefinedFields = UDFs,
                            ManufacturerSerialNumber = mnfSrlNum
                        }
                    },
                        CropID = CropID,
                        StageID = StageID,
                        CropFinPRoj = CropFinProj
                    });
                    addGR = true;
                   

                    if (addGR == false)
                    {
                        Globals.oApp.StatusBar.SetText("No clones were specified.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                        return;
                    }

                    if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();
                    // Make sure the "Goods Receipt" was created successfully
                    int intRG_key = NSC_DI.SAP.GoodsReceipt.Create(DateTime.Now, ListOfGoodsReceiptItems, null, GoodReceiptUDFs);

                    if (intRG_key == 0)
                    {
                        // Let the user know the creation of the "Goods Receipt" document failed
                        Globals.oApp.StatusBar.SetText("Failed to create the \"Goods Receipt\"!");
                        return;
                    }
                    if (CropID.Length > 0)
                    {
                        Forms.Crop.CropCycleManagement oCrop = new Crop.CropCycleManagement();
                        oCrop.AddDocToProject_Receipts(PMDocumentTypeEnum.pmdt_GoodsReceipt, intRG_key.ToString(), CropID, StageID);
                        NSC_DI.UTIL.Misc.KillObject(oCrop);
                    }
                    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    //_SBO_Form.Close();
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                //_SBO_Form.Close();
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                GC.Collect();
            }
        }

        private void BTN_FINISH_Click(Form pForm)
	    {
		    try
		    {
			    string MotherPlantItemCode =
				    ((EditText) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_MTHRI", _SBO_Form)).Value.ToString();

			    // Get strain ID from mother plant item code.
			    string SQL_GetStrainIDFromMotherPlant = @"SELECT [U_NSC_StrainID] FROM [OITM] WHERE [OITM].[ItemCode] = '" +
			                                            (new SqlParameter("ItemCode", MotherPlantItemCode)).Value.ToString() + "'";

                // Get results from the SQL helper
                //SAPbobsCOM.Fields resultFromSQLQuery = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(SQL_GetStrainIDFromMotherPlant);
                //string StrainIDFromSelectedMotherPlant = resultFromSQLQuery.Item(0).Value.ToString();

                var StrainIDFromSelectedMotherPlant = NSC_DI.UTIL.SQL.GetValue<string>(SQL_GetStrainIDFromMotherPlant, "");

                if (StrainIDFromSelectedMotherPlant == "")
                {
                    Globals.oApp.MessageBox($"There is no strain specified on the item master for item {MotherPlantItemCode}");

                    return;
                }
                    

                // Get the quantity from the form UI
                int QuantityOfClonesBeingCreated =
				    Convert.ToInt32(((EditText) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QUANT", _SBO_Form)).Value.ToString());

			    // Get selected warehouse ID from the form UI
			    ComboBox CMB_CWRHS = ((ComboBox) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_CWRHS", _SBO_Form));

			    // TODO: Removing for the time being. We may add this later with the option to skip the clone bank.
			    //SAPbouiCOM.ComboBox CMB_PWRHS = ((SAPbouiCOM.ComboBox)Helpers.Forms.GetControlFromForm(
			    //   ItemType: Helpers.Forms.FormControlTypes.ComboBox,
			    //   ItemUID: "CMB_PWRHS",
			    //   FormToSearchIn: _SBO_Form));

			    // Prepare a ComboBox helper
			    VERSCI.ComboBox ComboBox_Helper = new VERSCI.ComboBox(Globals.oApp, Globals.oCompany, _SBO_Form);

			    // Get the "description" from the ComboBox, where we are really storing the value
			    string DestinationWarehouseIdForClones = ComboBox_Helper.GetComboBoxValueOrDescription(cFormID, "CMB_CWRHS", null, CMB_CWRHS.Value);

			    //// TODO: Removing for the time being. We may add this later with the option to skip the clone bank.
			    //string DestinationWarehouseIdForPlants = ComboBox_Helper.GetComboBoxValueOrDescription(
			    //    pFormUID: cFormID,
			    //    pComboBoxUID: "CMB_PWRHS",
			    //    pComboBoxItemDescription: null,
			    //    pComboBoxItemValue: CMB_PWRHS.Value);


			    // Find what item is the "clone clipping" item for this strain that comes from the selected mother
			    string SQL_GetItemCodeOfClippingFromStrainID = @"
SELECT [ItemCode] FROM [OITM]
WHERE [OITM].[U_" + Globals.SAP_PartnerCode + @"_StrainID] = '" + StrainIDFromSelectedMotherPlant + @"'
  AND [OITM].[ItmsGrpCod] IN
      (SELECT [ItmsGrpCod] FROM [OITB] WHERE [ItmSGrpNam] IN " + ITEM_GROUP_NAME + ")";

			    // Get results from SQL server
			    var resultFromSQLQuery = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(SQL_GetItemCodeOfClippingFromStrainID);

			    string ItemCodeOfACloneClipping = resultFromSQLQuery.Item(0).Value.ToString();


                // Prepare a new list of items to be added in the "Goods Receipt"
                List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> ListOfGoodsReceiptItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();

			    // If the warehouse code is only one digit, add a leading zero.
			    if (DestinationWarehouseIdForClones.Length == 1)
			    {
				    DestinationWarehouseIdForClones = "0" + DestinationWarehouseIdForClones;
			    }

                // Get the new batch ID
                //Inter-Company Check 
                //string InterCoSubCode = NSC_DI.UTIL.SQL.GetValue<string>($@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 where T0.WhsCode = '{DestinationWarehouseIdForClones}'");// DELETE 12/30/2020 
                //string BatchIDOfCloneClippings = "CL-" + NSC_DI.SAP.BatchItems.GetNextAvailableBatchID(ItemCodeOfACloneClipping);
                string BatchIDOfCloneClippings = NSC_DI.SAP.BatchItems.NextBatch(ItemCodeOfACloneClipping);

                // Grab the ID of the mother plant that was selected by the user in the form
                string MotherPlantSerialId =
                ((EditText) CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_MTHRC", _SBO_Form)).Value.ToString();

                // Get the quantity from the form UI
                double dblPrice = 0.01;
                //CropInfo
                string CropID =
                    ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_CROC", _SBO_Form)).Value.ToString();
                
                string StageID = CommonUI.Forms.GetField<string>(pForm, "CBT_STAGE", true);
                string CropFinProj =
                    ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_CROF", _SBO_Form)).Value.ToString();

                string strSQL = $"Select T0.U_NSC_IsMedical from OSRN T0 where T0.DistNumber = '{MotherPlantSerialId}'";
                string strIsMedical = NSC_DI.UTIL.SQL.GetValue<string>(strSQL);

                Dictionary<string, string> UDFs = new Dictionary<string, string>();
                UDFs.Add("U_" + Globals.SAP_PartnerCode + "_MotherID", MotherPlantSerialId);
                UDFs.Add("U_" + Globals.SAP_PartnerCode + "_IsMedical", strIsMedical);

                if (CropID.Trim().Length > 0)
                {
                    UDFs.Add("U_" + Globals.SAP_PartnerCode + "_CropStageID", StageID);
                    UDFs.Add("U_" + Globals.SAP_PartnerCode + "_CropID", CropID);
                }                            

                // Add a "Clone Clipping" line item to the "Goods Receipt" document
                ListOfGoodsReceiptItems.Add(new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines()
                {
                    ItemCode = ItemCodeOfACloneClipping,
                    Quantity = QuantityOfClonesBeingCreated,
                    Price = dblPrice,
                    WarehouseCode = DestinationWarehouseIdForClones,
                    ListOfBatchLineItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem>()
                    {
                        new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem()
                        {
                            BatchNumber = BatchIDOfCloneClippings,
                            Quantity = QuantityOfClonesBeingCreated,
                            UserDefinedFields = UDFs
                        }
                    },
                    CropID = CropID,
                    StageID=StageID,
                    CropFinPRoj= CropFinProj
                });

                // Make sure the "Goods Receipt" was created successfully
                int intRG_key = NSC_DI.SAP.GoodsReceipt.Create(DateTime.Now, ListOfGoodsReceiptItems);

                if (intRG_key == 0)
			    {
				    // Let the user know the creation of the "Goods Receipt" document failed
				    Globals.oApp.StatusBar.SetText("Failed to create the \"Goods Receipt\"!");
				    return;
			    }
                Forms.Crop.CropCycleManagement oCrop = new Crop.CropCycleManagement();
                if (CropID.Length > 0)
                {
                    oCrop.AddDocToProject_Single(PMDocumentTypeEnum.pmdt_GoodsReceipt, intRG_key.ToString(), CropID, StageID);
                }

                // Close the form, user is done!
                _SBO_Form.Close();
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                _SBO_Form.Close();
            }
            finally
		    {
			    GC.Collect();
		    }
	    }

	    private void BTN_NEXT_Click(Form pForm)
        {
			switch (CurrentlySelectedTab(pForm))
            {
                case 1:
                    ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_2", _SBO_Form)).Select();
                    break;

                case 2:
                    ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_3", _SBO_Form)).Select();
                    break;

                case 3:
                    ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_4", _SBO_Form)).Select();
                    break;
            }
        }

		private int CurrentlySelectedTab(Form pForm)
        {
            for (int i = 1; i <= 4; i++)
            {
                Folder tab = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_" + i.ToString(), _SBO_Form);
                if (tab.Selected)
                {
                    return i;
                }
            }

            return 0;
        }

		private void BTN_BACK_Click(Form pForm)
        {
			switch (CurrentlySelectedTab(pForm))
            {
                case 2:
                    ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_1", _SBO_Form))
                    .Select();
                    break;

                case 3:
                    ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_2", _SBO_Form))
                    .Select();
                    break;

                case 4:
                    ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_3", _SBO_Form))
                    .Select();
                    break;
            }
        }

        private bool ValidateForTabFour()
        {
            bool _BubbleEvent = true;

            EditText TXT_QTY = (EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_QUANT", _SBO_Form);

            ComboBox CMB_CWRHS = (ComboBox)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.ComboBox, "CMB_CWRHS", _SBO_Form);

            // TODO: Removing for the time being. We may add this later with the option to skip the clone bank.
            //SAPbouiCOM.ComboBox CMB_PWRHS = (SAPbouiCOM.ComboBox)Helpers.Forms.GetControlFromForm(
            //    ItemType: Helpers.Forms.FormControlTypes.ComboBox,
            //    ItemUID: "CMB_PWRHS",
            //    FormToSearchIn: _SBO_Form);

            if (string.IsNullOrEmpty(TXT_QTY.Value))
            {
                // Don't allow SAP to continue
                _BubbleEvent = false;

                Globals.oApp.StatusBar.SetText("Please enter a quantity of Clones!", BoMessageTime.bmt_Short);

                TXT_QTY.Active = true;
            }

            if (string.IsNullOrEmpty(CMB_CWRHS.Value))
            {
                // Don't allow SAP to continue
                _BubbleEvent = false;

                Globals.oApp.StatusBar.SetText("Please select a destination warehouse for the Clone!", BoMessageTime.bmt_Short);

                CMB_CWRHS.Active = true;
            }

            //// TODO: Removing for the time being. We may add this later with the option to skip the clone bank.
            //if (string.IsNullOrEmpty(CMB_PWRHS.Value))
            //{
            //    // Don't allow SAP to continue
            //    _BubbleEvent = false;

            //    Globals.oApp.StatusBar.SetText(
            //        Text: "Please select a destination warehouse for the Plant!",
            //        Seconds: SAPbouiCOM.BoMessageTime.bmt_Short,
            //        Type: SAPbouiCOM.BoStatusBarMessageType.smt_Error);

            //    CMB_PWRHS.Active = true;
            //}

            if (!_BubbleEvent)
            {
                ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_3", _SBO_Form))
                .Select();
            }

            return _BubbleEvent;
        }

        private void Filter_Selection(Form pForm)// 11046
        {
            if (pForm.Visible == false) return;
            bool motherCHK = pForm.Items.Item("CHK_MTHR").Specific.Checked;
            var TXT_ADM = pForm.Items.Item("TXT_ADM");
            var TXT_IDNUM = pForm.Items.Item("TXT_IDNUM");
            var CBO_WRHS = pForm.Items.Item("CBT_WRHS");
            var CBO_STRN = pForm.Items.Item("CBT_STRN");
            var BTN_FLTR = pForm.Items.Item("BTN_FLTR");
            if (motherCHK)
            {
                // 11046 sets the values of the filter fields to empty if the user checks mother plant only
                TXT_ADM.Specific.Value = "";
                TXT_IDNUM.Specific.Value = "";
                NavSol.CommonUI.ComboBox.SetVal(pForm, "CBT_WRHS", 0, BoSearchKey.psk_Index);
                NavSol.CommonUI.ComboBox.SetVal(pForm, "CBT_STRN", 0, BoSearchKey.psk_Index);
            }
            // sets focus to txtdate and then enables or disables the filter fields based on inverse of mother plant being checked
            pForm.Items.Item("TXT_DATE").Specific.Active = true;
            TXT_ADM.Enabled = !motherCHK;
            BTN_FLTR.Enabled = !motherCHK;
            TXT_IDNUM.Enabled = !motherCHK;
            CBO_WRHS.Enabled = !motherCHK;            
            CBO_STRN.Enabled = !motherCHK;
        }

        private void FILL_FILTER_COMBOS(Form pForm)// 11046 loads the combo boxes 
        {
            try
            {
                var sql = @"SELECT DISTINCT OITM.U_NSC_StrainID, [@NSC_STRAIN].[Name]
FROM OITM
INNER JOIN [@NSC_STRAIN] ON OITM.U_NSC_StrainID = [@NSC_STRAIN].[Code]
WHERE OnHand > 0 AND U_NSC_StrainID IS NOT NULL";
                NavSol.CommonUI.ComboBox.ComboBoxAddVals(pForm, "CBT_STRN", sql, " ");
                sql = "SELECT DISTINCT WhsCode, WhsName FROM OWHS WHERE U_NSC_WhrsType = 'CLO' OR U_NSC_WhrsType = 'VEG' OR U_NSC_WhrsType = 'FLO'";
                if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})"; // 10823
                NavSol.CommonUI.ComboBox.ComboBoxAddVals(pForm, "CBT_WRHS", sql, " ");
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }           
        }
        
        private string WhereBuilder(string pWhere, string pClause)
        {
            if (pWhere.Length < 1)
                pClause = $"WHERE {pClause}";
            else
                pClause = $"AND {pClause}";

            return pClause;
        }

        private string MedicalSQL (string pItemCode, string pDistNum)
        {
            string medicalSQL = $"Select U_NSC_IsMedical from OSRN where DistNumber='{pDistNum}' AND ItemCode = '{pItemCode}'";
            string sql = NSC_DI.UTIL.SQL.GetValue<string>(medicalSQL);
            if (!string.IsNullOrEmpty(sql))
                return sql;
            else
            {
                medicalSQL = $"Select U_NSC_IsMedical from OBTN where DistNumber='{pDistNum}' AND ItemCode = '{pItemCode}'";
                sql = NSC_DI.UTIL.SQL.GetValue<string>(medicalSQL);
                return sql;
            }           
        }
    }
}