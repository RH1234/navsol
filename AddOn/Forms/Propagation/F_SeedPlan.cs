﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Propagation
{
    class F_SeedPlan : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID     = "NSC_SEEDPLAN";
        private const string cItemType   = "Seed";
        private const string cPdoProcess = "SEED";
        private NSC_DI.Globals.ItemGroupTypes ItemType = NSC_DI.Globals.ItemGroupTypes.CannabisSeed;

        private static System.Data.DataTable _dtFinProj = new System.Data.DataTable("FinProj");
        public int CropID { get; set; }
        public string CropName { get; set; }
        public string CropFinProjName { get; set; }



        private Dictionary<string, int> MatrixColumns = new Dictionary<string, int>()
        {
            {"ItemCode", 0},
            {"ItemName", 1},
            {"OnHand", 2},
            {"Quantity", 3},
            {"ProdBatchID", 4},
            {"WhsCode",  5},
            {"BatchNum", 6},
            {"SuppSerial", 7},
            {"CropID",   8},
            {"CropName", 9}
        };

		protected class SeedSelectedRow
		{
			public string ItemCode;
			public double Quantity;
            public string ProdBatchID;
            public string SourceWarehouseCode;
			public string BatchNo;
			public string DestinationWarehouseName;
			public string DestinationWarehouseCode;
            public string CropName;
            public string CropProjID;
        }

		protected class SeedTransactionData
		{
			public List<SeedSelectedRow> RowsToProcess = new List<SeedSelectedRow>();

			public DateTime dueDate;
			public string projectBatchNo;

			// Newly created Production key. Assinged after creating the Production Order
			public int productionOrderKey;
		}

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		public string FormUID { get; set; }
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
	    [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
	    public virtual bool OnBeforeItemPressed(ItemEvent pVal)
	    {
		    bool BubbleEvent = true;
		    Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


		    NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();

		    return BubbleEvent;
	    }
	    #endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "BTN_RELE":
					BTN_RELE_Click(oForm);
					break;

				case "MTX_SEED":
					MTX_SEED_Click(pVal);
					break;
                case "BTN_CROP":
                    SAPbouiCOM.DataTable dtCropProj = null;
                    try
                    {
                        if (oForm.DataSources.DataTables.Item("CropSrc").IsEmpty == false)
                        {
                            oForm.DataSources.DataTables.Item("CropSrc").Clear();
                            dtCropProj = oForm.DataSources.DataTables.Item("CropSrc");
                        }
                    }
                    catch
                    {
                        dtCropProj = oForm.DataSources.DataTables.Add("CropSrc");
                    }

                    dtCropProj.ExecuteQuery(@"Select T0.DocNum as [Crop ID],T0.NAME,T0.START as [Start Date],T0.DUEDATE [End Date] from [OPMG] T0 where T0.STATUS!='F'");

                    // open the form; 
                    string strCallingFormID = oForm.UniqueID.ToString();
                    Forms.Crop.CropCycleSelect.FormCreate(strCallingFormID, dtCropProj);

                    Forms.Crop.CropCycleSelect.CropProjCB = delegate (string callingFormUid, System.Data.DataTable pdtCropProj)
                    {
                        Form oFormDel = null;

                        oFormDel = Globals.oApp.Forms.Item(strCallingFormID) as Form;
                        if (pdtCropProj.Rows.Count == 0) return;

                        System.Data.DataRow row = pdtCropProj.Rows[0];
                        var strCropProjCode = row.ItemArray.GetValue(0);
                        var strCropProjName = row.ItemArray.GetValue(1);

                        oForm = Globals.oApp.Forms.Item(strCallingFormID);
                        //Set MTX Values

                        // Find the matrix in the form UI
                        Matrix MTX_SEED = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_SEED", oForm);
                        int intSelectedRow = MTX_SEED.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);

                        ((EditText)MTX_SEED.Columns.Item(MatrixColumns["CropID"]).Cells.Item(intSelectedRow).Specific).Value = strCropProjCode.ToString();
                        ((EditText)MTX_SEED.Columns.Item(MatrixColumns["CropName"]).Cells.Item(intSelectedRow).Specific).Value = strCropProjName.ToString();

                    };
                    break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (ChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public F_SeedPlan() : this(Globals.oApp, Globals.oCompany) { }

		public F_SeedPlan(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

        public void Form_Load()
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);
                
                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_MAIN", @"seedbank-icon.bmp");

				Load_Matrix();

                var oMat = _SBO_Form.Items.Item("MTX_SEED").Specific;

                CommonUI.Matrix.SetLink(oMat, 0, SAPbouiCOM.BoLinkedObject.lf_Items);


                _SBO_Form.Items.Item("CMB_PORDER").Specific.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);

                // Fill the ComboBox of Warehouses
                if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y")
                {
                    NavSol.CommonUI.CFL.CreateWH(_SBO_Form, "CFL_WH", "CBX_WHSE");
                    var firstPhase = NSC_DI.SAP.Phase.GetPhases().First();
                    NavSol.CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_NONE, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, firstPhase.CurrentPhase);
                    NavSol.CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND, "Inactive", BoConditionOperation.co_EQUAL, "N");

                    NavSol.CommonUI.CFL.AddCon_Branches(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND);                           // #10823
                }
                else
                    Load_Combobox_Warehouses();

                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                // Show the form
                _SBO_Form.VisibleEx = true;

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				_SBO_Form.Close();
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        private void MTX_SEED_Click(ItemEvent pVal)
        {
            // Find the matrix in the form UI
            Matrix MTX_SEED = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_SEED", _SBO_Form);
            try
            {
                int clickedRowId = pVal.Row;
                if (clickedRowId > MTX_SEED.RowCount) return;

                string ItemCodeOfNewSelectedRow = ((EditText)MTX_SEED.Columns.Item(MatrixColumns["ItemCode"]).Cells.Item(clickedRowId).Specific).Value;

                // Determine if this key press was of the same Item Group we had selected previously.
                bool deselectCurrent = false;
                List<int> rowsToReselect = new List<int>();
                for (int i = 1; i < (MTX_SEED.RowCount + 1); i++)
                {
                    string ItemCodeToCompare = ((EditText)MTX_SEED.Columns.Item(MatrixColumns["ItemCode"]).Cells.Item(i).Specific).Value;
                    // If the current row is now selected we can just continue looping.
                    if (!MTX_SEED.IsRowSelected(i) || i == clickedRowId)
                        continue;

                    rowsToReselect.Add(i);

                    if (!ItemCodeOfNewSelectedRow.Equals(ItemCodeToCompare))
                    {
                        deselectCurrent = true;
                    }
                }

                if (deselectCurrent)
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Item Code must be of the same type!", BoMessageTime.bmt_Short);

                    MTX_SEED.SelectRow(clickedRowId, false, false);
                    return;
                }

                // Grab the selected item in the Matrix
				string selectedItemCode = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(MTX_SEED, "Item Code");

                // Prepare the SQL statement that will find the projected time via the item code
	            string sql = @"
SELECT [@" + NSC_DI.Globals.tStrains + @"].[U_PrjctSeed] 
FROM [OITM]
JOIN [@" + NSC_DI.Globals.tStrains + @"] ON [OITM].[U_NSC_StrainID] = [@NSC_STRAIN].[Code]
WHERE [OITM].[ItemCode] = '" + selectedItemCode + "'";

                // Grab the results from the SQL query
				Recordset ResultsFromSQL = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(sql);

                // Find the textbox for estimated days
                EditText TXT_DAYS = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_DAYS", _SBO_Form);

                // Set the estimated days
                TXT_DAYS.Value = ResultsFromSQL.Fields.Item(0).Value.ToString();

                switch (pVal.ColUID)// not sure why this is needed, but the active field kept moving
                {
                    case "col_3":
                        MTX_SEED.Columns.Item("col_3").Cells.Item(clickedRowId).Specific.Active = true;
                        break;
                    case "col_4":
                        MTX_SEED.Columns.Item("col_4").Cells.Item(clickedRowId).Specific.Active = true; 
                        break;                   
                }
            }
			catch (Exception ex)
            {
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
        }

	    private void Load_Combobox_Warehouses()
	    {
		    try
		    {
			    // Get the first phase of config CSV
			    var firstPhase = NSC_DI.SAP.Phase.GetPhases().First();

			    // Prepare to run a SQL statement.
			    SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                // Count how many current records exist within the database.
                var sql = @"SELECT WhsCode, WhsName FROM OWHS LEFT JOIN OBPL ON OWHS.BPLid = OBPL.BPLid WHERE [U_NSC_WhrsType] = '" +   // #10823
                            (new SqlParameter("WhrsType", firstPhase.CurrentPhase)).Value.ToString() + "' AND [Inactive] = 'N'";
                if (Globals.BranchDflt >= 0) sql += $" AND OBPL.BPLid IN ({Globals.BranchList})";                                       // #10823

                oRecordSet.DoQuery(sql);                                                                                                // #10823

                // Find the Combobox of Warehouses from the Form UI
                ComboBox CBX_WHSE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CBX_WHSE", _SBO_Form) as ComboBox;

			    // If items already exist in the drop down
			    if (CBX_WHSE.ValidValues.Count > 0)
			    {
				    // Remove all currently existing values from warehouse drop down
				    for (int i = CBX_WHSE.ValidValues.Count; i-- > 0;)
				    {
					    CBX_WHSE.ValidValues.Remove(i, BoSearchKey.psk_Index);
				    }
			    }

			    // If more than 1 warehouses exists
			    if (oRecordSet.RecordCount > 1)
			    {
				    // Create the first item as an empty item
				    CBX_WHSE.ValidValues.Add("", "");

				    // Select the empty item (forcing the user to make a decision)
				    CBX_WHSE.Select(0, BoSearchKey.psk_Index);
			    }

			    // Add allowed warehouses to the drop down
			    for (int i = 0; i < oRecordSet.RecordCount; i++)
			    {
				    try
				    {
					    CBX_WHSE.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
				    }
				    catch
				    {
				    }
				    CBX_WHSE.Item.Enabled = true;
				    oRecordSet.MoveNext();
			    }

			    // Auto select our warehouse if we only have one and Disable the control
			    if (oRecordSet.RecordCount == 1)
			    {
				    //CBX_WHSE.Active = false;
				    //CBX_WHSE.Item.Enabled = false;
				    CBX_WHSE.Select(0, BoSearchKey.psk_Index);
			    }
		    }
		    catch (Exception ex)
		    {
			    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
				//NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

        private void CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            if (pVal.SelectedObjects == null) return;

            try
            {
                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_WH":
                        CommonUI.CFL.SetFieldsWH(pForm, pVal, "CBX_WHSE", "txtWH_CFL");
                        break;

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void SetCrop()
        {
           

        }

        private void BTN_RELE_Click(Form pForm)
        {
            try
            {
                // Grab the textbox for days from the form
                ComboBox CMB_PORDER = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_PORDER", _SBO_Form);

                if (string.IsNullOrEmpty(CMB_PORDER.Value))
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Please select how you'd like to group your production orders.");

                    // Focus the cursor in the offending input
                    CMB_PORDER.Active = true;
                    return;
                }

                SAPbouiCOM.EditText oEdit = pForm.Items.Item("TXT_REM").Specific;
                string Remarks = oEdit.Value;

                switch (CMB_PORDER.Value.ToString())
                {
                    case "Individual":
                        CreateIndividualProductionOrders(Remarks);
                        oEdit = pForm.Items.Item("TXT_REM").Specific;
                        oEdit.Value = "";
                        break;
                        //case "Grouped":
                        //    CreateGroupedProductionOrder();
                        //    break;
                }                
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(ex.Message);
            }
            finally
            {
                Load_Matrix();
                //NSC_DI.UTIL.Misc.KillObject(oForm);
            }
        }

        private void CreateIndividualProductionOrders(string pRemarks)
        {
            try
            {
                // Pull all data from the selected columns.
                SeedTransactionData seedTransDetails = PullSeedDetailsFromForm();
                if (seedTransDetails == null) return;

                bool AllWetBudPassed = true;
                double TotalProcessed = 0;

                if (seedTransDetails.RowsToProcess.Count <= 0)
                {
                    // Send success message to the client
                    Globals.oApp.StatusBar.SetText("Failed to Release and Issued all Selected Items!");

                    return;
                }

                bool bUpdatedProdO = false; 

                Crop.CropCycleManagement oCrop = new Crop.CropCycleManagement();

                foreach (SeedSelectedRow row in seedTransDetails.RowsToProcess)
                {
                    if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();
                    //--------------------------------------------
                    // PRODUCTION ORDER
                    // Split the plant ID by hyphen -
                    string PlantID_Holder = row.ItemCode;
                    var strainID = NSC_DI.SAP.Items.GetStrainID(PlantID_Holder);
                    string SourceItemCode = NSC_DI.UTIL.AutoStrain.GetAutoItemCodeValue("~", strainID); // Plant
                    string destWH = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT WhsCode FROM OWHS WHERE WhsName = '{_SBO_Form.Items.Item("CBX_WHSE").Specific.Value}'");   // #10824
                    var PdOKey = NSC_DI.SAP.ProductionOrder.CreateFromBOM(SourceItemCode, row.Quantity, DateTime.Now, destWH, true, "N", cPdoProcess, pRemarks, row.ProdBatchID);
                    if (PdOKey == 0) throw new Exception(NSC_DI.UTIL.Message.Format("Failed to create Production Order for batch " + row.BatchNo + 
                            Environment.NewLine + Globals.oCompany.GetLastErrorDescription()));

                    //Check the warehouse for the Clone(s) item(s) on tbe Production BOM and if needed adjust accordingly
                    //Get clone Warehouses for selected items on the Matrix by batchnumber 
                    SAPbobsCOM.ProductionOrders oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders) as SAPbobsCOM.ProductionOrders;
                    oPdO.GetByKey(PdOKey);
                    SAPbobsCOM.ProductionOrders_Lines oLines = oPdO.Lines;

                    //Update Destination Warehouse.
                    if (row.DestinationWarehouseCode != oPdO.Warehouse)
                    {
                        oPdO.Warehouse = row.DestinationWarehouseCode;
                        NSC_DI.SAP.Document.SetDefaultDistributionRules(oLines);
                        bUpdatedProdO = true;
                    }
                    //Update line Crop ID
                    string strCropFinHolder = null;
                    if (row.CropName.Length > 0)
                    {
                        bUpdatedProdO = true;
                        strCropFinHolder = oCrop.GetFinProj(row.CropProjID);
                        oPdO.Project = strCropFinHolder;

                    }
                    //Update line Item warehouse
                    for (int i = 0; i <= oLines.Count - 1; i++)
                    {
                        oLines.SetCurrentLine(i);
                        if (row.ItemCode == oLines.ItemNo)
                        {
                            //string strWhsSourceHolder = NSC_DI.UTIL.SQL.GetValue<string>(@"select WhsCode from [OWHS] where OWHS.WhsName = '" + row.SourceWarehouseCode + "'", null);
                            string strWhsSourceHolder = row.SourceWarehouseCode;
                            if (strWhsSourceHolder != oLines.Warehouse && oLines.PlannedQuantity > 0)
                            {
                                //Update warehouse
                                oLines.Warehouse = strWhsSourceHolder;
                                NSC_DI.SAP.Document.SetDefaultDistributionRules(oLines);
                                bUpdatedProdO = true;
                            }
                        }
                        if (row.CropName.Length > 0)
                        {
                            //oLines.StageID = oCrop.GetStageID("PROP_CLONE_" + row.CropName.ToString());
                            oLines.Project = strCropFinHolder;
                            bUpdatedProdO = true;
                        }
                    }
                    if (bUpdatedProdO == true)
                    {
                        oPdO.Update();
                    }

                    //--------------------------------------------
                    // ISSUE TO PRODUCTION ORDER
                    var PdOIssueKey = NSC_DI.SAP.ProductionOrder.Issue(PdOKey, "Seed", row.Quantity, row.BatchNo, null, strCropFinHolder);
                    if (PdOIssueKey == 0)
                    {
                        throw new Exception(NSC_DI.UTIL.Message.Format("Failed to Issue to Production for batch " + row.BatchNo + 
                            Environment.NewLine + Globals.oCompany.GetLastErrorDescription()));
                        continue;
                    }

                    if (row.CropName.Length > 0)
                    {
                        //Update Batch with crop data
                        NSC_DI.SAP.BatchItems.UpdateUDF(row.BatchNo, "CropID", row.CropProjID);
                        NSC_DI.SAP.BatchItems.UpdateUDF(row.BatchNo, "CropStageID", "PROP_SEED_" + row.CropProjID);
                    }


                    if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                    if (row.CropProjID.ToString().Length > 0)
                    {
                        string strStageID = "PROP_SEED_" + row.CropProjID.ToString();
                        int intDocNum = NSC_DI.UTIL.SQL.GetValue<int>("Select DocNum from OWOR where DocEntry=" + PdOKey.ToString());

                        oCrop.AddProdToProject(PdOKey, intDocNum, row.CropProjID, strStageID);
                        oCrop.AddDocToProject_Issue(PMDocumentTypeEnum.pmdt_GoodsIssue, PdOIssueKey.ToString(), row.CropProjID, strStageID);
                    }

                }

                Globals.oApp.StatusBar.SetText("Complete.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
            }

            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

            /*
             **** OLD CODE **** DELETE 02/22/2021
            foreach (SeedSelectedRow row in seedTransDetails.RowsToProcess)
            {
                // Prepare a list of line items for the "Production Order"
				List<NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem> components = new List<NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem>();

				foreach (var item in NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(row.ItemCode))
                {
                    components.Add(
						new NSC_DI.SAP.ProductionOrder_OLD.DisassemblyProductionOrderComponentLineItem()
                        {
                            ItemCodeForItemBeingCreated = item.ItemCode,
                            DestinationWarehouseCode = row.DestinationWarehouseCode,
                            BaseQuantity = item.Quantity,
                            PlannedQuantity = row.Quantity
                        }
                    );
                }

                // Create a Disassembly Production Order based on the form
				var disPDOnum = NSC_DI.SAP.ProductionOrder_OLD.CreateDisassemblyProductionOrder(row.ItemCode
                    , row.Quantity
                    , row.SourceWarehouseCode
                    , DateTime.Now
                    , components
                    , row.BatchNo);

				//if (!controllerProductionOrder.WasSuccessful)
				//{
				//	// Send a message to the client
				//	Globals.oApp.StatusBar.SetText("Failed to create production order!");

				//	AllWetBudPassed = false;
				//	continue;
				//}

                // Change status to released
				NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(disPDOnum, BoProductionOrderStatusEnum.boposReleased);

                // Prepare a list of Goods Issued Lines
				List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines> ListOfGoodsIssuedLines = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();

                // Add a new item to the list of Goods Issued Lines
                ListOfGoodsIssuedLines.Add(new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
                    {
                        ItemCode = row.ItemCode,
                        Quantity = row.Quantity,
                        WarehouseCode = row.SourceWarehouseCode,
						BatchLineItems = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>()
                        {
                            new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches() {BatchNumber = row.BatchNo, Quantity = row.Quantity}
                        }
                    }
                );

                // Create an "Issue From Production" document
                NSC_DI.SAP.IssueFromProduction.Create(disPDOnum, DateTime.Now, ListOfGoodsIssuedLines);

				//if (controllerIssueFromProduction.WasSuccessful)
				//{
                    TotalProcessed += row.Quantity;
				//}
				//else
				//{
				//	AllWetBudPassed = false;
				//}
            }

            */

            //this.Load_Matrix();
             finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
            }
        }

        private void CreateGroupedProductionOrder()
        {
 /*	// COMPLIANCE
            // Pull all the form data that is required.
            SeedTransactionData seedTransDetails = PullSeedDetailsFromForm();
            if (seedTransDetails == null)
                return;

            // Create a ProductionOrder controller
            Controllers.ProductionOrder controllerProductionOrder = new Controllers.ProductionOrder(
                SAPBusinessOne_Application: Globals.oApp,
                SAPBusinessOne_Company: Globals.oCompany);

            double TotalProcessed = 0;

            if (seedTransDetails.RowsToProcess.Count <= 0)
            {
                // Send success message to the client
                Globals.oApp.StatusBar.SetText("Failed to Release and Issued all Selected Items!");

                return;
            }

            #region Create GoodsIssued and Disassembly Production Order Data Lists

            string ItemCode = "";
            string DestinationWarehouseCode = "";
            string SourceWarehouseCode = "";
            string ProjectedBatchNumber = "";
            int TotalQuantity = 0;

            // Prepare a list of Goods Issued Lines
            List<Controllers.GoodsIssued.GoodsIssued_Lines> ListOfGoodsIssuedLines = new List<Controllers.GoodsIssued.GoodsIssued_Lines>();

            // Prepare a list of line items for the "Production Order"
            List<Controllers.ProductionOrder.DisassemblyProductionOrderComponentLineItem> components = new List<Controllers.ProductionOrder.DisassemblyProductionOrderComponentLineItem>();

            // Prepare a "Bill of Materials" controller
            Controllers.BillOfMaterials controllerBillOfMaterials = new Controllers.BillOfMaterials(Globals.oApp, Globals.oCompany);

            foreach (SeedSelectedRow row in seedTransDetails.RowsToProcess)
            {


                // Add a new item to the list of Goods Issued Lines
                ListOfGoodsIssuedLines.Add(
                    new Controllers.GoodsIssued.GoodsIssued_Lines()
                    {
                        ItemCode = row.ItemCode,
                        Quantity = row.Quantity,
                        WarehouseCode = row.SourceWarehouseCode,
                        BatchLineItems = new List<Controllers.GoodsIssued.GoodsIssued_Lines_Batches>()
                        {
                            new Controllers.GoodsIssued.GoodsIssued_Lines_Batches() {BatchNumber = row.BatchNo, Quantity = row.Quantity}
                        }
                    }
                );

                ItemCode = row.ItemCode;
                DestinationWarehouseCode = row.DestinationWarehouseCode;
                SourceWarehouseCode = row.SourceWarehouseCode;
                ProjectedBatchNumber = row.BatchNo;
                TotalQuantity += (int)row.Quantity;
            }


            foreach (var item in controllerBillOfMaterials.ListOfComponentsFromAnItemCode(ItemCode))
            {
                components.Add(
                    new Controllers.ProductionOrder.DisassemblyProductionOrderComponentLineItem()
                    {
                        ItemCodeForItemBeingCreated = item.ItemCode,
                        DestinationWarehouseCode = DestinationWarehouseCode,
                        BaseQuantity = TotalQuantity,
                        PlannedQuantity = TotalQuantity
                    }
                );
            }

            #endregion

            // Create a Disassembly Production Order based on the form
            controllerProductionOrder.CreateDisassemblyProductionOrder(
                ItemCodeForItemBeingDestroyed: ItemCode
                , QuantityOfItemsToDestroy: TotalQuantity
                , SourceWarehouseCode: SourceWarehouseCode
                , dueDate: DateTime.Now
                , components: components
                , ProjectedBatchNumber: ProjectedBatchNumber);

            if (!controllerProductionOrder.WasSuccessful)
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Failed to create production order!");
            }

            // Change status to released
            controllerProductionOrder.UpdateStatus(
                key: controllerProductionOrder.NewlyCreatedKey,
                status: BoProductionOrderStatusEnum.boposReleased);

            // Create a "Issue From Production" on the currently selected "Production Order"
            Controllers.IssueFromProduction controllerIssueFromProduction = new Controllers.IssueFromProduction(
                SAPBusinessOne_Application: Globals.oApp,
                SAPBusinessOne_Company: Globals.oCompany
            );

            // Create an "Issue From Production" document
            controllerIssueFromProduction.Create(
                productionOrderKey: controllerProductionOrder.NewlyCreatedKey,
                documentDate: DateTime.Now,
                listOfGoodsIssuedLines: ListOfGoodsIssuedLines
            );

            if (controllerIssueFromProduction.WasSuccessful)
            {
                // Send success message to the client
                Globals.oApp.StatusBar.SetText("Successfully Release and Issued the Selected Items!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            else
            {
                // Send success message to the client
                Globals.oApp.StatusBar.SetText("Failed to Release and Issued all Selected Items!");
            }

            this.Load_Matrix();
*/
        }

        /// <summary>
        /// Get all data from the SeedBank form that is required.
        /// </summary>
        /// <returns>SeedTransactionData Object</returns>
        private SeedTransactionData PullSeedDetailsFromForm()
        {
            SeedTransactionData seedDetails = new SeedTransactionData();

            // Grab the textbox for days from the form
			EditText TXT_DAYS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DAYS", _SBO_Form);

            int daysToGerminate;
            if (!int.TryParse(TXT_DAYS.Value, out daysToGerminate))
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Please enter in a number for the days!");

                // Focus the cursor in the offending input
                TXT_DAYS.Active = true;
                return null;
            }

            seedDetails.dueDate = DateTime.Now.AddDays(daysToGerminate);

            // Grab the matrix from the form ui
            Matrix MTX_SEED = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_SEED", _SBO_Form);


            // For each row already selected
            for (int i = 1; i < (MTX_SEED.RowCount + 1); i++)
            {
                SeedSelectedRow newRow = new SeedSelectedRow();

                try
                {
                    // If the current row is now selected we can just continue looping.
                    if (!MTX_SEED.IsRowSelected(i))
                        continue;

                    newRow.ItemCode = ((EditText)MTX_SEED.Columns.Item(MatrixColumns["ItemCode"]).Cells.Item(i).Specific).Value;
                    newRow.BatchNo = ((EditText)MTX_SEED.Columns.Item(MatrixColumns["BatchNum"]).Cells.Item(i).Specific).Value;
                    newRow.SourceWarehouseCode = ((EditText)MTX_SEED.Columns.Item(MatrixColumns["WhsCode"]).Cells.Item(i).Specific).Value;
                    newRow.CropProjID = ((EditText)MTX_SEED.Columns.Item(MatrixColumns["CropID"]).Cells.Item(i).Specific).Value;
                    newRow.CropName = ((EditText)MTX_SEED.Columns.Item(MatrixColumns["CropName"]).Cells.Item(i).Specific).Value;

                    newRow.ProdBatchID = ((EditText)MTX_SEED.Columns.Item(MatrixColumns["ProdBatchID"]).Cells.Item(i).Specific).Value;
                    if ((NSC_DI.UTIL.Settings.Version.GetCompliance() == "METRC") && string.IsNullOrEmpty(newRow.ProdBatchID?.Trim()))// Whitespace-Change  NSC_DI.UTIL.Strings.Empty(newRow.ProdBatchID))
                    {
                        Globals.oApp.MessageBox($"All selected rows require a value for 'Prod Batch ID'");
                        return null;
                    }

                    // Keep track of which warehouse was selected
                    string SelectedDestinationWarehouseID = "";

                    double parsedQuantity;
                    if (!double.TryParse(((EditText)MTX_SEED.Columns.Item(MatrixColumns["Quantity"]).Cells.Item(i).Specific).Value, out parsedQuantity))
                    {
                        // Send a message to the client
                        Globals.oApp.StatusBar.SetText("Please enter in a number for the quantity!");

                        // Focus the cursor in the offending input
                        ((EditText)MTX_SEED.Columns.Item(MatrixColumns["Quantity"]).Cells.Item(i).Specific).Active = true;

                        return null;
                    }

                    newRow.Quantity = parsedQuantity;

                    // Prepare a ComboBox helper
					VERSCI.ComboBox ComboBox_Helper = new VERSCI.ComboBox(Globals.oApp, Globals.oCompany, _SBO_Form);

                    // Find the ComboBox in the Form UI
                    ComboBox CBX_WHSE = ((ComboBox)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.ComboBox, "CBX_WHSE", _SBO_Form));

                    // Grab the value from the ComboBox
                    newRow.DestinationWarehouseName = CBX_WHSE.Value.ToString();

                    // Get the "description" from the ComboBox, where we are really storing the value
                    newRow.DestinationWarehouseCode = ComboBox_Helper.GetComboBoxValueOrDescription(cFormID, "CBX_WHSE", null, newRow.DestinationWarehouseName);


                    // Add a row to our Transfer list.
                    seedDetails.RowsToProcess.Add(newRow);
                }
                catch (Exception e) { }
            }

            return seedDetails;
        }

		private void Load_Matrix()
        {
            // #10823 --->
            // limit the list to items in ths pecific branch
            //var defBranch = Globals.BranchDflt;         //  -1 is no branches, 0 is no default branch
            //var brList = Globals.BranchList;
            //string destWH = _SBO_Form.Items.Item("CBX_WHSE").Specific.Value;
            //if (destWH != "" && defBranch >= 0)
            //{
            //    defBranch = NSC_DI.SAP.Branch.Get(destWH);
            //    brList = NSC_DI.SAP.Branch.GetAll_User_Str(defBranch);
            //}
            // #10823 <---
            // Prepare a Matrix helper -- 
            var sql = @"
SELECT [OITM].[ItemCode],[OITM].[ItemName],[OIBT].[Quantity] AS [OnHand],[OIBT].[Quantity] AS [Quantity], SPACE(50) AS [ProdBatchID],
       [OIBT].[WhsCode],[OIBT].[BatchNum],[OIBT].[SuppSerial],
OBTN.U_NSC_CropID as [CropID], OPMG.NAME as [CropName]  
,OBTN.U_NSC_CropID as [CropID]
, OPMG.NAME as [CropName]  
FROM [OIBT]
join [OBTN] on [OBTN].DistNumber = [OIBT].BatchNum
left Join OPMG on OPMG.AbsEntry = OBTN.U_NSC_CropID
left JOIN [OITM] ON [OITM].[ItemCode] = [OIBT].[ItemCode]
left JOIN [OITB] ON [OITM].[ItmsGrpCod] = [OITB].[ItmsGrpCod]
JOIN [OWHS] ON OWHS.WhsCode = OIBT.WhsCode                                -- // #10823
WHERE [OITB].[ItmsGrpNam] = '" + cItemType + @"' AND [OIBT].[Quantity] > 0 AND OIBT.WhsCode!='9999'";
            if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})";        // #10823

            VERSCI.Matrix MatrixHelper = new VERSCI.Matrix(Globals.oApp, _SBO_Form);

            // Load the matrix of tasks
            MatrixHelper.LoadDatabaseDataIntoMatrix("OIBT", "MTX_SEED", new List<VERSCI.Matrix.MatrixColumn>() {
                            new VERSCI.Matrix.MatrixColumn(){ Caption="Item Code", ColumnWidth=40, ColumnName="ItemCode", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Item Name", ColumnWidth=80, ColumnName="ItemName", ItemType = BoFormItemTypes.it_EDIT}               
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="On Hand", ColumnWidth=80, ColumnName="OnHand", ItemType = BoFormItemTypes.it_EDIT}   
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Quantity to Use", ColumnWidth=80, ColumnName="Quantity", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Prod Batch ID",     ColumnWidth=80, ColumnName="ProdBatchID", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}                            
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Source Warehouse", ColumnWidth=0, ColumnName="WhsCode", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Batch Number", ColumnWidth=80, ColumnName="BatchNum", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ ColumnName = "SuppSerial", Caption = "State ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT} //1164
                            ,new VERSCI.Matrix.MatrixColumn(){ ColumnName="CropID", Caption="Crop ID", ColumnWidth=1, ItemType = BoFormItemTypes.it_EDIT  }
                            ,new VERSCI.Matrix.MatrixColumn(){ ColumnName="CropName", Caption="Crop Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },                            
                        }, sql);

            // Find the matrix in the form UI
            Matrix MTX_SEED = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_SEED", _SBO_Form);
            MTX_SEED.SelectionMode = BoMatrixSelect.ms_Auto;

            CommonUI.Matrix.SetLink(MTX_SEED, 0, SAPbouiCOM.BoLinkedObject.lf_Items);
            MTX_SEED.Columns.Item(6).Width = 1;

            // check compliance system - only revelant for METRC
            MTX_SEED.Columns.Item(4).Visible = (NSC_DI.UTIL.Settings.Version.GetCompliance() == "METRC");

            MTX_SEED.AutoResizeColumns();
        }
    }
}