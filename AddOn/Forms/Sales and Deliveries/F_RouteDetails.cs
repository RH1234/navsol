﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using B1WizardBase;
using SAPbouiCOM;
using System.Linq;
using Matrix = NavSol.CommonUI.Matrix;

namespace NavSol.Forms.Sales_and_Deliveries
{
    class F_RouteDetails : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_ROUTE_DETAILS";

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		public string FormUID { get; set; }
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_KEY_DOWN
	    [B1Listener(BoEventTypes.et_KEY_DOWN, true, new string[] {cFormID})]
	    public virtual bool OnBeforeKeyDown(ItemEvent pVal)
	    {
		    if (pVal.CharPressed != 9) return true;

		    bool BubbleEvent = true;
		    Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

		    switch (pVal.ItemUID)
		    {
			    case "TXT_VEH":
				    HandleVehicleCFL();
				    break;

			    case "TXT_DRIVE":
				    HandleDriverCFL();
				    break;

                case "txtDriver2":
                    HandleDriverCFL2();
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();

		    return BubbleEvent;
	    }

		//--------------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, true, new string[] { cFormID })]
		public virtual bool OnBeforeMatrixLinkPressed(ItemEvent pVal)
		{
			//if (pVal.CharPressed != 9) return true;

			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			if (pVal.ItemUID == "MTX_ROUTE") MTX_LINK_PRESS(oForm, pVal);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "1":
					BTN_SAVE_Click();
					break;

				case "BTN_VOID":
					BTN_VOID_Click();
					break;

				case "BTN_DELETE":
					BTN_DELETE_Click();
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_FORM_RESIZE
		[B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
		public virtual void OnAfterFormResize(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			//ReSize();

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
	    #endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------
        #region Initial Form Methods

		public F_RouteDetails() : this(Globals.oApp, Globals.oCompany) { }

		public F_RouteDetails(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
		{
			_SBO_Application = SAPBusinessOne_Application;
			_SBO_Company = SAPBusinessOne_Company;
		}

		public void Form_Load()
        {
            // Must provide an Id/Code
        }

        public void Form_Load_Find(string RouteID)
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                //_SBO_Form.Items.Item("txtDepDate").Specific.PickerType = BoPickerType.pt_Date;    //  this does not work, so use user source
                CommonUI.Forms.CreateUserDataSource(_SBO_Form, "txtDepDate", "dsDepDate", BoDataType.dt_DATE, 50);

                _SBO_Form.DataSources.DBDataSources.Add("@" + NSC_DI.Globals.tRouteStops);

                _SBO_Form.DataSources.DBDataSources.Add("@" + NSC_DI.Globals.tRouteDetls);
                _SBO_Form.Items.Item("txtDepTime").Specific.DataBind.SetBound(true, "@" + NSC_DI.Globals.tRouteDetls, "U_SchdOutTime");

                //Load any route details that exist in the database.
                LoadExistingRouteDetails(RouteID);

                // Load the Matrix of Deliveries
                Load_Matrix();

                // Load in the Static Google Map
                LoadStaticRouteMap(RouteID);

                // Select the first tab by default
                ((Folder)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_1", _SBO_Form)).Select();

                //_SBO_Form.Mode = BoFormMode.fm_OK_MODE;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
				_SBO_Form.Freeze(false);
				_SBO_Form.VisibleEx = true;

				//NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        public void LoadExistingRouteDetails(string RouteID)
        {
			var details = NSC_DI.SAP.DispatchingManagement.GetRouteDetailsByRouteId(RouteID);

            // Get all controlls that we need to validate.
            EditText TXT_RTEID = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_RTEID", _SBO_Form);
            EditText TXT_ROUTE = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_ROUTE", _SBO_Form);
            EditText TXT_VEH = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_VEH", _SBO_Form);
            EditText TXT_VEHID = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_VEHID", _SBO_Form);
            EditText TXT_DRIVE = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_DRIVE", _SBO_Form);
            EditText TXT_EMPID = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_EMPID", _SBO_Form);
            CheckBox CHK_ACTIVE = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.CheckBox, "CHK_ACTIVE", _SBO_Form);
            Button BTN_SAVE = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "1", _SBO_Form);
            Button BTN_VOID = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_VOID", _SBO_Form);

            EditText txtDepDate = _SBO_Form.Items.Item("txtDepDate").Specific;
            EditText txtDepTime = _SBO_Form.Items.Item("txtDepTime").Specific;
            EditText txtRoom    = _SBO_Form.Items.Item("txtRoom").Specific;
            EditText txtDriver2 = _SBO_Form.Items.Item("txtDriver2").Specific;

            try
            {
                _SBO_Form.Freeze(true);
                _SBO_Form.VisibleEx = true;

                TXT_DRIVE.Value = details.DriverName;
                TXT_EMPID.Value = details.EmployeeId;
                TXT_ROUTE.Value = details.RouteName;
                TXT_VEH.Value = details.VehicleName;
                TXT_VEHID.Value = details.VehicleId;
                TXT_RTEID.Value = RouteID;
                string sql = $@"
SELECT UFD1.Descr
  FROM CUFD
 INNER JOIN UFD1 ON CUFD.TableID = UFD1.TableID AND CUFD.FieldID = UFD1.FieldID 
 WHERE CUFD.TableID = 'OCRD'
  AND CUFD.AliasID = 'NSC_LicenseType'
  AND UFD1.FldValue = '{details.LicenseType}'";
                var val = NSC_DI.UTIL.SQL.GetValue<string>(sql, null);

                _SBO_Form.Items.Item("txtLicType").Specific.Value = val;

                //if(details.DepartureDate > DateTime.Now.AddYears(-1)) txtDepDate.Value = NSC_DI.UTIL.Dates.GetForUI(details.DepartureDate);
                txtDepDate.Value = NSC_DI.UTIL.Dates.GetForUI(details.DepartureDate);
                txtDepTime.Value = details.DepartureTime.ToString();
                txtRoom.Value    = details.Room;
                txtDriver2.Value = details.DriverName2;

                txtDepDate.Item.Enabled = true;
                txtDepTime.Item.Enabled = true;
                txtRoom.Item.Enabled    = true;
                txtDriver2.Item.Enabled = true;

                _SBO_Form.DataSources.UserDataSources.Item("UCHK_ACT").Value = details.IsActive ? "Y" : "N";
                CHK_ACTIVE.Checked = details.IsActive;  // does not appear to work. so had to set the DS

                // Set all controls to active.. then disable later
                TXT_VEHID.Item.Enabled = true;
                TXT_VEHID.Active = true;
                BTN_VOID.Item.Enabled = true;

                TXT_ROUTE.Item.Enabled = true;
                TXT_DRIVE.Item.Enabled = true;
                TXT_VEH.Item.Enabled = true;
                CHK_ACTIVE.Item.Enabled = true;

                // Don't even allow the buttons to be clicked.
                BTN_SAVE.Item.Enabled = true;
                BTN_VOID.Item.Enabled = true;

                // Disable controls if we are not allow to save.
                if (!details.AllowEditing)
                {
                    TXT_VEHID.Active = true;
                    TXT_ROUTE.Item.Enabled = false;
                    TXT_DRIVE.Item.Enabled = false;
                    TXT_VEH.Item.Enabled = false;
                    CHK_ACTIVE.Item.Enabled = false;

                    // Don't even allow the buttons to be clicked.
                    BTN_SAVE.Item.Enabled = false;
                    BTN_VOID.Item.Enabled = false;

                    //TXT_DRIVE.Active = true;
 
                    txtDepDate.Item.Enabled = false;
                    txtDepTime.Item.Enabled = false;
                    txtRoom.Item.Enabled    = false;
                    txtDriver2.Item.Enabled = false;
                }

                if (!details.EnableVoidManifest)
                {
                    BTN_VOID.Item.Enabled = false;
                    CHK_ACTIVE.Item.Enabled = false;
                }

                // These are hidden fields. - NOT NECESSARY BECAUSE THEY ARE HIDDEN
				//TXT_RTEID.Item.Enabled = false;
				//TXT_EMPID.Item.Enabled = false;
				//TXT_VEHID.Item.Enabled = false;

                _SBO_Form.Freeze(false);
            }
            catch (Exception ex)
            {
                _SBO_Form.Freeze(false);
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
        }

        public void Load_Matrix()
        {
            try
            {
                EditText TXT_RTEID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_RTEID", _SBO_Form);

                if (TXT_RTEID == null || string.IsNullOrEmpty(TXT_RTEID.Value))
                {
                    // Failed to load RouteId from form.
                    Globals.oApp.StatusBar.SetText("Failed to Load Matrix. Please reload the form.", BoMessageTime.bmt_Short);
                    return;
                }

                string RouteID = TXT_RTEID.Value;

                // Prepare a Matrix helper
                VERSCI.Matrix MatrixHelper = new VERSCI.Matrix(Globals.oApp, _SBO_Form);

                // Get all Deliveries that are not assigned to a route.
                //                string sqlQueryToPopulateMatrix = $@"
                //SELECT [ODLN].[DocEntry] AS [Code], [ODLN].[CardName] AS [BusinessPartner], [ODLN].[Address] AS [Address], [ODLN].[DocDueDate] AS [DocDueDate], 
                //       [@{NSC_DI.Globals.tRouteStops}].[U_ArriveTime], [@{NSC_DI.Globals.tRouteStops}].[U_QRCode], [ODLN].[DocNum], 
                //       ISNULL([@{NSC_DI.Globals.tRouteStops}].U_DestSeqNum, 0) AS [Order],
                //       ISNULL([@{NSC_DI.Globals.tRouteStops}].LineId, -1) AS LineId
                //  FROM [ODLN]
                // INNER JOIN [@{NSC_DI.Globals.tRouteDetls}] ON [@{NSC_DI.Globals.tRouteDetls}].DocEntry = ODLN.U_NSC_RouteID
                //  LEFT JOIN [@{NSC_DI.Globals.tRouteStops}] ON [@{NSC_DI.Globals.tRouteStops}].Code = [@{NSC_DI.Globals.tRouteDetls}].Code
                // WHERE [ODLN].DocStatus='O' AND [ODLN].U_NSC_RouteID = '{RouteID}'
                // ORDER BY [@{NSC_DI.Globals.tRouteStops}].U_DestSeqNum";

                string sqlQueryToPopulateMatrix = $@"
SELECT COALESCE(ODLN.DocEntry, OWTQ.DocEntry) AS [Code],    COALESCE(ODLN.CardName, OWTQ.CardName) AS [BusinessPartner], 
       COALESCE(ODLN.Address,  OWTQ.Address)  AS [Address], COALESCE(ODLN.DocDueDate, OWTQ.DocDueDate) AS [DocDueDate],
       [@{NSC_DI.Globals.tRouteStops}].U_ArriveTime, [@{NSC_DI.Globals.tRouteStops}].U_QRCode, [@{NSC_DI.Globals.tRouteStops}].U_DocNum AS [DocNum], 
       ISNULL([@{NSC_DI.Globals.tRouteStops}].U_DestSeqNum, 0) AS [Order], ISNULL([@{NSC_DI.Globals.tRouteStops}].LineId, - 1) AS LineId,
       COALESCE(ODLN.ObjType, OWTQ.ObjType) AS [DocType], COALESCE(ODLN.DocNum, OWTQ.DocNum) AS [DocNum], [@{NSC_DI.Globals.tRouteStops}].U_RouteText
  FROM [@{NSC_DI.Globals.tRouteDetls}]
  LEFT JOIN [@{NSC_DI.Globals.tRouteStops}] ON [@{NSC_DI.Globals.tRouteStops}].Code = [@{NSC_DI.Globals.tRouteDetls}].Code
  LEFT JOIN ODLN ON[@{NSC_DI.Globals.tRouteStops}].U_DocEntry = ODLN.DocEntry AND [@{NSC_DI.Globals.tRouteStops}].U_DocType = ODLN.ObjType
  LEFT JOIN OWTQ ON[@{NSC_DI.Globals.tRouteStops}].U_DocEntry = OWTQ.DocEntry AND [@{NSC_DI.Globals.tRouteStops}].U_DocType = OWTQ.ObjType
 WHERE COALESCE(ODLN.DocStatus, OWTQ.DocStatus) = 'O' AND [@{NSC_DI.Globals.tRouteDetls}].DocEntry = '{RouteID}'
 ORDER BY[@{NSC_DI.Globals.tRouteStops}].U_DestSeqNum";

                // Populate the Matrix with our Query.
                MatrixHelper.LoadDatabaseDataIntoMatrix("Deliveries", "MTX_ROUTE", new List<VERSCI.Matrix.MatrixColumn>()
                {
                    new VERSCI.Matrix.MatrixColumn() {Caption = "Delivery Code",    ColumnWidth = 40, ColumnName = "Code",            ItemType = BoFormItemTypes.it_LINKED_BUTTON},
                    new VERSCI.Matrix.MatrixColumn() {Caption = "Document Number",  ColumnWidth = 80, ColumnName = "DocNum",          ItemType = BoFormItemTypes.it_EDIT},
                    new VERSCI.Matrix.MatrixColumn() {Caption = "Business Partner", ColumnWidth = 80, ColumnName = "BusinessPartner", ItemType = BoFormItemTypes.it_EDIT},
                    new VERSCI.Matrix.MatrixColumn() {Caption = "Address",          ColumnWidth = 80, ColumnName = "Address",         ItemType = BoFormItemTypes.it_EDIT},
                    new VERSCI.Matrix.MatrixColumn() {Caption = "Delivery Date",    ColumnWidth = 80, ColumnName = "DocDueDate",      ItemType = BoFormItemTypes.it_EDIT},
                    new VERSCI.Matrix.MatrixColumn() {Caption = "Delivery Time",    ColumnWidth = 80, ColumnName = "U_ArriveTime",    ItemType = BoFormItemTypes.it_EDIT, BindTable = "@" + NSC_DI.Globals.tRouteStops, BindField = "U_ArriveTime"},
                    new VERSCI.Matrix.MatrixColumn() {Caption = "Order",            ColumnWidth = 60, ColumnName = "Order",           ItemType = BoFormItemTypes.it_EDIT},
                    new VERSCI.Matrix.MatrixColumn() {Caption = "QR Code",          ColumnWidth = 60, ColumnName = "U_QRCode",        ItemType = BoFormItemTypes.it_COMBO_BOX, IsEditable = true, HasPhoto = true},
                    new VERSCI.Matrix.MatrixColumn() {Caption = "LineId",           ColumnWidth = 20, ColumnName = "LineId",          ItemType = BoFormItemTypes.it_EDIT},
                    new VERSCI.Matrix.MatrixColumn() {Caption = "DocType",          ColumnWidth = 20, ColumnName = "DocType",         ItemType = BoFormItemTypes.it_EDIT},
                    new VERSCI.Matrix.MatrixColumn() {Caption = "RouteText",        ColumnWidth = 20, ColumnName = "U_RouteText",     ItemType = BoFormItemTypes.it_EDIT}
                }, sqlQueryToPopulateMatrix);

                LoadMatrixQRCodes();
                LoadMatrixTripDurations();
                // Grab the Matrix MTX_ROUTE the form 
                SAPbouiCOM.Matrix MTX_ROUTE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ROUTE", _SBO_Form);

                //MTX_ROUTE.Columns.Item(5).DataBind.SetBound(true, "@" + NSC_DI.Globals.tRouteStops, "U_ArriveTime");
                MTX_ROUTE.Columns.Item(5).Editable = _SBO_Form.Items.Item("TXT_ROUTE").Enabled;
                MTX_ROUTE.Columns.Item(6).Editable = _SBO_Form.Items.Item("TXT_ROUTE").Enabled;
                MTX_ROUTE.Columns.Item(7).Editable = _SBO_Form.Items.Item("TXT_ROUTE").Enabled;
                MTX_ROUTE.Columns.Item(10).Editable = _SBO_Form.Items.Item("TXT_ROUTE").Enabled;

                MTX_ROUTE.Columns.Item(7).Visible  = false; //  QR Code. hide until can get it fixed
                MTX_ROUTE.Columns.Item(8).Visible  = false;
                MTX_ROUTE.Columns.Item(9).Visible  = false;

                MTX_ROUTE.SelectionMode = BoMatrixSelect.ms_Single;
                MTX_ROUTE.AutoResizeColumns();

                MTX_ROUTE.Columns.Item(7).Editable = true;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oBar);
                GC.Collect();
            }
        }

        public void Load_Matrix_OLD()
	    {
		    try
		    {
			    EditText TXT_RTEID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_RTEID", _SBO_Form);

			    if (TXT_RTEID == null || string.IsNullOrEmpty(TXT_RTEID.Value))
			    {
				    // Failed to load RouteId from form.
				    Globals.oApp.StatusBar.SetText("Failed to Load Matrix. Please reload the form.", BoMessageTime.bmt_Short);
				    return;
			    }

			    string RouteID = TXT_RTEID.Value;

			    // Prepare a Matrix helper
			    VERSCI.Matrix MatrixHelper = new VERSCI.Matrix(Globals.oApp, _SBO_Form);

			    // Get all Deliveries that are not assigned to a route.
			    string sqlQueryToPopulateMatrix = string.Format(@"
SELECT [ODLN].[DocEntry] AS [Code], [ODLN].[CardName] AS [BusinessPartner], [ODLN].[Address] AS [Address], [ODLN].[DocDueDate] AS [DocDueDate], 
       [ODLN].[U_NSC_TripDuration], [ODLN].[U_NSC_QRCode], [ODLN].[DocNum]
FROM [ODLN]
WHERE [ODLN].DocStatus='O' AND [ODLN].U_NSC_RouteID = '{0}'
ORDER BY [ODLN].DocDueDate DESC", RouteID);

			    // Populate the Matrix with our Query.
			    MatrixHelper.LoadDatabaseDataIntoMatrix("Deliveries", "MTX_ROUTE", new List<VERSCI.Matrix.MatrixColumn>()
			    {
				    new VERSCI.Matrix.MatrixColumn() {Caption = "Delivery Code", ColumnWidth = 40, ColumnName = "Code", ItemType = BoFormItemTypes.it_LINKED_BUTTON}
				    ,
                    new VERSCI.Matrix.MatrixColumn() {Caption = "Document Number", ColumnWidth = 80, ColumnName = "DocNum", ItemType = BoFormItemTypes.it_EDIT}
                    ,
                    new VERSCI.Matrix.MatrixColumn() {Caption = "Business Partner", ColumnWidth = 80, ColumnName = "BusinessPartner", ItemType = BoFormItemTypes.it_EDIT}
				    ,
				    new VERSCI.Matrix.MatrixColumn() {Caption = "Address", ColumnWidth = 80, ColumnName = "Address", ItemType = BoFormItemTypes.it_EDIT}
				    ,
				    new VERSCI.Matrix.MatrixColumn() {Caption = "Delivery Due Date", ColumnWidth = 80, ColumnName = "DocDueDate", ItemType = BoFormItemTypes.it_EDIT}
				    ,
                    new VERSCI.Matrix.MatrixColumn() {Caption = "Approx. Trip Time", ColumnWidth = 80, ColumnName = "U_NSC_TripDuration", ItemType = BoFormItemTypes.it_EDIT}
                    ,
                    new VERSCI.Matrix.MatrixColumn()
				    {
					    Caption = "QR Code",
					    ColumnWidth = 80,
					    ColumnName = "U_NSC_QRCode",
					    ItemType = BoFormItemTypes.it_COMBO_BOX,
					    IsEditable = true,
					    HasPhoto = true
				    }
			    }, sqlQueryToPopulateMatrix);

			    LoadMatrixQRCodes();
                LoadMatrixTripDurations();
			    // Grab the Matrix MTX_ROUTE the form 
			    SAPbouiCOM.Matrix MTX_ROUTE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_ROUTE", _SBO_Form);

			    MTX_ROUTE.SelectionMode = BoMatrixSelect.ms_Single;
		    }
		    catch (Exception ex)
		    {
			    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    //NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

        private void LoadMatrixTripDurations()
        {
            SAPbouiCOM.Matrix oMatrix = null;
            SAPbobsCOM.Documents oDelivery = null;
            try
            {
                oMatrix = _SBO_Form.Items.Item("MTX_ROUTE").Specific;
                List<Tuple<int, string>> oMissingTripDurations = new List<Tuple<int, string>>();
                for (int i = 1; i <= oMatrix.RowCount; i++)
                {
                    string oDuration = oMatrix.GetCellSpecific("col_4", i).Value;
                    if (String.IsNullOrEmpty(oDuration))
                    {
                        string oAddress = oMatrix.GetCellSpecific("col_2", i).Value;
                        if (!string.IsNullOrEmpty(oAddress))
                        { 
                            Tuple<int, string> oMissingTripDuration = new Tuple<int, string>(i, oAddress);
                            oMissingTripDurations.Add(oMissingTripDuration);
                        }
                        else
                        {
                            oMatrix.GetCellSpecific("col_2", i).Value = string.Empty;
                        }
                    }
                }
                string[] oAddresses = oMissingTripDurations.Select(n => n.Item2).ToArray();
                string[] oDistances = NSC_DI.UTIL.GoogleMaps.GetCalcualatedDistances(oAddresses);
                for (int i = 0; i < oMissingTripDurations.Count; i++)
                {
                    int oRow = oMissingTripDurations[i].Item1;
                    string oDistance = oDistances[i];
                    string code = oMatrix.GetCellSpecific("col_0", oRow).Value;
                    oDelivery = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes);
                    oDelivery.GetByKey(int.Parse(code));
                    oDelivery.UserFields.Fields.Item("U_NSC_TripDuration").Value = oDistance;
                    if (oDelivery.Update() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                    oMatrix.GetCellSpecific("col_4", oRow).Value = oDistance;
                }
            }
            catch (System.ComponentModel.WarningException ex)
            {
                Globals.oApp.StatusBar.SetText(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMatrix);
                NSC_DI.UTIL.Misc.KillObject(oDelivery);
                GC.Collect();
            }
        }

        private void LoadMatrixQRCodes()
        {
            SAPbouiCOM.Matrix oMatrix = null;
            SAPbobsCOM.Documents oDelivery = null;
            SAPbouiCOM.ComboBox oPicture = null;
            try
            {
                oMatrix = _SBO_Form.Items.Item("MTX_ROUTE").Specific;
                for (int i = 1; i <= oMatrix.RowCount; i++)
                {
                    oPicture = oMatrix.GetCellSpecific("col_7", i);

                    // commented out util can fix the path stuff 

                    //string value = oPicture.Value;
                    //if (string.IsNullOrEmpty(value))
                    //{
                    //    string code = oMatrix.GetCellSpecific("col_0", i).Value;
                    //    var relPath = String.Format(@"NavSol/AddressQRCodes/addressqrcode_{0}.png", code);
                    //    var path = Path.Combine(Globals.oCompany.BitMapPath, relPath);
                    //    Directory.CreateDirectory(Path.GetDirectoryName(path));
                    //    string address = oMatrix.GetCellSpecific("col_2", i).Value;
                    //    NSC_DI.UTIL.QRCodeUtil.CreateMapsQRCode(address, path);

                    //    oDelivery = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes);
                    //    oDelivery.GetByKey(int.Parse(code));
                    //    oDelivery.UserFields.Fields.Item("U_NSC_QRCode").Value = relPath;
                    //    if (oDelivery.Update() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                    //}
                }
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMatrix);
                NSC_DI.UTIL.Misc.KillObject(oDelivery);
                NSC_DI.UTIL.Misc.KillObject(oPicture);
                GC.Collect();
            }
        }

        private void LoadMatrixQRCodes_OLD()
        {
            SAPbouiCOM.Matrix oMatrix = null;
            SAPbobsCOM.Documents oDelivery = null;
            SAPbouiCOM.ComboBox oPicture = null;
            try
            {
                oMatrix = _SBO_Form.Items.Item("MTX_ROUTE").Specific;
                for (int i = 1; i <= oMatrix.RowCount; i++)
                {
                    oPicture = oMatrix.GetCellSpecific("col_6", i);
                    string value = oPicture.Value;
                    if (string.IsNullOrEmpty(value))
                    {
                        string code = oMatrix.GetCellSpecific("col_0", i).Value;
                        var relPath = String.Format(@"NavSol/AddressQRCodes/addressqrcode_{0}.png", code);
                        var path = Path.Combine(Globals.oCompany.BitMapPath, relPath);
                        Directory.CreateDirectory(Path.GetDirectoryName(path)); 
                        string address = oMatrix.GetCellSpecific("col_2", i).Value;
                        NSC_DI.UTIL.QRCodeUtil.CreateMapsQRCode(address, path);

                        oDelivery = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes);
                        oDelivery.GetByKey(int.Parse(code));
                        oDelivery.UserFields.Fields.Item("U_NSC_QRCode").Value = relPath;
                        if (oDelivery.Update() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                        
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMatrix);
                NSC_DI.UTIL.Misc.KillObject(oDelivery);
                NSC_DI.UTIL.Misc.KillObject(oPicture);
                GC.Collect();
            }
        }

        #endregion

        #region Main Form Methods
        private void ReSize()
		{
			try
			{
				// Attempt to auto expand the tabs with the form..
				// TODO: Extract this into a better feature. So tab controls expand with the form.
				// Chase would appreciate this feature.
				SAPbouiCOM.Item tabGroup = (SAPbouiCOM.Item)_SBO_Form.Items.Item("Item_0");

				if (tabGroup != null)
				{
					// These are just numbers I found that worked best with the form.
					int widthDiff = 50;
					int heightDiff = 95;

					// The 570 and 735 are the values the tab control initial height and width.
					tabGroup.Height = Math.Max(404, _SBO_Form.Height - heightDiff);
					tabGroup.Width = Math.Max(564, _SBO_Form.Width - widthDiff);
				}
			}
			catch (Exception ex)
			{
				// I could care less about your problems!
			}
		}

	    public void BTN_SAVE_Click()
	    {
            SAPbouiCOM.Matrix oMat = null;

            try
		    {
                _SBO_Form.Mode = BoFormMode.fm_UPDATE_MODE;

			    // Get all controlls that we need to validate.
			    EditText TXT_RTEID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_RTEID", _SBO_Form);
			    EditText TXT_ROUTE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ROUTE", _SBO_Form);
			    EditText TXT_VEH = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_VEH", _SBO_Form);
			    EditText TXT_DRIVE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DRIVE", _SBO_Form);
			    EditText TXT_VEHID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_VEHID", _SBO_Form);
			    EditText TXT_EMPID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_EMPID", _SBO_Form);
			    CheckBox CHK_ACTIVE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_ACTIVE", _SBO_Form);

			    // Validation to see if things are filled in
			    if (string.IsNullOrEmpty(TXT_ROUTE.Value))
			    {
				    // Send a message to the client
				    Globals.oApp.StatusBar.SetText("Please enter a route name", BoMessageTime.bmt_Short);
				    TXT_ROUTE.Active = true;
				    return;
			    }

			    if (string.IsNullOrEmpty(TXT_VEH.Value))
			    {
				    // Send a message to the client
				    Globals.oApp.StatusBar.SetText("Please select a vehicle for this route.", BoMessageTime.bmt_Short);
				    TXT_VEH.Active = true;
				    return;
			    }

			    if (string.IsNullOrEmpty(TXT_DRIVE.Value))
			    {
				    // Send a message to the client
				    Globals.oApp.StatusBar.SetText("Please select a driver for this route!", BoMessageTime.bmt_Short);
				    TXT_DRIVE.Active = true;
				    return;
			    }
                var s = _SBO_Form.Items.Item("txtDepTime").Specific.Value;
                // Creating an Update to the Route Details.
                NSC_DI.SAP.DispatchingManagement.UpdateRoute updateRouteDetails = new NSC_DI.SAP.DispatchingManagement.UpdateRoute()
                {
                    RouteId         = TXT_RTEID.Value,
                    Driver          = TXT_DRIVE.Value,
                    Vehicle         = TXT_VEH.Value,
                    VehicleId       = TXT_VEHID.Value,
                    EmployeeId      = TXT_EMPID.Value,
                    IsActive        = CHK_ACTIVE.Checked,

                    DepartureDate   = NSC_DI.UTIL.Dates.FromUI(_SBO_Form.Items.Item("txtDepDate").Specific.Value),
                    DepartureTime   = NSC_DI.UTIL.Dates.FromUI(_SBO_Form.Items.Item("txtDepDate").Specific.Value)
                                    + NSC_DI.UTIL.Dates.SAPTimeToTimeSpan(_SBO_Form.Items.Item("txtDepTime").Specific.Value),
                    EmployeeId2     = _SBO_Form.Items.Item("txtEMPID2").Specific.Value,
                    Driver2         = _SBO_Form.Items.Item("txtDriver2").Specific.Value,
                    Room            = _SBO_Form.Items.Item("txtRoom").Specific.Value,
                    Map = ""
                };

			    NSC_DI.SAP.DispatchingManagement.UpdateRouteDetails(updateRouteDetails);

                // update the stops
                MatrixSave(TXT_RTEID.Value);

                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Successfully saved route details!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                _SBO_Form.Mode = BoFormMode.fm_OK_MODE;
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
				//NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

        private void MatrixSave(string pCode)
        {
            int line = 0;
            SAPbouiCOM.Matrix                oMat    = null;
            SAPbouiCOM.ComboBox              oPic    = null;

            try
            {
                if(string.IsNullOrEmpty(pCode?.Trim())) pCode = _SBO_Form.Items.Item("TXT_RTEID").Specific.Value;// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(pCode))

                List<NSC_DI.SAP.DispatchingManagement.UpdateRouteStop> oStops = new List<NSC_DI.SAP.DispatchingManagement.UpdateRouteStop>();
                oMat = _SBO_Form.Items.Item("MTX_ROUTE").Specific;

                for (int i = 1; i <= oMat.RowCount; i++)
                {
                    NSC_DI.SAP.DispatchingManagement.UpdateRouteStop oStop = new NSC_DI.SAP.DispatchingManagement.UpdateRouteStop();

                    oPic = oMat.GetCellSpecific("col_7", i);
                    string value = oPic.Value;
                    if (string.IsNullOrEmpty(value))
                    {
                        string code = oMat.GetCellSpecific("col_0", i).Value;
                        string address = oMat.GetCellSpecific("col_3", i).Value;
                        var path = NSC_DI.UTIL.QRCodeUtil.GetPath();
                        NSC_DI.UTIL.QRCodeUtil.CreateMapsQRCode(address, path);
                        oStop.QRCode = path;
                        oStop.RouteText = "";

                        //oDelivery = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes);
                        //oDelivery.GetByKey(int.Parse(code));
                        //oDelivery.UserFields.Fields.Item("U_NSC_QRCode").Value = relPath;
                        //if (oDelivery.Update() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                    }

                    oStop.Line       = Convert.ToInt32(oMat.GetCellSpecific("col_8", i).Value);
                    oStop.ArriveDate = NSC_DI.UTIL.Dates.FromUI(oMat.GetCellSpecific("col_4", i).Value);
                    oStop.ArriveTime = oStop.ArriveDate + NSC_DI.UTIL.Dates.SAPTimeToTimeSpan(oMat.GetCellSpecific("col_5", i).Value);
                    string tmp       = oMat.GetCellSpecific("col_6", i).Value;
                    oStop.Order      = string.IsNullOrEmpty(tmp?.Trim()) ? -1 : Convert.ToInt32(tmp);// Whitespace-Change NSC_DI.UTIL.Strings.Empty(tmp)
                    oStop.DocType    = Convert.ToInt32(oMat.GetCellSpecific("col_9", i).Value);
                    oStop.DocEntry   = Convert.ToInt32(oMat.GetCellSpecific("col_0", i).Value);
                    oStop.DocNum     = Convert.ToInt32(oMat.GetCellSpecific("col_1", i).Value);
                    oStop.RouteText  = oMat.GetCellSpecific("col_10", i).Value;
                    oStops.Add(oStop);
                }

                NSC_DI.SAP.DispatchingManagement.UpdateRouteStops(pCode, oStops);
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMat);
                NSC_DI.UTIL.Misc.KillObject(oPic);
                GC.Collect();
            }
        }

        public void BTN_VOID_Click()
        {
            int confirmResult = Globals.oApp.MessageBox("Are you sure you want to void this Route of Deliveries?", 1, "Yes", "No");

            if (confirmResult != 1) return;


            // Route Id of this Route Details.
            EditText TXT_RTEID = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_RTEID", _SBO_Form);

            try
            {
				NSC_DI.SAP.DispatchingManagement.VoidRouteOfDeliveries(TXT_RTEID.Value);

                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Successfully voided the route!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                    _SBO_Form.Close();
            }
            catch (Exception ex)
            {
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
        }

        public void BTN_DELETE_Click()
        {
            // Handle deleting a Delivery List/Matrix.
            // Just requires an Update = ODNL.[U_VSC_RouteID] = NULL WHERE ODNL.Code = {SELECTED}
        }

        private void HandleDriverCFL()
        {
            var title = "Driver Choose From List";

			List<CommonUI.Matrix.MatrixColumn> MatrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
            {
                 new CommonUI.Matrix.MatrixColumn(){ Caption="FirstName", ColumnWidth=40, ColumnName="FirstName", ItemType= BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="LastName", ColumnWidth=80, ColumnName="LastName", ItemType = BoFormItemTypes.it_EDIT}               
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="MiddleName", ColumnWidth=80, ColumnName="MiddleName", ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="EmployeeID", ColumnWidth=80, ColumnName="EmployeeID", ItemType = BoFormItemTypes.it_EDIT}
            };

            int cflWidth = Math.Max(450, MatrixColumns.Count * 150);
            int cflHeight = 420;
            

            // Max Width is 80% of the users screen space.
            var width = Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
            var height = Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);

            var top = (Globals.oApp.Desktop.Height - height) / 2;
            var left = (Globals.oApp.Desktop.Width - width) / 2;

            // And lastly the Query to populate the Matrix.
            var sql = $@"
SELECT OHEM.firstName AS [FirstName], OHEM.lastName AS [LastName], OHEM.middleName AS [MiddleName], OHEM.empID AS [EmployeeID]
  FROM OHEM 
  JOIN HEM6 ON HEM6.empID = OHEM.empID
  JOIN OHTY ON HEM6.roleID = OHTY.typeID
 WHERE ACTIVE = 'Y' AND OHTY.[name] = 'Driver'";
            sql+= string.IsNullOrEmpty(_SBO_Form.Items.Item("txtEMPID2").Specific.Value?.Trim())? "": $" AND OHEM.empID <> {_SBO_Form.Items.Item("txtEMPID2").Specific.Value}";// Whitespace-Change NSC_DI.UTIL.Strings.Empty(_SBO_Form.Items.Item("txtEMPID2").Specific

            F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pSelectionItems)
            {
                _SBO_Form = B1Connections.theAppl.Forms.Item(pCallingFormUid);

                DriverChooseFromListCallBack(pSelectionItems);
            };
            Form cForm_Cfl = F_CFL.FormCreate("Driver Choose From List", _SBO_Form.UniqueID, sql, MatrixColumns, false, null, top, left, height, width);
        }

        private void HandleDriverCFL2()
        {
            var title = "Driver Choose From List";

            List<CommonUI.Matrix.MatrixColumn> MatrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
            {
                 new CommonUI.Matrix.MatrixColumn(){ Caption="FirstName",  ColumnWidth=40, ColumnName="FirstName",  ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="LastName",   ColumnWidth=80, ColumnName="LastName",   ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="MiddleName", ColumnWidth=80, ColumnName="MiddleName", ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="EmployeeID", ColumnWidth=80, ColumnName="EmployeeID", ItemType = BoFormItemTypes.it_EDIT}
            };

            int cflWidth = Math.Max(450, MatrixColumns.Count * 150);
            int cflHeight = 420;


            // Max Width is 80% of the users screen space.
            var width = Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
            var height = Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);

            var top = (Globals.oApp.Desktop.Height - height) / 2;
            var left = (Globals.oApp.Desktop.Width - width) / 2;

            // And lastly the Query to populate the Matrix.
            var sql = $@"
SELECT OHEM.firstName AS [FirstName], OHEM.lastName AS [LastName], OHEM.middleName AS [MiddleName], OHEM.empID AS [EmployeeID]
  FROM OHEM 
  JOIN HEM6 ON HEM6.empID = OHEM.empID
  JOIN OHTY ON HEM6.roleID = OHTY.typeID
 WHERE ACTIVE = 'Y' AND OHTY.[name] = 'Driver'";
            sql += string.IsNullOrEmpty(_SBO_Form.Items.Item("TXT_EMPID").Specific.Value?.Trim()) ? "" : $" AND OHEM.empID <> {_SBO_Form.Items.Item("TXT_EMPID").Specific.Value}";// Whitespace-Change NSC_DI.UTIL.Strings.Empty(_SBO_Form.Items.Item("TXT_EMPID").Specific.Value)

            F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pSelectionItems)
            {
                _SBO_Form = B1Connections.theAppl.Forms.Item(pCallingFormUid);

                DriverChooseFromListCallBack2(pSelectionItems);
            };
            Form cForm_Cfl = F_CFL.FormCreate("Driver Choose From List", _SBO_Form.UniqueID, sql, MatrixColumns, false, null, top, left, height, width);
        }

        public void DriverChooseFromListCallBack(System.Data.DataTable results)
        {
            try
            {
                EditText TXT_DRIVE = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_DRIVE", _SBO_Form);
                EditText TXT_EMPID = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_EMPID", _SBO_Form);

                TXT_EMPID.Value = results.Rows[0]["EmployeeID"] as string;
                TXT_DRIVE.Value = results.Rows[0]["FirstName"] + " " + results.Rows[0]["LastName"];
            }
            catch (Exception ex)
            {
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
        }

        public void DriverChooseFromListCallBack2(System.Data.DataTable results)
        {
            try
            {
                EditText TXT_DRIVE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtDriver2", _SBO_Form);
                EditText TXT_EMPID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtEMPID2", _SBO_Form);

                TXT_EMPID.Value = results.Rows[0]["EmployeeID"] as string;
                TXT_DRIVE.Value = results.Rows[0]["FirstName"] + " " + results.Rows[0]["LastName"];
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void HandleVehicleCFL()
        {

            var title = "Vehicle Choose From List";

            // Specifying the columns we want the Superior CFL to have.
			var matrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
            {
                 new CommonUI.Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=40, ColumnName="Name", ItemType= BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="VIN", ColumnWidth=80, ColumnName="VIN", ItemType = BoFormItemTypes.it_EDIT}               
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Make", ColumnWidth=80, ColumnName="Make", ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Model", ColumnWidth=80, ColumnName="Model", ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Code", ColumnWidth=80, ColumnName="Code", ItemType = BoFormItemTypes.it_EDIT}
            };

            int cflWidth = Math.Max(450, matrixColumns.Count * 150);
            int cflHeight = 420;

            // Max Width is 80% of the users screen space.
            var width = Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
            var height = Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);

            var top = (Globals.oApp.Desktop.Height - height) / 2;
            var left = (Globals.oApp.Desktop.Width - width) / 2;

            // And lastly the Query to populate the Matrix.
            var sql = "SELECT Name, [U_Vin] AS [VIN], U_Make AS [Make], U_Model As [Model], Code FROM [@" + NSC_DI.Globals.tVehicle + @"] WHERE U_Active = 'Y'";
            F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pResults)
            {
                _SBO_Form = B1Connections.theAppl.Forms.Item(pCallingFormUid);

                VehicleChooseFromListCallBack(pResults);
            };
            Form oForm_Cfl = F_CFL.FormCreate(title, _SBO_Form.UniqueID, sql, matrixColumns, false, null, top, left, height, width);
        }

        public void VehicleChooseFromListCallBack(System.Data.DataTable pResults)
        {
            try
            {
                EditText TXT_VEH = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_VEH", _SBO_Form);
                EditText TXT_VEHID = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_VEHID", _SBO_Form);

                TXT_VEHID.Value = pResults.Rows[0]["Code"] as string;
                TXT_VEH.Value = pResults.Rows[0]["Name"] + " " + pResults.Rows[0]["Make"] + " " + pResults.Rows[0]["Model"];
            }
            catch (Exception ex)
            {
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
        }

        public void RefreshForm()
        {
            Load_Matrix();
        }
        #endregion

		private void MTX_LINK_PRESS(Form pForm, ItemEvent ItemEvent)
        {
            
            int selectedRowIndex = ItemEvent.Row;

            if (selectedRowIndex > 0)
            {
                // Grab the matrix from the Form UI
                SAPbouiCOM.Matrix mxActive = (SAPbouiCOM.Matrix)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, ItemEvent.ItemUID, pForm);

                // Get the ID of the selected
                string routeId = ((EditText)mxActive.Columns.Item(0).Cells.Item(selectedRowIndex).Specific).Value.ToString();

                // Which column stores the ID
                int ColumnIDForIDOfItemSelected = 1;

                // Get the ID of the note selected
                string ItemSelected = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_ROUTE", pForm).Columns.Item(ColumnIDForIDOfItemSelected).Cells.Item(selectedRowIndex).Specific.Value.ToString();

                try
                {
                    // Open up the Production Order Master Data form
                    Globals.oApp.ActivateMenuItem("2051");

                    // Grab the "Production Order" form from the UI
                    Form formDelivery = Globals.oApp.Forms.GetForm("140", 0);

                    // Freeze the "Production Order" form
                    formDelivery.Freeze(true);

                    // Change the "Production Order" form to find mode
                    formDelivery.Mode = BoFormMode.fm_FIND_MODE;

                    // Insert the Production Order ID in the appropriate text field
                    ((EditText)formDelivery.Items.Item("8").Specific).Value = ItemSelected;

                    // Click the "Find" button
                    ((Button)formDelivery.Items.Item("1").Specific).Item.Click();

                    // Un-Freeze the "Production Order" form
                    formDelivery.Freeze(false);
                }
                catch (Exception ex)
                {
					Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				}
            }

        }

        #region Static Map Image Loading for Route
        /// <summary>
        /// Refreshes the Static Map Route Details which will make an API call to googles Static Map API.
        /// In order for this to work it needs a RouteId for the current route.
        /// </summary>
        /// <param name="RouteId">Unique Identifier of the Route</param>
        public void BTN_REFRESH_MAP_Click(string RouteId)
        {
            try
            {
                // Get the Image reference.
                PictureBox staticMapImage = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.PictureBox, "iMap", _SBO_Form) as PictureBox;

                string apiUri = GenerateStaticMapAPIUri();
                DownloadStaticImage(apiUri, RouteId);

                //If our File exists now. We can load it.. Otherwise load in the default image.
                if (File.Exists(Path.Combine(Globals.pathToImg, string.Format(@"routemaps\staticMap_{0}.jpg", RouteId))) ? true : false)
                {
                    staticMapImage.Picture = Path.Combine(Globals.pathToImg, string.Format(@"routemaps\staticMap_{0}.jpg", RouteId));
                }
                else
                {
                    staticMapImage.Picture = Path.Combine(Globals.pathToImg, @"routemaps\StaticMap.jpg");
                }
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        /// <summary>
        /// Load up a static good map of all the different locations that exist in the route details.
        /// </summary>
        /// <param name="RouteId">Unique Identifier of the Route</param>
        public void LoadStaticRouteMap(string RouteId)
        {
            try
            {
                // Get the Image reference.
                PictureBox staticMapImage = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.PictureBox, "iMap", _SBO_Form) as PictureBox;

                // If the Image doesn't exist we can use an API call to get it.
                if (!File.Exists(Path.Combine(Globals.pathToImg, string.Format(@"routemaps\staticMap_{0}.jpg", RouteId))) ? true : false)
                {
                    string apiUri = GenerateStaticMapAPIUri();
                    DownloadStaticImage(apiUri, RouteId);
                }

                //If our File exists now. We can load it.. Otherwise load in the default image.
                if (File.Exists(Path.Combine(Globals.pathToImg, string.Format(@"routemaps\staticMap_{0}.jpg", RouteId))) ? true : false)
                {
                    staticMapImage.Picture = Path.Combine(Globals.pathToImg, string.Format(@"routemaps\staticMap_{0}.jpg", RouteId));
                }
                else
                {
                    staticMapImage.Picture = Path.Combine(Globals.pathToImg, @"routemaps\StaticMap.jpg");
                }
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
        }

        /// <summary>
        /// Generates an API Call for getting a Static Map Image.
        /// Static API call as well.
        /// </summary>
        /// <returns></returns>
        public string GenerateStaticMapAPIUri()
        {
            try
            {
                // Getting a DataTable of the DataSource.
                DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("Deliveries");

                var apiKey = NSC_DI.UTIL.Settings.Value.Get("GoogleApiKey", true);
                string apiCall = NSC_DI.UTIL.Settings.Value.Get("GoogleMapsUri");

                // What kind of Pin Markers do we want to use for each route location.
                string pinMarkers = "&markers=size:mid";

                // The seperator google uses for each destination.
                string destinationSeperator = "%7C";

                // Build out all the different pin markers we want to create.
                // This is just done by giving google the actual addresses of the locations you want to pin.
                string destinations = "";
                for (int i = 0; i < DataTable.Rows.Count; i++)
                {
                    string addressToAdd = DataTable.GetValue("Address", i).ToString();
                    destinations += string.Format("{0}{1}", destinationSeperator, addressToAdd);
                }

                // Add in our API Key into the call.
                apiCall += string.Format("{0}{1}&key={2}", pinMarkers, destinations, apiKey);

                return apiCall;
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
        }

        //private static string GetDirectionsClipPath()
        //{
        //    DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("Deliveries");
        //    for (int i = 0; i < DataTable.Rows.Count; i++)
        //    {
        //        var apiKey = NSC_DI.UTIL.Settings.Value.Get("MapsApiKey", true);
        //        var apiCall = "https://maps.googleapis.com/maps/api/directions/json";
        //        var apiParms = "origin={0}destination={1}"
        //    using (var client = new WebClient()) {
        //            client.
        //    }
        //    }
        //}

        /// <summary>
        /// Method that takes in an API Call where an Image is downloaded and Saved 
        /// to a local directory for later use.
        /// </summary>
        /// <param name="apiUri">API Call for Downloading an Image</param>
        /// <param name="RouteId">Unique Identifier of the Route</param>
        public void DownloadStaticImage(string apiUri, string RouteId)
        {
            Uri uri = new Uri(apiUri);
            // We need to kill off these two processes.
            HttpWebResponse httpResponse;
            Stream imageStream;
            // Attempt to get the jpg from the API call.
            try
            {
                HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(uri);
                httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                imageStream = httpResponse.GetResponseStream();
                Bitmap staticMap = new Bitmap(imageStream);

                httpResponse.Close();
                imageStream.Close();

                // We are saving the images into the routemaps directory.
                string directory = Path.Combine(Globals.pathToImg, string.Format(@"routemaps\staticMap_{0}.jpg", RouteId));
                staticMap.Save(directory, ImageFormat.Jpeg);
            }
            catch (Exception ex)
            {
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
        }

        #endregion

    }
}
