﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbouiCOM;
using Matrix = NavSol.VERSCI.Matrix;

namespace NavSol.Forms.Sales_and_Deliveries
{
	class F_InboundDeliveriesList : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_INC_LIST";

		private enum TableNames { ID, Name, ItemCount, ArrivalDate, ManifestId }

		protected class InventoryManifestLookUpResult
		{
			public string success { get; set; }
			public string sessiontime { get; set; }
			public List<Data> data = new List<Data>();
		}

		protected class Data
		{
			public string item_count { get; set; }
			public string license_number { get; set; }
			public string manifest_id { get; set; }
			public string trade_name { get; set; }
			public string transfer_date { get; set; }
		}

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		public string FormUID { get; set; }
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			var BubbleEvent = true;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "MTX_MAIN":
					MTX_LINK_PRESS(pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		public F_InboundDeliveriesList() : this(Globals.oApp, Globals.oCompany) { }

		public F_InboundDeliveriesList(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
		{
			_SBO_Application = SAPBusinessOne_Application;
			_SBO_Company = SAPBusinessOne_Company;
		}

        public void Form_Load()
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

				// Set the main image
				CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_MAIN", @"list-icon.bmp");

				InitializeMatrix();

				LoadIncomingDeliveries();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
				_SBO_Form.Freeze(false);
				_SBO_Form.VisibleEx = true;

				//NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
				GC.Collect();
            }
        }

        private void InitializeMatrix()
        {
            // Find the Matrix on the form UI
			SAPbouiCOM.Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);
            MTX_MAIN.SelectionMode = BoMatrixSelect.ms_Single;

            if (MTX_MAIN.RowCount == 0)
            {
                // Prepare a Matrix Helper
                Matrix VirSci_Helpers_Matrix = new Matrix(Globals.oApp, _SBO_Form);

                VirSci_Helpers_Matrix.LoadDatabaseDataIntoMatrix("ItemTable", "MTX_MAIN", new List<Matrix.MatrixColumn>() {
                            new Matrix.MatrixColumn() { Caption="ID", ColumnName=TableNames.ManifestId.ToString("F"), ColumnWidth=80, ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                            ,new Matrix.MatrixColumn() { Caption="Name", ColumnName=TableNames.Name.ToString("F"), ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn() { Caption="Item Count", ColumnName=TableNames.ItemCount.ToString("F"), ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn() { Caption="Expected Arrival Date", ColumnName=TableNames.ArrivalDate.ToString("F"), ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn() { Caption="Manifest ID", ColumnName=TableNames.ManifestId.ToString("F"), ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT}
                        }, @"
SELECT * FROM (SELECT '9999' AS [ID],'99999999ABCDEFGHIJKLMNOPZA' AS [Name],'99999' AS [ItemCount], '99999999ABCDEFGHI' AS [ArrivalDate], '99999999ABCDEFGHIJKL' AS [ManifestId]) AS [ItemTable]"
                );

                // Get the table the Matrix is using..
                DataTable actualDataTable = _SBO_Form.DataSources.DataTables.Item("ItemTable");
                actualDataTable.Rows.Remove(0);
                MTX_MAIN.LoadFromDataSource();
            }
        }

        private void LoadIncomingDeliveries()
        {
 /*	// COMPLIANCE
            // Get our Settings controller for the State Information for Complain calls.
            NSC_DI.UTIL.Settings controllerSettings = new Controllers.Settings(
                SAPBusinessOne_Application: Globals.oApp,
                SAPBusinessOne_Company: Globals.oCompany,
                SAPBusinessOne_Form: _SBO_Form);

            try
            {
                // License_Number of this Facility
                string LocationId = controllerSettings.GetValueOfSetting("StateLocLic");

                // Prepare a connection to the state API
                var TraceCon = new NSCNSC_DI.SAP._DI.UTIL.Misc.KillObject(oForm);TraceabilityAPI(SAPBusinessOne_Application: Globals.oApp,
                    SAPBusinessOne_Company: Globals.oCompany,
                    SAPBusinessOne_Form: _SBO_Form);

                BioTrack.API bioCrapAPI = TraceCon.new_API_obj();

                BioTrack.Inventory.ManifestLookup(ref bioCrapAPI, LocationId);

                bioCrapAPI.PostToApi();

                if (!bioCrapAPI.WasSuccesful)
                {
                    Globals.oApp.StatusBar.SetText("No manifests were found. If you were expecting a delivery please refresh the form.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);

                    return;
                }

                string xmlToParse = bioCrapAPI.xmlDocFromResponse.InnerXml.ToString();

//				// Example data of the response. -- 
//				 xmlToParse = @"<xml>
//                        <data>
//                            <item_count>1</item_count>
//                            <license_number>18750</license_number>
//                            <manifest_id>7949844847294004</manifest_id>
//                            <trade_name>Trade 24</trade_name>
//                            <transfer_date>01/21/2014</transfer_date>
//                        </data>
//                        <data>
//                            <item_count>1</item_count>
//                            <license_number>18750</license_number>
//                            <manifest_id>7949844847294004</manifest_id>
//                            <trade_name>Trade 24</trade_name>
//                            <transfer_date>01/21/2014</transfer_date>
//                        </data>
//                        <success>1</success>
//                        <sessiontime>1390548537</sessiontime>
//                    </xml>"; 

                XDocument _xmlResponse = XDocument.Parse(xmlToParse);

                // Parse our Result data into an Object.
                InventoryManifestLookUpResult result = (from xmlRoot in _xmlResponse.Descendants("xml")
                                                        select new InventoryManifestLookUpResult
                                                        {
                                                            sessiontime = xmlRoot.Element("sessiontime").Value,
                                                            success = xmlRoot.Element("success").Value,
                                                            data = (from xmlElement in _xmlResponse.Descendants("data")
                                                                    select new Data
                                                                    {
                                                                        item_count = xmlElement.Element("item_count").Value,
                                                                        license_number = xmlElement.Element("license_number").Value,
                                                                        manifest_id = xmlElement.Element("manifest_id").Value,
                                                                        trade_name = xmlElement.Element("trade_name").Value,
                                                                        transfer_date = xmlElement.Element("transfer_date").Value
                                                                    }).ToList<Data>()
                                                        }).FirstOrDefault();


				SAPbouiCOM.Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

                try
                {
                    // If the DataTable exists before the Matrix creation.. clear the table out.
                    DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("ItemTable");

                    DataTable.Rows.Clear();

                }
                catch (Exception ex)
                {
                    // We don't care... I just need to clear out the DataTable if it exists.
                }

                int oNewRowNumber = 0;
                foreach (Data d in result.data)
                {
                    // Determine the next row number
                    DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("ItemTable");

                    DataTable.Rows.Add();
                    DataTable.SetValue(TableNames.ID.ToString("F"), oNewRowNumber, oNewRowNumber + 1);
                    DataTable.SetValue(TableNames.Name.ToString("F"), oNewRowNumber, d.trade_name.Substring(0, Math.Min(d.trade_name.Length, 25)));
                    DataTable.SetValue(TableNames.ItemCount.ToString("F"), oNewRowNumber, d.item_count);
                    DataTable.SetValue(TableNames.ArrivalDate.ToString("F"), oNewRowNumber, d.transfer_date);
                    DataTable.SetValue(TableNames.ManifestId.ToString("F"), oNewRowNumber, d.manifest_id);

                    oNewRowNumber += 1;
                }

                MTX_MAIN.LoadFromDataSource();
            }
            catch (Exception ex)
            {
                Globals.oApp.StatusBar.SetText("Failed to communicate with State API! Please reload the form.");
            }
*/
        }

		private void MTX_LINK_PRESS(ItemEvent ItemEvent)
		{
			var selectedRowIndex = ItemEvent.Row;

			if (selectedRowIndex > 0)
			{
				// Grab the matrix from the Form UI
				var mxActive = (SAPbouiCOM.Matrix)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, ItemEvent.ItemUID, _SBO_Form);

				// Get the ID of the selected
				var routeId = ((EditText)mxActive.Columns.Item(0).Cells.Item(selectedRowIndex).Specific).Value.ToString();

				var RouteDetailsForm = new F_RouteDetails(Globals.oApp, Globals.oCompany) as F_RouteDetails;
				RouteDetailsForm.Form_Load_Find(routeId);
			}
		}
	}
}