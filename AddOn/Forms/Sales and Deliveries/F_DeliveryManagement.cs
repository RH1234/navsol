﻿using System;
using System.Collections.Generic;
using System.Drawing;
using B1WizardBase;
using SAPbouiCOM;
using Matrix = NavSol.VERSCI.Matrix;

namespace NavSol.Forms.Sales_and_Deliveries
{
    class F_DeliveryManagement : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_DELIVERY_MGNT";

		private const string Deliveries_WaitingRoute_MTX_TABLE_NAME = "WaitingRoute";
		private const string TIMER_MTX_TABLE_NAME = "WaitingTimer";
		private const string ROUTE_READY_MTX_TABLE_NAME = "ReadyForDelivery";
		private const string ROUTE_OUT_MTX_TABLE_NAME = "OutForDelivery";

		private enum DeliveryMatrixColumns { Code, BusinessPartner, Address, DocDueDate }

		private enum WaitingTimerMatrixColumns { Code, Name, Driver, Vehicle }

		private enum RouteReadyMatrixColumns { Code, Name, Driver, Vehicle, TimeRemaining, TimerDate, TimerTime }

		private enum OutRouteMatrixColumns { Code, Name, Driver, Vehicle, DateTimeOut }

		private enum RouteHistoryMatrixColumns { Code, Name, Driver, Vehicle, DateTimeOut, DateTimeIn }

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		public string FormUID { get; set; }
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
	    [B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, true, new string[] {cFormID})]
	    public virtual bool OnBeforeMatrixLinkPressed(ItemEvent pVal)
	    {
		    var BubbleEvent = true;
		    var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

		    switch (pVal.ItemUID)
		    {
				//case "MTX_DELIV": Its a standard delivery
			    case "MTX_RDEL":
			    case "MTX_RCNT":
			    case "MTX_DOUT":
			    case "MTX_HIST":
				    MTX_LINK_PRESS(oForm, pVal);
				    break;
		    }

		    NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();

		    return BubbleEvent;
	    }

	    #endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "BTN_DELIV":
					CreateDelivery();
					break;

				case "BTN_START":
					StartDeliveryCounter();
					break;

				case "BTN_OUT":
					MarkDeliveryAsOut();
					break;

				case "BTN_IN":
					MarkDeliveryAsComplete();
					break;

				case "BTN_RFRESH":
					ReloadMatrices();
					break;

				case "MTX_RDEL":
					RouteReady_MTX_Pressed(pVal);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}


        //--------------------------------------------------------------------------------------- et_COMBO_SELECT
        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
        public virtual void OnAfterComboSelect(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            ConboSelect(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------
        //----------------------------------------------  Initial Form Methods

        public F_DeliveryManagement() : this(Globals.oApp, Globals.oCompany) { }

		public F_DeliveryManagement(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
		{
			_SBO_Application = SAPBusinessOne_Application;
			_SBO_Company = SAPBusinessOne_Company;
		}

        public void Form_Load()
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

				// Set the main image
				CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_MAIN", "delivery-icon.jpg");

				// Set a refresh button icon
				SAPbouiCOM.Button BTN_RFRESH = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "BTN_RFRESH", _SBO_Form);

				BTN_RFRESH.Type = SAPbouiCOM.BoButtonTypes.bt_Image;
				BTN_RFRESH.Image = System.IO.Path.Combine(Globals.pathToImg, "icon_refresh.png");

                // license Type
                CommonUI.ComboBox.ComboBoxAdd_UFD1(_SBO_Form, "cbxLicType", "OCRD", "NSC_LicenseType");
                CommonUI.Forms.SetField(_SBO_Form, "cbxLicType", "M");

                // Load all the Matrices of the form.
                Load_Deliveries_WaitingRoute_Matrix();

				Load_RoutesWaitingTimer_Matrix();
				Load_RoutesReadyForDelivery_Matrix();
				Load_RoutesOutForDelivery_Matrix();

				Load_RouteHistory_Matrix();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
				_SBO_Form.Freeze(false);
				_SBO_Form.VisibleEx = true;

				//NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        private void Load_Deliveries_WaitingRoute_Matrix()
        {
            try
            {
                // Get the license type
                var licType = CommonUI.Forms.GetField<string>(_SBO_Form, "cbxLicType");

                // Prepare a Matrix helper
                var MatrixHelper = new Matrix(Globals.oApp, _SBO_Form);

            // Get all Deliveries that are not assigned to a route.
            var sqlQueryToPopulateMatrix = string.Format($@"
SELECT [ODLN].[DocEntry] AS [Code], [ODLN].[CardName] AS [BusinessPartner], [ODLN].[Address] AS [Address], [ODLN].[DocDueDate] AS [DocDueDate]
FROM [ODLN] 
INNER JOIN OCRD ON OCRD.CardCode = ODLN.CardCode
WHERE [ODLN].DocStatus='O' 	AND ( [ODLN].U_NSC_RouteID IS NULL OR [ODLN].U_NSC_RouteID <= 0 )
  AND OCRD.U_NSC_LicenseType = '{licType}'
ORDER BY [ODLN].DocDueDate DESC");

            // Populate the Matrix with our Query.
            MatrixHelper.LoadDatabaseDataIntoMatrix(Deliveries_WaitingRoute_MTX_TABLE_NAME, "MTX_DELIV", new List<VERSCI.Matrix.MatrixColumn>() {
                            new Matrix.MatrixColumn(){ Caption="Delivery Code", ColumnWidth=40, ColumnName="Code", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                            ,new Matrix.MatrixColumn(){ Caption="Business Partner", ColumnWidth=80, ColumnName="BusinessPartner", ItemType = BoFormItemTypes.it_EDIT}               
                            ,new Matrix.MatrixColumn(){ Caption="Address", ColumnWidth=80, ColumnName="Address", ItemType = BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn(){ Caption="Delivery Due Date", ColumnWidth=80, ColumnName="DocDueDate", ItemType = BoFormItemTypes.it_EDIT}
  
                    }, sqlQueryToPopulateMatrix);

            


            // Grab the Matrix MTX_DELIV the form 
            SAPbouiCOM.Matrix MTX_DELIV = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_DELIV", _SBO_Form
            );
            //SET lINK TYPE
           SAPbouiCOM.LinkedButton LinkBtn = MTX_DELIV.Columns.Item("col_0").ExtendedObject as SAPbouiCOM.LinkedButton;
            LinkBtn.LinkedObject = BoLinkedObject.lf_DeliveryNotes;

            MTX_DELIV.SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        private void Load_RoutesWaitingTimer_Matrix()
	    {
		    try
		    {
			    // Prepare a Matrix helper
			    var MatrixHelper = new Matrix(Globals.oApp, _SBO_Form);

			    // Get all Routes that currently do not have the Timer complete or set
			    var sqlQueryToPopulateMatrix = string.Format(@"
SELECT [Code],[Name],[U_Driver] AS [Driver],[U_Vehicle] AS [Vehicle]
  FROM [@" + NSC_DI.Globals.tRouteDetls + @"]
 WHERE [U_Active] = 'Y' AND [U_CNT_DOWN_ON] IS NULL
 ORDER BY [U_CNT_DOWN_DATE] DESC");

			    // Populate the Matrix with our Query.
			    MatrixHelper.LoadDatabaseDataIntoMatrix(TIMER_MTX_TABLE_NAME, "MTX_RCNT", new List<Matrix.MatrixColumn>()
			    {   new Matrix.MatrixColumn() {Caption = "Id", ColumnWidth = 40, ColumnName = "Code", ItemType = BoFormItemTypes.it_LINKED_BUTTON},
				    new Matrix.MatrixColumn() {Caption = "Name", ColumnWidth = 80, ColumnName = "Name", ItemType = BoFormItemTypes.it_EDIT},
				    new Matrix.MatrixColumn() {Caption = "Driver", ColumnWidth = 80, ColumnName = "Driver", ItemType = BoFormItemTypes.it_EDIT},
				    new Matrix.MatrixColumn() {Caption = "Vehicle", ColumnWidth = 80, ColumnName = "Vehicle", ItemType = BoFormItemTypes.it_EDIT}
			    }, sqlQueryToPopulateMatrix);

			    // Grab the Matrix MTX_RCNT the form 
			    SAPbouiCOM.Matrix MTX_RCNT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_RCNT", _SBO_Form);

			    MTX_RCNT.SelectionMode = BoMatrixSelect.ms_Single;
		    }
		    catch (Exception ex)
		    {
			    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    //NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

        private void Load_RoutesReadyForDelivery_Matrix()
        {
            try
            {
                _SBO_Form.Freeze(true);

                // Prepare a Matrix helper
                var MatrixHelper = new Matrix(Globals.oApp, _SBO_Form);

                // Get all Routes that currently do not have the Timer complete or set
                var sqlQueryToPopulateMatrix = string.Format(@"
SELECT [Code],[Name],[U_Driver] AS [Driver],[U_Vehicle] AS [Vehicle],[U_CNT_DOWN_DATE] AS [TimerDate],[U_CNT_DOWN_ON] AS [TimerTime],'abc12345678acsvb12341223' as [TimeRemaining]
  FROM [@" + NSC_DI.Globals.tRouteDetls + @"]
 WHERE [U_Active] = 'Y' AND [U_OutTime] IS NULL AND [U_CNT_DOWN_ON] IS NOT NULL
 ORDER BY [U_CNT_DOWN_ON] DESC");

                // Populate the Matrix with our Query.
                MatrixHelper.LoadDatabaseDataIntoMatrix(ROUTE_READY_MTX_TABLE_NAME, "MTX_RDEL", new List<Matrix.MatrixColumn>() {
                            new Matrix.MatrixColumn(){ Caption="Id", ColumnWidth=40, ColumnName="Code", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                            ,new Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=80, ColumnName="Name", ItemType = BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn(){ Caption="Driver", ColumnWidth=80, ColumnName="Driver", ItemType = BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn(){ Caption="Vehicle", ColumnWidth=80, ColumnName="Vehicle", ItemType = BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn(){ Caption="Time Remaining", ColumnWidth=80, ColumnName="TimeRemaining", ItemType = BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn(){ Caption="Timer Start Date", ColumnWidth=80, ColumnName="TimerDate", ItemType = BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn(){ Caption="TimerTime", ColumnWidth=80, ColumnName="TimerTime", ItemType = BoFormItemTypes.it_EDIT}
                    }, sqlQueryToPopulateMatrix);

                // Grab the Matrix from the form 
                SAPbouiCOM.Matrix MTX_RDEL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_RDEL", _SBO_Form);

                // Change row color of items in matrix based off of the compliance status
                var MTX_RDEL_SETTINGS = MTX_RDEL.CommonSetting;

                // Get and Clear out our DataTable so we can populate with our new selected data.
                var RouteReadyMatrixDataTable = _SBO_Form.DataSources.DataTables.Item(ROUTE_READY_MTX_TABLE_NAME);

                for (var i = 1; i < (MTX_RDEL.RowCount + 1); i++)
                {
                    try
                    {
                        // Stored the Ticks of time so we can just parse the ticks right into DateTime.
                        long timeInTicks = long.Parse(MTX_RDEL.Columns.Item(RouteReadyMatrixColumns.TimerTime).Cells.Item(i).Specific.Value);
                        var timerStartTime = new DateTime(timeInTicks);

                        if (DateTime.Now >= (timerStartTime + GetRouteTimeSpan()))
                        {
                            MTX_RDEL_SETTINGS.SetRowBackColor(i, ColorTranslator.ToWin32(Color.LightGreen));
                            RouteReadyMatrixDataTable.SetValue("TimeRemaining", i - 1, "Ready");
                        }
                        else
                        {
                            timerStartTime = timerStartTime + GetRouteTimeSpan();
                            var timeRemaining = timerStartTime - DateTime.Now;

                            var formatted = string.Format("{0}{1}{2}{3}",
                                timeRemaining.Duration().Days > 0 ? string.Format("{0:0} d, ", timeRemaining.Days) : string.Empty,
                                timeRemaining.Duration().Hours > 0 ? string.Format("{0:0} h, ", timeRemaining.Hours) : string.Empty,
                                timeRemaining.Duration().Minutes > 0 ? string.Format("{0:0} m, ", timeRemaining.Minutes) : string.Empty,
                                timeRemaining.Duration().Seconds > 0 ? string.Format("{0:0} s", timeRemaining.Seconds) : string.Empty);

                            if (formatted.EndsWith(", "))
                                formatted = formatted.Substring(0, formatted.Length - 2);

                            if (string.IsNullOrEmpty(formatted))
                                formatted = "0 s";


                            MTX_RDEL_SETTINGS.SetRowBackColor(i, ColorTranslator.ToWin32(Color.LightPink));
                            RouteReadyMatrixDataTable.SetValue("TimeRemaining", i - 1, formatted);
                        }
                    }
                    catch (Exception ex)
                    {
                        // Send a message to the client
                        Globals.oApp.StatusBar.SetText("Failed handle matrix load events: " + ex.Message, BoMessageTime.bmt_Short);
                    }
                }


                // Find the Timer Ticks column to hide it
                foreach (Column oMatrixColumn in MTX_RDEL.Columns)
                {
                    if (oMatrixColumn.TitleObject.Caption == "TimerTime")
                    {
                        oMatrixColumn.Width = 0;
                        break;
                    }
                }

                // Set the Matrix Selection to single only.
                MTX_RDEL.SelectionMode = BoMatrixSelect.ms_Single;

                // Reload from our datasource.
                MTX_RDEL.LoadFromDataSource();
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            { 
                _SBO_Form.Freeze(false);
            }
        }

        public void ConboSelect(Form pForm, ItemEvent pVal)
        {
            try
            {
                if(pVal.ItemUID == "cbxLicType")                // license Type
                {
                    Load_Deliveries_WaitingRoute_Matrix();
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

        private static TimeSpan GetRouteTimeSpan()
        {
            double oValue;
            if (double.TryParse(NSC_DI.UTIL.Options.Value.GetSingle("RouteTimer"), out oValue))
            {
                return TimeSpan.FromHours(oValue);
            }
            else throw new ApplicationException("Option RouteTime is not valid");

        }

        private void Load_RoutesOutForDelivery_Matrix()
        {
            // Prepare a Matrix helper
            var MatrixHelper = new Matrix(Globals.oApp, _SBO_Form);

            // Get all Routes that currently do not have the Timer complete or set
            var sqlQueryToPopulateMatrix = string.Format(@"
SELECT [Code],[Name],[U_Driver] AS [Driver],[U_Vehicle] AS [Vehicle],[U_OutDate] AS [DateTimeOut]
  FROM [@" + NSC_DI.Globals.tRouteDetls + @"]
 WHERE [U_Active] = 'Y' AND [U_OutTime] IS NOT NULL AND [U_InTime] IS NULL
 ORDER BY [CreateDate] DESC");

            // Populate the Matrix with our Query.
            MatrixHelper.LoadDatabaseDataIntoMatrix("OutForDelivery", "MTX_DOUT", new List<Matrix.MatrixColumn>() {
                            new Matrix.MatrixColumn(){ Caption="Id", ColumnWidth=40, ColumnName="Code", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                            ,new Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=80, ColumnName="Name", ItemType = BoFormItemTypes.it_EDIT}               
                            ,new Matrix.MatrixColumn(){ Caption="Driver", ColumnWidth=80, ColumnName="Driver", ItemType = BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn(){ Caption="Vehicle", ColumnWidth=80, ColumnName="Vehicle", ItemType = BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn(){ Caption="Out Since", ColumnWidth=80, ColumnName="DateTimeOut", ItemType = BoFormItemTypes.it_EDIT}
   
                    }, sqlQueryToPopulateMatrix);

            // Grab the Matrix MTX_DOUT the form 
            SAPbouiCOM.Matrix MTX_DOUT = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_DOUT", _SBO_Form);

            MTX_DOUT.SelectionMode = BoMatrixSelect.ms_Single;
        }

        private void Load_RouteHistory_Matrix()
        {
            // Prepare a Matrix helper
            var MatrixHelper = new Matrix(Globals.oApp, _SBO_Form);

            // Get all Routes that currently do not have the Timer complete or set
            var sqlQueryToPopulateMatrix = string.Format(@"
SELECT [Code],[Name],[U_Driver] AS [Driver],[U_Vehicle] AS [Vehicle],[U_OutDate] AS [DateTimeOut],[U_InDate] AS [DateTimeIn]
  FROM [@" + NSC_DI.Globals.tRouteDetls + @"]
 WHERE [U_Active] = 'N' AND [U_InTime] IS NOT NULL
 ORDER BY [U_CNT_DOWN_DATE] DESC
");

            // Populate the Matrix with our Query.
            MatrixHelper.LoadDatabaseDataIntoMatrix("History", "MTX_HIST", new List<Matrix.MatrixColumn>() {
                            new Matrix.MatrixColumn(){ Caption="Id", ColumnWidth=40, ColumnName="Code", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                            ,new Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=80, ColumnName="Name", ItemType = BoFormItemTypes.it_EDIT}               
                            ,new Matrix.MatrixColumn(){ Caption="Driver", ColumnWidth=80, ColumnName="Driver", ItemType = BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn(){ Caption="Vehicle", ColumnWidth=80, ColumnName="Vehicle", ItemType = BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn(){ Caption="Out Date", ColumnWidth=80, ColumnName="DateTimeOut", ItemType = BoFormItemTypes.it_EDIT}
                            ,new Matrix.MatrixColumn(){ Caption="In Date", ColumnWidth=80, ColumnName="DateTimeIn", ItemType = BoFormItemTypes.it_EDIT}
   
                    }, sqlQueryToPopulateMatrix);

            // Grab the Matrix MTX_HIST the form 
            SAPbouiCOM.Matrix MTX_HIST = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_HIST", _SBO_Form
            );

            MTX_HIST.SelectionMode = BoMatrixSelect.ms_Single;
        }

        //----------------------------------------------  Main Form Methods
	    private void CreateDelivery()
	    {
		    try
		    {
			    // Get Controlls needed from the form.
			    SAPbouiCOM.Matrix MTX_DELIV = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_DELIV", _SBO_Form);

			    EditText TXT_DELIV = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DELIV", _SBO_Form);

			    // Validate that the user has entered a name for the new Delivery.
			    if (string.IsNullOrEmpty(TXT_DELIV.Value))
			    {
				    // Send a message to the client
				    Globals.oApp.StatusBar.SetText("Please enter in a name for the route!", BoMessageTime.bmt_Short);

				    // Set the field to active.. so they can fill it in.
				    TXT_DELIV.Active = true;
				    return;
			    }

			    var deliveryCodesInRoute = new List<int>();

			    // Get all Selected Matrix columns.. these are what the Route Line Items will be.
			    var deliveryCodeOfSelected = "";
			    for (var i = 1; i < (MTX_DELIV.RowCount + 1); i++)
			    {
				    try
				    {
					    // If the current row is now selected we can just continue looping.
					    if (!MTX_DELIV.IsRowSelected(i))
						    continue;

					    deliveryCodeOfSelected = MTX_DELIV.Columns.Item(DeliveryMatrixColumns.Code).Cells.Item(i).Specific.Value;
					    if (!string.IsNullOrEmpty(deliveryCodeOfSelected))
					    {
						    // Should have no trouble parsing integers from the Codes.
						    deliveryCodesInRoute.Add(Int32.Parse(deliveryCodeOfSelected));
					    }
				    }
				    catch (Exception ex)
				    {
					    // Send a message to the client
					    Globals.oApp.StatusBar.SetText("Failed to grab grid selections. Please try again.", BoMessageTime.bmt_Short);
				    }
			    }

			    if (deliveryCodesInRoute.Count == 0)
			    {
				    Globals.oApp.StatusBar.SetText("Please verify your selection before creating a route.", BoMessageTime.bmt_Short);
				    return;
			    }

			    NSC_DI.SAP.DispatchingManagement.CreateRoute newRouteToCreate = new NSC_DI.SAP.DispatchingManagement.CreateRoute();

			    newRouteToCreate.IsActive = true;
			    newRouteToCreate.RouteName = TXT_DELIV.Value;
                //newRouteToCreate.LicenseType = CommonUI.Forms.GetField<string>(_SBO_Form, "cbxLicType", true);
                newRouteToCreate.LicenseType = _SBO_Form.Items.Item("cbxLicType").Specific.Value; // this gets the code
                newRouteToCreate.DeliveryCodes = deliveryCodesInRoute;

			    string routeId = NSC_DI.SAP.DispatchingManagement.CreateNewRoute(newRouteToCreate);

			    var routeDetailsForm = new F_RouteDetails(Globals.oApp, Globals.oCompany) as F_RouteDetails;
			    routeDetailsForm.Form_Load_Find(routeId);

			    // Send a message to the client
			    Globals.oApp.StatusBar.SetText("Successfully created a new route!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

			    // Reload the Matrix.
			    Load_Deliveries_WaitingRoute_Matrix();
			    Load_RoutesWaitingTimer_Matrix();
			    TXT_DELIV.Value = "";
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    //NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
			    GC.Collect();
		    }
	    }

	    private void StartDeliveryCounter()
	    {
		    try
		    {
			    // Grab the Matrix from the form 
			    SAPbouiCOM.Matrix MTX_RCNT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_RCNT", _SBO_Form);

			    var routeCodeOfSelectedMatrix = "";
			    for (var i = 1; i < (MTX_RCNT.RowCount + 1); i++)
			    {
				    try
				    {
					    // If the current row is now selected we can just continue looping.
					    if (!MTX_RCNT.IsRowSelected(i)) continue;

					    routeCodeOfSelectedMatrix = MTX_RCNT.Columns.Item(WaitingTimerMatrixColumns.Code).Cells.Item(i).Specific.Value;
					    break;
				    }
				    catch (Exception ex)
				    {
					    // Something broke. Not really sure why but we don't need to error to the client.
					    // Validation will pick up the error.
				    }
			    }

			    // Validate that the user made a selection
			    if (string.IsNullOrEmpty(routeCodeOfSelectedMatrix))
			    {
				    // Send a message to the client
				    Globals.oApp.StatusBar.SetText("Please select a row before attempting!", BoMessageTime.bmt_Short);

				    return;
			    }

			    if (!NSC_DI.SAP.DispatchingManagement.DriverAndVehicleAreSet(routeCodeOfSelectedMatrix))
			    {
				    // Send a message to the client
				    Globals.oApp.StatusBar.SetText("Route must have Driver and Vehicle set!", BoMessageTime.bmt_Short);
				    return;
			    }

			    NSC_DI.SAP.DispatchingManagement.StartRouteDeliveriesTimer(routeCodeOfSelectedMatrix);

			    // Send a message to the client
			    Globals.oApp.StatusBar.SetText("Successfully started route timer!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

			    // Reload the Matrix.
			    Load_RoutesWaitingTimer_Matrix();
			    Load_RoutesReadyForDelivery_Matrix();
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    //NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

	    private void MarkDeliveryAsOut()
	    {
		    try
		    {
			    // Grab the Matrix from the form 
			    SAPbouiCOM.Matrix MTX_RDEL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_RDEL", _SBO_Form);

			    var routeCodeOfSelectedMatrix = "";

			    for (var i = 1; i < (MTX_RDEL.RowCount + 1); i++)
			    {
				    try
				    {
					    // If the current row is not selected we can just continue looping.
					    if (!MTX_RDEL.IsRowSelected(i))
						    continue;

					    routeCodeOfSelectedMatrix = MTX_RDEL.Columns.Item(RouteReadyMatrixColumns.Code).Cells.Item(i).Specific.Value;

					    // Validate that a driver was set.
					    if (string.IsNullOrEmpty(MTX_RDEL.Columns.Item(RouteReadyMatrixColumns.Driver).Cells.Item(i).Specific.Value))
					    {
						    Globals.oApp.StatusBar.SetText("Drlivery route must have a driver set!", BoMessageTime.bmt_Short);

						    return;
					    }

					    // Validate that a vehicle was set.
					    if (string.IsNullOrEmpty(MTX_RDEL.Columns.Item(RouteReadyMatrixColumns.Vehicle).Cells.Item(i).Specific.Value))
					    {
						    Globals.oApp.StatusBar.SetText("Delivery route must have a Vehicle set!", BoMessageTime.bmt_Short);

						    return;
					    }

					    // Set the Route Code of the selected row.
					    routeCodeOfSelectedMatrix = MTX_RDEL.Columns.Item(RouteReadyMatrixColumns.Code).Cells.Item(i).Specific.Value;

					    break;
				    }
				    catch (Exception ex)
				    {
					    // Send a message to the client
					    Globals.oApp.StatusBar.SetText("Failed to set route as out for deliveries!", BoMessageTime.bmt_Short);
					    return;
				    }
			    }

				if (routeCodeOfSelectedMatrix == "")
				{
					Globals.oApp.StatusBar.SetText("A Route must be selected.", BoMessageTime.bmt_Short);
					return;
				}

			    NSC_DI.SAP.DispatchingManagement.MarkRouteDeliveriesAsOut(routeCodeOfSelectedMatrix);

			    // Send a message to the client
			    Globals.oApp.StatusBar.SetText("Successfully set route status as out for deliveries!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

			    // Reload the Matrix.
			    Load_RoutesReadyForDelivery_Matrix();
			    Load_RoutesOutForDelivery_Matrix();
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    //NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

	    private void MarkDeliveryAsComplete()
	    {
		    try
		    {
			    // Grab the Matrix from the form 
			    SAPbouiCOM.Matrix MTX_DOUT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_DOUT", _SBO_Form);

			    // Get the selected Matrix Column so we know what delivery we are Modifying.
			    var routeCodeOfSelectedMatrix = "";
			    for (var i = 1; i < (MTX_DOUT.RowCount + 1); i++)
			    {
				    try
				    {
					    // If the current row is now selected we can just continue looping.
					    if (!MTX_DOUT.IsRowSelected(i))
						    continue;

					    routeCodeOfSelectedMatrix = MTX_DOUT.Columns.Item(OutRouteMatrixColumns.Code).Cells.Item(i).Specific.Value;
					    break;
				    }
				    catch (Exception ex)
				    {
					    // Send a message to the client
					    Globals.oApp.StatusBar.SetText("Failed to set route as checked in and complete!", BoMessageTime.bmt_Short);

					    return;
				    }
			    }

			    // Validate that the user made a selection
			    if (string.IsNullOrEmpty(routeCodeOfSelectedMatrix))
			    {
				    // Send a message to the client
				    Globals.oApp.StatusBar.SetText("Please select a row before attempting!", BoMessageTime.bmt_Short);

				    return;
			    }

			    NSC_DI.SAP.DispatchingManagement.MarkRouteDeliveriesAsIn(routeCodeOfSelectedMatrix);
				    // Send a message to the client
				    Globals.oApp.StatusBar.SetText("Successfully set route as checked in and complete!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

				    // Reload the Matrix.
				    Load_RoutesOutForDelivery_Matrix();
				    Load_RouteHistory_Matrix();

		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
				ReloadMatrices();
				GC.Collect();
		    }
	    }

	    private void ReloadMatrices()
        {
            try
            {
                // Freeze the form UI
                _SBO_Form.Freeze(true);

                // Load all the Matrices of the form.
                Load_Deliveries_WaitingRoute_Matrix();

                Load_RoutesWaitingTimer_Matrix();
                Load_RoutesReadyForDelivery_Matrix();
                Load_RoutesOutForDelivery_Matrix();

                Load_RouteHistory_Matrix();
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            { 
                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);
            }
        }

        //----------------------------------------------  Matrix Click Events
        private void RouteReady_MTX_Pressed(ItemEvent ItemEvent)
        {
            SAPbouiCOM.Matrix MTX_RDEL = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_RDEL", _SBO_Form);

            var selectedRowIndex = ItemEvent.Row;

            // Don't care if the selected is greater than the matrix rows.
            if (selectedRowIndex > MTX_RDEL.RowCount) return;

            try
            {
                string timeRemainingColumn = MTX_RDEL.Columns.Item(RouteReadyMatrixColumns.TimeRemaining).Cells.Item(selectedRowIndex).Specific.Value;

	            if (timeRemainingColumn.Equals("Ready")) return;

				MTX_RDEL.SelectRow(selectedRowIndex, false, false);

	            // Send a message to the client
	            Globals.oApp.StatusBar.SetText("Selected route must wait " + GetRouteTimeSpan().TotalHours + " hours before being sent out. See options to change this", BoMessageTime.bmt_Short);
            }
            catch (Exception ex)
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Error occurred while trying to access the matrix! Please reload the form.", BoMessageTime.bmt_Short);
            }
        }

	    private void MTX_LINK_PRESS(Form pForm, ItemEvent ItemEvent)
	    {
		    if (ItemEvent.Row < 1) return;
		    try
		    {
			    var selectedRowIndex = ItemEvent.Row;

			    // Grab the matrix from the Form UI
				var mxActive = (SAPbouiCOM.Matrix)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, ItemEvent.ItemUID, pForm);

			    // Get the ID of the selected
			    var routeId = ((EditText) mxActive.Columns.Item(0).Cells.Item(selectedRowIndex).Specific).Value.ToString();

			    var RouteDetailsForm = new F_RouteDetails(Globals.oApp, Globals.oCompany) as F_RouteDetails;
			    RouteDetailsForm.Form_Load_Find(routeId);
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
			    GC.Collect();
		    }
	    }
    }
}