﻿using System;
using System.Collections.Generic;
using System.Text;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using Matrix = NavSol.VERSCI.Matrix;

namespace NavSol.Forms.Sales_and_Deliveries
{
    class F_SalesOrder : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_SALES_ORDER";

		public string NewlyCreatedSalesOrderID { get; set; }
		public string NewlyCreatedDeliveryID { get; set; }

        private enum MatrixColumn { LineNo, ItemNo, ItemName, Quantity, OnHand, UnitPrice, Discount, Tax, TaxPercentage, LineTotal, WarehouseCode, StateId, SerialOrBatch, UniqueId }

        private bool _updateTotalOnLostFocus = false;
        private EditText _lastFocusedEditText;

        private LastFocusedItem _focusedItem;
        private enum LastFocusedItem { Quantity, UnitPrice, Discount, Tax, None }

        public enum SalesItemType { Batch, Serial, Normal }

        private enum ItemChooseFromListColumn { ItemNo, ItemName, UniqueId, OnHand, SerialOrBatch, WarehouseCode, StateId }
        private int _selectedRow = 0;
		
		public class InvLotCreate
		{
			public string BarcodeID;
			public double RemoveQuantity;
			public BioTrackAPIUnitOfMeasurement RemoveQuantityUOM;
		}
		public enum BioTrackAPIUnitOfMeasurement { g, mg, kg, oz, lb, each }

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public Form _SBO_Form;
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
	    [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
	    public virtual bool OnBeforeItemPressed(ItemEvent pVal)
	    {
		    var BubbleEvent = true;
		    var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


		    NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();

		    return BubbleEvent;
	    }
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
                            case "BTN_CRTE":
                                BTN_CRTE_Click();
                                break;

                            case "BTN_REMOV":
                                BTN_REMOV_Click();
                                break;

                            case "btnSALL":
                                HandleItemSelectionCFL();
                                break;

                            case "btnSBtch":
                                HandleItemSelectionCFL(true, false);
                                break;

                            case "btnSSer":
                                HandleItemSelectionCFL(false, true);
                                break;

                            // Apply discount to selected
                            case "btnADS":
                                ApplyDiscountToSelected();
                                break;

                            // Apply discount to all
                            case "btnADA":
                                ApplyDiscountToAll();
                                break;

                            // Apply tax to selected
                            case "btnATS":
                                ApplyTaxToSelected();
                                break;

                            // Apply tax to all
                            case "btnATA":
                                ApplyTaxToAll();
                                break;

                            case "btnCalc":
                                CalculateAllRows();
                                break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_KEY_DOWN
		[B1Listener(BoEventTypes.et_KEY_DOWN, false, new string[] { cFormID })]
		public virtual void OnAfterKeyDown(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "MTX_MAIN":
					// Kill this off for now.. Maybe we could add this back in with a delay or something.. But for now they can click a button.
					//try
					//{
					//    CalculateRowTotal(pVal.Row);
					//}
					//catch (Exception ex)
					//{
					//    // We don't have focus on the form.. Ignore this.
					//    log.Error("[Sales Order Form]: Keydown Event does not have focus and trying to access form controls.");
					//}
					break;

				case "TXT_DISCP":
					CalculateOrderTotals();
					break;

				case "TXT_BPNAME":
					if (pVal.CharPressed == 9) HandleBusinessPartnerSelectionCFL();
					break;

				case "txtTaxC":
					if (pVal.CharPressed == 9) HandleTaxSelectionCFL();
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_FORM_RESIZE
		[B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
		public virtual void OnAfterFormResize(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			ReSize();

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
	    #endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------
        #region Initial Form Load Methods

		public F_SalesOrder() : this(Globals.oApp, Globals.oCompany) { }

		public F_SalesOrder(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
		{
			_SBO_Application = SAPBusinessOne_Application;
			_SBO_Company = SAPBusinessOne_Company;
			_VirSci_Helper_Form = new VERSCI.Forms();
		}

        public void Form_Load()
        {
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

				// Load the Matrix of Deliveries
				Load_Matrix();

				_SBO_Form.Items.Item("TXT_TOTAL").Enabled = false;
				_SBO_Form.Items.Item("TXT_TAX").Enabled = false;
				_SBO_Form.Items.Item("TXT_BPCODE").Enabled = false;

				_SBO_Form.Items.Item("TXT_TAX").Specific.Value = "$ 0.00";
				_SBO_Form.Items.Item("TXT_TOTAL").Specific.Value = "$ 0.00";
			}
            catch (Exception ex)
            {
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
				_SBO_Form.Freeze(false);
				_SBO_Form.VisibleEx = true;

				//NSC_DI.UTIL.Misc.KillObject(mtx_mselc);
                GC.Collect();
            }
        }

		public void Load_Matrix()
        {
            string sqlQuery = string.Format(@"
SELECT
'999' AS                    [LineNo]
,'ABCDEFGHIJKLMNOPQRSTUVABCDEFGHIJK' AS [ItemNo]
,'ABCDEFGHIJKLMNOPQRSTUVABCDEFGHIJKLMNOPQRSTUVABCDEFGHIJKLMNOPQRSTUVABCDEFGHIJKLMNOPQRSTUV' AS [ItemName]
, '9999.99' AS              [Quantity]
, '9999.99' AS              [OnHand]
, '9999999.99' AS           [UnitPrice]
, '999999.99' AS            [Discount]
, 'ABCDEFGHIJKLMNOP' AS     [Tax]
, 'ABCDEFGHIJKLMNOP' AS     [TaxPercentage]
, 'ABCDEFGHIJKLMNOP' AS     [LineTotal]
, 'ABCDE' AS                [WarehouseCode]
, '980000020000002012355' AS [StateId]
, 'AB' AS [SerialOrBatch]
, 'ABCDEFGHIJKLMNOPQRSTUV12345' AS [UniqueId]
");

            // Prepare a Matrix helper
			Matrix MatrixHelper = new Matrix(Globals.oApp, _SBO_Form);

            // Populate the Matrix with our Query.
            MatrixHelper.LoadDatabaseDataIntoMatrix("Matrix", "MTX_MAIN", new List<Matrix.MatrixColumn>()
                {    new Matrix.MatrixColumn(){ Caption="Select", ColumnWidth=80, ColumnName="LineNo", ItemType = BoFormItemTypes.it_EDIT}       
                    ,new Matrix.MatrixColumn(){ Caption="Item No.", ColumnWidth=80, ColumnName="ItemNo", ItemType = BoFormItemTypes.it_EDIT}               
                    ,new Matrix.MatrixColumn(){ Caption="Item Name", ColumnWidth=80, ColumnName="ItemName", ItemType = BoFormItemTypes.it_EDIT}               
                    ,new Matrix.MatrixColumn(){ Caption="Quantity", ColumnWidth=40, ColumnName="Quantity", ItemType= BoFormItemTypes.it_EDIT, IsEditable = true}
                    ,new Matrix.MatrixColumn(){ Caption="On Hand", ColumnWidth=40, ColumnName="OnHand", ItemType= BoFormItemTypes.it_EDIT}
                    ,new Matrix.MatrixColumn(){ Caption="Unit Price", ColumnWidth=80, ColumnName="UnitPrice", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}
                    ,new Matrix.MatrixColumn(){ Caption="Discount %", ColumnWidth=80, ColumnName="Discount", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}
                    ,new Matrix.MatrixColumn(){ Caption="Tax Code", ColumnWidth=80, ColumnName="Tax", ItemType = BoFormItemTypes.it_EDIT}
                    ,new Matrix.MatrixColumn(){ Caption="Tax %", ColumnWidth=80, ColumnName="TaxPercentage", ItemType = BoFormItemTypes.it_EDIT}
                    ,new Matrix.MatrixColumn(){ Caption="Total(LC)", ColumnWidth=80, ColumnName="LineTotal", ItemType = BoFormItemTypes.it_EDIT}
                    ,new Matrix.MatrixColumn(){ Caption="Warehouse", ColumnWidth=80, ColumnName="WarehouseCode", ItemType = BoFormItemTypes.it_EDIT}
                    ,new Matrix.MatrixColumn(){ Caption="StateId", ColumnWidth=80, ColumnName="StateId", ItemType = BoFormItemTypes.it_EDIT}
                    ,new Matrix.MatrixColumn(){ Caption="Serial/Batch", ColumnWidth=80, ColumnName="SerialOrBatch", ItemType = BoFormItemTypes.it_EDIT}
                    ,new Matrix.MatrixColumn(){ Caption="Serial/Batch No.", ColumnWidth=80, ColumnName="UniqueId", ItemType = BoFormItemTypes.it_EDIT}
                }, sqlQuery);

            DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("Matrix");
            DataTable.Rows.Remove(0);

            // Grab the Matrix MTX_MAIN the form 
			var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;

            MTX_MAIN.LoadFromDataSource();
            MTX_MAIN.SelectionMode = BoMatrixSelect.ms_Auto;
            MTX_MAIN.AutoResizeColumns();
        }
		#endregion Initial Form Load Methods

		#region Calculation Methods
		private void CalculateRowTotal(int index)
        {
            // Get the Matrix so we can see what columns are active.
			var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;
			EditText quantityRow = MTX_MAIN.Columns.Item(MatrixColumn.Quantity).Cells.Item(index).Specific as EditText;
            EditText unitPriceRow = MTX_MAIN.Columns.Item(MatrixColumn.UnitPrice).Cells.Item(index).Specific as EditText;

            // We need quantity and unit price to calculate a Row Line total.
            if (!string.IsNullOrEmpty(quantityRow.Value) && !string.IsNullOrEmpty(unitPriceRow.Value))
            {
                try
                {
                    EditText setAsActive;

                    EditText onHandRowItem = MTX_MAIN.Columns.Item(MatrixColumn.OnHand).Cells.Item(index).Specific as EditText;
                    EditText discountRowItem = MTX_MAIN.Columns.Item(MatrixColumn.Discount).Cells.Item(index).Specific as EditText;
                    EditText taxCodeRowItem = MTX_MAIN.Columns.Item(MatrixColumn.Tax).Cells.Item(index).Specific as EditText;
                    EditText taxPercentageRowItem = MTX_MAIN.Columns.Item(MatrixColumn.TaxPercentage).Cells.Item(index).Specific as EditText;
                    EditText totalRowItem = MTX_MAIN.Columns.Item(MatrixColumn.LineTotal).Cells.Item(index).Specific as EditText;

                    #region Parse values and validate that they are of the right types

                    // Validate that we have a numeric value for Quantity.
                    double quantity = 0;
                    if (!double.TryParse(quantityRow.Value, out quantity))
                    {
                        Globals.oApp.StatusBar.SetText("Please validate that quantity is a numeric value and try again.", BoMessageTime.bmt_Short
                           );

                        quantityRow.Active = true;
                        return;
                    }

                    double onHand = 0;
                    if (!string.IsNullOrEmpty(onHandRowItem.Value))
                    {
                        onHand = Convert.ToDouble(onHandRowItem.Value);
                    }

                    // Validate that the entered quantity not exceeding the on hand.
                    if (quantity > onHand)
                    {
                        Globals.oApp.StatusBar.SetText("Quantity must be less than or equal to the onhand value.", BoMessageTime.bmt_Short
                           );

                        quantityRow.Active = true;
                        return;
                    }

                    // Validate that we have a numeric value for Unit Price.
                    double unitPrice = 0;
                    if (!double.TryParse(unitPriceRow.Value, out unitPrice))
                    {
                        Globals.oApp.StatusBar.SetText("Please validate that unit price is a numeric value and try again.", BoMessageTime.bmt_Short
                           );

                        unitPriceRow.Active = true;
                        return;
                    }

                    #endregion Parse values and validate that they are of the right types

                    // Calculate the line total.
                    double totalRowCost = quantity * unitPrice;
                    double discount = 0;

                    // If the discount has a value we can add it in as well.
                    if (!string.IsNullOrEmpty(discountRowItem.Value))
                    {
                        // Validate that we correctly got a number.
                        if (!double.TryParse(discountRowItem.Value, out discount))
                        {
                            Globals.oApp.StatusBar.SetText("Please validate that discount is a numeric value and try again.", BoMessageTime.bmt_Short
                            );

                            discountRowItem.Active = true;
                            return;
                        }

                        if (discount > 0)
                        {
                            totalRowCost -= (totalRowCost * (discount / 100));
                        }

                        // Validate that we are not in the negative.
                        if (totalRowCost < 0)
                        {
                            Globals.oApp.StatusBar.SetText("Line total falls into negative cost. Sales line must be positive.", BoMessageTime.bmt_Short
                            );

                            discountRowItem.Active = true;
                            return;
                        }
                    }

                    double taxRate = 0;
                    // If the tax has a value we can add it in as well.
                    if (!string.IsNullOrEmpty(taxCodeRowItem.Value))
                    {
                        // Validate that we correctly got a number.
                        if (!double.TryParse(taxPercentageRowItem.Value, out taxRate))
                        {
                            Globals.oApp.StatusBar.SetText("Failed to parse the tax rate from column.", BoMessageTime.bmt_Short
                            );

                            taxCodeRowItem.Active = true;
                            return;
                        }

                        if (taxRate > 0)
                        {
                            totalRowCost += (totalRowCost * (taxRate / 100));
                        }
                    }

                    // Update the Marix with the new Line total.
                    // * ToString("0.00") just means to always add in the zeros. ToString("#.##") will format to that amount of zeros.
                    DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("Matrix");
                    DataTable.SetValue(MatrixColumn.LineTotal.ToString("F"), (index - 1), totalRowCost.ToString("0.00"));
                    DataTable.SetValue(MatrixColumn.Quantity.ToString("F"), (index - 1), quantity.ToString("#.##"));
                    DataTable.SetValue(MatrixColumn.UnitPrice.ToString("F"), (index - 1), unitPrice.ToString("0.00"));
                    DataTable.SetValue(MatrixColumn.Discount.ToString("F"), (index - 1), discount.ToString("0.000"));
                    DataTable.SetValue(MatrixColumn.Tax.ToString("F"), (index - 1), taxCodeRowItem.Value);

                    totalRowItem.Value = "$ " + totalRowCost.ToString("0.00");

                    // Calculate the total.
                    CalculateOrderTotals();

                    unitPriceRow.Value = unitPrice.ToString("0.00");
                    discountRowItem.Value = discount.ToString("0.000");

                    // Going to ignore loading from the datatable since it causes a refresh and focus is lost.
                }
                catch (Exception ex)
                {
					throw new Exception(NSC_DI.UTIL.Message.Format(ex));
				}
            }
        }

        private void CalculateOrderTotals()
        {
            // Grab the Matrix MTX_MAIN the form 
			var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;

            // Get all the controls from the form. We need most of them.
			SalesOrderFormControls formControls = new SalesOrderFormControls(_VirSci_Helper_Form, _SBO_Form);

            // Calulate how much discount we need to add ontop of the items.
            double discountPercentage = 0;
            if (!string.IsNullOrEmpty(formControls.txtDiscountPercentage.Value))
            {
                double.TryParse(formControls.txtDiscountPercentage.Value, out discountPercentage);
            }

            if (discountPercentage > 100)
            {
                discountPercentage = 100;
                formControls.txtDiscountPercentage.Value = discountPercentage.ToString(); ;
            }
            else if (discountPercentage < 0)
            {
                discountPercentage = 0;
                formControls.txtDiscountPercentage.Value = discountPercentage.ToString(); ;
            }

            discountPercentage /= 100; // Make it an actual percentage.

            double grandTotal = 0;
            double totalTax = 0;
            double totalWithoutTaxWithDiscount = 0;

            for (int i = 1; i < MTX_MAIN.RowCount + 1; i++)
            {
                EditText quantityRowItem = MTX_MAIN.Columns.Item(MatrixColumn.Quantity).Cells.Item(i).Specific as EditText;
                EditText unitPriceRowItem = MTX_MAIN.Columns.Item(MatrixColumn.UnitPrice).Cells.Item(i).Specific as EditText;
                EditText taxPercentageRowItem = MTX_MAIN.Columns.Item(MatrixColumn.TaxPercentage).Cells.Item(i).Specific as EditText;
                EditText discountPercentageRowItem = MTX_MAIN.Columns.Item(MatrixColumn.Discount).Cells.Item(i).Specific as EditText;

                double discountOnRow = 0;
                if (!string.IsNullOrEmpty(discountPercentageRowItem.Value))
                {
                    double.TryParse(discountPercentageRowItem.Value, out discountOnRow);
                }

                try
                {
                    double taxTotalFromRow = 0;

                    // Inorder to get how much each line of tax was we need to calculate the line total without discount.
                    double quantity = Convert.ToDouble(quantityRowItem.Value);
                    double unitPrice = Convert.ToDouble(unitPriceRowItem.Value);
                    double lineTotal = quantity * unitPrice;

                    double taxPercentage = 0;
                    if (!string.IsNullOrEmpty(taxPercentageRowItem.Value))
                    {
                        double.TryParse(taxPercentageRowItem.Value, out taxPercentage);
                    }

                    double rawTotal = (quantity * unitPrice);
                    double rawTotalWithLineDiscount = rawTotal - (rawTotal * (discountOnRow / 100));
                    totalWithoutTaxWithDiscount += rawTotalWithLineDiscount;

                    double totalWithOrderDiscount = rawTotalWithLineDiscount - (rawTotalWithLineDiscount * discountPercentage);

                    if (taxPercentage > 0)
                    {
                        taxTotalFromRow = totalWithOrderDiscount * (taxPercentage / 100);
                    }

                    totalTax += taxTotalFromRow;
                    grandTotal += totalWithOrderDiscount + taxTotalFromRow;
                }
                catch (Exception ex)
                {
					throw new Exception(NSC_DI.UTIL.Message.Format(ex));
				}
            }

            formControls.txtTotalBeforeDiscount.Value = "$ " + totalWithoutTaxWithDiscount.ToString("0.00");
            formControls.txtTotalBeforeDiscount.Active = false;
            formControls.txtTotalBeforeDiscount.Item.Enabled = false;


            double discountAmount = totalWithoutTaxWithDiscount * discountPercentage;

            formControls.txtDiscount.Value = "$ " + discountAmount.ToString("0.00");
            formControls.txtDiscount.Active = false;
            formControls.txtDiscount.Item.Enabled = false;

            // Update the total tax for the Sales Order.
            formControls.txtTax.Value = "$ " + totalTax.ToString("0.00");
            formControls.txtTax.Active = false;
            formControls.txtTax.Item.Enabled = false;

            // Update the Total for some reason updating it makes it enabled again.
            formControls.txtTotal.Value = "$ " + grandTotal.ToString("0.00");
            formControls.txtTotal.Active = false;
            formControls.txtTotal.Item.Enabled = false;
        }
        #endregion

        #region Choose From List Methods
        /// <summary>
        /// Handles the creation of the where clause so the Item Selection Choose from List
        /// will not contain the items that have already been selected and exist on the matrix.
        /// </summary>
        /// <param name="MTX_MAIN">Reference to the Main Matrix</param>
        /// <returns></returns>
		private WhereClauseHelper CreateItemSelectionWhereClauseExclusion(SAPbouiCOM.Matrix MTX_MAIN)
        {
            StringBuilder itemSelectionWhereClause = new StringBuilder();

            // Setting up the clauses based on the query from ItemSelectionCFL.
            WhereClauseHelper clauseCreationHelper = new WhereClauseHelper();

            // Loop through the Matrix Rows.
            for (int i = 1; i < MTX_MAIN.RowCount + 1; i++)
            {
                EditText uniqueIdRowItem = MTX_MAIN.Columns.Item(MatrixColumn.UniqueId).Cells.Item(i).Specific as EditText;
                EditText batchOrSerialRowItem = MTX_MAIN.Columns.Item(MatrixColumn.SerialOrBatch).Cells.Item(i).Specific as EditText;

                try
                {
                    switch (uniqueIdRowItem.Value)
                    {
                        case "B":
                            clauseCreationHelper.BatchNumbersToExclude.Add(batchOrSerialRowItem.Value);
                            break;

                        case "S":
                            // We need to Split it to 
                            clauseCreationHelper.SerialNumbersToExclude.Add(batchOrSerialRowItem.Value);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    // Nothing we can do but ignore it.
					//log.Error("[Sales Order Form]: Random null in item selection or tax selection.", ex);
                    continue;
                }
            }
            clauseCreationHelper.CreateWhereClauses();
            return clauseCreationHelper;
        }

        private void HandleItemSelectionCFL(bool isBatch = true, bool isSerial = true)
        {
            // Find the matrix in the form
			var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;
            

           var title = "Item Choose From List";
            
			var matrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
            {
                 new CommonUI.Matrix.MatrixColumn(){ Caption="Item No.", ColumnWidth=40, ColumnName="ItemNo", ItemType= BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Item Name", ColumnWidth=80, ColumnName="ItemName", ItemType = BoFormItemTypes.it_EDIT}    
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="List Price", ColumnWidth=80, ColumnName="UnitPrice", ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Batch/Serial No.", ColumnWidth=80, ColumnName="UniqueId", ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="OnHand", ColumnWidth=80, ColumnName="OnHand", ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="SerialOrBatch", ColumnWidth=80, ColumnName="SerialOrBatch", ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="WarehouseCode", ColumnWidth=80, ColumnName="WarehouseCode", ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="StateId", ColumnWidth=80, ColumnName="StateId", ItemType = BoFormItemTypes.it_EDIT}
            };

            int cflWidth = Math.Max(450, matrixColumns.Count * 150);
            int cflHeight = 420;

            // Max Width is 80% of the users screen space.
            var width = Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
            var height = Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);

            var top = (Globals.oApp.Desktop.Height - height) / 2;
            var left = (Globals.oApp.Desktop.Width - width) / 2;

            WhereClauseHelper clauseHelper = CreateItemSelectionWhereClauseExclusion(MTX_MAIN);
            var sql = String.Empty;
            if (isBatch)
            {
                // And lastly the Query to populate the Matrix.
                sql = string.Format(@"
SELECT [OITM].[ItemCode] AS	[ItemNo], [OITM].[ItemName] AS [ItemName] 
,(SELECT [ITM1].[Price] FROM [ITM1] WHERE [ITM1].[PriceList] = '1' AND [ITM1].[Price] > 0 AND [ITM1].[ItemCode] = [OITM].[ItemCode]) AS [UnitPrice]
,[OBTN].[DistNumber] AS	[UniqueID]
,(SELECT [OIBT].[Quantity] FROM [OIBT] WHERE [OBTN].[DistNumber] = [OIBT].[BatchNum] AND [OIBT].[Quantity] > 0) AS [OnHand]
, 'B' AS [SerialOrBatch]
, (SELECT [OIBT].[WhsCode] FROM [OIBT] WHERE [OBTN].[DistNumber] = [OIBT].[BatchNum] AND [OIBT].[Quantity] > 0) AS [WarehouseCode]
, [OBTN].[U_NSC_StateID] AS	[StateId]
FROM [OBTN] 
JOIN [OITM] ON [OITM].[ItemCode] = [OBTN].[ItemCode]
WHERE (SELECT [OIBT].[Quantity] FROM [OIBT] WHERE [OBTN].[DistNumber] = [OIBT].[BatchNum] AND [OIBT].[Quantity] > 0) > 0  -- Always contains a where clause
AND ISNULL([OBTN].[U_NSC_StateID], '') != '' 
AND [OITM].[SellItem] = 'Y' -- Item Must be sellable.");
            }

            if (isBatch && isSerial)
            {
                sql += string.Format(@" UNION ALL ");
            }

            if (isSerial)
            {
                sql += string.Format(@"

SELECT OSRN.ItemCode AS  [ItemNo], [OSRN].[itemName] AS [ItemName]
, (SELECT [ITM1].[Price] FROM [ITM1] WHERE [ITM1].[PriceList] = '1' AND [ITM1].[Price] > 0 AND [ITM1].[ItemCode] = [OSRN].[ItemCode]) AS [UnitPrice]
, CONVERT(nvarchar,[OSRN].[SysNumber]) AS [UniqueID]
, '1' AS [OnHand] -- Quantity is always one for Serial Items.
, 'S' AS [SerialOrBatch]
, (SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber])  AS [WarehouseCode]
, [OSRN].[U_NSC_StateID] AS [StateId]
FROM [OSRN] 
JOIN [OITM] ON [OITM].[ItemCode] = [OSRN].[ItemCode]
WHERE [OSRN].[U_NSC_StateID] IS NOT NULL
AND [OSRN].[U_NSC_StateID] <> ''
AND [OITM].[SellItem] = 'Y' -- Item Must be sellable.
AND (SELECT [OSRI].[Status] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber]) = 0
ORDER BY [ItemName], [OnHand]");

            }

            F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pResults)
            {
                _SBO_Form = B1Connections.theAppl.Forms.Item(pCallingFormUid);


                ItemChooseFromListCallBack(pResults);
            };
            Form oForm_Cfl = F_CFL.FormCreate(title, _SBO_Form.UniqueID, sql, matrixColumns, true, null, top, left, height, width);

        }

        public void ItemChooseFromListCallBack(System.Data.DataTable pResults)
        {
            try
            {
                // Find the matrix in the form
				var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;
				DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("Matrix");
                int newRowIndex = MTX_MAIN.RowCount;
                foreach (System.Data.DataRow results in pResults.Rows)
                {
                    // Round the decimal places to two
                    double truncatedDouble = Math.Truncate(Convert.ToDouble(results["OnHand"]) * 100) / 100;

                    // Update the Marix with the Itemcode
                    DataTable.Rows.Add();
                    DataTable.SetValue(MatrixColumn.ItemNo.ToString("F"), newRowIndex, results["ItemNo"]);
                    DataTable.SetValue(MatrixColumn.ItemName.ToString("F"), newRowIndex, results["ItemName"]);
                    DataTable.SetValue(MatrixColumn.UnitPrice.ToString("F"), newRowIndex, results["UnitPrice"]);
                    DataTable.SetValue(MatrixColumn.OnHand.ToString("F"), newRowIndex, truncatedDouble.ToString());
                    DataTable.SetValue(MatrixColumn.WarehouseCode.ToString("F"), newRowIndex, results["WarehouseCode"]);
                    DataTable.SetValue(MatrixColumn.StateId.ToString("F"), newRowIndex, results["StateId"]);
                    DataTable.SetValue(MatrixColumn.SerialOrBatch.ToString("F"), newRowIndex, results["SerialOrBatch"]);
                    DataTable.SetValue(MatrixColumn.UniqueId.ToString("F"), newRowIndex, results["UniqueId"]);
                    DataTable.SetValue(MatrixColumn.UniqueId.ToString("F"), newRowIndex, results["UniqueId"]);
                    DataTable.SetValue(MatrixColumn.UniqueId.ToString("F"), newRowIndex, results["UniqueId"]);

                    newRowIndex++;
                }

                // Add new row to matrix
                MTX_MAIN.LoadFromDataSource();

                // Select our current form.
                _SBO_Form.Select();
            }
            catch (Exception ex)
            {
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
        }

        private void HandleBusinessPartnerSelectionCFL()
        {
 			// Get the business parnter name from the form.
	        EditText TXT_BPNAME = _SBO_Form.Items.Item("TXT_BPNAME").Specific;

			// Only handle the tab event if the text field is empty.
	        if (string.IsNullOrEmpty(TXT_BPNAME.Value) == false) return;

	        TXT_BPNAME.Active = true;

            var title = "Business Partner Choose From List";

            var matrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
            {
                 new CommonUI.Matrix.MatrixColumn(){ Caption="Code", ColumnWidth=80, ColumnName="Code", ItemType = BoFormItemTypes.it_EDIT}               
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=40, ColumnName="Name", ItemType= BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Type", ColumnWidth=80, ColumnName="Type", ItemType = BoFormItemTypes.it_EDIT}
            };

            int cflWidth = Math.Max(450, matrixColumns.Count * 150);
            int cflHeight = 420;
            
            var width = Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
            var height = Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);

            var top = (Globals.oApp.Desktop.Height - height) / 2;
            var left = (Globals.oApp.Desktop.Width - width) / 2;

            var sql = @"
SELECT [OCRD].[CardCode] AS [Code], [OCRD].[CardName] AS [Name], CASE [OCRD].[CardType] 
  WHEN  'C' THEN 'Customer'
  WHEN  'S' THEN 'Vendor'
  WHEN  'L' THEN 'Lead'
  END AS [Type]
FROM [OCRD]
WHERE [OCRD].[CardType]  = 'C'";

            F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pResults)
            {
                _SBO_Form = B1Connections.theAppl.Forms.Item(pCallingFormUid);

                BusinessPartnerChooseFromListCallBack(pResults);
            };
            Form oForm_Cfl = F_CFL.FormCreate(title, _SBO_Form.UniqueID, sql, matrixColumns, false, null, top, left, height, width);
        }

        public void BusinessPartnerChooseFromListCallBack(System.Data.DataTable results)
        {
            try
            {
                _SBO_Form.Select();

                _SBO_Form.Items.Item("TXT_BPCODE").Specific.Value = results.Rows[0]["Code"] as string;
                _SBO_Form.Items.Item("TXT_BPNAME").Specific.Value = results.Rows[0]["Name"] as string;
            }
            catch (Exception ex)
            {
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
        }

        public void HandleTaxSelectionCFL()
        {

			EditText txtTaxC = _SBO_Form.Items.Item("txtTaxC").Specific;

			// Only handle the tab event if the text field is empty.
			if (string.IsNullOrEmpty(txtTaxC.Value) == false) return;
			txtTaxC.Active = true;

            var title = "Tax Code Choose From List";

            var matrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
            {
                 new CommonUI.Matrix.MatrixColumn(){ Caption="Code", ColumnWidth=40, ColumnName="Code", ItemType= BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=80, ColumnName="Name", ItemType = BoFormItemTypes.it_EDIT}               
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Rate", ColumnWidth=80, ColumnName="Rate", ItemType = BoFormItemTypes.it_EDIT}
            };

            int cflWidth = Math.Max(450, matrixColumns.Count * 150);
            int cflHeight = 420;

            // Max Width is 80% of the users screen space.
            var width = Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
            var height = Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);

            var top = (Globals.oApp.Desktop.Height - height) / 2;
            var left = (Globals.oApp.Desktop.Width - width) / 2;

            // And lastly the Query to populate the Matrix.
            var sql = string.Format("SELECT [OSTC].[Code], [OSTC].[Name], [OSTC].[Rate] FROM [OSTC]");

            // Get the Quantity Line.
            EditText taxCode = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txtTaxC", _SBO_Form);

            // Set Tax Code Line to active.
            taxCode.Active = true;

            // Select our current form.
            _SBO_Form.Select();

            F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pResults)
            {
                _SBO_Form = B1Connections.theAppl.Forms.Item(pCallingFormUid);


                TaxChooseFromListCallBack(pResults);
            };
            Form oForm_Cfl = F_CFL.FormCreate(title, _SBO_Form.UniqueID, sql, matrixColumns, false, null, top, left, height, width);
        }

        public void TaxChooseFromListCallBack(System.Data.DataTable results)
        {
            try
            {
                EditText taxCode = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txtTaxC", _SBO_Form);
                EditText taxRate = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txtTaxR", _SBO_Form);

                taxCode.Value = results.Rows[0]["Code"] as string;
                taxRate.Value = results.Rows[0]["Rate"] as string;
            }
            catch (Exception ex)
            {
                // What are you complaining about?
            }

            // Select our current form.
            _SBO_Form.Select();

        }
        #endregion

		private void ReSize()
		{
			try
			{
				var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;
				MTX_MAIN.AutoResizeColumns();
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				//NSC_DI.UTIL.Misc.KillObject(oBar);
				GC.Collect();
			}
		}

        public void SaveQuantitiesToDatable()
        {
            // Find the matrix in the form
			var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;

            DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("Matrix");

            _SBO_Form.Freeze(true);
            // Update all rows with new tax info.
            for (int i = 1; i < MTX_MAIN.RowCount + 1; i++)
            {
                try
                {
                    // Need to save all of these fields first because they could of been altered by the user manually.
                    string quantity = (MTX_MAIN.Columns.Item(MatrixColumn.Quantity).Cells.Item(i).Specific as EditText).Value;
                    string unitPrice = (MTX_MAIN.Columns.Item(MatrixColumn.UnitPrice).Cells.Item(i).Specific as EditText).Value;
                    string discount = (MTX_MAIN.Columns.Item(MatrixColumn.Discount).Cells.Item(i).Specific as EditText).Value;

                    DataTable.SetValue(MatrixColumn.Quantity.ToString("F"), (i - 1), quantity);
                    DataTable.SetValue(MatrixColumn.UnitPrice.ToString("F"), (i - 1), unitPrice);
                    DataTable.SetValue(MatrixColumn.Discount.ToString("F"), (i - 1), discount);
                }
                catch (Exception ex) { }
            }
            _SBO_Form.Freeze(false);
        }

        public void ApplyTaxToSelected()
        {
            // Find the matrix in the form
			var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;
            EditText txtTaxC = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txtTaxC", _SBO_Form);
            EditText txtTaxR = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txtTaxR", _SBO_Form);

            if (string.IsNullOrEmpty(txtTaxC.Value))
            {
                Globals.oApp.StatusBar.SetText("Please select a tax to apply first!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);

                txtTaxC.Value = "";
                txtTaxC.Active = true;
                return;
            }

            if (string.IsNullOrEmpty(txtTaxR.Value))
            {
                Globals.oApp.StatusBar.SetText("Please select a tax to apply first!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);

                txtTaxC.Value = "";
                txtTaxC.Active = true;
                return;
            }

            SaveQuantitiesToDatable();

            _SBO_Form.Freeze(true);

            DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("Matrix");

            // Update all rows with new tax info.
            for (int i = 1; i < MTX_MAIN.RowCount + 1; i++)
            {
                if (MTX_MAIN.IsRowSelected(i))
                {
                    DataTable.SetValue(MatrixColumn.Tax.ToString("F"), (i - 1), txtTaxC.Value);
                    DataTable.SetValue(MatrixColumn.TaxPercentage.ToString("F"), (i - 1), txtTaxR.Value);

                    // CalculateRowTotal(i);
                }
            }

            MTX_MAIN.LoadFromDataSource();
            _SBO_Form.Freeze(false);
            txtTaxC.Value = "";
            txtTaxR.Value = "";
        }

        public void ApplyTaxToAll()
        {
            // Find the matrix in the form
			var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;
            EditText txtTaxC = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txtTaxC", _SBO_Form);
            EditText txtTaxR = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txtTaxR", _SBO_Form);

            if (string.IsNullOrEmpty(txtTaxC.Value))
            {
                Globals.oApp.StatusBar.SetText("Please select a tax to apply first!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                txtTaxC.Value = "";
                txtTaxC.Active = true;
                return;
            }

            if (string.IsNullOrEmpty(txtTaxR.Value))
            {
                Globals.oApp.StatusBar.SetText("Please select a tax to apply first!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                txtTaxC.Value = "";
                txtTaxC.Active = true;
                return;
            }

            SaveQuantitiesToDatable();

            _SBO_Form.Freeze(true);

            DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("Matrix");

            // Update all rows with new tax info.
            for (int i = 0; i < DataTable.Rows.Count; i++)
            {
                DataTable.SetValue(MatrixColumn.Tax.ToString("F"), i, txtTaxC.Value);
                DataTable.SetValue(MatrixColumn.TaxPercentage.ToString("F"), i, txtTaxR.Value);
            }

            MTX_MAIN.LoadFromDataSource();

            _SBO_Form.Freeze(false);
            txtTaxC.Value = "";
            txtTaxR.Value = "";
        }

        public void ApplyDiscountToSelected()
        {
            // Find the matrix in the form
			var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;
            EditText txtDiscnt = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txtDiscnt", _SBO_Form);

            if (string.IsNullOrEmpty(txtDiscnt.Value))
            {
                Globals.oApp.StatusBar.SetText("Please set a discount to apply first!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                txtDiscnt.Value = "";
                txtDiscnt.Active = true;
                return;
            }

            double discountToApply = -1;
            double.TryParse(txtDiscnt.Value, out discountToApply);
            if (discountToApply < 0)
            {
                Globals.oApp.StatusBar.SetText("Discount must be numeric and greater than or equal to 0", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                txtDiscnt.Value = "";
                txtDiscnt.Active = true;
                return;
            }

            SaveQuantitiesToDatable();

            _SBO_Form.Freeze(true);
            DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("Matrix");

            // Update all selected rows with new discount amount.
            for (int i = 1; i < MTX_MAIN.RowCount + 1; i++)
            {
                if (MTX_MAIN.IsRowSelected(i))
                {
                    DataTable.SetValue(MatrixColumn.Discount.ToString("F"), i - 1, discountToApply.ToString());
                }
            }

            MTX_MAIN.LoadFromDataSource();
            _SBO_Form.Freeze(false);
            txtDiscnt.Value = "";
        }

        public void ApplyDiscountToAll()
        {
            // Find the matrix in the form
			var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;
            EditText txtDiscnt = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txtDiscnt", _SBO_Form);

            if (string.IsNullOrEmpty(txtDiscnt.Value))
            {
                Globals.oApp.StatusBar.SetText("Please set a discount to apply first!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                txtDiscnt.Value = "";
                txtDiscnt.Active = true;
                return;
            }

            double discountToApply = -1;
            double.TryParse(txtDiscnt.Value, out discountToApply);
            if (discountToApply < 0)
            {
                Globals.oApp.StatusBar.SetText("Discount must be numeric and greater than or equal to 0", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                txtDiscnt.Value = "";
                txtDiscnt.Active = true;
                return;
            }

            SaveQuantitiesToDatable();

            _SBO_Form.Freeze(true);
            DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("Matrix");

            // Update all rows with new tax info.
            for (int i = 0; i < DataTable.Rows.Count; i++)
            {
                DataTable.SetValue(MatrixColumn.Discount.ToString("F"), i, discountToApply.ToString());
            }

            MTX_MAIN.LoadFromDataSource();
            _SBO_Form.Freeze(false);
            txtDiscnt.Value = "";
        }

        public void CalculateAllRows()
        {
            // Find the matrix in the form
			var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;

	        ProgressBar _progressBar = Globals.oApp.StatusBar.CreateProgressBar("Calculating Lines ...", MTX_MAIN.RowCount, false);
	        _progressBar.Value = 1;
            _SBO_Form.Freeze(true);

            // Update all rows with new tax info.
            for (int i = 1; i < MTX_MAIN.RowCount + 1; i++)
            {
                CalculateRowTotal(i);
	            _progressBar.Value = i;
            }

            _progressBar.Stop();

            _SBO_Form.Freeze(false);
        }

        #region Document Creation
        private bool CreateSalesDocument(SalesOrderDocument salesDocumentData)
        {
            bool createdSuccessfully = true;

            Documents salesOrderDocument = Globals.oCompany.GetBusinessObject(BoObjectTypes.oOrders);

            // Assign all needed variables for a sales order.
            salesOrderDocument.DocDate			= DateTime.Now;
            salesOrderDocument.DocDueDate		= salesDocumentData.DeliveryDate;

            salesOrderDocument.TaxDate			= DateTime.Now;
            salesOrderDocument.OpeningRemarks	= salesDocumentData.Comment;

            salesOrderDocument.DocTotal			= salesDocumentData.Total;
            salesOrderDocument.CardCode			= salesDocumentData.BusinessPartner;

            salesOrderDocument.DiscountPercent	= salesDocumentData.DiscountPercentage;

            foreach (SalesOrderLineItem line in salesDocumentData.LineItem)
            {
                salesOrderDocument.Lines.ItemCode		= line.ItemCode;
                salesOrderDocument.Lines.WarehouseCode	= line.SourceWarehouseCode;
                salesOrderDocument.Lines.Quantity		= line.Quantity;
                salesOrderDocument.Lines.UnitPrice		= line.UnitPrice;
                salesOrderDocument.Lines.DiscountPercent = line.DiscountPercentage;
                salesOrderDocument.Lines.TaxCode		= line.TaxCode;
                salesOrderDocument.Lines.Price			= line.UnitPrice;
                salesOrderDocument.Lines.Add();
            }

            int ErrorCodeFromSAP = salesOrderDocument.Add();

            if (ErrorCodeFromSAP != 0)
            {
                createdSuccessfully = false;
                string ErrorMessageFromSAP = Globals.oCompany.GetLastErrorDescription();
				Globals.oApp.StatusBar.SetText(ErrorMessageFromSAP);
				throw new System.ComponentModel.WarningException();
			}

            string newCode = null;

            // Get the newly created object's code
            Globals.oCompany.GetNewObjectCode(out newCode);

            if (newCode != null)
            {
                NewlyCreatedSalesOrderID = Convert.ToInt32(newCode).ToString();
            }

            return createdSuccessfully;
        }

        private bool CreateDeliveryDocument(int SalesOrderID, SalesOrderDocument salesDocumentData)
        {
            bool createdSuccessfully = true;

            Documents deliveryDocument = Globals.oCompany.GetBusinessObject(BoObjectTypes.oDeliveryNotes);
            
            deliveryDocument.DocDate = DateTime.Now;
            deliveryDocument.DocDueDate = salesDocumentData.DeliveryDate;

            deliveryDocument.TaxDate = DateTime.Now;
            deliveryDocument.OpeningRemarks = salesDocumentData.Comment;

            deliveryDocument.DocTotal = salesDocumentData.Total;
            deliveryDocument.CardCode = salesDocumentData.BusinessPartner;

            int lineCount = 0;
            foreach (SalesOrderLineItem line in salesDocumentData.LineItem)
            {
                deliveryDocument.Lines.BaseEntry = SalesOrderID;
                deliveryDocument.Lines.BaseType = (int)BoAPARDocumentTypes.bodt_Order;
                deliveryDocument.Lines.BaseLine = lineCount;
                lineCount++;

                deliveryDocument.Lines.ItemCode = line.ItemCode;
                deliveryDocument.Lines.WarehouseCode = line.SourceWarehouseCode;
                deliveryDocument.Lines.Quantity = line.Quantity;
                deliveryDocument.Lines.UnitPrice = line.UnitPrice;
                deliveryDocument.Lines.DiscountPercent = line.DiscountPercentage;
                deliveryDocument.Lines.TaxCode = line.TaxCode;
                deliveryDocument.Lines.Price = line.UnitPrice;

                deliveryDocument.Lines.UserFields.Fields.Item("U_VSC_StateID").Value = line.StateId;

                if (line.ItemType == SalesItemType.Batch)
                {
                    deliveryDocument.Lines.BatchNumbers.BatchNumber = line.BatchNumber;
                    deliveryDocument.Lines.BatchNumbers.Quantity = line.Quantity;
                    deliveryDocument.Lines.BatchNumbers.Add();
                }
                else if (line.ItemType == SalesItemType.Serial)
                {
                    // Need to do a split.
                    deliveryDocument.Lines.SerialNumbers.SystemSerialNumber = line.SysNumber;
                    deliveryDocument.Lines.SerialNumbers.Add();
                }
                deliveryDocument.Lines.Add();
            }

            int ErrorCodeFromSAP = deliveryDocument.Add();

            if (ErrorCodeFromSAP != 0)
            {
                string ErrorMessageFromSAP = Globals.oCompany.GetLastErrorDescription();
				throw new Exception(NSC_DI.UTIL.Message.Format(ErrorMessageFromSAP));
			}

            string newCode = null;

            // Get the newly created object's code
            Globals.oCompany.GetNewObjectCode(out newCode);

            if (newCode != null)
            {
                NewlyCreatedDeliveryID = Convert.ToInt32(newCode).ToString();
            }

            return createdSuccessfully;
        }

        private void ClearForm()
        {
			SalesOrderFormControls formControls = new SalesOrderFormControls(_VirSci_Helper_Form, _SBO_Form);
            formControls.ClearForm();
            Load_Matrix();
        }

        #endregion Document Creation

        #region Button Click Events

        private bool TaxCodeExists(string taxCode)
        {
            bool exists = false;

            string sqlQuery = string.Format("SELECT [OSTC].[Code], [OSTC].[Name], [OSTC].[Rate] FROM [OSTC] WHERE [OSTC].[Code] = '{0}'", taxCode);

            Recordset dbCommunication = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as Recordset;

            // Grab data from SQL
            dbCommunication.DoQuery(sqlQuery);

            // Add allowed warehouses to the drop down
            if (dbCommunication.RecordCount > 0)
            {
                exists = true;
            }

            return exists;
        }

        private void BTN_CRTE_Click()
        {
            try
            {
                // Force focus on the form that we just clicked.
                _SBO_Form.Select();

                // Calculate the sales order total including taxes and discounts
                CalculateAllRows();

                #region Creating required controllers - Api Included.
 /*	// COMPLIANCE
                // Prepare a connection to the state API
                var TraceCon = new NSC_DI.SAP.TraceabilityAPI(
                    SAPBusinessOne_Application: Globals.oApp,
                    SAPBusinessOne_Company: Globals.oCompany
                );

                // Create an API call -> inventory_manifest
                BioTrack.API bioTrackApi = TraceCon.new_API_obj();

                // Get our Settings controller for the State Information for Complain calls.
                NSC_DI.UTIL.Settings controllerSettings = new Controllers.Settings(
                    SAPBusinessOne_Application: Globals.oApp,
                    SAPBusinessOne_Company: Globals.oCompany,
                    SAPBusinessOne_Form: null
                );

                // Add the API Call into our compliance monitor.
                Controllers.Compliance controllerCompliance = new Controllers.Compliance(
                    SAPBusinessOne_Application: Globals.oApp,
                    SAPBusinessOne_Company: Globals.oCompany
                );

                // Prepare a controller for the "Goods Receipt" documents
                Controllers.GoodsReceipt controllerGoodsReceipt = new Controllers.GoodsReceipt(
                    SAPBusinessOne_Application: Globals.oApp,
                    SAPBusinessOne_Company: Globals.oCompany
                );

                // Prepare a controller for the "Goods Issued" documents
                Controllers.GoodsIssued controllerGoodsIssued = new Controllers.GoodsIssued(
                       SAPBusinessOne_Application: Globals.oApp,
                       SAPBusinessOne_Company: Globals.oCompany
                );
*/
                #endregion Creating required controllers

                // Get controls of the form.
				SalesOrderFormControls formControls = new SalesOrderFormControls(_VirSci_Helper_Form, _SBO_Form);

                #region Validate that the Text Fields have any value.

                // Handle Validation. 
                if (string.IsNullOrEmpty(formControls.txtBusinessPartnerName.Value) || string.IsNullOrEmpty(formControls.txtBusinessPartnerCode.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please select a business partner!");
                    _SBO_Form.Select();
                    formControls.txtBusinessPartnerName.Active = true;
                    return;
                }

                if (string.IsNullOrEmpty(formControls.txtDeliveryDate.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter a sales/delivery date!");
                    _SBO_Form.Select();
                    formControls.txtDeliveryDate.Active = true;
                    return;
                }

                DateTime salesOrderDate;

                try
                {
                    string DateToParse = formControls.txtDeliveryDate.Value;
                    int year = Convert.ToInt32(DateToParse.Substring(0, 4));
                    int month = Convert.ToInt32(DateToParse.Substring(4, 2));
                    int day = Convert.ToInt32(DateToParse.Substring(6, 2));

                    salesOrderDate = new DateTime(year, month, day);
                }
                catch (Exception ex)
                {
                    Globals.oApp.StatusBar.SetText("Please enter a valid sales/delivery date!");
                    _SBO_Form.Select();
                    formControls.txtDeliveryDate.Active = true;
                    return;
                }
                #endregion

				var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;

                #region Prepare lists of data we will need for transaction to process
                // Prepare a list of "Goods Receipt" lines items to process
				List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> goodsReceiptLines = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();

                // Prepare a list of "Goods Issues" line items to process
				List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines> goodsIssueLines = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();

                // Prepare a list of "Inventory Lot" line items to process with the state
				//List<BioTrack.Inventory.InvLotCreate> inventoryLotsToSplit = new List<BioTrack.Inventory.InvLotCreate>();	// COMPLIANCE
				List<InvLotCreate> inventoryLotsToSplit = new List<InvLotCreate>();

                // Prepare a list of plants to conver to inventory
                List<string> plantsToConvertToInventory = new List<string>();

                // Prepare a list of batches that we will be sublotting, and the amount of sublots we've processed already
                Dictionary<string, int> DictionaryOfNewBatchCounts = new Dictionary<string, int>(); 
                #endregion

                #region Calculate Discounts
                double orderDiscount = 0;
                if (!string.IsNullOrEmpty(formControls.txtDiscountPercentage.Value))
                {
                    double.TryParse(formControls.txtDiscountPercentage.Value, out orderDiscount);
                }

                // Make it a percentage
                orderDiscount /= 100;
                #endregion

                // Prepare the "Sales Order" SAP document.
                SalesOrderDocument salesDocument = new SalesOrderDocument(formControls.txtBusinessPartnerCode.Value, salesOrderDate, Convert.ToDouble(formControls.txtTax.Value.Replace("$", "")), orderDiscount, Convert.ToDouble(formControls.txtTotal.Value.Replace("$", "")), formControls.txtComment.Value);

                #region Prepare "Goods Receipt Line Items", "Goods Issued Line Items", and Washington State API Calls
                // For every row in the matrix.  (Matrix Rows start at 1 per SAP)
                for (int i = 1; i < MTX_MAIN.RowCount + 1; i++)
                {
                    #region Product Information Grabbing -- Matrx Loop

                    // If Matrix row does not have an item code selected, ignore it.
                    if (string.IsNullOrEmpty(MTX_MAIN.Columns.Item(MatrixColumn.ItemNo).Cells.Item(i).Specific.Value)) continue;

                    // We handle serialized items and batch items differently.
                    bool isSerializedItem = MTX_MAIN.Columns.Item(MatrixColumn.SerialOrBatch).Cells.Item(i).Specific.Value == "S" ? true : false;
                    bool isBatchedItem = MTX_MAIN.Columns.Item(MatrixColumn.SerialOrBatch).Cells.Item(i).Specific.Value == "B" ? true : false;

                    // Grab data from the selected matrix row.
                    string itemCode = MTX_MAIN.Columns.Item(MatrixColumn.ItemNo).Cells.Item(i).Specific.Value.ToString();
                    double quantity = Convert.ToDouble(MTX_MAIN.Columns.Item(MatrixColumn.Quantity).Cells.Item(i).Specific.Value);
                    double onhand = Convert.ToDouble(MTX_MAIN.Columns.Item(MatrixColumn.OnHand).Cells.Item(i).Specific.Value);

                    double discount = Convert.ToDouble(MTX_MAIN.Columns.Item(MatrixColumn.Discount).Cells.Item(i).Specific.Value);
                    double unitPrice = Convert.ToDouble(MTX_MAIN.Columns.Item(MatrixColumn.UnitPrice).Cells.Item(i).Specific.Value);
                    double lineTotal = Convert.ToDouble(MTX_MAIN.Columns.Item(MatrixColumn.LineTotal).Cells.Item(i).Specific.Value.Replace("$", ""));

                    string taxCode = MTX_MAIN.Columns.Item(MatrixColumn.Tax).Cells.Item(i).Specific.Value.ToString();
                    string taxPercentage = MTX_MAIN.Columns.Item(MatrixColumn.TaxPercentage).Cells.Item(i).Specific.Value.ToString();

                    #region Validate Tax Rate/Code

                    EditText taxPercentageRowItem = MTX_MAIN.Columns.Item(MatrixColumn.TaxPercentage).Cells.Item(i).Specific as EditText;

                    // If the tax has a value we can add it in as well.
                    if (!string.IsNullOrEmpty(taxCode))
                    {
                        double taxRate = 0;
                        // Validate that we correctly got a number.
                        if (!double.TryParse(taxPercentage, out taxRate))
                        {
                            Globals.oApp.StatusBar.SetText("Please verify you have chose a tax code by pressing tab in the given matrix column", BoMessageTime.bmt_Short);

                            EditText taxCodeRowItem = MTX_MAIN.Columns.Item(MatrixColumn.Tax).Cells.Item(i).Specific as EditText;
                            taxCodeRowItem.Value = "";
                            taxPercentageRowItem.Value = "";
                            taxCodeRowItem.Active = true;
                            return;
                        }
                    }

                    if (!string.IsNullOrEmpty(taxPercentageRowItem.Value) && !TaxCodeExists(taxCode))
                    {
                        EditText taxCodeRowItem = MTX_MAIN.Columns.Item(MatrixColumn.Tax).Cells.Item(i).Specific as EditText;
                        taxCodeRowItem.Value = "";
                        taxPercentageRowItem.Value = "";
                        taxCodeRowItem.Active = true;
                        return;
                    }
                    #endregion

                    string stateId = MTX_MAIN.Columns.Item(MatrixColumn.StateId).Cells.Item(i).Specific.Value;
                    string warehouseCode = MTX_MAIN.Columns.Item(MatrixColumn.WarehouseCode).Cells.Item(i).Specific.Value;
                    string serialOrBatchNo = MTX_MAIN.Columns.Item(MatrixColumn.UniqueId).Cells.Item(i).Specific.Value.ToString();
                    #endregion

                    // Prepare a new "Sales Order Line Item"
                    SalesOrderLineItem salesLineItem = new SalesOrderLineItem(itemCode, SalesItemType.Normal, serialOrBatchNo, quantity, unitPrice, warehouseCode, stateId, discount, lineTotal, taxCode);

                    #region Batch Managed Item
                    // If an item is a "batch managed" item.
                    if (isBatchedItem)
                    {
                        // Set the "Sales Order Line Item" to batch mode
                        salesLineItem.ItemType = SalesItemType.Batch;
                        salesLineItem.BatchNumber = serialOrBatchNo;

                        // Add batch information to the "Goods Issued Line Item"
						NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines goodsIssuedLineItem_TotalSold = new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
                        {
                            ItemCode = itemCode,
                            Quantity = quantity,
                            WarehouseCode = warehouseCode,
							BatchLineItems = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>() 
                            {
                                new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches()
                                {
                                    BatchNumber = serialOrBatchNo, 
                                    Quantity= quantity
                                }
                            }
                        };
                        

						// If the amount we are selling is not the full amount of the items we have on hand, we need to call to the state and split the inventory into a new lot number.
                        if (quantity != onhand)
                        {
                            // Create a new WA state Inventory split request
                            inventoryLotsToSplit.Add
                            (
                                new InvLotCreate()
                                {
                                    BarcodeID = stateId,
                                    RemoveQuantity = quantity,
                                    RemoveQuantityUOM = BioTrackAPIUnitOfMeasurement.g}
                            );

                            // Keep track of the index of the inventory lot to split, so once we have the results from the state we can easily find the newly created state lot/batch ID.
                            salesLineItem.IndexOfNewStateId = inventoryLotsToSplit.Count - 1;

                            // Keep track of the newly generated batch ID
                            string newBatchID = "";

                            #region Calculate How Many Times This Batch Has Been Sub-Lotted
                            // Prepare an integer to keep track of how many times the batch has been sublotted
                            int totalTimesSublottedNow = 0;

                            // See if this item has already been sublotted already in this transaction
                            if (DictionaryOfNewBatchCounts.ContainsKey(serialOrBatchNo))
                            {
                                // Get the total number of times we've sublotted this batch already in this transaction
                                int totalTimesSublottedAlready = DictionaryOfNewBatchCounts[serialOrBatchNo];

                                // Add one to the total number of times we've sublotted this batch already in this transaction
                                totalTimesSublottedNow = totalTimesSublottedAlready + 1;

                                // Set the current dictionary value to the newly updated total number of times we've sublotted this batch already in this transaction
                                DictionaryOfNewBatchCounts[serialOrBatchNo] = totalTimesSublottedNow;
                            }
                            else
                            {
                                // Call SQL and see how many sublots we've made already with this batch as the base
                                string SQL_Query_CountPreviousSubLot = "SELECT COUNT(*) FROM [OBTN] WHERE [OBTN].[DistNumber] LIKE '" + serialOrBatchNo + "-%'";

								string countFromSQL = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(SQL_Query_CountPreviousSubLot).Fields.Item(0).Value.ToString();

                                int totalTimesSublottedThisBatchInThePast = Convert.ToInt32(countFromSQL);

                                // Add the total number of times we've sublotted this batch in the past and add one
                                totalTimesSublottedNow = totalTimesSublottedThisBatchInThePast + 1;

                                // Add a new dictionary value to keep track of how many times we've sub-lotted this batch in this transactions.
                                DictionaryOfNewBatchCounts.Add(serialOrBatchNo, totalTimesSublottedNow);
                            }
                            #endregion

                            // Set new batch ID based off of current batch and sub-lot count
                            newBatchID = serialOrBatchNo + "-" + totalTimesSublottedNow;

                            // Set the "Sales Order Line Item" to the newly created batch ID
                            salesLineItem.BatchNumber = newBatchID;

                            // Create a "Goods Receipt" line item for the total amount of the item we are selling.
                            // We are creating this document because the state system requires sub-lotting of less than full quantities.
							goodsReceiptLines.Add(new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines()
                            {
                                ItemCode = itemCode,
                                Quantity = quantity,
                                WarehouseCode = warehouseCode,
								ListOfBatchLineItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem>() 
                                { 
                                    new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem()
                                    {
                                        BatchNumber = newBatchID, 
                                        Quantity = quantity,
                                        UserDefinedFields = new Dictionary<string,string>()
                                        {
                                            {"U_VSC_StateID",stateId}
                                        }
                                    }
                                }
                            });
                            // Add batch information to the "Goods Issued Line Item"
                            goodsIssueLines.Add(goodsIssuedLineItem_TotalSold);
                        }
                    }
                    #endregion

                    #region Serialized Items
                    // Item is serailized managed
                    else if (isSerializedItem)
                    {
                        // Set the "Sales Order Line Item" to serial mode
                        salesLineItem.ItemType = SalesItemType.Serial;
                        salesLineItem.SysNumber = Convert.ToInt32(serialOrBatchNo);

                        // If serialized and has a state ID, send "Convert to Sellable" API command to the state
                        plantsToConvertToInventory.Add(stateId);
                    }
                    #endregion

                    // Add the new line to the "Sales Order" document.
                    salesDocument.LineItem.Add(salesLineItem);

                }
                #endregion

                #region Create Goods Receipt
                if (goodsReceiptLines.Count > 0)
                {
                    // Process the "Goods Receipt" document
					NSC_DI.SAP.GoodsReceipt.Create(DateTime.Now, goodsReceiptLines);

					//// If we failed creating the "Goods Receipt" document
					//if (!controllerGoodsReceipt.WasSuccessful)
					//{
					//	Globals.oApp.StatusBar.SetText("Failed to create SAP 'Goods Receipt' document for the newly created batches."
					//	);

					//	// Send the Viridian team an e-mail if any errors happened here.  This is a mission-critical business process.
					//	SAP_BusinessOne.Helpers.EMail.Send(
					//		EmailAddresses: new[] { "errors@viridiansciences.com" },
					//		Subject: "ERROR: Prime - I502 - Producer/Processor",
					//		Body: "There was an error on the Prime - I502 - Producer/Processor AddOn." +
					//		"  Specifically in the \"Sales Order\" form, when trying to create the 'Goods Receipt' SAP document." +
					//		"  The last error message as follows:" +
					//		Environment.NewLine + Environment.NewLine +
					//		controllerGoodsReceipt.LastErrorMessage
					//	);

					//	return;
					//}
                } 
                #endregion

                #region Create Goods Issued
                if (goodsIssueLines.Count > 0)
                {
                    // Process the "Goods Issued" document
					NSC_DI.SAP.GoodsIssued.Create(DateTime.Now, goodsIssueLines);

					//// If the "Goods Issued" document did not get created successfully.
					//if (!controllerGoodsIssued.WasSuccessful)
					//{
					//	Globals.oApp.StatusBar.SetText("Failed to create SAP 'Goods Issued' document for the sold items."
					//	);

					//	// Send the Viridian team an e-mail if any errors happened here.  This is a mission-critical business process.
					//	SAP_BusinessOne.Helpers.EMail.Send(
					//		EmailAddresses: new[] { "errors@viridiansciences.com" },
					//		Subject: "ERROR: Prime - I502 - Producer/Processor",
					//		Body: "There was an error on the Prime - I502 - Producer/Processor AddOn." +
					//		"  Specifically in the \"Sales Order\" form, when trying to create the 'Goods Issued' SAP document." +
					//		"  The last error message as follows:" +
					//		Environment.NewLine + Environment.NewLine +
					//		controllerGoodsIssued.LastErrorMessage
					//	);

					//	return;
					//}
                } 
                #endregion

                #region Creates "Sales Order"
                // Attempt to create the SAP "Sales Order" Document.
	            CreateSalesDocument(salesDocument);
				//if (!CreateSalesDocument(salesDocument))
				//{
				//	// Failed to create the SAP "Sales Order" document 
				//	Globals.oApp.StatusBar.SetText("Failed to create SAP 'Sales Order' document."
				//	);

				//	// Send the Viridian team an e-mail if any errors happened here.  This is a mission-critical business process.
				//	SAP_BusinessOne.Helpers.EMail.Send(
				//		EmailAddresses: new[] { "errors@viridiansciences.com" },
				//		Subject: "ERROR: Prime - I502 - Producer/Processor",
				//		Body: "There was an error on the Prime - I502 - Producer/Processor AddOn." +
				//		"  Specifically in the \"Sales Order\" form, when trying to create the 'Sales Order' SAP document." +
				//		"  The last error message as follows:" +
				//		Environment.NewLine + Environment.NewLine +
				//		LastErrorMessage
				//	);

				//	return;
				//} 
                #endregion

                #region Create "Delivery" document
                // Attempt to create the SAP "Delivery" document
	            CreateDeliveryDocument(Convert.ToInt32(NewlyCreatedSalesOrderID), salesDocument);
				//if (!CreateDeliveryDocument(Convert.ToInt32(NewlyCreatedSalesOrderID), salesDocument))
				//{
				//	// Failed to create the SAP "Delivery" document
				//	Globals.oApp.StatusBar.SetText("Failed to create SAP 'Delivery' document."
				//	);

				//	// Send the Viridian team an e-mail if any errors happened here.  This is a mission-critical business process.
				//	SAP_BusinessOne.Helpers.EMail.Send(
				//		EmailAddresses: new[] { "errors@viridiansciences.com" },
				//		Subject: "ERROR: Prime - I502 - Producer/Processor",
				//		Body: "There was an error on the Prime - I502 - Producer/Processor AddOn." +
				//		"  Specifically in the \"Sales Order\" form, when trying to create the 'Delivery' SAP document." +
				//		"  The last error message as follows:" +
				//		Environment.NewLine + Environment.NewLine +
				//		LastErrorMessage
				//	);

				//	return;
				//} 
                #endregion

                #region Post to the Washington State API - Inventory
 /*	// COMPLIANCE
                if (plantsToConvertToInventory.Count > 0)
                {
                    BioTrack.Plant.Convert_To_Inventory(BioTrackAPI: ref bioTrackApi, BarcodeIDs: plantsToConvertToInventory.ToArray());

                    // Create new "Compliance" line item
                    string compID_PlantConvert = controllerCompliance.CreateComplianceLineItem(
                           Reason: NavSol.Models.Compliance.ComplianceReasons.Inventory_Split,
                           API_Call: bioTrackApi.XmlApiRequest.ToString()
                    );

                    // Post to the Washington State system
                    bioTrackApi.PostToApi();

                    // If the post to the state was successful
                    if (bioTrackApi.WasSuccesful)
                    {
                        // Update the compliance monitor with a success
                        controllerCompliance.UpdateComliancy(
                            ComplianceID: compID_PlantConvert,
                            ResponseXML: bioTrackApi.xDocFromResponse.ToString(),
                            Status: Controllers.Compliance.ComplianceStatus.Success
                        );
                    }
                    else
                    {
                        // Update the compliance monitor with a failure
                        controllerCompliance.UpdateComliancy(
                            ComplianceID: compID_PlantConvert,
                            ResponseXML: bioTrackApi.xDocFromResponse.ToString(),
                            Status: Controllers.Compliance.ComplianceStatus.Failed
                        );
                    }
                }

                if (inventoryLotsToSplit.Count > 0)
                {
                    // Prepare the post to the state API system to split inventory
                    BioTrack.Inventory.Split(BioTrackAPI: ref bioTrackApi, data: inventoryLotsToSplit.ToArray());

                    // Create new "Compliance" line item
                    string compID = controllerCompliance.CreateComplianceLineItem(
                           Reason: NavSol.Models.Compliance.ComplianceReasons.Inventory_Split,
                           API_Call: bioTrackApi.XmlApiRequest.ToString());

                    // Post to the Washington State system
                    bioTrackApi.PostToApi();

                    // If the post to the state was successful
                    if (bioTrackApi.WasSuccesful)
                    {
                        // Update the compliance monitor with a success
                        controllerCompliance.UpdateComliancy(
                            ComplianceID: compID,
                            ResponseXML: bioTrackApi.xDocFromResponse.ToString(),
                            Status: Controllers.Compliance.ComplianceStatus.Success
                        );

                        // Get the XML response from the state system
                        XmlNodeList responseBarcodesXML = bioTrackApi.xmlDocFromResponse.SelectNodes("/xml/barcode_id");

                        // Prepare a SQL query to update batches and delivery line items with the new state ID
                        string SQL_Query_UpdateBatches = "";
                        string SQL_Query_UpdateDeliveryLineItems = "";

                        // For each state id we got back we need to associate it with the batch we are creating in sap for the seperation.
                        for (int i = 0; i < responseBarcodesXML.Count; i++)
                        {
                            string newStateId = responseBarcodesXML[i].InnerText;
                            string oldStateId = inventoryLotsToSplit[i].BarcodeID;

                            foreach (var item in goodsReceiptLines)
                            {
                                var firstBatchItemInGoodsReceiptLineItem = item.ListOfBatchLineItems[0];
                                var oldStateIDFromFirstBatch = firstBatchItemInGoodsReceiptLineItem.UserDefinedFields["U_VSC_StateID"].ToString();
                                if (oldStateIDFromFirstBatch == oldStateId)
                                {
                                    string batchIDToUpdate = item.ListOfBatchLineItems[0].BatchNumber;

                                    // Update the newly created batch with the new state ID
                                    SQL_Query_UpdateBatches += @"
UPDATE [OBTN]
SET [OBTN].[U_NSC_StateID] = '" + newStateId + @"'
WHERE [OBTN].[DistNumber] = '" + batchIDToUpdate + @"'
AND [OBTN].[U_NSC_StateID] = '" + oldStateId + @"'
";
                                    // Update the delivery line item with the appropriate state ID
                                    SQL_Query_UpdateDeliveryLineItems += @"
UPDATE [DLN1]
SET [DLN1].[U_NSC_StateID] = '" + newStateId + @"'
WHERE
[DLN1].[DocEntry] = '" + NewlyCreatedDeliveryID + @"'
AND [DLN1].[U_NSC_StateID] = '" + oldStateId + @"'
";
                                }
                            }
                        }

                        // Run SQL to update the database with the new state ID's
                        _VirSci_Helper_SQL.RunSQLQuery(SQLQuery: SQL_Query_UpdateBatches);
                        _VirSci_Helper_SQL.RunSQLQuery(SQLQuery: SQL_Query_UpdateDeliveryLineItems);

                        // Alert the user about the successful sales order!
                        Globals.oApp.StatusBar.SetText("Successfully processed the sales order and posted new batch sublots!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success
                        );
                    }
                    // Posting the "Inventory Lot" state API failed.
                    else
                    {
                        // Update the compliance monitor with a failure
                        controllerCompliance.UpdateComliancy(
                            ComplianceID: compID,
                            ResponseXML: bioTrackApi.xDocFromResponse.ToString(),
                            Status: Controllers.Compliance.ComplianceStatus.Failed
                        );

                        // Alert the end user something went wrong with the state post
                        Globals.oApp.StatusBar.SetText("Failed to communicate with State API! See compliancy monitor for reason.  Please contact Viridian Sciences support before trying to process this delivery."
                        );

                        // Send the Viridian team an e-mail if any errors happened here.  This is a mission-critical business process.
                        SAP_BusinessOne.Helpers.EMail.Send(
                            EmailAddresses: new[] { "errors@viridiansciences.com" },
                            Subject: "ERROR: Prime - I502 - Producer/Processor",
                            Body: "There was an error on the Prime - I502 - Producer/Processor AddOn." +
                            "  Specifically in the \"Sales Order\" form, when trying to post the inventory lot API call." +
                            "  The Washington state response is as follows:" +
                            Environment.NewLine + Environment.NewLine +
                            bioTrackApi.xDocFromResponse.ToString()
                        );
                    }
                }
*/
                #endregion

                // Clear the form of all selections and entries
                ClearForm();
            }
			catch (System.ComponentModel.WarningException)
			{
				// message was already displayed
			}
			catch (Exception ex)
            {
				//// Send the Viridian team an e-mail if any errors happened here.  This is a mission-critical business process.
				//SAP_BusinessOne.Helpers.EMail.Send(
				//	EmailAddresses: new[] { "errors@viridiansciences.com" },
				//	Subject: "ERROR: Prime - I502 - Producer/Processor",
				//	Body: "There was an error on the Prime - I502 - Producer/Processor AddOn." +
				//	"  Specifically in the \"Sales Order\" form, when trying to create a sales order." +
				//	"  The .NET exception is as follows:" +
				//	Environment.NewLine + Environment.NewLine +
				//	ex.ToString()
				//);
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
        }

	    /// <summary>
	    /// Handle removing an item from the matrix.
	    /// </summary>
	    public void BTN_REMOV_Click()
	    {
		    try
		    {
			    var MTX_MAIN = _SBO_Form.Items.Item("MTX_MAIN").Specific;

			    DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("Matrix");

			    // Go backwards across the list to remove the correct lines..
			    for (int i = MTX_MAIN.RowCount; i >= 1; i--)
			    {
				    if (MTX_MAIN.IsRowSelected(i))
				    {
					    try
					    {
						    //Remove this row out of the datatable since its selected.
						    DataTable.Rows.Remove(i - 1);
					    }
					    catch (Exception ex)
					    {
						    /*  */
					    }
				    }
			    }

			    MTX_MAIN.LoadFromDataSource();
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
				//NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

	    #endregion Button Click Events

        #region Data Classes For Ease Of Use

        public class SalesOrderDocument
        {
            public string BusinessPartner { get; set; }
            public DateTime DeliveryDate { get; set; }
            public double Tax { get; set; }
            public double DiscountPercentage { get; set; }
            public double Total { get; set; }
            public string Comment { get; set; }

            public List<SalesOrderLineItem> LineItem { get; set; }

            public SalesOrderDocument(string BusinessPartner, DateTime DeliveryDate, double Tax, double DiscountPercentage, double Total, string Comment)
            {
                this.BusinessPartner = BusinessPartner;
                this.DeliveryDate = DeliveryDate;
                this.Tax = Tax;
                this.DiscountPercentage = DiscountPercentage;
                this.Total = Total;
                this.Comment = Comment;

                LineItem = new List<SalesOrderLineItem>();
            }
        }

        public class SalesOrderLineItem
        {
            public string ItemCode { get; set; }

            public int SysNumber { get; set; }
            public string BatchNumber { get; set; }

            public double Quantity { get; set; }
            public double UnitPrice { get; set; }
            public double DiscountPercentage { get; set; }
            public double LineTotal { get; set; }
            public string TaxCode { get; set; }

            public string SourceWarehouseCode { get; set; }

            public string StateId { get; set; }

            public SalesItemType ItemType { get; set; }

            // Simple way to get new State Id if it was split.
            // All Line items will be set as -1. If a number is > 0 then that's the index we should look
            // for in the returned Ids for this Line item.
            public int IndexOfNewStateId = -1;

            /// <summary>
            ///  Sorry for the nasty long params.
            /// </summary>
            public SalesOrderLineItem(string ItemCode, SalesItemType ItemType, string UniqueIdentifier, double Quantity, double UnitPrice,
                string SourceWarehouseCode, string StateId, double Discount, double LineTotal, string TaxCode)
            {
                this.ItemCode = ItemCode;
                this.ItemType = ItemType;
                this.Quantity = Quantity;
                this.UnitPrice = UnitPrice;
                this.SourceWarehouseCode = SourceWarehouseCode;
                this.StateId = StateId;

                DiscountPercentage = Discount;
                this.LineTotal = LineTotal;
                this.TaxCode = TaxCode;

                switch (ItemType)
                {
                    case SalesItemType.Batch:
                        BatchNumber = UniqueIdentifier;
                        break;
                    case SalesItemType.Serial:
                        // Removing the ItemCode from the Serial Number so we are left with the System Number.
                        string sysNumber = UniqueIdentifier;
                        int convertedSysNumber = 0;
                        if (Int32.TryParse(sysNumber, out convertedSysNumber))
                        {
                            SysNumber = convertedSysNumber;
                        }
                        break;
                }
            }
        }

        public class SalesOrderFormControls
        {
            public EditText txtBusinessPartnerName { get; set; }
            public EditText txtBusinessPartnerCode { get; set; }
            public EditText txtTotalBeforeDiscount { get; set; }
            public EditText txtDeliveryDate { get; set; }
            public EditText txtDiscount { get; set; }
            public EditText txtDiscountPercentage { get; set; }
            public EditText txtTax { get; set; }
            public EditText txtTotal { get; set; }
            public EditText txtComment { get; set; }

			public SalesOrderFormControls(VERSCI.Forms FormHelper, Form FormToSearch)
            {
                txtBusinessPartnerName	= FormHelper.GetControlFromForm(VERSCI.Forms.FormControlTypes.EditText, "TXT_BPNAME", FormToSearch);
                txtBusinessPartnerCode	= FormHelper.GetControlFromForm(VERSCI.Forms.FormControlTypes.EditText, "TXT_BPCODE", FormToSearch);
                txtDeliveryDate			= FormHelper.GetControlFromForm(VERSCI.Forms.FormControlTypes.EditText, "TXT_DDATE", FormToSearch);
                txtTotalBeforeDiscount	= FormHelper.GetControlFromForm(VERSCI.Forms.FormControlTypes.EditText, "TXT_TBDIS", FormToSearch);
                txtDiscount				= FormHelper.GetControlFromForm(VERSCI.Forms.FormControlTypes.EditText, "TXT_DISC", FormToSearch);
                txtDiscountPercentage	= FormHelper.GetControlFromForm(VERSCI.Forms.FormControlTypes.EditText, "TXT_DISCP", FormToSearch);
				txtTax					= FormHelper.GetControlFromForm(VERSCI.Forms.FormControlTypes.EditText, "TXT_TAX", FormToSearch);
				txtTotal				= FormHelper.GetControlFromForm(VERSCI.Forms.FormControlTypes.EditText, "TXT_TOTAL", FormToSearch);
				txtComment				= FormHelper.GetControlFromForm(VERSCI.Forms.FormControlTypes.EditText, "TXT_REMARK", FormToSearch);
            }

            public void ClearForm()
            {
                if (txtComment != null)
                {
                    txtComment.Value = "";
                }

                if (txtDeliveryDate != null)
                {
                    txtDeliveryDate.Value = "";
                }

                if (txtBusinessPartnerName != null)
                {
                    txtBusinessPartnerName.Value = "";
                }

                if (txtBusinessPartnerCode != null)
                {
                    txtBusinessPartnerCode.Value = "";
                }

                if (txtTotalBeforeDiscount != null)
                {
                    txtTotalBeforeDiscount.Value = "";
                }

                if (txtDiscountPercentage != null)
                {
                    txtDiscountPercentage.Value = "";
                }

                if (txtDiscount != null)
                {
                    txtDiscount.Value = "";
                }

                if (txtTax != null)
                {
                    txtTax.Value = "";
                }

                if (txtTotal != null)
                {
                    txtTotal.Value = "";
                }
            }
        }

        protected class WhereClauseHelper
        {
            public List<string> SerialNumbersToExclude = new List<string>();
            public List<string> BatchNumbersToExclude = new List<string>();

            public string BatchItemWhereClause { get; set; }
            public string SerialItemWhereClause { get; set; }

            // Create the actual where clauses for both serial and batched items.
            public void CreateWhereClauses()
            {
                BatchItemWhereClause = " WHERE [OIBT].[Quantity] > 0 ";
                if (BatchNumbersToExclude.Count > 0)
                {
                    BatchItemWhereClause += @" 
AND [OIBT].[BatchNum] NOT IN 
( ";
                    foreach (string batchNo in BatchNumbersToExclude)
                    {
                        BatchItemWhereClause += batchNo + ", ";
                    }
                    BatchItemWhereClause += ")";
                }

                if (SerialNumbersToExclude.Count > 0)
                {
                    SerialItemWhereClause = @"
WHERE (CONVERT(nvarchar,[OSRN].[ItemCode])+'-'+CONVERT(nvarchar,[OSRN].[SysNumber])) NOT IN 
( ";
                    foreach (string serialNo in SerialNumbersToExclude)
                    {
                        SerialItemWhereClause += serialNo + ", ";
                    }
                    SerialItemWhereClause += ")";
                }
            }
        }

        #endregion Data Classes For Ease Of Use
    }
}
