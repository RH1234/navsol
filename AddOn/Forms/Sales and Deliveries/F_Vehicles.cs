﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbouiCOM;
using Matrix = NavSol.VERSCI.Matrix;

namespace NavSol.Forms.Sales_and_Deliveries
{
    class F_Vehicles : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_VEHICLE";

	    private Dictionary<string, string> vehicle_info;

		// FOR OLD CODE
		public static VERSCI.Forms _VirSci_Helper_Form;
		public static SAPbouiCOM.Application _SBO_Application;
		public static SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		public string FormUID { get; set; }
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
	    [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
	    public virtual bool OnBeforeItemPressed(ItemEvent pVal)
	    {
		    var BubbleEvent = true;
		    var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			if (pVal.ItemUID == "1") BubbleEvent = btn_Add();

		    NSC_DI.UTIL.Misc.KillObject(oForm);
		    GC.Collect();

		    return BubbleEvent;
	    }
	    #endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		public F_Vehicles() : this(Globals.oApp, Globals.oCompany) { }

		public F_Vehicles(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
		{
			_SBO_Application = SAPBusinessOne_Application;
			_SBO_Company = SAPBusinessOne_Company;
		}

        public void Form_Load()
        {
            this.Form_Load_Find();
        }

        public Form Form_Load_Find(Int32? ID = null)
        {
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, true);
                _SBO_Form = oForm;

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                // Set form mode to find
                _SBO_Form.Mode = BoFormMode.fm_FIND_MODE;

				if (ID == null || ID == 0) _SBO_Form.Mode = BoFormMode.fm_ADD_MODE;

                // Find the relevant controls we will want access to
                EditText task_id = (EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "txtDocEnt", _SBO_Form);

                // Enable UDO Form navigation buttons
                _SBO_Form.DataBrowser.BrowseBy = "txtDocEnt";
				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				_SBO_Form.Freeze(false);
				_SBO_Form.VisibleEx = true;

				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
            }

	    private bool btn_Add()
	    {
		    if (_SBO_Form.Mode != BoFormMode.fm_ADD_MODE) return true;

		    try
		    {
			    string strMake = _SBO_Form.Items.Item("txtMake").Specific.Value;
				string strModel = _SBO_Form.Items.Item("txtModel").Specific.Value;
				string strColor = _SBO_Form.Items.Item("txtColor").Specific.Value;
				string strPlate = _SBO_Form.Items.Item("txtPlate").Specific.Value;
				string strVIN = _SBO_Form.Items.Item("txtVIN").Specific.Value;
				string strYear = _SBO_Form.Items.Item("txtYear").Specific.Value;
				string strMiles = _SBO_Form.Items.Item("txtMiles").Specific.Value;

			    //Find and prepair the next CODEID (for both SAP numbering and VID biotrack reasons)
			    // Prepare to run a SQL statement.
			    SAPbobsCOM.Recordset oRecordSet = (SAPbobsCOM.Recordset) _SBO_Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

			    // Count how many current records exist within the database.
			    oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tVehicle + "] ");

				var txtCode = _SBO_Form.Items.Item("txtCode").Specific;

			    try
			    {
				    txtCode.Value = oRecordSet.Fields.Item(0).Value.ToString();
			    }
			    catch
			    {
				    txtCode.Value = "1";
			    }
			    finally
			    {
				    // Run GarbageCollection
				    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
				    GC.Collect();
			    }

			    vehicle_info = new Dictionary<string, string>();
			    vehicle_info.Add("Make", strMake);
			    vehicle_info.Add("Model", strModel);
			    vehicle_info.Add("Color", strColor);
			    vehicle_info.Add("Plate", strPlate);
			    vehicle_info.Add("VIN", strVIN);
			    vehicle_info.Add("Year", strYear);
			    vehicle_info.Add("Miles", strMiles);
			    vehicle_info.Add("Code", txtCode.Value);

			    foreach (KeyValuePair<string, string> entry in vehicle_info)
			    {
				    if (string.IsNullOrEmpty(entry.Value))
				    {
					    _SBO_Application.StatusBar.SetText("All fields must be filled!");
						return false;
				    }
				    int foobar;
				    if (!int.TryParse(strYear, out foobar))
				    {
					    _SBO_Application.StatusBar.SetText("Year must be a number!");
						return false;
					}
			    }

			    return true;
		    }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return false;
		    }
		    finally
		    {
			    //NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }
	}
}
