﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms
{
    public class F_Splash : B1Event
    {
        private static System.Threading.Timer m_timer = null;

        public static SAPbouiCOM.Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
        {
            Item oItm = null;

            try
            {
                Form oForm = CommonUI.Forms.Load("NSC_SPLASH", pSingleInstance);

                if (pCallingFormUID != null)
                {
                    oItm = oForm.Items.Add("TXT_REF", BoFormItemTypes.it_EDIT);
                    oItm.Top = -20;
                    oItm.Specific.Value = pCallingFormUID;
                }

                FormSetup(oForm);

                return oForm;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex, false));
            }
        }

        private static void FormSetup(Form pForm)
        {
            try
            {
                CommonUI.Forms.SetField(pForm, "PIC_SPLASH", System.IO.Path.Combine(Globals.pathToImg, "Viridian-splash.bmp"));
                CommonUI.Forms.SetField(pForm, "STA_VER", Globals.ProdVersion);

                if (Globals.oApp.ClientType == BoClientType.ct_Browser)
                {
                    // Keep Browser from cutting off image
                    pForm.Items.Item("PIC_SPLASH").Width = pForm.Width - 20; 
                }

                var cb = new System.Threading.TimerCallback(SplashTimerCB);

                m_timer = new System.Threading.Timer(cb, pForm.UniqueID, 0, 500);
            }
            catch (Exception ex)
            {
                pForm.Close();
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
            }
        }

        private static void SplashTimerCB(object state)
        {
            Form oForm = null;
            try
            {
                if (!CommonUI.Forms.IsFormOpen("NSC_SPLASH")) throw new Exception();

                var formUID = state as string;

                oForm = CommonUI.Forms.GetFormFromUID(formUID);

                var loadingVal = CommonUI.Forms.GetField<string>(oForm, "STA_LOAD");

                if (loadingVal.Substring(7).Length >= 3) loadingVal = "Loading";
                else loadingVal += ".";

                CommonUI.Forms.SetField(oForm, "STA_LOAD", loadingVal);

            }
            catch (Exception)
            {
                if (m_timer != null) m_timer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
                m_timer = null;
            }
        }
    }
}
