﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using Matrix = NavSol.CommonUI.Matrix;
using NavSol.Forms.Production;


namespace NavSol.Forms
{
	public class F_PlantDetails : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "NSC_PLANT_DET";
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            //****This line was causing the allpication to crash as the function could not resolve itself causing an overflow. 
            //var plant = CommonUI.Forms.GetField<string>(oForm, "TXT_ITEM");
            EditText oEdit = oForm.Items.Item("TXT_ITEM").Specific;
            string plant = oEdit.Value; 

            switch (pVal.ItemUID)
			{

				case "TAB_WATER":
					Load_Matrix(oForm, "WATER");
					break;

				case "TAB_FEED":
					Load_Matrix(oForm, "FEED");
					break;

				case "TAB_TREAT":
					Load_Matrix(oForm, "TREAT");
					break;

				case "TAB_WASTE":
					Load_Matrix(oForm, "WASTE");
					break;

				case "TAB_XFER":
					Load_Matrix(oForm, "TRANSFER");
					break;
                case "TAB_TASKS":
                    Load_Matrix(oForm, "TASKS");
                    break;
                case "TAB_PICS":
                    Load_Matrix(oForm, "PICS");
                    break;

                // Water
                case "BTN_H2O":
                    // Open the Watering Can with the selected plants.  
                   // (new Production.F_WaterWizard()).Form_Load(new string[] { plant });
                    Production.F_WaterWizard H2O_Wiz = new F_WaterWizard(B1Connections.theAppl, Globals.oApp.Company.GetDICompany());
                    H2O_Wiz.Form_Load(false, new string[] { plant });                    
                  break;
				// Feed
				case "BTN_FEED":
                    Production.F_FeedWizard.Form_Load(false, new string[] { plant });
					break;

				// Treat
				case "BTN_TREAT":
                    (new Production.F_TreatWizard()).Form_Load(new string[] { plant });
					break;

				// Prune
				case "BTN_PRUNE":
                   (new Production.F_WasteWizard()).Form_Load(false, new string[] { plant });
					break;

				// Task
				case "BTN_TASK":
                    // TODO: Pass ID of plant that the new task is for
                    F_GreenbookTask.Form_Load_Create();
					break;

                // Add Remarks to Plant Journal
                case "BTN_SREM":
                    //Save Remarks to the plant journal 
                    oEdit = oForm.Items.Item("TXT_REM").Specific;
                    SaveRemarks(oEdit.Value, plant);

                    break;

                case "CHK_ISMO":
					CHK_ISMO(oForm);
					break;
			}

			//NSC_DI.UTIL.Misc.KillObject(oForm);
            //NSC_DI.UTIL.Misc.KillObject(oEdit);
            GC.Collect();
		}

		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm, string pPlantID, string pPlantPhase = null)//, string pItemCode = null
        {
            Folder oTAB_WATER = null;

            try
			{
				// Freeze the Form UI
				pForm.Freeze(true);

                // Hide the task for now until we have Greenbooks working

                pForm.Items.Item("BTN_TASK").Enabled = false;

                pForm.Items.Item("TAB_TASKS").Enabled = false;
                string itemCode = NSC_DI.SAP.Items.GetItemCode(pPlantID);
                string managedType = NSC_DI.SAP.Items.ManagedBy(itemCode);
                string SQL_Query_PlantDetails = null;
                //if (((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG")) ||( NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && pPlantPhase == "FLO"))
                if(managedType.ToUpper() == "S")
                {
                    SQL_Query_PlantDetails = @"
SELECT
[OWHS].[WhsName] AS [Warehouse]
,[OSRN].[CreateDate] AS [DateCreated]
,[OSRN].[LotNumber] AS [Lot]
,[OSRN].[itemName]
,ISNULL((
    SELECT 
    TOP 1 
    ISNULL([@NSC_WATERLOG].[U_DateCreated],'') 
    FROM 
    [@NSC_WATERLOG] 
    WHERE 
    [@NSC_WATERLOG].[U_PlantID] = (CONVERT(nvarchar,[OSRN].[ItemCode])+'-'+CONVERT(nvarchar,[OSRN].[DistNumber])
  ) 
),'12/30/1899 12:00:00 AM')  AS [DateLastWatered]
,ISNULL( (
    SELECT 
    TOP 1 
    ISNULL([@NSC_FEEDLOG].[U_DateCreated],'') 
    FROM 
    [@NSC_FEEDLOG] 
    WHERE 
    [@NSC_FEEDLOG].[U_PlantID] = (CONVERT(nvarchar,[OSRN].[ItemCode])+'-'+CONVERT(nvarchar,[OSRN].[SysNumber])
  ) 
),'12/30/1899 12:00:00 AM')  AS [DateLastFeed]
,[OSRN].[U_NSC_IsMother]";
                    if (pPlantPhase != null)
                    {
                        SQL_Query_PlantDetails += ", '" + pPlantPhase + "' as [State] ";
                    }

                    SQL_Query_PlantDetails += @"
FROM [OSRN]
JOIN [OWHS]
ON [OWHS].[WhsCode] = (SELECT [OSRI].[WhsCode] FROM [OSRI] WHERE [OSRI].[ItemCode] = [OSRN].[ItemCode] AND [OSRI].[SysSerial] = [OSRN].[SysNumber])
WHERE
(CONVERT(nvarchar,[OSRN].[DistNumber]))  = '" + pPlantID.Replace("'", "''") + @"'";

                }
                else
                {

                    SQL_Query_PlantDetails = @"SELECT  [OWHS].[WhsName] AS[Warehouse] 
,[OBTN].[CreateDate] AS[DateCreated]
,[OBTN].[LotNumber] AS[Lot]
,[OBTN].[itemName]
,ISNULL((
    SELECT
    TOP 1 
    ISNULL([@NSC_WATERLOG].[U_DateCreated],'')
    FROM
    [@NSC_WATERLOG]
    WHERE
    [@NSC_WATERLOG].[U_PlantID] = (CONVERT(nvarchar,[OBTN].[ItemCode])+'-'+CONVERT(nvarchar, [OBTN].[DistNumber])
  ) 
),'12/30/1899 12:00:00 AM')  AS[DateLastWatered]
,ISNULL((
    SELECT
    TOP 1 
    ISNULL([@NSC_FEEDLOG].[U_DateCreated],'')
    FROM
    [@NSC_FEEDLOG]
    WHERE
    [@NSC_FEEDLOG].[U_PlantID] = (CONVERT(nvarchar,[OBTN].[ItemCode])+'-'+CONVERT(nvarchar, [OBTN].[SysNumber])
  ) 
),'12/30/1899 12:00:00 AM')  AS[DateLastFeed]
,[OBTN].[U_NSC_IsMother] ";
                    if (pPlantPhase != null)
                    {
                        SQL_Query_PlantDetails += ", '" + pPlantPhase + "' as [State] ";
                    }

                        SQL_Query_PlantDetails += @"FROM[OBTN]
JOIN[OWHS]
ON[OWHS].[WhsCode] = (SELECT[OIBT].[WhsCode] FROM[OIBT] join [OWHS] on [OIBT].[WhsCode]  = [OWHS].WhsCode WHERE [OWHS].[U_NSC_WhrsType]= '"+ pPlantPhase + @"' and [OIBT].[ItemCode] = [OBTN].[ItemCode] AND[OIBT].SysNumber = [OBTN].[SysNumber])
WHERE 
 (CONVERT(nvarchar,[OBTN].[DistNumber]))  ='" + pPlantID.Replace("'", "''") + @"'";

                }

                    var FieldsReturnedFromSQL = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(SQL_Query_PlantDetails);

				// Validate that we actual have results to return. We should since this it only accessed when data does exist.
				if (FieldsReturnedFromSQL == null)
				{
					Globals.oApp.StatusBar.SetText("Failed to load data into the plant details!");
					return;
				}

				var TXT_ITEM	= pForm.Items.Item("TXT_ITEM").Specific;
				var TXT_CRTD	= pForm.Items.Item("TXT_CRTD").Specific;
				var TXT_FEED	= pForm.Items.Item("TXT_FEED").Specific;
				var TXT_WATER	= pForm.Items.Item("TXT_WATER").Specific;
				var TXT_LOT		= pForm.Items.Item("TXT_LOT").Specific;
				var TXT_WHSE	= pForm.Items.Item("TXT_WHSE").Specific;
				var TXT_STRN	= pForm.Items.Item("TXT_STRN").Specific;
                var TXT_REM     = pForm.Items.Item("TXT_REM").Specific;
                var TXT_STATE   = pForm.Items.Item("TXT_STATE").Specific;

                TXT_ITEM.Value = pPlantID;
				TXT_WHSE.Value = FieldsReturnedFromSQL.Item(0).Value.ToString();
				TXT_CRTD.Value = FieldsReturnedFromSQL.Item(1).Value.ToString();
				TXT_LOT.Value  = FieldsReturnedFromSQL.Item(2).Value.ToString();
				TXT_STRN.Value = FieldsReturnedFromSQL.Item(3).Value.ToString();
                var IsMother   = FieldsReturnedFromSQL.Item(6).Value.ToString();
                var lastDateWatered=FieldsReturnedFromSQL.Item(4).Value.ToString();
                var lastFeed   = FieldsReturnedFromSQL.Item(5).Value.ToString();
                if(pPlantPhase != null)
                    TXT_STATE.Value = FieldsReturnedFromSQL.Item(7).Value.ToString();

                //Add Remarks
                string Remarks = NSC_DI.UTIL.SQL.GetValue<string>(@"Select U_Note from [@NSC_PLANTJOURNAL] WHERE [U_PlantID] = '" + pPlantID + "' and U_EntryType = 'Remarks'");
                TXT_REM.Value = Remarks;



                // Clear SAP's default date created for NULL dates
                
                try
                {
                    // Grab the dates returned from the SQL query
                    lastDateWatered = lastDateWatered.Replace("12/30/1899 12:00:00 AM", "");
                    lastFeed = lastFeed.Replace("12/30/1899 12:00:00 AM", "");                   
                   
                }
                catch (Exception ex)
                {
                    //They do not exist so no need to do anything just keep on moving through the process. 
                }
                finally
                {
                    // Set the value in the textboxes
                    TXT_WATER.Value = lastDateWatered;
                    TXT_FEED.Value = lastFeed;
                }


				Load_TabCount(pForm);

                // Select the first tab by default
                //**** The line below was causing application to crash as it could not reslove itself in the GetField<T> in the Form.cs class
                //pForm.Items.Item("TAB_WATER").Specific.Select();

                pForm.Items.Item("BTN_H2O").Enabled = true;
				pForm.Items.Item("BTN_FEED").Enabled = true;
				pForm.Items.Item("BTN_TREAT").Enabled = true;
				pForm.Items.Item("BTN_PRUNE").Enabled = true;

				
                
                CommonUI.CheckBox.SetValue(pForm, "CHK_ISMO", IsMother.ToString());

                oTAB_WATER = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_WATER", pForm);
                oTAB_WATER.Select();
            }
            catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
                pForm.VisibleEx = true;
                pForm.Freeze(false);
                //NSC_DI.UTIL.Misc.KillObject(oTAB_WATER);
                GC.Collect();

			}
		}

        public static Form FormCreate(string pPlantID, bool pSingleInstance = false, string pCallingFormUID = null, string pPlantPhase = null)//, string pItemCode = null
        {
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm, pPlantID, pPlantPhase);// pItemCode,
                return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
		}
		}

		public static void Load_Matrix(Form pForm, string LogType)
        {
			EditText TXT_ITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEM", pForm);

            switch (LogType)
            {

                case "WATER":

                    NavSol.CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tWaterLog, "mtx_plan", new List<Matrix.MatrixColumn>() {
                                            new Matrix.MatrixColumn() { ColumnName = "Code", Caption = "ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_DateCreated", Caption = "Date Created", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_Details", Caption = "Journal Entry", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_PHLEVEL", Caption = "PH Level", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_PPM", Caption = "PPM(s)", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }

                        },
@"Select T0.Code, T0.U_DateCreated, 'Watered ' + FORMAT(T0.U_Amount, 'N6', 'en-us') + ' ' + T0.U_Measure as U_Details, T0.U_PHLEVEL, T0.U_PPM
 from [@" + NSC_DI.Globals.tWaterLog + "] T0 WHERE T0.[U_PlantID] = '" + TXT_ITEM.Value + "'");
                    break;

                case "FEED":
                    NavSol.CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tFeedLog, "mtx_plan", new List<Matrix.MatrixColumn>() {
                                            new Matrix.MatrixColumn() { ColumnName = "Code", Caption = "ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_DateCreated", Caption = "Date Created", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "ItemCode", Caption = "Journal Entry", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_PH", Caption = "PH Level", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_ADPPM", Caption = "PPM(s)", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_TEMP", Caption = "Temp", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            //,new Matrix.MatrixColumn() { ColumnName = "U_ADPPM", Caption = "TDS for Additive", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_ADTDS", Caption = "Additive Temp", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_ADEC", Caption = "EC for Additive", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                    },
      @"Select T0.Code, T0.U_DateCreated, 'Fed ' + FORMAT(T0.U_Amount, 'N6', 'en-us') + ' ' + T1.[ItemName] as ItemCode, T0.U_PH, T0.[U_TEMP],
T0.[U_ADPPM], T0.[U_ADTDS], T0.[U_ADEC]
 from [@" + NSC_DI.Globals.tFeedLog + "] T0 Join OITM T1 on T0.U_ItemCode = T1.ItemCode WHERE T0.[U_PlantID] = '" + TXT_ITEM.Value + "'");
                    break;
                case "WASTE":
                    string strSQL = @"Select * from [@" + NSC_DI.Globals.tPlantJournal + "] WHERE [U_PlantID] = '" + TXT_ITEM.Value + "' and U_EntryType='Waste'";

                    NavSol.CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tPlantJournal, "mtx_plan", new List<Matrix.MatrixColumn>() {
                                            new Matrix.MatrixColumn() { ColumnName = "Code", Caption = "ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_DateCreated", Caption = "Date Created", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_Note", Caption = "Journal Entry", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                    }, strSQL);

                    break;
                case "TRANSFER":
                    string strSQL1 = @"Select * from [@" + NSC_DI.Globals.tPlantJournal + "] WHERE [U_PlantID] = '" + TXT_ITEM.Value + "' and U_EntryType='Transfer'";

                    NavSol.CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tPlantJournal, "mtx_plan", new List<Matrix.MatrixColumn>() {
                                            new Matrix.MatrixColumn() { ColumnName = "Code", Caption = "ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_DateCreated", Caption = "Date Created", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new Matrix.MatrixColumn() { ColumnName = "U_Note", Caption = "Journal Entry", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                    }, strSQL1);
                    break;
                case "TREAT":
                    NavSol.CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tTreatLog, "mtx_plan", new List<NavSol.CommonUI.Matrix.MatrixColumn>() {
                                            new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "Code", Caption = "ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_DateCreated", Caption = "Date Created", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_Details", Caption = "Journal Entry", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_PSPPM", Caption = "PPM(s)", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_PSTDS", Caption = "TDS", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                                            ,new NavSol.CommonUI.Matrix.MatrixColumn() { ColumnName = "U_PSEC", Caption = "EC", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }

                        },
     @"Select T0.Code, T0.U_DateCreated, 'Treated ' + FORMAT(T0.U_Amount, 'N6', 'en-us') + ' of ' + OITM.ItemName as U_Details, T0.U_PSPPM, T0.U_PSTDS,T0.U_PSEC
 from [@" + NSC_DI.Globals.tTreatLog + "] T0  join OITM on T0.U_ItemCode = OITM.ItemCode WHERE T0.[U_PlantID] = '" + TXT_ITEM.Value + "'");

                    break;
            }
        }


		public static void Load_TabCount(Form pForm)
        {
			Folder TAB_WATER =  CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_WATER", pForm);
			Folder TAB_FEED =  CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_FEED", pForm);
			Folder TAB_TREAT =  CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_TREAT", pForm);
			Folder TAB_WASTE =  CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_WASTE", pForm);
			Folder TAB_XFER =  CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_XFER", pForm);

			EditText TXT_ITEM =  CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEM", pForm);

            // Prepare to run a SQL statement.
            SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            // Count how many current records exist within the database.
            oRecordSet.DoQuery(@"
SELECT
(SELECT COUNT(*) FROM [@NSC_PLANTJOURNAL] WHERE [U_EntryType] = 'Water'  AND [U_PlantID] = '" + TXT_ITEM.Value + @"')
,(SELECT COUNT(*) FROM [@NSC_PLANTJOURNAL] WHERE [U_EntryType] = 'Feed'  AND [U_PlantID] = '" + TXT_ITEM.Value + @"')
,(SELECT COUNT(*) FROM [@NSC_PLANTJOURNAL] WHERE [U_EntryType] = 'Treat'  AND [U_PlantID] = '" + TXT_ITEM.Value + @"')
,(SELECT COUNT(*) FROM [@NSC_PLANTJOURNAL] WHERE [U_EntryType] = 'Waste'  AND [U_PlantID] = '" + TXT_ITEM.Value + @"')
,(SELECT COUNT(*) FROM [@NSC_PLANTJOURNAL] WHERE [U_EntryType] = 'Transfer'  AND [U_PlantID] = '" + TXT_ITEM.Value + "')");

            TAB_WATER.Caption = "Water (" + oRecordSet.Fields.Item(0).Value.ToString() + ")";
            TAB_FEED.Caption = "Feed (" + oRecordSet.Fields.Item(1).Value.ToString() + ")";
            TAB_TREAT.Caption = "Treat (" + oRecordSet.Fields.Item(2).Value.ToString() + ")";
            TAB_WASTE.Caption = "Waste (" + oRecordSet.Fields.Item(3).Value.ToString() + ")";
            TAB_XFER.Caption = "Transfer (" + oRecordSet.Fields.Item(4).Value.ToString() + ")";
        }
        private static void SaveRemarks(string pRemarks, string pPlant)
        {
            string msg = "Remarks Saved";
            try
            {
                // Create plant journal entries for remarks           
                NSC_DI.SAP.PlantJournal.AddPlantJournalEntry(pPlant, NSC_DI.Globals.JournalEntryType.Remarks,
                    pRemarks);
            
            }
            catch
            {
                msg = "Failed To Save Remarks";
            }
            finally
            {

            }
            Globals.oApp.MessageBox(msg);

        }

		private static void CHK_ISMO(Form pForm)
		{
			try
			{
                //Note: if CHK_ISMO.Checked is set to 'true' that fires a fake et_ITEM_PRESSED event. Notably on form load where the plant is a mother...
                // Setting a already by default false CheckBox to false does not fire the event, EG if the .Checked did not change, no event is fired.
                // The function below is FUBAR and will cause a crash
                //string New_State = CommonUI.Forms.GetField<string>(pForm, "CHK_ISMO");
                CheckBox CHK_ISMO = pForm.Items.Item("CHK_ISMO").Specific;
                string New_State=null;
                double dblJDate;
                if (CHK_ISMO.Checked==true)
                {
                    New_State = "Y";
                    dblJDate = DateTime.Today.ToOADate() + 24105018.5;
                }
                else
                {
                    New_State = "N";
                }
                
                var plantDistNo = CommonUI.Forms.GetField<string>(pForm, "TXT_ITEM");

                var itemCode = NSC_DI.SAP.Items.SNs.GetItemCode(plantDistNo);

                NSC_DI.SAP.Plant.ChangeMotherStatus(itemCode, plantDistNo, New_State);
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
			}

		}

	}
}
