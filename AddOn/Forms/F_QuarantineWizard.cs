﻿using System;
using System.Collections.Generic;
using System.Linq;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using Matrix = NavSol.CommonUI.Matrix;

namespace NavSol.Forms
{
	internal class F_QuarantineWizard : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_QUARANTINE_WIZ";
        private static string CurrentPhase = null;

        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_KEY_DOWN
        [B1Listener(BoEventTypes.et_KEY_DOWN, true, new string[] { cFormID })]
        public virtual bool OnBeforeKeyDown(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

           if (pVal.ItemUID == "TXT_LOOKUP" && (pVal.CharPressed == 13 || pVal.CharPressed == 9))
            {
                BTN_LOOKUP_ITEM_PRESSED(oForm);

            }


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
            return BubbleEvent;
        }

        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "btn_ok":
					BTN_OK_Click(oForm);
					break;
                case "BTN_LOOKUP":
                    BTN_LOOKUP_ITEM_PRESSED(oForm);
                    break;
                case "BTN_REMV":
                    BTN_RMV_Click(oForm);
                    break;
            }

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_COMBO_SELECT
		[B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] {cFormID})]
		public virtual void OnAfterComboSelect(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			if (pVal.ItemUID == "CMB_QTYPE") CMB_QTYPE_Selected(oForm);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (ChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        [B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
        public virtual void OnAfterFormResize(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            FormReSize(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

        }

        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        private static void FormSetup(Form pForm)
		{
			try
			{
				CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_Main", @"quarantine-icon.bmp");

                // set the Quarantine Type Valid Values
                NavSol.CommonUI.ComboBox.ComboBoxAdd_UDT_CN(pForm, "CMB_QTYPE", NSC_DI.Globals.tQuarantineType);  
            }
            catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
			}
		}

		public static Form FormCreate(Dictionary<string, string> pListOfPlantIDsToLoad = null, bool pSingleInstance = false, string pCallingFormUID = null, string pCurrentPhase = null)
		{
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);

                CurrentPhase = pCurrentPhase;

                Form_Load(oForm, pListOfPlantIDsToLoad,pCurrentPhase);

				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

        private static void Form_Load(Form pForm, Dictionary<string, string> pListOfPlantIDsToLoad, string pCurrentPhase = null)
        {
            try
            {
                pForm.Freeze(true);

                // Prepare the SQL statement that will load the selected plants data into the matrix
                string sql = null;
                if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && pCurrentPhase == "FLO"))
                {

                    sql = $"SELECT N.DistNumber, I.WhsCode, N.CreateDate, N.itemName,N.MnfSerial FROM OSRN AS N LEFT JOIN OSRI AS I ON N.ItemCode = I.ItemCode AND N.SysNumber = I.SysSerial";

                    // add selection criteria if any
                    if (pListOfPlantIDsToLoad?.Any() == true)
                    {
                        var fmtIDs = pListOfPlantIDsToLoad.Keys.Select(n => "'" + n + "'");

                        sql += $" WHERE DistNumber IN ({string.Join(",", fmtIDs)})";
                    }

                    var matrixColumns = new List<Matrix.MatrixColumn>()
                {
                    new Matrix.MatrixColumn() {ColumnName = "DistNumber", Caption = "ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                    new Matrix.MatrixColumn() {ColumnName = "itemName", Caption = "Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                    new Matrix.MatrixColumn() {ColumnName = "WhsCode", Caption = "Warehouse ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                    new Matrix.MatrixColumn() {ColumnName = "CreateDate", Caption = "Date Created", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                    new Matrix.MatrixColumn() {ColumnName = "MnfSerial", Caption = "State Id", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}
                };

                    Matrix.LoadDatabaseDataIntoMatrix(pForm, "OSRN", "mtx_plan", matrixColumns, sql);

                }
                else
                {

                    sql = $"SELECT N.DistNumber, I.WhsCode, N.CreateDate, N.itemName,I.Quantity, N.MnfSerial FROM OBTN AS N LEFT JOIN OIBT AS I ON N.ItemCode = I.ItemCode AND I.BatchNum= N.DistNumber  join OWHS on I.WhsCode = [OWHS].WhsCode where ";

                    var where = "(";
                    // add selection criteria if any
                    if (pListOfPlantIDsToLoad == null)
                    {
                        //this is hit when the form os called from the menu and has to load with no data. 
                        where += " [OWHS].U_NSC_WhrsType = 'AlphaAndOmega' )";
                    }
                    else
                    {
                        foreach (var row in pListOfPlantIDsToLoad)
                        {
                            if (where == "(")
                            {
                                where += $"DistNumber = '{row.Key}' AND  I.WhsCode = '{row.Value}' ";
                            }
                            else
                            {
                                where += ((where == "") ? "AND (" : ") OR (");
                                where += $"DistNumber = '{row.Key}' AND  I.WhsCode = '{row.Value}' ";
                            }
                        }
                        where += ((where == "") ? "" : ")");
                    }

                    sql += where;

                    var matrixColumns = new List<Matrix.MatrixColumn>()
                        {
                            new Matrix.MatrixColumn() {ColumnName = "DistNumber", Caption = "ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                            new Matrix.MatrixColumn() {ColumnName = "itemName", Caption = "Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                            new Matrix.MatrixColumn() {ColumnName = "Quantity", Caption = "Quantity", ColumnWidth = 80, IsEditable =true, ItemType = BoFormItemTypes.it_EDIT},
                            new Matrix.MatrixColumn() {ColumnName = "WhsCode", Caption = "Warehouse ID", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                            new Matrix.MatrixColumn() {ColumnName = "CreateDate", Caption = "Date Created", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                            new Matrix.MatrixColumn() {ColumnName = "MnfSerial", Caption = "State Id", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}
                        };
                    Matrix.LoadDatabaseDataIntoMatrix(pForm, "OBTN", "mtx_plan", matrixColumns, sql);
                }

                // Show the form
                pForm.VisibleEx = true;                
                pForm.Items.Item("mtx_plan").Specific.SelectionMode = BoMatrixSelect.ms_Auto;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
            }
        }

        public static Form Form_Load()
        {
            string pCallingFormUID = null;
            bool pSingleInstance = false;
            string pCurrentPhase = null;

            Item oItm = null;
            try
            {
                var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
                {
                    oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
                    oItm.Top = -20;
                    oItm.Specific.Value = pCallingFormUID;
                }

                FormSetup(oForm);

                CurrentPhase = pCurrentPhase;

                Form_Load(oForm, null, pCurrentPhase);
                oForm.Freeze(true);

                SAPbouiCOM.Matrix MX = oForm.Items.Item("mtx_plan").Specific;
                MX.Clear();
                //Do a row check to make sure it is emply
                DataTable DataTable = null;
                if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y")
                {
                    DataTable = oForm.DataSources.DataTables.Item("OSRN");

                }
                else
                {
                    DataTable = oForm.DataSources.DataTables.Item("OBTN");
                }
                DataTable.Rows.Clear();
                MX.LoadFromDataSource();
                
                oForm.Freeze(false);

                return oForm;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }
        private void BTN_LOOKUP_ITEM_PRESSED(Form pForm,int Clearout=0)
        {

            pForm.Freeze(true);
            try
            {
                // Grad the textbox for the barcode scanner from the form UI
                EditText TXT_LOOKUP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", pForm);

                // If nothing was passed in the textbox, just return
                if (TXT_LOOKUP.Value.Length == 0)
                {
                    Globals.oApp.StatusBar.SetText("No Data To Scan! - Please Enter A Valid Value.");
                    return;
                }

                // Grab the matrix from the form UI
                SAPbouiCOM.Matrix MTX_PLANTS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "mtx_plan", pForm);
                SAPbouiCOM.EditText TXTID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", pForm);

                string sql = null;
                string ID = TXTID.Value;

                if (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" )
                {
                    //For Serialized plants

                    sql = $"SELECT N.DistNumber as [DistNumber], I.WhsCode  as [WhsCode], N.CreateDate as [CreateDate], N.itemName as [itemName],N.MnfSerial as [MnfSerial] FROM OSRN AS N LEFT JOIN OSRI AS I ON N.ItemCode = I.ItemCode AND N.SysNumber = I.SysSerial";
                    sql += $" WHERE DistNumber='" + ID + "' OR [MnfSerial]='" + ID + "'";

                   Recordset RS =  NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(sql);

                    if (RS.RecordCount > 1|| RS.RecordCount==0)
                    {
                        
                        Globals.oApp.StatusBar.SetText("Your unique key returned more than one value or is not a unique key. Please check your Inventory");
                        return;

                    }
                    else
                    {
                        RS.MoveFirst();
                        // Getting a DataTable of the DataSource.
                        DataTable DataTable = pForm.DataSources.DataTables.Item("OSRN");
                        DataTable.Rows.Add(1);
                        DataTable.SetValue("DistNumber", DataTable.Rows.Count - 1, RS.Fields.Item("DistNumber").Value);
                        DataTable.SetValue("itemName", DataTable.Rows.Count - 1, RS.Fields.Item("itemName").Value);
                        DataTable.SetValue("WhsCode", DataTable.Rows.Count - 1, RS.Fields.Item("WhsCode").Value);
                        DataTable.SetValue("CreateDate", DataTable.Rows.Count - 1, RS.Fields.Item("CreateDate").Value);
                        DataTable.SetValue("MnfSerial", DataTable.Rows.Count - 1, RS.Fields.Item("MnfSerial").Value);

                       
                        MTX_PLANTS.LoadFromDataSource();
                    }
                }
                else
                {
                    // For Gen AG
                    sql = $"SELECT N.DistNumber, I.WhsCode,I.[Quantity], N.CreateDate, N.itemName, N.MnfSerial FROM OBTN AS N LEFT JOIN OBTQ AS I ON N.ItemCode = I.ItemCode AND N.SysNumber = I.SysNumber join OWHS on I.WhsCode = [OWHS].WhsCode";
                    sql += $" WHERE (DistNumber='" + ID + "' OR [MnfSerial]='" + ID + "') and [OWHS].U_NSC_WhrsType != 'QND' ";

                    Recordset RS = NSC_DI.UTIL.SQL.GetRecordSetFromSQLQuery(sql);

                    if (RS.RecordCount > 1 || RS.RecordCount == 0)
                    {
                        if (RS.RecordCount == 0)
                        {
                            Globals.oApp.StatusBar.SetText("Your unique key not a unique key. Please check your Inventory");
                            return;
                        }
                        else
                        {
                            //Get Warehouse from user
                            Globals.oApp.StatusBar.SetText("More than one batch is active.");
                            return;

                            int WhrseCode = 0;
                            //Adjust query 
                            sql = "";
                            sql = $"SELECT N.DistNumber, I.WhsCode,I.[Quantity], N.CreateDate, N.itemName, N.MnfSerial FROM OBTN AS N LEFT JOIN OBTQ AS I ON N.ItemCode = I.ItemCode AND N.SysNumber = I.SysNumber";
                            sql += $" WHERE (DistNumber='" + ID + "' OR [MnfSerial]='" + ID + "') and WhsCode=" + WhrseCode;

                            //// Getting a DataTable of the DataSource.
                            //DataTable DataTable = pForm.DataSources.DataTables.Item("OBTN");
                            //DataTable.Rows.Add(1);
                            //DataTable.SetValue("DistNumber", DataTable.Rows.Count - 1, RS.Fields.Item("DistNumber").Value);
                            //DataTable.SetValue("itemName", DataTable.Rows.Count - 1, RS.Fields.Item("itemName").Value);
                            //DataTable.SetValue("Quantity", DataTable.Rows.Count - 1, RS.Fields.Item("Quantity").Value);
                            //DataTable.SetValue("WhsCode", DataTable.Rows.Count - 1, RS.Fields.Item("WhsCode").Value);
                            //DataTable.SetValue("CreateDate", DataTable.Rows.Count - 1, RS.Fields.Item("CreateDate").Value);
                            //DataTable.SetValue("MnfSerial", DataTable.Rows.Count - 1, RS.Fields.Item("MnfSerial").Value);

                            //MTX_PLANTS.LoadFromDataSource();

                        }

                    }
                    else
                    {
                        if (RS.RecordCount == 1)
                        {
                            RS.MoveFirst();

                            // Getting a DataTable of the DataSource.
                            DataTable DataTable = pForm.DataSources.DataTables.Item("OBTN");
                            DataTable.Rows.Add(1);

                            DataTable.SetValue("DistNumber", DataTable.Rows.Count - 1, RS.Fields.Item("DistNumber").Value);
                            DataTable.SetValue("itemName", DataTable.Rows.Count - 1, RS.Fields.Item("itemName").Value);
                            DataTable.SetValue("Quantity", DataTable.Rows.Count - 1, RS.Fields.Item("Quantity").Value);
                            DataTable.SetValue("WhsCode", DataTable.Rows.Count - 1, RS.Fields.Item("WhsCode").Value);
                            DataTable.SetValue("CreateDate", DataTable.Rows.Count - 1, RS.Fields.Item("CreateDate").Value);
                            DataTable.SetValue("MnfSerial", DataTable.Rows.Count - 1, RS.Fields.Item("MnfSerial").Value);

                            MTX_PLANTS.LoadFromDataSource();
                        }
                    }
                }

                // clear and reselect barcode field so we can add more
                CommonUI.Forms.SetField(pForm, "TXT_LOOKUP", string.Empty);

                (pForm.Items.Item("TXT_LOOKUP").Specific as EditText).Active = true;

              
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
            }
        }


        private void CMB_QTYPE_Selected(Form pForm)
		{
			ComboBox oCBO = null;
			Recordset oRecordSet = null;

			try
			{
				// Find the quarantine type ComboBox in the form UI
				oCBO = pForm.Items.Item("CMB_QTYPE").Specific;

				// Keep track of the type of warehouses we will be looking for
				var SQL_Parameter_WarehouseTypeToFind = "";

                var QuarState = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_QuarState FROM [@{NSC_DI.Globals.tQuarantineType}] WHERE Code = '{oCBO.Value.ToString()}'");


                switch (QuarState)
				{
					case "SICK":
						SQL_Parameter_WarehouseTypeToFind = "QNX";
						break;

					case "DEST":
						SQL_Parameter_WarehouseTypeToFind = "QND";
						break;

					case "SALE":
						SQL_Parameter_WarehouseTypeToFind = "QNS";
						break;
				}

                if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y")
                {
                    NavSol.CommonUI.CFL.CreateWH(pForm, "CFL_WH", "CMB_WHSE");
                    NavSol.CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_NONE, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, SQL_Parameter_WarehouseTypeToFind);
                    NavSol.CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_AND, "Inactive", BoConditionOperation.co_EQUAL, "N");
                    NavSol.CommonUI.CFL.AddCon_Branches(pForm, "CFL_WH", BoConditionRelationship.cr_AND); // 10823
                    return;
                }

                // Prepare to run a SQL statement.
                oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                // Count how many current records exist within the database.
                var sql = @"SELECT [WhsCode], [WhsName] FROM [OWHS] WHERE [U_" + Globals.SAP_PartnerCode + "_" + "WhrsType] = '" +
                                   SQL_Parameter_WarehouseTypeToFind + "'";
                if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})"; // #10823

                oRecordSet.DoQuery(sql);
				// Find the Combobox of Warehouses from the Form UI
				oCBO = pForm.Items.Item("CMB_WHSE").Specific;

                // Remove all currently existing values from warehouse drop down
                if (oCBO.ValidValues.Count > 0)
				{
					for (var i = oCBO.ValidValues.Count; i-- > 0; )
					{
						oCBO.ValidValues.Remove(i, BoSearchKey.psk_Index);
					}
				}

				//if (oRecordSet.RecordCount > 1)
				//{
                    // Clear any current selections remaining from other tabs
                    oCBO.ValidValues.Add("", "");
                    oCBO.Select(0, BoSearchKey.psk_Index);
                // throws an error, but works
                try { oCBO.ValidValues.Remove(0, BoSearchKey.psk_Index); } catch { };
                //}

				oRecordSet.MoveFirst();

				// Add allowed warehouses to the drop down
				for (var i = 0; i < oRecordSet.RecordCount; i++)
				{
					oCBO.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
					oRecordSet.MoveNext();
				}

				// If a single value is present, select it
				if (oCBO.ValidValues.Count == 1) oCBO.Select(0, BoSearchKey.psk_Index);

				if (oCBO.ValidValues.Count > 1)	oCBO.Item.Enabled = true;
            }
            catch (Exception ex)
			{
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oCBO);
				NSC_DI.UTIL.Misc.KillObject(oRecordSet);
			}
		}

		private void BTN_OK_Click(Form pForm)
		{
			ProgressBar oBar		= null;
			SAPbouiCOM.Matrix		mtx_plan	= null;
			ComboBox	oCBO		= null;
			EditText	TXT_REMARK	= null;

			try
			{
				// Prepare a new progress bar
				oBar = Globals.oApp.StatusBar.CreateProgressBar("Starting to Quaratine!", 10, false);

				UpdateProgressBar(oBar, "Finding your selected plants.", 1);

				#region Populate a List of Selected Plant Id's  --------------------------------------------------------
				// Keep track of the selected Plant ID's
				var SelectedPlantIDs = new List<string>();
				var BarcodesOfPlantsToDestroy = new List<string>();
				// Keep track of the selected plants and their "from warehouse" (if transferring)
				var Dictionary_Of_PlantsandWarehouses = new Dictionary<string, string>();
                var Dictionary_Of_Plants_Batch_WarehouseAndQty = new Dictionary<string, Dictionary<string,double>>();
                bool splitQuarantine = false;

                #region Determining which Warehouse was selected  --------------------------------------------------------
                // Keep track of which warehouse was selected
                var SelectedDestinationWarehouseID = "";

                // Make sure a warehouse was selected
                try
                {
                    // Find the ComboBox in the Form UI
                    oCBO = pForm.Items.Item("CMB_WHSE").Specific;

                    // Grab the value from the ComboBox
                    var SelectedWarehouseDescription = oCBO.Value.ToString();

                    // Get the "description" from the ComboBox, where we are really storing the value
                    SelectedDestinationWarehouseID = CommonUI.ComboBox.GetComboBoxValueOrDescription(ref pForm, "CMB_WHSE", null, SelectedWarehouseDescription);
                    if (String.IsNullOrEmpty(SelectedDestinationWarehouseID))// Whitespace-Change
                        throw new Exception(); // 10823 needed in case the user cannot choose a warehouse because they dont have branch access
                }
                catch (Exception ex)
                {
                    oBar.Stop();
                    Globals.oApp.StatusBar.SetText("Please select a destination warehouse first!");
                    return;
                }
                #endregion  Determining which Warehouse was selected


                // Grab the matrix from the form UI
                mtx_plan = pForm.Items.Item("mtx_plan").Specific;

				
                if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase == "FLO"))
                {
                    // For each row already selected
                    for (var i = 1; i < (mtx_plan.RowCount + 1); i++)
                    {

                        // Grab the selected row's Plant ID column
                        var plantID = ((EditText)mtx_plan.Columns.Item(0).Cells.Item(i).Specific).Value;
                        var warehouseCode = ((EditText)mtx_plan.Columns.Item(2).Cells.Item(i).Specific).Value;
                        double Qty = 1.0;
                        var stateId = ((EditText)mtx_plan.Columns.Item(4).Cells.Item(i).Specific).Value;
                        // Add the selected row plant ID to list
                        Dictionary_Of_PlantsandWarehouses.Add(plantID, warehouseCode);

                        SelectedPlantIDs.Add(plantID);
                        BarcodesOfPlantsToDestroy.Add(stateId);
                    }

                }
                else
                {                   
                    System.Collections.Generic.List<NSC_DI.SAP.BatchItems.SubBatch.BatchRec> oBatches = new System.Collections.Generic.List<NSC_DI.SAP.BatchItems.SubBatch.BatchRec>();
                    // For each row already selected                    
                    for (var i = 1; i < (mtx_plan.RowCount + 1); i++)
                    {
                       
                        // Grab the selected row's Plant ID column
                        var plantID = ((EditText)mtx_plan.Columns.Item(0).Cells.Item(i).Specific).Value;
                        string warehouseCode = ((EditText)mtx_plan.Columns.Item(3).Cells.Item(i).Specific).Value;
                        double Qty = Convert.ToDouble(((EditText)mtx_plan.Columns.Item(2).Cells.Item(i).Specific).Value);
                        var stateId = ((EditText)mtx_plan.Columns.Item(5).Cells.Item(i).Specific).Value;
                        // Add the selected row plant ID to list
                        var dicWarehouse_Qty = new Dictionary<string, double>();
                        
                        //dicWarehouse_Qty.Add(warehouseCode, Qty);

                        //Dictionary_Of_Plants_Batch_WarehouseAndQty.Add(plantID, dicWarehouse_Qty);
                        //SelectedPlantIDs.Add(plantID);
                        //BarcodesOfPlantsToDestroy.Add(stateId);

                        // checks to see if the users wants to only quarantine a portion of the plants in inventory.
                        // if thats the case it runs the create method and then sets the splitQuarantine to true.
                        string SQLQuantity = $@"
SELECT
[OBTQ].[Quantity]
FROM [OBTQ]
JOIN [OBTN] ON [OBTN].[ItemCode] = [OBTQ].[ItemCode] AND [OBTN].[SysNumber] = [OBTQ].[SysNumber]
where obtn.DistNumber = '{plantID}' and obtq.WhsCode = '{warehouseCode}'";
                        float DBQuantity = float.Parse(NSC_DI.UTIL.SQL.GetValue<string>(SQLQuantity));
                        float matrixQuantity = float.Parse(((EditText)mtx_plan.Columns.Item(2).Cells.Item(i).Specific).Value);
                        if (DBQuantity > matrixQuantity)
                        {
                            NSC_DI.SAP.BatchItems.SubBatch.BatchRec batRec = new NSC_DI.SAP.BatchItems.SubBatch.BatchRec
                            {
                                Item = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT ItemCode FROM OBTN WHERE DistNumber = '{plantID}'"),
                                FromWarehouse = warehouseCode,
                                ToWarehouse = SelectedDestinationWarehouseID,
                                Quantity = matrixQuantity,
                                Bin = 0,
                                OldBatch = plantID,
                                NewBatch = ""
                            };
                            oBatches.Add(batRec);
                            splitQuarantine = true;
                        }
                        else
                        {
                            dicWarehouse_Qty.Add(warehouseCode, Qty);
                            Dictionary_Of_Plants_Batch_WarehouseAndQty.Add(plantID, dicWarehouse_Qty);
                            SelectedPlantIDs.Add(plantID);
                            BarcodesOfPlantsToDestroy.Add(stateId);
                        }
                    }
                    if(oBatches.Count > 0)
                        NSC_DI.SAP.BatchItems.SubBatch.Create(oBatches, "Inventory_split");
                    //if (SelectedPlantIDs.Count == 0 && splitQuarantine)
                    //{
                    //    Globals.oApp.StatusBar.SetText("Quarantine successful, sub-batch created for quarantined plants");
                    //    return;
                    //}                        
                }

                UpdateProgressBar(oBar, "Found your selected plants.", 1);

                // Make sure some plants were selected
                if (SelectedPlantIDs.Count == 0 && !splitQuarantine) // added splitQuarantine bool so that if the user selects partial quarantine then it continues throught he method.
				{
					oBar.Stop();
					Globals.oApp.StatusBar.SetText("Please select some plants first!");
					return;
				}
				#endregion

				UpdateProgressBar(oBar, "Validated your selected plants.", 1);



				UpdateProgressBar(oBar, "Discovered your selected destination warehouse.", 1);

				#region Determine Which type of Quaratine is being Applied => variable: QuarantineType  --------------------------------------------------------
				// Find the quarantine type ComboBox in the form UI
				oCBO = pForm.Items.Item("CMB_QTYPE").Specific;
                string remarks = pForm.Items.Item("TXT_REMARK").Specific.Value;

                var QuarantineType = oCBO.Value.ToString();

				//var selectedState = NSC_DI.UTIL.Misc.GetQuarantineStateFromString(QuarantineType);
                var selectedState = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_QuarState FROM [@{NSC_DI.Globals.tQuarantineType}] WHERE Code = '{QuarantineType}'");
                var quarName = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Name FROM [@{NSC_DI.Globals.tQuarantineType}] WHERE Code = '{QuarantineType}'");

                #endregion

                UpdateProgressBar(oBar, "Determined destination plant state.", 1);

				UpdateProgressBar(oBar, "Preparing to move plants to new warehouse.", 1);

                if ((NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "VEG") || (NSC_DI.UTIL.Settings.Value.Get("Serialize Plants") == "Y" && NSC_DI.UTIL.Settings.Value.Get("Serialize Stage") == "FLO" && CurrentPhase == "FLO"))
                {
                    //****Serial****

                    // Group stock transfers by the "From Warehouse" fields
                    var newDic = Dictionary_Of_PlantsandWarehouses.GroupBy(t => t.Value).ToDictionary(t => t.Key, t => t.Select(r => r.Key).ToList());

                    // Foreach list of plants within a "From Warehouse"
                    foreach (var fromwarehouse in newDic.Keys)
                    {
                        UpdateProgressBar(oBar, "Moving plants from warehouse #" + fromwarehouse);

                        var fromWhName = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT WhsName from OWHS where WhsCode = '{fromwarehouse}'");

                        // Transfer plants
                        NSC_DI.SAP.Plant.TransferPlants(newDic[fromwarehouse], SelectedDestinationWarehouseID, fromWhName, null, null, quarName, remarks);//pForm.Items.Item("TXT_REMARK").Specific;

                        UpdateProgressBar(oBar, "Finished moving plants from warehouse #" + fromWhName);

                        foreach (var plantID in newDic[fromwarehouse])
                        {
                            var ItemCode = NSC_DI.SAP.Items.SNs.GetItemCode(plantID);

                            var SysNumber = NSC_DI.SAP.Items.SNs.GetSysNumber(plantID);

                            NSC_DI.SAP.Plant.ChangeQuarantineStatus(ItemCode, SysNumber, selectedState);
                        }
                    }
                }
                else
                {
                    //****Batch****
                    // Group stock transfers by the "From Warehouse" fields
                    //var newDic = Dictionary_Of_Plants_Batch_WarehouseAndQty.GroupBy(t => t.Value).ToDictionary(t => t.Key, t => t.Select(r => r.Key).ToList());

                    // Foreach list of plants within a "From Warehouse"
                    if(Dictionary_Of_Plants_Batch_WarehouseAndQty.Keys.Count>0)
                    {
                        UpdateProgressBar(oBar, "Moving plants from warehouse #" + Dictionary_Of_Plants_Batch_WarehouseAndQty.Select(t=> t.Value).ToString());
                        // Transfer plants
                        NSC_DI.SAP.Plant.TransferPlantsBatch(Dictionary_Of_Plants_Batch_WarehouseAndQty, SelectedDestinationWarehouseID);

                        UpdateProgressBar(oBar, "Finished moving plants! ");
                    }
                    //foreach (var fromwarehouse in newDic.Keys)
                    //{
                    //    UpdateProgressBar(oBar, "Moving plants from warehouse #" + fromwarehouse);

                    //    var fromWhName = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT WhsName from OWHS where WhsCode = '{fromwarehouse}'");

                    //    // Transfer plants
                    //    NSC_DI.SAP.Plant.TransferPlantsBatch(newDic[fromwarehouse], SelectedDestinationWarehouseID, fromWhName);

                    //    UpdateProgressBar(oBar, "Finished moving plants from warehouse #" + fromWhName);

                    //    foreach (var plantID in newDic[fromwarehouse])
                    //    {
                    //        var ItemCode = NSC_DI.SAP.Items.SNs.GetItemCode(plantID);

                    //        var SysNumber = NSC_DI.SAP.Items.SNs.GetSysNumber(plantID);

                    //        NSC_DI.SAP.Plant.ChangeQuarantineStatus(ItemCode, SysNumber, selectedState);
                    //    }
                    //}
                }

				if (selectedState == NSC_DI.Globals.QuarantineStates.DEST.ToString())
				{
					// I'm assuming that we are not caring if they specify this or not.
					TXT_REMARK = pForm.Items.Item("TXT_REMARK").Specific;

					var reasonForDestruction = "Reason not specified";
					if (!string.IsNullOrEmpty(TXT_REMARK.Value))
					{
						reasonForDestruction = TXT_REMARK.Value.ToString();
					}
				}

					UpdateProgressBar(oBar, "Finished moving all plants.", 1);
					oBar.Stop();

				pForm.Close();
			}
			catch (Exception ex)
			{
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oBar);
				NSC_DI.UTIL.Misc.KillObject(mtx_plan);
				NSC_DI.UTIL.Misc.KillObject(oCBO);
				NSC_DI.UTIL.Misc.KillObject(TXT_REMARK);
				GC.Collect();
			}
		}

		private void UpdateProgressBar(ProgressBar oBar, string Message, int ProgressToIncreaseBy = 0)
		{
			try
			{
				// Set progress bar message
				oBar.Text = Message;

				// If we are increasing the progress bar size
				if (ProgressToIncreaseBy > 0)
				{
					// Calculate the new progress
					var NewProgressBarValue = oBar.Value + ProgressToIncreaseBy;

					// Make sure our new progress amount is not greater than the maxium allowed
					if (NewProgressBarValue <= oBar.Maximum)
					{
						oBar.Value = NewProgressBarValue;
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
		}

        private void CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            if (pVal.SelectedObjects == null) return;

            try
            {
                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_WH":
                        CommonUI.CFL.SetFieldsWH(pForm, pVal, "CMB_WHSE", "txtWH_CFL");
                        break;

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void FormReSize(Form pForm)
        {
            try
            {
                pForm.Items.Item("TXT_REMARK").Left = pForm.Items.Item("LBL_REMARK").Left;
                pForm.Items.Item("TXT_REMARK").Width = pForm.ClientWidth - pForm.Items.Item("TXT_REMARK").Left - 18;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(MTX_MAIN);
                GC.Collect();
            }
        }

        private void BTN_RMV_Click(Form pForm)
        {
            SAPbouiCOM.Matrix mtx_plan = null;
            try
            {
                mtx_plan = pForm.Items.Item("mtx_plan").Specific;

                for (var i = 1; i < (mtx_plan.RowCount + 1); i++)
                {
                    // If the row was selected
                    if (!mtx_plan.IsRowSelected(i)) continue;                    
                    mtx_plan.DeleteRow(i);
                    i--;
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(mtx_plan);
                GC.Collect();
            }           
        }
    }
}