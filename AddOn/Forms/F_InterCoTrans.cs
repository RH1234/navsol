﻿using System;
//using System.Runtime.InteropServices;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms
{
    public class F_InterCoTrans : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "NSC_INTER_CO_TRANS";
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "":	// Issue Button Cicked
					Globals.oApp.MessageBox("IN");
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
		[B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] {cFormID})]
		public virtual void OnAfterChooseFromList(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				// BusinessPartner ChooseFromList
				case "ddStore":
					loadRequestMx(oForm, 0);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			try
			{
				// Freeze the Form UI
				pForm.Freeze(true);

				//LoadWhrs dropdown
				loadWhrsDropDown(pForm);

				//Load TopMx
				loadRequestMx(pForm, 1);

				//Load BottomMx
				loadTransfers(pForm, 1);

				pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		private static void loadWhrsDropDown(Form pForm)
        {
            try
            {
                string strSQL = "Select WhsCode, WhsName from OWHS T0 where T0.WhsCode like 'STR%'";

				ComboBox cmbHolder = pForm.Items.Item("ddStore").Specific as ComboBox;

				CommonUI.ComboBox.ComboBoxAddVals(pForm, "ddStore", strSQL, string.Empty);

				NSC_DI.UTIL.Misc.KillObject(cmbHolder);
				GC.Collect();
            }
            catch { }

        }

		private static void loadRequestMx(Form pForm, int pFirstTimer, string strSQL = "")
        {
            if (pFirstTimer==1)
            {
                Matrix mxHolder = pForm.Items.Item("mxRequests").Specific;
                mxHolder.LoadFromDataSourceEx();
                mxHolder.AutoResizeColumns();
            }
            else
            {


                Matrix mxHolder = pForm.Items.Item("mxRequests").Specific as Matrix;
                pForm.DataSources.DataTables.Item("DTTranRq").Clear();

                ComboBox oCombo = pForm.Items.Item("ddStore").Specific as ComboBox;
                string strHolder = oCombo.Value;
                if (strHolder.Length > 0)
                {
                    strSQL = string.Format(@"Select T0.DocEntry, T0.DocNum,T6.WhsName,CASE when T0.DocStatus ='O' then 'Open' else 'Closed' End as DocStatus,T5.ItmsGrpNam,sum(T1.quantity) as Qty,T2.UomName,T0.DocDueDate
                  from OWTQ T0 inner join WTQ1 T1 on T0.DocEntry=T1.DocEntry
                  inner join OUOM T2 on T1.UomEntry = T2.UomEntry
                  inner join OITM T4 on T1.ItemCode=T4.itemCode
                  inner join OITB T5 on T4.ItmsGrpCod=T5.ItmsGrpCod
				  inner join OWHS T6 on T0.ToWhsCode=T6.WhsCode
                  where T0.U_NSC_WeedProdTrnsf='Y'  and T0.ToWhsCode='{0}'
                  group by T4.ItmsGrpCod,T0.DocEntry,T6.WhsName,T0.DocNum,T0.DocStatus,T5.ItmsGrpNam,T0.DocDueDate,T2.UomName
                  Order by T0.DocEntry", strHolder);
                }
                else
                {
                    strSQL = @"Select T0.DocEntry, T0.DocNum,T6.WhsName,CASE when T0.DocStatus ='O' then 'Open' else 'Closed' End as DocStatus,T5.ItmsGrpNam,sum(T1.quantity) as Qty,T2.UomName,T0.DocDueDate
                  from OWTQ T0 inner join WTQ1 T1 on T0.DocEntry=T1.DocEntry
                  inner join OUOM T2 on T1.UomEntry = T2.UomEntry
                  inner join OITM T4 on T1.ItemCode=T4.itemCode
                  inner join OITB T5 on T4.ItmsGrpCod=T5.ItmsGrpCod
				  inner join OWHS T6 on T0.ToWhsCode=T6.WhsCode
                  where T0.U_NSC_WeedProdTrnsf='Y'
                  group by T4.ItmsGrpCod,T0.DocEntry,T6.WhsName,T0.DocNum,T0.DocStatus,T5.ItmsGrpNam,T0.DocDueDate,T2.UomName
                  Order by T0.DocEntry";
                }
                pForm.DataSources.DataTables.Item("DTTranRq").ExecuteQuery(strSQL);
                mxHolder.LoadFromDataSourceEx();
                mxHolder.AutoResizeColumns();

				NSC_DI.UTIL.Misc.KillObject(mxHolder);
                GC.Collect();
            }
        }

		private static void loadTransfers(Form pForm, int pFirstTimer, string strSQL = "")
        {
            if (pFirstTimer == 1)
            {
                Matrix mxHolder = pForm.Items.Item("mxCompTran").Specific as Matrix;
                mxHolder.LoadFromDataSourceEx();
                mxHolder.AutoResizeColumns();
            }
            else
            {


                Matrix mxHolder = pForm.Items.Item("mxCompTran").Specific as Matrix;
                pForm.DataSources.DataTables.Item("DTTranRq").Clear();

                ComboBox oCombo = pForm.Items.Item("ddStore").Specific as ComboBox;
                string strHolder = oCombo.Value;
                if (strHolder.Length > 0)
                {
                    strSQL = string.Format(@"Select T0.DocEntry, T0.DocNum,T6.WhsName,CASE when T0.DocStatus ='O' then 'Open' else 'Closed' End as DocStatus,T5.ItmsGrpNam,sum(T1.quantity) as Qty,T2.UomName,T0.DocDueDate
                  from OWTR T0 inner join WTR1 T1 on T0.DocEntry=T1.DocEntry
                  inner join OUOM T2 on T1.UomEntry = T2.UomEntry
                  inner join OITM T4 on T1.ItemCode=T4.itemCode
                  inner join OITB T5 on T4.ItmsGrpCod=T5.ItmsGrpCod
                  inner join OWHS T6 on T0.ToWhsCode=T6.WhsCode
                  where T0.U_NSC_WeedProdTrnsf='Y'  and T0.ToWhsCode='{0}'
                  group by T4.ItmsGrpCod,T0.DocEntry,T6.WhsName,T0.DocNum,T0.DocStatus,T5.ItmsGrpNam,T0.DocDueDate,T2.UomName
                  Order by T0.DocEntry", strHolder);
                }
                else
                {
                    strSQL = @"Select T0.DocEntry, T0.DocNum,T6.WhsName,CASE when T0.DocStatus ='O' then 'Open' else 'Closed' End as DocStatus,T5.ItmsGrpNam,sum(T1.quantity) as Qty,T2.UomName,T0.DocDueDate
                  from OWTR T0 inner join WTR1 T1 on T0.DocEntry=T1.DocEntry
                  inner join OUOM T2 on T1.UomEntry = T2.UomEntry
                  inner join OITM T4 on T1.ItemCode=T4.itemCode
                  inner join OITB T5 on T4.ItmsGrpCod=T5.ItmsGrpCod
                  inner join OWHS T6 on T0.ToWhsCode=T6.WhsCode
                  where T0.U_NSC_WeedProdTrnsf='Y'
                  group by T4.ItmsGrpCod,T0.DocEntry,T6.WhsName,T0.DocNum,T0.DocStatus,T5.ItmsGrpNam,T0.DocDueDate,T2.UomName
                  Order by T0.DocEntry";
                }
                pForm.DataSources.DataTables.Item("DTTranRq").ExecuteQuery(strSQL);
                mxHolder.LoadFromDataSourceEx();
                mxHolder.AutoResizeColumns();

				NSC_DI.UTIL.Misc.KillObject(mxHolder);
                GC.Collect();
            }
        }
	}
}
