﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using System.Linq;

namespace NavSol.Forms
{
	public class F_CannabisIntakeWizard : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "NSC_CANABISINTAKE";

        private static System.Data.DataTable _dtBins = new System.Data.DataTable("Bins");
        private static int BinDtKey = -1;
        private static List<SAPbouiCOM.DataTable> BinList = new List<SAPbouiCOM.DataTable>();
		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			if (pVal.ItemUID == "TAB_3") BubbleEvent = CheckBP(oForm);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}
        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, true, new string[] { cFormID })]
        public virtual bool OnBeforeChooseFromList(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            if (pVal.ActionSuccess == true) return false;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            var pFormID = oForm.UniqueID; // Save unique id to get form in callback

            switch (pVal.ItemUID)
            {
                // Crop/Project
                case "TXT_CROP":
                    string strCrop = NSC_DI.UTIL.SQL.GetValue<string>("Select T0.U_Value1 as [CropCycle] from [@NSC_USER_OPTIONS] T0 where T0.Name='Crop Cycle Management'");
                    if (strCrop == "Y")
                    {
                        SAPbouiCOM.DataTable dtCropProj = null;
                        try
                        {
                            if (oForm.DataSources.DataTables.Item("CropSrc").IsEmpty == false)
                            {
                                oForm.DataSources.DataTables.Item("CropSrc").Clear();
                                dtCropProj = oForm.DataSources.DataTables.Item("CropSrc");
                            }
                        }
                        catch
                        {
                            dtCropProj = oForm.DataSources.DataTables.Add("CropSrc");
                        }

                        dtCropProj.ExecuteQuery(@"Select T0.DocNum as [Crop ID],T0.NAME,T0.START as [Start Date],T0.DUEDATE [End Date] from [OPMG] T0 where T0.STATUS!='F'");

                        // open the form; 
                        Forms.Crop.CropCycleSelect.FormCreate(pFormID, dtCropProj);

                        Forms.Crop.CropCycleSelect.CropProjCB = delegate (string callingFormUid, System.Data.DataTable pdtCropProj)
                        {
                            Form oFormDel = null;

                            oFormDel = Globals.oApp.Forms.Item(pFormID) as Form;
                            if (pdtCropProj.Rows.Count == 0) return;

                            System.Data.DataRow row = pdtCropProj.Rows[0];
                            var strCropProjCode = row.ItemArray.GetValue(0);
                            var strCropProjName = row.ItemArray.GetValue(1);

                            oForm = Globals.oApp.Forms.Item(pFormID);
                            oForm.DataSources.UserDataSources.Item("UDS_CRO").ValueEx = strCropProjName.ToString(); ;

                            SAPbouiCOM.EditText oEdit = oForm.Items.Item("TXT_CROC").Specific;
                            oEdit.Value = strCropProjCode.ToString();

                            oEdit = oForm.Items.Item("TXT_CROF").Specific;
                        //string strFinHolder= NSC_DI.UTIL.SQL.GetValue<string>("Select T0.FIPROJECT from [OPMG] T0 where T0.DocNum =" + strCropProjCode);
                        oEdit.Value = NSC_DI.UTIL.SQL.GetValue<string>("Select T0.FIPROJECT from [OPMG] T0 where T0.DocNum =" + strCropProjCode);

                        //Show dropdown
                        ShowAndLoad_StageCMB(oForm, Convert.ToInt32(strCropProjCode));


                        };

                        BubbleEvent = false;
                    }

                    break;

            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "BTN_ADDLS":
                    BTN_ADDLS_Click(oForm);
                    break;

				case "BTN_FINISH":
                    BTN_FINISH_Click_OLD(oForm);

                    break;

				case "BTN_NEXT":
					BTN_NEXT_Click(oForm);
					break;

				case "BTN_BACK":
					BTN_BACK_Click(oForm);
					break;

				case "TAB_1":
					TAB_1_Click(oForm);
					break;

				case "TAB_2":
					TAB_2_Click(oForm);
					break;

				case "TAB_3":
					TAB_3_Click(oForm);
					break;

				case "TAB_4":
					TAB_4_Click(oForm);
					break;
				case "BTN_REMOVE":
					RemoveSelectedItemsFromMatrix(oForm);
					break;

                case "btnCopyPO":
                    CopyPO(oForm);
                    break;

                case "btcSetRows":
                    btcSetRows(oForm);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
		[B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] {cFormID})]
		public virtual void OnAfterChooseFromList(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
            if (((ChooseFromListEvent)pVal).SelectedObjects == null) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				// BusinessPartner ChooseFromList
				case "TXT_BPART":
					TXT_BPART_ChooseFromList_Selected(oForm, (IChooseFromListEvent) pVal);
					break;
				// Item ChooseFromList
				case "TXT_ITMCFL":
					TXT_ITMCFL_ChooseFromList_Selected(oForm, (IChooseFromListEvent) pVal);
					break;
				// Warehouse ChooseFromList
				case "TXT_WRHS":
					TXT_WRHS_ChooseFromList_Selected(oForm, (IChooseFromListEvent) pVal);
                    break;
                // PO ChooseFromList
                case "txtPONum":
                    PO_CFL_Selected(oForm, (ChooseFromListEvent)pVal);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, false, new string[] { cFormID })]
        public virtual void OnAfterValidate(ItemEvent pVal)
        {
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.InnerEvent == false && pVal.ItemUID == "TXT_ITMCFL")
            {
                BinSet(oForm);
                PO_Set(oForm);
            }

            if (pVal.InnerEvent == false && pVal.ItemUID == "TXT_WRHS") BinSet(oForm);

            if (pVal.InnerEvent == false && pVal.ItemUID == "txtPONum") PO_Set(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_COMBO_SELECT
        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
        public virtual void OnAfterComboSelect(ItemEvent pVal)
        {
            if (pVal.InnerEvent == true) return;
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "btcSetRows":
                    btcSetRows(oForm);
                    break;
            }
           
            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        private static void FormSetup(Form pForm)
		{
			try
			{
                _dtBins = new System.Data.DataTable("Bins");

                _dtBins.Columns.AddRange(new System.Data.DataColumn[] {
                    new System.Data.DataColumn("Key",       typeof(int)),
                    new System.Data.DataColumn("Qty",       typeof(decimal)),
                    new System.Data.DataColumn("BinCode",   typeof(string)),
                    new System.Data.DataColumn("BinAbs",    typeof(string)),
                    new System.Data.DataColumn("DestWH",    typeof(string))
                });

				// Freeze the Form UI
				pForm.Freeze(true);

                BinAddFields(pForm);

                pForm.DataSources.DataTables.Add("dtBins");
                
                // Set the main image
                CommonUI.Forms.SetFieldvalue.Picture(pForm, "IMG_HEADER", "WizardHeader.bmp");

				// Find the header label
				StaticText LBL_HEADER = pForm.Items.Item("LBL_HEADER").Specific;

				// Increase the size of the header label
				LBL_HEADER.Item.FontSize = 12;

				// Set the header label to a bold style
				LBL_HEADER.Item.TextStyle = 1;

				// Set the due date to todays date.
				EditText TXT_DUE = pForm.Items.Item("TXT_DUE").Specific;

				if (TXT_DUE != null)
				{
					TXT_DUE.Value = DateTime.Now.ToString("yyyyMMdd");
				}

                //Set default value for  IsMedical
                pForm.DataSources.UserDataSources.Item("UCHK_ISME").Value="N";

                //Set default value for PassedQA
                pForm.DataSources.UserDataSources.Item("UCHK_QA").Value="N";

                // Find the Matrix on the form UI
                Matrix MTX_OITEM = pForm.Items.Item("MTX_OITEM").Specific;
				MTX_OITEM.SelectionMode = BoMatrixSelect.ms_Auto;

				if (MTX_OITEM.RowCount == 0)
				{
					CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "ItemTable", "MTX_OITEM", new List<CommonUI.Matrix.MatrixColumn>()
					{
						new CommonUI.Matrix.MatrixColumn() {Caption = "ID",                     ColumnName = "LineItem",    IsEditable = false, ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
						new CommonUI.Matrix.MatrixColumn() {Caption = "Code",                   ColumnName = "Code",        IsEditable = false, ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
						new CommonUI.Matrix.MatrixColumn() {Caption = "Name",                   ColumnName = "Name",        IsEditable = false, ColumnWidth = 180, ItemType = BoFormItemTypes.it_EDIT},
						new CommonUI.Matrix.MatrixColumn() {Caption = "Destination Warehouse",  ColumnName = "Warehouse",   IsEditable = (Globals.BranchDflt < 0),  ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}, // 10823-3
						new CommonUI.Matrix.MatrixColumn() {Caption = "Quantity",               ColumnName = "Quantity",    IsEditable = true,  ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}, // rhh
                        new CommonUI.Matrix.MatrixColumn() {Caption = "UoM",                    ColumnName = "UoM",         IsEditable = false, ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {Caption = "Medical",                ColumnName = "IsMedical",   IsEditable = true,  ColumnWidth = 80, ItemType = BoFormItemTypes.it_CHECK_BOX}, // rhh
                        new CommonUI.Matrix.MatrixColumn() {Caption = "Bin",                    ColumnName = "Bin",         IsEditable = true,  ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}, // rhh
                        new CommonUI.Matrix.MatrixColumn() {Caption = "State ID",               ColumnName = "MnfSerial",   IsEditable = true,  ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},  // rhh
                        new CommonUI.Matrix.MatrixColumn() {Caption = "Price",                  ColumnName = "Price",       IsEditable = true,  ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},  // rhh
                        new CommonUI.Matrix.MatrixColumn() {Caption = "Passed QA",              ColumnName = "PassedQA",    IsEditable = true,  ColumnWidth = 80, ItemType = BoFormItemTypes.it_CHECK_BOX}, // rhh
                        new CommonUI.Matrix.MatrixColumn() {Caption = "Crop ID",                ColumnName = "CropID",      IsEditable = true,  ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},  // rhh
                        new CommonUI.Matrix.MatrixColumn() {Caption = "Stage ID",               ColumnName = "StageID",     IsEditable = true,  ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},  // rhh
                        new CommonUI.Matrix.MatrixColumn() {Caption = "POEntry",                ColumnName = "POEntry",     IsEditable = false, ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                        new CommonUI.Matrix.MatrixColumn() {Caption = "POLine",                 ColumnName = "POLine",      IsEditable = false, ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},

                    }, @"
SELECT * 
  FROM (SELECT 
'99999999'AS [LineItem]
,'99999999ABCDEFGHIJKLMNOPQRSTUVWXYZ' AS [Code]
,'99999999ABCDEFGHIJKLMNOPQRSTUVWXYZ99999999ABCDEFGH99999999ABCDEFGHIJKLMNOPQRSTUVWXYZ99999999ABCDEFGH' AS [Name]
, '99999999' AS [Warehouse]
, '9999999999999.99' AS [Quantity]
, '9ABCSSDS901234567892' as [UoM]
, 'N' as [IsMedical]
, 'ABC123456789101112131415dcdsddsdvsdvs16' as [Bin]
, '1000000.00' AS [Price]
, 'ABC123456789101112131415dcdsddsdvsdvs16' AS [MnfSerial]
,'N' as [PassedQA]
,'99999999ABCDEFGHIJKLMNOPQRSTUVWXYZ99999999ABCDEFGH99999999ABCDEFGHIJKLMNOPQRSTUVWXYZ99999999ABCDEFGH' AS [CropID]
,'99999999ABCDEFGHIJKLMNOPQRSTUVWXYZ99999999ABCDEFGH99999999ABCDEFGHIJKLMNOPQRSTUVWXYZ99999999ABCDEFGH' AS [StageID]
,'999999' AS [POEntry]
,'999999' AS [POLine]) 
AS [ItemTable]"
                        );

                    // hide the bin column - it shows the default bin and is causing confusion
                    //MTX_OITEM.Columns.Item(7).Visible = false;


                    // Remove the first item in the Matrix
                    DataTable DataTable = pForm.DataSources.DataTables.Item("ItemTable");
					DataTable.Rows.Remove(0);
				}

				MTX_OITEM.LoadFromDataSource();
                MTX_OITEM.AutoResizeColumns();

                CommonUI.Forms.CreateUserDataSource(pForm, "txtPONum", "dsPO", BoDataType.dt_SHORT_TEXT, 100);
                NavSol.CommonUI.CFL.Create(pForm, "CFL_PO", BoLinkedObject.lf_PurchaseOrder);
                pForm.Items.Item("txtPONum").Specific.ChooseFromListUID = "CFL_PO";
                pForm.Items.Item("txtPONum").Specific.ChooseFromListAlias = "DocNum";

                // RHH **********************************************************
                // temp code until I can get back to this feature
                //pForm.Items.Item("staPO").Top = -20;
                //pForm.Items.Item("txtPONum").Top = -20;
                //pForm.Items.Item("btnCopyPO").Top = -20;
                //pForm.Items.Item("txtPOEntry").Top = -20;
                //pForm.Items.Item("btcSetRows").Top = -20;

                pForm.Items.Item("btcSetRows").Specific.ValidValues.Add("All Rows", "1");
                pForm.Items.Item("btcSetRows").Specific.ValidValues.Add("Selected Rows", "2");

                FilterItemChooseFromList(pForm);
				FilterSupplierChooseFromList(pForm);

                // Warehouse CFL filters
                NavSol.CommonUI.CFL.AddCon(pForm, "CFL_WRHS", BoConditionRelationship.cr_NONE, "Inactive", BoConditionOperation.co_EQUAL, "N");   //10823
                NavSol.CommonUI.CFL.AddCon_Branches(pForm, "CFL_WRHS", BoConditionRelationship.cr_AND);   //10823

                // Select the first tab by default
                ((Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_1", pForm)).Select();

                // RHH - hidden field so that focus can be set to it so that fields can be disabled
                Item oItm = pForm.Items.Add("tmp", BoFormItemTypes.it_EDIT);
                oItm.Width = 1;
                oItm.Height = 1;

                pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

				// create hidden field to save the calling form
				if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("txtPare", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		private static bool CheckBP(Form pForm)
		{
			//make sure a BP has been selected
			try
			{
				if (pForm.Items.Item("TXT_VENID").Specific.Value.ToString().Trim() == "")
				{
					pForm.Items.Item("TAB_2").Specific.Select();
					Globals.oApp.StatusBar.SetText("Please select a vendor first!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return false;
			}
		}

		private static void FilterItemChooseFromList(Form pForm)
		{
			SAPbouiCOM.ChooseFromList CFL_OITEM = pForm.ChooseFromLists.Item("CFL_OITEM");

			Conditions objConditions = CFL_OITEM.GetConditions();

			Condition objCondition;
			objCondition = objConditions.Add();

			objCondition.Alias = "PrchseItem";
			objCondition.Operation = BoConditionOperation.co_EQUAL;

			// Filter results for purchasable items for cannabis items Only.
			objCondition.CondVal = "Y";

			CFL_OITEM.SetConditions(objConditions);
		}

		private static void FilterSupplierChooseFromList(Form pForm)
		{
			SAPbouiCOM.ChooseFromList CFL_BPART = pForm.ChooseFromLists.Item("CFL_BPART");

			Conditions objConditionsBP = CFL_BPART.GetConditions();

			Condition objConditionBP;
			objConditionBP = objConditionsBP.Add();

			objConditionBP.Alias = "CardType";
			objConditionBP.Operation = BoConditionOperation.co_EQUAL;
			objConditionBP.CondVal = "S";

			CFL_BPART.SetConditions(objConditionsBP);
		}

        private static void FilterPO_CFL(Form pForm)
        {
            try
            {
                var vendor = pForm.Items.Item("TXT_VENID").Specific.Value;
                NavSol.CommonUI.CFL.AddCon(pForm, "CFL_PO", BoConditionRelationship.cr_NONE, "CardCode", BoConditionOperation.co_EQUAL, vendor);
                NavSol.CommonUI.CFL.AddCon(pForm, "CFL_PO", BoConditionRelationship.cr_AND, "DocStatus", BoConditionOperation.co_EQUAL, "O");
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void ShowAndLoad_StageCMB(Form pForm, int pProjDocNum)
        {
            //Select T0.UniqueID as [Stage ID] from PMG1 T0 where T0.AbsEntry = 40 and T0.UniqueID like 'IN_%'
            //CBT_STAGE
            SAPbouiCOM.ComboBox oCMB = pForm.Items.Item("CBT_STAGE").Specific;
            SAPbouiCOM.DataTable oDT = pForm.DataSources.DataTables.Item("dtSTA");
            oDT.Clear();
            oDT.ExecuteQuery(@"Select T0.UniqueID as [Stage ID] from PMG1 T0 where T0.AbsEntry = "+ pProjDocNum + " and T0.UniqueID like 'IN_%'");
            if(oCMB.ValidValues.Count>0)
            {
                for(int j= oCMB.ValidValues.Count; j<= oCMB.ValidValues.Count;j--)
                {
                    if (j > 0)
                    {
                        oCMB.ValidValues.Remove(j - 1, BoSearchKey.psk_Index);
                    }else
                    {
                        //Break out of loop
                        j = 2;
                    }

                }
                //string strHolder = oCMB.ValidValues.Item(j).Value;
                //    int RecAdj = j;
                //    if (oCMB.ValidValues.Item(j).Value == RecAdj.ToString())
                //    {
                        
                //}
                
            }
            for (int i=0;i<oDT.Rows.Count;i++)
            {
                oCMB.ValidValues.Add(i.ToString(),oDT.GetValue(0, i));                

            }

            //LBL_STAGE
            SAPbouiCOM.StaticText oLBL = pForm.Items.Item("LBL_STAGE").Specific;
            oLBL.Item.Visible = true;
            oCMB.Item.Visible = true;
            
        }

        private static void BTN_ADDLS_Click_OLD(Form pForm)
		{
			#region Validation

			// Validate that we have an Item selected.
            string itemCode = CommonUI.Forms.GetField<string>(pForm, "TXT_ITEMC");

            string itemName = CommonUI.Forms.GetField<string>(pForm, "TXT_ITMCFL");

            if (string.IsNullOrEmpty(itemCode?.Trim()))// (NSC_DI.UTIL.Strings.Empty(itemCode)) Whitespace-Change
            {
				Globals.oApp.StatusBar.SetText("Please select an item!", BoMessageTime.bmt_Short);
                (pForm.Items.Item("TXT_ITEMC").Specific as EditText).Active = true;
				return;
			}

            // Validate that a warehouse is selected.
            string warehouseCode = CommonUI.Forms.GetField<string>(pForm, "TXT_WRHSC");
			
			string warehouseName = CommonUI.Forms.GetField<string>(pForm, "TXT_WRHS");

            if (string.IsNullOrEmpty(warehouseCode))
			{
				Globals.oApp.StatusBar.SetText("Please select a warehouse!", BoMessageTime.bmt_Short);
				(pForm.Items.Item("TXT_WRHS").Specific as EditText).Active = true;
				return;
			}

			// Validate that a quantity was entered.
			string quantity = CommonUI.Forms.GetField<string>(pForm, "TXT_QUANT");

			if (string.IsNullOrEmpty(quantity))
			{
				Globals.oApp.StatusBar.SetText("Please enter a quantity!", BoMessageTime.bmt_Short);
				(pForm.Items.Item("TXT_QUANT").Specific as EditText).Active = true;
				return;
			}

            //Validate that the UoM was set
            //UCHK_ISME
            string UoM = CommonUI.Forms.GetField<string>(pForm, "LBL_UoM");

            if (string.IsNullOrEmpty(UoM))
            {
                Globals.oApp.StatusBar.SetText("Please enter a unit of Measure!", BoMessageTime.bmt_Short);
                return;
            }

            // Validate that a price was entered.
            string price = CommonUI.Forms.GetField<string>(pForm, "TXT_PRICE");

			if (string.IsNullOrEmpty(price))
			{
				Globals.oApp.StatusBar.SetText("Please enter a price!", BoMessageTime.bmt_Short);
				(pForm.Items.Item("TXT_PRICE").Specific as EditText).Active = true;
				return;
			}

            //Capture IsMedical
            string IsMed = pForm.DataSources.UserDataSources.Item("UCHK_ISME").Value;

            //Capture PassedQA
            string PassedQA = pForm.DataSources.UserDataSources.Item("UCHK_QA").Value;

            //Capture the State ID
            string StateID = CommonUI.Forms.GetField<string>(pForm, "TXT_STID");

            #endregion

            //SAPbouiCOM.ComboBox CMB_STATE = Helpers.Forms.GetControlFromForm(ItemType: Helpers.Forms.FormControlTypes.ComboBox, ItemUID: "CMB_STATE", FormToSearchIn: pForm);

            // Find the matrix in the form UI
            Matrix MTX_OITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_OITEM", pForm);

            // Determine the next row number
            //int oNewRowNumber = MTX_OITEM.RowCount;
            int oNewRowNumber = MTX_OITEM.RowCount;

            DataTable DataTable = pForm.DataSources.DataTables.Item("ItemTable");

			DataTable.Rows.Add();
			DataTable.SetValue("LineItem",  oNewRowNumber, oNewRowNumber + 1);
			DataTable.SetValue("Code",      oNewRowNumber, itemCode);
			DataTable.SetValue("Name",      oNewRowNumber, itemName);
			DataTable.SetValue("Warehouse", oNewRowNumber, warehouseCode);
			DataTable.SetValue("Quantity",  oNewRowNumber, quantity);
            DataTable.SetValue("UoM",       oNewRowNumber, UoM);
            DataTable.SetValue("IsMedical", oNewRowNumber, IsMed);
            DataTable.SetValue("Price",     oNewRowNumber, price);
            DataTable.SetValue("MnfSerial", oNewRowNumber, StateID);
            DataTable.SetValue("PassedQA",  oNewRowNumber, PassedQA);
            var bin = pForm.Items.Item("cboBin").Specific.Value;
            DataTable.SetValue("Bin",       oNewRowNumber, bin);
            //Capture the State ID
            string CropID = CommonUI.Forms.GetField<string>(pForm, "TXT_CROC");
            DataTable.SetValue("CropID", oNewRowNumber, CropID);
            string StageID = CommonUI.Forms.GetField<string>(pForm, "CBT_STAGE",true);
            DataTable.SetValue("StageID", oNewRowNumber, StageID);




            // Configure Bins
            BinSet(pForm);
            //pForm.Items.Item("staBin").Visible = false;
            //pForm.Items.Item("cboBin").Visible = false;
            //if (NSC_DI.SAP.Bins.GetLevelCount(warehouseCode) > 0)
            //{
            //    pForm.Items.Item("staBin").Visible = false;
            //    pForm.Items.Item("cboBin").Visible = false;
            //    // FOR NOW JUST USE THE DEFAULT RECEIVING BIN


            //    //// Populate Bin datatable
            //    //SAPbouiCOM.DataTable dtBinIn = pForm.DataSources.DataTables.Item("dtBins");
            //    //NavSol.Forms.F_BinSelect.CreateDT(dtBinIn);

            //    //dtBinIn.Rows.Add(1);

            //    //dtBinIn.SetValue("ItemCode", 0, itemCode);
            //    //dtBinIn.SetValue("Name", 0, itemCode);
            //    //dtBinIn.SetValue("DstWhCode", 0, warehouseCode);
            //    //dtBinIn.SetValue("To WH", 0, warehouseName);
            //    //dtBinIn.SetValue("XFer Qty", 0, quantity);
            //    //dtBinIn.SetValue("Bin Qty", 0, quantity);
            //    //dtBinIn.SetValue("Old Bin Qty", 0, quantity);
            //    //dtBinIn.SetValue("LinkRecNo", 0, -1);

            //    //var pFormID = pForm.UniqueID; // Save unique id to get form in callback

            //    //// open the form; dest warehouse must be set
            //    //F_BinSelect.FormCreate(pFormID, dtBinIn, int.Parse(quantity), warehouseCode, warehouseCode, false);

            //    //F_BinSelect.BinsCB = delegate (string callingFormUid, System.Data.DataTable dtBins)
            //    //{
            //    //    Form oForm = null;

            //    //    oForm = Globals.oApp.Forms.Item(pFormID) as Form;
            //    //    if (dtBins.Rows.Count == 0) return;
            //    //    //BinList.Add(dtBins);
            //    //};
            //}

            // Add new row to matrix
            //MTX_OITEM.AddRow(RowCount: 1, Position: oNewRowNumber);
            MTX_OITEM.LoadFromDataSource();
            MTX_OITEM.AutoResizeColumns();

            // Clear item code
            CommonUI.Forms.SetField(pForm, "TXT_ITEMC", string.Empty);

            // Clear warehouse code
            //  TXT_WRHSC.Value = "";

            // Clear quantity
            CommonUI.Forms.SetField(pForm, "TXT_QUANT", string.Empty);

            // Clear price
            CommonUI.Forms.SetField(pForm, "TXT_PRICE", string.Empty);

            // Clear Choose from Lists.
            CommonUI.Forms.SetField(pForm, "TXT_ITMCFL", string.Empty);

            //Clear IsMedical
            pForm.DataSources.UserDataSources.Item("UCHK_ISME").Value="N";

            //clear the state id
            CommonUI.Forms.SetField(pForm, "TXT_STID", string.Empty);


            //Clear IsMedical
            pForm.DataSources.UserDataSources.Item("UCHK_QA").Value = "N";

            //Clear CropID
            pForm.DataSources.UserDataSources.Item("UDS_CRO").Value = "";
            //Hide Stages
            SAPbouiCOM.StaticText oLBL = pForm.Items.Item("LBL_STAGE").Specific;
            oLBL.Item.Visible = false;
            SAPbouiCOM.ComboBox oCMB = pForm.Items.Item("CBT_STAGE").Specific;
            oCMB.Item.Visible = false;
            // Not sure why we should have to do this but this is enabling after the Add to List button is pressed.
            EnableFinishButton(pForm, false);
		}

        private static void BinAddFields(Form pForm)
        {
            SAPbouiCOM.Item oItm = null;
            try
            {
                //----------------------------------------
                // add the Bin combo box - ONLY UNTIL THE BIN SELECTION FORM IS UTILIZED
                oItm = pForm.Items.Add("staBin", BoFormItemTypes.it_STATIC);
                oItm.Left = pForm.Items.Item("LBL_STID").Left;
                oItm.Top = pForm.Items.Item("TXT_WRHS").Top + pForm.Items.Item("TXT_WRHS").Height + 1;
                oItm.Width = pForm.Items.Item("LBL_STID").Width;
                oItm.Visible = false;
                oItm.Specific.Caption = "Default Bin:";

                oItm = pForm.Items.Add("cboBin", BoFormItemTypes.it_EDIT);      //  it_COMBO_BOX
                oItm.Left = pForm.Items.Item("TXT_STID").Left;  // pForm.Items.Item("staBin").Left + pForm.Items.Item("staBin").Width + 1;
                oItm.Top = pForm.Items.Item("staBin").Top;
                oItm.Width = pForm.Items.Item("TXT_STID").Width;
                oItm.Visible = false;
                oItm.Enabled = false;
                //----------------------------------------
            }
            catch (Exception ex)
            {
                //Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private static void PO_Set(Form pForm)
        {
            try
            {               
                var enable = string.IsNullOrEmpty(pForm.Items.Item("TXT_ITMCFL").Specific.Value?.Trim());// Whitespace-Change NSC_DI.UTIL.Strings.Empty(pForm.Items.Item("TXT_ITMCFL").Specific.Value)
                try
                {
                    pForm.Items.Item("txtPONum").Enabled = enable;
                }
                catch { }
                pForm.Items.Item("btcSetRows").Enabled  = enable;
                pForm.Items.Item("btnCopyPO").Enabled   = !enable;
                pForm.Items.Item("btcSetRows").Enabled  = enable;
                                
                enable = string.IsNullOrEmpty(pForm.Items.Item("txtPONum").Specific.Value?.Trim());// Whitespace-Change NSC_DI.UTIL.Strings.Empty(pForm.Items.Item("txtPONum").Specific.Value)
                pForm.Items.Item("TXT_ITMCFL").Enabled  = enable;
                pForm.Items.Item("TXT_QUANT").Enabled   = enable;
                pForm.Items.Item("TXT_PRICE").Enabled   = enable;
                pForm.Items.Item("btnCopyPO").Enabled   = !enable;

            }
            catch (Exception ex)
            {
                //Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                GC.Collect();
            }
        }

        private static void BinSet(Form pForm)
        {
            try
            {
                var wh = pForm.Items.Item("TXT_WRHSC").Specific.Value;
                if (pForm.ActiveItem == "staBin" || pForm.ActiveItem == "cboBin") pForm.ActiveItem = "TXT_STID";
                pForm.Items.Item("staBin").Visible = false;
                pForm.Items.Item("cboBin").Visible = false;
                pForm.Items.Item("cboBin").Specific.Value = "";
                if (NSC_DI.SAP.Bins.GetLevelCount(wh) <= 0) return;
                {
                    var sVal = pForm.Items.Item("TXT_QUANT").Specific.Value;
                    sVal = string.IsNullOrEmpty(sVal) ? "0.0" : sVal;
                    pForm.Items.Item("staBin").Visible = true;
                    pForm.Items.Item("cboBin").Visible = true;

                    // get the receiving Bin - if there is one DftBinAbs
                    var defBin = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT OBIN.BinCode,'' FROM OBIN WHERE OBIN.WhsCode = '{wh}' AND ReceiveBin = 'Y'", "");
                    if (defBin == "") defBin = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT TOP 1 OBIN.BinCode,'' FROM OBIN INNER JOIN OWHS ON OWHS.DftBinAbs = OBIN.AbsEntry WHERE OBIN.WhsCode = '{wh}'", "");

                    // takes too long to remove all bins when WH changhes - so just have a txt field for now
                    pForm.Items.Item("cboBin").Specific.Value = defBin;

                    //                    var sql = $@"
                    //SELECT OBIN.BinCode,'' FROM OBIN
                    //       WHERE OBIN.WhsCode = '{wh}' AND (OBIN.MaxLevel = 0.0 OR
                    //	   OBIN.MaxLevel - 
                    //	   ISNULL((SELECT SUM(OIBQ.OnHandQty) FROM OIBQ WHERE OIBQ.BinAbs = OBIN.AbsEntry AND OIBQ.WhsCode = OBIN.WhsCode), 0.0) >= {sVal})";

                    //                    //pForm.Freeze(true);
                    //                    CommonUI.ComboBox.LoadComboBoxSQL(pForm, "cboBin", sql);
                    //                    if (defBin == "")
                    //                        pForm.Items.Item("cboBin").Specific.Select(0, BoSearchKey.psk_Index);
                    //                    else
                    //                        pForm.Items.Item("cboBin").Specific.Select(defBin, BoSearchKey.psk_ByValue);
                }
            }
            catch (Exception ex)
            {
                //Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                //NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }

        private static void BTN_ADDLS_Click(Form pForm)
        {
            try
            {
                #region Validation

                // Validate that we have an Item selected.
                string itemCode = CommonUI.Forms.GetField<string>(pForm, "TXT_ITEMC");

                string itemName = CommonUI.Forms.GetField<string>(pForm, "TXT_ITMCFL");

                if (string.IsNullOrEmpty(itemCode?.Trim()))// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(itemCode))
                {
                    Globals.oApp.StatusBar.SetText("Please select an item!", BoMessageTime.bmt_Short);
                    (pForm.Items.Item("TXT_ITEMC").Specific as EditText).Active = true;
                    return;
                }

                // Validate that a warehouse is selected.
                string warehouseCode = CommonUI.Forms.GetField<string>(pForm, "TXT_WRHSC");

                string warehouseName = CommonUI.Forms.GetField<string>(pForm, "TXT_WRHS");

                if (string.IsNullOrEmpty(warehouseCode))
                {
                    Globals.oApp.StatusBar.SetText("Please select a warehouse!", BoMessageTime.bmt_Short);
                    (pForm.Items.Item("TXT_WRHS").Specific as EditText).Active = true;
                    return;
                }

                // Validate that a quantity was entered.
                string quantity = CommonUI.Forms.GetField<string>(pForm, "TXT_QUANT");

                if (string.IsNullOrEmpty(quantity))
                {
                    Globals.oApp.StatusBar.SetText("Please enter a quantity!", BoMessageTime.bmt_Short);
                    (pForm.Items.Item("TXT_QUANT").Specific as EditText).Active = true;
                    return;
                }

                //Validate that the UoM was set
                string UoM = CommonUI.Forms.GetField<string>(pForm, "LBL_UoM");

                if (string.IsNullOrEmpty(UoM))
                {
                    Globals.oApp.StatusBar.SetText("Please enter a unit of Measure!", BoMessageTime.bmt_Short);
                    return;
                }

                // Validate that a price was entered.
                string price = CommonUI.Forms.GetField<string>(pForm, "TXT_PRICE");

                if (string.IsNullOrEmpty(price))
                {
                    Globals.oApp.StatusBar.SetText("Please enter a price!", BoMessageTime.bmt_Short);
                    (pForm.Items.Item("TXT_PRICE").Specific as EditText).Active = true;
                    return;
                }

                //Capture IsMedical
                string IsMed = pForm.DataSources.UserDataSources.Item("UCHK_ISME").Value;

                //Capture PassedQA
                string PassedQA = pForm.DataSources.UserDataSources.Item("UCHK_QA").Value;

                //Capture the State ID
                string StateID = CommonUI.Forms.GetField<string>(pForm, "TXT_STID");

                #endregion


                // Find the matrix in the form UI
                Matrix oMat = pForm.Items.Item("MTX_OITEM").Specific;

                // Determine the next row number
                int binKey = ++BinDtKey;
                var bins = "";

                DataTable DataTable = pForm.DataSources.DataTables.Item("ItemTable");

                DataTable.Rows.Add();
                DataTable.SetValue("LineItem",  oMat.RowCount, binKey);
                DataTable.SetValue("Code",      oMat.RowCount, itemCode);
                DataTable.SetValue("Name",      oMat.RowCount, itemName);
                DataTable.SetValue("Warehouse", oMat.RowCount, warehouseCode);
                DataTable.SetValue("Quantity",  oMat.RowCount, quantity);
                DataTable.SetValue("UoM",       oMat.RowCount, UoM);
                DataTable.SetValue("Price",     oMat.RowCount, price);
                DataTable.SetValue("MnfSerial", oMat.RowCount, StateID);
                DataTable.SetValue("PassedQA",  oMat.RowCount, PassedQA);
                DataTable.SetValue("IsMedical", oMat.RowCount, IsMed);
                DataTable.SetValue("Bin",       oMat.RowCount, bins);
                DataTable.SetValue("CropID",    oMat.RowCount, CommonUI.Forms.GetField<string>(pForm, "TXT_CROC"));
                DataTable.SetValue("StageID",   oMat.RowCount, CommonUI.Forms.GetField<string>(pForm, "CBT_STAGE"));

                // hide the bin column - it shows the default bin and is causing confusion
                //oMat.Columns.Item(7).Visible = false;

                // Configure Bins

                if (NSC_DI.SAP.Bins.IsManaged(warehouseCode))
                {
                    // Populate Bin datatable
                    SAPbouiCOM.DataTable dtBinIn = pForm.DataSources.DataTables.Item("dtBins");
                    NavSol.Forms.F_BinSelect.dtCreateIn(dtBinIn);

                    dtBinIn.Rows.Add();

                    dtBinIn.SetValue("ItemCode",        0, itemCode);
                    dtBinIn.SetValue("Name",            0, itemName);
                    dtBinIn.SetValue("DstWhCode",       0, warehouseCode);
                    dtBinIn.SetValue("To WH",           0, warehouseName);
                    //dtBinIn.SetValue("Serial\\Batch",   0, );                 // SN \ batch does not exist yet
                    dtBinIn.SetValue("XFer Qty",        0, quantity);
                    dtBinIn.SetValue("Bin Qty",         0, quantity);
                    dtBinIn.SetValue("Old Bin Qty",     0, quantity);
                    dtBinIn.SetValue("LinkRecNo",       0, -1);

                    var pFormID = pForm.UniqueID; // Save unique id to get form in callback

                    // open the form; dest warehouse must be set
                    F_BinSelect.FormCreate(pFormID, dtBinIn, double.Parse(quantity), warehouseCode, warehouseCode, false, false);
                    F_BinSelect.BinsCB = delegate (string callingFormUid, System.Data.DataTable dtBins)
                    {
                        Form oForm = null;

                        oForm = Globals.oApp.Forms.Item(pFormID) as Form;
                        if (dtBins.Rows.Count == 0) return;
                        foreach (System.Data.DataRow dr in dtBins.Rows)
                        {
                            _dtBins.Rows.Add(binKey, dr["Quantity"], dr["BinCode"], dr["BinAbs"], dr["DestWH"]);
                            bins += (dtBins.Rows.Count == 1) ? dr["BinCode"] : "<" + dr["BinCode"] + "> ";
                        }
                        //Matrix oMat = pForm.Items.Item("MTX_OITEM").Specific;
                        DataTable.SetValue("Bin", oMat.RowCount-1, bins);
                        oMat.LoadFromDataSource();
                    };
                    //return;     // user hit "Cancel" - NOPE. have to find a different solution
                }

                // Add new row to matrix
                //MTX_OITEM.AddRow(RowCount: 1, Position: oNewRowNumber);
                oMat.LoadFromDataSource();
                oMat.AutoResizeColumns();

                // Clear item code
                CommonUI.Forms.SetField(pForm, "TXT_ITEMC", string.Empty);

                // Clear warehouse code
                //  TXT_WRHSC.Value = "";

                // Clear quantity
                CommonUI.Forms.SetField(pForm, "TXT_QUANT", string.Empty);

                // Clear price
                CommonUI.Forms.SetField(pForm, "TXT_PRICE", string.Empty);

                // Clear Choose from Lists.
                CommonUI.Forms.SetField(pForm, "TXT_ITMCFL", string.Empty);

                //Clear IsMedical
                pForm.DataSources.UserDataSources.Item("UCHK_ISME").Value = "N";

                // Not sure why we should have to do this but this is enabling after the Add to List button is pressed.
                EnableFinishButton(pForm, false);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(pForm);
                GC.Collect();
            }
        }

        /// <summary>
        /// Handle a Matrix Click event. In this case we will be getting setting a selected row?
        /// </summary>
        private static void RemoveSelectedItemsFromMatrix(Form pForm)
		{
			// Getting a DataTable of the DataSource.
			DataTable DataTable = pForm.DataSources.DataTables.Item("ItemTable");

			// Grab the matrix from the form UI
			Matrix MTX_OITEM =
				CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_OITEM", pForm) as Matrix;

			// Go backwards across the list to remove the correct lines..
			for (int i = MTX_OITEM.RowCount; i >= 1; i--)
			{
				if (MTX_OITEM.IsRowSelected(i))
				{
					try
					{
						//Remove this row out of the datatable since its selected.
						DataTable.Rows.Remove(i - 1);
                        System.Data.DataRow[] result = _dtBins.Select($"Key = {i - 1}");
                        foreach (System.Data.DataRow r in result) r.Delete();
                    }
					catch (Exception ex)
					{
						/*  */
					}
				}
			}
			MTX_OITEM.LoadFromDataSource();
            MTX_OITEM.AutoResizeColumns();
        }

        private static void TXT_BPART_ChooseFromList_Selected(Form pForm, IChooseFromListEvent pChooseFromListEvent)
		{
			SAPbouiCOM.ChooseFromList oChooseFromList = pForm.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

			if (pChooseFromListEvent.BeforeAction == false)
			{
				DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

				try
				{
					string oBusinessPartnerCode = oDataTable.GetValue(0, 0).ToString();
					string oBusinessPartnerName = oDataTable.GetValue(1, 0).ToString();

					// Set the userdatasource for the name textbox
					pForm.DataSources.UserDataSources.Item("UDS_BPART").ValueEx = Convert.ToString(oBusinessPartnerName);

					// Save the business partners code
					((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_VENID", pForm))
						.Value = oBusinessPartnerCode;

                    pForm.Items.Item("MTX_OITEM").Specific.Clear();
                }
                catch (Exception ex)
				{
                    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                }
            }
		}

        private static void TXT_ITMCFL_ChooseFromList_Selected(Form pForm, IChooseFromListEvent pChooseFromListEvent)
        {
            SAPbouiCOM.DataTable        oDataTable      = null;
            SAPbouiCOM.ChooseFromList   oChooseFromList = null;

            if (pChooseFromListEvent.BeforeAction) return;

            oChooseFromList = pForm.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);
            oDataTable      = pChooseFromListEvent.SelectedObjects;

            try
            {
                string oItemCode = oDataTable.GetValue(0, 0).ToString();
                string oItemName = oDataTable.GetValue(1, 0).ToString();

                // Set the userdatasource for the name textbox
                pForm.DataSources.UserDataSources.Item("UDS_OITEM").ValueEx = Convert.ToString(oItemName);

                // Save the business partners code
                ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEMC", pForm))
                    .Value = oItemCode;

                // Fill warehouse and pricing information if we have it

                var dt = NSC_DI.UTIL.SQL.DataTable($"SELECT W.WhsCode, W.WhsName, I.LastPurPrc FROM OITM AS I LEFT JOIN OWHS AS W ON I.DfltWH = W.WhsCode WHERE ItemCode = '{oItemCode}'");
                var dr = dt.Rows[0];

                if (Globals.BranchDflt < 0)  // 10823-3 if branches, then do not set default WH
                {
                    if (!String.IsNullOrEmpty(dr["WhsCode"] as string))
                    {
                        pForm.DataSources.UserDataSources.Item("UDS_WRHS").ValueEx = dr["WhsName"].ToString();
                        pForm.Items.Item("TXT_WRHSC").Specific.Value = dr["WhsCode"];
                    }
                }

                PO_Set(pForm);

                //Determine if Purchase UoM exists is not set to inventory and then default to Grams
                string strUoM = null;
                strUoM = NSC_DI.UTIL.SQL.GetValue<string>($"Select T0.BuyUnitMsr from OITM T0 where T0.ItemCode = '" + oItemCode + "'");
                if (strUoM.Length > 0)
                {
                    SAPbouiCOM.StaticText lblUoM = pForm.Items.Item("LBL_UoM").Specific;
                    lblUoM.Caption = strUoM;
                }
                else
                {
                    strUoM = NSC_DI.UTIL.SQL.GetValue<string>($"Select T0.InvntryUom from OITM T0 where T0.ItemCode = '" + oItemCode + "'");
                    if (strUoM.Length > 0)
                    {
                        //Check for Inventory UoM
                        SAPbouiCOM.StaticText lblUoM = pForm.Items.Item("LBL_UoM").Specific;
                        lblUoM.Caption = strUoM;
                    }
                    else
                    {
                        //Default to grams if nothing is set
                        SAPbouiCOM.StaticText lblUoM = pForm.Items.Item("LBL_UoM").Specific;
                        lblUoM.Caption = "g";
                    }
                }

                if ((int)dr["LastPurPrc"] > 0) pForm.Items.Item("TXT_PRICE").Specific.Value = dr["LastPurPrc"];
            }
            catch (Exception ex) // don't log errors ???
            {

            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDataTable);
                NSC_DI.UTIL.Misc.KillObject(oChooseFromList);
                GC.Collect();
            }
        }

        private static void TXT_WRHS_ChooseFromList_Selected(Form pForm, IChooseFromListEvent pChooseFromListEvent)
		{
			SAPbouiCOM.ChooseFromList oChooseFromList = pForm.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

			if (pChooseFromListEvent.BeforeAction == false)
			{
				DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

				try
				{
					string oWarehouseCode = oDataTable.GetValue(0, 0).ToString();
					string oWarehouseName = oDataTable.GetValue(1, 0).ToString();

					// Set the userdatasource for the name textbox
					pForm.DataSources.UserDataSources.Item("UDS_WRHS").ValueEx = Convert.ToString(oWarehouseName);

					// Save the business partners code
					((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHSC", pForm))
						.Value = oWarehouseCode;

					// Prepare to run a SQL statement.
					Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

					// Count how many current records exist within the database.
					oRecordSet.DoQuery(
					                   string.Format(@"
                        SELECT DISTINCT TOP 1 [OWHS].[U_{0}_WhrsType]
                        FROM [OWHS]
                        WHERE [OWHS].WhsCode = '{1}'", Globals.SAP_PartnerCode, oWarehouseCode));

					string queryResultType = "";

					if (oRecordSet.RecordCount <= 0)
					{
						// The warehouse doesn't have a warehouse type set.
						queryResultType = "None";
					}
					else
					{
						// Move to the first record.
						oRecordSet.MoveFirst();

						// We should only ever get one row from this.
						queryResultType = oRecordSet.Fields.Item(0).Value.ToString();
					}

					// ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_STATE", pForm)).Value = queryResultType; // where's the control ???
				}
				catch (Exception ex)
				{

				}
			}
		}

        private static void PO_CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            SAPbouiCOM.DataTable oDT = null;

            try
            {
                oDT = pVal.SelectedObjects;
                pForm.DataSources.UserDataSources.Item("dsPO").ValueEx = oDT.GetValue("DocNum", 0).ToString();
                pForm.Items.Item("txtPOEntry").Specific.Value = oDT.GetValue("DocEntry", 0).ToString();

                PO_Set(pForm);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }

        private void Process(SAPbouiCOM.Form pForm, string pToWH, System.Data.DataTable dtBins = null)
        {
            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                NSC_DI.SAP.Plant.Transfer(dtBins);

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                Globals.oApp.StatusBar.SetText("Successfully moved items.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
            }
            catch (NSC_DI.SAP.B1Exception ex)
            {
                Globals.oApp.StatusBar.SetText(ex.Message);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                NSC_DI.UTIL.Misc.KillObject(pForm);
                GC.Collect();
            }
        }

        private static void BTN_FINISH_Click(Form pForm)
		{
			#region Validate that the User has enter in a date before we go and create a bunch of controllers

			// Validate that we have a Due Date Selected.
			EditText TXT_DUE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DUE", pForm);
			string intakeDueDate = TXT_DUE.Value;

			// Validate that we have a date selected.
			if (string.IsNullOrEmpty(intakeDueDate))
			{
				Globals.oApp.StatusBar.SetText("Please enter a date!");
				TXT_DUE.Active = true;
				return;
			}

            // Validate that we have at least one item to put on the GoodsReceipt PO.
            // DataTable of all the items that we are purchasing.
            DataTable DataTable = pForm.DataSources.DataTables.Item("ItemTable");
            if (DataTable.IsEmpty)
            {
                Globals.oApp.StatusBar.SetText("You must add at least one item to create this document.");
                return;
            }
            // Validate that the user has not set up a item but forgot to add it to the matrix.
            EditText TXT_ITMCODE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITMCFL", pForm);
            if(TXT_ITMCODE.Value.Trim().Length>0)
            {
                Globals.oApp.StatusBar.SetText("You have set up an item but not added it to the inbound list of items please add or delete the item in Step 2");
                return;
            }

            string vendorID = CommonUI.Forms.GetField<string>(pForm, "TXT_VENID");
            DateTime dueDate = Convert.ToDateTime(intakeDueDate.Substring(4, 2) + "/" + intakeDueDate.Substring(6, 2) + "/" + intakeDueDate.Substring(0, 4));
            #endregion

            //---------------------------------
            var oBar = Globals.oApp.StatusBar.CreateProgressBar("Creating Goods Receipt Purchase Order.", DataTable.Rows.Count + 2, false);
            SAPbobsCOM.Documents oGoodsReceiptPO = null;

			try
			{
                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();
                int serialNumberIncrementer = 0;

                oGoodsReceiptPO             = (SAPbobsCOM.Documents)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseDeliveryNotes);
                oGoodsReceiptPO.DocType     = SAPbobsCOM.BoDocumentTypes.dDocument_Items;
                oGoodsReceiptPO.CardCode    = vendorID;
                oGoodsReceiptPO.DocDueDate  = dueDate;

                int binBaseRow = 0;

                for (int i = 0; i < DataTable.Rows.Count; i++)
				{
                    binBaseRow++;

                    int lineItem         = int.Parse(DataTable.GetValue("LineItem", i).ToString());
					string itemCode      = DataTable.GetValue("Code", i).ToString();
					string itemName      = DataTable.GetValue("Name", i).ToString();
					double price         = Convert.ToDouble(DataTable.GetValue("Price", i).ToString());
                    double totQtyRec     = Convert.ToDouble(DataTable.GetValue("Quantity", i).ToString());
                    string UoM           = DataTable.GetValue("UoM", i).ToString();
                    string IsMed         = DataTable.GetValue("IsMedical", i).ToString();
                    string warehouseCode = DataTable.GetValue("Warehouse", i).ToString();
                    string StateID       = DataTable.GetValue("MnfSerial", i).ToString();
                    string PassedQA      = DataTable.GetValue("PassedQA", i).ToString();
                    string bin           = DataTable.GetValue("Bin", i).ToString();
                    string manageBy      = NSC_DI.SAP.Items.ManagedBy(itemCode);

                    if (string.IsNullOrEmpty(IsMed?.Trim()))    IsMed      = "N";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(IsMed))
                    if (string.IsNullOrEmpty(PassedQA?.Trim())) PassedQA   = "N";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(PassedQA))

                    oBar.Text = "Created a Line Item : " + itemCode;
                    if (oGoodsReceiptPO.Lines.ItemCode != "") oGoodsReceiptPO.Lines.Add();

                    oGoodsReceiptPO.Lines.ItemCode      = itemCode;
                    oGoodsReceiptPO.Lines.Quantity      = totQtyRec;
                    //if (LineItem.UnitOfMeasure != null && LineItem.UnitOfMeasure.Length > 0) { oGoodsReceiptPurchaseOrder.Lines.MeasureUnit = LineItem.UnitOfMeasure; }
                    oGoodsReceiptPO.Lines.UnitPrice     = price;
                    oGoodsReceiptPO.Lines.WarehouseCode = warehouseCode;
                    //oGoodsReceiptPO.Lines.ProjectCode   = 

                    System.Data.DataRow[] binRows = _dtBins.Select($"Key = {lineItem }", "DestWH ASC");     //  , "DestWH ASC"

                    if (manageBy == "B")    // Batched Item
                    {
                        string batchId = NSC_DI.SAP.BatchItems.NextBatch(itemCode);
                        //oGoodsReceiptPO.Lines.BatchNumbers.Add();
                        oGoodsReceiptPO.Lines.BatchNumbers.BatchNumber              = batchId;
                        oGoodsReceiptPO.Lines.BatchNumbers.Quantity                 = totQtyRec;
                        oGoodsReceiptPO.Lines.BatchNumbers.ManufacturerSerialNumber = StateID;
                        oGoodsReceiptPO.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_IsMedical").Value = IsMed;
                        oGoodsReceiptPO.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_PassedQA").Value  = PassedQA;
                        if (binRows.Length > 0)
                        {
                            foreach (System.Data.DataRow r in binRows)
                            {
                                oGoodsReceiptPO.Lines.BinAllocations.Quantity       = totQtyRec;
                                oGoodsReceiptPO.Lines.BinAllocations.BinAbsEntry    = int.Parse(r["BinAbs"].ToString());
                                oGoodsReceiptPO.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = oGoodsReceiptPO.Lines.Count-1;
                                //oGoodsReceiptPO.Lines.BinAllocations.BaseLineNumber = binBaseRow;

                                if (r["DestWH"].ToString() != warehouseCode)
                                {
                                    // the warehouse has changed.

                                    // 1st, correct the quantities for the current record.
                                    var qty = (double)_dtBins.Compute("Sum(Quantity)", $"Key = { lineItem } AND DestWH = {warehouseCode}");
                                    oGoodsReceiptPO.Lines.Quantity              = qty;
                                    oGoodsReceiptPO.Lines.BatchNumbers.Quantity = qty;
                                    binBaseRow++;
                                    warehouseCode = r["DestWH"].ToString();
                                    qty = (double)_dtBins.Compute("Sum(Quantity)", $"Key = { lineItem } AND DestWH = {warehouseCode}");

                                    // 2nd, add a new line to the doc
                                    oGoodsReceiptPO.Lines.Add();
                                    oGoodsReceiptPO.Lines.WarehouseCode                         = warehouseCode;
                                    oGoodsReceiptPO.Lines.Quantity                              = qty;
                                    oGoodsReceiptPO.Lines.BatchNumbers.ManufacturerSerialNumber = StateID;
                                    oGoodsReceiptPO.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_IsMedical").Value  = IsMed;
                                    oGoodsReceiptPO.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_PassedQA").Value   = PassedQA;
                                    oGoodsReceiptPO.Lines.BinAllocations.BaseLineNumber         = oGoodsReceiptPO.Lines.Count - 1;
                                    oGoodsReceiptPO.Lines.BinAllocations.Quantity               = qty;
                                }
                            }
                        }
                    }

                    if (manageBy == "S")    // Serialized Item
                    {
                        string batchId = NSC_DI.SAP.BatchItems.NextBatch(itemCode);
                        //oGoodsReceiptPO.Lines.BatchNumbers.Add();
                        oGoodsReceiptPO.Lines.BatchNumbers.BatchNumber              = batchId;
                        oGoodsReceiptPO.Lines.BatchNumbers.Quantity                 = totQtyRec;
                        oGoodsReceiptPO.Lines.BatchNumbers.ManufacturerSerialNumber = StateID;
                        oGoodsReceiptPO.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_IsMedical").Value = IsMed;
                        oGoodsReceiptPO.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_PassedQA").Value  = PassedQA;
                    }

                    oBar.Value++;
				}

				oBar.Text = "Recieving Goods..";

                NSC_DI.SAP.Document.Add(oGoodsReceiptPO);

				oBar.Value++;

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                Globals.oApp.StatusBar.SetText("Goods Received!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
				oBar.Text = "Goods Received!..";
				oBar.Value++;
			}
			catch (Exception ex)
			{
				oBar.Text = "Failed to Receive Goods!";
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				oBar.Stop();
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                NSC_DI.UTIL.Misc.KillObject(oBar);
                NSC_DI.UTIL.Misc.KillObject(oGoodsReceiptPO);
                GC.Collect();
                //pForm.Close();
            }
        }

        private static void BTN_FINISH_Click_OLD(Form pForm)
        {
            #region Validate that the User has enter in a date before we go and create a bunch of controllers
           // string fromPOEntry = null;
            // Validate that we have a Due Date Selected.
            EditText TXT_DUE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DUE", pForm);
            EditText TXT_REMARK = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_REMARK", pForm);

            string intakeDueDate = TXT_DUE.Value;
            string comments = null;
            if (!NSC_DI.UTIL.Strings.Empty(TXT_REMARK.Value))
                comments = TXT_REMARK.Value;

            // Validate that we have a date selected.
            if (string.IsNullOrEmpty(intakeDueDate))
            {
                Globals.oApp.StatusBar.SetText("Please enter a date!");

                TXT_DUE.Active = true;
                return;
            }

            // Validate that we have at least one item to put on the GoodsReceipt PO.
            // DataTable of all the items that we are purchasing.
            DataTable DataTable = pForm.DataSources.DataTables.Item("ItemTable");
            if (DataTable.IsEmpty)
            {
                Globals.oApp.StatusBar.SetText("You must add at least one item to create this document.");
                return;
            }
            // Validate that the user has not set up a item but forgot to add it to the matrix.
            EditText TXT_ITMCODE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITMCFL", pForm);
            if (TXT_ITMCODE.Value.Trim().Length > 0)
            {
                Globals.oApp.StatusBar.SetText("You have set up an item but not added it to the inbound list of items please add or delete the item in Step 2");
                return;
            }
            #endregion

            //---------------------------------
            // Creating controllers and variables for data access

            // We need to query the database for what kind of items we are taking in..
            Recordset sqlResults = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);


            List<NSC_DI.SAP.GoodsReceiptPO.GoodsReciept_Lines> ListOfItems =
                new List<NSC_DI.SAP.GoodsReceiptPO.GoodsReciept_Lines>();

            //Controllers.BatchItems controllerBatchItems = new Controllers.BatchItems(
            //	SAPBusinessOne_Application: Globals.oApp,
            //	SAPBusinessOne_Company: Globals.oCompany);

            //// Prepare a controller for plants
            //Producer.Controllers.Plant controllerPlant = new Producer.Controllers.Plant(
            //	SAPBusinessOne_Application: Globals.oApp,
            //	SAPBusinessOne_Company: Globals.oCompany,
            //	SAPBusinessOne_Form: pForm
            //	);

            //EditText TXT_CROC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_CROC", pForm);
            //string strProj_DocNum = TXT_CROC.Value;
            //---------------------------------
            // Progress Bar Initialization

            // Setting up a Progress Bar.
            var oBar = Globals.oApp.StatusBar.CreateProgressBar("Creating Goods Receipt Purchase Order.", DataTable.Rows.Count + 2, false);
            string strCrop = NSC_DI.UTIL.SQL.GetValue<string>("Select T0.U_Value1 as [CropCycle] from [@NSC_USER_OPTIONS] T0 where T0.Name='Crop Cycle Management'");
            Crop.CropCycleManagement oCrop = new Crop.CropCycleManagement();

            SAPbobsCOM.Documents oGoodsReceiptPO;
           
            try
            {
                string fromPOEntry = null;
                int batchNumberIncrementer = 0;
                int serialNumberIncrementer = 0;

                for (int i = 0; i < DataTable.Rows.Count; i++)
                {                    
                    if (string.IsNullOrEmpty(("LineItem", i).ToString()?.Trim())) continue;  // a blank line can be added, Whitespace-Change (NSC_DI.UTIL.Strings.Empty(DataTable.GetValue("LineItem", i).ToString()
                    int    lineItem         = int.Parse(DataTable.GetValue("LineItem", i).ToString());
                    string itemCode         = DataTable.GetValue("Code", i).ToString();
                    string itemName         = DataTable.GetValue("Name", i).ToString();
                    double price            = Convert.ToDouble(DataTable.GetValue("Price", i).ToString());
                    double totQtyRec        = Convert.ToDouble(DataTable.GetValue("Quantity", i).ToString());
                    string UoM              = DataTable.GetValue("UoM", i).ToString();
                    string IsMed            = DataTable.GetValue("IsMedical", i).ToString();
                    string warehouseCode    = DataTable.GetValue("Warehouse", i).ToString();
                    //Inter-Company Check // DELETE 20201230
                    //string InterCoSubCode   = NSC_DI.UTIL.SQL.GetValue<string>($@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 where T0.WhsCode = '{warehouseCode}'");// DELETE 12/30/2020 


                    string StateID          = DataTable.GetValue("MnfSerial", i).ToString();
                    string PassedQA         = DataTable.GetValue("PassedQA", i).ToString();
                    string bin              = DataTable.GetValue("Bin", i).ToString();

                    if (string.IsNullOrEmpty(IsMed?.Trim())) IsMed = "N";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(IsMed)) 
                    if (string.IsNullOrEmpty(PassedQA?.Trim())) PassedQA = "N";// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(PassedQA))
                    string POEntry = DataTable.GetValue("POEntry", i).ToString();
                    if (NSC_DI.UTIL.Strings.Empty(fromPOEntry) && !NSC_DI.UTIL.Strings.Empty(POEntry))
                        fromPOEntry = POEntry;
                    int POEntryNum = 0;
                    int.TryParse(POEntry, out POEntryNum);
                    int POLine = -1;
                    int.TryParse(DataTable.GetValue("POLine", i).ToString(), out POLine);

                    string strCropID = null;
                    string strStageID = null;


                    Dictionary<string, string> UserDefinedFields = new Dictionary<string, string>();
                    UserDefinedFields.Add("U_NSC_IsMedical", IsMed);
                    UserDefinedFields.Add("U_NSC_PassedQA", PassedQA);
                    if (strCrop.Trim() == "Y")
                    {
                        strCropID = DataTable.GetValue("CropID", i).ToString();
                        strStageID = DataTable.GetValue("StageID", i).ToString();
                        UserDefinedFields.Add("U_NSC_CropID", strCropID); //AddDocToProject
                        UserDefinedFields.Add("U_NSC_CropStageID", strStageID); //AddDocToProject
                    }

                    IList<NSC_DI.SAP.GoodsReceiptPO.GoodsReceipt_Lines_BinAlloc> nscGRPObins = null;

                    //if (_dtBins.Select().Any(n => n["LineNum"] as int? == lineItem))
                    // HAS AS MANY LINES AS THE BIN SELECT FORM. 
                    // THE QTY BEING RECEIVED COULD BE 10 BUT THE NUMBER OF LINES COULD BE 3
                    System.Data.DataRow[] result = _dtBins.Select($"Key = {lineItem}");
                    if (result.Length > 0)
                    {
                        foreach (System.Data.DataRow row in result)
                        {
                            if (string.IsNullOrEmpty(row["DestWH"].ToString()?.Trim()) == false) warehouseCode = row["DestWH"].ToString();// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(row["DestWH"].ToString()) == false)
                            if (nscGRPObins == null) nscGRPObins = new List<NSC_DI.SAP.GoodsReceiptPO.GoodsReceipt_Lines_BinAlloc>();
                            //string binsAbs = _dtBins.Select().First(n => int.Parse(n["LineNum"].ToString()) == lineItem)["BinAbs"] as string;
                            var qty = Convert.ToDouble(row["Qty"].ToString());
                            bin = row["BinAbs"].ToString();

                            nscGRPObins.Add(new NSC_DI.SAP.GoodsReceiptPO.GoodsReceipt_Lines_BinAlloc()
                            {
                                Quantity = Convert.ToDouble(row["Qty"].ToString()),
                                BinsAbs = row["BinAbs"].ToString()
                            });
                        }
                    }
                    else if (string.IsNullOrEmpty(bin?.Trim()) == false)// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(bin)
                    {
                        // there is a bin on the grid and none from the "Bin Select" form, so use it
                        nscGRPObins = new List<NSC_DI.SAP.GoodsReceiptPO.GoodsReceipt_Lines_BinAlloc>();
                        nscGRPObins.Add(new NSC_DI.SAP.GoodsReceiptPO.GoodsReceipt_Lines_BinAlloc()
                        {
                            Quantity = totQtyRec,
                            BinsAbs = NSC_DI.SAP.Bins.GetAbsFromCode(bin).ToString()
                        });
                    }
                    #region Database Query to check if the current item is Batched Or Serial

                    string sqlQuery = string.Format("SELECT ManBtchNum, ManSerNum FROM OITM WHERE OITM.ItemCode = '{0}'", itemCode);

                    sqlResults.DoQuery(sqlQuery);
                    sqlResults.MoveFirst();
                    // Figure out if we are dealing with a Batched Item, Serialized Item, or neither.
                    string batch = sqlResults.Fields.Item("ManBtchNum").Value.ToString();
                    string serial = sqlResults.Fields.Item("ManSerNum").Value.ToString();

                    bool isBatch = sqlResults.Fields.Item("ManBtchNum").Value.ToString() == "Y" ? true : false;
                    bool isSerial = sqlResults.Fields.Item("ManSerNum").Value.ToString() == "Y" ? true : false;

                    #endregion

                    if (isBatch)
                    {
                        #region Create Batched Lines for the GoodsReceiptPO
                        // We have a Batched Item.
                        string batchId = NSC_DI.SAP.BatchItems.NextBatch(itemCode, 1, "B", batchNumberIncrementer).First();

                        if (nscGRPObins != null)
                        {
                            nscGRPObins[0].BatchNumber = batchId;
                        }

                        // Incremenet the batch number. We need to make this outside of the MAIN loop because we could have multiple batch items coming in.
                        batchNumberIncrementer++;
                        //Update the counter on the item master.
                        //****Split Batch number
                        //Isolate the batch Number
                        string strLastNumber = null;
                        strLastNumber = batchId.Split('-').Last();
                        //Increment by 1
                        int BatchNumPlusOne = Convert.ToInt32(strLastNumber) + 1;
                        //Set value to NextBatchNum UDF on OITM
                        NSC_DI.SAP.Item.UpdateItemUDF_Int(itemCode, "_NextNum", BatchNumPlusOne);

                        //Check for Inter-Company. If turned on then pull subsidiary copmany code from warehouse. 


                        if (strCropID!=null && strCropID.Length > 0)
                        {
                            ListOfItems.Add(new NSC_DI.SAP.GoodsReceiptPO.GoodsReciept_Lines()
                            {
                                BaseDoc         = POEntryNum,
                                BaseLine        = POLine,
                                Price           = price,
                                ItemCode        = itemCode,
                                Quantity        = totQtyRec,
                                WarehouseCode   = warehouseCode,
                                UnitOfMeasure   = UoM,
                                FinProject      = oCrop.GetFinProj(strCropID),
                                BatchLineItems  = new List<NSC_DI.SAP.GoodsReceiptPO.GoodsReciept_Lines_Batches>()
                            {
                                new NSC_DI.SAP.GoodsReceiptPO.GoodsReciept_Lines_Batches()
                                {
                                   BatchNumber  = batchId,
                                   Quantity     = totQtyRec,
                                   StateID      = StateID


                                }
                            },
                                BinAllocations = nscGRPObins,
                                UserDefinedFields = UserDefinedFields
                            });
                        }
                        else
                        {
                            ListOfItems.Add(new NSC_DI.SAP.GoodsReceiptPO.GoodsReciept_Lines()
                            {
                                BaseDoc = POEntryNum,
                                BaseLine = POLine,
                                Price = price,
                                ItemCode = itemCode,
                                Quantity = totQtyRec,
                                WarehouseCode = warehouseCode,
                                UnitOfMeasure = UoM,
                                BatchLineItems = new List<NSC_DI.SAP.GoodsReceiptPO.GoodsReciept_Lines_Batches>()
                            {
                                new NSC_DI.SAP.GoodsReceiptPO.GoodsReciept_Lines_Batches()
                                {
                                   BatchNumber  = batchId,
                                   Quantity     = totQtyRec,
                                   StateID      = StateID


                                }
                            },
                                BinAllocations = nscGRPObins,
                                UserDefinedFields = UserDefinedFields
                            });
                        }
                        
                        #endregion

                        oBar.Text = "Created a Batch Line Item : " + itemCode;
                    }
                    else if (isSerial)
                    {
                        #region Create Serial Lines for the GoodsReceiptPO

                        IEnumerable<string> newSerialNumbers = NSC_DI.SAP.Plant.GetNextAvailablePlantSerialID(itemCode, Convert.ToInt32(totQtyRec));

                        // NOT SURE OF THE PURPOSE OF THESE 2 LINES
                        //var binAbs = (nscGRPObins == null)? null: nscGRPObins[0].BinsAbs;
                        //nscGRPObins = newSerialNumbers.Select(n => new NSC_DI.SAP.GoodsReceiptPO.GoodsReceipt_Lines_BinAlloc() { SerialNumber = n, BinsAbs = binAbs, Quantity = 1 }).ToList();

                        ListOfItems.Add(new NSC_DI.SAP.GoodsReceiptPO.GoodsReciept_Lines()
                        {
                            BaseDoc = POEntryNum,
                            BaseLine = POLine,
                            Price = price,
                            ItemCode = itemCode,
                            Quantity = totQtyRec,
                            WarehouseCode = warehouseCode,
                            SerialNumbers = newSerialNumbers,
                            BinAllocations = nscGRPObins,
                            UserDefinedFields = UserDefinedFields,
                            IsMedical = IsMed,
                            Serial_StateID = StateID
                        });

                        #endregion

                        oBar.Text = "Created a Serial Line Item : " + itemCode;
                    }
                    else
                    {
                        // We don't have a Batch or Serial Number. Just get the item.
                        ListOfItems.Add(new NSC_DI.SAP.GoodsReceiptPO.GoodsReciept_Lines()
                        {
                            BaseDoc = POEntryNum,
                            BaseLine = POLine,
                            Price = price,
                            ItemCode = itemCode,
                            Quantity = totQtyRec,
                            WarehouseCode = warehouseCode,
                            BinAllocations = nscGRPObins
                        });

                        oBar.Text = "Created a Line Item : " + itemCode;
                    }

                    // For each item we might need to create a Compliance call. Let this method handle that.
                    //InventoryComplianceCalls.Add(complianceData);
                    oBar.Value++;
                }

                oBar.Text = "Recieving Goods..";

                // Construct a "Goods Receipt PO" document
                string vendorID = CommonUI.Forms.GetField<string>(pForm, "TXT_VENID");
                DateTime dueDate = Convert.ToDateTime(intakeDueDate.Substring(4, 2) + "/" + intakeDueDate.Substring(6, 2) + "/" + intakeDueDate.Substring(0, 4));
                
                string strDocEntry = NSC_DI.SAP.GoodsReceiptPO.AddGoodsReceiptPO(vendorID, dueDate, ListOfItems, CommonUI.Forms.GetField<string>(pForm, "TXT_VENREF"), comments, fromPOEntry);

                oCrop.AddDocToProject(PMDocumentTypeEnum.pmdt_GoodsReceiptPO, strDocEntry, DataTable);

                oBar.Value++;

                Globals.oApp.StatusBar.SetText("Goods Received!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                oBar.Text = "Goods Received!..";
                oBar.Value++;
            }
            catch(System.ComponentModel.WarningException ex)
            {
                Globals.oApp.MessageBox(ex.Message);
            }
            catch (Exception ex)
            {
                oBar.Text = "Failed to Received Goods!";
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oBar.Stop();
                pForm.Close();
            }
        }

        private static void BTN_NEXT_Click(Form pForm)
		{
            Matrix oMat = null;

            try
            {
                switch (CurrentlySelectedTab(pForm))
			{
				case 1:
					((Folder)
						 CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_2", pForm))
						.Select();
					break;

				case 2:
					((Folder)
						 CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_3", pForm))
						.Select();
					break;

				case 3:
                        // save the manual changes to the grid
                        oMat = pForm.Items.Item("MTX_OITEM").Specific;
                        oMat.FlushToDataSource();
                        ((Folder)
						 CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_4", pForm))
						.Select();
					break;
			}
        }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }

		private static int CurrentlySelectedTab(Form pForm)
		{
			for (int i = 1; i <= 4; i++)
			{
				Folder tab = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_" + i.ToString(), pForm);
				if (tab.Selected)
				{
					return i;
				}
			}

			return 0;
		}

		private static void BTN_BACK_Click(Form pForm)
		{
			switch (CurrentlySelectedTab(pForm))
			{
				case 2:
					((Folder)
						 CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_1", pForm))
						.Select();
					break;

				case 3:
					((Folder)
						 CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_2", pForm))
						.Select();
					break;

				case 4:
					((Folder)
						 CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Folder, "TAB_3", pForm))
						.Select();
					break;
			}
		}

		private static void TAB_1_Click(Form pForm)
		{
			// Disable Back Button
			EnableBackButton(pForm, false);

			// Disable Back Button
			EnableFinishButton(pForm, false);
		}

		private static void TAB_2_Click(Form pForm)
		{
			// Enable Back Button
			EnableBackButton(pForm, true);

			// Select the business partner textbox
			((EditText)
				 CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_BPART", pForm))
				.Active = true;
		}

		private static void TAB_3_Click(Form pForm)
		{
			// Disable Finish Button
			EnableFinishButton(pForm, false);

			EnableNextButton(pForm, true);

            FilterPO_CFL(pForm);

            string strCrop = NSC_DI.UTIL.SQL.GetValue<string>("Select T0.U_Value1 as [CropCycle] from [@NSC_USER_OPTIONS] T0 where T0.Name='Crop Cycle Management'");
            if(strCrop.Trim()=="N")
            {
                //Disable and Hide the Crop Cycle form elements
                SAPbouiCOM.EditText oEDT = pForm.Items.Item("TXT_CROP").Specific;
                oEDT.Item.Enabled = false;
                SAPbouiCOM.DataTable oDT = pForm.DataSources.DataTables.Item("dtSTA");
                oDT.Clear();
            }
            // Select the Item Textbox/ChooseFromList by default
            EditText TXT_ITMCFL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITMCFL", pForm);
            TXT_ITMCFL.Active = true;

        }

        private static void TAB_4_Click(Form pForm)
		{
			EnableFinishButton(pForm, true);

			// Disable Next Button
			EnableNextButton(pForm, false);
		}

		/// <summary>
		/// Simple method that sets the state of the Finished Button.
		/// </summary>
		/// <param name="isEnabled">State of the buttons enabled property</param>
		private static void EnableFinishButton(Form pForm, bool isEnabled)
		{
			Button BTN_FINISH =  CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_FINISH", pForm);
			BTN_FINISH.Item.Enabled = isEnabled;
		}

		/// <summary>
		/// Simple method that sets the state of the Next Button.
		/// </summary>
		/// <param name="isEnabled">State of the buttons enabled property</param>
		private static void EnableNextButton(Form pForm, bool isEnabled)
		{
			Button BTN_NEXT =  CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_NEXT", pForm);
			BTN_NEXT.Item.Enabled = isEnabled;
		}

		/// <summary>
		/// Simple method that sets the state of the Back Button.
		/// </summary>
		/// <param name="isEnabled">State of the buttons enabled property</param>
		private static void EnableBackButton(Form pForm, bool isEnabled)
		{
			Button BTN_BACK =  CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_BACK", pForm);
			BTN_BACK.Item.Enabled = isEnabled;
		}

        private static void btcSetRows(Form pForm)
        {
            Matrix      oMat = null;
            DataTable   oDT  = null;
            ProgressBar oBar = null;

            try
            {
                var allRows = false;

                var cntRowSel = 0;
                bool[] rowSelected = null;

                // get the matrix 
                oMat = pForm.Items.Item("MTX_OITEM").Specific;
                oMat.FlushToDataSource();

                switch (pForm.Items.Item("btcSetRows").Specific.Selected.Description)
                {
                    case "1":
                        allRows = true;
                        break;
                    case "2":
                        allRows = false;
                        // only want to scan the whole matrix if required.
                        cntRowSel = CommonUI.Matrix.RowSelected(oMat, out rowSelected);
                        if(cntRowSel <= 0)
                        {
                            Globals.oApp.StatusBar.SetText("No rows were selected.");
                            return;
                        }
                        break;
                    default:
                        return;
                }

                oMat = pForm.Items.Item("MTX_OITEM").Specific;
                oDT = pForm.DataSources.DataTables.Item("ItemTable");

                // see if there columns selected
                bool[] colSelected;
                var cntColSel   = CommonUI.Matrix.ColumnSelected(oMat, out colSelected);    // the actual number of selected columns
                var cols = CommonUI.Matrix.GetColumnList(oDT);

                // get the current  field values
                var IsMedVal    = pForm.DataSources.UserDataSources.Item("UCHK_ISME").Value;
                var PassedQAVal = pForm.DataSources.UserDataSources.Item("UCHK_QA").Value;
                var WHCodeVal   = CommonUI.Forms.GetField<string>(pForm, "TXT_WRHSC");
                var CropIDVal   = CommonUI.Forms.GetField<string>(pForm, "TXT_CROP");
                var StageIDVal  = CommonUI.Forms.GetField<string>(pForm, "CBT_STAGE");


                oBar = Globals.oApp.StatusBar.CreateProgressBar("Updating Matrix", oDT.Rows.Count, true);

                pForm.Freeze(true);

                for (int i = 0; i < oDT.Rows.Count; i++)
                {
                    oBar.Value = i;

                    if (allRows == false && rowSelected[i] == false) continue;
                    // warehouse
                    //if (colSelected[Array.IndexOf(cols, "Warehouse")] || string.IsNullOrWhiteSpace(WHCodeVal) == false) oDT.SetValue("Warehouse", i, WHCodeVal);
                    Setcell(colSelected, cntColSel, cols, oDT, i, "Warehouse", string.IsNullOrEmpty(WHCodeVal?.Trim()), WHCodeVal);// Whitespace-Change NSC_DI.UTIL.Strings.Empty(WHCodeVal)

                    // medical
                    //if (colSelected[Array.IndexOf(cols, "IsMedical")] || IsMedVal == "Y") oDT.SetValue("IsMedical", i, IsMedVal);
                    Setcell(colSelected, cntColSel, cols, oDT, i, "IsMedical", IsMedVal == "N", IsMedVal);

                    // PassedQA
                    //if (colSelected[Array.IndexOf(cols, "PassedQA")] || PassedQAVal == "Y") oDT.SetValue("PassedQA", i, PassedQAVal);
                    Setcell(colSelected, cntColSel, cols, oDT, i, "PassedQA", PassedQAVal == "N", PassedQAVal);

                    // CropCycle
                    //if (colSelected[Array.IndexOf(cols, "CropID")] || string.IsNullOrWhiteSpace(CropIDVal) == false) oDT.SetValue("CropID", i, CropIDVal);
                    Setcell(colSelected, cntColSel, cols, oDT, i, "CropID", string.IsNullOrEmpty(CropIDVal?.Trim()), CropIDVal);// Whitespace-Change NSC_DI.UTIL.Strings.Empty(CropIDVal)

                    // CropStage
                    //if (colSelected[Array.IndexOf(cols, "StageID")] || string.IsNullOrWhiteSpace(StageIDVal) == false) oDT.SetValue("StageID", i, StageIDVal);
                    Setcell(colSelected, cntColSel, cols, oDT, i, "StageID", string.IsNullOrEmpty(StageIDVal?.Trim()), StageIDVal);// Whitespace-Change NSC_DI.UTIL.Strings.Empty(StageIDVal)

                }

                oMat.LoadFromDataSource();
                oMat.AutoResizeColumns();
                oBar.Value = oDT.Rows.Count;


            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                if (oBar != null) oBar.Stop();
                NSC_DI.UTIL.Misc.KillObject(oBar);
                NSC_DI.UTIL.Misc.KillObject(oDT);
                NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }

        private static void Setcell(bool[] pColSelected, int pColSelCnt, string[] pCols, SAPbouiCOM.DataTable pDT, int pRow, string pCol, bool pEmpty, string pValue)
        {
            try
            {
                if (pColSelected[Array.IndexOf(pCols, pCol)] == false && pColSelCnt > 0) return;    // not the right column selected
                if(pColSelCnt == 0 && pEmpty) return;   // no columns have been selected, but the value is empty
                pDT.SetValue(pCol, pRow, pValue);

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void CopyPO(Form pForm)
        {
            Matrix      oMat = null;
            DataTable   oDT  = null;
            Documents   oPO  = null;
            DataTable   oDataTable = null;

            try
            {
                oDT = pForm.DataSources.DataTables.Item("ItemTable");
                oMat = pForm.Items.Item("MTX_OITEM").Specific;
                for (int i = 1; i <= oMat.RowCount; i++) //REBEL 2358
                {
                    if (!NSC_DI.UTIL.Strings.Empty(oMat.GetCellSpecific("col_13", i).Value))
                    {                        
                        Globals.oApp.StatusBar.SetText("You have already copied a PO, please delete previous PO first.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                        return;
                    }
                }

                // get the PO
                oPO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseOrders);

                int po = 0;
                if (!int.TryParse(pForm.Items.Item("txtPOEntry").Specific.Value, out po)) throw new Exception(NSC_DI.UTIL.Message.Format("Invalid PO"));

                NSC_DI.SAP.Document.GetByKey(out oPO, SAPbobsCOM.BoObjectTypes.oPurchaseOrders, po);

                pForm.Items.Item("TXT_VENREF").Specific.Value = oPO.NumAtCard;

                //Capture IsMedical
                var IsMed = pForm.DataSources.UserDataSources.Item("UCHK_ISME").Value;

                //Capture PassedQA
                var PassedQA = pForm.DataSources.UserDataSources.Item("UCHK_QA").Value;


                // Validate that a warehouse is selected.
                var warehouseCode = CommonUI.Forms.GetField<string>(pForm, "TXT_WRHSC");
                var warehouseName = CommonUI.Forms.GetField<string>(pForm, "TXT_WRHS");

                pForm.Freeze(true);

                // Find the matrix in the form UI
                //oMat = pForm.Items.Item("MTX_OITEM").Specific;

                // Determine the next row number
                int binKey = ++BinDtKey;
                //string CopyPONum = pForm.Items.Item("txtPONum").Specific.Value; DELETE 7/1/21

                //oDT = pForm.DataSources.DataTables.Item("ItemTable");
                for (int i = 0; i < oPO.Lines.Count; i++)
                {
                    oPO.Lines.SetCurrentLine(i);
                    warehouseCode = CommonUI.Forms.GetField<string>(pForm, "TXT_WRHSC");
                    if (warehouseCode == "") warehouseCode = oPO.Lines.WarehouseCode;
                    oDT.Rows.Add();
                    oDT.SetValue("LineItem", oMat.RowCount, binKey);
                    oDT.SetValue("Code", oMat.RowCount, oPO.Lines.ItemCode);
                    oDT.SetValue("Name", oMat.RowCount, oPO.Lines.ItemDescription);
                    oDT.SetValue("Warehouse", oMat.RowCount, warehouseCode);
                    oDT.SetValue("Quantity", oMat.RowCount, oPO.Lines.Quantity);
                    oDT.SetValue("UoM", oMat.RowCount, oPO.Lines.UoMCode);
                    oDT.SetValue("IsMedical", oMat.RowCount, IsMed);
                    var bin = pForm.Items.Item("cboBin").Specific.Value;
                    oDT.SetValue("Bin", oMat.RowCount, bin);
                    oDT.SetValue("MnfSerial", oMat.RowCount, oPO.Lines.UserFields.Fields.Item("U_NSC_StateID").Value);
                    oDT.SetValue("Price", oMat.RowCount, oPO.Lines.UnitPrice);
                    oDT.SetValue("PassedQA", oMat.RowCount, PassedQA);
                    // Crop ID
                    // Stage ID
                    oDT.SetValue("POEntry", oMat.RowCount, oPO.Lines.DocEntry);
                    oDT.SetValue("POLine", oMat.RowCount, oPO.Lines.LineNum);
                    // hide the bin column - it shows the default bin and is causing confusion
                    //oMat.Columns.Item(7).Visible = false; DELETE 7/1/21


                    // Configure Bins

                    //if (NSC_DI.SAP.Bins.IsManaged(warehouseCode))
                    //{
                    //    // Populate Bin datatable
                    //    SAPbouiCOM.DataTable dtBinIn = pForm.DataSources.DataTables.Item("dtBins");
                    //    NavSol.Forms.F_BinSelect.dtCreateIn(dtBinIn);

                    //    dtBinIn.Rows.Add();

                    //    dtBinIn.SetValue("ItemCode",    0, itemCode);
                    //    dtBinIn.SetValue("Name",        0, itemName);
                    //    dtBinIn.SetValue("DstWhCode",   0, warehouseCode);
                    //    dtBinIn.SetValue("To WH",       0, warehouseName);
                    //    //dtBinIn.SetValue("Serial\\Batch",   0, );                 // SN \ batch does not exist yet
                    //    dtBinIn.SetValue("XFer Qty",    0, quantity);
                    //    dtBinIn.SetValue("Bin Qty",     0, quantity);
                    //    dtBinIn.SetValue("Old Bin Qty", 0, quantity);
                    //    dtBinIn.SetValue("LinkRecNo",   0, -1);

                    //    var pFormID = pForm.UniqueID; // Save unique id to get form in callback

                    //    // open the form; dest warehouse must be set
                    //    F_BinSelect.FormCreate(pFormID, dtBinIn, int.Parse(quantity), warehouseCode, warehouseCode, false, false);
                    //    F_BinSelect.BinsCB = delegate (string callingFormUid, System.Data.DataTable dtBins)
                    //    {
                    //        Form oForm = null;

                    //        oForm = Globals.oApp.Forms.Item(pFormID) as Form;
                    //        if (dtBins.Rows.Count == 0) return;
                    //        foreach (System.Data.DataRow dr in dtBins.Rows)
                    //        {
                    //            _dtBins.Rows.Add(binKey, dr["Quantity"], dr["BinCode"], dr["BinAbs"], dr["DestWH"]);
                    //        }
                    //    };
                    //}

                    // Add new row to matrix
                    //MTX_OITEM.AddRow(RowCount: 1, Position: oNewRowNumber); DELETE 7/1/21
                    oMat.LoadFromDataSource();
                }

                oMat.AutoResizeColumns();
              
                // Clear item code
                CommonUI.Forms.SetField(pForm, "TXT_ITEMC", string.Empty);

                // Clear warehouse code
                //  TXT_WRHSC.Value = "";

                // Clear quantity
                CommonUI.Forms.SetField(pForm, "TXT_QUANT", string.Empty);

                // Clear price
                CommonUI.Forms.SetField(pForm, "TXT_PRICE", string.Empty);

                // Clear Choose from Lists.
                CommonUI.Forms.SetField(pForm, "TXT_ITMCFL", string.Empty);

                //Clear IsMedical
                pForm.DataSources.UserDataSources.Item("UCHK_ISME").Value = "N";

                CommonUI.Forms.SetField(pForm, "txtPONum", string.Empty);
                CommonUI.Forms.SetField(pForm, "txtPOEntry", string.Empty);
                PO_Set(pForm);

                // Not sure why we should have to do this but this is enabling after the Add to List button is pressed.
                EnableFinishButton(pForm, false);
                pForm.Items.Item("txtPOEntry").Enabled = false;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oPO);
                NSC_DI.UTIL.Misc.KillObject(oDT);
                NSC_DI.UTIL.Misc.KillObject(oMat);
                NSC_DI.UTIL.Misc.KillObject(oDataTable);
            }
        }
    }
}