﻿using System;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms
{
    public class F_BatchSerialTracking : B1Event
    {
        // ---------------------------------- VARS, CLASSES --------------------------------------------------------
        const string cFormID = "NSC_BATCH_SERIAL_TRACKING";

        private static string _callingFormUid   = String.Empty;

        // ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPressed(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "1" && pVal.FormMode == (int)BoFormMode.fm_UPDATE_MODE) LoadGrid(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        // ---------------------------------- AFTER EVENT  --------------------------------------------------------

        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "btnCollaps") Collapse(oForm);
            if (pVal.ItemUID == "btnExpand") Expand(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (IChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public static Form FormCreate(bool pSingleInstance = true, string pCallingFormUID = null)
        {
            Item oItm = null;
            try
            {
                var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                //// create hidden field to save the calling form
                //if (pCallingFormUID != null)
                //{
                //    oItm = oForm.Items.Add("txtPare", BoFormItemTypes.it_EDIT);
                //    oItm.Top = -20;
                //    oItm.Specific.Value = pCallingFormUID;
                //}

                FormSetup(oForm);
                return oForm;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private static void FormSetup(Form pForm)
        {
            try
            {
                CommonUI.Forms.CreateUserDataSource(pForm, "txtItemCod", "dsCode", BoDataType.dt_SHORT_TEXT, 50);
                CommonUI.CFL.Create(pForm, "CFL_Code", BoLinkedObject.lf_Items);
                CommonUI.CFL.AddCon(pForm, "CFL_Code", BoConditionRelationship.cr_NONE, "QryGroup64", BoConditionOperation.co_EQUAL, "N");
                pForm.Items.Item("txtItemCod").Specific.ChooseFromListUID = "CFL_Code";
                pForm.Items.Item("txtItemCod").Specific.ChooseFromListAlias = "ItemCode";

                CommonUI.Forms.CreateUserDataSource(pForm, "txtItemNam", "dsName", BoDataType.dt_SHORT_TEXT, 100);
                CommonUI.CFL.Create(pForm, "CFL_Name", BoLinkedObject.lf_Items);
                CommonUI.CFL.AddCon(pForm, "CFL_Name", BoConditionRelationship.cr_NONE, "QryGroup64", BoConditionOperation.co_EQUAL, "N");
                pForm.Items.Item("txtItemNam").Specific.ChooseFromListUID = "CFL_Name";
                pForm.Items.Item("txtItemNam").Specific.ChooseFromListAlias = "ItemName";

                CommonUI.Forms.CreateUserDataSource(pForm, "txtBatch", "dsBatch", BoDataType.dt_SHORT_TEXT, 36);
                CommonUI.CFL.Create(pForm, "CFL_Batch", BoLinkedObject.lf_BatchNumbers);
                CommonUI.CFL.AddCon(pForm, "CFL_Batch", BoConditionRelationship.cr_NONE, "ItemCode", BoConditionOperation.co_EQUAL, "");
                pForm.Items.Item("txtBatch").Specific.ChooseFromListUID = "CFL_Batch";
                pForm.Items.Item("txtBatch").Specific.ChooseFromListAlias = "DistNumber";

                CommonUI.Forms.CreateUserDataSource(pForm, "txtSN", "dsSN", BoDataType.dt_SHORT_TEXT, 36);
                CommonUI.CFL.Create(pForm, "CFL_SN", BoLinkedObject.lf_SerialNumbers);
                CommonUI.CFL.AddCon(pForm, "CFL_SN", BoConditionRelationship.cr_NONE, "ItemCode", BoConditionOperation.co_EQUAL, "");
                pForm.Items.Item("txtSN").Specific.ChooseFromListUID = "CFL_SN";
                pForm.Items.Item("txtSN").Specific.ChooseFromListAlias = "DistNumber";

                pForm.DataSources.DataTables.Add("dtGrid");
                pForm.Items.Item("grdData").Specific.DataTable = pForm.DataSources.DataTables.Item("dtGrid");

                //pForm.Mode = BoFormMode.fm_OK_MODE;

                //// set the list of Warehouses
                //var sql = "SELECT WhsCode, WhsName FROM OWHS";
                //CommonUI.ComboBox.LoadComboBoxSQL(pForm, "cboDstWhC", sql);

                //pForm.Items.Item("grdSrcLocs").Specific.SelectionMode = BoMatrixSelect.ms_Auto;
                //pForm.Items.Item("grdDstLocs").Specific.SelectionMode = BoMatrixSelect.ms_Auto;
                //pForm.Items.Item("grdSrcLocs").Enabled = true;

                ////// the level check boxes get enabled when a combobox value changes
                ////for (var i = 1; i < 5; i++)
                ////{
                ////    pForm.Items.Item("chkLevel" + i.ToString()).SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, (int)BoAutoFormMode.afm_All, BoModeVisualBehavior.mvb_False);
                ////}

                //// RHH - hidden field so that focus can be set to it so that the dest WH can be disabled
                //Item oItm = pForm.Items.Add("tmp", BoFormItemTypes.it_EDIT);
                //oItm.Width = 1;
                //oItm.Height = 1;
                //oItm.Specific.Active = true;
                //CommonUI.Forms.SetAutoManaged(pForm, "cboDstWhC", false); // does not appear to work. so set the form's automanage to false
                ////pForm.AutoManaged = false;
                //pForm.Items.Item("cboDstWhC").Enabled = _AllowMultiWH; 

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.VisibleEx = true;
            }
        }

        private void CFL_Selected(Form pForm, IChooseFromListEvent pVal)
        {
            //if (pVal.InnerEvent) return;
            if (pVal.SelectedObjects == null) return;
            SAPbouiCOM.DataTable oDT = null;

            try
            {
                pForm.ChooseFromLists.Item(pVal.ChooseFromListUID);
                oDT = pVal.SelectedObjects;

                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_Code":
                        pForm.Items.Item("txtSN").Specific.Value = "";
                        pForm.Items.Item("txtBatch").Specific.Value = "";
                        goto case "CFL_Name";

                    case "CFL_Name":
                        LoadGrid(pForm);    // clear the grid
                        pForm.Mode = BoFormMode.fm_OK_MODE;
                        var itemCode = oDT.GetValue("ItemCode", 0).ToString();
                        var managedBy = NSC_DI.SAP.Items.ManagedBy(itemCode);

                        pForm.DataSources.UserDataSources.Item("dsCode").ValueEx = itemCode;
                        pForm.DataSources.UserDataSources.Item("dsName").ValueEx = oDT.GetValue("ItemName", 0).ToString();

                        CommonUI.CFL.AddCon(pForm, "CFL_Batch", BoConditionRelationship.cr_NONE, "ItemCode", BoConditionOperation.co_EQUAL, itemCode);
                        CommonUI.CFL.AddCon(pForm, "CFL_SN",    BoConditionRelationship.cr_NONE, "ItemCode", BoConditionOperation.co_EQUAL, itemCode);

                        pForm.Items.Item("staBatchSN").Specific.Caption = "";
                        pForm.Items.Item("txtBatch").Visible            = (managedBy == "B");
                        pForm.Items.Item("txtSN").Visible               = (managedBy == "S");

                        switch (managedBy)
                        {
                            case "B":
                                pForm.Items.Item("staBatchSN").Specific.Caption = "Batch ID";
                                break;

                            case "S":
                                pForm.Items.Item("staBatchSN").Specific.Caption = "Serial Number";
                                break;
                        }
                        break;

                    case "CFL_Batch":
                        pForm.DataSources.UserDataSources.Item("dsBatch").ValueEx = oDT.GetValue("DistNumber", 0).ToString();
                        pForm.Mode = BoFormMode.fm_UPDATE_MODE;
                        break;
                    case "CFL_SN":
                        pForm.DataSources.UserDataSources.Item("dsSN").ValueEx = oDT.GetValue("DistNumber", 0).ToString();
                        pForm.Mode = BoFormMode.fm_UPDATE_MODE;
                        break;
                }


                //pForm.Items.Item("txtItemNam").Specific.value = oDT.GetValue("ItemName", 0).ToString();

                //string itemCode = oDT.GetValue("ItemCode", 0).ToString();
                //string itemName = oDT.GetValue("ItemName", 0).ToString();

                //pForm.DataSources.UserDataSources.Item("UDS_MOTHR").ValueEx = Convert.ToString(itemName);

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }

        private void Collapse(Form pForm)
        {
            SAPbouiCOM.Grid oGrid = null;

            try
            {

                pForm.Items.Item("grdData").Specific.CollapseLevel = 1;
                //pForm.Items.Item("grdData").Specific.Rows.CollapseAll();
                pForm.Items.Item("btnCollaps").Enabled = false;
                pForm.Items.Item("btnExpand").Enabled = true;

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrid);
                GC.Collect();
            }
        }

        private void Expand(Form pForm)
        {
            SAPbouiCOM.Grid oGrid = null;

            try
            {

                pForm.Items.Item("grdData").Specific.CollapseLevel = 0;
                //pForm.Items.Item("grdData").Specific.Rows.ExpandAll();
                pForm.Items.Item("btnCollaps").Enabled = true;
                pForm.Items.Item("btnExpand").Enabled = false;

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrid);
                GC.Collect();
            }
        }

        private void LoadGrid(Form pForm)
        {
            SAPbouiCOM.Grid oGrd = null;

            try
            {
                var name = "";
                var itemCode = pForm.Items.Item("txtItemCod").Specific.value;
                var managedBy = NSC_DI.SAP.Items.ManagedBy(itemCode);
                if (managedBy == "B") name = NSC_DI.SAP.BatchSerialTracking.Load(itemCode, pForm.Items.Item("txtBatch").Specific.value);
                if (managedBy == "S") name = NSC_DI.SAP.BatchSerialTracking.Load(itemCode, pForm.Items.Item("txtSN").Specific.value);

                pForm.Freeze(true);
                oGrd = pForm.Items.Item("grdData").Specific;
                var sql = $"SELECT U_Level, U_ItemCode, U_ItemName, U_Batch, U_Serial, U_Receipt, U_PdO FROM { NSC_DI.UTIL.SQL.SetTableName(NSC_DI.Globals.tBatSerTrace)} WHERE Name = '{name}'";
                pForm.DataSources.DataTables.Item("dtGrid").ExecuteQuery(sql);

                // rename the column headers (remove the U_)
                for (var i = 0; i < oGrd.Columns.Count; i++)
                {
                    var title = oGrd.Columns.Item(i).TitleObject.Caption;
                    if (title.Substring(0, 2) == "U_") oGrd.Columns.Item(i).TitleObject.Caption = title.Substring(2);
                }

                //oGrd.Columns.Item("Code").Visible = false;
                //oGrd.Columns.Item("Name").Visible = false;

                Expand(pForm);
                pForm.Items.Item("grdData").Specific.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }
    }
}