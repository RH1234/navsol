﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Product_Processing
{
	internal class F_PackagingPP : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_PACKAGING_PP";

		private enum SelectedMatrixColumns
		{
			ItemCode,
			ItemName,
			OnHand,
            UoM,
			Quantity,
			BatchNumber,
            Medical,
            StateID,
            LabTestID,
			SourceWarehouseCode,
            Notes,
		}

		private enum ProductMatrixColumns
		{
			ItemCode,
			ItemName,
			UsedPerPackage,
		}

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		public string FormUID { get; set; }

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}

		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;
            CheckBox oChkBox_SpecialProd = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_SPRO", _SBO_Form) as CheckBox;
            EditText TXT_SPITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_SITM", _SBO_Form) as EditText;
            Matrix MTX_PROD = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PROD", _SBO_Form) as Matrix;
            Button BTN_APPLY = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "BTN_APPLY", _SBO_Form) as Button;
            EditText TXT_QTY = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QTY", _SBO_Form) as EditText;
            EditText TXT_DAYS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DAYS", _SBO_Form) as EditText;

            bool SpecialProdOrder;

            switch (pVal.ItemUID)
			{
				case "BTN_SELECT":
					BTN_SELECT_Click();
                    
                    SpecialProdOrder = oChkBox_SpecialProd.Checked;
                    if (SpecialProdOrder == true)
                    {
                        LoadProductCreationMatrix(null);
                        if (MTX_PROD.Item.Visible == true)
                        {
                            TXT_SPITEM.Item.Visible = true;
                            TXT_SPITEM.Item.Enabled = true;
                            _SBO_Form.Items.Item("txtItemNam").Visible = true;
                            BTN_APPLY.Item.Enabled = true;
                            TXT_QTY.Item.Enabled = true;
                            TXT_QTY.Value = "";
                            //Get total wieht and set it
                            double dblTotalWeight = GetTotalQuantityFromSelected();
                            TXT_QTY.Value = dblTotalWeight.ToString();

                        }
                        else
                        {
							TXT_SPITEM.Item.Visible = false;
                            _SBO_Form.Items.Item("txtItemNam").Visible = false;
                            //BTN_APPLY.Item.Enabled = false;
                            TXT_QTY.Value = "";
                            TXT_DAYS.Active = true;
                            TXT_QTY.Item.Enabled = false;
                        }
                    }
                    break;
                case "BTN_LOOKUP":
                    BTN_LOOKUP_ITEM_PRESSED();
                    break;
                case "BTN_APPLY":
                    SpecialProdOrder = oChkBox_SpecialProd.Checked;
                    if (SpecialProdOrder == true)
                    {
                        CreateSpecialProductionOrder();
                    }
                    else
                    {
                        BTN_APPLY_Click();
                    }
					break;

				case "MTX_MAIN":
					MTX_MAIN_Click(pVal);
					break;

				case "MTX_PROD":
					PROD_MATRIX_ITEM_PRESSED(pVal);
					break;
                case "CHK_SPRO":

                    _SBO_Form.Items.Item("MTX_MAIN").Specific.SelectionMode = BoMatrixSelect.ms_Single; //  ver 32.002.9 - only allow user to select one row if "Special Production" is unchecked
                    SpecialProdOrder = oChkBox_SpecialProd.Checked;
                    if (SpecialProdOrder == true)
                    {
                        _SBO_Form.Items.Item("MTX_MAIN").Specific.SelectionMode = BoMatrixSelect.ms_Auto;   //  ver 32.002.9 - only allow user to select one row if "Special Production" is unchecked
                        LoadProductCreationMatrix(null);
                        if (MTX_PROD.Item.Visible == true)
                        {
                            TXT_SPITEM.Item.Visible = true;
                            TXT_SPITEM.Item.Enabled = true;
                            _SBO_Form.Items.Item("txtItemNam").Visible = true;
                            BTN_APPLY.Item.Enabled = true;
                            TXT_QTY.Item.Enabled = true;
                        }
                        else
                        {
							TXT_SPITEM.Item.Visible = false;
                            _SBO_Form.Items.Item("txtItemNam").Visible = false;
                        }

                    }
                    break;
                case "TAB_1":
                    Load_Matrix(_SBO_Form);
                    break;
                case "TAB_2":
                    Load_Matrix(_SBO_Form);
                    break;
                case "TAB_3":
                    Load_Matrix(_SBO_Form);
                    break;
            }

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "MTX_MAIN":
					MATRIX_LINK_PRESSED(pVal, pVal.ItemUID);
					break;

				case "MTX_SELECT":
					MATRIX_LINK_PRESSED(pVal, pVal.ItemUID);
					break;

				case "MTX_PROD":
					MATRIX_LINK_PRESSED(pVal, pVal.ItemUID);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            CFL_Selected(oForm, (ChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
        public virtual void OnAfterComboSelect(ItemEvent pVal)
        {
            //10823-2
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "CMB_WHSE") CMB_WHSE_Selected(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public F_PackagingPP() : this(Globals.oApp, Globals.oCompany)
		{
		}

		public F_PackagingPP(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
		{
			_SBO_Application = SAPBusinessOne_Application;
			_SBO_Company = SAPBusinessOne_Company;
		}

		public void Form_Load()
		{
			try
            { 
				_SBO_Form = CommonUI.Forms.Load(cFormID, true);
                _SBO_Form.Visible = true;

                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_MAIN", @"packaging-icon.bmp");

                // create the User Data Sources
                //CommonUI.Forms.CreateUserDataSource(_SBO_Form, "TAB_1", "TAB_1", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 2);
                //CommonUI.Forms.CreateUserDataSource(_SBO_Form, "CHK_SPRO", "udsSPRO", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1);


                if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y") //'DEV', 'FIN', 'PRO''
                {
                    NavSol.CommonUI.CFL.CreateWH(_SBO_Form, "CFL_WH", "CMB_WHSE");
                    CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_NONE, "Inactive", BoConditionOperation.co_EQUAL, "N");
                    CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, "DEV", 1);
                    CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_OR, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, "FIN");
                    CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_OR, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, "PRO", 0, 1);
                    CommonUI.CFL.AddCon_Branches(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND);    //10823
                }
                else
                    Load_Combobox_Warehouses();

                // chkPack
                CommonUI.Forms.CreateUserDataSource(_SBO_Form, "chkPack", "udsPack", BoDataType.dt_SHORT_TEXT, 1);

                // chkAllowDs
                CommonUI.Forms.CreateUserDataSource(_SBO_Form, "chkAllowDs", "udsAllowDs", BoDataType.dt_SHORT_TEXT, 1);

                // chkIsPrdBa
                CommonUI.Forms.CreateUserDataSource(_SBO_Form, "chkIsPrdBa", "udsIsPrdBa", BoDataType.dt_SHORT_TEXT, 1);
                var defVal = NSC_DI.UTIL.SQL.GetValue<string>("SELECT Dflt FROM CUFD WHERE TableID = 'OWOR' AND AliasID = 'NSC_isProductionBatch'", "N");
                _SBO_Form.Items.Item("chkIsPrdBa").Specific.Checked = (defVal == "Y") ? true : false;

                // This will be populated with type of QA Batch.
                Matrix MTX_MAIN = (Matrix)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);
                if (MTX_MAIN != null) {MTX_MAIN.SelectionMode = BoMatrixSelect.ms_Single;}
                

				// This is set to be not selectable. Since it would be done from the last matrix.
				SAPbouiCOM.Matrix MTX_SELECT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_SELECT", _SBO_Form);
				MTX_SELECT.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_None;

				// This is the Matrix that we will populate with products we can create from the selected item type.
				SAPbouiCOM.Matrix MTX_PROD = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PROD", _SBO_Form);
				MTX_PROD.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single;

				SAPbouiCOM.EditText TXT_DAYS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DAYS", _SBO_Form) as SAPbouiCOM.EditText;

				if (TXT_DAYS != null)
				{
					TXT_DAYS.Item.Enabled = true;
					TXT_DAYS.Value = "1";
				}

                Folder TAB_FG = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_1", _SBO_Form) as Folder;
                TAB_FG.Select();
                // Loading all Matrices. Passing true loads all three.
                Load_Matrix(_SBO_Form);

				// Un-Freeze the Form UI
				_SBO_Form.Freeze(false);

				// Show the form
				_SBO_Form.VisibleEx = true;

				SAPbouiCOM.EditText TXT_LOOKUP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", _SBO_Form) as SAPbouiCOM.EditText;
				TXT_LOOKUP.Active = true;

				EditText TXT_SPITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_SITM", _SBO_Form) as EditText;

                // CFL
                //_SBO_Form.Items.Item("TXT_SITM").Visible = true;
                //_SBO_Form.Items.Item("TXT_SITM").Width=120;
                //_SBO_Form.Items.Item("TXT_SITM").Left = _SBO_Form.Items.Item("2").Left;
                //_SBO_Form.Items.Item("TXT_SITM").Top = _SBO_Form.Items.Item("2").Top + _SBO_Form.Items.Item("2").Height + 5;
                CommonUI.Forms.CreateUserDataSource(_SBO_Form, "TXT_SITM", "UDS_SITM", BoDataType.dt_SHORT_TEXT, 20);
				CommonUI.CFL.Create(_SBO_Form, "CFL_SITM", BoLinkedObject.lf_Items);
				_SBO_Form.Items.Item("TXT_SITM").Specific.ChooseFromListUID = "CFL_SITM";
				_SBO_Form.Items.Item("TXT_SITM").Specific.ChooseFromListAlias = "ItemCode";
                CommonUI.CFL.AddCon(_SBO_Form, "CFL_SITM", BoConditionRelationship.cr_NONE, "QryGroup64", BoConditionOperation.co_EQUAL, "N");

                TXT_SPITEM.Item.Enabled = true;
                _SBO_Form.Items.Item("txtItemNam").Visible = false;

                ////Select a Mother Plant
                //SAPbouiCOM.ChooseFromList CFL_SITM= _SBO_Form.ChooseFromLists.Item("CFL_SITM");

                //Conditions objConditions = CFL_SITM.GetConditions();

                //// Make sure the plant item is a mother plant
                //Condition objCondition = objConditions.Add();
                //objCondition.Alias = "U_NSC_IsMother";
                //objCondition.Operation = BoConditionOperation.co_EQUAL;
                //objCondition.CondVal = "Y";

                //CFL_SITM.SetConditions(objConditions);

                TXT_SPITEM.Item.Visible = false;

			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private void Load_Combobox_Warehouses()
		{

            // Prepare to run a SQL statement.
            SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            // Count how many current records exist within the database.
            var sqlQry = "SELECT [WhsCode], [WhsName] FROM [OWHS] WHERE [U_NSC_WhrsType] in ('DEV', 'FIN', 'PRO') AND [Inactive] ='N'";
            if (Globals.BranchDflt >= 0) sqlQry += $" AND OWHS.BPLid IN ({Globals.BranchList})";  // #10823

            oRecordSet.DoQuery(sqlQry);

			// Find the Combobox of Warehouses from the Form UI
			ComboBox CMB_WHSE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WHSE", _SBO_Form) as ComboBox;

			// If items already exist in the drop down
			if (CMB_WHSE.ValidValues.Count > 0)
			{
				// Remove all currently existing values from warehouse drop down
				for (int i = CMB_WHSE.ValidValues.Count; i-- > 0;)
				{
					CMB_WHSE.ValidValues.Remove(i, BoSearchKey.psk_Index);
				}
			}

			// If more than 1 warehouses exists
			if (oRecordSet.RecordCount > 1)
			{
				// Create the first item as an empty item
				CMB_WHSE.ValidValues.Add("", "");

				// Select the empty item (forcing the user to make a decision)
				CMB_WHSE.Select(0, BoSearchKey.psk_Index);
			}

			// Add allowed warehouses to the drop down
			for (int i = 0; i < oRecordSet.RecordCount; i++)
			{
				try
				{
					CMB_WHSE.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
				}
				catch
				{
				}
				CMB_WHSE.Item.Enabled = true;
				oRecordSet.MoveNext();
			}
		}

        private void CMB_WHSE_Selected(Form pForm)
        {
            //10823-2
            //
            // this method is required because if a method is called from an event handler,
            // the method must call MessageBox and not throw a new exception

            try
            {
                if (Globals.BranchDflt >= 0)
                    if (Globals.BranchDflt >= 0)
                        Load_Matrix(pForm); //10823-2
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
            }
        }
        private void CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            if (pVal.SelectedObjects == null) return;

            try
            {
                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_WH":
                        //CommonUI.CFL.SetVal(pForm, pVal, "WhsName");
                        CommonUI.CFL.SetFieldsWH(pForm, pVal, "CMB_WHSE", "txtWH_CFL");
                        if (Globals.BranchDflt >= 0)
                            Load_Matrix(pForm); //10823-2
                        break;

                    case "CFL_SITM":
                        TXT_SITM_ChooseFromList_Selected(pForm, pVal);
                        break;
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void Load_Matrix(Form pForm)
		{
			// Prepare a Matrix helper
			VERSCI.Matrix MatrixHelper = new VERSCI.Matrix(Globals.oApp, pForm);
             
            string TabSelection = "";
            Folder TAB_FG = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_1", pForm) as Folder;
            if (TAB_FG.Selected == true || pForm.PaneLevel == 1) { TabSelection = "OBTN.[U_NSC_PassedQA] = 'Y' "; }
            Folder TAB_WIP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_2", pForm) as Folder;
            if (TAB_WIP.Selected == true || pForm.PaneLevel == 2) { TabSelection = "OBTN.[U_NSC_PassedQA] = 'N' "; }
            Folder TAB_ALL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_3", pForm) as Folder;
            if (TAB_ALL.Selected == true || pForm.PaneLevel == 3) { TabSelection = "(OBTN.[U_NSC_PassedQA] = 'Y' or OBTN.[U_NSC_PassedQA] = 'N') "; }

            // #10823 --->
            // limit the list to items in ths pecific branch
            var defBranch = Globals.BranchDflt;         //  -1 is no branches, 0 is no default branch
            var brList = Globals.BranchList;
            var destWH = pForm.Items.Item("CMB_WHSE").Specific.Value;
            if (destWH != "" && defBranch >= 0)
            {
                defBranch = NSC_DI.SAP.Branch.Get(destWH);
                brList = NSC_DI.SAP.Branch.GetAll_User_Str(defBranch);
            }
            // #10823 <---

            string sqlQuery = $@"
SELECT OBTN.ItemCode, OITM.ItemName, OBTQ.Quantity AS OnHand,[OITM].InvntryUom as 'UoM', OBTQ.Quantity AS Quantity, OBTN.DistNumber AS BatchNumber,
       case when OBTN.U_NSC_IsMedical ='Y' then 'Yes' else 'No' END as 'Medical',
	   ISNULL(OBTN.MnfSerial,'') as 'StateID',
       COALESCE(OBTN.U_NSC_LabTestID,'') AS LabTestID, OBTQ.WhsCode AS SourceWarehouseCode, OBTN.Notes
  FROM OBTN
  LEFT OUTER JOIN OBTQ ON OBTN.ItemCode = OBTQ.ItemCode AND OBTN.SysNumber = OBTQ.SysNumber
 INNER JOIN OITM ON OITM.ItemCode = OBTN.ItemCode
 INNER JOIN OWHS ON OBTQ.WhsCode = OWHS.WhsCode
 WHERE OITM.U_NSC_Processable = 'Y' AND OITM.OnHand > 0 AND OBTQ.Quantity > 0 AND {TabSelection}
   AND ISNULL(OWHS.U_NSC_WhrsType, '') NOT IN ('QNX', 'QND', 'QNS')";
            var wh = CommonUI.Forms.GetField<string>(pForm, "CMB_WHSE", true);
            var branch = NSC_DI.SAP.Warehouse.GetBranch(wh);
            if (Globals.BranchDflt >= 0) sqlQuery += $" AND OWHS.BPLid IN ({branch})";     //10823-2
            //if (defBranch >= 0) sqlQuery += $" AND OWHS.BPLid IN ({brList})";        // #10823

            //string TabSelection = "";
            //Folder TAB_FG = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_1", _SBO_Form) as Folder;
            //if (TAB_FG.Selected == true) { TabSelection = "[OIBT].[U_NSC_PassedQA] = 'Y' "; }
            //Folder TAB_WIP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_2", _SBO_Form) as Folder;
            //if (TAB_WIP.Selected == true) { TabSelection = "[OIBT].[U_NSC_PassedQA] = 'N' "; }
            //Folder TAB_ALL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_3", _SBO_Form) as Folder;
            //if (TAB_ALL.Selected == true) { TabSelection = "[OIBT].[U_NSC_PassedQA] = 'Y' or [OIBT].[U_NSC_PassedQA] = 'N' "; }
            //SELECT
            //[OIBT].[ItemCode] 
            //,[OIBT].[ItemName]
            //,[OIBT].[Quantity] AS [OnHand]
            //,[OIBT].[Quantity] AS [Quantity]
            //,[OIBT].[BatchNum] AS [BatchNumber]
            //,case when [OIBT].[U_NSC_IsMedical] ='Y' then 'Yes'
            //else 'No' END as 'Medical'
            //, ISNULL([OBTN].[MnfSerial],'') as 'StateID'
            //, COALESCE(OIBT.U_NSC_LabTestID,'') AS LabTestID
            //,[OIBT].[WhsCode] AS [SourceWarehouseCode]
            //,[OBTN].[Notes]
            //FROM
            //[OIBT]

            //JOIN [OITM] 
            //on [OITM].[ItemCode] = [OIBT].[ItemCode] 
            //JOIN [OBTN] on [OBTN].[DistNumber] =[OIBT].[BatchNum]
            //WHERE
            //[OITM].[U_NSC_Processable] = 'Y'
            //AND [OITM].[OnHand] > 0
            //AND [OIBT].[Quantity] > 0
            //AND " + TabSelection + @"-- They must have QA passed before allowing them to process?\
            //";

            MatrixHelper.LoadDatabaseDataIntoMatrix("SelectionTable", "MTX_MAIN", new List<VERSCI.Matrix.MatrixColumn>()
					{new VERSCI.Matrix.MatrixColumn() {Caption = "Item Code", ColumnWidth = 40, ColumnName = "ItemCode", ItemType = BoFormItemTypes.it_LINKED_BUTTON},
						new VERSCI.Matrix.MatrixColumn() {Caption = "Item Name", ColumnWidth = 80, ColumnName = "ItemName", ItemType = BoFormItemTypes.it_EDIT},
						new VERSCI.Matrix.MatrixColumn() {Caption = "On Hand", ColumnWidth = 80, ColumnName = "OnHand", ItemType = BoFormItemTypes.it_EDIT},
                        new VERSCI.Matrix.MatrixColumn() {Caption = "UoM", ColumnWidth = 80, ColumnName = "UoM", ItemType = BoFormItemTypes.it_EDIT},
                        new VERSCI.Matrix.MatrixColumn()
						{
							Caption     = "Quantity To Use",
							ColumnWidth = 80,
							ColumnName  = "Quantity",
							ItemType    = BoFormItemTypes.it_EDIT,
							IsEditable  = true
						}
						,
						new VERSCI.Matrix.MatrixColumn() {Caption = "Batch", ColumnWidth = 80, ColumnName = "BatchNumber", ItemType = BoFormItemTypes.it_EDIT}
						,
                        new VERSCI.Matrix.MatrixColumn() {Caption = "Medical", ColumnWidth = 80, ColumnName = "Medical", ItemType = BoFormItemTypes.it_EDIT}
                        ,
                        new VERSCI.Matrix.MatrixColumn() {Caption = "StateID", ColumnWidth = 80, ColumnName = "StateID", ItemType = BoFormItemTypes.it_EDIT}
                        ,
                        new VERSCI.Matrix.MatrixColumn() {Caption = "Lab Test ID", ColumnWidth = 80, ColumnName = "LabTestID", ItemType = BoFormItemTypes.it_EDIT}
                        ,
                        new VERSCI.Matrix.MatrixColumn()
						{
							Caption     = "Source Warehouse",
							ColumnWidth = 80,
							ColumnName  = "SourceWarehouseCode",
							ItemType    = BoFormItemTypes.it_EDIT
						}
                        ,
                        new VERSCI.Matrix.MatrixColumn()
                        {
                            Caption     = "Notes",
                            ColumnWidth = 300,
                            ColumnName  = "Notes",
                            ItemType    = BoFormItemTypes.it_EDIT
                        }
                    }, sqlQuery);

			MatrixHelper.LoadDatabaseDataIntoMatrix("SelectedTable", "MTX_SELECT", new List<VERSCI.Matrix.MatrixColumn>()
					{new VERSCI.Matrix.MatrixColumn() {Caption = "Item Code", ColumnWidth = 40, ColumnName = "ItemCode", ItemType = BoFormItemTypes.it_LINKED_BUTTON},
						new VERSCI.Matrix.MatrixColumn() {Caption = "Item Name", ColumnWidth = 80, ColumnName = "ItemName", ItemType = BoFormItemTypes.it_EDIT},
						new VERSCI.Matrix.MatrixColumn() {Caption = "On Hand", ColumnWidth = 80, ColumnName = "OnHand", ItemType = BoFormItemTypes.it_EDIT},
                        new VERSCI.Matrix.MatrixColumn() {Caption = "UoM", ColumnWidth = 80, ColumnName = "UoM", ItemType = BoFormItemTypes.it_EDIT},
                        new VERSCI.Matrix.MatrixColumn() {Caption = "Quantity To Use", ColumnWidth = 80, ColumnName = "Quantity", ItemType = BoFormItemTypes.it_EDIT},
                        new VERSCI.Matrix.MatrixColumn() {Caption = "Batch", ColumnWidth = 80, ColumnName = "BatchNumber", ItemType = BoFormItemTypes.it_EDIT},
                        new VERSCI.Matrix.MatrixColumn() {Caption = "Medical", ColumnWidth = 80, ColumnName = "Medical", ItemType = BoFormItemTypes.it_EDIT},
                        new VERSCI.Matrix.MatrixColumn() {Caption = "StateID", ColumnWidth = 80, ColumnName = "StateID", ItemType = BoFormItemTypes.it_EDIT},
                        new VERSCI.Matrix.MatrixColumn() {Caption = "Lab Test ID", ColumnWidth = 80, ColumnName = "LabTestID", ItemType = BoFormItemTypes.it_EDIT},

                        new VERSCI.Matrix.MatrixColumn()
						{
							Caption     = "Source Warehouse",
							ColumnWidth = 80,
							ColumnName  = "SourceWarehouseCode",
							ItemType    = BoFormItemTypes.it_EDIT
						}
					}, sqlQuery);
		}

		/// <summary>
		/// Populates the Production Matrix with Products that can be created from the Item(s) Selected.
		/// </summary>
		/// <param name="itemCodeOfSelected">Unqiue Identifier of the Selected Item</param>
		private void LoadProductCreationMatrix(string itemCodeOfSelected)
		{
			// Prepare a Matrix helper
			VERSCI.Matrix MatrixHelper = new VERSCI.Matrix(Globals.oApp, _SBO_Form);

            CheckBox oChkBox_SpecialProd = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_SPRO", _SBO_Form) as CheckBox;
            bool SpecialProdOrder = oChkBox_SpecialProd.Checked;
            if (SpecialProdOrder) itemCodeOfSelected = null;
            // Load the matrix of tasks
            MatrixHelper.LoadDatabaseDataIntoMatrix("ProductTable", "MTX_PROD", new List<VERSCI.Matrix.MatrixColumn>()
					{new VERSCI.Matrix.MatrixColumn() {Caption = "Item Code", ColumnWidth = 40, ColumnName = "ItemCode", ItemType = BoFormItemTypes.it_LINKED_BUTTON},
					 new VERSCI.Matrix.MatrixColumn() {Caption = "Item Name", ColumnWidth = 80, ColumnName = "ItemName", ItemType = BoFormItemTypes.it_EDIT},
					 new VERSCI.Matrix.MatrixColumn() {Caption = "Used Per Package", ColumnWidth = 80, ColumnName = "UsedPerPackage", ItemType = BoFormItemTypes.it_EDIT}
					}, @"SELECT [ITT1].[Father] AS [ItemCode], [OITM].[ItemName],   [ITT1].[Quantity] AS [UsedPerPackage] FROM [ITT1] JOIN [OITM] ON [ITT1].[Father] = [OITM].[ItemCode] JOIN [OITB] ON [OITB].[ItmsGrpCod] = [OITM].[ItmsGrpCod]
WHERE   [OITB].[ItmsGrpNam] NOT IN ('Mature Plant', 'Trim Sample', 'Wet Cannabis Sample') And OITB.U_NSC_CanType <> 'QCLot' AND [Code] = '" + itemCodeOfSelected + @"'");
                
        }

		private void BTN_SELECT_Click()
		{
			Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form) as Matrix;
			Matrix MTX_SELECT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_SELECT", _SBO_Form) as Matrix;
			Matrix MTX_PROD = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PROD", _SBO_Form) as Matrix;
			Button BTN_SELECT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "BTN_SELECT", _SBO_Form) as Button;
			Button BTN_LOOKUP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "BTN_LOOKUP", _SBO_Form) as Button;
			StaticText LBL_PROD = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.StaticText, "LBL_PROD", _SBO_Form) as StaticText;
			EditText TXT_LOOKUP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", _SBO_Form) as EditText;
			Button BTN_APPLY = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "BTN_APPLY", _SBO_Form) as Button;
            EditText TXT_SPITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_SITM", _SBO_Form) as EditText;
            Folder TAB_FG = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_1", _SBO_Form) as Folder;
            Folder TAB_WIP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_2", _SBO_Form) as Folder;
            Folder TAB_ALL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_3", _SBO_Form) as Folder;
            Folder TAB_SELECT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_4", _SBO_Form) as Folder;
            EditText TXT_REM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_REM", _SBO_Form) as EditText;
            StaticText LBL_REM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.StaticText, "LBL_REM", _SBO_Form) as StaticText;


            // Freeze the Form UI
            _SBO_Form.Freeze(true);

			// We can use the visibility of Main to see what state we are in.. Either we already have selected items and we need to select products
			// otherwise the User wants to reselect batches to use.
			if (MTX_MAIN.Item.Visible)
			{
				// Get and Clear out our DataTable so we can populate with our new selected data.
				DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("SelectedTable");

				// Remove all rows from our selected matrix
				int rowsToRemove = DataTable.Rows.Count;
				for (int i = 0; i < rowsToRemove; i++)
				{
					DataTable.Rows.Remove(0);
				}

				string ItemCodeSelected = "";
				int rowToAdd = 0;

				for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
				{
					try
					{
						// If the current row is now selected we can just continue looping.
						if (!MTX_MAIN.IsRowSelected(i))
							continue;

						string itemCode = ((EditText) MTX_MAIN.Columns.Item(SelectedMatrixColumns.ItemCode).Cells.Item(i).Specific).Value;
						string itemName = ((EditText) MTX_MAIN.Columns.Item(SelectedMatrixColumns.ItemName).Cells.Item(i).Specific).Value;
						string OnHand = ((EditText) MTX_MAIN.Columns.Item(SelectedMatrixColumns.OnHand).Cells.Item(i).Specific).Value;
                        string UoM = ((EditText)MTX_MAIN.Columns.Item(SelectedMatrixColumns.UoM).Cells.Item(i).Specific).Value;
                        string Quantity = ((EditText) MTX_MAIN.Columns.Item(SelectedMatrixColumns.Quantity).Cells.Item(i).Specific).Value;
						string BatchNumber = MTX_MAIN.Columns.Item(SelectedMatrixColumns.BatchNumber).Cells.Item(i).Specific.Value.ToString();
                        string IsMedical = MTX_MAIN.Columns.Item(SelectedMatrixColumns.Medical).Cells.Item(i).Specific.Value.ToString();
                        string StateID = MTX_MAIN.Columns.Item(SelectedMatrixColumns.StateID).Cells.Item(i).Specific.Value.ToString();
                        string labTestID = MTX_MAIN.Columns.Item(SelectedMatrixColumns.LabTestID).Cells.Item(i).Specific.Value.ToString(); 
						string SourceWarehouseCode = MTX_MAIN.Columns.Item(SelectedMatrixColumns.SourceWarehouseCode).Cells.Item(i).Specific.Value.ToString();

						DataTable.Rows.Add();
						DataTable.SetValue("ItemCode", rowToAdd, itemCode);
						DataTable.SetValue("ItemName", rowToAdd, itemName);
						DataTable.SetValue("OnHand", rowToAdd, OnHand);
                        DataTable.SetValue("UoM", rowToAdd, UoM);
                        DataTable.SetValue("Quantity", rowToAdd, Quantity);
						DataTable.SetValue("BatchNumber", rowToAdd, BatchNumber);
                        DataTable.SetValue("Medical", rowToAdd, IsMedical);
                        DataTable.SetValue("StateID", rowToAdd, StateID);
                        DataTable.SetValue("LabTestID", rowToAdd, labTestID);
						DataTable.SetValue("SourceWarehouseCode", rowToAdd, SourceWarehouseCode);

						rowToAdd++;
						ItemCodeSelected = itemCode;
					}
					catch (Exception e)
					{
						//TODO-LOG: Log error.
					}

					TXT_LOOKUP.Active = true;

					MTX_MAIN.Item.Visible = false;
					MTX_SELECT.Item.Visible = true;
					MTX_PROD.Item.Visible = true;
					LBL_PROD.Item.Visible = true;
					BTN_LOOKUP.Item.Enabled = false;
                    LBL_REM.Item.Visible = true;
                    TXT_REM.Item.Visible = true;


                    CheckBox oChkBox_SpecialProd = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_SPRO", _SBO_Form) as CheckBox;
                    bool SpecialProdOrder = oChkBox_SpecialProd.Checked;
                    if (SpecialProdOrder == true)
                    {
                        TXT_SPITEM.Item.Visible = true;
                        _SBO_Form.Items.Item("txtItemNam").Visible = true;
                    }

                    //Show Selected Items Tab
                    TAB_SELECT.Item.Visible = true;
                    //Hide all other tabs
                    TAB_FG.Item.Visible = false;
                    TAB_WIP.Item.Visible = false;
                    TAB_ALL.Item.Visible = false;
                    

                    BTN_SELECT.Item.Specific.Caption = "Previous - Select Cannabis";

				}

				MTX_SELECT.LoadFromDataSource();
				LoadProductCreationMatrix(ItemCodeSelected);

            }
			else
			{
				TXT_LOOKUP.Active = true;
				MTX_MAIN.Item.Visible = true;
				MTX_SELECT.Item.Visible = false;
				MTX_PROD.Item.Visible = false;
				LBL_PROD.Item.Visible = false;
				BTN_LOOKUP.Item.Enabled = true;
				TXT_SPITEM.Item.Visible = false;
                _SBO_Form.Items.Item("txtItemNam").Visible = false;

                BTN_APPLY.Item.Enabled = false;

                //LBL_REM.Item.Visible = false;
                //TXT_REM.Item.Visible = false;
                TXT_REM.Value = "";

                BTN_SELECT.Item.Specific.Caption = "Next - Choose Product";
                //Show Tabs
                TAB_FG.Item.Visible = true;
                TAB_WIP.Item.Visible = true;
                TAB_ALL.Item.Visible = true;
                TAB_SELECT.Item.Visible = false;

            }

            // Freeze the Form UI           
            _SBO_Form.Freeze(false);
		}

        private void CreateSpecialProductionOrder()
        {

            Matrix MTX_SELECT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_SELECT", _SBO_Form) as Matrix;
            EditText TXT_SITM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_SITM", _SBO_Form) as EditText;
            EditText TXT_QTY = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QTY", _SBO_Form) as EditText;
            EditText TXT_DAYS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DAYS", _SBO_Form) as EditText;
            ComboBox CMB_WHSE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WHSE", _SBO_Form) as ComboBox;

            // Validate Data is good to go for the Production Order.

            double quantityToCreate = 0;
            DateTime dueDate = DateTime.Now;

            int estimatedDays = 1;
            if (!int.TryParse(TXT_DAYS.Value, out estimatedDays))
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Please enter in a number for the days!");

                // Focus the cursor in the offending input
                TXT_DAYS.Active = true;
                return;
            }

            if (string.IsNullOrEmpty(TXT_QTY.Value) || !double.TryParse(TXT_QTY.Value, out quantityToCreate))
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Something went wrong with the calculated quantity!");

                return;
            }

            // Prepare a ComboBox helper
            VERSCI.ComboBox ComboBox_Helper = new VERSCI.ComboBox(Globals.oApp, Globals.oCompany, _SBO_Form);

            // Grab the value from the ComboBox
            string destinationWarehouse = CMB_WHSE.Value.ToString();

            // Get the warehouse code from the description
            string destinationWarehouseCode = ComboBox_Helper.GetComboBoxValueOrDescription(cFormID, "CMB_WHSE", null, destinationWarehouse);
            //Validate that a warehouse was selected 
            if (string.IsNullOrEmpty(destinationWarehouseCode))
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Please Select A Destination Warehouse!");
                return;
            }

            dueDate = DateTime.Now.AddDays(estimatedDays);

            // Prepare a list of line items for the "Production Order" document
            List<NSC_DI.SAP.ProductionOrder.SpecialProductionOrderComponentLineItem> components =
                new List<NSC_DI.SAP.ProductionOrder.SpecialProductionOrderComponentLineItem>();

            // Bill of Materials
            for (int i=1; i<= MTX_SELECT.RowCount;i++)
            {
                string ItemCode = ((EditText)MTX_SELECT.Columns.Item(SelectedMatrixColumns.ItemCode).Cells.Item(i).Specific).Value;
                string ItemQty = ((EditText)MTX_SELECT.Columns.Item(SelectedMatrixColumns.Quantity).Cells.Item(i).Specific).Value;
                string ItemWhsCode = ((EditText)MTX_SELECT.Columns.Item(SelectedMatrixColumns.SourceWarehouseCode).Cells.Item(i).Specific).Value;
                string ItemBatch = ((EditText)MTX_SELECT.Columns.Item(SelectedMatrixColumns.BatchNumber).Cells.Item(i).Specific).Value;

                components.Add(
                               new NSC_DI.SAP.ProductionOrder.SpecialProductionOrderComponentLineItem()
                               {
                                   ItemCodeForItemOnBOM = ItemCode,
                                   BaseQuantity = Convert.ToDouble(ItemQty),
                                   PlannedQuantity = quantityToCreate,
                                   SourceWarehouseCode = ItemWhsCode,
                                   BatchNo= ItemBatch
                               }
                    );
            }

            //Create Production Order and Issue Components
            int ProdKey = NSC_DI.SAP.ProductionOrder.CreateSpecialProdOrderAndIssue(TXT_SITM.Value, Convert.ToDouble(TXT_QTY.Value), destinationWarehouseCode, dueDate, components, true,"Y");

            // set the isProductionBatch 
            NSC_DI.SAP.ProductionOrder.SetUDF(ProdKey, "U_NSC_isProductionBatch", _SBO_Form.Items.Item("chkIsPrdBa").Specific.Checked ? "Y" : "N");

            // set the Process 
            NSC_DI.SAP.ProductionOrder.SetUDF(ProdKey, "U_NSC_Process", _SBO_Form.Items.Item("chkPack").Specific.Checked ? "Packaging" : "Inventory_convert");

            // Clear the form out.
            ResetForm();
        }

        private void BTN_APPLY_Click()
		{
            SAPbouiCOM.Matrix oMat = null;
            try
            {
                oMat = _SBO_Form.Items.Item("MTX_SELECT").Specific;
                EditText TXT_QTY = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QTY", _SBO_Form) as EditText;
                EditText TXT_DAYS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DAYS", _SBO_Form) as EditText;
                ComboBox CMB_WHSE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WHSE", _SBO_Form) as ComboBox;
                EditText TXT_REM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_REM", _SBO_Form) as EditText;


                // Validate Data is good to go for the Production Order.

                double quantityToCreate = 0;
                DateTime dueDate = DateTime.Now;

                int estimatedDays = 1;
                if (!int.TryParse(TXT_DAYS.Value, out estimatedDays))
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Please enter in a number for the days!");

                    // Focus the cursor in the offending input
                    TXT_DAYS.Active = true;
                    return;
                }

                if (string.IsNullOrEmpty(TXT_QTY.Value) || !double.TryParse(TXT_QTY.Value, out quantityToCreate))
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Something went wrong with the calculated quantity!");

                    return;
                }

                //if (_SBO_Form.Items.Item("chkAllowDs").Specific.Checked)
                //{

                //}

                // Prepare a ComboBox helper
                VERSCI.ComboBox ComboBox_Helper = new VERSCI.ComboBox(Globals.oApp, Globals.oCompany, _SBO_Form);

                // Grab the value from the ComboBox
                string destinationWarehouse = CMB_WHSE.Value.ToString(); //10824

                // Get the warehouse code from the descriptionf
                string destinationWarehouseCode = ComboBox_Helper.GetComboBoxValueOrDescription(cFormID, "CMB_WHSE", null, destinationWarehouse);
                //Validate that a warehouse was selected 
                if (string.IsNullOrEmpty(destinationWarehouseCode))
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Please Select A Destination Warehouse!");

                    return;
                }
                
                dueDate = DateTime.Now.AddDays(estimatedDays);

                //Remarks
                string Remarks = TXT_REM.Value;

                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();
                int newPDOKey = 0;
                var process = _SBO_Form.Items.Item("chkPack").Specific.Checked ? "Packaging" : "Inventory_convert";

                if (_SBO_Form.Items.Item("CHK_SPRO").Specific.Checked)
                {
                    // ------------------ SPECIAL PRODUCTION ORDER
                    // Prepare a list of line items for the "Production Order" document
                    List<NSC_DI.SAP.ProductionOrder_OLD.StandardProductionOrderComponentLineItem> components =
                        new List<NSC_DI.SAP.ProductionOrder_OLD.StandardProductionOrderComponentLineItem>();


                    // Bill of Materials
                    foreach (var item in NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(GetItemCodeOfItemToBuild()))
                    {
                        components.Add(
                                       new NSC_DI.SAP.ProductionOrder_OLD.StandardProductionOrderComponentLineItem()
                                       {
                                           ItemCodeForItemBeingDestroyed = item.ItemCode,
                                           BaseQuantity = item.Quantity,
                                           PlannedQuantity = quantityToCreate,
                                           SourceWarehouseCode = item.WarehouseCode
                                       }
                            );
                    }

                    // Create a Production Order based on the form
                    newPDOKey = NSC_DI.SAP.ProductionOrder_OLD.CreateStandardProductionOrder(GetItemCodeOfItemToBuild()
                                                                                             , quantityToCreate
                                                                                             , destinationWarehouseCode  //10824
                                                                                             , dueDate
                                                                                             , components
                                                                                             , IsCannabisItem: "Y"
                                                                                             , pRemarks: Remarks
                                                                                             , pProcess: process
                        );

                    // set the isProductionBatch 
                    NSC_DI.SAP.ProductionOrder.SetUDF(newPDOKey, "U_NSC_isProductionBatch", _SBO_Form.Items.Item("chkIsPrdBa").Specific.Checked ? "Y" : "N");
 
                    // Update the newly created production order to "Released" status.
                    NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(newPDOKey, BoProductionOrderStatusEnum.boposReleased);
                }
                else
                {
                    //--------------------------------  STANDARD PRODUCTION ORDER
                    // create PdO
                    newPDOKey = NSC_DI.SAP.ProductionOrder.CreateFromBOM(GetItemCodeOfItemToBuild(), quantityToCreate, dueDate, destinationWarehouseCode, true, "Y",pRemarks: Remarks, pProcess: process);

                    // set the isProductionBatch 
                    NSC_DI.SAP.ProductionOrder.SetUDF(newPDOKey, "U_NSC_isProductionBatch", _SBO_Form.Items.Item("chkIsPrdBa").Specific.Checked ? "Y" : "N");

                    //--------------------------------
                    // set the batch IDs

                    // get the item and batch from the selected component matrix
                    string batch = oMat.Columns.Item(SelectedMatrixColumns.BatchNumber).Cells.Item(1).Specific.Value.ToString();
                    string item = oMat.Columns.Item(SelectedMatrixColumns.ItemCode).Cells.Item(1).Specific.Value.ToString();
                    string wh = oMat.Columns.Item(SelectedMatrixColumns.SourceWarehouseCode).Cells.Item(1).Specific.Value.ToString();
                    var dt = NSC_DI.UTIL.SQL.DataTable($"SELECT ItemCode, LineNum FROM WOR1 WHERE DocEntry = {newPDOKey} AND IssueType = 'M' AND PlannedQty > 0");
                    
                    foreach (System.Data.DataRow row in dt.Rows)
                    {
                        // batch ID was selected on the matrix. Also sets the row to the correct warehouse.
                        if (row["ItemCode"].ToString() == item)
                            NSC_DI.SAP.ProductionOrder.AddBatchSN(newPDOKey, item, Convert.ToInt32(row["LineNum"]), wh, batch);
                        else
                            if (NSC_DI.UTIL.Options.Value.GetSingle("Packaging PP - Issue all") == "Y") NSC_DI.SAP.ProductionOrder.AddBatchSN(newPDOKey, row["ItemCode"].ToString());
                    }
                }
                //--------------------------------
                if (_SBO_Form.Items.Item("chkAllowDs").Specific.Checked)
                    NSC_DI.SAP.ProductionOrder.IssueManual(newPDOKey, true, GetTotalQuantityFromSelected()); // Issue all cannabis items that have a batch assigned.
                else
                    NSC_DI.SAP.ProductionOrder.IssueManual(newPDOKey);
                //if (NSC_DI.UTIL.Options.Value.GetSingle("Packaging PP - Issue all") == "Y")
                //{
                //    NSC_DI.SAP.ProductionOrder.IssueManual(newPDOKey); // Issue all cannabis items
                //}
                //else
                //{
                //    // Issue selected cannabis items
                //    if (IssueBatchesUsedForPackage(newPDOKey) == false)
                //    {
                //        Globals.oApp.StatusBar.SetText("Failed to issue goods!");
                //        return;
                //    }
                //}

                    //Send a message to the client
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                Globals.oApp.StatusBar.SetText("Successfully created production order!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                //Clear the form out.
                ResetForm();
            }
            catch (NSC_DI.SAP.B1Exception ex)
            {
                Globals.oApp.MessageBox("Document was not created:" + Environment.NewLine + ex.Message);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                NSC_DI.UTIL.Misc.KillObject(oMat);
            }
        }
        private void BTN_APPLY_Click_OLD()
        {
            try
            {
                Matrix MTX_PROD = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_PROD", _SBO_Form) as Matrix;
                EditText TXT_QTY = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QTY", _SBO_Form) as EditText;
                EditText TXT_DAYS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_DAYS", _SBO_Form) as EditText;
                ComboBox CMB_WHSE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WHSE", _SBO_Form) as ComboBox;

                // Validate Data is good to go for the Production Order.

                double quantityToCreate = 0;
                DateTime dueDate = DateTime.Now;

                int estimatedDays = 1;
                if (!int.TryParse(TXT_DAYS.Value, out estimatedDays))
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Please enter in a number for the days!");

                    // Focus the cursor in the offending input
                    TXT_DAYS.Active = true;
                    return;
                }

                if (string.IsNullOrEmpty(TXT_QTY.Value) || !double.TryParse(TXT_QTY.Value, out quantityToCreate))
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Something went wrong with the calculated quantity!");

                    return;
                }

                // Prepare a ComboBox helper
                VERSCI.ComboBox ComboBox_Helper = new VERSCI.ComboBox(Globals.oApp, Globals.oCompany, _SBO_Form);

                // Grab the value from the ComboBox
                string destinationWarehouse = CMB_WHSE.Value.ToString();

                // Get the warehouse code from the description
                string destinationWarehouseCode = ComboBox_Helper.GetComboBoxValueOrDescription(cFormID, "CMB_WHSE", null, destinationWarehouse);

                dueDate = DateTime.Now.AddDays(estimatedDays);

                // Prepare a list of line items for the "Production Order" document
                List<NSC_DI.SAP.ProductionOrder_OLD.StandardProductionOrderComponentLineItem> components =
                    new List<NSC_DI.SAP.ProductionOrder_OLD.StandardProductionOrderComponentLineItem>();


                // Bill of Materials
                foreach (var item in NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(GetItemCodeOfItemToBuild()))
                {
                    components.Add(
                                   new NSC_DI.SAP.ProductionOrder_OLD.StandardProductionOrderComponentLineItem()
                                   {
                                       ItemCodeForItemBeingDestroyed = item.ItemCode,
                                       BaseQuantity = item.Quantity,
                                       PlannedQuantity = quantityToCreate,
                                       SourceWarehouseCode = item.WarehouseCode
                                   }
                        );
                }

                // Create a Production Order based on the form
                int newPDOKey = NSC_DI.SAP.ProductionOrder_OLD.CreateStandardProductionOrder(GetItemCodeOfItemToBuild()
                                                                                         , quantityToCreate
                                                                                         , destinationWarehouseCode
                                                                                         , dueDate
                                                                                         , components
                                                                                         , IsCannabisItem: "Y"
                    );

                // Update the newly created production order to "Released" status.
                NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(newPDOKey, BoProductionOrderStatusEnum.boposReleased);

                // Issue selected cannabis items
                if (IssueBatchesUsedForPackage(newPDOKey))
                {

                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Successfully created production order!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                    // Clear the form out.
                    ResetForm();

                    return;
                }
                else
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Failed to issue goods!");
                }
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private bool IssueBatchesUsedForPackage(int productionOrderKey)
        {
            // issue the batch info for the selected components only
            SAPbobsCOM.ProductionOrders oPdO = null;
            SAPbouiCOM.Matrix oMat = null;
            try
            {
                oPdO = (SAPbobsCOM.ProductionOrders)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);

                // Issue From Production
                string itemCode         = GetItemCodeOfSelected();
                double totalQuantity    = GetTotalQuantityFromSelected();
                string sourceWarehouse  = GetSourceWarehouseOfSelected();
                List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines> ListOfGoodsIssuedLines = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();
                oMat = _SBO_Form.Items.Item("MTX_SELECT").Specific;

                for (int i = 1; i <= oMat.RowCount; i++)
                {
                    string batchNumber = oMat.Columns.Item(SelectedMatrixColumns.BatchNumber).Cells.Item(i).Specific.Value.ToString();
                    double batchQuantity = Convert.ToDouble(oMat.Columns.Item(SelectedMatrixColumns.Quantity).Cells.Item(i).Specific.Value);

                    List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches> ListOfGoodsIssuedLineBatches = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>();
                    ListOfGoodsIssuedLineBatches.Add(new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches() { BatchNumber = batchNumber, Quantity = batchQuantity });
 
                    // Add a new item to the list of Goods Issued Lines
                    ListOfGoodsIssuedLines.Add(
                                               new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
                                               {
                                                   ItemCode = itemCode,
                                                   Quantity = totalQuantity,
                                                   WarehouseCode = sourceWarehouse,
                                                   BatchLineItems = ListOfGoodsIssuedLineBatches
                                               }
                        );
                }

                if (ListOfGoodsIssuedLines.Count == 0) return true;

                // Create an "Issue From Production" document
                NSC_DI.SAP.IssueFromProduction.Create(productionOrderKey, DateTime.Now, ListOfGoodsIssuedLines, "Packaging Planner");

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }

        private bool IssueBatchesUsedForPackage_OLD(int productionOrderKey)
		{
			try
			{
				// Grab the Matrix from the form 
				Matrix MTX_SELECT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_SELECT", _SBO_Form);

				// Issue From Production
				string itemCode         = GetItemCodeOfSelected();
				double totalQuantity    = GetTotalQuantityFromSelected();
				string sourceWarehouse  = GetSourceWarehouseOfSelected();
				List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches> ListOfGoodsIssuedLineBatches = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>();

				for (int i = 1; i < (MTX_SELECT.RowCount + 1); i++)
				{
					try
					{
						string batchNumber   = MTX_SELECT.Columns.Item(SelectedMatrixColumns.BatchNumber).Cells.Item(i).Specific.Value.ToString();
						double batchQuantity = Convert.ToDouble(MTX_SELECT.Columns.Item(SelectedMatrixColumns.Quantity).Cells.Item(i).Specific.Value);

						ListOfGoodsIssuedLineBatches.Add(new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches() {BatchNumber = batchNumber, Quantity = batchQuantity});
					}
					catch (Exception e)
					{
						//TODO-LOG: Log error.
					}
				}

				// Prepare a list of Goods Issued Lines
				List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines> ListOfGoodsIssuedLines = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();

				// Add a new item to the list of Goods Issued Lines
				ListOfGoodsIssuedLines.Add(
				                           new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
				                           {
					                           ItemCode         = itemCode,
					                           Quantity         = totalQuantity,
					                           WarehouseCode    = sourceWarehouse,
					                           BatchLineItems   = ListOfGoodsIssuedLineBatches
				                           }
					);

				// Create an "Issue From Production" document
				NSC_DI.SAP.IssueFromProduction.Create(productionOrderKey, DateTime.Now, ListOfGoodsIssuedLines, "Packaging Planner");

				return true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				//NSC_DI.UTIL.Misc.KillObject(MTX_SELECT);
				GC.Collect();
			}
		}

		private string GetItemCodeOfSelected()
        {
            Matrix MTX_SELECT = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_SELECT", _SBO_Form) as Matrix;

            // All Item codes should be the same so just grab the first item code in the Matrix.
            return MTX_SELECT.Columns.Item(SelectedMatrixColumns.ItemCode).Cells.Item(1).Specific.Value.ToString();
        }

        private string GetSourceWarehouseOfSelected()
        {
            Matrix MTX_SELECT = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_SELECT", _SBO_Form) as Matrix;

            // All Item codes should be the same so just grab the first item code in the Matrix.
            return MTX_SELECT.Columns.Item(SelectedMatrixColumns.SourceWarehouseCode).Cells.Item(1).Specific.Value.ToString();
        }

        private string GetItemCodeOfItemToBuild()
        {
            Matrix MTX_PROD = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_PROD", _SBO_Form) as Matrix;

            for (int i = 1; i < (MTX_PROD.RowCount + 1); i++)
            {
                if (MTX_PROD.IsRowSelected(i))
                {
                    return MTX_PROD.Columns.Item(0).Cells.Item(i).Specific.Value.ToString();
                }
            }
            return null;
        }

        private double GetTotalQuantityFromSelected()
        {
            double amountSelected = 0;
            Matrix MTX_SELECT = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_SELECT", _SBO_Form) as Matrix;

            for (int i = 1; i < (MTX_SELECT.RowCount + 1); i++)
            {
                try
                {
                    string Quantity = ((EditText)MTX_SELECT.Columns.Item(SelectedMatrixColumns.Quantity).Cells.Item(i).Specific).Value;
                    amountSelected += double.Parse(Quantity);
                }
                catch (Exception e)
                {
                    Globals.oApp.StatusBar.SetText("Error when attempting to parse quantity. Please re-check quantity fields!");

                    break;
                }
            }

            return amountSelected;
        }

        private void ResetForm()
        {
            // Getting needed references

            Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form) as Matrix;
            Matrix MTX_SELECT = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_SELECT", _SBO_Form) as Matrix;
            Matrix MTX_PROD = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_PROD", _SBO_Form) as Matrix;
            Button BTN_SELECT = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_SELECT", _SBO_Form) as Button;
            Button BTN_LOOKUP = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_LOOKUP", _SBO_Form) as Button;
            StaticText LBL_PROD = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.StaticText, "LBL_PROD", _SBO_Form) as StaticText;
            EditText TXT_LOOKUP = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", _SBO_Form) as EditText;
            EditText TXT_QTY = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_QTY", _SBO_Form) as EditText;
            Button BTN_APPLY = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_APPLY", _SBO_Form) as Button;
            EditText TXT_DAYS = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_DAYS", _SBO_Form) as EditText;
            EditText TXT_SITM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_SITM", _SBO_Form) as EditText;
            Folder TAB_FG = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_1", _SBO_Form) as Folder;
            Folder TAB_WIP = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_2", _SBO_Form) as Folder;
            Folder TAB_ALL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_3", _SBO_Form) as Folder;
            Folder TAB_SELECT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_4", _SBO_Form) as Folder;

            // Freeze the Form UI
            _SBO_Form.Freeze(true);

            TXT_LOOKUP.Active = true;

            MTX_MAIN.Item.Visible = true;
            MTX_SELECT.Item.Visible = false;
            MTX_PROD.Item.Visible = false;
            LBL_PROD.Item.Visible = false;
            TXT_SITM.Item.Visible = false;
            _SBO_Form.Items.Item("txtItemNam").Visible = false;
            BTN_LOOKUP.Item.Enabled = true;

            BTN_APPLY.Item.Enabled = false;

            //Show Tabs
            TAB_FG.Item.Visible = true;
            TAB_WIP.Item.Visible = true;
            TAB_ALL.Item.Visible = true;
            TAB_SELECT.Item.Visible = false;

            try
            {
                // Determine if this key press was of the same Item Group we had selected previously.
                bool deselectCurrent = false;
                List<int> rowsToReselect = new List<int>();
                for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
                {
                    MTX_MAIN.SelectRow(i, false, false);
                }

                // Reset Data in form.
                BTN_SELECT.Item.Specific.Caption = "Next - Choose Product";
                TXT_QTY.Value = "";
                //Load_Combobox_Warehouses();
                TXT_DAYS.Value = "1";
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

            // Un-Freeze the Form UI
            _SBO_Form.Freeze(false);

            // Reload the matrix.
            Load_Matrix(_SBO_Form);
        }

        private void BTN_LOOKUP_ITEM_PRESSED()
        {
            // Grad the textbox for the barcode scanner from the form UI
            EditText TXT_LOOKUP = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_LOOKUP", _SBO_Form);

            // If nothing was passed in the textbox, just return
            if (TXT_LOOKUP.Value.Length == 0)
            {
                return;
            }

            // Grab the matrix from the form UI
            Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form) as Matrix;

            // Keep track of the warehouse ID from the barcode (if passed)
            string WarehouseID = "";

            // If the first four characters of the barcode are "WHSE", then the rest of the barcode contains the warehouse code.
            if (TXT_LOOKUP.Value.Length >= 4 && TXT_LOOKUP.Value.Substring(0, 4) == "WHSE")
            {
                WarehouseID = TXT_LOOKUP.Value.Replace("WHSE-", "");
            }

            // Keep track if anything was selected.  At the end, if nothing was selected, click on the tab for that warehouse and select the plants in that warehouse
            bool WasAnythingSelected = false;

            // Clear out the current Selections
            for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
            {
                MTX_MAIN.SelectRow(i, false, false);
            }

            // For each row already selected
            for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
            {
                // Grab the row's Plant ID column
                string uniqueIdentifier = ((EditText)MTX_MAIN.Columns.Item(SelectedMatrixColumns.ItemCode).Cells.Item(i).Specific).Value;

                //// Grab the row's Warehouse Code column
                //string warehouseID = ((SAPbouiCOM.EditText)MTX_MAIN.Columns.Item(MatrixColumns.WhsCode).Cells.Item(i).Specific).Value;

                // Grab the row's Stated ID column
                string plantsStateID =((SAPbouiCOM.EditText)MTX_MAIN.Columns.Item(SelectedMatrixColumns.StateID).Cells.Item(i).Specific).Value;

                // Grab the row's Stated ID column
                string plantsBatchID = ((SAPbouiCOM.EditText)MTX_MAIN.Columns.Item(SelectedMatrixColumns.BatchNumber).Cells.Item(i).Specific).Value;

                // If the plant's ID matches the scanned barcode
                if (TXT_LOOKUP.Value == uniqueIdentifier || TXT_LOOKUP.Value == plantsStateID || TXT_LOOKUP.Value == plantsBatchID)
                {
                    // Select the row where the plant ID's match
                    MTX_MAIN.SelectRow(i, true, true);

                    WasAnythingSelected = true;
                    ((EditText)MTX_MAIN.Columns.Item(SelectedMatrixColumns.Quantity).Cells.Item(i).Specific).Active = true;
                }
            }

            // Empty the barcode scanner field
            TXT_LOOKUP.Value = "";

            if (!WasAnythingSelected)
            {
             
            }

            // Focus back into the barcode scanner's text field
            TXT_LOOKUP.Active = true;
        }

        private void TXT_SITM_ChooseFromList_Selected(Form pForm, ChooseFromListEvent pChooseFromListEvent)
        {
            SAPbouiCOM.ChooseFromList oChooseFromList = pForm.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

            if (pChooseFromListEvent.BeforeAction == false)
            {
                DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

                try
                {
                    string oItemCode = oDataTable.GetValue(0, 0).ToString();
                    string oItemSysSerial = oDataTable.GetValue(1, 0).ToString();

                    string itemCode = oDataTable.GetValue("ItemCode", 0).ToString();
                    string itemName = oDataTable.GetValue("ItemName", 0).ToString();

                    // Set the userdatasource for the name textbox
                    pForm.DataSources.UserDataSources.Item("UDS_SITM").ValueEx = Convert.ToString(itemCode);
                    //_SBO_Form.DataSources.UserDataSources.Item("UDS_SITM").ValueEx = Convert.ToString(itemName);
                    pForm.Items.Item("txtItemNam").Specific.Value = itemName;
                }
                catch (Exception ex)
                {
                    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }
            }
        }

        private void PROD_MATRIX_ITEM_PRESSED(ItemEvent SAP_UI_ItemEvent)
        {
            // Get references from the form.
            EditText TXT_QTY = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_QTY", _SBO_Form) as EditText;
            Matrix MTX_PROD = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_PROD", _SBO_Form);
            Button BTN_APPLY = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Button, "BTN_APPLY", _SBO_Form) as Button;

            // Update the Quanity To Create Text Field based on the Quantity the Product to create consumes per Product created.
            double usedPerPackage = 1;
            try
            {
                string UsedPerPackage = ((EditText)MTX_PROD.Columns.Item(ProductMatrixColumns.UsedPerPackage).Cells.Item(SAP_UI_ItemEvent.Row).Specific).Value;
                usedPerPackage = double.Parse(UsedPerPackage);

                BTN_APPLY.Item.Enabled = false;
             
                double ttlQuan = GetTotalQuantityFromSelected();
                // We don't want to devide by zero.
                if (usedPerPackage <= 0)
                    usedPerPackage = 1;
                // if the used-per is 1 sets it to the amount the user brought over, otherwise divides the amount the user brought over by used-per
                if (usedPerPackage != 1)               
                    TXT_QTY.Value = (Math.Floor(ttlQuan / usedPerPackage)).ToString();               
                if (usedPerPackage == 1)
                    TXT_QTY.Value = ttlQuan.ToString();

                BTN_APPLY.Item.Enabled = true;
            }
            catch (Exception e)
            {
                // Send a message to the client
                Globals.oApp.StatusBar.SetText("Something went wrong trying to guess the quantity of product you are building!", BoMessageTime.bmt_Short
                );
            }
        }

        private void MTX_MAIN_Click(ItemEvent SAP_UI_ItemEvent)
        {
            // Find the matrix in the form UI
            Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

            try
            {
                //  ver 32.002.9 - only allow user to select one row if "Special Production" is unchecked
                if (_SBO_Form.Items.Item("CHK_SPRO").Specific.Checked == false) return;

                string ItemCodeOfNewSelectedRow = ((EditText)MTX_MAIN.Columns.Item(SelectedMatrixColumns.ItemCode).Cells.Item(SAP_UI_ItemEvent.Row).Specific).Value;

                // Determine if this key press was of the same Item Group we had selected previously.
                bool deselectCurrent = false;
                List<int> rowsToReselect = new List<int>();
                for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
                {
                    string ItemCodeToCompare = ((EditText)MTX_MAIN.Columns.Item(SelectedMatrixColumns.ItemCode).Cells.Item(i).Specific).Value;
                    // If the current row is now selected we can just continue looping.
                    if (!MTX_MAIN.IsRowSelected(i) || i == SAP_UI_ItemEvent.Row)
                        continue;

                    rowsToReselect.Add(i);

                    CheckBox oChkBox_SpecialProd = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.CheckBox, "CHK_SPRO", _SBO_Form) as CheckBox;
                    bool SpecialProdOrder = oChkBox_SpecialProd.Checked;
                   
                    if (!ItemCodeOfNewSelectedRow.Equals(ItemCodeToCompare))
                    {
                        if (SpecialProdOrder == true)
                        {
                            deselectCurrent = false;
                        }
                        else
                        {
                            deselectCurrent = true;
                        }
                    }
                }

                if (deselectCurrent)
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText("Item Code must be of the same type!", BoMessageTime.bmt_Short);

                    MTX_MAIN.SelectRow(SAP_UI_ItemEvent.Row, false, false);
                    return;
                }
            }
            catch (Exception e)
            {
                //TODO-LOG: Log error?
            }
        }

        private void MATRIX_LINK_PRESSED(ItemEvent SAP_UI_ItemEvent, string MatrixToUse)
        {
            // Attempt to grab the selected row ID
            int SelectedRowID = SAP_UI_ItemEvent.Row;

            if (SelectedRowID > 0)
            {
                // Which column stores the ID
                int ColumnIDForIDOfItemSelected = 0;

                // Get the ID of the note selected
                string ItemSelected = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, MatrixToUse, _SBO_Form).Columns.Item(ColumnIDForIDOfItemSelected).Cells.Item(SelectedRowID).Specific.Value.ToString();

                Form formProductionOrder = null;
                try
                {
                    formProductionOrder = Globals.oApp.Forms.GetForm("150", 0);
                    formProductionOrder.Select();
                }
                catch (Exception e)
                {
                    // Open up the Item Master Data form
                    Globals.oApp.ActivateMenuItem("3073");

                    // Grab the "Item Master" form from the UI
                    formProductionOrder = Globals.oApp.Forms.GetForm("150", 0);

                    // Freeze the "Item Master" form
                    formProductionOrder.Freeze(true);

                    // Change the "Item Master" form to find mode
                    formProductionOrder.Mode = BoFormMode.fm_FIND_MODE;

                    // Insert the Item Master ID in the appropriate text field
                    ((EditText)formProductionOrder.Items.Item("5").Specific).Value = ItemSelected;

                    // Click the "Find" button
                    ((Button)formProductionOrder.Items.Item("1").Specific).Item.Click();

                    // Un-Freeze the "Production Order" form
                    formProductionOrder.Freeze(false);
                }
            }
        }
    }
}