﻿using System;
using System.Collections.Generic;
using System.Text;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using System.Linq;
using Matrix = SAPbouiCOM.Matrix;

namespace NavSol.Forms.Product_Processing
{
    class F_BatchManager : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "NSC_BATCH_MNG";

        private const string BATCH_PREFIX = "B";

        protected class BatchItemsGroup
        {
            public List<BatchItem> BatchItems = new List<BatchItem>();
            public double TotalQuantity;
            public string SourceWarehouseCode;
            public string ItemCode;
        }

        protected class BatchItem
        {
            public double QuantityToBatch;
            public string BatchNumber;
        }

        // Keeping a reference of all the Columns that exist on our table 
        // so I can get the index without having to loop through all the columns to find the correct column.
        private Dictionary<string, int> MatrixColumns = new Dictionary<string, int>()
        {
            {"ItemCode", 0}, {"ItemName", 1}, {"OnHand", 2}, {"Quantity", 3}, {"WhsName", 4}, {"WhsCode", 5}, {"BatchNum", 6}, {"HarvName", 7}
        };

        // FOR OLD CODE
        public VERSCI.Forms _VirSci_Helper_Form;
        public SAPbouiCOM.Application _SBO_Application;
        public SAPbobsCOM.Company _SBO_Company;
        public SAPbouiCOM.Form _SBO_Form;
        public string FormUID { get; set; }
        private NSC_DI.Globals.ItemGroupTypes itemGroupType = NSC_DI.Globals.ItemGroupTypes.Sample;
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPressed(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            //NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------
        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            switch (pVal.ItemUID)
            {
                case "MTX_MAIN":
                    HandleMatrix_Click(pVal.Row);
                    break;
                case "BTN_FNSH":
                    BTN_FNSH_Click(oForm);
                    break;

                case "btnBatCFL":
                    CFL_Item_Branches(oForm, pVal);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_KEY_DOWN
        [B1Listener(BoEventTypes.et_KEY_DOWN, false, new string[] { cFormID })]
        public virtual void OnAfterKeyDown(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            // Capturing the Down Arrow Button Press.
            //if (pVal.ItemUID == "txtBatch" && pVal.CharPressed == 9) HandleTab(oForm, "txtBatch");
            if (pVal.ItemUID == "txtWhse" && pVal.CharPressed == 9) HandleTab(oForm, "txtWhse");

            if (pVal.ItemUID == "txtBatch" && pVal.CharPressed == 9) CFL_Item_Branches(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_FORM_RESIZE
        [B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
        public virtual void OnAfterFormResize(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            FormReSize(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (IChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, false, new string[] { cFormID })]
        public virtual void OnAfterValidate(ItemEvent pVal)
        {
            if (pVal.InnerEvent == true) return;
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public F_BatchManager() : this(Globals.oApp, Globals.oCompany) { }

        public F_BatchManager(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

        public void Form_Load()
        {
            Matrix oMat = null;

            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_MAIN", @"seedbank-icon.bmp");

                _SBO_Form.Items.Item("txtBatSize").Specific.Value = NSC_DI.UTIL.Options.Value.GetSingle("Max Lot Size - TRIM");
                _SBO_Form.Items.Item("staMaxUOM").Specific.Caption = NSC_DI.UTIL.Options.Value.GetSingle("Max Lot Size - TRIM", 2);

                // Wh CFL   
                CommonUI.Forms.CreateUserDataSource(_SBO_Form, "txtWHCode", "dsWHCode", BoDataType.dt_SHORT_TEXT, 50);
                //CommonUI.Forms.CreateUserDataSource(_SBO_Form, "txtWHName", "dsWHName", BoDataType.dt_SHORT_TEXT, 50);
                CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_NONE, "Inactive", BoConditionOperation.co_EQUAL, "N");
                CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, "PRO", 1);
                CommonUI.CFL.AddCon(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_OR, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, "CUR", 0, 1);
                CommonUI.CFL.AddCon_Branches(_SBO_Form, "CFL_WH", BoConditionRelationship.cr_AND);    //10823

                // Item CFL  
                if (Globals.BranchDflt < 0)
                {
                    CommonUI.Forms.CreateUserDataSource(_SBO_Form, "TXT_ITEMC", "dsItmCode", BoDataType.dt_SHORT_TEXT, 50);
                    CommonUI.Forms.CreateUserDataSource(_SBO_Form, "txtBatch", "dsItmName", BoDataType.dt_SHORT_TEXT, 50);
                    NavSol.CommonUI.CFL.Create(_SBO_Form, "CFL_ITEM", BoLinkedObject.lf_Items);
                    CommonUI.CFL.AddCon(_SBO_Form, "CFL_ITEM", BoConditionRelationship.cr_NONE, "validFor", BoConditionOperation.co_EQUAL, "Y");
                    CommonUI.CFL.AddCon(_SBO_Form, "CFL_ITEM", BoConditionRelationship.cr_AND, $"U_{Globals.SAP_PartnerCode}_" + "Batchable", BoConditionOperation.co_EQUAL, "Y");
                    CommonUI.CFL.AddCon(_SBO_Form, "CFL_ITEM", BoConditionRelationship.cr_AND, $"{NSC_DI.SAP.Items.Property.GetFieldName("Template")}", BoConditionOperation.co_EQUAL, "N");
                    _SBO_Form.Items.Item("txtBatch").Specific.ChooseFromListUID = "CFL_ITEM";

                    _SBO_Form.Items.Item("btnBatCFL").Visible = false;
                }

                oMat = _SBO_Form.Items.Item("MTX_MAIN").Specific;
                oMat.SelectionMode = BoMatrixSelect.ms_Auto;

                // Loading all Matrices. Passing true loads all three.
                Load_Matrix();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                _SBO_Form.VisibleEx = true;

                NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }

        private void CFL_Item_Branches(Form pForm, ItemEvent pVal)
        {
            try
            {
                if (Globals.BranchDflt < 0) return;

                // if they tab off of the field and it contains a value, then return
                if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pForm.Items.Item("txtBatch").Specific.Value != "") return;

                var wh = pForm.Items.Item("txtWHCode").Specific.Value;
                if (pForm.Items.Item("txtWHName").Specific.Value == "")
                {
                    Globals.oApp.MessageBox("You must select a Warehouse 1st.");
                    return;
                }
                var branch = NSC_DI.SAP.Warehouse.GetBranch(wh);
                var sql = $@"
SELECT DISTINCT OITM.ItemName, OITM.ItemCode --, OWHS.WhsCode, OWHS.WhsName, OITW.OnHand
  FROM OITM
 INNER JOIN OITW ON OITM.ItemCode = OITW.ItemCode
 INNER JOIN OWHS ON OITW.WhsCode = OWHS.WhsCode
 WHERE OITM.QryGroup64 = 'N' AND OITM.validFor = 'Y' AND OITM.U_NSC_Batchable = 'Y' AND OWHS.BPLid = '{branch}'
 ORDER BY OITM.ItemName";

                F_CFL_GRID.CflCB = delegate (string pCallingFormUid)
                {
                    _SBO_Form = CommonUI.Forms.GetFormFromUID(pCallingFormUid);
                    var itemCode = _SBO_Form.Items.Item("TXT_ITEMC").Specific.Value;
                    Load_Matrix(NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(itemCode));
                };

                F_CFL_GRID.FormCreate("Batch Choose From List", sql, pForm.UniqueID, "txtBatch,TXT_ITEMC", "ItemName,ItemCode");


            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void CFL_Selected(Form pForm, IChooseFromListEvent pVal)
        {
            if (pVal.SelectedObjects == null) return;
            SAPbouiCOM.DataTable oDT = null;

            try
            {
                pForm.ChooseFromLists.Item(pVal.ChooseFromListUID);
                oDT = pVal.SelectedObjects;

                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_WH":
                        pForm.Items.Item("txtWHCode").Specific.Value = "";
                        pForm.DataSources.UserDataSources.Item("dsWHCode").ValueEx = oDT.GetValue("WhsCode", 0).ToString();
                        pForm.Items.Item("txtWHName").Specific.Value = "";
                        pForm.DataSources.UserDataSources.Item("UDS_WRHS").ValueEx = oDT.GetValue("WhsName", 0).ToString();
                        break;

                    case "CFL_ITEM": 
                        pForm.Items.Item("TXT_ITEMC").Specific.Value = "";
                        pForm.Items.Item("txtBatch").Specific.Value = "";
                        var val = oDT.GetValue("ItemName", 0).ToString();
                        pForm.DataSources.UserDataSources.Item("dsItmName").ValueEx = val;
                        val = oDT.GetValue("ItemCode", 0).ToString();
                        pForm.DataSources.UserDataSources.Item("dsItmCode").ValueEx = val;
                        _SBO_Form = pForm;
                        Load_Matrix(NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(val));
                        break;
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }

        private void HandleBatchSelectionCFL(Form pForm)
        {
            var title = "Select an Item";    // #2784 - change title

            var matrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
            {
                 new CommonUI.Matrix.MatrixColumn(){ Caption="Code", ColumnWidth=200, ColumnName="Code", ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=200, ColumnName="Name", ItemType= BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="OnHand", ColumnWidth=40, ColumnName="OnHand", ItemType= SAPbouiCOM.BoFormItemTypes.it_EDIT}
            };

            //int cflWidth = Math.Max(600, matrixColumns.Count * 200);
            //int cflHeight = 420;

            // Max Width is 80% of the users screen space.
            //This does not currently work correctly in the Web Browser version so we are hard coding the dimensions of the form.
            var width = 700;    //Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
            var height = 400;   //Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);

            var top = 40;       //(int)((Globals.oApp.Desktop.Height - height) / 0.7);
            var left = 380;     //(Globals.oApp.Desktop.Width - width) / 2;

            // And lastly the Query to populate the Matrix.
            //var sql = "SELECT OITM.ItemCode AS [Code], OITM.ItemName AS [Name],  OITM.OnHand AS [OnHand] FROM OITM WHERE (U_NSC_Sampleable = 'Y' OR U_NSC_Batchable = 'Y') AND " + NSC_DI.SAP.Items.Property.GetFieldName("Template") + " = 'N'";
            var sql = "SELECT OITM.ItemCode AS [Code], OITM.ItemName AS [Name],  OITM.OnHand AS [OnHand] FROM OITM WHERE U_NSC_Batchable = 'Y'";    // #2772 - Should just look at "Is Batchable"
            sql += $" AND OITM.{NSC_DI.SAP.Items.Property.GetFieldName("Template")} = 'N'"; // #2784 - filter out template items

            F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pResults)
            {
                _SBO_Form = CommonUI.Forms.GetFormFromUID(pCallingFormUid);
                BatchSelectionCFLCallBack(pForm, pResults);
            };
            Form oForm_Cfl = F_CFL.FormCreate(title, pForm.UniqueID, sql, matrixColumns, false, null, top, left, height, width);
        }

        public void BatchSelectionCFLCallBack(Form pForm, System.Data.DataTable results)
        {
            try
            {
                //_SBO_Form = CommonUI.Forms.GetFormFromUID(;

                EditText TXT_ITEMC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEMC", _SBO_Form) as EditText;

                EditText txtBatch = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtBatch", _SBO_Form) as EditText;

                // Select our current form.
                _SBO_Form.Select();

                TXT_ITEMC.Value = results.Rows[0]["Code"] as string;
                txtBatch.Value = results.Rows[0]["Name"] as string;

                string itemCode = results.Rows[0]["Code"] as string;
                // Get the quantity of the Item we are trying to create.
                Load_Matrix(NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(itemCode));
            }
            catch (Exception ex)
            {
                // What are you complaining about?
            }
        }

        private void HandleWarehouseSelectionCFL(Form pForm)
        {
            try
            {
                var title = "Select a Warehouse";   // #2785- change title

                // Specifying the columns we want the Superior CFL to have.
                var matrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
                {new CommonUI.Matrix.MatrixColumn(){ Caption="Code", ColumnWidth=200, ColumnName="Code", ItemType = BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=200, ColumnName="Name", ItemType= BoFormItemTypes.it_EDIT}};

                //int cflWidth = Math.Max(450, matrixColumns.Count * 150);
                //int cflHeight = 420;

                // Max Width is 80% of the users screen space.
                //This does not currently work correctly in the Web Browser version so we are hard coding the dimensions of the form.
                var width = 700;    //Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
                var height = 400;   //Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);

                var top = 40;       //(int)((Globals.oApp.Desktop.Height - height) / 0.7);
                var left = 380;     //(Globals.oApp.Desktop.Width - width) / 2;

                // And lastly the Query to populate the Matrix.
                var sql = "SELECT  WhsCode AS [Code],  WhsName AS [Name] FROM [OWHS] WHERE [U_NSC_WhrsType] IN ('PRO', 'CUR')";
                sql += " AND Inactive = 'N'";

                F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pResults)
                {
                    _SBO_Form = CommonUI.Forms.GetFormFromUID(pCallingFormUid);
                    WarehouseSelectionCFLCallBack(pResults);
                };


                Form oForm_Cfl = F_CFL.FormCreate(title, pForm.UniqueID, sql, matrixColumns, false, null, top, left, height, width);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public void WarehouseSelectionCFLCallBack(System.Data.DataTable results)
        {
            try
            {
                EditText TXT_WRHSC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtWHCode", _SBO_Form) as EditText;

                EditText txtWhse = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtWhse", _SBO_Form) as EditText;

                // Select our current form.
                _SBO_Form.Select();

                TXT_WRHSC.Value = results.Rows[0]["Code"] as string;
                txtWhse.Value = results.Rows[0]["Name"] as string;
            }
            catch (Exception ex)
            {
                // What are you complaining about?
            }
        }

        private int HandleMatrix_Click(int rowIndex)
        {
            int ret = 0;
            try
            {

                EditText TXT_TOTAL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_TOTAL", _SBO_Form);

                var total = GetSelectedTotal();

                Matrix CurrentMatrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form) as Matrix;

                //
                // Check limits
                //

                var itemCode = CommonUI.Forms.GetField<string>(_SBO_Form, "TXT_ITEMC");

                // TODO: handle wet processing

                var isTrim = NSC_DI.SAP.Items.Property.AnyYes(itemCode, "Trim Lot QA", "Dry Unbatched Trim");

                var isBud = NSC_DI.SAP.Items.Property.AnyYes(itemCode, "Flower Lot QA", "Dry Unbatched Cannabis", "Dry Cannabis", "Unbatched Ground Cannabis");


                string[] limit = null;
                string limitType = null;
                decimal quantity = 0M;
                var ItemUOM = NSC_DI.SAP.Items.GetField<string>(itemCode, "InvntryUom");

                if (isTrim)
                {
                    limit = NSC_DI.UTIL.Options.Value.Get("Max Lot Size - TRIM");
                    limitType = "Trim";
                }
                else if (isBud)
                {
                    limit = NSC_DI.UTIL.Options.Value.Get("Max Lot Size - BUD");
                    limitType = "Bud";
                }

                if (limit == null || decimal.TryParse(limit[0], out quantity) && quantity == 0)
                {
                    if (limit != null)  // if there is no limit, then skip the error message.
                    {
                        Globals.oApp.MessageBox("Production has been halted via User Settings.");
                        ret = 1;
                        return ret;
                    }
                }

                //---------------------------
                // don't check the entered quantity because there can be multiple batches
                return 0;

                if (limit != null && quantity > 0)
                {

                    string uom = "";

                    // Use the Unit of Measure if there is one                    
                    if (limit.Length > 1 && !string.IsNullOrEmpty(limit[1]?.Trim())) uom = limit[1];// Whitespace-Change !NSC_DI.UTIL.Strings.Empty(limit[1])

                    var limitValue = NSC_DI.UTIL.Units.ConvertWeight(quantity, uom, ItemUOM);

                    if (total > limitValue)
                    {
                        // Deselect the affected row
                        (_SBO_Form.Items.Item("MTX_MAIN").Specific as Matrix).SelectRow(rowIndex, false, true);

                        var message = $"{limitType} must not exceed {quantity} {uom} ( {limitValue}g ) of weight. Please see user options.";

                        Globals.oApp.StatusBar.SetText(message, BoMessageTime.bmt_Short);

                        throw new System.ComponentModel.WarningException();
                    }
                }
                CommonUI.Forms.SetField(_SBO_Form, "TXT_TOTAL", total.ToString());
            }
            catch (NSC_DI.UTIL.Units.UndefinedUnitException ex)
            {
                Globals.oApp.MessageBox(ex.Message + ". Please check the user options and try again");
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex.Message));
            }
            return ret;
        }

        private decimal GetSelectedTotal()
        {
            Matrix oMatrix = null;
            try
            {
                oMatrix = (_SBO_Form.Items.Item("MTX_MAIN").Specific as Matrix);
                decimal totalOfSelectedRows = 0;
                int i = oMatrix.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);

                while (i != -1)
                {
                    // Make sure the selections have valid input.

                    string qtyToParse = CommonUI.Forms.GetField<string>(_SBO_Form, "MTX_MAIN", i, MatrixColumns["Quantity"]);

                    var onHandValue = CommonUI.Forms.GetField<decimal>(_SBO_Form, "MTX_MAIN", i, MatrixColumns["OnHand"]);

                    if (!decimal.TryParse(qtyToParse, out decimal quantityToApply) || onHandValue < quantityToApply)
                    {
                        oMatrix.GetCellSpecific(MatrixColumns["Quantity"], i).Active = true;
                        Globals.oApp.StatusBar.SetText("Please enter a valid number!", BoMessageTime.bmt_Short);

                        throw new System.ComponentModel.WarningException();
                    }

                    totalOfSelectedRows += quantityToApply;

                    i = oMatrix.GetNextSelectedRow(i, BoOrderType.ot_RowOrder);
                }

                return totalOfSelectedRows;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMatrix);
            }
        }

        private void Load_Matrix()
        {
            try
            {
                // Prepare a Matrix helper
                VERSCI.Matrix MatrixHelper = new VERSCI.Matrix(Globals.oApp, _SBO_Form);

                // Load the matrix of tasks
                MatrixHelper.LoadDatabaseDataIntoMatrix("ItemTable", "MTX_MAIN", new List<VERSCI.Matrix.MatrixColumn>() {
                            new VERSCI.Matrix.MatrixColumn(){ Caption="Item Code", ColumnWidth=20, ColumnName="ItemCode", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Item Name", ColumnWidth=80, ColumnName="ItemName", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="On Hand", ColumnWidth=80, ColumnName="OnHand", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Quantity", ColumnWidth=80, ColumnName="Quantity", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Warehouse", ColumnWidth=80, ColumnName="WhsName", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="WarehouseCode", ColumnWidth=80, ColumnName="WhsCode", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Batch Number", ColumnWidth=80, ColumnName="BatchNum", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Harvest Name", ColumnWidth=80, ColumnName="HarvName", ItemType = BoFormItemTypes.it_EDIT}
                    }, @"
SELECT * 
FROM (
SELECT '99999999' AS [ItemCode]
,'99999999ABCDEFGHIJKLMNOPQRSTUVWXYZ' AS [ItemName]
, '1000000.00' AS OnHand
, '1000000.00' AS Quantity
, '99999999' AS WhsName
, '99999999' AS WhsCode
, '99999999' AS BatchNum
, SPACE(32) AS HarvName
) 
AS [ItemTable]
");

                // Remove the first item in the Matrix
                DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("ItemTable");
                DataTable.Rows.Remove(0);

                Matrix MTX_OITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);
                MTX_OITEM.LoadFromDataSource();

                // Find the Satte ID column to hide it
                foreach (Column oMatrixColumn in MTX_OITEM.Columns)
                {
                    if (oMatrixColumn.TitleObject.Caption == "WarehouseCode")
                    {
                        oMatrixColumn.Width = 1;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(sboTransfer);
                GC.Collect();
            }
        }

        private void Load_Matrix(List<NSC_DI.SAP.BillOfMaterials.BillOfMaterialLineItem> ListOfItemBatchItemCodes)
        {
            try
            {
                // Make sure we don't load the matrix when we don't have any BatchItems.
                if (ListOfItemBatchItemCodes.Count == 0)
                {
                    Load_Matrix();
                    return;
                }

                // Prepare a Matrix helper
                VERSCI.Matrix MatrixHelper = new VERSCI.Matrix(Globals.oApp, _SBO_Form);

                string sqlQuery = $@"
SELECT OITM.ItemCode, OITM.ItemName, OBTQ.Quantity AS OnHand, OBTQ.WhsCode, OBTN.DistNumber AS BatchNum,
  	   OBTQ.Quantity AS Quantity, OWHS.WhsName, OBTN.U_NSC_HarvestName AS HarvName
  FROM OBTN
  LEFT OUTER JOIN OBTQ ON OBTN.ItemCode = OBTQ.ItemCode AND OBTN.SysNumber = OBTQ.SysNumber
 INNER JOIN OITM ON OITM.ItemCode = OBTN.ItemCode AND [OITM].ManBtchNum = 'Y' AND [OBTQ].[Quantity] > 0
 INNER JOIN [OWHS] on [OWHS].[WhsCode] = [OBTQ].[WhsCode]
 WHERE ";

                //            string sqlQuery = @"
                //SELECT DISTINCT
                //[OITM].[ItemCode]
                //,[OITM].[ItemName]
                //,[OIBT].[Quantity] AS [OnHand]
                //,[OIBT].[WhsCode]
                //,[OIBT].[BatchNum]
                //,[OIBT].[Quantity] AS [Quantity]
                //,[OWHS].[WhsName]
                //FROM [OITM]
                //JOIN [OIBT] on [OIBT].[ItemCode] = [OITM].[ItemCode]
                //    AND [OITM].ManBtchNum = 'Y' AND [OIBT].[Quantity] > 0
                //JOIN [OBTN] on [OBTN].[ItemCode] = [OITM].[ItemCode]
                //JOIN [OWHS] on [OWHS].[WhsCode] = [OIBT].[WhsCode]
                //WHERE ";

                // We need to filter the Matrix by ItemCode 
                StringBuilder queryFilter = new StringBuilder("");
                EditText TXT_SelectedItem = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEMC", _SBO_Form);
                //queryFilter.AppendLine(string.Format(" [OITM].[ItemCode] = '{0}'", TXT_SelectedItem.Value));
                for (int i = 0; i < ListOfItemBatchItemCodes.Count; i++)
                {
                    string itemCode = ListOfItemBatchItemCodes[i].ItemCode;
                    //Load all Batchable Items
                    if (string.IsNullOrEmpty(queryFilter.ToString()))
                        queryFilter.AppendLine(string.Format(" [OITM].[ItemCode] = '{0}'", itemCode));
                    else
                        queryFilter.AppendLine(string.Format(" OR [OITM].[ItemCode] = '{0}'", itemCode));
                }

                if (string.IsNullOrEmpty(queryFilter.ToString()))
                {
                    queryFilter.AppendLine(" [OITM].[ItemCode] = 'NoResultsFound'");
                }

                // Combine the filter with the original query.
                sqlQuery += queryFilter.ToString();

                if (Globals.BranchDflt >= 0)
                {
                    var wh = _SBO_Form.Items.Item("txtWHCode").Specific.Value;
                    var br = NSC_DI.SAP.Warehouse.GetBranch(wh);
                    sqlQuery += $" AND OWHS.BPLid = {br}"; //10823-2
                }
                // Load the matrix of tasks
                MatrixHelper.LoadDatabaseDataIntoMatrix("ItemTable", "MTX_MAIN", new List<VERSCI.Matrix.MatrixColumn>() {
                            new VERSCI.Matrix.MatrixColumn(){ Caption="Item Code", ColumnWidth=20, ColumnName="ItemCode", ItemType= BoFormItemTypes.it_LINKED_BUTTON}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Item Name", ColumnWidth=50, ColumnName="ItemName", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="On Hand", ColumnWidth=80, ColumnName="OnHand", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Quantity", ColumnWidth=80, ColumnName="Quantity", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Warehouse", ColumnWidth=80, ColumnName="WhsName", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="WarehouseCode", ColumnWidth=0, ColumnName="WhsCode", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Batch Number", ColumnWidth=80, ColumnName="BatchNum", ItemType = BoFormItemTypes.it_EDIT}
                            ,new VERSCI.Matrix.MatrixColumn(){ Caption="Harvest Name", ColumnWidth=80, ColumnName="HarvName", ItemType = BoFormItemTypes.it_EDIT}
                    }, sqlQuery);

                Matrix MTX_OITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);
                MTX_OITEM.LoadFromDataSource();

                if (MTX_OITEM.RowCount < 1) Globals.oApp.SetStatusBarMessage("There is no inventory for items that build the selected Item.");

                CommonUI.Matrix.SetLink(MTX_OITEM, 0, SAPbouiCOM.BoLinkedObject.lf_Items);   // #3416 - add drill down

                // Find the State ID column to hide it
                foreach (Column oMatrixColumn in MTX_OITEM.Columns)
                {
                    if (oMatrixColumn.TitleObject.Caption == "WarehouseCode" || oMatrixColumn.TitleObject.Caption == "ItemType")
                    {
                        oMatrixColumn.Width = 1;
                        break;
                    }
                }
            }

            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(sboTransfer);
                GC.Collect();
            }
        }

        private void BTN_FNSH_Click_NEW()
        {
            // upgrade code to use the new lib code - not finished yet.

            double totalOfSelectedRows = 0;

            // Show the "Loading" form.
            var oForm = F_Waiting.FormCreate(true);

            F_Waiting.SetProgressBar(oForm, 1, "Beginning Process...");

            // Grab the matrix from the form UI
            Matrix CurrentMatrix = _SBO_Form.Items.Item("MTX_MAIN").Specific;

            F_Waiting.SetProgressBar(oForm, 5, "Validating Form...");

            #region Validation
            double quantityToApply = 0;
            string destinationWarehouse = "";
            string creationItemCode = "";

            EditText TXT_WRHSC = (EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtWHCode", _SBO_Form);

            EditText TXT_ITEMC = (EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEMC", _SBO_Form);

            if (string.IsNullOrEmpty(TXT_ITEMC.Value))
            {
                Globals.oApp.StatusBar.SetText("Please select an Item!", BoMessageTime.bmt_Short);

                try
                {
                    ((EditText)(CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITMCFL", _SBO_Form))).Active = true;
                }
                catch (Exception ex) {/* */}

                return;
            }

            if (string.IsNullOrEmpty(TXT_WRHSC.Value))
            {
                Globals.oApp.StatusBar.SetText("Please select a Warehouse!", BoMessageTime.bmt_Short);

                try
                {
                    ((EditText)(CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHS", _SBO_Form))).Active = true;
                }
                catch (Exception ex) {/* */}

                return;
            }

            destinationWarehouse = TXT_WRHSC.Value;
            creationItemCode = TXT_ITEMC.Value;
            var sourceBatchID = "";
            #endregion

            Dictionary<string, BatchItemsGroup> GroupedBatchItems = new Dictionary<string, BatchItemsGroup>();
            List<BatchItem> AllSelectedItems = new List<BatchItem>();
            // For each row already selected
            try
            {
                F_Waiting.SetProgressBar(oForm, 25, "Gathering Selected Fields...");

                int i = CurrentMatrix.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);
                while (i != -1)
                {
                    BatchItem newBatchItem = new BatchItem();
                    string qtyToParse = null;
                    // Make sure the selections have valid input.
                    if (CurrentMatrix.Columns.Item(MatrixColumns["Quantity"]).Cells.Item(i).Specific.Value.Length > 0)
                    {
                        qtyToParse = ((EditText)CurrentMatrix.Columns.Item(MatrixColumns["Quantity"]).Cells.Item(i).Specific).Value;
                    }
                    else
                    {
                        Globals.oApp.StatusBar.SetText("Please enter a valid number!", BoMessageTime.bmt_Short);
                        F_Waiting.Form_Close(oForm);
                        return;
                    }
                    double onHandValue = double.Parse(((EditText)CurrentMatrix.Columns.Item(MatrixColumns["OnHand"]).Cells.Item(i).Specific).Value);
                    if (!double.TryParse(qtyToParse, out quantityToApply) || onHandValue < quantityToApply)
                    {
                        ((EditText)CurrentMatrix.Columns.Item(MatrixColumns["Quantity"]).Cells.Item(i).Specific).Active = true;

                        Globals.oApp.StatusBar.SetText("Please enter a valid number!", BoMessageTime.bmt_Short);

                        return;
                    }
                    if (quantityToApply == 0.0 || quantityToApply.ToString().Length == 0)
                    {
                        Globals.oApp.StatusBar.SetText("Please enter a valid number greater than zero!", BoMessageTime.bmt_Short);
                        F_Waiting.Form_Close(oForm);
                        return;
                    }

                    string ItemCode = ((EditText)CurrentMatrix.Columns.Item(MatrixColumns["ItemCode"]).Cells.Item(i).Specific).Value;
                    string SourceWarehouse = ((EditText)CurrentMatrix.Columns.Item(MatrixColumns["WhsCode"]).Cells.Item(i).Specific).Value;
                    newBatchItem.QuantityToBatch = quantityToApply;
                    newBatchItem.BatchNumber = ((EditText)CurrentMatrix.Columns.Item(MatrixColumns["BatchNum"]).Cells.Item(i).Specific).Value;

                    totalOfSelectedRows += quantityToApply;
                    string keyOfCurrent = ItemCode + SourceWarehouse;

                    if (sourceBatchID == "") sourceBatchID = newBatchItem.BatchNumber;

                    // Aggregating by Warehouse and Item Code.
                    // We want to keep track of Individual amounts and what batch they come from as well.
                    if (GroupedBatchItems.ContainsKey(keyOfCurrent))
                    {
                        // Add to our existing group.
                        GroupedBatchItems[keyOfCurrent].TotalQuantity += newBatchItem.QuantityToBatch;
                        GroupedBatchItems[keyOfCurrent].BatchItems.Add(newBatchItem);
                    }
                    else
                    {
                        // Create a new BatchItemGroup
                        BatchItemsGroup batchItemGroup = new BatchItemsGroup();
                        batchItemGroup.ItemCode = ItemCode;
                        batchItemGroup.SourceWarehouseCode = SourceWarehouse;
                        batchItemGroup.BatchItems.Add(newBatchItem);
                        batchItemGroup.TotalQuantity = newBatchItem.QuantityToBatch;
                        // Add to our group
                        GroupedBatchItems.Add(keyOfCurrent, batchItemGroup);
                    }
                    // This might be able to remove.
                    AllSelectedItems.Add(newBatchItem);
                    i = CurrentMatrix.GetNextSelectedRow(i, BoOrderType.ot_RowOrder);
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }

            // We have no selected Rows.
            if (AllSelectedItems.Count == 0) return;

            // Create a Batch Items Controller.. So we can get an available batch number.
            string newBatchId = NSC_DI.SAP.BatchItems.NextBatch(creationItemCode, BATCH_PREFIX);   // OP7926

            F_Waiting.SetProgressBar(oForm, 75, "Gathering Data for Documents Creation...");

            #region Create Components and GoodsIssueLines from our Groups
            // Using our Aggregated data in the components. -- For Production Order
            List<NSC_DI.SAP.ProductionOrder.SpecialProductionOrderComponentLineItem> components = new List<NSC_DI.SAP.ProductionOrder.SpecialProductionOrderComponentLineItem>();

            // Prepare a list of Goods Issued Lines
            List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines> ListOfGoodsIssuedLines = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();

            // For each group that we aggregated we are going to create a components for our Production Order
            // And a Goods Issued Line. The Goods Issued Line may have multiple Batches associated with it.
            foreach (BatchItemsGroup group in GroupedBatchItems.Values)
            {
                var lineNum = 0;
                components.Add
                (
                    new NSC_DI.SAP.ProductionOrder.SpecialProductionOrderComponentLineItem()
                    {
                        ItemCodeForItemOnBOM = group.ItemCode,
                        SourceWarehouseCode = group.SourceWarehouseCode,
                        BaseQuantity = 1,
                        PlannedQuantity = group.TotalQuantity
                    }
                );

                // Prepare a list of Batch line items for the 
                List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches> issueBatches = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>();

                foreach (BatchItem batchItem in group.BatchItems)
                {
                    issueBatches.Add
                    (
                        new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches()
                        {
                            BatchNumber = batchItem.BatchNumber,
                            Quantity = batchItem.QuantityToBatch
                        }
                    );
                }

                // Add a new item to the list of Goods Issued Lines
                ListOfGoodsIssuedLines.Add
                (
                    new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
                    {
                        BaseLine = lineNum++,
                        ItemCode = group.ItemCode,
                        Quantity = group.TotalQuantity,
                        WarehouseCode = group.SourceWarehouseCode,
                        BatchLineItems = issueBatches
                    }
                );
            }
            #endregion

            F_Waiting.SetProgressBar(oForm, 85, "Creating Production Order...");

            // Create a Disassembly Production Order based on the form
            var newPDOKwy = NSC_DI.SAP.ProductionOrder.CreateSpecialProdOrderAndIssue(creationItemCode
                , totalOfSelectedRows
                , destinationWarehouse
                , DateTime.Now
                , components
            //, newBatchId
            );

            // Update the Production Order to release.
            NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(newPDOKwy, BoProductionOrderStatusEnum.boposReleased);

            F_Waiting.SetProgressBar(oForm, 90, "Creating Goods Issued Documents...");

            // Create an "Issue From Production" document
            NSC_DI.SAP.IssueFromProduction.Create(newPDOKwy, DateTime.Now, ListOfGoodsIssuedLines, "Batch Manager");

            F_Waiting.SetProgressBar(oForm, 99, "Creating Goods Receipts...");
            #region Create Goods Receipt

            List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> ListOfGoodsReceiptLines = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();
            ListOfGoodsReceiptLines.Add(new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines()
            {
                ItemCode = creationItemCode,
                Quantity = totalOfSelectedRows,
                WarehouseCode = destinationWarehouse,
                ListOfBatchLineItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem>()
                {
                    new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem()
                    {
                        BatchNumber = newBatchId,
                        Quantity    = totalOfSelectedRows
                    }
                }
            });

            // Construct a "Goods Receipt PO" document
            NSC_DI.SAP.ReceiptFromProduction.Create(newPDOKwy, DateTime.Now, ListOfGoodsReceiptLines, "Batch Manager");

            //Close production order
            NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(newPDOKwy, BoProductionOrderStatusEnum.boposClosed);

            // update the Batch UDFs    
            NSC_DI.SAP.BatchItems.CopyUDFs(newBatchId, sourceBatchID, new string[1] { "U_NSC_StateID" });

            F_Waiting.SetProgressBar(oForm, 100, "Finished with SAP!...");
            F_Waiting.Form_Close(oForm);

            Globals.oApp.StatusBar.SetText("Successfully created a new Batch!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
            #endregion

            ClearForm();
        }

        private void BTN_FNSH_Click(Form pForm)
        {
            double totalOfSelectedRows = 0;

            // Show the "Loading" form.
            var oForm = F_Waiting.FormCreate(true);

            F_Waiting.SetProgressBar(oForm, 1, "Beginning Process...");

            // Grab the matrix from the form UI
            Matrix CurrentMatrix = _SBO_Form.Items.Item("MTX_MAIN").Specific;

            F_Waiting.SetProgressBar(oForm, 5, "Validating Form...");

            #region Validation

            int returnCode = HandleMatrix_Click(CurrentMatrix.GetNextSelectedRow(0, BoOrderType.ot_RowOrder));
            if (returnCode == 1) return;

            double quantityToApply = 0;
            string destinationWarehouse = "";
            string creationItemCode = "";

            EditText TXT_WRHSC = (EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtWHCode", _SBO_Form);

            EditText TXT_ITEMC = (EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEMC", _SBO_Form);

            EditText TXT_REM = (EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_REM", _SBO_Form);

            EditText TXT_StID = (EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_StID", _SBO_Form);

            if (string.IsNullOrEmpty(TXT_ITEMC.Value))
            {
                Globals.oApp.StatusBar.SetText("Please select an Item!", BoMessageTime.bmt_Short);

                try
                {
                    ((EditText)(CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITMCFL", _SBO_Form))).Active = true;
                }
                catch (Exception ex) {/* */}

                return;
            }

            if (string.IsNullOrEmpty(TXT_WRHSC.Value))
            {
                Globals.oApp.StatusBar.SetText("Please select a Warehouse!", BoMessageTime.bmt_Short);

                try
                {
                    ((EditText)(CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHS", _SBO_Form))).Active = true;
                }
                catch (Exception ex) {/* */}

                return;
            }

            if (string.IsNullOrEmpty(_SBO_Form.Items.Item("txtNumBats").Specific.Value))
            {
                Globals.oApp.StatusBar.SetText("Please enter the number of batches to create!", BoMessageTime.bmt_Short);
                return;
            }

            destinationWarehouse = TXT_WRHSC.Value;
            creationItemCode     = TXT_ITEMC.Value;
            var sourceBatchID    = "";
            var sTag             = TXT_StID.Value;  
            var maxBatSize       = Convert.ToInt32(_SBO_Form.Items.Item("txtBatSize").Specific.Value); 
            var maxBatUOM        = _SBO_Form.Items.Item("staMaxUOM").Specific.Caption; 
            var numBats          = Convert.ToInt32(_SBO_Form.Items.Item("txtNumBats").Specific.Value);
            var ItemCode         = "";
            var ItemCodeUOM      = "";

            #endregion

            Dictionary<string, BatchItemsGroup> GroupedBatchItems = new Dictionary<string, BatchItemsGroup>();
            List<BatchItem> AllSelectedItems = new List<BatchItem>();
            SAPbobsCOM.UserTable oUDT = null;
            // For each row already selected
            try
            {
                F_Waiting.SetProgressBar(oForm, 25, "Gathering Selected Fields...");

                int i = CurrentMatrix.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);
                while (i != -1)
                {
                    BatchItem newBatchItem = new BatchItem();

                    // Make sure the selections have valid input.
                    string qtyToParse = ((EditText)CurrentMatrix.Columns.Item(MatrixColumns["Quantity"]).Cells.Item(i).Specific).Value;
                    double onHandValue = double.Parse(((EditText)CurrentMatrix.Columns.Item(MatrixColumns["OnHand"]).Cells.Item(i).Specific).Value);
                    if (!double.TryParse(qtyToParse, out quantityToApply) || onHandValue < quantityToApply)
                    {
                        ((EditText)CurrentMatrix.Columns.Item(MatrixColumns["Quantity"]).Cells.Item(i).Specific).Active = true;

                        Globals.oApp.StatusBar.SetText("Please enter a valid number!", BoMessageTime.bmt_Short);

                        return;
                    }
                    if (quantityToApply == 0.0)
                    {
                        Globals.oApp.StatusBar.SetText("Please enter a valid number greater than zero!", BoMessageTime.bmt_Short);
                        F_Waiting.Form_Close(oForm);
                        return;
                    }

                    ItemCode = ((EditText)CurrentMatrix.Columns.Item(MatrixColumns["ItemCode"]).Cells.Item(i).Specific).Value;
                    ItemCodeUOM = NSC_DI.SAP.Items.GetField<string>(ItemCode, "InvntryUom");
                    string SourceWarehouse = ((EditText)CurrentMatrix.Columns.Item(MatrixColumns["WhsCode"]).Cells.Item(i).Specific).Value;
                    newBatchItem.QuantityToBatch = quantityToApply;
                    newBatchItem.BatchNumber = ((EditText)CurrentMatrix.Columns.Item(MatrixColumns["BatchNum"]).Cells.Item(i).Specific).Value;

                    totalOfSelectedRows += quantityToApply;
                    string keyOfCurrent = ItemCode + SourceWarehouse;

                    if (sourceBatchID == "") sourceBatchID = newBatchItem.BatchNumber;

                    // Aggregating by Warehouse and Item Code.
                    // We want to keep track of Individual amounts and what batch they come from as well.
                    if (GroupedBatchItems.ContainsKey(keyOfCurrent))
                    {
                        // Add to our existing group.
                        GroupedBatchItems[keyOfCurrent].TotalQuantity += newBatchItem.QuantityToBatch;
                        GroupedBatchItems[keyOfCurrent].BatchItems.Add(newBatchItem);
                    }
                    else
                    {
                        // Create a new BatchItemGroup
                        BatchItemsGroup batchItemGroup = new BatchItemsGroup();
                        batchItemGroup.ItemCode = ItemCode;
                        batchItemGroup.SourceWarehouseCode = SourceWarehouse;
                        batchItemGroup.BatchItems.Add(newBatchItem);
                        batchItemGroup.TotalQuantity = newBatchItem.QuantityToBatch;
                        // Add to our group
                        GroupedBatchItems.Add(keyOfCurrent, batchItemGroup);
                    }
                    // This might be able to remove.
                    AllSelectedItems.Add(newBatchItem);
                    i = CurrentMatrix.GetNextSelectedRow(i, BoOrderType.ot_RowOrder);
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }

            // We have no selected Rows.
            if (AllSelectedItems.Count == 0)
            {
                F_Waiting.Form_Close(oForm);
                Globals.oApp.StatusBar.SetText("Select a row!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                return;
            }

            // ----------------------------
            // multiple batches 
            var batSize = (totalOfSelectedRows / (double)numBats);  // quantity per batch 
            if (batSize < 1.0) throw new Exception("Batch size is less than 1.");
            //batSize = (batSize > maxBatSize) ? maxBatSize : batSize;
            maxBatSize = (double)NSC_DI.UTIL.Units.ConvertWeight(maxBatSize, maxBatUOM, ItemCodeUOM);
            if (batSize > maxBatSize)
            {
                F_Waiting.Form_Close(oForm);
                Globals.oApp.MessageBox($"Batch would be greater than the Max Batch Size ({maxBatSize} {maxBatUOM}).");
                return;
            }
            int cnt = (int)Math.Ceiling(totalOfSelectedRows / (double)batSize);  // have to create
            var lastBatSize = totalOfSelectedRows - (((double)cnt - 1.0) * (double)batSize);


            //Inter-Company Check
            // DELETE 12/30/2020 string InterCoSubCode = NSC_DI.UTIL.SQL.GetValue<string>($@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 where T0.WhsCode = '{destinationWarehouse}'");
            // Create a Batch Items Controller.. So we can get an available batch number.
            //string newBatchId = NSC_DI.SAP.BatchItems.NextBatch(creationItemCode, BATCH_PREFIX);   // OP7926

            //string InterCo = NSC_DI.UTIL.SQL.GetValue<string>("select T0.U_Value from dbo.[@NSC_SETTINGS] T0 where T0.Name = 'Inter-Company'");
            //try
            //{
            //    //Inter-Company Check 
            //    if (InterCo == "Y" && InterCoSubCode.Trim().Length > 0)
            //    {
            //        newBatchId = InterCoSubCode.Trim() + "-" + newBatchId;
            //    }
            //}
            //catch
            //{
            //    throw new Exception(NSC_DI.UTIL.Message.Format("Inter-Company Error: Please check your settings (Y or N) and make sure you have a subsidiary code on your warehouse."));
            //}

            F_Waiting.SetProgressBar(oForm, 75, "Gathering Data for Documents Creation...");

            #region Create Components and GoodsIssueLines from our Groups
            // Using our Aggregated data in the components. -- For Production Order
            List<NSC_DI.SAP.ProductionOrder_OLD.StandardProductionOrderComponentLineItem> components = new List<NSC_DI.SAP.ProductionOrder_OLD.StandardProductionOrderComponentLineItem>();

            // Prepare a list of Goods Issued Lines
            List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines> ListOfGoodsIssuedLines = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();

            // For each group that we aggregated we are going to create a components for our Production Order
            // And a Goods Issued Line. The Goods Issued Line may have multiple Batches associated with it.
            foreach (BatchItemsGroup group in GroupedBatchItems.Values)
            {
                var lineNum = 0;
                components.Add
                (
                    new NSC_DI.SAP.ProductionOrder_OLD.StandardProductionOrderComponentLineItem()
                    {
                        ItemCodeForItemBeingDestroyed = group.ItemCode,
                        SourceWarehouseCode = group.SourceWarehouseCode,
                        BaseQuantity = 1,
                        PlannedQuantity = group.TotalQuantity
                    }
                );

                // Prepare a list of Batch line items for the 
                List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches> issueBatches = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>();

                foreach (BatchItem batchItem in group.BatchItems)
                {
                    issueBatches.Add
                    (
                        new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches()
                        {
                            BatchNumber = batchItem.BatchNumber,
                            Quantity = batchItem.QuantityToBatch
                        }
                    );
                }

                // Add a new item to the list of Goods Issued Lines
                ListOfGoodsIssuedLines.Add
                (
                    new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
                    {
                        BaseLine = lineNum++,
                        ItemCode = group.ItemCode,
                        Quantity = group.TotalQuantity,
                        WarehouseCode = group.SourceWarehouseCode,
                        BatchLineItems = issueBatches

                    }
                );
            }
            #endregion

            try
            {
                F_Waiting.SetProgressBar(oForm, 85, "Creating Production Order...");

                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                //Grap Remarks/Notes
                string strRemarks = TXT_REM.Value.ToString();
                // Create a Disassembly Production Order based on the form
                string pDoProcess; 
                switch (NSC_DI.UTIL.Settings.Version.GetCompliance().ToUpper())
                {
                    case "METRC":
                        pDoProcess = "Harvests_Create_Package";
                        break;
                    default:
                        pDoProcess = "inventory_create_lot";
                        break;

                }

                var newPDOKwy = NSC_DI.SAP.ProductionOrder_OLD.CreateStandardProductionOrder(creationItemCode
                    , totalOfSelectedRows
                    , destinationWarehouse
                    , DateTime.Now
                    , components
                    , ""
                    , "N"
                    , strRemarks
                    , pDoProcess
                );

                // Update the Production Order to release.
                NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(newPDOKwy, BoProductionOrderStatusEnum.boposReleased);

                F_Waiting.SetProgressBar(oForm, 90, "Creating Goods Issued Documents...");

                // Create an "Issue From Production" document
                NSC_DI.SAP.IssueFromProduction.Create(newPDOKwy, DateTime.Now, ListOfGoodsIssuedLines, "Batch Manager");

                F_Waiting.SetProgressBar(oForm, 99, "Creating Goods Receipts...");
                #region Create Goods Receipt

                string strStateID = TXT_StID.Value;

                var newBatchId = NSC_DI.SAP.BatchItems.NextBatch(creationItemCode, cnt, BATCH_PREFIX).ToArray<string>();  

                var batList = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem>();
                for (var i = 0; i < cnt; i++)
                {
                    batList.Add(new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem()
                    {
                        BatchNumber              = newBatchId[i],
                        Quantity                 = (i == cnt-1)? lastBatSize : (double)batSize,
                        ManufacturerSerialNumber = NSC_DI.UTIL.AutoStrain.NextTagNumber(strStateID, i),
                        BatchNotes               = strRemarks
                    });
                }

                List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> ListOfGoodsReceiptLines = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();
                ListOfGoodsReceiptLines.Add(new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines()
                {
                    ItemCode             = creationItemCode,
                    Quantity             = totalOfSelectedRows,
                    WarehouseCode        = destinationWarehouse,
                    ListOfBatchLineItems = batList
                });


                // Construct a "Goods Receipt PO" document
                NSC_DI.SAP.ReceiptFromProduction.Create(newPDOKwy, DateTime.Now, ListOfGoodsReceiptLines, "Batch Manager");

                //Close production order
                NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(newPDOKwy, BoProductionOrderStatusEnum.boposClosed);

                // update the Batch UDFs    
                for (var i = 0; i < cnt; i++)
                {
                    NSC_DI.SAP.BatchItems.CopyUDFs(newBatchId[i], sourceBatchID, new string[1] { "U_NSC_StateID" });
                   
                }
                
                if (pForm.Items.Item("CHK_FNSH").Specific.Checked)
                {
                    string HarvestName;
                    bool harvestError = false;
                    // if check for finish is checked, goes through the process of finishing the harvest
                    for (var i = 1; i <= CurrentMatrix.RowCount; i++)
                    {
                        if (!CurrentMatrix.IsRowSelected(i))
                            continue; // if row is not selected
                       
                        HarvestName = CurrentMatrix.GetCellSpecific(MatrixColumns["HarvName"], i).Value;
                        if (NSC_DI.UTIL.Strings.Empty(HarvestName))
                        {
                            harvestError = true;
                            continue; // The user selects Finish Harvest but there is no harvest name associated
                        }                           
                      
                        string code = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Code FROM [@{NSC_DI.Globals.tCompHarvest}] WHERE U_HarvestName = '{HarvestName}'");
                        if (NSC_DI.UTIL.Strings.Empty(code))
                        {
                            harvestError = true;
                            continue;// The user selects "Finish Harvest", there is a harvest name in OBTN but not NSC_COMP_HARVEST
                        }

                        oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tCompHarvest);// sets the datatable to Comp Harvest
                        oUDT.GetByKey(code); // gets the item that will be updated
                        oUDT.UserFields.Fields.Item("U_Finished").Value = "Y"; // updates U_Finished to Y
                        if (oUDT.Update() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription())); // throws error if there is an issue updating the schema
                    }
                    if(harvestError)
                        Globals.oApp.StatusBar.SetText("Unable to finish one or more of the harvests for the items selected.");
                }                

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                F_Waiting.SetProgressBar(oForm, 100, "Finished with SAP!...");
                //F_Waiting.Form_Close(oForm);

                Globals.oApp.StatusBar.SetText("Successfully created a new Batch!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                #endregion

                ClearForm();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                F_Waiting.Form_Close(oForm);
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
            }
        }

        private void ClearForm()
        {
            try
            {
                _SBO_Form.Select();

                try
                {
                    EditText warehouseCode = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtWHCode", _SBO_Form);

                    if (warehouseCode != null)
                    {
                        warehouseCode.Value = "";
                    }
                }
                catch { }

                try
                {
                    EditText warehouseName = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtWHName", _SBO_Form);

                    if (warehouseName != null)
                    {
                        warehouseName.Value = "";
                    }
                }
                catch { }

                try
                {
                    EditText batchToCreate = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "txtBatch", _SBO_Form);

                    if (batchToCreate != null)
                    {
                        batchToCreate.Value = "";
                    }
                }
                catch { }

                try
                {
                    EditText batchItemCode = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEMC", _SBO_Form);

                    if (batchItemCode != null)
                    {
                        batchItemCode.Value = "";
                    }
                }
                catch { }

                try
                {
                    EditText selectionTotal = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_TOTAL", _SBO_Form);

                    if (selectionTotal != null)
                    {
                        selectionTotal.Value = "";
                    }
                }
                catch { }

                try
                {
                    EditText StateID = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_StID", _SBO_Form);

                    if (StateID != null)
                    {
                        StateID.Value = "";
                    }
                }
                catch { }

                try
                {
                    EditText Remarks = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_REM", _SBO_Form);

                    if (Remarks != null)
                    {
                        Remarks.Value = "";
                    }
                }
                catch { }

                _SBO_Form.Items.Item("txtNumBats").Specific.Value = "";

                Load_Matrix();
            }
            catch (Exception ex)
            {
                /* They closed the form or moved somewhere else.. just ignore*/
            }
        }

        private void HandleTab(Form pForm, string pField)
        {
            EditText oTxt = null;
            try
            {
                oTxt = pForm.Items.Item(pField).Specific;
                if (string.IsNullOrEmpty(oTxt.Value) == false) return;  // Only handle the tab event if the text field is empty.

                oTxt.Active = true;
                if (pField == "txtWhse")
                {
                    HandleWarehouseSelectionCFL(pForm);
                }
                else if (pField == "txtBatch")
                {
                    HandleBatchSelectionCFL(pForm);
                }

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oTxt);
                GC.Collect();
            }
        }

        private static void FormReSize(Form pForm)
        {
            Matrix MTX_MAIN = null;
            try
            {
                MTX_MAIN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", pForm
                );

                MTX_MAIN.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(MTX_MAIN);
                GC.Collect();
            }
        }
    }
}