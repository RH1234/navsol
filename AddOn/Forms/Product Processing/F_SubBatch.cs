﻿using System;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms.Product_Processing
{
    public class F_SubBatch : B1Event
    {
        // ---------------------------------- VARS, CLASSES --------------------------------------------------------
        const string cFormID = "NSC_SUB-BATCH";


        // ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeValidate(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "grdBatches") BubbleEvent = Validate_Grid(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPressed(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "1" && pVal.FormMode == (int)SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) BubbleEvent = Press_Update(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        // ---------------------------------- AFTER EVENT  --------------------------------------------------------


        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (ChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CLICK
        [B1Listener(BoEventTypes.et_CLICK, false, new string[] { cFormID })]
        public virtual void OnAfterClick(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "btnItmCCFL" || pVal.ItemUID == "btnItmNCFL") CFL_Item_Branches(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_KEY_DOWN
        [B1Listener(BoEventTypes.et_KEY_DOWN, false, new string[] { cFormID })]
        public virtual void OnAfterKeyDown(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "txtItemCod" && pVal.CharPressed == 9) CFL_Item_Branches(oForm, pVal);  // 10823-3
            if (pVal.ItemUID == "txtDesc"    && pVal.CharPressed == 9) CFL_Item_Branches(oForm, pVal);  // 10823-3

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public static void FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
        {
            Form oForm = null;
            Item oItm = null;

            try
            {
                oForm = CommonUI.Forms.Load(cFormID, true);

                oForm.Freeze(true);

                FormSetup(oForm);

                GridLoad(oForm);

                oForm.Mode = BoFormMode.fm_OK_MODE;

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oForm.Freeze(false);
                oForm.VisibleEx = true;
                NSC_DI.UTIL.Misc.KillObject(oForm);
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private static void FormSetup(Form pForm)
        {
            try
            {
                pForm.DataSources.DataTables.Add("dtBatches");
                pForm.Items.Item("grdBatches").Specific.DataTable = pForm.DataSources.DataTables.Item("dtBatches");
                pForm.Items.Item("grdBatches").Specific.SelectionMode = BoMatrixSelect.ms_Auto;
                pForm.Items.Item("grdBatches").Enabled = true;

                if (Globals.BranchDflt >= 0)    // 10823-3
                {
                    pForm.Items.Item("btnItmCCFL").Visible = true;
                    pForm.Items.Item("btnItmNCFL").Visible = true;
                }
                else
                {
                    // Item CFL   
                    CommonUI.Forms.CreateUserDataSource(pForm, "txtItemCod", "dsItmCode", BoDataType.dt_SHORT_TEXT, 50);
                    CommonUI.Forms.CreateUserDataSource(pForm, "txtDesc", "dsItmName", BoDataType.dt_SHORT_TEXT, 50);
                    NavSol.CommonUI.CFL.Create(pForm, "CFL_ITEM", BoLinkedObject.lf_Items);
                    CommonUI.CFL.AddCon(pForm, "CFL_ITEM", BoConditionRelationship.cr_NONE, "validFor", BoConditionOperation.co_EQUAL, "Y");
                    CommonUI.CFL.AddCon(pForm, "CFL_ITEM", BoConditionRelationship.cr_AND, $"U_{Globals.SAP_PartnerCode}_" + "Batchable", BoConditionOperation.co_EQUAL, "Y");
                    CommonUI.CFL.AddCon(pForm, "CFL_ITEM", BoConditionRelationship.cr_AND, $"{NSC_DI.SAP.Items.Property.GetFieldName("Template")}", BoConditionOperation.co_EQUAL, "N");
                    CommonUI.CFL.AddCon(pForm, "CFL_ITEM", BoConditionRelationship.cr_AND, "OnHand", BoConditionOperation.co_GRATER_THAN, "0");
                    pForm.Items.Item("txtItemCod").Specific.ChooseFromListUID = "CFL_ITEM";
                    pForm.Items.Item("txtItemCod").Specific.ChooseFromListAlias = "ItemCode";

                    NavSol.CommonUI.CFL.Create(pForm, "CFL_DESC", BoLinkedObject.lf_Items);
                    CommonUI.CFL.AddCon(pForm, "CFL_DESC", BoConditionRelationship.cr_NONE, "validFor", BoConditionOperation.co_EQUAL, "Y");
                    CommonUI.CFL.AddCon(pForm, "CFL_DESC", BoConditionRelationship.cr_AND, $"U_{Globals.SAP_PartnerCode}_" + "Batchable", BoConditionOperation.co_EQUAL, "Y");
                    CommonUI.CFL.AddCon(pForm, "CFL_DESC", BoConditionRelationship.cr_AND, $"{NSC_DI.SAP.Items.Property.GetFieldName("Template")}", BoConditionOperation.co_EQUAL, "N");
                    CommonUI.CFL.AddCon(pForm, "CFL_DESC", BoConditionRelationship.cr_AND, "OnHand", BoConditionOperation.co_GRATER_THAN, "0");
                    pForm.Items.Item("txtDesc").Specific.ChooseFromListUID = "CFL_DESC";
                    pForm.Items.Item("txtDesc").Specific.ChooseFromListAlias = "ItemName";
                }

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.VisibleEx = true;
            }
        }

        private static void GridLoad(Form pForm, bool pClear = true)
        {
            Grid oGrd = null;

            try
            {
                // if sql in empty, then reset all of the fields
                if(pClear)
                {
                    pForm.Items.Item("txtItemCod").Specific.Value = "";
                    pForm.Items.Item("txtDesc").Specific.Value    = "";
                }

                // the batch qty shows what is available (includes committed). the bin qty shows what is in the bin (does not includes committed)
                // also, the validate only looks at the specific row. so if the same batch is in multiple bins, then could be a problem.

                var itemCode = pForm.Items.Item("txtItemCod").Specific.Value;
                var sql = $@"
SELECT OITW.ItemCode, OITM.ItemName, OITW.WhsCode, OWHS.WhsName, OBIN.BinCode, OBTN.DistNumber, OBTN.MnfSerial AS [Assigned State ID], SUM(OBTQ.Quantity - ISNULL(OBTQ.CommitQty, 0)) AS 'Batch Qty', 
       SUM(OBBQ.OnHandQty) AS [Bin Qty], 0.00 AS [Sub Qty], SPACE(50) AS 'Sub State ID', OBTN.AbsEntry AS BatchID, OBIN.AbsEntry AS BinID, OBTQ.SysNumber
  FROM OITW 
 INNER JOIN OITM ON OITM.ItemCode = OITW.ItemCode 
 INNER JOIN OWHS ON OITW.WhsCode = OWHS.WhsCode 
 INNER JOIN OBTQ ON OITW.ItemCode = OBTQ.ItemCode AND OITW.WhsCode = OBTQ.WhsCode AND OBTQ.Quantity - ISNULL(OBTQ.CommitQty, 0) > 0 
 INNER JOIN OBTN ON OBTQ.MdAbsEntry = OBTN.AbsEntry 
  LEFT JOIN OBBQ ON OBTQ.MdAbsEntry = OBBQ.SnBMDAbs  AND OBBQ.OnHandQty > 0 
  LEFT JOIN OBIN ON OBBQ.BinAbs = OBIN.AbsEntry
 WHERE OITM.ItemCode = '{itemCode}' AND OITW.OnHand > 0
 GROUP BY OITW.ItemCode, OITM.ItemName, OITW.WhsCode, OWHS.WhsName, OITW.WhsCode, OBIN.AbsEntry, OBIN.BinCode, OBTN.AbsEntry, OBTN.DistNumber, OBTN.MnfSerial, OBTQ.SysNumber
 ORDER BY OITW.ItemCode, OITW.WhsCode, OBIN.BinCode";

                oGrd = pForm.Items.Item("grdBatches").Specific;
                pForm.DataSources.DataTables.Item("dtBatches").ExecuteQuery(sql);

                // only the new quantity column is editable
                for (var i = 0; i < oGrd.Columns.Count; i++)
                {
                    oGrd.Columns.Item(i).Editable = false;
                }               
                oGrd.Columns.Item("Sub Qty").Editable               = true;
                oGrd.Columns.Item("BinID").Visible                  = false;
                oGrd.Columns.Item("BatchID").Visible                = false;
                oGrd.Columns.Item("Sub Qty").RightJustified         = true;
                oGrd.Columns.Item("Batch Qty").RightJustified       = true;
                oGrd.Columns.Item("Bin Qty").RightJustified         = true;
                //oGrd.Columns.Item("Warehouse Qty").RightJustified   = true;
                if (NSC_DI.UTIL.Settings.Value.Get("State Compliance").ToUpper() == "METRC")
                    oGrd.Columns.Item("Sub State ID").Editable = true;
                else
                    oGrd.Columns.Item("Sub State ID").Visible = false;

                pForm.Items.Item("grdBatches").Specific.AutoResizeColumns();

                // if the item is blank, then clear the table
                if(itemCode == "") pForm.DataSources.DataTables.Item("dtBatches").Rows.Remove(0);

                pForm.Mode = BoFormMode.fm_OK_MODE;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }

        private void CFL_Item_Branches(Form pForm, ItemEvent pVal)  // 10823-3
        {
            try
            {
                if (Globals.BranchDflt < 0) return;

                var sql = "SELECT ";

                if (pVal.ItemUID == "btnItmCCFL" || pVal.ItemUID == "txtItemCod")
                {
                    // if they tab off of the field and it contains a value, then return
                    if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pForm.Items.Item("txtItemCod").Specific.Value != "") return;
                    sql += "OITM.ItemCode, OITM.ItemName";
                }

                if (pVal.ItemUID == "btnItmNCFL" || pVal.ItemUID == "txtDesc")
                {
                    // if they tab off of the field and it contains a value, then return
                    if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pForm.Items.Item("txtDesc").Specific.Value != "") return;

                    sql += "OITM.ItemName, OITM.ItemCode";
                }

                sql += $@", OWHS.WhsCode, OWHS.WhsName, OITW.OnHand
  FROM OITM
 INNER JOIN OITW ON OITM.ItemCode = OITW.ItemCode
 INNER JOIN OWHS ON OITW.WhsCode = OWHS.WhsCode
 WHERE OITM.QryGroup64 = 'N' AND OITM.validFor = 'Y' AND OITM.U_NSC_Batchable = 'Y' AND OITW.OnHand > 0 AND OWHS.BPLid IN ({Globals.BranchList})
 ORDER BY OITM.ItemName";

                F_CFL_GRID.CflCB = delegate (string pCallingFormUid)
                {
                    pForm = CommonUI.Forms.GetFormFromUID(pCallingFormUid);
                    GridLoad(pForm, false);
                };
                F_CFL_GRID.FormCreate("Item Choose From List", sql, pForm.UniqueID, "txtItemCod,txtDesc", "ItemCode,ItemName");
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            //if (pVal.InnerEvent) return;
            if (pVal.SelectedObjects == null) return;

            try
            {
                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_ITEM":
                        CommonUI.CFL.SetItemFieldsUDS(pForm, pVal, "txtItemCod", "txtDesc");
                        break;

                    case "CFL_DESC":
                        CommonUI.CFL.SetItemFieldsUDS(pForm, pVal, "txtItemCod", "txtDesc");
                        break;
                }

                // load the grid
                GridLoad(pForm, false);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static bool Validate_Grid(Form pForm, ItemEvent pVal)
        {
            // returns false if an error

            SAPbouiCOM.DataTable oDT = null;

            try
            {
                var row = pForm.Items.Item(pVal.ItemUID).Specific.GetDataTableRowIndex(pVal.Row);
                oDT = pForm.Items.Item(pVal.ItemUID).Specific.DataTable;

                // QUANTITY

                if (pVal.ColUID != "Sub Qty") return true;
                var subQty = oDT.GetValue("Sub Qty", row);
                if (subQty == 0) return true;

                //var availQty = oDT.GetValue("Warehouse Qty", row);
                var batQty   = oDT.GetValue("Batch Qty", row);
                var binQty   = oDT.GetValue("Bin Qty", row);
                //availQty     = (batQty == 0) ? availQty : Math.Min(batQty, availQty);
                var availQty = (binQty == 0) ? batQty : Math.Min(binQty, batQty);

                if (subQty <= availQty) return true;

                Globals.oApp.StatusBar.SetText("You have exceeded the available quantity.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                return false;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return false;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }

        private static bool Press_Update(Form pForm)
        {
            SAPbouiCOM.DataTable oDT    = null;

            try
            {
                System.Collections.Generic.List<NSC_DI.SAP.BatchItems.SubBatch.BatchRec> oBatches = new System.Collections.Generic.List<NSC_DI.SAP.BatchItems.SubBatch.BatchRec>();

                //oDT = pForm.Items.Item("grdBatches").Specific.DataTable;
                oDT = pForm.DataSources.DataTables.Item("dtBatches");

                var oItemsDT = NSC_DI.SAP.BatchItems.SubBatch.DT_Define();

                for (var i = 0; i < oDT.Rows.Count; i++)
                {
                    var subQty = oDT.GetValue("Sub Qty", i);
                    if (subQty <= 0.00) continue;

                    var itemCode = oDT.GetValue("ItemCode", i);
                    var wh = oDT.GetValue("WhsCode", i);
                    var mnfSerial = oDT.GetValue("Sub State ID", i);
                    if (NSC_DI.UTIL.Settings.Value.Get("State Compliance").ToUpper() == "METRC" && (string.IsNullOrEmpty(mnfSerial.ToString()?.Trim())))// Whitespace-Change NSC_DI.UTIL.Strings.Empty(mnfSerial.ToString()
                        throw new Exception(NSC_DI.UTIL.Message.Format("You must select a State ID for the new sub batch!")); 

                    NSC_DI.SAP.BatchItems.SubBatch.BatchRec batRec = new NSC_DI.SAP.BatchItems.SubBatch.BatchRec
                    {
                        Item = itemCode,
                        FromWarehouse = wh,
                        ToWarehouse   = wh,
                        Quantity    = subQty,
                        Bin         = oDT.GetValue("BinID", i),
                        OldBatch    = oDT.GetValue("DistNumber", i),
                        NewBatch    = "",
                        MnfSerial = mnfSerial                        
                    };

                    oBatches.Add(batRec);
                }
            
                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();
                NSC_DI.SAP.BatchItems.SubBatch.Create(oBatches, "Inventory_split");    // create sub-batches using a goods issue.

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);

                Globals.oApp.StatusBar.SetText("Process successful.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                GridLoad(pForm, false);
                return false;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return false;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }
    }
}