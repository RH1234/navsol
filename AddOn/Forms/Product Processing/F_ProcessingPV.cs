﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Product_Processing
{
    class F_ProcessingPV : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_PROCESSING_PV";
        private const string BATCH_PREFIX = "PK";
        private const string cPdOReceiptType = "Inventory Convert";
        private enum MatrixColumns { ProductionOrder, IssueMore, ItemCode, ItemName, Quantity,UoM, QuantityToUse, StateID, ProdBatchID, ExpDate, CreateDate, Warehouse, DestWarehouse }

		// FOR OLD CODE
		public VERSCI.Forms _VirSci_Helper_Form;
		public SAPbouiCOM.Application _SBO_Application;
		public SAPbobsCOM.Company _SBO_Company;
		public SAPbouiCOM.Form _SBO_Form;
		public string FormUID { get; set; }
        private static System.Data.DataTable _dtByProd = new System.Data.DataTable("ByProd");
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}

		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------

		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "BTN_APPLY":
					BTN_APPLY_Click();
					break;

                case "MTX_MAIN":
                    MTX_ITEMS_Click(oForm, pVal);
                    SetRemarks();
                    break;
            }

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
			_SBO_Form = oForm;

			switch (pVal.ItemUID)
			{
				case "MTX_MAIN":
					MAIN_MATRIX_LINK_PRESSED(pVal);
                    break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		public F_ProcessingPV() : this(Globals.oApp, Globals.oCompany)
		{
		}

		public F_ProcessingPV(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
		{
			_SBO_Application = SAPBusinessOne_Application;
			_SBO_Company = SAPBusinessOne_Company;
		}

		public void Form_Load()
		{
            Column oColumn = null;
            Matrix oMatrix = null;
            try
            {
                _SBO_Form = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                _SBO_Form.Freeze(true);

                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(_SBO_Form, "IMG_MAIN", @"packaging-icon.bmp");

                SAPbouiCOM.Button BTN_APPLY = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Button, "BTN_APPLY", _SBO_Form) as SAPbouiCOM.Button;

                BTN_APPLY.Item.Enabled = true;              
                
                // Loading all Matrices. Passing true loads all three.
                Load_Matrix(_SBO_Form);
                //_SBO_Form.DataSources.UserDataSources.Add("UDDate", SAPbouiCOM.BoDataType.dt_DATE, 254);
                //oMatrix = _SBO_Form.Items.Item("MTX_MAIN").Specific;
                //oColumn = oMatrix.Columns.Item("col_9");//               .Add("Col_9", BoFormItemTypes.it_EDIT);
                //oColumn.DataBind.SetBound(true, "", "UDDate");
                //Add Columns
                if (_dtByProd.Columns.Count == 0)
                {
                    _dtByProd.Columns.Add("ItemCode");
                    _dtByProd.Columns.Add("ItemName");
                    _dtByProd.Columns.Add("PlannedQty");
                    _dtByProd.Columns.Add("wareHouse");
                    _dtByProd.Columns.Add("LineNum");
                    _dtByProd.Columns.Add("BatchID");
                    _dtByProd.Columns.Add("StateID");
                }
                else
                {
                    _dtByProd.Clear();
                }
                // Un-Freeze the Form UI
                _SBO_Form.Freeze(false);

                // Show the form
                _SBO_Form.VisibleEx = true;
               
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                _SBO_Form.Freeze(false);
                _SBO_Form.VisibleEx = true;
                NSC_DI.UTIL.Misc.KillObject(oColumn);
                NSC_DI.UTIL.Misc.KillObject(oMatrix);
                GC.Collect();
            }
		}

        private void SetRemarks()
        {
            try
            {
                //Get the Remarks Textbox
                SAPbouiCOM.EditText oEdit = _SBO_Form.Items.Item("TXT_REM").Specific;
                // Grab the Matrix from the form 
                SAPbouiCOM.Matrix matrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);
                //Get selected production order
                string ProdNumder = CommonUI.MatrixExtensionMethods.GetStringOfSelectedRow(matrix, "Production Order");

                string Remarks = NSC_DI.UTIL.SQL.GetValue<string>(@"Select OWOR.Comments from OWOR where OWOR.DocNum=" + ProdNumder);
                oEdit.Value = Remarks;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox("Please Select The Production Order");
            }
            finally
            {
                GC.Collect();
            }
        }

        private void MTX_ITEMS_Click(Form pForm, ItemEvent SAP_UI_ItemEvent)
        {
            // Find the matrix in the form UI
            Matrix Matrix_Items = pForm.Items.Item("MTX_MAIN").Specific;

            try
            {
                string ItemCodeOfNewSelectedRow = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.ItemCode).Cells.Item(SAP_UI_ItemEvent.Row).Specific).Value;

                // Determine if this key press was of the same Item Group we had selected previously.
                bool deselectCurrent = false;
                List<int> rowsToReselect = new List<int>();
                for (int i = 1; i < (Matrix_Items.RowCount + 1); i++)
                {
                    string ItemCodeToCompare = ((EditText)Matrix_Items.Columns.Item(MatrixColumns.ItemCode).Cells.Item(i).Specific).Value;
                    // If the current row is now selected we can just continue looping.
                    if (!Matrix_Items.IsRowSelected(i) || i == SAP_UI_ItemEvent.Row)
                        continue;

                    rowsToReselect.Add(i);

                    if (!ItemCodeOfNewSelectedRow.Equals(ItemCodeToCompare))
                    {
                        deselectCurrent = true;
                    }
                }

                if (deselectCurrent)
                {
                    // Send a message to the client
                    Globals.oApp.StatusBar.SetText(
                        Text: "Item Code must be of the same type!",
                        Seconds: BoMessageTime.bmt_Short,
                        Type: BoStatusBarMessageType.smt_Error);

                    Matrix_Items.SelectRow(SAP_UI_ItemEvent.Row, false, false);
                    return;
                }
            }
            catch
            {

            }
        }

        private void Load_Matrix(Form pForm)
        {
            try
            {
                // Prepare a Matrix helper
                VERSCI.Matrix oMatrixHelper = new VERSCI.Matrix(Globals.oApp, _SBO_Form);

                string orderStatus = "R";

                string sql = $@"
SELECT[OWOR].[DocEntry] as 'ProductionOrder', [OWOR].[U_NSC_ProductionBatchID] AS [ProdBatchID],
(SELECT CASE WHEN COUNT(LineNum) > 0 
THEN 'Y' ELSE 'N' END 
FROM WOR1 
WHERE Docentry = [OWOR].[DocEntry] AND IssueType = 'M' AND IssuedQty = 0 AND PlannedQty > 0) AS [IssueMore],
[OITM].[ItemName], [OWOR].[ItemCode], [OWOR].[PlannedQty] AS 'Quantity',[OITM].InvntryUom AS 'UoM', [OWOR].[PlannedQty] AS 'QuantityToUse',
(SELECT TOP(1) OSRI.IntrSerial FROM OSRI INNER JOIN SRI1 ON OSRI.ItemCode = SRI1.ItemCode AND OSRI.SysSerial = SRI1.SysSerial
INNER JOIN OITM ON SRI1.ItemCode = OITM.ItemCode WHERE SRI1.BsDocEntry = owor.DocEntry AND SRI1.BaseType = 60 AND OITM.QryGroup36 = 'Y')  AS StateID,
SPACE(64) AS ExpDate,[OWOR].[CreateDate],
[OWOR].[Warehouse] AS [DestWarehouse], (SELECT TOP 1 [WOR1].[wareHouse] FROM [WOR1]  WHERE [WOR1].[DocEntry] = [OWOR].[DocNum] ) AS [Warehouse]
FROM [OWOR]
INNer JOIN [OITM] ON [OITM].[ItemCode] = [OWOR].[ItemCode]
inner JOIN [OWHS] ON [OWHS].[WhsCode] = [OWOR].[Warehouse]
WHERE [OWOR].[Status] = '{orderStatus}' AND [OWOR].[U_NSC_CnpProduct] = 'Y' AND [OWOR].[U_NSC_Process] = 'Inventory_convert'";
                if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})"; //10823-2
                sql += $@"ORDER BY CONVERT(int, [OWOR].[DocNum]) DESC";

                // Load plants into the matrix      
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(
                        _SBO_Form,
                              "OWOR",
                              "MTX_MAIN",
                              new List<CommonUI.Matrix.MatrixColumn>() {
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="ProductionOrder", Caption="Production Order", ColumnWidth=80, ItemType = BoFormItemTypes.it_LINKED_BUTTON  }
                                    ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="IssueMore", Caption="Issue +", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT},
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="ItemCode", Caption="Item Code", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="ItemName", Caption="Item Name", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="Quantity", Caption="Quantity", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }
                                    ,new CommonUI.Matrix.MatrixColumn(){ Caption="UoM", ColumnWidth=80, ColumnName="UoM", ItemType = BoFormItemTypes.it_EDIT, IsEditable = false}
                                    ,new CommonUI.Matrix.MatrixColumn(){ Caption="Quantity To Receive", ColumnWidth=80, ColumnName="QuantityToUse", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}
                                    ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="StateID", Caption="StateID", ColumnWidth=140, ItemType = BoFormItemTypes.it_EDIT, IsEditable = true }
                                    ,new CommonUI.Matrix.MatrixColumn(){ Caption="Prod Batch ID", ColumnWidth=80, ColumnName="ProdBatchID", ItemType = BoFormItemTypes.it_EDIT, IsEditable = true}
                                    ,new CommonUI.Matrix.MatrixColumn(){ Caption="Expiration Date", ColumnWidth=80, ColumnName="ExpDate", ItemType=BoFormItemTypes.it_EDIT, DataType = BoDataType.dt_DATE, IsEditable = true} //, DataType=BoDataType.dt_DATE, IsEditable=true
                                    ,new CommonUI.Matrix.MatrixColumn(){ ColumnName="CreateDate", Caption="Created On", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="Warehouse", Caption="Warehouse ID", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  },
                                    new CommonUI.Matrix.MatrixColumn(){ ColumnName="DestWarehouse", Caption="Dest. Warehouse", ColumnWidth=80, ItemType = BoFormItemTypes.it_EDIT  }                                   
                              }, sql, "UDDate"); // 12582 sets pUserDataSource to the name of the userdatasource instantiated in Form_Load()
                                                 // Sets the value of the state id to the serial num of the associated metrc tag (if it exists)

                // Grab the matrix from the form UI
                SAPbouiCOM.Matrix matrix = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form) as SAPbouiCOM.Matrix;                
               
              
                for (int i = 1; i <= matrix.RowCount; i++)
                    {
                        if (!NSC_DI.UTIL.Strings.Empty(matrix.Columns.Item(MatrixColumns.StateID).Cells.Item(i).Specific.Value))
                            matrix.CommonSetting.SetCellEditable(i, (int)MatrixColumns.StateID, false); // any state id's that have a valid value are set to uneditable
                    }
                // Enforce single selection only.
                matrix.SelectionMode = BoMatrixSelect.ms_Single;
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

        }

        /// <summary>
        /// Handles the Release and Close Event.
        /// </summary>
        private void BTN_APPLY_Click(int pByProductsCollected =0, string pByProdCB="")
        {
            try
            {               
                //check for byproducts
                //McGyvered needs reworkd
                try
                {
                    if (_SBO_Form.TypeCount == 0)
                    {
                        //This should throw an error on call back because Form COM object is detatched.
                    }
                }
                catch
                {
                    _SBO_Form = Globals.oApp.Forms.GetForm(cFormID, 0);
                }

                // Grab the Matrix from the form 
                SAPbouiCOM.Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

                string itemCode = "";
                double quantity = 0;
                string destinationWarehouseCode = "";
                int productionOrderKey = 0;
                string stateID = String.Empty;
                string IssueMore = String.Empty;
                SAPbobsCOM.BatchNumberDetail oBN = null;
                string prodBatchID = "";
                string expDate = "";

                for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
                {
                    if (MTX_MAIN.IsRowSelected(i))
                    {
                        try
                        {
                            itemCode = MTX_MAIN.Columns.Item(MatrixColumns.ItemCode).Cells.Item(i).Specific.Value.ToString();
                            quantity = Convert.ToDouble(MTX_MAIN.Columns.Item(MatrixColumns.QuantityToUse).Cells.Item(i).Specific.Value);
                            destinationWarehouseCode = MTX_MAIN.Columns.Item(MatrixColumns.DestWarehouse).Cells.Item(i).Specific.Value.ToString();
                            productionOrderKey = Convert.ToInt32(MTX_MAIN.Columns.Item(MatrixColumns.ProductionOrder).Cells.Item(i).Specific.Value);
                            stateID = MTX_MAIN.Columns.Item(MatrixColumns.StateID).Cells.Item(i).Specific.Value.ToString();

                            IssueMore = MTX_MAIN.Columns.Item(MatrixColumns.IssueMore).Cells.Item(i).Specific.Value.ToString();
                            prodBatchID = MTX_MAIN.Columns.Item(MatrixColumns.ProdBatchID).Cells.Item(i).Specific.Value.ToString();
                            expDate = MTX_MAIN.Columns.Item(MatrixColumns.ExpDate).Cells.Item(i).Specific.Value.ToString();                            
                        }
                        catch (Exception e)
                        {
                            // Send a message to the client about the fail of the document creation
                            Globals.oApp.StatusBar.SetText("Failed to Receipted Package(s)!");

                            return;
                        }

                        // Break out of the loop.
                        i = MTX_MAIN.RowCount + 2;
                    }
                }

                if (string.IsNullOrEmpty(itemCode))
                {
                    Globals.oApp.StatusBar.SetText("Select a row to process.");
                    return;
                }

                if (string.IsNullOrEmpty(itemCode) || quantity <= 0 || string.IsNullOrEmpty(destinationWarehouseCode))
                {
                    Globals.oApp.StatusBar.SetText("Failed to aquire adequate data about the package to complete!");
                    return;
                }

                if (IssueMore == "Y")
                {
                    // not all of the "Manual" components have been issued
                    if(Globals.oApp.MessageBox("Not all of the 'Manual' components have been issued.\rDo You want to continue?", 2, "Yes", "No") == 2) return;
                }
                // Set so the user cannot set a production batch id for a PO that is not 
                if (!string.IsNullOrEmpty(prodBatchID?.Trim()) && NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_NSC_isProductionBatch FROM OWOR WHERE DocEntry = {productionOrderKey}").ToUpper() == "N")// Whitespace-Change (!NSC_DI.UTIL.Strings.Empty(prodBatchID)
                {
                    Globals.oApp.StatusBar.SetText($"The Production Order {productionOrderKey.ToString()} is not selected as a Production Batch");
                    return;
                }

                //Inter-Company Check

                //string InterCoSubCode = NSC_DI.UTIL.SQL.GetValue<string>($@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 where T0.WhsCode = '{destinationWarehouseCode}'"); // DELETE 2020/12/29

                //Check if Byproducts Exist
                int NoOfByProducts = NSC_DI.UTIL.SQL.GetValue<int>("select Count(WOR1.ItemCode) from WOR1 where WOR1.DocEntry = " + productionOrderKey + " and WOR1.PlannedQty < 0");

                if (NoOfByProducts > 0 && pByProductsCollected==0)
                {
                    int iReturnVal = Globals.oApp.MessageBox("There are ByProducts to Report. What would you like to do?", 2, " Assign Values ", " Cancel ");
                    
                    switch (iReturnVal)
                    {
                        case 1:
                            //set Results to DT 

                            var pFormID = _SBO_Form.UniqueID; // Save unique id to get form in callback
                            SAPbouiCOM.DataTable dtByProd = null;

                            try
                            {                               

                                if (_SBO_Form.DataSources.DataTables.Item("BySrc").IsEmpty == false)
                                {
                                    _SBO_Form.DataSources.DataTables.Item("BySrc").Clear();
                                     dtByProd = _SBO_Form.DataSources.DataTables.Item("BySrc");
                                }
                            }
                            catch
                            {
                                     dtByProd = _SBO_Form.DataSources.DataTables.Add("BySrc");
                            }

                            dtByProd.ExecuteQuery(@"select WOR1.ItemCode,OITM.ItemName, WOR1.PlannedQty, SPACE(10) AS Warehouse,WOR1.LineNum 
                                                    ,'                                                         ' as [BatchID]
                                                    ,'                                                         ' as [StateID] 
                                                    from WOR1
                                                    join OITM on WOR1.ItemCode = OITM.ItemCode
                                                    where WOR1.DocEntry = " + productionOrderKey + " and WOR1.PlannedQty < 0 and WOR1.IssueType='M'");
     
                            // open the form; ByProduct Planned Quantity must be set
                            F_ByProdSelect.FormCreate(pFormID, dtByProd);

                            F_ByProdSelect.ByProdsCB = delegate (string callingFormUid, System.Data.DataTable pdtByProd)
                            {
                                Form oForm = null;

                                oForm = Globals.oApp.Forms.Item(pFormID) as Form;
                                //if (pdtByProd.Rows.Count == 0) return;
                                foreach (System.Data.DataRow dr in pdtByProd.Rows)
                                {
                                    var batchID = NSC_DI.SAP.BatchItems.NextBatch(dr["ItemCode"].ToString(), "BP");
                                    _dtByProd.Rows.Add(dr["ItemCode"], dr["ItemName"], dr["PlannedQty"], dr["Warehouse"], dr["LineNum"], batchID, dr["StateID"]);
                                }
                                BTN_APPLY_Click(1, callingFormUid);
                            };

                            return;
                            //break;
                        case 2:
                            return;
                    }
                }
                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                // Create a List to hold items
                NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines ParentItem = new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines();
            
                ParentItem.ItemCode = itemCode;
                ParentItem.Quantity = quantity;
                ParentItem.WarehouseCode = destinationWarehouseCode;

                if(NSC_DI.SAP.Items.GetField<string>(itemCode, "ManSerNum") == "Y") ParentItem.SerialNumbers= NSC_DI.UTIL.AutoStrain.NextSN(itemCode, "Serial Plant", Convert.ToInt32(quantity));

                // when pass in intercomp code, going to have to find everywhere and change every form that needs int co code 
                var newBatchID = NSC_DI.SAP.BatchItems.NextBatch(itemCode, BATCH_PREFIX);
                ////////////////DELETE 3/9/21 EBISHOP\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                //try 
                //{
                //    //Inter-Company Check 
                //    if (InterCo == "Y" && InterCoSubCode.Trim().Length > 0)
                //    {

                //        newBatchID = InterCoSubCode.Trim() + "-" + newBatchID;
                //    }

                //}
                //catch
                //{
                //    throw new Exception(NSC_DI.UTIL.Message.Format("Inter-Company Error: Please check your settings (Y or N) and make sure you have a subsidiary code on your warehouse."));
                //}
                if (NSC_DI.SAP.Items.GetField<string>(itemCode, "ManBtchNum") == "Y")
                {
                    // get the source batch object - have to do this here because once the the PdO is received, the BACKFLUSH items will be issued and mess up the data.
                    // this is for copying the batch UDFs.
                    var oldBatchID = NSC_DI.SAP.ProductionOrder.GetBatchNumberFromIssue(productionOrderKey);
                    oBN = NSC_DI.SAP.BatchItems.GetInfo(oldBatchID);

                    ParentItem.ListOfBatchLineItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem>()
                    {
                        new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem()
                        {
                            BatchNumber         = newBatchID,
                            Quantity            = quantity,
                            ManufacturerSerialNumber = stateID,
                            ExpDate =  !string.IsNullOrEmpty(expDate) ? NSC_DI.UTIL.Dates.FromUI(expDate) : DateTime.MinValue,
                            LotNumber = NSC_DI.UTIL.Settings.Value.Get("Inter-Company") == "Y" ? prodBatchID : null
                        }
                    };
                }

                var grDocNum = 0;

                //Byproducts
                string strHolder = _dtByProd.Rows.Count.ToString();
                if (_dtByProd.Rows.Count > 0)
                {
                    //ByProducts
                    //var newBatchID_ByProd = NSC_DI.SAP.BatchItems.NextBatch(itemCode, BATCH_PREFIX);

                    // convert to a VS dataTable
                    System.Data.DataTable newDT = new System.Data.DataTable("ByProd");
                    newDT.Columns.Add("ItemCode", typeof(string));
                    newDT.Columns.Add("ItemName", typeof(string));
                    newDT.Columns.Add("PlannedQty", typeof(double));
                    newDT.Columns.Add("wareHouse", typeof(string));
                    newDT.Columns.Add("LineNum", typeof(string));
                    newDT.Columns.Add("BatchID", typeof(string));
                    newDT.Columns.Add("StateID", typeof(string));
                    //newDT.Columns.Add("ProdBatchID", typeof(string));

                    for (var i = 0; i < _dtByProd.Rows.Count; i++)
                    {
                        if (double.Parse(_dtByProd.Rows[i]["PlannedQty"].ToString()) == 0) continue;
                        newDT.Rows.Add(_dtByProd.Rows[i]["ItemCode"], _dtByProd.Rows[i]["ItemName"], _dtByProd.Rows[i]["PlannedQty"], _dtByProd.Rows[i]["wareHouse"],
                                       _dtByProd.Rows[i]["LineNum"], NSC_DI.SAP.BatchItems.NextBatch(_dtByProd.Rows[i]["ItemCode"].ToString(), "BP"),
                                       _dtByProd.Rows[i]["StateID"]);//, _dtByProd.Rows[i]["ProdBatchID"]);
                    }

                    //Receive Parent
                    grDocNum = NSC_DI.SAP.ProductionOrder.ReceiveParent(productionOrderKey, DateTime.Now, ParentItem);

                    //Receive ByProducts
                    NSC_DI.SAP.ProductionOrder.ReceiveByProducts(productionOrderKey, newDT, oBN, expDate);

                    // set U_NSC_ProductionBatchID
                    if(prodBatchID.Length > 0)
                        NSC_DI.SAP.ProductionOrder.SetUDF(productionOrderKey, "U_NSC_ProductionBatchID", prodBatchID);
                }
                else
                {
                    //No ByProducts
                    grDocNum = NSC_DI.SAP.ProductionOrder.ReceiveParent(productionOrderKey, DateTime.Now, ParentItem);
                    // set U_NSC_ProductionBatchID
                    if (prodBatchID.Length > 0)
                        NSC_DI.SAP.ProductionOrder.SetUDF(productionOrderKey, "U_NSC_ProductionBatchID", prodBatchID);
                }
                // copy UDFs to reciept obj
                var oTo = NSC_DI.SAP.BatchItems.GetInfoFromDoc(BoObjectTypes.oInventoryGenEntry, grDocNum, 0);
                NSC_DI.UTIL.UDO.CopyUDFs(oTo, oBN, new string[1] { "U_NSC_StateID" }); // may have to add more values, these are UDFs that do not get transferred over

                          

                //NSC_DI.SAP.BatchItems.SetStateID(newBatchID, itemCode, stateID);

                // check if it is a special PdO
                if (NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_NSC_Special FROM OWOR WHERE DocEntry = " + productionOrderKey.ToString(), false) == "Y")
                {
                    if (oBN != null)
                    {
                        // set the batch info
                        oBN.UserFields.Item("U_NSC_PassedQA").Value = "N";
                        oBN.UserFields.Item("U_NSC_LabTestID").Value = "";
                    }
                }

                if (NSC_DI.SAP.Items.GetField<string>(itemCode, "ManBtchNum") == "Y") NSC_DI.SAP.BatchItems.UpdateUDFsFromObj(newBatchID, oBN, new string[1] { "U_NSC_StateID" });

                if (NSC_DI.UTIL.SQL.GetValue<string>($"Select U_NSC_isProductionBatch from OWOR where DocNum = {productionOrderKey}", null).ToUpper() == "Y")//11017 This has to be here because it gets reset otherwise
                {
                    NSC_DI.SAP.BatchItems.SetUDF(oTo.ItemCode, oTo.Batch, "U_NSC_PassedQA", "N");                   
                    NSC_DI.SAP.BatchItems.SetUDF(oTo.ItemCode, oTo.Batch, "U_NSC_ProdBatchID", prodBatchID);
                }
                //Try Receive ByProducts Here

                NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(productionOrderKey, BoProductionOrderStatusEnum.boposClosed);
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                Globals.oApp.StatusBar.SetText("Successfully Receipted Package(s)!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                Load_Matrix(_SBO_Form);
            }
            catch (NSC_DI.SAP.B1Exception ex) { Globals.oApp.StatusBar.SetText(ex.Message); }
            catch (Exception ex)
            {
                _dtByProd.Rows.Clear();
                //_SBO_Form.Close();
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                GC.Collect();
            }
        }
        private void CheckPdoItems(Form pForm)
        {
            try
            {
                SAPbouiCOM.Matrix MTX_MAIN = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form);

                int PdoKey = 0;

                for (int i = 1; i < (MTX_MAIN.RowCount + 1); i++)
                {
                    if (MTX_MAIN.IsRowSelected(i) == false) continue;

                    // now check the PdO and warn if not all "Manual" items have not been received.
                    string sql = $"SELECT COUNT(LineNum) FROM WOR1";
                    //if()
                }

                //if (string.IsNullOrEmpty(itemCode))
                //{
                //    Globals.oApp.StatusBar.SetText("Select a row to process.");
                //    return;
                //}

                //if (string.IsNullOrEmpty(itemCode) || quantity <= 0 || string.IsNullOrEmpty(destinationWarehouseCode))
                //{
                //    Globals.oApp.StatusBar.SetText("Failed to aquire adequate data about the package to complete!");
                //    return;
                //}
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        #region Matrix Click Events

        private void MAIN_MATRIX_LINK_PRESSED(ItemEvent SAP_UI_ItemEvent)
        {
            // Attempt to grab the selected row ID
            int SelectedRowID = SAP_UI_ItemEvent.Row;

            if (SelectedRowID > 0)
            {
                // Which column stores the ID
                int ColumnIDForIDOfItemSelected = 0;

                // Get the ID of the note selected
                string ItemSelected = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form).Columns.Item(ColumnIDForIDOfItemSelected).Cells.Item(SelectedRowID).Specific.Value.ToString();

                // Open up the Production Order Master Data form
                Globals.oApp.ActivateMenuItem("4369");

                // Grab the "Production Order" form from the UI
                Form formProductionOrder = Globals.oApp.Forms.GetForm("65211", 0);

                // Freeze the "Production Order" form
                formProductionOrder.Freeze(true);

                // Change the "Production Order" form to find mode
                formProductionOrder.Mode = BoFormMode.fm_FIND_MODE;

                // Insert the Production Order ID in the appropriate text field
                ((EditText)formProductionOrder.Items.Item("18").Specific).Value = ItemSelected;

                // Click the "Find" button
                ((Button)formProductionOrder.Items.Item("1").Specific).Item.Click();

                // Un-Freeze the "Production Order" form
                formProductionOrder.Freeze(false);
            }
        }

        #endregion

        #region Data Classes
        #endregion

    }
}