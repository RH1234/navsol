﻿using System;
using System.Linq;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms.Product_Processing
{
    class F_PackagingPV : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_PACKAGING_PV";
        private const string BATCH_PREFIX = "PK";
        private const string cPdOReceiptType = "Inventory Convert";

		public string FormUID { get; set; }
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] {cFormID})]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			bool BubbleEvent = true;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeValidate(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            if (pVal.InnerEvent == true) return BubbleEvent;
            if (pVal.ItemChanged == false) return BubbleEvent;
            //if (pVal.ActionSuccess == false) return BubbleEvent;

            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            BubbleEvent = ValidateFields(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] {cFormID})]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "BTN_APPLY":
					BTN_APPLY_Click(oForm);
					break;

                case "grdPdOs":
                    SetRemarks(oForm, pVal);
                    ClearDetails(oForm, pVal);
                    Load_Metrc_Tag_DistNum(oForm, pVal);
                    break;
            }

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, false, new string[] { cFormID })]
        public virtual void OnAfterValidate(ItemEvent pVal)
        {
            if (pVal.InnerEvent == true) return;
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);


            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public F_PackagingPV() : this(Globals.oApp, Globals.oCompany)
		{
		}

		public F_PackagingPV(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
		{
		}

		public void Form_Load()
		{
            Form oForm = null;

            try
            {
                oForm = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                oForm.Freeze(true);

                // Set the main image
                CommonUI.Forms.SetFieldvalue.Icon(oForm, "IMG_MAIN", @"packaging-icon.bmp");

                // if I don't addd these, the fields are cleared by B1
                CommonUI.Forms.CreateUserDataSource(oForm, "txtStart",  "udsStart", BoDataType.dt_SHORT_TEXT, 50);
                CommonUI.Forms.CreateUserDataSource(oForm, "txtNumber", "udsNumber", BoDataType.dt_SHORT_TEXT, 10);
                CommonUI.Forms.CreateUserDataSource(oForm, "txtQtyPer", "udsQtyPer", BoDataType.dt_SHORT_TEXT, 10);
                oForm.DataSources.UserDataSources.Add("UDDate", SAPbouiCOM.BoDataType.dt_DATE, 32);

                ClearQtyGrid(oForm);

                oForm.Items.Item("BTN_APPLY").Enabled = true;

                Load_Grid_PdOs(oForm);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oForm.Freeze(false);
                oForm.VisibleEx = true;
                NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
            }
        }

        private void SetRemarks(Form pForm, ItemEvent pVal)
        {
            SAPbouiCOM.Grid oGrdPdOs = null;
            try
            {
                oGrdPdOs = pForm.Items.Item("grdPdOs").Specific;
                if (pVal.Row < 0) return;
                if (oGrdPdOs.Rows.SelectedRows.Count == 0) return;
                //var row = oGrdPdOs.Rows.SelectedRows.Item(0, BoOrderType.ot_RowOrder);
                var ProdNumder = oGrdPdOs.DataTable.GetValue("Production Order", pVal.Row);

                string Remarks = NSC_DI.UTIL.SQL.GetValue<string>(@"Select OWOR.Comments from OWOR where OWOR.DocNum=" + ProdNumder);
                pForm.Items.Item("TXT_REM").Specific.Value = Remarks;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrdPdOs);
                GC.Collect();
            }
        }

        private void ClearDetails(Form pForm, ItemEvent pVal)
        {
            SAPbouiCOM.Grid oGrdQtys = null;
            try
            {
                // pVal != null is so that this sub can be called at the end of the update
                if (pVal != null && pVal.Row < 0) return;
                if (pVal != null && pVal.ColUID != "RowsHeader") return;

                ClearQtyGrid(pForm);

                pForm.Items.Item("txtNumber").Specific.Value = ""; 
                pForm.Items.Item("txtQtyPer").Specific.Value = "";
                pForm.Items.Item("txtStart").Specific.Value = "";

                pForm.Items.Item("txtNumber").Enabled = true;
                pForm.Items.Item("txtQtyPer").Enabled = true;
                pForm.Items.Item("txtStart").Enabled = true;

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrdQtys);
                GC.Collect();
            }
        }

        private void ClearQtyGrid(Form pForm)
        {
            SAPbouiCOM.DataTable oDT = null;
            SAPbouiCOM.Grid oGrdQtys = null;

            try
            {
                pForm.Freeze(true);

                oGrdQtys = pForm.Items.Item("grdQtys").Specific;
                oGrdQtys.DataTable.Clear();

                oDT = oGrdQtys.DataTable;
                oDT.Columns.Add("Quantity",     BoFieldsType.ft_Integer, 6);
                oDT.Columns.Add("Tag Number",   BoFieldsType.ft_Text,    50);
                oDT.Columns.Add("Lot Number",   BoFieldsType.ft_Text,    50);

                oGrdQtys.Columns.Item("Quantity").Editable = false;
                oGrdQtys.Columns.Item("Tag Number").Editable = false;
                oGrdQtys.Columns.Item("Lot Number").Editable = true;

                oGrdQtys.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oDT);
                NSC_DI.UTIL.Misc.KillObject(oGrdQtys);
                GC.Collect();
            }
        }

        private bool ValidateFields(Form pForm, ItemEvent pVal)
        {
            SAPbouiCOM.Grid oGrdQtys = null;
            SAPbouiCOM.Grid oGrdPdOs = null;

            try
            {
                //var s = pForm.Items.Item("txtQtyPer").Specific.Value;
                if (pForm.Items.Item(pVal.ItemUID).Type != BoFormItemTypes.it_EDIT) return true;    // if not a field, then return
                var fieldVal = pForm.Items.Item(pVal.ItemUID).Specific.Value;                       // get the value of the field

                oGrdPdOs = pForm.Items.Item("grdPdOs").Specific;
                oGrdQtys = pForm.Items.Item("grdQtys").Specific;

                ClearQtyGrid(pForm);

                if (pVal.ItemUID == "txtStart")
                {
                    pForm.Items.Item("txtQtyPer").Specific.Value = "";
                    pForm.Items.Item("txtNumber").Specific.Value = "";
                    pForm.Items.Item("txtQtyPer").Enabled = true;
                    pForm.Items.Item("txtNumber").Enabled = true;
                    return true;
                }

                var row = oGrdPdOs.Rows.SelectedRows.Item(0, BoOrderType.ot_RowOrder);
                if (row < 0) return true;
                if (fieldVal == "" || fieldVal == "0")
                {
                    pForm.Items.Item("txtQtyPer").Specific.Value = "";
                    pForm.Items.Item("txtNumber").Specific.Value = "";
                    pForm.Items.Item("txtQtyPer").Enabled = true;
                    pForm.Items.Item("txtNumber").Enabled = true;
                    return true;
                }

                if (oGrdPdOs.Rows.SelectedRows.Count == 0) return true;
                var totQty = NavSol.CommonUI.Forms.GetField<int>(pForm, "grdPdOs", row, "Total Quantity");
                //if (System.Diagnostics.Debugger.IsAttached) totQty = 10;                                    // +++++++++++++++++++++++++++++++++++
                var numBat = 0;
                var qtyPer = 0;

                //----------------------------
                // # of batches
                if (pVal.ItemUID == "txtNumber")
                {
                    numBat = NavSol.CommonUI.Forms.GetField<int>(pForm, "txtNumber");
                    if (numBat > totQty || numBat < 0)
                    {
                        Globals.oApp.StatusBar.SetText("# of Batches is more than the Total Quantity or is less than zero.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                        return false;
                    }

                    qtyPer = totQty / numBat;   // (int)Math.Truncate((decimal)totQty / (decimal)numBat)
                    pForm.Items.Item("txtQtyPer").Enabled = false;
                    pForm.Items.Item("txtQtyPer").Specific.Value = qtyPer;
                }

                //----------------------------
                // Qty Per batch
                if (pVal.ItemUID == "txtQtyPer")
                {
                    qtyPer = NavSol.CommonUI.Forms.GetField<int>(pForm, "txtQtyPer");
                    if (qtyPer < 0)
                    {
                        Globals.oApp.StatusBar.SetText("Qty Per Batch is less than zero.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                        return false;
                    }

                    numBat = (int)Math.Ceiling((decimal)totQty / (decimal)qtyPer);
                    pForm.Items.Item("txtNumber").Enabled = false;
                    pForm.Items.Item("txtNumber").Specific.Value = numBat;
                }

                //----------------------------
                // load batch grid
                Load_Grid_Qtys(pForm, totQty, numBat, qtyPer);

                return true;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return false;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrdPdOs);
                NSC_DI.UTIL.Misc.KillObject(oGrdQtys);
                GC.Collect();
            }
        }

        private void Load_Grid_PdOs(Form pForm)
        {
            SAPbouiCOM.Grid oGrdPdOs = null;
            try
            {
                oGrdPdOs = pForm.Items.Item("grdPdOs").Specific;               

                string orderStatus = "R";
                string minDate = DateTime.MinValue.ToString("MM/dd/yyyy");

                string sql = $@"
SELECT[OWOR].[DocEntry] as 'Production Order', [OWOR].[PlannedQty] as 'Total Quantity', [OITM].InvntryUom as 'UoM', OBTN.MnfSerial as StateID,
(SELECT OITL.DocDate FROM OITL WHERE DocDate <= '12/31/1950') AS ExpDate -- This line is so that we can get a null date in the Grid column
FROM [OWOR]
JOIN [OITM] ON [OITM].[ItemCode] = [OWOR].[ItemCode]
JOIN [OWHS] ON [OWHS].[WhsCode] = [OWOR].[Warehouse]
JOIN [IGE1] ON IGE1.BaseEntry = OWOR.DocEntry
JOIN [OITL] ON OITL.DocEntry = IGE1.DocEntry AND OITL.DocLine = IGE1.LineNum AND OITL.DocType = IGE1.ObjType AND OITL.StockEff = 1
JOIN [ITL1] ON ITL1.LogEntry = OITL.LogEntry 
JOIN [OBTN] ON OBTN.ItemCode = ITL1.ItemCode AND OBTN.SysNumber = ITL1.SysNumber 
WHERE [OWOR].[Status] = '{orderStatus}' AND [OWOR].[U_NSC_CnpProduct] = 'Y' AND [OWOR].[U_NSC_Process] = 'Packaging'";
                if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})"; //10823-2
                sql += $@"ORDER BY CONVERT(int, [OWOR].[DocNum]) DESC";

                oGrdPdOs.DataTable.ExecuteQuery(sql);

                for (var i = 0; i < oGrdPdOs.Columns.Count; i++)
                {
                    oGrdPdOs.Columns.Item(i).Editable = false;
                }
                oGrdPdOs.Columns.Item("ExpDate").Editable = true;
                //for(int i = 0; i < oGrdPdOs.Rows.Count; i++)
                //{
                //    oGrdPdOs.DataTable.Columns.Item("ExpDate").Cells.Item(i).Value = "";
                //}
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oGrdPdOs);
                GC.Collect();
            }
        }

        private void Load_Grid_Qtys(Form pForm, int pTotQty, int pNumBat, int pQtyPer)
        {
            SAPbouiCOM.DataTable oDT = null;

            try
            {
                pForm.Freeze(true);

                var sTag = pForm.Items.Item("txtStart").Specific.Value;

                oDT = pForm.Items.Item("grdQtys").Specific.DataTable;
                for (var i = 0; i < pNumBat; i++)
                {
                    oDT.Rows.Add(1);
                    if (i == pNumBat - 1) pQtyPer = pTotQty - (pQtyPer * (pNumBat - 1)); // have to adjust the quantity for the last row

                    oDT.SetValue("Quantity", i, pQtyPer);
                    oDT.SetValue("Tag Number", i, NSC_DI.UTIL.AutoStrain.NextTagNumber(sTag, i));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }

        private void BTN_APPLY_Click(Form pForm, int pByProductsCollected =0, string pByProdCB="")
        {
            SAPbouiCOM.Grid oGrdPdOs            = null;
            SAPbobsCOM.Documents oGR            = null;
            SAPbouiCOM.DataTable oQtyDT         = null;
            SAPbobsCOM.ProductionOrders oPdO    = null;
            SAPbobsCOM.BatchNumberDetail oBN    = null;
            var sql = "";

            try
            {
                // -------------------------------------
                // get the Production Order and validate
                // -------------------------------------
                oGrdPdOs = pForm.Items.Item("grdPdOs").Specific;
                if (oGrdPdOs.Rows.SelectedRows.Count == 0) return;
                var row = oGrdPdOs.Rows.SelectedRows.Item(0, BoOrderType.ot_RowOrder);
                if (row < 0)
                {
                    Globals.oApp.StatusBar.SetText("Select a row to process.");
                    return;
                }
                //oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                var pdoKey = NavSol.CommonUI.Forms.GetField<int>(pForm, "grdPdOs", row, "Production Order");
                oPdO = NSC_DI.SAP.ProductionOrder.OpenPdO(pdoKey);

                sql = $"SELECT CASE WHEN COUNT(LineNum) > 0 THEN 'Y' ELSE 'N' END FROM WOR1 WHERE Docentry = {pdoKey} AND IssueType = 'M' AND IssuedQty = 0 AND PlannedQty > 0";
                var IssueMore = NSC_DI.UTIL.SQL.GetValue<string>(sql);
                if (IssueMore == "Y")
                {
                    // not all of the "Manual" components have been issued
                    if (Globals.oApp.MessageBox("Not all of the 'Manual' components have been issued.\rDo You want to continue?", 2, "Yes", "No") == 2) return;
                }

                //Inter-Company Check
                //string InterCoSubCode = NSC_DI.SAP.Warehouse.GetField<string>(oPdO.Warehouse, "U_NSC_SubsidiaryID");// DELETE 12/30/2020 

                //// Set so the user cannot set a production batch id for a PO that is not 
                //if (!String.IsNullOrWhiteSpace(oPdO.UserFields.Fields.Item("U_NSC_ProductionBatchID").Value) && oPdO.UserFields.Fields.Item("U_NSC_ProductionBatchID").Value.ToUpper() == "N")
                //{
                //    Globals.oApp.StatusBar.SetText($"The Production Order is not selected as a Production Batch");
                //    return;
                //}

                //!string.IsNullOrEmpty(NavSol.CommonUI.Forms.GetField<string>(pForm, "grdPdOs", row, "ExpDate")) ? NavSol.CommonUI.Forms.GetField<string>(pForm, "grdPdOs", row, "ExpDate") : "";
                string stateID = String.Empty;
                string prodBatchID = "";
                DateTime expDate = NavSol.CommonUI.Forms.GetField<DateTime>(pForm, "grdPdOs", row, "ExpDate");
                oQtyDT = pForm.Items.Item("grdQtys").Specific.DataTable;
                if (oQtyDT.Rows.Count < 1)
                {
                    Globals.oApp.SetStatusBarMessage("No Batches were specified.", BoMessageTime.bmt_Short, true);
                    return;
                }

                // -------------------------------------
                // create the Goods Receipt
                // -------------------------------------
                var oldBatchID = NSC_DI.SAP.ProductionOrder.GetBatchNumberFromIssue(pdoKey);
                oBN = NSC_DI.SAP.BatchItems.GetInfo(oldBatchID);

                Globals.oCompany.StartTransaction();

                oGR = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry);
                oGR.DocDate = DateTime.Now;
                oGR.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items;

                var getEmpID = NSC_DI.SAP.Employee.GetEmpID_User();
                if (getEmpID != null) oGR.UserFields.Fields.Item("U_NBS_EmpID").Value = getEmpID;

                oGR.Lines.BaseEntry     = pdoKey;
                oGR.Lines.BaseType      = (int)SAPbobsCOM.BoObjectTypes.oProductionOrders;
                oGR.Lines.Quantity      = oPdO.PlannedQuantity;
                oGR.Lines.WarehouseCode = oPdO.Warehouse;

                // batches
                var newBatchID = NSC_DI.SAP.BatchItems.NextBatch(oPdO.ItemNo, oQtyDT.Rows.Count, BATCH_PREFIX);
                for (var i = 0; i < oQtyDT.Rows.Count; i++)
                {
                    if (i > 0) oGR.Lines.BatchNumbers.Add();

                    oGR.Lines.BatchNumbers.BatchNumber              = newBatchID.ElementAt(i);
                    oGR.Lines.BatchNumbers.Quantity                 = oQtyDT.GetValue("Quantity", i);
                    oGR.Lines.BatchNumbers.ManufacturerSerialNumber = oQtyDT.GetValue("Tag Number", i);
                    if(expDate != DateTime.MinValue)
                        oGR.Lines.BatchNumbers.ExpiryDate = expDate;
                    NSC_DI.UTIL.UDO.CopyUDFs(oGR.Lines.BatchNumbers, oBN, new string[1] { "U_NSC_StateID" });

                    // see if a special PdO
                    if (oPdO.UserFields.Fields.Item("U_NSC_Special").Value == "Y")
                    {
                        oGR.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_PassedQA").Value = "N";
                        oGR.Lines.BatchNumbers.UserFields.Fields.Item("U_NSC_LabTestID").Value = "";
                    }
                }

                // Branch
                var br = NSC_DI.SAP.Branch.Get(oPdO.Warehouse);
                if (br > 0) oGR.BPL_IDAssignedToInvoice = br;

                // Default Distribution Rules
                NSC_DI.SAP.Document.SetDefaultDistributionRules(oGR.Lines);

                // -------------------------------------
                // ADD THE GOODS RECEIPT
                // -------------------------------------
                NSC_DI.SAP.Document.Add(oGR);

                // -------------------------------------
                // set the Lot Numbers
                // -------------------------------------
                var j = 0;
                foreach(var batch in newBatchID)
                {
                    var lot = oQtyDT.GetValue("Lot Number", j++);
                    if(lot.Trim().Length > 0) NSC_DI.SAP.BatchItems.setAttribute2(batch, oPdO.ItemNo, lot);
                }

                // -------------------------------------
                // CLOSE THE PRODUCTION ORDER
                // -------------------------------------
                oPdO.ProductionOrderStatus = BoProductionOrderStatusEnum.boposClosed;
                NSC_DI.SAP.Document.Update(oPdO);

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                Globals.oApp.StatusBar.SetText("Successfully Receipted Package(s)!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                ClearDetails(pForm, null);
                Load_Grid_PdOs(pForm);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                NSC_DI.UTIL.Misc.KillObject(oGR);
                NSC_DI.UTIL.Misc.KillObject(oPdO);
                NSC_DI.UTIL.Misc.KillObject(oQtyDT);
                NSC_DI.UTIL.Misc.KillObject(oGrdPdOs);
                GC.Collect();
            }
        }

        private void Load_Metrc_Tag_DistNum(Form pForm, ItemEvent pVal)
        {
            if (NSC_DI.UTIL.Settings.Value.Get("State Compliance").ToUpper() == "METRC")
            {
                SAPbouiCOM.Grid oGrdPdOs = null;
                try
                {
                    var test = pVal;

                    oGrdPdOs = pForm.Items.Item("grdPdOs").Specific;
                    if (oGrdPdOs.Rows.SelectedRows.Count == 0) return;
                    var row = oGrdPdOs.Rows.SelectedRows.Item(0, BoOrderType.ot_RowOrder);
                    //oPdO = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                    var pdoKey = NavSol.CommonUI.Forms.GetField<int>(pForm, "grdPdOs", row, "Production Order");

                    string sql;
                    // 12963 if the Compliance is Metrc then we need to check to see if their exists an Metrc Tag issued associated with each pdo
                    //for (int i = 1; i <= oGrdPdOs.RowCount; i++)
                    //{
                    //    string PdO = oGrdPdOs.Columns.Item(MatrixColumns.ProductionOrder).Cells.Item(i).Specific.Value;
                    sql = $@"
SELECT TOP(1) OSRI.IntrSerial
FROM OSRI
INNER JOIN SRI1 ON OSRI.ItemCode = SRI1.ItemCode and OSRI.SysSerial = SRI1.SysSerial
INNER JOIN OITM ON SRI1.ItemCode = OITM.ItemCode
WHERE SRI1.BsDocEntry = {pdoKey} AND SRI1.BaseType = 60 AND OITM.QryGroup36 = 'Y'
ORDER BY OSRI.IntrSerial ASC";
                    // SQL looks for the serial number of the issued metrc tag, if it exist, if not just returns null/""
                    string metrcTagDistNum = NSC_DI.UTIL.SQL.GetValue<string>(sql);
                    if (!NSC_DI.UTIL.Strings.Empty(metrcTagDistNum))
                    {
                        // if there is a serial number associated with an issued metrc tag then the serial number of metrc tag is set as the stateid that will be associated with the receipt
                        // Then makes the stateid cell uneditable
                        pForm.Items.Item("txtStart").Specific.Value = metrcTagDistNum;
                        pForm.Items.Item("txtNumber").Specific.Active = true;
                        pForm.Items.Item("txtStart").Enabled = false;
                        //oGrdPdOs.CommonSetting.SetCellEditable(i, (int)MatrixColumns.StateID, false);
                    }
                }
                catch (Exception ex)
                {
                    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                }
                finally
                {
                    NSC_DI.UTIL.Misc.KillObject(oGrdPdOs);
                }

                
                //}
            }
        }
    }
}