﻿using System;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms
{
	class F_GreenbookNewNote : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "NSC_GBOOK_NOTE";

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			try
			{
				pForm.Freeze(true);
				pForm.Mode = BoFormMode.fm_ADD_MODE;

				pForm.Items.Item("txt_cdate").Specific.Value = DateTime.UtcNow.ToString("yyyyMMdd");
				pForm.Items.Item("txt_crby").Specific.Value = Globals.oCompany.UserName;
				var sql = "SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tGBNote + "]";
				pForm.Items.Item("note_id").Specific.Value = NSC_DI.UTIL.SQL.GetValue<int>(sql, 1);

				pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null, bool pLoadForm = true)
		{
            Form oForm = null;
            Item oItm = null;
			try
			{
                oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				if(pLoadForm) FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
                NSC_DI.UTIL.Misc.KillObject(oForm);
                GC.Collect();
			}
		}

		public static Form Form_Load_Find(int ID)
		{
			EditText note_id = null;
			Button	 btn_1	 = null;

			try
			{
				var oForm = FormCreate(false, null, false);
				oForm.Freeze(true);
				oForm.Mode = BoFormMode.fm_FIND_MODE;

                // Find the relevant controls we will want to make edits to
				note_id = oForm.Items.Item("note_id").Specific;
				btn_1	= oForm.Items.Item("1").Specific;

                // Only allow noteID to be edited during "Find" mode.
                note_id.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_All), BoModeVisualBehavior.mvb_False);
                note_id.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_Find), BoModeVisualBehavior.mvb_True);

                // Enable UDO Form navigation buttons
				oForm.DataBrowser.BrowseBy = "note_id";

                // Set the ID of the note
                note_id.Value = ID.ToString();

                // Click the "Find" button
                btn_1.Item.Click();
				
				oForm.VisibleEx = true;
				return oForm;
			}
 			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(note_id);
				NSC_DI.UTIL.Misc.KillObject(btn_1);
				GC.Collect();
			}
       }

	}
}
