﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms
{
    class F_QuarantineWizard_Batch : B1Event
    {
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "NSC_QUARBATCH_WIZ";

		private enum MatrixColumn { ItemCode, ItemName, Quantity, UniqueID, StateId, WarehouseCode }

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "btn_ok":
					BTN_OK_Click(oForm);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_COMBO_SELECT
		[B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
		public virtual void OnAfterComboSelect(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			if (pVal.ItemUID == "CMB_QTYPE") CMB_QTYPE_Selected(oForm);

            if (pVal.ItemUID == "CMB_WHSE") CMB_WHSE_Selected(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (ChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        private static void FormSetup(Form pForm)
		{
			try
			{
				pForm.Freeze(true);

                CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_Main", @"quarantine-icon.bmp");

                // set the Quarantine Type Valid Values
                //List<Tuple<string, string>> list = new List<Tuple<string, string>>();
                //if (NSC_DI.UTIL.Settings.Value.Get("State Compliance", false).ToUpper() == "BIOTRACK")
                //{
                //    list.Add(new Tuple<string, string>("0", "Other"));
                //    list.Add(new Tuple<string, string>("1", "Waste"));
                //    list.Add(new Tuple<string, string>("2", "Unhealthy or Died"));
                //    list.Add(new Tuple<string, string>("3", "Infestation"));
                //    list.Add(new Tuple<string, string>("4", "Product Return"));
                //    list.Add(new Tuple<string, string>("5", "Mistake"));
                //    list.Add(new Tuple<string, string>("6", "Spoilage"));
                //    list.Add(new Tuple<string, string>("7", "Quality Control"));
                //}
                //else
                //{
                //    list.Add(new Tuple<string, string>("DEST", "Destroy"));
                //    list.Add(new Tuple<string, string>("SALE", "Sale"));
                //    list.Add(new Tuple<string, string>("SICK", "Sick"));
                //}

                //CommonUI.ComboBox.ComboBoxAddValsS(pForm, "CMB_QTYPE", list, null);
                CommonUI.ComboBox.ComboBoxAdd_UDT_CN(pForm, "CMB_QTYPE", NSC_DI.Globals.tDestructTypes);

                FormLoad(pForm);

				pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
				GC.Collect();
			}
		}

		private static void FormLoad(Form pForm)
	    {
		    Matrix mtx_plan = null;

		    try
		    {
			    // Prepare the SQL statement that will load the selected plants data into the matrix
			    var sql = @"
SELECT [OITM].[ItemCode], [OITM].[ItemName], [OIBT].[Quantity] AS [Quantity], [OIBT].[BatchNum] AS [UniqueID], [OBTN].[U_NSC_StateID] AS [StateId], [OIBT].[WhsCode] AS [WarehouseCode]
  FROM [OIBT] 
  JOIN [OITM] ON [OITM].[ItemCode] = [OIBT].[ItemCode]
  JOIN [OBTN] ON [OBTN].[DistNumber] = [OIBT].[BatchNum]
  JOIN [OWHS] ON [OIBT].[WhsCode] = [OWHS].[WhsCode]
 WHERE [OIBT].[Quantity] > 0
   AND OBTN.U_NSC_QuarState NOT IN ('DEST', 'SALE')
";
                var wh = CommonUI.Forms.GetField<string>(pForm, "CMB_WHSE", true);
                var branch = NSC_DI.SAP.Warehouse.GetBranch(wh);
                if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({branch})";     //10823-2

                // Load data about selected plants into the matrix
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "OIBT", "mtx_plan", new List<CommonUI.Matrix.MatrixColumn>()
				    {
                         new CommonUI.Matrix.MatrixColumn(){ ColumnName=MatrixColumn.ItemCode.ToString("F"), ColumnWidth=80, Caption="Code", ItemType = BoFormItemTypes.it_EDIT}               
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName=MatrixColumn.ItemName.ToString("F"), ColumnWidth=40, Caption="Item Name", ItemType= BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName=MatrixColumn.Quantity.ToString("F"), ColumnWidth=80, Caption="Quantity", ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName=MatrixColumn.UniqueID.ToString("F"), ColumnWidth=80, Caption="Unique ID", ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName=MatrixColumn.StateId.ToString("F"), ColumnWidth=80, Caption="State Id", ItemType = BoFormItemTypes.it_EDIT}
                        ,new CommonUI.Matrix.MatrixColumn(){ ColumnName=MatrixColumn.WarehouseCode.ToString("F"), ColumnWidth=80, Caption="Warehouse Code", ItemType = BoFormItemTypes.it_EDIT}
                    }, sql
					);

				mtx_plan = pForm.Items.Item("mtx_plan").Specific;
			    mtx_plan.SelectionMode = BoMatrixSelect.ms_Auto;
		    }
		    catch (Exception ex)
		    {
			    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
                pForm.VisibleEx = true;
				NSC_DI.UTIL.Misc.KillObject(mtx_plan);
			    GC.Collect();
		    }
	    }

	    public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
	    {
		    Item oItm = null;
		    try
		    {
			    var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
			    {
				    oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
				    oItm.Top = -20;
				    oItm.Specific.Value = pCallingFormUID;
			    }

			    FormSetup(oForm);
			    return oForm;
		    }
		    catch (Exception ex)
		    {
			    throw new Exception(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
				NSC_DI.UTIL.Misc.KillObject(oItm);
			    GC.Collect();
		    }
	    }

		private void CMB_QTYPE_Selected(Form pForm)
        {
            // Find the quarantine type ComboBox in the form UI
			ComboBox CMB_QTYPE = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.ComboBox, "CMB_QTYPE", pForm);
            
            // Keep track of the type of warehouses we will be looking for
            var sql = "SELECT [WhsCode], [WhsName] FROM [OWHS] WHERE [U_" + Globals.SAP_PartnerCode + "_" + "WhrsType] = '";
            var whType = "";
            var val = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT U_QuarState FROM [@{NSC_DI.Globals.tDestructTypes}] WHERE Code = '{CMB_QTYPE.Value.ToString()}'");

            switch (val)
            {
                case "DEST":
                    whType = "QND";
                    sql += "QND";
                    sql += "'";
                    break;

                case "SALE":
                    whType = "QNS";
                    sql += "QNS";
                    sql += "'";
                    break;

                default:
                    sql = "SELECT [WhsCode], [WhsName] FROM [OWHS] WHERE [U_" + Globals.SAP_PartnerCode + "_" + "WhrsType] LIKE 'Q%' AND [Inactive] = 'N'";
                    break;
            }
            if (Globals.BranchDflt >= 0) sql += $" AND OWHS.BPLid IN ({Globals.BranchList})";                                       // #10823

            if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y")
            {
                NavSol.CommonUI.CFL.CreateWH(pForm, "CFL_WH", "CMB_WHSE");
                if(whType == "")
                    NavSol.CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_NONE, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_START, "Q");
                else
                    NavSol.CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_NONE, "U_" + Globals.SAP_PartnerCode + "_" + "WhrsType", BoConditionOperation.co_EQUAL, whType);

                NavSol.CommonUI.CFL.AddCon(pForm, "CFL_WH", BoConditionRelationship.cr_AND, "Inactive", BoConditionOperation.co_EQUAL, "N");
                NavSol.CommonUI.CFL.AddCon_Branches(pForm, "CFL_WH", BoConditionRelationship.cr_AND);   //10823
                return;
            }

            // Prepare to run a SQL statement.
            SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

            // Count how many current records exist within the database.
            oRecordSet.DoQuery(sql);

            pForm.Freeze(true);
            // Find the Combobox of Warehouses from the Form UI
            ComboBox CMB_WHSE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_WHSE", pForm);
            try
            {
               

                // Remove all currently existing values from warehouse drop down
                if (CMB_WHSE.ValidValues.Count > 0)
                {
                    for (var i = CMB_WHSE.ValidValues.Count; i-- > 0;)
                    {
                        CMB_WHSE.ValidValues.Remove(i, BoSearchKey.psk_Index);
                    }
                }

                //if (oRecordSet.RecordCount > 1)
                //{
                    CMB_WHSE.ValidValues.Add("", "");
                    CMB_WHSE.Select(0, BoSearchKey.psk_Index);
                    // throws an error, but works
                    try { CMB_WHSE.ValidValues.Remove(0, BoSearchKey.psk_Index); } catch { };
                //}

                oRecordSet.MoveFirst();

                // Add allowed warehouses to the drop down
                for (var i = 0; i < oRecordSet.RecordCount; i++)
                {
                    try
                    {
                        CMB_WHSE.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
                    }
                    catch { }
                    oRecordSet.MoveNext();
                }

                // If a single value is present, select it
                if (CMB_WHSE.ValidValues.Count >= 1)
                {
                    // Select the first item in the combobox
                    CMB_WHSE.Select(0, BoSearchKey.psk_Index);
                }

                pForm.Items.Item("CMB_WHSE").Update();
            }
            catch { }
            finally
            {
                pForm.Freeze(false);

                
                if (CMB_WHSE.ValidValues.Count > 1)
                {                   
                    CMB_WHSE.Item.Enabled = true;
                }
            }

        }

        private void CMB_WHSE_Selected(Form pForm)
        {
            //10823-2
            //
            // this method is required because if a method is called from an event handler,
            // the method must call MessageBox and not throw a new exception

            try
            {
                if (Globals.BranchDflt >= 0)
                    FormLoad(pForm);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
            }
        }
        private void BTN_OK_Click(Form pForm)
	    {
		    // ProgressBar oBar = null;
		    Matrix mtx_plan = null;
		    // EditText TXT_REMARK = null;

		    try
		    {
			    // oBar = Globals.oApp.StatusBar.CreateProgressBar("Starting to Quaratine!", 2, false);

			    #region Creating required controllers - Api Included.  --------------------------------------------------------
/*		// COMPLIANCE
				// Prepare a connection to the state API
				var TraceCon = new NSC_DI.SAP.TraceabilityAPI(
					SAPBusinessOne_Application: Globals.oApp,
					SAPBusinessOne_Company: Globals.oCompany
					);

				// Create an API call -> inventory_manifest
				BioTrack.API bioTrackApi = TraceCon.new_API_obj();
*/
			    #endregion Creating required controllers


			    // Make sure a warehouse was selected
				var destinationWarehouseCode = CommonUI.Forms.GetField<string>(pForm, "CMB_WHSE", true);

			    if (string.IsNullOrEmpty(destinationWarehouseCode))
			    {
				    Globals.oApp.StatusBar.SetText("Please select a destination warehouse first!");
                    throw new System.ComponentModel.WarningException();
			    }

			    // Grab the matrix from the form UI
			    mtx_plan = pForm.Items.Item("mtx_plan").Specific;

			    var BarcodesOfBatchesToDestroy = new List<string>();

				var transferRequestsByWarehouse =
					new Dictionary<string, NSC_DI.SAP.BatchItems.StockTransferCreate>();

                // UpdateProgressBar(oBar, "Finding your selections...", 1);

                var selectedRow = mtx_plan.GetNextSelectedRow(0, BoOrderType.ot_RowOrder);

                if (selectedRow < 0)
                {
                    Globals.oApp.StatusBar.SetText("Please select batch(es) to destroy first!");
                    throw new System.ComponentModel.WarningException();
                }

                while (selectedRow >= 0)
			    {
                    
                    var itemCode = CommonUI.Forms.GetField<string>(pForm, "mtx_plan", selectedRow, 0);
                    var quantity = CommonUI.Forms.GetField<double>(pForm, "mtx_plan", selectedRow, 2);
                    var uniqueId = CommonUI.Forms.GetField<string>(pForm, "mtx_plan", selectedRow, 3);
                    var stateId = CommonUI.Forms.GetField<string>(pForm, "mtx_plan", selectedRow, 4);
                    var sourceWarehouseCode = CommonUI.Forms.GetField<string>(pForm, "mtx_plan", selectedRow, 5);

					var stockTransferDocument = new NSC_DI.SAP.BatchItems.StockTransferCreate(sourceWarehouseCode, $"Transfering Serial/Batch Items from {sourceWarehouseCode} to {destinationWarehouseCode}");

				    BarcodesOfBatchesToDestroy.Add(stateId);

				    if (transferRequestsByWarehouse.ContainsKey(sourceWarehouseCode))
				    {
						var newLine = new NSC_DI.SAP.BatchItems.StockTransferLine()
					    {
						    BatchNumber = uniqueId,
						    DestinationWarehouseCode = destinationWarehouseCode,
						    ItemCode = itemCode,
						    Quantity = quantity
					    };

					    transferRequestsByWarehouse[sourceWarehouseCode].StockTransferLines.Add(newLine);
				    }
				    else
				    {
					    stockTransferDocument.DocDate = DateTime.Now;
					    stockTransferDocument.TaxDate = DateTime.Now;
					    stockTransferDocument.FromWarehouse = sourceWarehouseCode;
						stockTransferDocument.StockTransferLines = new List<NSC_DI.SAP.BatchItems.StockTransferLine>();
                        

						var newLine = new NSC_DI.SAP.BatchItems.StockTransferLine()
					    {
						    BatchNumber = uniqueId,
						    DestinationWarehouseCode = destinationWarehouseCode,
						    ItemCode = itemCode,
						    Quantity = quantity
                            
					    };
					    stockTransferDocument.StockTransferLines.Add(newLine);

					    transferRequestsByWarehouse.Add(sourceWarehouseCode, stockTransferDocument);
				    }

                    selectedRow = mtx_plan.GetNextSelectedRow(selectedRow, BoOrderType.ot_RowOrder);
			    }

                //UpdateProgressBar(oBar, string.Format("Finished found {0}", BarcodesOfBatchesToDestroy.Count), 1);

                // Find the quarantine type ComboBox in the form UI
                string QuarantineType = CommonUI.Forms.GetField<string>(pForm, "CMB_QTYPE");

                //var selectedState = NSC_DI.UTIL.Misc.GetQuarantineStateFromString(QuarantineType);
                var selectedState = NSC_DI.UTIL.Misc.GetQuarantineStateFromString(NSC_DI.UTIL.Misc.GetQuarantineStateFromType(QuarantineType));

                // oBar.Stop();
                // oBar = Globals.oApp.StatusBar.CreateProgressBar("Moving Selected Inventory to Quarantine", BarcodesOfBatchesToDestroy.Count, false);

                // Foreach list of plants within a "From Warehouse"
                if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

			    foreach (var transfers in transferRequestsByWarehouse)
			    {
				    // Transfer Inventory
					NSC_DI.SAP.BatchItems.CreateStockTransfer(transfers.Value);

				    foreach (var transferLines in transfers.Value.StockTransferLines)
				    {
						//UpdateProgressBar(oBar, string.Format("Transfered Item...{0}", BarcodesOfBatchesToDestroy.Count), 1);
						NSC_DI.SAP.BatchItems.ChangeQuarantineStatus(transferLines.BatchNumber, selectedState);
				    }
			    }

                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
			   //  oBar.Stop();

			    // oBar = Globals.oApp.StatusBar.CreateProgressBar("API Call", 2, false);
			    // UpdateProgressBar(oBar, "Creating State Complaincy Call", 1);

				if (selectedState == NSC_DI.Globals.QuarantineStates.DEST)
			    {
				    // I'm assuming that we are not caring if they specify this or not.
					//TXT_REMARK = pForm.Items.Item("TXT_REMARK").Specific;

				 //   var reasonForDestruction = "Reason not specified";
				 //   if (!string.IsNullOrEmpty(TXT_REMARK.Value))
				 //   {
					//    reasonForDestruction = TXT_REMARK.Value.ToString();
				 //   }

			    }

                // oBar.Stop();

                Globals.oApp.StatusBar.SetText("Quarantine batch completed!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

			    // Close the quarantine wizard
			    pForm.Close();
		    }
            //catch (System.ComponentModel.WarningException ex) { }
            catch (NSC_DI.SAP.B1Exception ex)
            {
                Globals.oApp.StatusBar.SetText(ex.Message);
            }
		    catch (Exception ex)
		    {
			    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
		    }
		    finally
		    {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
				//if (oBar != null) oBar.Stop();
				//NSC_DI.UTIL.Misc.KillObject(oBar);
			    GC.Collect();
		    }
	    }

	    private void UpdateProgressBar(ProgressBar oBar, string Message, int ProgressToIncreaseBy = 0)
		{
			try
			{
				// Set progress bar message
				oBar.Text = Message;

				// If we are increasing the progress bar size
				if (ProgressToIncreaseBy > 0)
				{
					// Calculate the new progress
					var NewProgressBarValue = oBar.Value + ProgressToIncreaseBy;

					// Make sure our new progress amount is not greater than the maxium allowed
					if (NewProgressBarValue <= oBar.Maximum)
					{
						oBar.Value = NewProgressBarValue;
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
		}

        private void CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            if (pVal.SelectedObjects == null) return;

            try
            {
                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_WH":
                        CommonUI.CFL.SetFieldsWH(pForm, pVal, "CMB_WHSE", "txtWH_CFL");
                        if (Globals.BranchDflt >= 0)
                            FormLoad(pForm); //10823-2
                        break;

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}