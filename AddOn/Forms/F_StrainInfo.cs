﻿using System;
using System.Drawing;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;
using System.Linq;

namespace NavSol.Forms
{
	public class F_StrainInfo : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------

		private const string cFormID = "NSC_STRAIN_INFO";
		
		private const int MAX_IMAGE_WIDTH = 220;
		private const int MAX_IMAGE_HEIGHT = 220;

		private class MediaTabControls
		{
			public PictureBox imgItem;
			public PictureBox imgDominant;
			public PictureBox imgSubDominant;
			public PictureBox imgUnderTone;

			public EditText txtLooks;
			public EditText txtTaste;

			public void LoadControls(Form pForm)
			{
				imgItem			= pForm.Items.Item("IMG_ICON").Specific;
				imgDominant		= pForm.Items.Item("IMG_DOMNT").Specific; 
				imgSubDominant	= pForm.Items.Item("IMG_SDOMNT").Specific; 
				imgUnderTone	= pForm.Items.Item("IMG_UNDTN").Specific; 
				txtTaste		= pForm.Items.Item("TXT_TASTE").Specific; 
				txtLooks		= pForm.Items.Item("TXT_LOOKS").Specific; 
			}
		}

        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_RIGHT_CLICK
        [B1Listener(BoEventTypes.et_RIGHT_CLICK, true, new string[] { cFormID })]
        public virtual bool OnBeforeRightClick(ContextMenuInfo pVal)
        {
            var BubbleEvent = true;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            // disable this pop-up
            BubbleEvent = false;

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			var BubbleEvent = true;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			if (pVal.ItemUID == "1" && oForm.Mode == BoFormMode.fm_ADD_MODE) BubbleEvent = BTN_Add_Click(oForm);
            if (pVal.ItemUID == "1" && oForm.Mode == BoFormMode.fm_UPDATE_MODE) BubbleEvent = UpdateStrainInfo(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}

		//--------------------------------------------------------------------------------------- et_FORM_DATA_ADD
		[B1Listener(BoEventTypes.et_FORM_DATA_ADD, true, new string[] { cFormID })]
		public virtual bool OnBeforeFormDataAdd(BusinessObjectInfo pVal)
		{
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			var BubbleEvent = true;

			if (oForm != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
                case "1":
                    OK_CLICK_After(oForm);
                    break;

                case "BTN_BROWSE":
					BTN_BrowseForFile_Click(oForm);
					break;
                //case "IMG_ICON":
                //    BTN_BrowseForFile_Click(oForm);
                //    break;

            }

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

        private static void OK_CLICK_After(Form pForm)
        {
            Form Form_Waiting = null;
            Form Form_Parent = null;
            try
            {
                Form_Waiting = F_Waiting.FormCreate();
                F_Waiting.SetProgressBar(Form_Waiting, 24);

                if (pForm.Mode == BoFormMode.fm_ADD_MODE) Form_Load(pForm);

                F_Waiting.SetProgressBar(Form_Waiting, 65);

                if (CommonUI.Forms.HasField(pForm, "TXT_PARE"))
                {
                    var parentFormUID = CommonUI.Forms.GetField<string>(pForm, "TXT_PARE");

                    Form_Parent = B1Connections.theAppl.Forms.Item(parentFormUID);

                    try
                    {
                        F_StrainList.Refresh(Form_Parent);
                    }
                    catch
                    {
                    }
                }

                F_Waiting.SetProgressBar(Form_Waiting, 100);
                F_Waiting.Form_Close(Form_Waiting);
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {

            }
        }

        //--------------------------------------------------------------------------------------- et_FORM_RESIZE
        [B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
		public virtual void OnAfterFormResize(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			FormReSize(oForm);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}
		#endregion AFTER EVENT

		//---------------------------------------------- SUBS       --------------------------------------------------------

		private static void FormSetup(Form pForm)
		{
			try
			{
				// add values to conmbo boxes
				NavSol.CommonUI.ComboBox.LoadComboBoxSQL(pForm, "TXT_STYPE", "SELECT * FROM [@" + NSC_DI.Globals.tStrainType + "]", "Code", "Name");
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("TXT_PARE", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);

				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		public static Form FormCreate_Load(string pNewStrainName = null, bool pSingleInstance = false, string pCallingFormUID = null)
		{
			try
			{
				var pForm = FormCreate(pSingleInstance, pCallingFormUID);
				Form_Load(pForm, pNewStrainName);

				return pForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		public static Form FormCreate_Find(string pStrainID = null, bool pSingleInstance = false, string pCallingFormUID = null)
		{
			try
			{
				var pForm = FormCreate(pSingleInstance, pCallingFormUID);
				Form_Find(pForm, pStrainID);

				return pForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		private static void Form_Load(Form pForm, string pNewStrainName = null)
		{
			EditText TextBox_ID = null;

			try
			{
				// Free the Form UI
				pForm.Freeze(true);

				// Select the first tab by default
				pForm.Items.Item("Item_1").Specific.Select();

				//// Set the form to Add mode
				//pForm.Mode = BoFormMode.fm_ADD_MODE;

				// Find the ID and Name textboxes in the UI
				TextBox_ID = pForm.Items.Item("TXT_SID").Specific;

				pForm.Items.Item("TXT_SNAME").Specific.Value = pNewStrainName;

                // TODO: get a predermined id number
                //var idVal = NSC_DI.UTIL.AutoStrain.NextStrainCode("");
                // Set the textbox for the ID to the pre-determined number.
                //TextBox_ID.Value = idVal

                // Only allow task_id to be edited during "Find" mode.
                TextBox_ID.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_All), BoModeVisualBehavior.mvb_False);
				TextBox_ID.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_Find), BoModeVisualBehavior.mvb_True);

				// Set the strain API image
				//((SAPbouiCOM.PictureBox)Helpers.Forms.GetControlFromForm(Helpers.Forms.FormControlTypes.PictureBox, "IMG_API", FormToSearchIn: _SBO_Form)).Picture = Globals.pathToImg + @"\" + "api-logo.bmp";

				// Show the form
				pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);

				NSC_DI.UTIL.Misc.KillObject(TextBox_ID);
				GC.Collect();
			}
		}

		private static void Form_Find(Form pForm, string pStrainID)
		{
			EditText TextBox_ID = null;
			try
			{
				// Free the Form UI
				pForm.Freeze(true);

				// Set form to Find mode
				pForm.Mode = BoFormMode.fm_FIND_MODE;

				// Enable UDO navigation buttons
				pForm.DataBrowser.BrowseBy = "TXT_SID";

				// Find the ID and Name textboxes in the UI
				TextBox_ID = pForm.Items.Item("TXT_SID").Specific;

				// Only allow task_id to be edited during "Find" mode.
				TextBox_ID.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_All), BoModeVisualBehavior.mvb_False);
				TextBox_ID.Item.SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, Convert.ToInt32(BoAutoFormMode.afm_Find), BoModeVisualBehavior.mvb_True);

				// Set the textbox for the ID to the pre-determined number.
				TextBox_ID.Value = pStrainID;

				// Click the "Find" button
				pForm.Items.Item("1").Click();

				// Select the first tab by default
				pForm.Items.Item("Item_1").Specific.Select();

				// TODO: Show real strain average numbers.  THC, CBD, time, etc.
				//Set needed Variables
				//Seed ID
				ComboBox TXT_STYPE = pForm.Items.Item("TXT_STYPE").Specific;
				string strItemSeedId = null;
				switch (TXT_STYPE.Value)
				{
					case "Indica":
						strItemSeedId = "SE-I-" + pStrainID;
						break;
					case "Sativa":
						strItemSeedId = "SE-S-" + pStrainID;
						break;
					case "Hybrid":
						strItemSeedId = "SE-H-" + pStrainID;
						break;
				}


				// Prepare to run a SQL statement.
				Recordset oRecordSet2 = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
				// Count how many current records exist within the database.
				var TXT_SNAME = pForm.Items.Item("TXT_SNAME").Specific as EditText;
				var strSQL = string.Format(@"Declare @MotherCnt integer
Declare @SeedCnt Integer
Declare @AvgTHC float
Declare @AvgCBD float
Declare @AvgCBN float
Declare @AvgTHCA Float
Declare @AvgYeild Float
Select @SeedCnt = (Select Sum(T1.OnHand) from OITW T1 Where T1.ItemCode ='{0}')
Select @MotherCnt = (SELECT Count(T0.[itemName]) as RecCount FROM [OSRI] T0 WHERE T0.[U_NSC_IsMother] = 'Y' and  T0.[itemName] Like '{1}%')
Select @AvgYeild = (Select Round(Avg(T2.Quantity),2) from IGN1 T2 Where T2.ItemCode like 'WC-%{2}')
Select @AvgTHC =(select AVG(T3.U_NSC_THC) from OBTN T3 where T3.ItemCode like '%{2}' and T3.U_NSC_THC is not null and T3.U_NSC_PassedQA='Y')
Select @AvgCBD =(select AVG(T4.U_NSC_CBD) from OBTN T4 where T4.ItemCode like '%{2}' and T4.U_NSC_CBD is not null and T4.U_NSC_PassedQA='Y')
Select @AvgCBN =(select AVG(T5.U_NSC_CBN) from OBTN T5 where T5.ItemCode like '%{2}' and T5.U_NSC_CBD is not null and T5.U_NSC_PassedQA='Y')
Select @AvgTHCA =(select AVG(T6.U_NSC_THCA) from OBTN T6 where T6.ItemCode like '%{2}' and T6.U_NSC_THCA is not null and T6.U_NSC_PassedQA='Y')

Select @MotherCnt as MotherCnt, @SeedCnt as SeedCnt,@AvgYeild as AvgYeild,@AvgTHC as AvgTHC, @AvgCBD as AvgCBD,@AvgTHCA as AvgTHCA, @AvgCBN as AvgCBN",
					strItemSeedId, NSC_DI.UTIL.SQL.FixSQL(TXT_SNAME.Value), pStrainID);
				oRecordSet2.DoQuery(strSQL);

				//Set Seeds Count
				pForm.Items.Item("TXT_SDNUM").Specific.Value = oRecordSet2.Fields.Item("SeedCnt").Value.ToString();

				//Set Mother Count
				pForm.Items.Item("TXT_MTHNUM").Specific.Value = oRecordSet2.Fields.Item("MotherCnt").Value.ToString();
				
				//Set Avg Yeild
				pForm.Items.Item("TXT_AYIELD").Specific.Value = oRecordSet2.Fields.Item("AvgYeild").Value.ToString();
				
				//Set Testing Averages & Yeild
				pForm.Items.Item("TXT_ATHC").Specific.Value = oRecordSet2.Fields.Item("AvgTHC").Value.ToString();
				pForm.Items.Item("TXT_ACBD").Specific.Value = oRecordSet2.Fields.Item("AvgCBD").Value.ToString();
				pForm.Items.Item("TXT_ACBN").Specific.Value = oRecordSet2.Fields.Item("AvgCBN").Value.ToString();

				// Show the form
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.VisibleEx = true;
				pForm.Freeze(false);

				NSC_DI.UTIL.Misc.KillObject(TextBox_ID);
				GC.Collect();
			}
		}
        private static void IMG_DOMNT_Click(Form pForm)
        {

        }
        private static void IMG_SDOMNT_Click(Form pForm)
        {

        }

        private static void IMG_UNDTN_Click(Form pForm)
        {

        }
        private delegate string AskCb();
		private static void BTN_BrowseForFile_Click(Form pForm)
		{
            try
            {
                Delegate ask = new AskCb(AskForFile);
				string SelectedFileName = (string)Globals.oSTAThreadDispatcher.Invoke(ask);

                var tabControlMedia = new MediaTabControls();
				tabControlMedia.LoadControls(pForm);
				
				// No Item was selected.
                if(string.IsNullOrEmpty(SelectedFileName)) return;

                // Load the image into memory
                var img = Image.FromFile(SelectedFileName);

                // Image was null.
                if (img == null) return;

                // Set the picturebox width and height to the selected images.
                float aspectRatio = 1;

                // Check if the Width is greater than the height and breaking our width limitation.
                if (img.Width > img.Height && img.Width > MAX_IMAGE_WIDTH)
                {
                    aspectRatio = (float)(img.Height) / (float)(img.Width);
                    tabControlMedia.imgItem.Item.Width = MAX_IMAGE_WIDTH;
                    tabControlMedia.imgItem.Item.Height = (int)(MAX_IMAGE_WIDTH * aspectRatio);
                }
                // Check if the Height is greater than the width and breaking out height limitation.
                else if (img.Height > img.Width && img.Height > MAX_IMAGE_HEIGHT)
                {
                    aspectRatio = (float)(img.Width) / (float)(img.Height);
                    tabControlMedia.imgItem.Item.Height = MAX_IMAGE_HEIGHT;
                    tabControlMedia.imgItem.Item.Width = (int)(MAX_IMAGE_HEIGHT * aspectRatio);
                }
                else
                {
                    // The Image is in range of our maximum.
                    tabControlMedia.imgItem.Item.Width = img.Width;
                    tabControlMedia.imgItem.Item.Height = img.Height;
                }

                // Set the picturebox image to the selected image.
                tabControlMedia.imgItem.Picture = SelectedFileName;

				Globals.oApp.SetStatusBarMessage("You selected the file '" + SelectedFileName + "'!", BoMessageTime.bmt_Short, false);
			}
            catch (Exception e)
            {
                Globals.oApp.SetStatusBarMessage("Failed to load image!", BoMessageTime.bmt_Short, false);
            }
        }

		private static bool BTN_Add_Click(Form pForm)
		{
            EditText TextBox_Name = null;
            EditText TextBox_ID = null;
            ComboBox ComboBox_StrainType = null;
			try
			{
				if (pForm.Mode != BoFormMode.fm_ADD_MODE) return true;

				// Check for existing strain

				TextBox_Name = pForm.Items.Item("TXT_SNAME").Specific;

				// Validate that the user selected a strain type.

				if (string.IsNullOrEmpty(TextBox_Name.Value))
				{
					Globals.oApp.StatusBar.SetText("Please enter a valid Strain Name!", BoMessageTime.bmt_Short);

					TextBox_Name.Active = true;
					return false;
				}

                TextBox_ID = pForm.Items.Item("TXT_SID").Specific;


                string SQL_Query_FindStrainByName = $"SELECT [Code] FROM [@NSC_STRAIN] WHERE [Name] = '{TextBox_Name.Value.ToString().Replace("'", "''")}' AND Code <> '{NSC_DI.UTIL.SQL.FixSQL(TextBox_ID.Value.ToString())}'"; //[U_StrainName]

                var ResultsFromSQL = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(SQL_Query_FindStrainByName);

				if (ResultsFromSQL != null && ResultsFromSQL.Count > 0)
				{
					Globals.oApp.StatusBar.SetText("There is already a strain named '" + TextBox_Name.Value + "'");

					return false;
				}

                // Grab controls from the form UI that we will want to access
                ComboBox_StrainType = pForm.Items.Item("TXT_STYPE").Specific;
                if (string.IsNullOrEmpty(ComboBox_StrainType.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter a valid Strain Type!", BoMessageTime.bmt_Short);

                    ComboBox_StrainType.Active = true;
                    return false;
                }

				// Validate that the user selected a strain type.
				if (string.IsNullOrEmpty(ComboBox_StrainType.Value))
				{
					Globals.oApp.StatusBar.SetText("Please select a Type of Strain!", BoMessageTime.bmt_Short);
					ComboBox_StrainType.Active = true;
					return false;
				}
                
				string StrainName = TextBox_Name.Value;
                
				Globals.oCompany.StartTransaction();

                if (string.IsNullOrEmpty(TextBox_ID.Value) || NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Code FROM [@{NSC_DI.Globals.tStrains}] WHERE Code = '{TextBox_ID.Value.ToString()}'") == null)
                {
                    string NewStrainID = NSC_DI.UTIL.AutoStrain.NextStrainCode(ComboBox_StrainType.Value);
                    TextBox_ID.Value = NewStrainID;
                }

                var StrainID = TextBox_ID.Value;

                if (NSC_DI.UTIL.Settings.Value.Get("Use Template Items") == "Y")
                {
                    FillItems(StrainID, StrainName); // Template - show progress bar, etc.

                    Globals.oApp.SetStatusBarMessage("All strain items added successfully!", IsError: false);
                    // Create Strain UDO
                    NSC_DI.SAP.Strain.CreateStrain(StrainID, StrainName, ComboBox_StrainType.Value);
                    UpdateStrainInfo(pForm);
                    System.Media.SystemSounds.Beep.Play();
                }
                else // The old way
                {
                    NSC_DI.SAP.Strain.AddItemsToStrain(StrainID, StrainName);
                    NSC_DI.SAP.Strain.AddBOMsToStrain(StrainID, StrainName);
                }

                Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

				return true;
			}
			catch (Exception ex)
			{
				if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
				return false;
			}
			finally
			{
                NSC_DI.UTIL.Misc.KillObject(TextBox_ID);
                NSC_DI.UTIL.Misc.KillObject(TextBox_Name);
                NSC_DI.UTIL.Misc.KillObject(ComboBox_StrainType);
				GC.Collect();
			}
		}
        
        private static void FillItems(string pStrainID, string pStrainName)
        {
            SAPbouiCOM.ProgressBar oBar = null;
            try
            {
                if (NSC_DI.UTIL.SQL.GetValue<int>($"SELECT COUNT(*) FROM [@{NSC_DI.Globals.tAutoItem }]", 0) == 0)
                {
                    throw new Exception("There are no entries in the Auto Items table.");
                }

                var autoCount  = NSC_DI.UTIL.SQL.GetValue<int>("SELECT(SELECT COUNT(*) FROM OITT AS T WITH (NOLOCK) JOIN OITM AS M WITH (NOLOCK) ON T.Code = M.ItemCode WHERE M.QryGroup64 = 'Y') + (SELECT COUNT(*) FROM OITM WITH (NOLOCK) WHERE QryGroup64 = 'Y') AS AutoCount");

                oBar = Globals.oApp.StatusBar.CreateProgressBar("Creating Strain Items and Bill of Materials.", autoCount + 3, false);
                oBar.Value = 0;

                var itemNames = NSC_DI.UTIL.AutoStrain.GetAutoItemsInfo(pStrainID, pStrainName);

                oBar.Value++;

                var autoBOMCodes = NSC_DI.SAP.BillOfMaterials.GetTemplateCodes();

                oBar.Value++;

                var itemNamesDict = itemNames.ToDictionary(n => n.AutoItemCode);

                oBar.Value++;

                // Process Auto Items

                foreach (var n in itemNames)
                {
                    // only process new auto items
                    
                    if (!NSC_DI.SAP.Items.HasKey(n.ItemCode)) NSC_DI.SAP.Items.CopyTemplate(n.AutoItemCode, n.ItemCode, n.ItemName, pStrainID);

                    oBar.Value++;
                }

                // Process Auto Boms

                foreach (var autoCode in autoBOMCodes)
                {
                    var code = itemNamesDict[autoCode].ItemCode;

                    if (!NSC_DI.SAP.BillOfMaterials.HasKey(code)) NSC_DI.SAP.BillOfMaterials.CopyTemplate(autoCode, itemNamesDict);

                    oBar.Value++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (oBar != null) oBar.Stop();

                NSC_DI.UTIL.Misc.KillObject(oBar);

                GC.Collect();
            }
        }

		private static string AskForFile()
		{
			try
			{
                var ofd = new System.Windows.Forms.OpenFileDialog();
                ofd.Filter = "Image Files(*.BMP;*.JPG;*.PNG,*.PCX)|*.BMP;*.JPG;*.PNG,*.PCX|All files (*.*)|*.*";

                var y = ofd.ShowDialog();

				return ofd.FileName;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
		}

		private static void FormReSize(Form pForm)
		{
			Item tabGroup = null;
			try
			{
				// Attempt to auto expand the tabs with the form..
				// TODO: Make extract this into a better feature. So tab controls expand with the form.
				// Chase would appreciate this feature.
				tabGroup = pForm.Items.Item("~GRP#1");

				if (tabGroup == null) return;

				// These are just numbers I found that worked best with the form.
				var widthDiff = 50;
				var heightDiff = 75;

				// The 570 and 735 are the values the tab control initial height and width.
				tabGroup.Height = Math.Max(570, pForm.Height - heightDiff);
				tabGroup.Width = Math.Max(735, pForm.Width - widthDiff);
			}
			catch (Exception ex)
			{
				NSC_DI.UTIL.Misc.KillObject(tabGroup);
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(tabGroup);
				GC.Collect();
			}
		}

        private static bool UpdateStrainInfo(Form pForm)
        {
            UserTable sboTable = null;
            SAPbouiCOM.Item oItem = null;
            
            try
            {
                var code = pForm.Items.Item("TXT_SID").Specific.Value;
                sboTable = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tStrains);
                if(!sboTable.GetByKey(code))
                    throw new Exception(NSC_DI.UTIL.Message.Format($"Couldn't Find {code}"));
                for (int i = 0; i < pForm.Items.Count; i++)
                {
                    oItem = pForm.Items.Item(i);
                    if (oItem.UniqueID == "TXT_SID" || oItem.UniqueID == "TXT_SNAME" || oItem.UniqueID == "TXT_STYPE")
                        continue;
                    if (!oItem.Enabled)
                        continue;
                    dynamic dbField;
                    Console.Write(oItem.UniqueID);
                    try
                    {
                        dbField = oItem.Specific.DataBind.Alias;
                        if (string.IsNullOrEmpty(dbField)) continue;
                    }
                    catch
                    {
                        // If an alias is not specified in the form file we cant update that field in the DB so we continue without updating that field
                        continue;
                    }
                    Console.WriteLine("       " + dbField);
                    switch (oItem.Type)
                    {
                        case BoFormItemTypes.it_EDIT:
                        case BoFormItemTypes.it_EXTEDIT:
                        case BoFormItemTypes.it_COMBO_BOX:                           
                            sboTable.UserFields.Fields.Item(dbField).Value = oItem.Specific.Value;
                            break;
                        case BoFormItemTypes.it_CHECK_BOX:
                            sboTable.UserFields.Fields.Item(dbField).Value = (oItem.Specific.Checked) ? "Y" : "N";
                            break;
                        case BoFormItemTypes.it_PICTURE:
                            sboTable.UserFields.Fields.Item(dbField).Value = oItem.Specific.Picture;
                            break;
                        default:
                            Console.WriteLine("");
                            break;
                    }                   
                }
                if (sboTable.Update() != 0)
                    throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(sboTable);
                NSC_DI.UTIL.Misc.KillObject(oItem);
            }
        }
	}
}
