﻿using System;
using B1WizardBase;
using SAPbouiCOM;

namespace NavSol.Forms
{
    public class F_BinSelect : B1Event
    {
        // ---------------------------------- VARS, CLASSES --------------------------------------------------------
        const string cFormID = "NSC_BIN_SELECT";

        private static bool _ExpandResults;
        private static bool _SerialPhase        = false;
        private static bool _AllowMultiWH       = false;
        private static string _callingFormUid   = String.Empty;
        public static BinSelectCallback BinsCB  = null;
        public delegate void BinSelectCallback(string callingFormUid, System.Data.DataTable dtValues);

        // ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeValidate(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if ( (pVal.ItemUID == "grdSrcLocs" || pVal.ItemUID == "grdDstLocs")) BubbleEvent = Validate_Grid(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        //--------------------------------------------------------------------------------------- et_CLICK
        [B1Listener(BoEventTypes.et_CLICK, true, new string[] { cFormID })]
        public virtual bool OnBeforeClick(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "grdSrcLocs") BubbleEvent = Click_SourceGrid(oForm, pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        // ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_FORM_RESIZE
        [B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]
        public virtual void OnAfterFormResize(ItemEvent pVal)
        {
            if (pVal.InnerEvent) return;
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            ReSize_Form(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "1") Press_OK();

            if (pVal.ItemUID == "chkLevel1" || pVal.ItemUID == "chkLevel2" || pVal.ItemUID == "chkLevel3" || pVal.ItemUID == "chkLevel4") Press_LevelFields(oForm, pVal);

            if (pVal.ItemUID == "btnToDst") Press_ToDestGrid(oForm, pVal.Row);
            if (pVal.ItemUID == "btnToSrc") Press_ToSrcGrid(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            CFL_Selected(oForm, (ChooseFromListEvent)pVal);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_COMBO_SELECT
        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
        public virtual void OnAfterComboSelect(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "cboDstWhC") CboSelect_SetWhName(oForm, pVal.ItemUID);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CLICK
        [B1Listener(BoEventTypes.et_CLICK, false, new string[] { cFormID })]
        public virtual void OnAfterClick(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            SetLevelCheckBox(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //---------------------------------------------- SUBS       --------------------------------------------------------

        public static void FormCreate(string pCallingFormUID, string pSQL, double pQty = 0.0, string pSrcWhCode = "", string pDstWhCode = "", bool pExpandResults = false, bool pSerialStage = false)
        {
            Form oForm = null;
            Item oItm = null;

            try
            {
                _SerialPhase = pSerialStage;
                //var formStr = FormXML();
                //formStr = formStr.Replace("(NSC_FORM)", cFormID);
                //oForm = CommonUI.Forms.LoadXML_Modal(formStr);

                oForm = CommonUI.Forms.Load(cFormID, true);

                _callingFormUid = pCallingFormUID;
                _ExpandResults  = pExpandResults;

                oForm.Freeze(true);

                FormSetup(oForm, pDstWhCode);

                oForm.Items.Item("txtQty").Specific.Value = pQty;

                oForm.Items.Item("txtSrcWhC").Specific.Value = pSrcWhCode;
                CboSelect_SetWhName(oForm, "txtSrcWhC");
                oForm.Items.Item("cboDstWhC").Specific.Select(pDstWhCode, BoSearchKey.psk_ByValue);

                FormLoad(oForm, pSQL);

                oForm.Mode = BoFormMode.fm_OK_MODE;

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oForm.Freeze(false);
                oForm.VisibleEx = true;
                NSC_DI.UTIL.Misc.KillObject(oForm);
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        public static void FormCreate(string pCallingFormUID, System.Data.DataTable pSrcDT, double pQty = -1, string pSrcWhCode = "", string pDstWhCode = "", 
                                        bool pExpandResults = false, bool pAllowMultiWH = false)
        {
            SAPbouiCOM.DataTable dtBinIn = null;

            try
            {
                dtCreateIn(dtBinIn);
                foreach(System.Data.DataRow row in pSrcDT.Rows)
                {

                    // if the source WH is empty, get it
                    if (row["SrcWhCode"].ToString() == "")
                    {
                        if (row["BinCode"].ToString() != "") // use the Bin to get the WH
                        {
                            row["SrcWhCode"] = NSC_DI.SAP.Bins.GetWH(row["BinCode"].ToString());
                        }
                        else
                        {
                            //row["SrcWhCode"] = oForm.Items.Item("txtSrcWhC")>Specific.Value;
                        }
                    }

                    dtBinIn.Rows.Add(1);

                    dtBinIn.SetValue("ItemCode",        0, row["ItemCode"].ToString());
                    dtBinIn.SetValue("Name",            0, row["Name"].ToString());
                    dtBinIn.SetValue("SrcWhCode",       0, row["SrcWhCode"].ToString());
                    dtBinIn.SetValue("From WH",         0, row["From WH"].ToString());
                    dtBinIn.SetValue("DstWhCode",       0, row["DstWhCode"].ToString());
                    dtBinIn.SetValue("To WH",           0, row["To WH"].ToString());
                    dtBinIn.SetValue("Serial\\Batch",   0, row["Serial\\Batch"].ToString());
                    dtBinIn.SetValue("BinCode",         0, row["BinCode"].ToString());
                    dtBinIn.SetValue("LevelID1",        0, row["LevelID1"].ToString());
                    dtBinIn.SetValue("LevelID2",        0, row["LevelID2"].ToString());
                    dtBinIn.SetValue("LevelID3",        0, row["LevelID3"].ToString());
                    dtBinIn.SetValue("LevelID4",        0, row["LevelID4"].ToString());

                    dtBinIn.SetValue("XFer Qty", 0, row["XFer Qty"].ToString());
                    dtBinIn.SetValue("Bin Qty", 0, row["Bin Qty"].ToString());
                    dtBinIn.SetValue("Old Bin Qty", 0, row["Old Bin Qty"].ToString());
                    dtBinIn.SetValue("LinkRecNo", 0, row["LinkRecNo"].ToString());



                    //oDT.Columns.Add("ItemCode", BoFieldsType.ft_Text, 50);
                    //oDT.Columns.Add("Name", BoFieldsType.ft_Text, 100);
                    //oDT.Columns.Add("SrcWhCode", BoFieldsType.ft_Text, 8);
                    //oDT.Columns.Add("From WH", BoFieldsType.ft_Text, 100);
                    //oDT.Columns.Add("DstWhCode", BoFieldsType.ft_Text, 8);
                    //oDT.Columns.Add("To WH", BoFieldsType.ft_Text, 100);
                    //oDT.Columns.Add("Serial\\Batch", BoFieldsType.ft_Text, 50);
                    //oDT.Columns.Add("BinCode", BoFieldsType.ft_Text, 254);
                    //oDT.Columns.Add("LevelID1", BoFieldsType.ft_Text, 20);
                    //oDT.Columns.Add("LevelID2", BoFieldsType.ft_Text, 20);
                    //oDT.Columns.Add("LevelID3", BoFieldsType.ft_Text, 20);
                    //oDT.Columns.Add("LevelID4", BoFieldsType.ft_Text, 20);
                    //oDT.Columns.Add("XFer Qty", BoFieldsType.ft_Quantity, 10);
                    //oDT.Columns.Add("Bin Qty", BoFieldsType.ft_Quantity, 10);
                    //oDT.Columns.Add("Old Bin Qty", BoFieldsType.ft_Quantity, 10);
                    //oDT.Columns.Add("LinkRecNo", BoFieldsType.ft_Integer, 5);        // this contains the record number to the other Grid
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(dtBinIn);
                GC.Collect();
            }
        }

        public static void FormCreate(string pCallingFormUID, SAPbouiCOM.DataTable pSrcDT, double pQty = -1, string pSrcWhCode = "", string pDstWhCode = "", 
                                        bool pExpandResults = false, bool pAllowMultiWH = false)
        {
            Form oForm = null;
            Item oItm = null;

            try
            {
                //var formStr = FormXML();
                //formStr = formStr.Replace("(NSC_FORM)", cFormID);
                //oForm = CommonUI.Forms.LoadXML_Modal(formStr);
                oForm = CommonUI.Forms.Load(cFormID, true);

                _callingFormUid = pCallingFormUID;
                _ExpandResults  = pExpandResults;
                _AllowMultiWH   = pAllowMultiWH;

                oForm.Freeze(true);

                FormSetup(oForm, pDstWhCode);
          
                oForm.Items.Item("txtQty").Specific.Value = pQty < 0 ? pSrcDT.Rows.Count : pQty;

                oForm.Items.Item("txtSrcWhC").Specific.Value = pSrcWhCode;
                CboSelect_SetWhName(oForm, "txtSrcWhC");
                //oForm.Items.Item("cboDstWhC").Specific.Select(pDstWhCode, BoSearchKey.psk_ByValue);

                FormLoad(oForm, pSrcDT);

                oForm.Mode = BoFormMode.fm_OK_MODE;

                SetLevelCheckBox(oForm);
                //// if the source WH has no bins, then the "Copy Locations" check boxes are not valid
                //var copyLocsFlag = NSC_DI.SAP.Bins.GetLevelCount(oForm.Items.Item("grdSrcLocs").Specific.DataTable.GetValue("SrcWhCode", 0)) > 0;
                //for (var i = 1; i < 5; i++)
                //{
                //    oForm.Items.Item("chkLevel" + i).Enabled = copyLocsFlag;
                //}
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (oForm != null) oForm.Freeze(false);
                if (oForm != null) oForm.VisibleEx = true;
                NSC_DI.UTIL.Misc.KillObject(oForm);
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private static void FormSetup(Form pForm, string pDstWhCode)
        {
            try
            {
                pForm.DataSources.DataTables.Add("dtSrcLocs");
                pForm.Items.Item("grdSrcLocs").Specific.DataTable = pForm.DataSources.DataTables.Item("dtSrcLocs");

                pForm.DataSources.DataTables.Add("dtDstLocs");
                pForm.Items.Item("grdDstLocs").Specific.DataTable = pForm.DataSources.DataTables.Item("dtDstLocs");

                // set the list of Warehouses
                var sql = "SELECT WhsCode, WhsName FROM OWHS";
                CommonUI.ComboBox.LoadComboBoxSQL(pForm, "cboDstWhC", sql);

                pForm.Items.Item("grdSrcLocs").Specific.SelectionMode = BoMatrixSelect.ms_Auto;
                pForm.Items.Item("grdDstLocs").Specific.SelectionMode = BoMatrixSelect.ms_Auto;
                pForm.Items.Item("grdSrcLocs").Enabled = true;

                //// the level check boxes get enabled when a combobox value changes
                //for (var i = 1; i < 5; i++)
                //{
                //    pForm.Items.Item("chkLevel" + i.ToString()).SetAutoManagedAttribute(BoAutoManagedAttr.ama_Editable, (int)BoAutoFormMode.afm_All, BoModeVisualBehavior.mvb_False);
                //}

                // RHH - hidden field so that focus can be set to it so that the dest WH can be disabled
                Item oItm = pForm.Items.Add("tmp", BoFormItemTypes.it_EDIT);
                oItm.Width = 1;
                oItm.Height = 1;
                oItm.Specific.Active = true;
                CommonUI.Forms.SetAutoManaged(pForm, "cboDstWhC", false); // does not appear to work. so set the form's automanage to false
                //pForm.AutoManaged = false;

                if (NSC_DI.UTIL.Options.Value.GetUseCFL_WareHouse() == "Y" && _AllowMultiWH) NavSol.CommonUI.CFL.CreateWH(pForm, "CFL_WH", "cboDstWhC"); // WH CFL 
                pForm.Items.Item("cboDstWhC").Specific.Select(pDstWhCode, BoSearchKey.psk_ByValue);
                CboSelect_SetWhName(pForm, "cboDstWhC");
                pForm.Items.Item("cboDstWhC").Enabled = _AllowMultiWH;

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.VisibleEx = true;
            }
        }

        private static void FormLoad_OLD(Form pForm, string pSQL)
        {
            Grid oGrd = null;
            CheckBox chkLevel = null;
            SAPbouiCOM.DataTable dtSource = null;

            try
            {
                dtSource = pForm.DataSources.DataTables.Item("dtSrcLocs");    // NavSol.Forms.F_BinSelect.CreateDT();
                dtCreateIn(dtSource);
                dtSource.ExecuteQuery(pSQL);
                if (NSC_DI.SAP.Bins.GetLevelCount(dtSource.GetValue("SrcWhCode", 0)) == 0 && NSC_DI.SAP.Bins.GetLevelCount(dtSource.GetValue("DstWhCode", 0)) == 0)
                {
                    Globals.oApp.MessageBox("The Source and Destination Warehouses have no Bins.");
                    pForm.Close();
                }

                //-------------------------------
                // SOURCE GRID
                oGrd = pForm.Items.Item("grdSrcLocs").Specific;
                oGrd.DataTable.CopyFrom(dtSource);

                // only the transfer column is editable
                for (var i = 0; i < oGrd.Columns.Count; i++)
                {
                    oGrd.Columns.Item(i).Editable = false;
                }
                oGrd.Columns.Item("XFer Qty").Editable = true;

                string[] BinLevNames = NSC_DI.SAP.Bins.GetLevelNames();

                // hide the un-used BIN LEVEL columns
                dtCreateIn(pForm.Items.Item("grdDstLocs").Specific.DataTable);
                //for (var i = 0; i < BinLevNames.Length; i++)
                //{
                //    BinLevNames[i] = (BinLevNames[i] == "") ? "^" + i.ToString() : BinLevNames[i];
                //    oGrd.Columns.Item(BinLevNames[i]).Visible = BinLevNames[i].Substring(0, 1) != "^";
                //    pForm.Items.Item("grdDstLocs").Specific.Columns.Item(BinLevNames[i]).Visible = oGrd.Columns.Item(BinLevNames[i]).Visible;
                //}

                oGrd.Columns.Item("SrcWhCode").Visible       = false;
                oGrd.Columns.Item("DstWhCode").Visible       = false;
                oGrd.Columns.Item("Serial\\Batch").Visible   = false;
                oGrd.Columns.Item("BinCode").Visible         = false;
                oGrd.Columns.Item("LinkRecNo").Visible       = false;
                oGrd.Columns.Item("XFer Qty").RightJustified = true;
                oGrd.Columns.Item("Bin Qty").RightJustified  = true;
                oGrd.Columns.Item("Old Bin Qty").Visible     = false;
                pForm.Items.Item("grdSrcLocs").Enabled       = false;
                pForm.Items.Item("grdSrcLocs").Specific.AutoResizeColumns();

                // get the Source and Destination WH from the 1st row and set the form fields
                var sWH = dtSource.GetValue("SrcWhCode", 0);
                pForm.Items.Item("txtSrcWhC").Specific.Value = sWH;
                pForm.Items.Item("txtSrcWhN").Specific.Value = NSC_DI.SAP.Warehouse.GetName(sWH);
                sWH = dtSource.GetValue("DstWhCode", 0);
                pForm.Items.Item("cboDstWhC").Specific.SelectExclusive(sWH, BoSearchKey.psk_ByValue);
                SetLevelNames(pForm, sWH);  // set the level names based on the destination WH

                //-------------------------------
                // DESTINATION GRID
                oGrd = pForm.Items.Item("grdDstLocs").Specific;
                for (var i = 0; i < oGrd.Columns.Count; i++) { oGrd.Columns.Item(i).Editable = false; }
                oGrd.Columns.Item("SrcWhCode").Visible       = false;
                oGrd.Columns.Item("DstWhCode").Visible       = false;
                oGrd.Columns.Item("XFer Qty").RightJustified = true;
                oGrd.Columns.Item("Bin Qty").Visible         = false;
                oGrd.Columns.Item("Bin Qty").RightJustified  = true;
                oGrd.Columns.Item("Old Bin Qty").Visible     = false;
                oGrd.Columns.Item("LinkRecNo").Visible       = false;
                oGrd.AutoResizeColumns();

                // if the source WH has no bins, then the "Copy Locations" check boxes are not valid
                if (NSC_DI.SAP.Bins.GetLevelCount(pForm.Items.Item("grdSrcLocs").Specific.DataTable.GetValue("SrcWhCode", 0)) == 0)
                {
                    for (var i = 1; i < 5; i++)
                    {
                        pForm.Items.Item("chkLevel" + i).Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(chkLevel);
                GC.Collect();
            }
        }

        private static void FormLoad(Form pForm, string pSQL)
        {
            SAPbouiCOM.DataTable dtSource = null;

            try
            {
                dtSource = pForm.DataSources.DataTables.Item("dtSrcLocs");    
                dtCreateIn(dtSource);
                dtSource.ExecuteQuery(pSQL);
                FormLoad(pForm, dtSource);
             }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void FormLoad(Form pForm, SAPbouiCOM.DataTable dtSource)
        {
            Grid oGrd = null;

            try
            {
                //dtSource = pForm.DataSources.DataTables.Item("dtSrcLocs");    // NavSol.Forms.F_BinSelect.CreateDT();
                var destWH = dtSource.GetValue("DstWhCode", 0);
                if (NSC_DI.SAP.Bins.GetLevelCount(dtSource.GetValue("SrcWhCode", 0)) == 0 && NSC_DI.SAP.Bins.GetLevelCount(destWH) == 0)
                {
                    if (NSC_DI.UTIL.SQL.GetValue<string>($"SELECT BinActivat FROM OWHS WHERE WhsCode = '{destWH}'") == "N")
                    {
                        //System.Data.DataTable newDT = new System.Data.DataTable("Bins");
                        //BinsCB(_callingFormUid, newDT);
                    }
                    else Globals.oApp.MessageBox("The Source and Destination Warehouses have no Bins.");
                    pForm.Close();
                }
       
                //-------------------------------
                // SOURCE GRID
                oGrd = pForm.Items.Item("grdSrcLocs").Specific;
                oGrd.DataTable.CopyFrom(dtSource);

                // only the transfer column is editable
                for (var i = 0; i < oGrd.Columns.Count; i++)
                {
                    oGrd.Columns.Item(i).Editable = false;
                }
                oGrd.Columns.Item("XFer Qty").Editable = true;

                string[] BinLevNames = NSC_DI.SAP.Bins.GetLevelNames();

                // hide the un-used BIN LEVEL columns
                dtCreateIn(pForm.Items.Item("grdDstLocs").Specific.DataTable);
                //for (var i = 0; i < BinLevNames.Length; i++)
                //{
                //    BinLevNames[i] = (BinLevNames[i] == "") ? "^" + i.ToString() : BinLevNames[i];
                //    oGrd.Columns.Item(BinLevNames[i]).Visible = BinLevNames[i].Substring(0, 1) != "^";
                //    pForm.Items.Item("grdDstLocs").Specific.Columns.Item(BinLevNames[i]).Visible = oGrd.Columns.Item(BinLevNames[i]).Visible;
                //}

                oGrd.Columns.Item("SrcWhCode").Visible       = false;
                oGrd.Columns.Item("DstWhCode").Visible       = false;
                oGrd.Columns.Item("Serial\\Batch").Visible   = true;
                oGrd.Columns.Item("BinCode").Visible         = true;
                oGrd.Columns.Item("LevelID1").Visible        = false;
                oGrd.Columns.Item("LevelID2").Visible        = false;
                oGrd.Columns.Item("LevelID3").Visible        = false;
                oGrd.Columns.Item("LevelID4").Visible        = false;
                oGrd.Columns.Item("LinkRecNo").Visible       = false;
                oGrd.Columns.Item("XFer Qty").RightJustified = true;
                oGrd.Columns.Item("Bin Qty").RightJustified  = true;
                oGrd.Columns.Item("ManBySBN").Visible        = false;
                oGrd.Columns.Item("LinkRecNo").Visible       = false;
                oGrd.Columns.Item("Old Bin Qty").Visible     = false;
                pForm.Items.Item("grdSrcLocs").Enabled       = false;
                SetRowNumbers(pForm);
                pForm.Items.Item("grdSrcLocs").Specific.AutoResizeColumns();

                // get the Source and Destination WH from the 1st row and set the form fields
                var sWH = dtSource.GetValue("SrcWhCode", 0);
                pForm.Items.Item("txtSrcWhC").Specific.Value = sWH;
                pForm.Items.Item("txtSrcWhN").Specific.Value = NSC_DI.SAP.Warehouse.GetName(sWH);
                sWH = dtSource.GetValue("DstWhCode", 0);
                pForm.Items.Item("cboDstWhC").Specific.SelectExclusive(sWH, BoSearchKey.psk_ByValue);
                SetLevelNames(pForm, sWH);  // set the level names based on the destination WH

                //-------------------------------
                // DESTINATION GRID
                oGrd = pForm.Items.Item("grdDstLocs").Specific;
                for (var i = 0; i < oGrd.Columns.Count; i++) { oGrd.Columns.Item(i).Editable = false; }
                oGrd.Columns.Item("SrcWhCode").Visible       = false;
                oGrd.Columns.Item("DstWhCode").Visible       = false;
                oGrd.Columns.Item("XFer Qty").RightJustified = true;
                oGrd.Columns.Item("Bin Qty").Visible         = false;
                oGrd.Columns.Item("Bin Qty").RightJustified  = true;
                oGrd.Columns.Item("Old Bin Qty").Visible     = false;
                oGrd.Columns.Item("LevelID1").Visible        = false;
                oGrd.Columns.Item("LevelID2").Visible        = false;
                oGrd.Columns.Item("LevelID3").Visible        = false;
                oGrd.Columns.Item("LevelID4").Visible        = false;
                oGrd.Columns.Item("LinkRecNo").Visible       = false;
                oGrd.Columns.Item("ManBySBN").Visible        = false;
                oGrd.AutoResizeColumns();

                SetLevelCheckBox(pForm);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }

        private void ReSize_Form(Form pForm)
        {
            SAPbouiCOM.Item oItm = null;

            try
            {
                pForm.Freeze(true);
                oItm = pForm.Items.Item("grdDstLocs");
                //oItm.Width  = pForm.Items.Item("grdSrcLocs").Width;
                oItm.Left = pForm.Items.Item("cboDstWhC").Left;
                oItm.Specific.AutoResizeColumns();
                pForm.Items.Item("grdSrcLocs").Specific.AutoResizeColumns();

                var aa = oItm.Left + oItm.Width + 34;
                //pForm.Width = pForm.Width;  // oItm.Left + oItm.Width + 34;
            }
            catch (Exception ex)
            {
                //Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));   // the fields do not exist when the for m is first opened
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oItm);
                GC.Collect();
            }
        }

        private void CFL_Selected(Form pForm, ChooseFromListEvent pVal)
        {
            //if (pVal.InnerEvent) return;
            if (pVal.SelectedObjects == null) return;

            try
            {
                switch (pVal.ChooseFromListUID)
                {
                    case "CFL_WH":
                        CommonUI.CFL.SetVal(pForm, pVal, "WhsName");
                        break;

                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void SetLevelCheckBox(Form pForm)
        {
            Grid oGrd = null;
            CheckBox chkLevel = null;

            try
            {
                // if the source WH has no bins, then the "Copy Locations" check boxes are not valid
                if (NSC_DI.SAP.Bins.GetLevelCount(pForm.Items.Item("grdSrcLocs").Specific.DataTable.GetValue("SrcWhCode", 0)) == 0)
                {
                    pForm.Freeze(false);
                    for (var i = 1; i < 5; i++)
                    {
                        pForm.Items.Item("chkLevel" + i).Enabled = false;
                    }
                    //pForm.Freeze(true);
                    return;
                }

                // if there is a source bin that does not exist in the destination WH, then the "Copy Locations" check boxes are not valid
                oGrd = pForm.Items.Item("grdSrcLocs").Specific;
                for (var j = 0; j < oGrd.Rows.Count; j++)
                {
                    string sWH  = oGrd.DataTable.GetValue("SrcWhCode", j);
                    string dWH  = pForm.Items.Item("cboDstWhC").Specific.Value();
                    string dBin = "";
                    string sBin = oGrd.DataTable.GetValue("BinCode", j);
                    var    indx = sWH.Length;
                    if (sBin.Trim().Length > indx) dBin = dWH + sBin.Substring(indx);

                    if (NSC_DI.SAP.Bins.GetAbsFromCode(dBin) == 0)
                    {
                        pForm.Freeze(false);
                        for (var i = 1; i < 5; i++)
                        {
                            pForm.Items.Item("chkLevel" + i).Enabled = false;
                        }
                        //pForm.Freeze(true);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                NSC_DI.UTIL.Misc.KillObject(chkLevel);
                GC.Collect();
            }
        }

        //public Form FormCreate(SAPbouiCOM.DataTable pBinSource, SAPbouiCOM.DataTable pBinDest)
        //{
        //    Form oForm = null;
        //    Item oItm = null;
        //    try
        //    {
        //        var formStr = FormXML();
        //        formStr = formStr.Replace("(NSC_FORM)", cFormID);
        //        oForm = CommonUI.Forms.LoadXML(formStr, pSingleInstance);

        //        FormSetup(oForm);

        //        _BinSourceDT = pBinSource;

        //        return oForm;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(NSC_DI.UTIL.Message.Format(ex));
        //    }
        //    finally
        //    {
        //        //NSC_DI.UTIL.Misc.KillObject(oForm);
        //        NSC_DI.UTIL.Misc.KillObject(oItm);
        //        GC.Collect();
        //    }
        //}

        private static void Press_LevelFields(Form pForm, ItemEvent pVal)
        {
            CheckBox chkLevel = null;
            try
            {
                var levelNum = pVal.ItemUID.Substring(pVal.ItemUID.Length - 1);

                chkLevel = pForm.Items.Item(pVal.ItemUID).Specific;
                if (chkLevel.Checked)
                {
                    // get the starting Level from the source table
                    var bin = pForm.Items.Item("grdSrcLocs").Specific.DataTable.GetValue("BinCode", 0);
                    if(string.IsNullOrEmpty(bin?.Trim()))// Whitespace-Change (NSC_DI.UTIL.Strings.Empty(bin))
                    {
                        try
                        {
                            pForm.Items.Item("cboLevel" + levelNum).Enabled = true;
                            pForm.Items.Item("cboLevel" + levelNum).Specific.Select(1, SAPbouiCOM.BoSearchKey.psk_Index);
                        }
                        catch { }
                        return;
                    }
                    //var levNum  = bin.Substring(bin.Length - 1);
                    var levCode = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT SL{levelNum}Code FROM OBIN WHERE BinCode = '{bin}'", "");
                    pForm.Items.Item("cboLevel" + levelNum).Specific.Select(levCode, SAPbouiCOM.BoSearchKey.psk_ByValue);

                    pForm.Items.Item("cboLevel" + levelNum).Specific.Active = false;
                    pForm.Items.Item("cboLevel" + levelNum).Enabled = false;
                }
                else
                {
                    pForm.Items.Item("cboLevel" + levelNum).Enabled = true;
                    pForm.Items.Item("cboLevel" + levelNum).Specific.Select(0, SAPbouiCOM.BoSearchKey.psk_Index);
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(chkLevel);
            }
        }

        private static void CboSelect_SetWhName(Form pForm, string pWhFieldName)
        {
            try
            {
               var fldWH = pForm.Items.Item(pWhFieldName).Specific;
                if(pWhFieldName == "txtSrcWhN")
                {
                    pForm.Items.Item("txtSrcWhN").Specific.Value = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT WhsName FROM OWHS WHERE WhsCode = '{fldWH.Value}'", "");
                }

                if (pWhFieldName == "cboDstWhC")
                {
                    pForm.Items.Item("txtDstWhN").Specific.Value = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT WhsName FROM OWHS WHERE WhsCode = '{fldWH.Value}'", "");
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private bool Click_SourceGrid(Form pForm, ItemEvent pVal)
        {
            SAPbouiCOM.Grid oGrd = null;
            try
            {
                if (pVal.Row < 0) return true;
                if (SetCount(pForm, pVal.Row)) return true;
                oGrd = pForm.Items.Item("grdSrcLocs").Specific;
                oGrd.Rows.SelectedRows.Remove(pVal.Row);
                return false;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return false;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }

        private void Press_ToDestGrid(Form pForm, int pRow)
        {
            // NEED TO CHECK FOR MULTIPLE ENTRIES IN THE DESTINATION GRID.

            SAPbobsCOM.Recordset oBinRS  = null;
            SAPbouiCOM.Grid      oSrcGrd = null;
            SAPbouiCOM.DataTable oSrcDT  = null;
            SAPbouiCOM.DataTable oDstDT  = null;

            try
            {
                pForm.Freeze(true);

                // the levels must be specified
                for (var i = 1; i < 5; i++)
                {
                    if (pForm.Items.Item("cboLevel" + i.ToString()).Visible == false)                   continue;
                    if (pForm.Items.Item("cboLevel" + i.ToString()).Specific.Value.Trim().Length > 0)   continue;
                    if (pForm.Items.Item("chkLevel" + i.ToString()).Specific.Checked)                   continue;
                    Globals.oApp.SetStatusBarMessage("All Bin Levels must be specified.", BoMessageTime.bmt_Medium);
                    return;
                }

                // check the quantity total
                if (SetCount(pForm) == false)
                {
                    Globals.oApp.StatusBar.SetText("You have exceeded the specified quantity.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                    return;
                }

                oBinRS  = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oSrcGrd = pForm.Items.Item("grdSrcLocs").Specific;
                oSrcDT  = oSrcGrd.DataTable;
                oDstDT  = pForm.Items.Item("grdDstLocs").Specific.DataTable;

                string[] BinLevNames = NSC_DI.SAP.Bins.GetLevelNames();

                // get the warehouse
                var WH = pForm.Items.Item("cboDstWhC").Specific.Value;

                // get the level checkbox values
                var chk1 = pForm.Items.Item("chkLevel1").Specific.Checked;
                var chk2 = pForm.Items.Item("chkLevel2").Specific.Checked;
                var chk3 = pForm.Items.Item("chkLevel3").Specific.Checked;
                var chk4 = pForm.Items.Item("chkLevel4").Specific.Checked;

                // the "last" level value
                string lastLv1 = null;
                string lastLv2 = null;
                string lastLv3 = null;
                string lastLv4 = null;

                for (var i = 0; i < oSrcGrd.Rows.SelectedRows.Count; i++)
                {
                    var r = oSrcGrd.GetDataTableRowIndex(oSrcGrd.Rows.SelectedRows.Item(i, BoOrderType.ot_RowOrder));

                    // get the level values
                    var Lv1 = chk1 ? oSrcDT.GetValue("LevelID1", r) : pForm.Items.Item("cboLevel1").Specific.Value;
                    var Lv2 = chk2 ? oSrcDT.GetValue("LevelID2", r) : pForm.Items.Item("cboLevel2").Specific.Value;
                    var Lv3 = chk3 ? oSrcDT.GetValue("LevelID3", r) : pForm.Items.Item("cboLevel3").Specific.Value;
                    var Lv4 = chk4 ? oSrcDT.GetValue("LevelID4", r) : pForm.Items.Item("cboLevel4").Specific.Value;

                    // check if a system bin
                    if (NSC_DI.SAP.Bins.IsSystem(oSrcDT.GetValue("BinCode", r)))
                    {
                        oBinRS = NSC_DI.SAP.Bins.GetBinQtys(WH, Lv1, Lv2, Lv3, Lv4, "Y");
                    }
                    else
                    {
                        // if a level has changed then get the list of Bins
                        if (Lv1 != lastLv1 || Lv2 != lastLv2 || Lv3 != lastLv3 || Lv4 != lastLv4)
                        {
                            oBinRS = NSC_DI.SAP.Bins.GetBinQtys(WH, Lv1, Lv2, Lv3, Lv4);
                            lastLv1 = Lv1;
                            lastLv2 = Lv2;
                            lastLv3 = Lv3;
                            lastLv4 = Lv4;
                        }
                    }

                    var XFqtyS  = oSrcGrd.DataTable.GetValue("XFer Qty", r);
                    var BinQtyS = oSrcGrd.DataTable.GetValue("Bin Qty", r);
                    //var maxQty  = NSC_DI.SAP.Bins.GetFreeQty(oSrcGrd.DataTable.GetValue("ItemCode", r), WH, Lv1, Lv2, Lv3, Lv4);
                    var RecNo   = oSrcGrd.DataTable.GetValue("LinkRecNo", r);

                    // already copied, if the quantity has changed delete all recs in the dest table for this entry
                    //This was causing the process to fail so I commented it out 5/3/18
                    //if (RecNo > -1)
                    //{
                    //    var XFqtyD          = oDstDT.GetValue("XFer Qty", RecNo);
                    //    var qtyOrigBinQty   = oDstDT.GetValue("Old Bin Qty", RecNo);
                    //    if (XFqtyS + BinQtyS == qtyOrigBinQty) continue;
                    //    DeleteRecsDT(pForm, oDstDT, "LinkRecNo", RecNo);
                    //    oSrcDT.SetValue("Bin Qty", r, qtyOrigBinQty - XFqtyS);
                    //}

                    // create entries in the destination grid
                    //pForm.Freeze(true);
                    CopyRowDT(pForm, oSrcDT, oDstDT, r, oBinRS);
                    var NewQty = BinQtyS - XFqtyS;
                    //update the source bin qty 
                    oSrcDT.SetValue("XFer Qty", r, 0);
                    string strHolder = oSrcDT.Columns.Item(13).Name;
                    oSrcDT.SetValue(oSrcDT.Columns.Item(13).Name, r,NewQty);
                    //pForm.Freeze(false);

                    var sTmp = pForm.Items.Item("txtTotal").Specific.Value;
                    sTmp = (sTmp == "") ? "0.0" : sTmp;
                    double total = Convert.ToDouble(sTmp) + XFqtyS;
                    pForm.Items.Item("txtTotal").Specific.Value = total;
                }

                oSrcGrd.Rows.SelectedRows.Clear();

                pForm.Items.Item("1").Enabled = (pForm.Items.Item("txtTotal").Specific.Value == pForm.Items.Item("txtQty").Specific.Value);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oBinRS);
                NSC_DI.UTIL.Misc.KillObject(oSrcGrd);
                NSC_DI.UTIL.Misc.KillObject(oSrcDT);
                NSC_DI.UTIL.Misc.KillObject(oDstDT);
                GC.Collect();
            }
        }

        private void Press_ToSrcGrid(Form pForm)
        {
            SAPbouiCOM.Grid oGrd        = null;
            SAPbouiCOM.DataTable oSrcDT = null;
            SAPbouiCOM.DataTable oDstDT = null;

            try
            {
                pForm.Freeze(true);

                oGrd = pForm.Items.Item("grdDstLocs").Specific;
                oDstDT  = oGrd.DataTable;
                oSrcDT  = pForm.Items.Item("grdSrcLocs").Specific.DataTable;
                int[] recs = new int[oGrd.Rows.SelectedRows.Count];
                for (var i = oGrd.Rows.SelectedRows.Count - 1; i >= 0 ; i--)
                {
                    var j = oGrd.Rows.SelectedRows.Item(i, BoOrderType.ot_RowOrder);

                    var r = oGrd.GetDataTableRowIndex(j);
                    recs[i] = r;
                    var sRow = oDstDT.GetValue("LinkRecNo", r);
                    var qtyD = oDstDT.GetValue("XFer Qty", r);
                    var qtyS = oSrcDT.GetValue("Bin Qty", sRow) + qtyD;
                    oSrcDT.SetValue("Bin Qty", sRow, qtyS);
                    oSrcDT.SetValue("XFer Qty", sRow, oSrcDT.GetValue("XFer Qty", sRow) + qtyD);

                    var total = Convert.ToDouble(pForm.Items.Item("txtTotal").Specific.Value) - qtyD;
                    pForm.Items.Item("txtTotal").Specific.Value = total;

                    //oDstDT.Rows.Remove(r);
                }

                for (var i = recs.Length - 1; i >= 0; i--) { oDstDT.Rows.Remove(recs[i]); }
                oGrd.Rows.SelectedRows.Clear();

                pForm.Items.Item("1").Enabled = (pForm.Items.Item("txtTotal").Specific.Value == pForm.Items.Item("txtCount").Specific.Value);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                NSC_DI.UTIL.Misc.KillObject(oSrcDT);
                NSC_DI.UTIL.Misc.KillObject(oDstDT);
                GC.Collect();
            }
        }

        private static bool Validate_Grid(Form pForm, ItemEvent pVal)
        {
            // returns false if an error

            SAPbouiCOM.DataTable oDT = null;

            try
            {
                var row = pForm.Items.Item(pVal.ItemUID).Specific.GetDataTableRowIndex(pVal.Row);
                oDT = pForm.Items.Item(pVal.ItemUID).Specific.DataTable;

                // QUANTITY

                if (pVal.ColUID == "XFer Qty") 
                {
                    if (oDT.GetValue("XFer Qty", row) == 0) return true;
                    if (oDT.GetValue("XFer Qty", row) <= oDT.GetValue("Bin Qty", row) && oDT.GetValue("XFer Qty", row) > 0 && oDT.GetValue("Bin Qty", row) > 0)
                    {
                        return SetCount(pForm);
                    }

                    Globals.oApp.StatusBar.SetText("Invalid Quantity.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return false;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }

        private void Press_OK()
        {
            SAPbouiCOM.Form      oForm  = null;
            SAPbouiCOM.DataTable oDT    = null;
            SAPbouiCOM.DataTable oSrcDT = null;

            try
            {
                //pForm.Close();
                oForm = B1Connections.theAppl.Forms.GetForm(cFormID, 1);

                if (BinsCB == null) return;
                oDT = oForm.Items.Item("grdDstLocs").Specific.DataTable;
                if (oDT.Rows.Count <= 0) return;

                oSrcDT = oForm.Items.Item("grdSrcLocs").Specific.DataTable;
                var fromBin = "";
                // convert to a System dataTable    
                var newDT = dtCreateOut("Bins");

                if (_ExpandResults)
                {
                    var row = 0;
                    for (var i = 0; i < oDT.Rows.Count; i++)
                    {
                        fromBin = oSrcDT.GetValue("BinCode", oDT.GetValue("LinkRecNo", i));
                        var binAbs = NSC_DI.SAP.Bins.GetAbsFromCode(oDT.GetValue("BinCode", i)).ToString();
                        if (oDT.GetValue("BinCode", i) == "S" || _SerialPhase)   // only expand serial numbers
                        {
                            for (var j = 0; j < oDT.GetValue("XFer Qty", i); j++)
                            {
                                newDT.Rows.Add(row, oDT.GetValue("BinCode", i), binAbs, 1,
                                    oDT.GetValue("ItemCode", i), oDT.GetValue("Serial\\Batch", i), oDT.GetValue("DstWhCode", i),
                                    oDT.GetValue("SrcWhCode", i), oDT.GetValue("ManBySBN", i));
                                row++;
                            }
                        }
                        else
                        {
                            newDT.Rows.Add(i, oDT.GetValue("BinCode", i), binAbs, oDT.GetValue("XFer Qty", i),
                            oDT.GetValue("ItemCode", i), oDT.GetValue("Serial\\Batch", i), oDT.GetValue("DstWhCode", i),
                            oDT.GetValue("SrcWhCode", i), fromBin, oDT.GetValue("ManBySBN", i));
                            row++;
                        }
                    }
                }
                else
                {
                    for (var i = 0; i < oDT.Rows.Count; i++)
                    {
                        fromBin = oSrcDT.GetValue("BinCode", oDT.GetValue("LinkRecNo", i));
                        var binAbs = NSC_DI.SAP.Bins.GetAbsFromCode(oDT.GetValue("BinCode", i)).ToString();
                        newDT.Rows.Add(i, oDT.GetValue("BinCode", i), binAbs, oDT.GetValue("XFer Qty", i),
                            oDT.GetValue("ItemCode", i), oDT.GetValue("Serial\\Batch", i), oDT.GetValue("DstWhCode", i),
                            oDT.GetValue("SrcWhCode", i), fromBin, oDT.GetValue("ManBySBN", i));
                    }
                }

                BinsCB(_callingFormUid, newDT);
                oForm.Close();
            }
            catch (Exception ex)
            {
                //Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDT);
                NSC_DI.UTIL.Misc.KillObject(oSrcDT);
                GC.Collect();
            }
        }

        public static void dtCreateIn(SAPbouiCOM.DataTable oDT)
        {
            // create the SAP UI DataTable - IN

            try
            {
                string[] BinLevNames = NSC_DI.SAP.Bins.GetLevelNames();
                for (var i = 0; i < BinLevNames.Length; i++)
                {
                    // the colomns have to have a unique name
                    if (BinLevNames[i] == "") BinLevNames[i] = "^" + i.ToString();
                }

                oDT.Clear();    // deletes columns and rows

                oDT.Columns.Add("ItemCode",      BoFieldsType.ft_Text, 50);
                oDT.Columns.Add("Name",          BoFieldsType.ft_Text, 100);
                oDT.Columns.Add("SrcWhCode",     BoFieldsType.ft_Text, 8);
                oDT.Columns.Add("From WH",       BoFieldsType.ft_Text, 100);
                oDT.Columns.Add("DstWhCode",     BoFieldsType.ft_Text, 8);
                oDT.Columns.Add("To WH",         BoFieldsType.ft_Text, 100);
                oDT.Columns.Add("Serial\\Batch", BoFieldsType.ft_Text, 50);
                oDT.Columns.Add("BinCode",       BoFieldsType.ft_Text, 254);
                oDT.Columns.Add("LevelID1",      BoFieldsType.ft_Text, 20);
                oDT.Columns.Add("LevelID2",      BoFieldsType.ft_Text, 20);
                oDT.Columns.Add("LevelID3",      BoFieldsType.ft_Text, 20);
                oDT.Columns.Add("LevelID4",      BoFieldsType.ft_Text, 20);
                oDT.Columns.Add("XFer Qty",      BoFieldsType.ft_Quantity, 10);
                oDT.Columns.Add("Bin Qty",       BoFieldsType.ft_Quantity, 10);
                oDT.Columns.Add("Old Bin Qty",   BoFieldsType.ft_Quantity, 10);
                oDT.Columns.Add("LinkRecNo",     BoFieldsType.ft_Integer, 5);       // this contains the record number to the other Grid
                oDT.Columns.Add("ManBySBN",      BoFieldsType.ft_Text, 1);          // manage by Serial Number or Batch or None
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
            }
        }

        public static System.Data.DataTable dtCreateOut(string pName)
        {
            // create system DataTable - OUT 

            try
            {
                System.Data.DataTable newDT = new System.Data.DataTable(pName);
                newDT.Columns.Add("Order",          typeof(int));
                newDT.Columns.Add("BinCode",        typeof(string));
                newDT.Columns.Add("BinAbs",         typeof(string));
                newDT.Columns.Add("Quantity",       typeof(double));
                newDT.Columns.Add("ItemCode",       typeof(string));
                newDT.Columns.Add("Serial\\Batch",  typeof(string));
                newDT.Columns.Add("DestWH",         typeof(string));
                newDT.Columns.Add("FromWH",         typeof(string));
                newDT.Columns.Add("FromBin",        typeof(string));
                newDT.Columns.Add("ManBySBN",       typeof(string));

                return newDT;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
            }
        }

        private static bool SetCount(Form pForm, int pRow = -1)
        {
            // returns false if there is an issue with the quantities

            // if a row is specified and the row is not selected, then also include in the calculations

            SAPbouiCOM.Grid oGrd = null;

            try
            {
                oGrd = pForm.Items.Item("grdSrcLocs").Specific;

                // get the quantity sum of selected rows
                var total = 0D;
                for (var i = 0; i < oGrd.Rows.SelectedRows.Count; i++)
                {
                    var r = oGrd.GetDataTableRowIndex(oGrd.Rows.SelectedRows.Item(i, BoOrderType.ot_RowOrder));
                    total += oGrd.DataTable.GetValue("XFer Qty", r);
                }

                // a specific row was specified, include it in the validation
                if (pRow >= 0 && oGrd.Rows.IsSelected(pRow) == false)
                {
                    var r = oGrd.GetDataTableRowIndex(pRow);
                    total += oGrd.DataTable.GetValue("XFer Qty", r);
                }
                pForm.Items.Item("txtCount").Specific.Value = total;
                var qty = Convert.ToDouble(pForm.Items.Item("txtQty").Specific.Value.ToString());

                if (total <= qty) return true;
                Globals.oApp.StatusBar.SetText("You have exceeded the specified quantity.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Warning);
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }

        private static void HideShowLevelFields(Form pForm, string pWHs, string pWHd)
        {
            try
            {
                // NOT COMPLETE
                // only show the level fields that exist in both warehouses
                var fWH = NSC_DI.SAP.Bins.GetLevelNames(pWHs);
                var tWH = NSC_DI.SAP.Bins.GetLevelNames(pWHd);
                //if(fWH.SequenceEqual(tWH))
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }

        private static void SetLevelNames(Form pForm, string pWHs)
        {
            CheckBox chkLevel = null;
            try
            {
                // TODO -> only show the level fields that exist in both warehouses, unless only one WH is specified
                string[] BinLevNames = NSC_DI.SAP.Bins.GetLevelNames(pWHs);
                for (var i = 1; i <= BinLevNames.Length; i++)
                {
                    pForm.DataSources.UserDataSources.Add("dsLevel" + i.ToString(), BoDataType.dt_SHORT_TEXT, 1);
                    chkLevel = pForm.Items.Item("chkLevel" + i.ToString()).Specific.DataBind.SetBound(true, "", "dsLevel" + i.ToString());
                    //chkLevel.DataBind.SetBound(true, "chkLevel" + i.ToString());
                    pForm.Items.Item("staLevel" + i.ToString()).Specific.Caption = BinLevNames[i - 1];
                    pForm.Items.Item("chkLevel" + i.ToString()).Visible = (BinLevNames[i - 1] != "");
                    pForm.Items.Item("staLevel" + i.ToString()).Visible = (BinLevNames[i - 1] != "");
                    pForm.Items.Item("cboLevel" + i.ToString()).Visible = (BinLevNames[i - 1] != "");
                    if (BinLevNames[i - 1] == "") continue;
                    // get the list of each levels
                    //var sql = $"SELECT SLCode, Descr FROM OBSL INNER JOIN OBFC ON OBSL.FldAbs = OBFC.FldNum WHERE OBFC.FldType = 'S' AND OBFC.Activated = 'Y' AND OBSL.FldAbs = {i}";
                    //var sql = $"SELECT DISTINCT OBIN.SL{i}Code,'' FROM OBIN  WHERE WhsCode = '{pWHs}' AND SysBin = 'N' AND EXISTS (SELECT AbsEntry FROM OBFC WHERE OBFC.FldType = 'S' AND OBFC.Activated = 'Y' AND OBFC.FldNum = {i}) ORDER BY OBIN.SL{i}Code";
                    var sql = $"SELECT DISTINCT OBIN.SL{i}Code,'' FROM OBIN  WHERE WhsCode = '{pWHs}' AND EXISTS (SELECT AbsEntry FROM OBFC WHERE OBFC.FldType = 'S' AND OBFC.Activated = 'Y' AND OBFC.FldNum = {i}) ORDER BY OBIN.SL{i}Code";
                    CommonUI.ComboBox.LoadComboBoxSQL(pForm, "cboLevel" + i.ToString(), sql);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(chkLevel);
                GC.Collect();
            }
        }

        private void CopyRowDT(Form pForm, SAPbouiCOM.DataTable pFromDT, SAPbouiCOM.DataTable pToDT, int pRow, 
                               SAPbobsCOM.Recordset pBinList)
        {
            // copy row

            try
            {
                var xFqty = pFromDT.GetValue("XFer Qty", pRow);
                // if the source WH is empty, get it
                var srcWH = pFromDT.GetValue("SrcWhCode", pRow);
                if (srcWH == "")
                {
                    // use the Bin to get the WH
                    if (pFromDT.GetValue("BinCode", pRow) != "")
                        srcWH = NSC_DI.SAP.Bins.GetWH(pFromDT.GetValue("BinCode", pRow));
                    else
                        srcWH = pForm.Items.Item("txtSrcWhC").Specific.Value;
                }

                pBinList.MoveFirst();

                while (xFqty > 0)
                {
                    var MaxQty = pBinList.Fields.Item("MaxLevel").Value;
                    var freeQty = pBinList.Fields.Item("FreeQty").Value;
                    if (freeQty <= 0 && MaxQty > 0)    // selected a bin that is already full - only if there is a max qty
                    {
                        pBinList.MoveNext();
                        continue; 
                    }
                    var binCode = pBinList.Fields.Item("BinCode").Value;
                    var newQty  = Math.Min(xFqty, freeQty);
                    if (MaxQty == 0) newQty = xFqty;        // there is no max qty
                    if (newQty < 1) return;
                    pToDT.Rows.Add(1);
                    for(var i = 0; i < pToDT.Columns.Count; i++)
                    {
                        pToDT.SetValue(i, pToDT.Rows.Count-1, pFromDT.GetValue(i, pRow));
                    }
                    pToDT.SetValue("SrcWhCode",     pToDT.Rows.Count - 1, srcWH);
                    pToDT.SetValue("DstWhCode",     pToDT.Rows.Count - 1, pForm.Items.Item("cboDstWhC").Specific.Value);
                    pToDT.SetValue("To WH",         pToDT.Rows.Count - 1, pForm.Items.Item("txtDstWhN").Specific.Value);
                    pToDT.SetValue("XFer Qty",      pToDT.Rows.Count - 1, newQty);
                    pToDT.SetValue("BinCode",       pToDT.Rows.Count - 1, binCode);
                    pToDT.SetValue("LinkRecNo",     pToDT.Rows.Count - 1, pRow);
                    pFromDT.SetValue("LinkRecNo",   pRow, pToDT.Rows.Count - 1);

                    xFqty -= newQty;
                    if (pBinList.RecordCount > 0 ) pBinList.MoveNext();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }

        private static void DeleteRecsDT(Form pForm, SAPbouiCOM.DataTable oDT, string pCol, dynamic pValue)
        {
            try
            {
                for(var i = oDT.Rows.Count-1; i >= 0; i--)
                {
                    if (oDT.GetValue(pCol, i) == pValue) oDT.Rows.Remove(i);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private static void SetRowNumbers(Form pForm)
        {
            SAPbouiCOM.DataTable oDT = null;
            try
            {
                //-------------------------------
                // SOURCE GRID
                oDT = pForm.Items.Item("grdSrcLocs").Specific.Datatable;
                for (var i = 0; i < oDT.Rows.Count; i++)
                {
                    //oDT.SetValue(-1, i, i);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oDT);
                GC.Collect();
            }
        }
    }
}