﻿using System;
using System.Collections.Generic;
using B1WizardBase;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol.Forms
{
	class F_Quarantine_Batch : B1Event
	{
		#region ---------------------------------- VARS, CLASSES --------------------------------------------------------
		private const string cFormID = "NSC_QUARBATCH";
		
		//DO NOT CHANGE.. Or stuff may break.
		public enum MatrixColumns { BatchNumber, StateID, Quantity, ItemName, WarehouseCode, WarehouseName, CreateDate, ItemCode, Timer }

		#endregion VARS

		#region ---------------------------------- BEFORE EVENT --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
		public virtual bool OnBeforeItemPressed(ItemEvent pVal)
		{
			var BubbleEvent = true;
			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "TAB_1":
					// Load data about plants into the matrix
					Load_Matrx_Main(oForm, NSC_DI.Globals.QuarantineStates.SALE);
					break;

				case "TAB_2":
					// Load data about plants into the matrix
					Load_Matrx_Main(oForm, NSC_DI.Globals.QuarantineStates.DEST);
					break;

				case "TAB_3":
					// Load data about plants into the matrix
					Load_Matrx_Main(oForm, NSC_DI.Globals.QuarantineStates.DEST, true);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();

			return BubbleEvent;
		}
		#endregion BEFORE EVENT

		#region ---------------------------------- AFTER EVENT  --------------------------------------------------------
		//--------------------------------------------------------------------------------------- et_ITEM_PRESSED
		[B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterItemPressed(ItemEvent pVal)
		{
			if (pVal.ActionSuccess == false) return;

			var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			switch (pVal.ItemUID)
			{
				case "BTN_LOOKUP":
					BTN_LOOKUP(oForm);
					break;

				case "BTN_DEST":
					BTN_DEST_Click(oForm);
					break;
			}

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

		//----------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
		[B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
		public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
		{
			if (pVal.Row < 1) return;
			if (pVal.ActionSuccess == false) return;
			Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

			if (pVal.ItemUID == "MTX_MAIN") MTX_MAIN_Matrix_Link_Pressed(oForm, pVal);

			NSC_DI.UTIL.Misc.KillObject(oForm);
			GC.Collect();
		}

        [B1Listener(BoEventTypes.et_FORM_RESIZE, false, new string[] { cFormID })]

        public virtual void OnAfterFormResize(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;
            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            FormReSize(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

        }
        #endregion AFTER EVENT

        //---------------------------------------------- SUBS       --------------------------------------------------------

        private static void FormSetup(Form pForm)
		{
			try
			{
				pForm.Freeze(true);

				CommonUI.Forms.SetFieldvalue.Icon(pForm, "IMG_Main", @"quarantine-icon.bmp");
				pForm.Items.Item("TAB_1").Specific.Select();
				Load_TabCount(pForm);

				pForm.VisibleEx = true;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
			}
		}

		public static Form FormCreate(bool pSingleInstance = false, string pCallingFormUID = null)
		{
			Item oItm = null;
			try
			{
				var oForm = CommonUI.Forms.Load(cFormID, pSingleInstance);

                // create hidden field to save the calling form
                if (pCallingFormUID != null)
				{
					oItm = oForm.Items.Add("CallingForm", BoFormItemTypes.it_EDIT);
					oItm.Top = -20;
					oItm.Specific.Value = pCallingFormUID;
				}

				FormSetup(oForm);
				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				GC.Collect();
			}
		}

		private static void Load_Matrx_Main(Form pForm, NSC_DI.Globals.QuarantineStates QuarantineState, bool loadTimerColumn = false)
		{
			Matrix MTX_MAIN = null;

			try
			{
				// Freeze the Form UI
				pForm.Freeze(true);
                
                var WarehouseType = "";
                var TXT_RMRK = pForm.Items.Item("TXT_RMRK");
                var LBL_RMRK = pForm.Items.Item("LBL_RMRK");
                var TXT_LOOK = pForm.Items.Item("TXT_LOOKUP");
                var BTN_DEST = pForm.Items.Item("BTN_DEST");

                if (TXT_RMRK.Visible)
                {
                    TXT_RMRK.Specific.Value = "";
                    TXT_LOOK.Specific.Active = true;                    
                }            
                BTN_DEST.Visible = false;
                LBL_RMRK.Visible = false;
                TXT_RMRK.Visible = false;

				switch (QuarantineState)
				{
					case NSC_DI.Globals.QuarantineStates.DEST:
						WarehouseType = "QND";
						if (!loadTimerColumn)
						{
							pForm.Items.Item("BTN_DEST").Visible = true;
                            pForm.Items.Item("LBL_RMRK").Visible = true;
                            pForm.Items.Item("TXT_RMRK").Visible = true;
                        }
						break;

					case NSC_DI.Globals.QuarantineStates.SALE:
						WarehouseType = "QNS";
						break;
				}

				var matrixColumnCreation = new List<CommonUI.Matrix.MatrixColumn>()
				{
					new CommonUI.Matrix.MatrixColumn()
					{
						ColumnName = MatrixColumns.BatchNumber.ToString("F"),
						Caption = "BatchNumber",
						ColumnWidth = 80,
						ItemType = BoFormItemTypes.it_LINKED_BUTTON
					}
					,
					new CommonUI.Matrix.MatrixColumn()
					{
						ColumnName = MatrixColumns.StateID.ToString("F"),
						Caption = "State ID",
						ColumnWidth = 80,
						ItemType = BoFormItemTypes.it_EDIT
					}
					,
					new CommonUI.Matrix.MatrixColumn()
					{
						ColumnName = MatrixColumns.Quantity.ToString("F"),
						Caption = "Quantity",
						ColumnWidth = 80,
						ItemType = BoFormItemTypes.it_EDIT
					}
					,
					new CommonUI.Matrix.MatrixColumn()
					{
						ColumnName = MatrixColumns.ItemName.ToString("F"),
						Caption = "Name",
						ColumnWidth = 80,
						ItemType = BoFormItemTypes.it_EDIT
					}
					,
					new CommonUI.Matrix.MatrixColumn()
					{
						ColumnName = MatrixColumns.WarehouseCode.ToString("F"),
						Caption = "Warehouse ID",
						ColumnWidth = 80,
						ItemType = BoFormItemTypes.it_EDIT
					}
					,
					new CommonUI.Matrix.MatrixColumn()
					{
						ColumnName = MatrixColumns.WarehouseName.ToString("F"),
						Caption = "Warehouse",
						ColumnWidth = 80,
						ItemType = BoFormItemTypes.it_EDIT
					}
					,
					new CommonUI.Matrix.MatrixColumn()
					{
						ColumnName = MatrixColumns.CreateDate.ToString("F"),
						Caption = "Date Created",
						ColumnWidth = 80,
						ItemType = BoFormItemTypes.it_EDIT
					}
					,
					new CommonUI.Matrix.MatrixColumn()
					{
						ColumnName = MatrixColumns.ItemCode.ToString("F"),
						Caption = "Item Code",
						ColumnWidth = 80,
						ItemType = BoFormItemTypes.it_EDIT
					}
				};

				var additionalColumnToQuery = "";
				var additionalClauseToQuery = "";

				if (QuarantineState == NSC_DI.Globals.QuarantineStates.DEST)
				{
                    var destroy = NSC_DI.UTIL.Dates.Quarantine.DestroyTimeStamp;

                    if (loadTimerColumn)
					{
						additionalColumnToQuery = ",[OIBT].[U_NSC_TimerQuar] AS [Timer]";
						additionalClauseToQuery = string.Format(" AND CONVERT(int, ISNULL([OIBT].[U_NSC_TimerQuar], 0)) > CONVERT(int, ISNULL({0}, 0)) ", destroy);
						matrixColumnCreation.Add(new CommonUI.Matrix.MatrixColumn()
						{
							ColumnName = MatrixColumns.Timer.ToString("F"),
							Caption = "Timer",
							ColumnWidth = 80,
							ItemType = BoFormItemTypes.it_EDIT
						});
					}
					else
					{
						additionalClauseToQuery = string.Format(" AND CONVERT(int, ISNULL([OIBT].[U_NSC_TimerQuar], 0)) <= CONVERT(int, ISNULL({0}, 0)) ", destroy);
					}

				}
                var defBranch = Globals.BranchDflt;         //  -1 is no branches, 0 is no default branch
                var brList = Globals.BranchList;
                //var destWH = pForm.Items.Item("CBX_WHSE").Specific.Value;
                //if (destWH != "" && defBranch >= 0)
                //{
                  //  defBranch = NSC_DI.SAP.Branch.Get(destWH);
                  //  brList = NSC_DI.SAP.Branch.GetAll_User_Str(defBranch);
               // }
                // #10823 <---
                var sqlQuery = $@"
SELECT
[OIBT].[BatchNum] AS [BatchNumber]
, OBTN.U_NSC_StateID as [StateId]
, [OIBT].[Quantity] AS [Quantity]
, [OIBT].[BatchNum] AS [BatchNumber]
, [OITM].[ItemName] AS [ItemName]
, [OIBT].[WhsCode] AS [WarehouseCode]
, [OWHS].[WhsName] AS [WarehouseName]
, [OIBT].[CreateDate] AS [CreateDate]
, [OITM].[ItemCode]  AS [ItemCode]
{additionalColumnToQuery}
FROM [OIBT] 
JOIN [OITM] ON [OITM].[ItemCode] = [OIBT].[ItemCode]
JOIN [OITB] ON [OITM].[ItmsGrpCod] = [OITB].[ItmsGrpCod]
JOIN [OWHS] ON [OWHS].[WhsCode] = [OIBT].[WhsCode]
LEFT JOIN OBTN ON OBTN.DistNumber = OIBT.BatchNum AND OIBT.ItemCode = OBTN.ItemCode
WHERE [OIBT].[Quantity] > 0 
--AND [OIBT].[U_NSC_QuarState] != 'NONE'
AND [OWHS].[U_NSC_WhrsType] = '{WarehouseType}'
{additionalClauseToQuery}";
                if (defBranch >= 0) sqlQuery += $" AND OWHS.BPLid IN ({brList})";        // #10823

                // Prepare a Matrix helper
                CommonUI.Matrix.LoadDatabaseDataIntoMatrix(pForm, "OIBT", "MTX_MAIN", matrixColumnCreation, sqlQuery);

				// Grab the matrix from the form UI
				MTX_MAIN = pForm.Items.Item("MTX_MAIN").Specific;

				// Allow multi-selection on the Matrix
				MTX_MAIN.SelectionMode = BoMatrixSelect.ms_Auto;

				if (loadTimerColumn)
				{
					// Get and Clear out our DataTable so we can populate with our new selected data.
					var RouteReadyMatrixDataTable = pForm.DataSources.DataTables.Item("OIBT");

					for (var i = 1; i < (MTX_MAIN.RowCount + 1); i++)
					{
						try
						{
							// Stored the Ticks of time so we can just parse the ticks right into DateTime.
							var timeSpan = long.Parse(MTX_MAIN.Columns.Item(MatrixColumns.Timer).Cells.Item(i).Specific.Value);
							var timerStartTime = NSC_DI.UTIL.Dates.FromTimeStamp(timeSpan);

                            var timerEndTime = NSC_DI.UTIL.Dates.Quarantine.DestroyDate;

                            //var formatted = NSC_DI.UTIL.Dates.GetFormattedTimespan(timerStartTime, timerEndTime); // #1150
                            TimeSpan span = timerStartTime - timerEndTime;                                          // #1150
                            string formatted = Math.Round(span.TotalHours, 1).ToString() + " hours";                // #1150

                            RouteReadyMatrixDataTable.SetValue("Timer", i - 1, formatted);
						}
						catch (Exception ex)
						{
							// Send a message to the client
							Globals.oApp.StatusBar.SetText("Failed handle matrix load events!", BoMessageTime.bmt_Short);
						}
					}
				}

                // Reload from our datasource.                
				MTX_MAIN.LoadFromDataSource();
				MTX_MAIN.AutoResizeColumns();

				Load_TabCount(pForm);

			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false); // Un-Freeze the Form UI
				NSC_DI.UTIL.Misc.KillObject(MTX_MAIN);
				GC.Collect();
			}
        }

		private static void Load_TabCount(Form pForm)
		{
			Recordset oRecordSet = null;

			try
			{
				// Prepare to run a SQL statement.
				oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);


                var destroyTimestamp = NSC_DI.UTIL.Dates.Quarantine.DestroyTimeStamp;


                // #10823
                var branchWhere = "";
                if (Globals.BranchDflt >= 0) branchWhere += $" AND OWHS.BPLid IN ({Globals.BranchList})";

                var sqlQuery = $@"
SELECT
(SELECT COUNT 
(DISTINCT [OIBT].[BatchNum])
FROM [OIBT] 
JOIN [OBTN] ON [OBTN].[DistNumber] = [OIBT].[BatchNum]
JOIN [OWHS] ON [OWHS].[WhsCode] = [OIBT].[WhsCode]
WHERE [OBTN].[Status] = 0 
AND [OIBT].[Quantity] > 0 
AND [OBTN].[U_NSC_QuarState] = 'SALE' {branchWhere})
            ,
(SELECT COUNT 
(DISTINCT [OIBT].[BatchNum])
FROM [OIBT] 
JOIN [OBTN] ON [OBTN].[DistNumber] = [OIBT].[BatchNum]
JOIN [OWHS] ON [OWHS].[WhsCode] = [OIBT].[WhsCode]
WHERE [OBTN].[Status] = 0 
AND [OIBT].[Quantity] > 0 
--AND [OBTN].[U_NSC_QuarState] = 'DEST' RH 07oct2020 - commented this out
AND [OWHS].[U_NSC_WhrsType] = 'QND'
AND CONVERT(int, ISNULL([OIBT].[U_NSC_TimerQuar], 0)) <= CONVERT(int, ISNULL({destroyTimestamp}, 0)) {branchWhere} )
            ,
(SELECT COUNT 
(DISTINCT [OIBT].[BatchNum])
FROM [OIBT] 
JOIN [OBTN] ON [OBTN].[DistNumber] = [OIBT].[BatchNum]
JOIN [OWHS] ON [OWHS].[WhsCode] = [OIBT].[WhsCode]
WHERE [OBTN].[Status] = 0 
AND [OIBT].[Quantity] > 0 
AND [OBTN].[U_NSC_QuarState] = 'DEST'
AND CONVERT(int, ISNULL([OIBT].[U_NSC_TimerQuar], 0)) > CONVERT(int, ISNULL({destroyTimestamp}, 0)) {branchWhere} )";
                

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);

				pForm.Items.Item("TAB_1").Specific.Caption = "Sales (" + oRecordSet.Fields.Item(0).Value.ToString() + ")";
				pForm.Items.Item("TAB_2").Specific.Caption = "Destroy (" + oRecordSet.Fields.Item(1).Value.ToString() + ")";
				pForm.Items.Item("TAB_3").Specific.Caption = "Awaiting Timer (" + oRecordSet.Fields.Item(2).Value.ToString() + ")";
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oRecordSet);
				GC.Collect();
			}
		}

		private static int CurrentlySelectedTab(Form pForm)
        {
            for (var i = 1; i < 7; i++)
            {
				if (pForm.Items.Item("TAB_" + i).Specific.Selected) return i;

            }
            return 0;
        }

		// RHH - wasn't being referenced, so commented out. There is an error that needs to be resolved - the form is being referenced
		//private List<string> SelectedBatchIds
		//{
		//	get
		//	{
		//		// Keep track of the selected Plant ID's
		//		var SelectedPlantIDs = new List<string>();

		//		// Grab the matrix from the form UI
		//		var MTX_MAIN = Helpers.Forms.GetControlFromForm(Helpers.Forms.FormControlTypes.Matrix, "MTX_MAIN", _SBO_Form) as Matrix;

		//		// For each row already selected
		//		for (var i = 1; i < (MTX_MAIN.RowCount + 1); i++)
		//		{
		//			if (MTX_MAIN.IsRowSelected(i))
		//			{
		//				// Grab the selected row's Plant ID column
		//				var plantID = ((EditText)MTX_MAIN.Columns.Item(0).Cells.Item(i).Specific).Value;
		//				// Add the selected row plant ID to list
		//				SelectedPlantIDs.Add(plantID);
		//			}
		//		}

		//		return SelectedPlantIDs;
		//	}
		//	set
		//	{
		//		return;
		//	}
		//}

		private static void BTN_LOOKUP(Form pForm)
		{
			Matrix		MTX_MAIN	= null;
			EditText	TXT_LOOKUP	= null;

			try
			{
				// Freeze the form UI
				pForm.Freeze(true);

				// Grab the matrix from the form UI
				MTX_MAIN = pForm.Items.Item("MTX_MAIN").Specific;

				// Find the textbox for scanner results in the form
				TXT_LOOKUP = pForm.Items.Item("TXT_LOOKUP").Specific;

				// For each row in the matrix
				for (var i = 1; i < (MTX_MAIN.RowCount + 1); i++)
				{
					// Grab the selected row's Plant ID column
					var plantID = ((EditText)MTX_MAIN.Columns.Item(0).Cells.Item(i).Specific).Value;

					if (TXT_LOOKUP.Value != plantID) continue;

					MTX_MAIN.SelectRow(i, true, true);
					break;
				}

				TXT_LOOKUP.Value = "";

				TXT_LOOKUP.Active = true;
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
				NSC_DI.UTIL.Misc.KillObject(MTX_MAIN);
				NSC_DI.UTIL.Misc.KillObject(TXT_LOOKUP);
				GC.Collect();
			}
		}

		private static void BTN_DEST_Click(Form pForm)
        {
			Matrix MTX_MAIN = null;
			try
			{
				if (Globals.oApp.MessageBox("Are you sure you want to destroy the selected?", 1, "Yes", "No") != 1) return;

/*		// COMPLIANCE
				// Create compliance call to delete plant
				NSC_DI.SAP.Compliance VirSci_Controllers_Compliance = new Controllers.Compliance(SAPBusinessOne_Application: Globals.oApp,
					SAPBusinessOne_Company: Globals.oCompany);
*/
				// Grab the matrix from the form UI
				MTX_MAIN = pForm.Items.Item("MTX_MAIN").Specific;

				// Prepare a list of items to issue in the "Goods Issued" document.
				var ListOfGoodsIssuedItems = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();

				var StateIdsToDestroy = new List<string>();

				// For each row already selected
				for (var i = 1; i < (MTX_MAIN.RowCount + 1); i++)
				{
					// If the row was selected
					if (!MTX_MAIN.IsRowSelected(i)) continue;

					double quantity = Convert.ToDouble((MTX_MAIN.Columns.Item(MatrixColumns.Quantity).Cells.Item(i).Specific).Value);
					string batchNumber = MTX_MAIN.Columns.Item(MatrixColumns.BatchNumber).Cells.Item(i).Specific.Value;
					string stateId = MTX_MAIN.Columns.Item(MatrixColumns.StateID).Cells.Item(i).Specific.Value;
                    string remarks = pForm.Items.Item("TXT_RMRK").Specific.Value;

					StateIdsToDestroy.Add(stateId);

					// Grab the serial ID, item code, and warehouse code from the matrix of the selected item.
					var ItemCode = ((EditText) MTX_MAIN.Columns.Item(MatrixColumns.ItemCode).Cells.Item(i).Specific).Value;
					var WarehouseCode = ((EditText) MTX_MAIN.Columns.Item(MatrixColumns.WarehouseCode).Cells.Item(i).Specific).Value;

					// Add a new item to our Goods Issued document line items
					ListOfGoodsIssuedItems.Add(new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
					{
						Quantity = quantity,
						BatchLineItems = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>()
						{
							new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches()
							{
								BatchNumber = batchNumber,
								Quantity = quantity
							}
						},
						ItemCode = ItemCode,
						WarehouseCode = WarehouseCode,
                        UserDefinedFields = new Dictionary<string, string>()
                        {
                           {"U_NSC_InvXFerResn", remarks}
                        }
                    });
				}

				// Create SAP "Goods Issued" document in order to remove batch from inventory
				// Attempt to create the "Goods Issued" document.
				NSC_DI.SAP.GoodsIssued.Create(DateTime.Now, ListOfGoodsIssuedItems);
				Load_TabCount(pForm);

				// Reload the matrix with the current quarantine for destruction plants.
				Load_Matrx_Main(pForm, NSC_DI.Globals.QuarantineStates.DEST);

                /*		// COMPLIANCE
                                // Prepare a connection to the state API
                                    var TraceCon = new NSC_DI.SAP.TraceabilityAPI(SAPBusinessOne_Application: Globals.oApp,
                                        SAPBusinessOne_Company: Globals.oCompany,
                                        SAPBusinessOne_Form: _SBO_Form);

                                    BioTrack.API btAPI = TraceCon.new_API_obj();

                                    var allSuccessful = true;

                                    foreach (var barcode in StateIdsToDestroy)
                                    {
                                        BioTrack.Inventory.Destroy(ref btAPI, BarcodeID: barcode, Reason: "Destroying Schedule Cannabis Item");

                                        string compID = VirSci_Controllers_Compliance.CreateComplianceLineItem(
                                                                                                               Reason: Models.Compliance.ComplianceReasons.Inventory_Destroy,
                                            API_Call: btAPI.XmlApiRequest.ToString()
                                            );

                                        btAPI.PostToApi();

                                        if (btAPI.WasSuccesful)
                                        {
                                            VirSci_Controllers_Compliance.UpdateComliancy(
                                                                                          ComplianceID: compID,
                                                ResponseXML: btAPI.xDocFromResponse.ToString(),
                                                Status: Controllers.Compliance.ComplianceStatus.Success);
                                        }
                                        else
                                        {
                                            allSuccessful = false;
                                            VirSci_Controllers_Compliance.UpdateComliancy(
                                                                                          ComplianceID: compID,
                                                ResponseXML: btAPI.xDocFromResponse.ToString(),
                                                Status: Controllers.Compliance.ComplianceStatus.Failed);
                                        }
                                    }

                                    if (allSuccessful)
                                    {
                                        // Send a message to the client
                                        Globals.oApp.StatusBar.SetText("Successfully Destroyed Selected Inventory!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
                                    }
                                    else
                                    {
                                        // Send a message to the client
                                        Globals.oApp.StatusBar.SetText("Failed to Destroyed All Selected Inventory.", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                                    }
                                }
                                else
                                {
                                    // Send a message to the client
                                    Globals.oApp.StatusBar.SetText("Failed to Destory the Item!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Error);
                                }
                 */
                pForm.Items.Item("TXT_RMRK").Specific.Value = "";
				Globals.oApp.StatusBar.SetText("Successfully Destroyed Selected Inventory!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);
			}
			catch (Exception ex)
			{
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(MTX_MAIN);
				GC.Collect();
			}
        }

		private void MTX_MAIN_Matrix_Link_Pressed(Form pForm, ItemEvent pVal)
		{
            Form oBatchDetails = null;
            try
            {
                var selectedRowID = pVal.Row;
                if (selectedRowID < 1) return;

                var BatchSelected = CommonUI.Forms.GetField<string>(pForm, "MTX_MAIN", selectedRowID, 0);

                // Show the batch details

                Globals.oApp.ActivateMenuItem(NavSol.Id.InventoryClass.Batch_Details);

                oBatchDetails = Globals.oApp.Forms.GetForm("65053", 0);
                
                oBatchDetails.Freeze(true);
                
                oBatchDetails.Mode = BoFormMode.fm_FIND_MODE;
                
                ((EditText)oBatchDetails.Items.Item("62").Specific).Value = BatchSelected; // Batch

                ((Button)oBatchDetails.Items.Item("37").Specific).Item.Click(); // Find

            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (oBatchDetails != null) oBatchDetails.Freeze(false);
                NSC_DI.UTIL.Misc.KillObject(oBatchDetails);
                GC.Collect();
            }
        }

        private void FormReSize(Form pForm)
        {
            try
            {
                pForm.Items.Item("TXT_RMRK").Width = pForm.ClientWidth - pForm.Items.Item("TXT_RMRK").Left - 10;                
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(MTX_MAIN);
                GC.Collect();
            }
        }

    }
}