﻿using System;
using System.Collections.Generic;
using System.Linq;
using SAPbobsCOM;
using SAPbouiCOM;

namespace NavSol
{
    public partial class AddOn
    {
        public static bool Init() // retrurn true if completed succusfully
        {


            //-----------------------------------
            // see if the DB has changed
            if (NSC_DI.UTIL.UDO.TableExists(NSC_DI.Globals.tSettings) && NSC_DI.UTIL.Settings.Version.DBChanged(Globals.DBVersion.ToString()) == false) return true;


            if (Globals.AddonInitialized && Globals.oApp.MessageBox("AddOn fields not up-to-date" + Environment.NewLine + " Update?", 1, "Ok", "No", "Cancel") != 1) return false;

            AdminInfo adminInfo = null;
            try
            {
                try
                {
                    adminInfo = Globals.oCompService.GetAdminInfo();
                    adminInfo.SetItemsWarehouses = BoYesNoEnum.tYES;
                    Globals.oCompService.UpdateAdminInfo(adminInfo);
                }
                catch (Exception ex)
                {
                    Globals.oApp.StatusBar.SetText("Unable to update 'Auto Add All Warehouses to New and Existing Items'." + Environment.NewLine + "Continuing with Install\\Update.", BoMessageTime.bmt_Short);
                }

                if (Create() == false) return false;
                AfterInitUpdates();
                NSC_DI.UTIL.Settings.Version.SetDB(Globals.DBVersion.ToString());

                //adminInfo.DefaultWarehouse = "10";
                //Globals.oCompService.UpdateAdminInfo(adminInfo);

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                // ignore exceptions
                return false;
            }
        }

        private static void AfterInitUpdates() // combines recent after initialization changes with the db specific method
        {
            try
            {
                RebuildStrainTable();
                NSC_DI.UTIL.UDO.ChangeTableDescription(NSC_DI.Globals.tCompHarvest, "Compliance Active Harvest");
                ReplaceJointWithPreRoll();

                // UPDATE MENUS
                UpdateSettingsMenuTable("NSC_SEEDBANK", null, "Seedling Planner", "NSC_SEEDPLAN");

                // ***********   THIS THROWS AN ERROR  ************
                //string salesLimDefault = NSC_DI.UTIL.SQL.GetValue<string>(@"SELECT FldValue FROM UFD1 WHERE (TableID = 'oitm') AND (FieldID = 24) AND (IndexID = 0)");
                //var salesLimit = new Dictionary<string, string>() { { "-", "" }, { "U", "Usable Marijuana" }, { "E", "Edible" }, { "M", "MIP" }, { "L", "Liquid" } };
                //if(salesLimDefault != "-")
                //    NSC_DI.UTIL.UDO.UpdateField("OITM", "SalesLimitType", "Sales Limit Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, salesLimit, "-", true);

                // init new settings
                NSC_DI.UTIL.Settings.Value.Set("Use SQL Transactions", "N", "Use SQL Transactions", false);
                NSC_DI.UTIL.Options.Value.GetSingle("Copy Distribution Rules", 1, "Copy Distribution Rules / Dimensions.", "Y");

                if (Globals.DBVersion == 44)
                {
                    // does not work
                    //NSC_DI.UTIL.UDO.UpdateField("OSRN", "QuarState", "Quarantine State", BoFieldTypes.db_Alpha, EditSize: 20);
                }

                if (Globals.DBVersion == 42)
                {
                    // well, they do not want this after all
                    //DeleteAutoBOMs();
                    //DeleteAutoItems();
                }

                if (Globals.DBVersion == 38)
                {
                    NSC_DI.UTIL.Settings.Value.Set("AddedInvGoodsIsu", "N");
                    NSC_DI.UTIL.Settings.Value.Set("AddedInvTypeXFer", "N");
                    NSC_DI.UTIL.Settings.Value.Set("AddedDestructTypes", "N");
                    NSC_DI.UTIL.Settings.Value.Set("AddedQuarantineType", "N");
                }

                //	COMPLIANCE - Inventory Transfer Types
                if (Globals.DBVersion == 36)
                {
                    NSC_DI.UTIL.UDO.FieldRemove(NSC_DI.Globals.tCompliance, "CompCode");    // delete the old field
                    NSC_DI.UTIL.Settings.Value.Set("AddedInvTypeXFer", "N", "Indicates that all Inventory Transfer Types have been created.", true);
                }

                //	Batch Serial Trace
                if (Globals.DBVersion == 31) NSC_DI.UTIL.UDO.UserTableRemove(NSC_DI.Globals.tBatSerTrace);

                //	Compliance Log
                if (Globals.DBVersion == 26)
                {
                    NSC_DI.UTIL.UDO.UserTableRemove("NSC_COMP");                            // delete the old table
                    NSC_DI.UTIL.UDO.FieldRemove(NSC_DI.Globals.tCompliance, "CallType");    // delete the old field
                }
                if (Globals.DBVersion == 27) NSC_DI.UTIL.UDO.FieldRemove(NSC_DI.Globals.tCompliance, "Status");      // delete the old field
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static bool Create()
        {
            ProgressBar oBar = null;
            Form oForm = null;
            try
            {

                oBar = Globals.oApp.StatusBar.CreateProgressBar("Creating User Fields.", 32, false);
                oBar.Value = 0;
                CreateSettings();
                CreateUserOptions();
                //FillUOMs();
                //FillUOM_Groups();

                CreateCustom(ref oBar);
                CreateCustomCannabis(ref oBar);
 
                ComplianceCreate(ref oBar);

                FillSettingsTable();

                //DB_Ver_Specific();
                CreateSAP(ref oBar);
                CreateSAPCannabis(ref oBar);
                CreateAuto(ref oBar);

                CreateSQL();

                FillTestSampleUDFs();
                FillInvTypeXFer();
                //FillInvGoodsIsu(); obsolete
                FillComplianceType();
                FillDestructTypes();
                FillQuarantineType();
                FillTestList();
                FillAdjustmentType();

                FillSettingsMenuTableAll();
                FillSettingsMenuTableNew();
                FillUserOptionsTable();

                FillWarehouses();
                // create the item groups
                FillItemGroups();

                FillitemProperties();

                FillFieldLabels();

                FillProdCat();

                // Fill some Tables
                if (NSC_DI.UTIL.Settings.Value.Get("Use Template Items") == "Y") FillAutoItem_NEW();
                else FillAutoItem_OLD();

                oBar.Value++;

                if (NSC_DI.UTIL.Settings.Value.Get("Use Template Items") == "Y") FillAutoBOM_NEW();
                else FillAutoBOM_OLD();

                oBar.Value++;

                FillAutoStruct();

                FillPdoPhases();

                FillStrainTypes();

                //NSC_DI.UTIL.UDO.UserTableRemove(NSC_DI.Globals.tInvTypGdsIsu);    // this table stays

                NSC_DI.SAP.Employee.CreateRole("Driver", "Driver");
                oBar.Value++;
                //AddOn.oApp.SetStatusBarMessage("Creating User Fields.", SAPbouiCOM.BoMessageTime.bmt_Medium, false);
                return true;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return false;

            }
            finally
            {
                if (oBar != null) oBar.Stop();
                NSC_DI.UTIL.Misc.KillObject(oBar);
                NSC_DI.UTIL.Misc.KillObject(oForm);
            }
        }

        internal static void CreateSQL()
        {
            // create views, stored procs, and functions

            var sql = "";
            var sqlCode = "";

            // DROP VIEW IF EXISTS view_name
            // DROP PROCUDURE IF EXISTS proc_name 

            try
            {
                // NSC_DOC_BnS 
                var NSC_DOC_BnS = $@"SELECT
       OBTN.DistNumber AS BatchID, OSRN.DistNumber AS SerialID, 
       COALESCE(OBTN.AbsEntry , OSRN.AbsEntry ) AS AbsEntry , 
       COALESCE(OBTN.MnfSerial, OSRN.MnfSerial) AS MnfSerial, 
       COALESCE(OBTN.ItemCode,  OSRN.ItemCode) AS ItemCode,
       COALESCE(OBTN.LotNumber, OSRN.LotNumber) AS [LotNumber], 
       COALESCE(OBTN.Notes, OSRN.Notes) AS Notes,
       COALESCE(OBTN.DistNumber, OSRN.DistNumber) AS DistNumber, 
       ITL1.Quantity AS Quantity, OITL.DocEntry, OITL.DocType, OITL.DocLine, 
       COALESCE(OBTN.U_NSC_HarvestPdO, OSRN.U_NSC_HarvestPdO) AS HarvestPdO,
       COALESCE(OBTN.U_NSC_RemediateCode, OSRN.U_NSC_RemediateCode) AS RemediateCode,
       COALESCE(OBTN.U_NSC_PassedQA, OSRN.U_NSC_PassedQA) AS PassedQA
  FROM OITL
  LEFT JOIN ITL1 ON OITL.LogEntry = ITL1.LogEntry
  LEFT JOIN OBTN ON ITL1.SysNumber = OBTN.SysNumber AND OBTN.ItemCode = ITL1.ItemCode
  LEFT JOIN OSRN ON ITL1.SysNumber = OSRN.SysNumber AND OSRN.ItemCode = ITL1.ItemCode
 WHERE OITL.StockEff = '1' ";

                //// see if there is a change
                //sql = NSC_DI.UTIL.SQL.FixSQL($"SELECT OBJECT_DEFINITION(OBJECT_ID('NBS_DOC_BnS'))");
                //sqlCode = NSC_DI.UTIL.SQL.GetValue<string>($"EXEC('{sql}')", null);
                //if(sqlCode != null && sqlCode != NBS_DOC_BnS)

                sql = $"EXEC('DROP VIEW IF EXISTS NSC_DOC_BnS')";
                NSC_DI.UTIL.SQL.RunSQLQuery(sql, null);

                NSC_DOC_BnS = NSC_DI.UTIL.SQL.FixSQL(NSC_DOC_BnS);
                sql = $"EXEC('CREATE VIEW NSC_DOC_BnS AS {NSC_DOC_BnS}')";
                NSC_DI.UTIL.SQL.RunSQLQuery(sql, null);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
            }
        }

        internal static void InitSettings()
        {
            CreateSettings();
            NSC_DI.UTIL.Settings.Version.SetDB("0");
            NSC_DI.UTIL.Settings.Version.SetProduct("0");
            FillSettingsTable();
        }

        //public static void DB_Ver_Specific() // mods that are for specific DB versions
        //{
        //    try
        //    {
        //        //-----------------------------------
        //        // init new settings
        //        NSC_DI.UTIL.Settings.Value.Set("Use SQL Transactions", "N", "Use SQL Transactions", false);
        //        NSC_DI.UTIL.Options.Value.GetSingle("Copy Distribution Rules", 1, "Copy Distribution Rules / Dimensions.", "Y");

        //        if (Globals.DBVersion == 44)
        //        {
        //            // does not work
        //            //NSC_DI.UTIL.UDO.UpdateField("OSRN", "QuarState", "Quarantine State", BoFieldTypes.db_Alpha, EditSize: 20);
        //        }

        //        if (Globals.DBVersion == 42)
        //        {
        //            // well, they do not want this after all
        //            //DeleteAutoBOMs();
        //            //DeleteAutoItems();
        //        }

        //        if (Globals.DBVersion == 38)
        //        {
        //            NSC_DI.UTIL.Settings.Value.Set("AddedInvGoodsIsu", "N");
        //            NSC_DI.UTIL.Settings.Value.Set("AddedInvTypeXFer", "N");
        //            NSC_DI.UTIL.Settings.Value.Set("AddedDestructTypes", "N");
        //            NSC_DI.UTIL.Settings.Value.Set("AddedQuarantineType", "N");
        //        }

        //        //	COMPLIANCE - Inventory Transfer Types
        //        if (Globals.DBVersion == 36)
        //        {
        //            NSC_DI.UTIL.UDO.FieldRemove(NSC_DI.Globals.tCompliance, "CompCode");    // delete the old field
        //            NSC_DI.UTIL.Settings.Value.Set("AddedInvTypeXFer", "N", "Indicates that all Inventory Transfer Types have been created.", true);
        //        }

        //        //	Batch Serial Trace
        //        if (Globals.DBVersion == 31) NSC_DI.UTIL.UDO.UserTableRemove(NSC_DI.Globals.tBatSerTrace);

        //        //	Compliance Log
        //        if (Globals.DBVersion == 26)
        //        {
        //            NSC_DI.UTIL.UDO.UserTableRemove("NSC_COMP");                            // delete the old table
        //            NSC_DI.UTIL.UDO.FieldRemove(NSC_DI.Globals.tCompliance, "CallType");    // delete the old field
        //        }
        //        if (Globals.DBVersion == 27) NSC_DI.UTIL.UDO.FieldRemove(NSC_DI.Globals.tCompliance, "Status");      // delete the old field
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(NSC_DI.UTIL.Message.Format(ex));
        //    }
        //    finally
        //    {
        //    }
        //}

        private static void CreateSAP(ref ProgressBar oBar)
        {
            // SAP OBJECTS

            try
            {
                var YN0 = new Dictionary<string, string>() { { "Y", "Yes" }, { "N", "No" } };
                var syncStatus = new Dictionary<string, string>() { { "R", "Ready" }, { "H", "Hold" }, { "P", "Processed" } };

                //--------------- 
                //	OHEM - Employee Master
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OHEM", "NavSolUser", "NavSol Apps Username", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable("OHEM", "NavSolPass", "NavSol Apps Password", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable("OHEM", "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, syncStatus, "R", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OHEM", "Updated", "Employee Updated", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true);
                NSC_DI.UTIL.UDO.FieldRemove("OHEM", "StateUserKey");
                //NSC_DI.UTIL.UDO.AddFieldToTable("OHEM", "StateUserKey", "Employee State ID",    BoFieldTypes.db_Alpha, EditSize: 250);

                //--------------- 
                //	OSRN - Serial Numbers
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "IsMother", "Is A Mother Plant", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "MotherID", "Mother Plant ID", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "QuarState", "Quarantine State", BoFieldTypes.db_Alpha, EditSize: 4, ValidValues: new Dictionary<string, string>() { { "NONE", "None" }, { "SICK", "Sick" }, { "DEST", "Destroy" }, { "SALE", "Sale" } }, DefaultValue: "NONE");
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "Barcode", "Barcode", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "TimerQuar", "CountDown", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "LabTestID", "Lab Test ID", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "PassedQA", "Passed QA", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "IsMedical", "Is Medical", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, syncStatus, "R", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "HarvestDate", "Harvest Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "MotherStatusChg", "Mother Status Changed", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true);

                NSC_DI.UTIL.UDO.AddValidValue("OSRN", "StateSyncStatus", "U", "Updated"); // new value
                NSC_DI.UTIL.UDO.AddValidValue("OSRN", "PassedQA", "F", "Failed");  // new value

                //NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "Moisture", "Moisture", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);	//Test Results for the Batch Moisture
                //NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "AeroBact", "Aerobic Bacteria", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                //NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "Bile", "Bile Tolerance", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                //NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "Coliforms", "Coliforms", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                //NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "E_Coli", "E Coli and Salmonella", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                //NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "YeistMold", "Yeist and Mold", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);

                //--------------- 
                //	ODLN - Marketing Document Header
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("ODLN", "RouteID", "Route ID", BoFieldTypes.db_Numeric);
                //NSC_DI.UTIL.UDO.AddFieldToTable("ODLN", "QRCode",       "QR Code",                       BoFieldTypes.db_Alpha, BoFldSubTypes.st_Image, 5000);
                //NSC_DI.UTIL.UDF.AddFieldToTable("ODLN", "TripDuration", "Approximate Trip Duration",     BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable("ODLN", "TransDate", "Transaction Date Time", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("ODLN", "PdORcptType", "Production Order Receipt Type", BoFieldTypes.db_Alpha, EditSize: 64);

                NSC_DI.UTIL.UDO.FieldRemove("ODLN", "QRCode");          // moved to the route stops table
                NSC_DI.UTIL.UDO.FieldRemove("ODLN", "TripDuration");    // moved to the route stops table

                //--------------- 
                //	DLN1 - Marketing Document Row
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("DLN1", "RouteID", "Route ID", BoFieldTypes.db_Numeric);

                //--------------- 
                // IGE1 - Goods Issue Row   ( Marketing Document )
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("IGE1", "AdjustmentReason", "Adjustment Reason", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 64);

                //--------------- 
                // IGN1 - Goods Receipt Row ( Marketing Document )
                //--------------- 
                //NSC_DI.UTIL.UDO.AddFieldToTable("IGN1", "WasteType", "Waste Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 30);
                NSC_DI.UTIL.UDO.FieldRemove("IGN1", "WasteType");    // change of plans

                //--------------- 
                // OWHS 
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OWHS", "OcrCode1", "Default Distribution Rule 1", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 8);
                NSC_DI.UTIL.UDO.AddFieldToTable("OWHS", "OcrCode2", "Default Distribution Rule 2", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 8);
                NSC_DI.UTIL.UDO.AddFieldToTable("OWHS", "OcrCode3", "Default Distribution Rule 3", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 8);
                NSC_DI.UTIL.UDO.AddFieldToTable("OWHS", "OcrCode4", "Default Distribution Rule 4", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 8);
                NSC_DI.UTIL.UDO.AddFieldToTable("OWHS", "OcrCode5", "Default Distribution Rule 5", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 8);

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oBar.Value++;
            }
        }

        private static void CreateSAPCannabis(ref ProgressBar oBar)
        {
            // SAP OBJECTS

            try
            {
                var YN0 = new Dictionary<string, string>() { { "Y", "Yes" }, { "N", "No" } };
                var syncStatus = new Dictionary<string, string>() { { "R", "Ready" }, { "H", "Hold" }, { "P", "Processed" } };
                var salesLimit = new Dictionary<string, string>() { { "-", "" }, { "U", "Usable Marijuana" }, { "E", "Edible" }, { "M", "MIP" }, { "L", "Liquid" } };              

                if (Globals.IsCannabis == false) return;

                //--------------- 
                //	OIQR - Inventory Posting
                //--------------- 

                // if the NBS_EmpID is alpha, have to remove it.
                if (Globals.DBVersion <= 72)
                {
                    if (NSC_DI.UTIL.SQL.GetValue<string>("SELECT TypeID FROM CUFD WHERE TableID = 'OIQR' AND AliasID = 'NBS_EmpID'", "") == "A")
                        NSC_DI.UTIL.UDO.FieldRemove("OIQR", "EmpID", "NBS");    // delete the old field
                }
                NSC_DI.UTIL.UDO.AddFieldToTable("OIQR", "EmpID", "Employee ID", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 10, PartnerCode: "NBS");
                NSC_DI.UTIL.UDO.AddFieldToTable("OIQR", "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, syncStatus, "R", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OIQR", "AdjustmentReason", "Adjustment Reason", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OIQR", "ReasonNote", "Reason Note", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 250);
                NSC_DI.UTIL.UDO.AddFieldToTable("OIQR", "InvAdjType", "Inventory Adjustment Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, null, null, false, NSC_DI.Globals.tInvAdjustTypes);
                NSC_DI.UTIL.UDO.FieldRemove("OIQR", "EmpID");           // delete the old field

                //--------------- 
                //	OITM - Item Master
                //--------------- 


                NSC_DI.UTIL.UDO.FieldRemove("OITM", "MotherStatusChg", "NSC"); //11803
                NSC_DI.UTIL.UDO.FieldRemove("OITM", "IsMother", "NSC"); //11803
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "StrainID", "Strain ID", BoFieldTypes.db_Alpha, EditSize: 25);
                //NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "IsMother", "Is a Mother Plant", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true); //11803
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "Sampleable", "Is Item Sampleable", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "Batchable", "Is Item Batchable", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "Processable", "Is Item Processable", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "NextNum", "Next Number for SNs or Batches", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 8);
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "Factor", "Factor", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "Template", "Template Code", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20);
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, syncStatus, "H", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "Description2", "Description  2", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 200);
                //NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "LastUpdated",  "Last Updated",                     BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 30); // created by iConnect
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "Brand", "Brand", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "SalesLimitType", "Sales Limit Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, salesLimit, "-", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "NSC_THC_CBD", "THC,CBD Override", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 32);
                NSC_DI.UTIL.UDO.UpdateField("OITM", "NextNum", null, 8);
                if (Globals.DBVersion <= 115)             
                    NSC_DI.UTIL.UDO.FieldRemove("OITM", "ReportUOM");
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "LeafLnkPrnt", "LeafLink Parent", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100);
                NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "LeafLnkSync", "Sync to LeafLink", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true);
                //--------------- 
                //	OITW - Item \ Warehouse Master
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OITW", "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, syncStatus, "R", true);

                //--------------- 
                //	OCRD - Business Partner
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "StateUBI", "State Unique ID", BoFieldTypes.db_Alpha, EditSize: 64);
                //TODo: Only have it build for retail MV. 
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "LocUBI", "State Location", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "Birthday", "Date of Birth", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "StateDL", "State Driver's License ID", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "StateStart", "Effective Start Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "StateEnd", "Effective End Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "StateRecPlant", "Recommended Plants", BoFieldTypes.db_Numeric);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "StateRecSmokQuant", "Recommended Smokable Quantity", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "LicenseType", "License Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1,
                    new Dictionary<string, string>() { { "M", "Medical" }, { "R", "Recreational" } }, "M", true);

                //--------------- 
                //	OCPR - BP Contacts table
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OCPR", "CareGiverID", "Care Giver ID", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCPR", "CareGivStateDL", "CareGiver Driver's License ID", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCPR", "StateStart", "Effective Start Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCPR", "StateEnd", "Effective End Date", BoFieldTypes.db_Date);

                //--------------- 
                //	DLN1 - Marketing Document Row
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("DLN1", "StateID", "State ID", BoFieldTypes.db_Alpha, EditSize: 64);

                //--------------- 
                //	ORDR - Marketing Document Header
                //--------------- 

                // if the NBS_EmpID is alpha, have to remove it.
                if (Globals.DBVersion <= 72)
                {
                    if (NSC_DI.UTIL.SQL.GetValue<string>("SELECT TypeID FROM CUFD WHERE TableID = 'ORDR' AND AliasID = 'NBS_EmpID'", "") == "A")
                        NSC_DI.UTIL.UDO.FieldRemove("ORDR", "EmpID", "NBS");    // delete the old field
                }
                NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "WeedProdTrnsf", "Cannabis Transfer", BoFieldTypes.db_Alpha, EditSize: 1);
                //NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "InvGdsIsuType", "Goods Issue Type",        BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, null, null, false, NSC_DI.Globals.tInvTypGdsIsu);
                NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "InvXFerType", "Inventory XFer Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, null, null, false, NSC_DI.Globals.tInvTypXFer);
                NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "InvXFerResn", "Inventory XFer Reason", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 250);
                NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "ComplianceType", "Compliance Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, null, null, false, NSC_DI.Globals.tCompTypes);
                NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "EmpID", "Employee ID", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 10, PartnerCode: "NBS");
                NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "ComplianceManf", "Compliance Manifest", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50);
                NSC_DI.UTIL.UDO.FieldRemove("ORDR", "InvGdsIsuType");   // delete the old field
                NSC_DI.UTIL.UDO.UserTableRemove(NSC_DI.Globals.tInvTypGdsIsu);
                //NSC_DI.UTIL.UDO.FieldRemove(    "ORDR", "EmpID");           // delete the old field

                //--------------- 
                //	OBTN - Batch Number Master
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OBTN", "NextSubNum", "Next Sub Batch Number", BoFieldTypes.db_Numeric, EditSize: 8);
                NSC_DI.UTIL.UDO.AddFieldToTable("OBTN", "HarvestPdO", "Harvest Production Order Number", BoFieldTypes.db_Numeric, EditSize: 8);
                NSC_DI.UTIL.UDO.AddFieldToTable("OBTN", "CropID", "Crop Cycle Id", BoFieldTypes.db_Numeric, EditSize: 8);
                NSC_DI.UTIL.UDO.AddFieldToTable("OBTN", "CropStageID", "Crop Stage ID", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable("OBTN", "WasteType", "Waste Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, null, null, false, NSC_DI.Globals.tWasteTypes);
                NSC_DI.UTIL.UDO.AddFieldToTable("OBTN", "RemediateCode", "Remediate Code", BoFieldTypes.db_Numeric, EditSize: 8);
                NSC_DI.UTIL.UDO.AddFieldToTable("OBTN", "HarvestName", "Harvest Name", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 100);
                NSC_DI.UTIL.UDO.AddFieldToTable("OBTN", "ProdBatchID", "Production Batch ID", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50); //11010 Same as the one in OWOR
                NSC_DI.UTIL.UDO.AddKey("HvstNam", "OBTN", "NSC_HarvestName", BoYesNoEnum.tNO);
                NSC_DI.UTIL.UDO.AddFieldToTable("OBTN", "GrossWeight", "Gross Weight", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50);
                Dictionary<string, string> testTypeVals = new Dictionary<string, string>() { { "-", "" }, { "L", "Initial Lab" }, { "I", "Internal" }, { "R","Retest" } };
                NSC_DI.UTIL.UDO.AddFieldToTable("OBTN", "TestType", "Test Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 32, testTypeVals, "-");
                NSC_DI.UTIL.UDO.AddFieldToTable("OBTN", "WasteWeight", "Waste Weight", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement, 10); // 13816


                //--------------------
                //  OINV - AR Invoice
                //--------------------
                NSC_DI.UTIL.UDO.AddFieldToTable("OINV", "CustomerType", "Customer Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 3,
                    new Dictionary<string, string>() {{"MED", "Medical" }, {"REC", "Recreational" } });
                NSC_DI.UTIL.UDO.AddFieldToTable("OINV", "PatientLicense", "Patient License", BoFieldTypes.db_Alpha, EditSize: 30);
                NSC_DI.UTIL.UDO.AddFieldToTable("OINV", "CaregiverLicense", "Caregiver License", BoFieldTypes.db_Alpha, EditSize: 20);               
                NSC_DI.UTIL.UDO.AddFieldToTable("OINV", "ExtIDType", "Ext ID Type", BoFieldTypes.db_Alpha, EditSize: 30);
                string ExtIDDescr = NSC_DI.UTIL.SQL.GetValue<string>(@"SELECT Descr FROM CUFD WHERE(TableID = 'oinv') AND (AliasID = 'nsc_extidtype')");
                if (ExtIDDescr.ToUpper() != "EXT ID TYPE")
                    NSC_DI.UTIL.UDO.UpdateField("OINV", "ExtIDType", "Ext ID Type", BoFieldTypes.db_Alpha, EditSize: 30);


                //--------------- 
                //	OITB - Item Group
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OITB", "CanType", "Cannabis Item Groups", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 6,
                    new Dictionary<string, string>()
                    {
						//{"1", "Cannabis - Seed"},
						//{"2", "Clone Clipping"},
						//{"3", "Cannabis - Plant"},
						//{"4", "Wet Cannabis"},    
						//{"5", "Dried Cannabis"},
						//{"6", "Manicured Trim"},
						//{"7", "Manicured Bud"},
						//{"8", "Plant Waste"},
						//{"9", "Packaged Cannabis"},
						//{"10", "Concentrate"},
						//{"11", "Additive"},
						//{"12", "Pesticide"},
						//{"13", "Sample"},
						//{"14", "QA Batch"}
						{"-", ""},
                        {"QCLot", "QC ed Lot"},
                        {"PacC", "Packaged Cannabis"},
                        {"PacX", "Packaged Extract"},
                        {"PacM", "Packaging Material"},
                        {"FgFl", "Finished Good for Flower"}
                    }
                    , "-");
                NSC_DI.UTIL.UDO.AddFieldToTable("OITB", "StateID", "State ID", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OITB", "CmplCatUOM", "Compliance Category UOM", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1,
                    new Dictionary<string, string>()
                    {
                        {"-", ""},
                        {"C", "Count Based"},
                        {"W", "Weight Based"}
                    }
                    , "-");
                NSC_DI.UTIL.UDO.AddFieldToTable("OITB", "CmplTestID", "Compliance Test ID", BoFieldTypes.db_Alpha, EditSize: 64);                            
                NSC_DI.UTIL.UDO.AddFieldToTable("OITB", "ProdCat", "Product Category", BoFieldTypes.db_Alpha, EditSize: 64, LinkTable: NSC_DI.Globals.tProdCategory);
                var reportUOM = new Dictionary<string, string>() { { "-", "" }, { "ea", "Each" }, { "fl oz", "Fluid Ounces" }, { "gal", "Gallons" }, { "g", "Grams" },
                    { "kg", "Kilograms"}, { "l", "Liters" }, { "mg", "Milligrams" }, { "ml", "Millileters"}, { "oz", "Ounces" }, { "pt", "Pints" }, { "lb", "Pounds" }, { "qt", "Quarts" } };
                
                NSC_DI.UTIL.UDO.AddFieldToTable("OITB", "ReportUOM", "POS Reporting UOM", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 32, reportUOM, "-", true);

                //--------------- 
                //	OITL - Inventory Transactions Log Header
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OITL", "CmpTransID", "Compliance Transaction ID", BoFieldTypes.db_Alpha, EditSize: 32);

                //--------------- 
                //	ITL1 - Inventory Transactions Log Detail
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("ITL1", "CmpTransID", "Compliance Transaction ID", BoFieldTypes.db_Alpha, EditSize: 32);

                //--------------- 
                //	OITT - BOM
                //--------------- 
                //NSC_DI.UTIL.UDO.AddFieldToTable("OITT", "Pln_Batch", "Projected Batch", BoFieldTypes.db_Alpha, EditSize: 250);
                //NSC_DI.UTIL.UDO.AddFieldToTable("OITT", "CnpProduct", "Is Cannabis Product", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true);
                NSC_DI.UTIL.UDO.FieldRemove("OITT", "Process");
                NSC_DI.UTIL.UDO.AddFieldToTable("OITT", "DefProcess", "Default Process", BoFieldTypes.db_Alpha, EditSize: 32);

                //--------------- 
                //	OWOR - Production Order StateSyncStatus
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OWOR", "Pln_Batch", "Projected Batch", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable("OWOR", "CnpProduct", "Is Cannabis Product", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OWOR", "Special", "PdO Special", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable("OWOR", "Process", "PdO Process", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 32, null, "");
                NSC_DI.UTIL.UDO.AddFieldToTable("OWOR", "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 32, null, "R");
                NSC_DI.UTIL.UDO.AddFieldToTable("OWOR", "CropStageID", "Crop Stage ID", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable("OWOR", "isProductionBatch", "Is Production Batch", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OWOR", "ProductionBatchID", "Production Batch ID", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50);
                NSC_DI.UTIL.UDO.AddFieldToTable("OWOR", "SampleType", "Sample Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10,
                    new Dictionary<string, string>()
                    {
                        {"-", ""},
                        {"Customer", "Customer"},
                        {"Employee", "Employee"},
                        {"Lab",      "Vendor Lab"}
                    }
                    , "-");
                NSC_DI.UTIL.UDO.AddFieldToTable("OWOR", "SampleName", "Sample Name", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable("OWOR", "EmpID", "Employee ID", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, LinkSysObjEnum: UDFLinkedSystemObjectTypesEnum.ulEmployeesInfo); 

                //---------------------
                // OCLG - Activity
                //---------------------
                //NSC_DI.UTIL.UDO.AddFieldToTable("OCLG", "EmpID", "Employee ID", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, LinkSysObjEnum: UDFLinkedSystemObjectTypesEnum.ulEmployeesInfo); 

                //--------------- 
                //	OSRN - Serial Numbers
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "StateID", "State ID", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "WetWgt", "Wet weight", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "HarvestDate", "Harvest Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "CBD", "CBD", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "CBN", "CBN", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "THC", "THC", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "THCA", "THCA", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "TotalCanna", "Total Cannabinoids", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "ResSolvent", "Residual Solvent", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "CollectAdd", "API Collect Additional", BoFieldTypes.db_Numeric);   // no more BioTrack
                                                                                                                            // Used to associate Group harvested plants. We have to track all the StateIds so we can do a create lot.
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "GroupNum", "Group Batch #", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "CropID", "Crop Cycle Id", BoFieldTypes.db_Numeric, EditSize: 8);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "CropStageID", "Crop Stage ID", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable("OSRN", "WasteWeight", "Waste Weight", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement, 10); // 13816

                //--------------- 
                //	OWHS - Warehouse Master
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OWHS", "WhrsType", "Warehouse Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 3,
                    new Dictionary<string, string>()
                    {
                      { "GER", "Germinating" }
                    , { "CLO", "Clone" }
                    , { "VEG", "Vegetative" }
                    , { "FLO", "Flowering" }
                    , { "DRY", "Drying" }
                    , { "CUR", "Curing" }
                    , { "WET", "Wet" }
                    , { "WTR", "Water Tank" }
                    , { "QNX", "Quarantine - Sick" }
                    , { "QND", "Quarantine - Destroy" }
                    , { "QNS", "Quarantine - Sales" }
                    , { "DEV", "Derivative Processing" }
                    , { "FIN", "Finished Goods" }
                    , { "PRO", "Processing" }
                    , { "WOR", "Work Room" }
                    });
                if (NSC_DI.UTIL.Settings.Version.GetCompliance().ToUpper() == "METRC") // 11927 For METRC
                {
                    // checks to see if the field is set to the biotrack options since thats what the dflt was. Updates it to the new way for metrc 
                    int stateUDFCount = NSC_DI.UTIL.SQL.GetValue<int>(@"
                        SELECT COUNT (UFD1.FieldID)
                        FROM UFD1
                        INNER JOIN CUFD on UFD1.FieldID = CUFD.FieldID
                        WHERE (UFD1.TableID = 'owhs') AND (CUFD.ALIASID = 'NSC_State')");
                    if (stateUDFCount > 0)
                        NSC_DI.UTIL.UDO.UpdateField("OWHS", "State", "State Warehouse Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, null, null, false, NSC_DI.Globals.tStateLocType);
                    else
                        NSC_DI.UTIL.UDO.AddFieldToTable("OWHS", "State", "State Warehouse Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, null, null, false, NSC_DI.Globals.tStateLocType); // sets it to the metrc settings for new dbs
                }
                if (NSC_DI.UTIL.Settings.Version.GetCompliance().ToUpper() == "BIOTRACK") // 11927 For BIOTRACK, was the dflt before
                    NSC_DI.UTIL.UDO.AddFieldToTable("OWHS", "State", "State Warehouse Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 3, new Dictionary<string, string>() { { "INV", "Inventory" }, { "CUL", "Cultivation" } });

                NSC_DI.UTIL.UDO.AddFieldToTable("OWHS", "StateID", "State ID", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 64,
                    new Dictionary<string, string>()
                    {
                      { "Producer",     "Producer" }
                    , { "Processor",    "Processor" }
                    , { "Wholesaler",   "Wholesaler" }
                    , { "Retailer",     "Retailer" }
                    , { "Testing Lab",  "Testing Lab" }
                    , { "Research",     "Research" }
                    });
                NSC_DI.UTIL.UDO.AddFieldToTable("OWHS", "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, new Dictionary<string, string>() { { "R", "Ready" }, { "H", "Hold" }, { "P", "Processed" } }, "R", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OWHS", "DG_LocationID", "DataGreen Location ID", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 8, DefaultValue: "0");
                NSC_DI.UTIL.UDO.AddFieldToTable("OWHS", "SubsidiaryID", "Subsidiary Co. ID", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 8, DefaultValue: "");

                //--------------- 
                //	ORDR - Sales Order
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, new Dictionary<string, string>() { { "R", "Ready" }, { "H", "Hold" }, { "P", "Processed" } }, "R", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "StateSyncStatus_INV2", "INV State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, new Dictionary<string, string>() { { "R", "Ready" }, { "H", "Hold" }, { "P", "Processed" } }, "H", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "StateSyncStatus_WHT", "WH State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, new Dictionary<string, string>() { { "R", "Ready" }, { "H", "Hold" }, { "P", "Processed" } }, "H", true);

                NSC_DI.UTIL.UDO.AddValidValue("ORDR", "StateSyncStatus", "U", "Updated"); // new value

                //--------------- 
                // OIGE - Goods Issue
                //--------------- 
                NSC_DI.UTIL.UDO.AddFieldToTable("OIGE", "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, new Dictionary<string, string>() { { "R", "Ready" }, { "H", "Hold" }, { "P", "Processed" } }, "R", true);
                NSC_DI.UTIL.UDO.AddFieldToTable("OIGE", "InvAdjType", "Inventory Adjustment Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50, null, null, false, NSC_DI.Globals.tInvAdjustTypes);
                NSC_DI.UTIL.UDO.AddFieldToTable("OIGE", "MixMats", "Mix Materials", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 30, null, null, false, NSC_DI.Globals.tMixMats);
                //NSC_DI.UTIL.UDO.AddFieldToTable("OIGE", "WasteWgt", "Waste Weight", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 32);
                NSC_DI.UTIL.UDO.AddFieldToTable("OIGE", "WasteUoM", "Waste UoM", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 32, new Dictionary<string, string>() { {"Grams", "Grams"}, {"Ounces", "Ounces"}, {"Kilograms", "Kilograms"}, {"Pounds","Pounds"} }, "Grams", true);
                NSC_DI.UTIL.UDO.FieldRemove("OIGE", "WasteWgt");                

                //****CropCycle Management **** 
                Forms.Crop.CropCycleManagement Crop = new Forms.Crop.CropCycleManagement();
                //---------------
                // PMG2 - Project Stages (Obj 63)
                //---------------
                Crop.AddStages_DB();
                //---------------
                // PMG6 - Project Tasks by Stage (Obj ?)
                //---------------

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oBar.Value++;
            }
        }

        private static void CreateCustom(ref ProgressBar oBar)
        {
            // CUSTOM USER OBJECTS

            try
            {
                var YN0 = new Dictionary<string, string>() { { "Y", "Yes" }, { "N", "No" } };
                var syncStatus = new Dictionary<string, string>() { { "R", "Ready" }, { "H", "Hold" }, { "P", "Processed" } };

                // TRANSACTION LOG
                NSC_DI.UTIL.UDO.AddUserTable("SPTRANSLOG", "Post Trans Log", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable("NSC_SPTRANSLOG", "ObjectType", "Object Type", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable("NSC_SPTRANSLOG", "TransType", "Trans Type", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable("NSC_SPTRANSLOG", "ObjectCode", "Object Code", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable("NSC_SPTRANSLOG", "ItemCode", "Itemt Code", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable("NSC_SPTRANSLOG", "RecCount", "Number Records Updated", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 8, null, "0");


                //--------------- 
                //	Batch Serial Trace
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tBatSerTrace, "Batch Serial Tracking", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tBatSerTrace, "Level", "Level", BoFieldTypes.db_Numeric, EditSize: 8);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tBatSerTrace, "ItemCode", "Item Code", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tBatSerTrace, "ItemName", "Item Name", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tBatSerTrace, "Batch", "Batch", BoFieldTypes.db_Alpha, EditSize: 40);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tBatSerTrace, "Serial", "Serial Number", BoFieldTypes.db_Alpha, EditSize: 40);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tBatSerTrace, "Receipt", "Receipt", BoFieldTypes.db_Numeric, EditSize: 8);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tBatSerTrace, "PdO", "PdO", BoFieldTypes.db_Numeric, EditSize: 8);
                oBar.Value++;

                //--------------- 
                //	Microvertical Menu Item
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tSettingsMenu, "Settings Menu", BoUTBTableType.bott_NoObject);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tSettingsMenu, "MenuCode", "Menu Code", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tSettingsMenu, "ParentName", "Parent Name", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tSettingsMenu, "PredecessorName", "Predecessor Name", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tSettingsMenu, "SubMenu", "Is Sub Menu", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tSettingsMenu, "ImageIcon", "Icon Image Name", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tSettingsMenu, "Cannabis", "Is Cannabis", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tSettingsMenu, "MV_Grow", "Grow Micro Vertical", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tSettingsMenu, "MV_Processor", "Processor Micro Vertical", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tSettingsMenu, "MV_Distributor", "Distributor Micro Vertical", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tSettingsMenu, "MV_Retail", "Retail Micro Vertical", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0);
                oBar.Value++;

                //--------------- 
                //	Air Quality Log
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tAirLog, "Air Quality Log", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAirLog, "CreatedBy", "Created By", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAirLog, "CreatedOn", "Created On", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAirLog, "CO2", "Carbon Dioxide Levels", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAirLog, "Humidity", "Relative Humidity Percentage", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAirLog, "Temperature", "Recorded Temperature", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAirLog, "DegreeType", "C or F Degrees", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAirLog, "Warehouse", "Warehouse", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldGUID(NSC_DI.Globals.tAirLog);
                oBar.Value++;

                //--------------- 
                //	Feed Log
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tFeedLog, "Feed Log", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFeedLog, "PlantID", "Plant ID", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFeedLog, "ItemCode", "Item Code", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFeedLog, "DateCreated", "Date the Treatment was applied", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFeedLog, "Amount", "Amount of Additive applied", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFeedLog, "ADPPM", "PPM for Additive applied", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFeedLog, "ADTDS", "TDS for Additive applied", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFeedLog, "ADEC", "EC for Additive applied", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFeedLog, "TEMP", "Temp of Additive", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFeedLog, "PH", "PH of Additive", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldGUID(NSC_DI.Globals.tFeedLog);
                oBar.Value++;

                //--------------- 
                //	Filter Management Log
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tFilterLog, "Filter Management Log", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFilterLog, "ItemCode", "Filter Item", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFilterLog, "CreatedBy", "Created By", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFilterLog, "CreatedOn", "Created On", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFilterLog, "ReDate", "Replace On", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFilterLog, "Quantity", "Quantity", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tFilterLog, "Warehouse", "Warehouse", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldGUID(NSC_DI.Globals.tFilterLog);
                oBar.Value++;

                ////--------------- 
                ////	GreenBook Notes
                ////--------------- 
                //NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tGBNote, "GreenBook Notes", BoUTBTableType.bott_MasterData);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBNote, "CreatedBy", "Created By", BoFieldTypes.db_Alpha, EditSize: 250);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBNote, "CreatedOn", "Created On", BoFieldTypes.db_Date);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBNote, "Name", "Effect Name", BoFieldTypes.db_Memo);
                //NSC_DI.UTIL.UDO.AddUDO(NSC_DI.Globals.tGBNote);
                //oBar.Value++;

                ////--------------- 
                ////	GreenBook Tasks
                ////--------------- 
                //NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tGBTask, "GreenBook Tasks", BoUTBTableType.bott_MasterData);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTask, "CreatedBy", "Created By", BoFieldTypes.db_Alpha, EditSize: 250);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTask, "CreatedOn", "Created On", BoFieldTypes.db_Date);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTask, "AssignedTo", "Assigned To", BoFieldTypes.db_Alpha, EditSize: 250);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTask, "PlantID", "Plant ID", BoFieldTypes.db_Alpha, EditSize: 250);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTask, "DueDate", "Due Date", BoFieldTypes.db_Date);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTask, "IsComplete", "Is Complete", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN2, "0", true);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTask, "IsApproved", "Is Approved", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN2, "0", true);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTask, "Notify", "Notify Employee", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN2, "0", true);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTask, "NoApproval", "Approval Not Required", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN2, "0", true);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTask, "Task", "Task", BoFieldTypes.db_Memo);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTask, "TaskName", "Task Name", BoFieldTypes.db_Alpha, EditSize: 250);
                //NSC_DI.UTIL.UDO.AddUDO(NSC_DI.Globals.tGBTask);
                //oBar.Value++;

                ////--------------- 
                ////	GreenBook Task List
                ////--------------- 
                //NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tGBTaskList, "GreenBook Task List", BoUTBTableType.bott_MasterData);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTaskList, "ListName", "Effect Name", BoFieldTypes.db_Alpha, EditSize: 250);
                //NSC_DI.UTIL.UDO.AddUDO(NSC_DI.Globals.tGBTaskList);
                //oBar.Value++;

                ////--------------- 
                ////	GreenBook Task Notes
                ////--------------- 
                //// RHH - need to change to bott_NoObjectAutoIncrement
                //NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tGBTaskNote, "GreenBook Task Notes", BoUTBTableType.bott_NoObjectAutoIncrement);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTaskNote, "CreatedBy", "Created By", BoFieldTypes.db_Alpha, EditSize: 250);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTaskNote, "CreatedOn", "Created On", BoFieldTypes.db_Date);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTaskNote, "TaskID", "TaskID", BoFieldTypes.db_Alpha, EditSize: 250);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tGBTaskNote, "Note", "Note", BoFieldTypes.db_Memo);
                //NSC_DI.UTIL.UDO.AddUDO(NSC_DI.Globals.tGBTaskNote);
                //oBar.Value++;

                //--------------- 
                //	Harvest 
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tHarvest, "Harvest", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tHarvest, "PdONum", "Production Order Number", BoFieldTypes.db_Numeric, EditSize: 8);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tHarvest, "HarvestType", "Harvest Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1,
                    new Dictionary<string, string>()
                    {{ "F", "Full" }
                    ,{ "P", "Partial" }
                    ,{ "C", "Completed Partial" }
                    }, "F");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tHarvest, "CreatedBy", "Created By", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tHarvest, "CreatedOn", "Created On", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tHarvest, "ItemCode", "Item Code", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tHarvest, "ItemType", "Item Type- SN, Batch, None", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1,
                    new Dictionary<string, string>()
                    {{ "S", "Serialized" }
                    ,{ "B", "Batch" }
                    ,{ "N", "None" }
                    }, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tHarvest, "ID", "ID - SN or Batch Number", BoFieldTypes.db_Alpha, EditSize: 40);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tHarvest, "Quantity", "Quantity", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tHarvest, "ComplianceID", "Compliance ID", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tHarvest, "DistNumber", "Batch / Serial Number", BoFieldTypes.db_Alpha, EditSize: 36);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tHarvest, "NameSearch", "Name Search", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddKey("Name", NSC_DI.Globals.tHarvest, "NameSearch", BoYesNoEnum.tNO);
                oBar.Value++;

                //--------------- 
                //	Light Life Manager
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tLightLife, "Light Life Manager", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tLightLife, "CreatedBy", "Created By", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tLightLife, "CreatedOn", "Created On", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tLightLife, "LightItem", "Light Item", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tLightLife, "Quantity", "Quantity", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tLightLife, "ReplaceDate", "Replace Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tLightLife, "Warehouse", "Warehouse", BoFieldTypes.db_Alpha, EditSize: 250);
                oBar.Value++;

                //--------------- 
                //	Quarantine Types
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tQuarantineType, "Quarantine Type", BoUTBTableType.bott_NoObject);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tQuarantineType, "QuarState", "Quarantine State", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, new Dictionary<string, string>() { { "DEST", "Destruction" }, { "SICK", "Sick" } }, "DEST");
                oBar.Value++;

                //--------------- 
                //	Route Details
                //--------------- 
                if (NSC_DI.UTIL.UDO.TableExists(NSC_DI.Globals.tRouteDetls) && NSC_DI.UTIL.UDO.TableExists(NSC_DI.Globals.tRouteStops) == false)
                {
                    // tRouteStops does not exist. have to remove UDO and tRouteDetls (to clear table)
                    NSC_DI.UTIL.UDO.UDO_Remove(NSC_DI.Globals.tRouteDetls);
                    NSC_DI.UTIL.UDO.UserTableRemove(NSC_DI.Globals.tRouteDetls);
                    NSC_DI.UTIL.SQL.RunSQLQuery($"EXEC('UPDATE ODLN SET U_NSC_RouteID = NULL')");
                }

                if (NSC_DI.UTIL.UDO.GetFieldSubType(NSC_DI.Globals.tRouteDetls, "OutTime") != "T") NSC_DI.UTIL.UDO.FieldRemove(NSC_DI.Globals.tRouteDetls, "OutTime");
                if (NSC_DI.UTIL.UDO.GetFieldSubType(NSC_DI.Globals.tRouteDetls, "InTime") != "T") NSC_DI.UTIL.UDO.FieldRemove(NSC_DI.Globals.tRouteDetls, "InTime");
                if (NSC_DI.UTIL.UDO.GetFieldSubType(NSC_DI.Globals.tRouteDetls, "SchdOutTime") != "T") NSC_DI.UTIL.UDO.FieldRemove(NSC_DI.Globals.tRouteDetls, "SchdOutTime");
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tRouteDetls, "Route Details", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "DateCreated", "Date Created", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "Driver", "Driver", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "Driver2", "Driver 2", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "Vehicle", "Vehicle", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "Map", "Map", BoFieldTypes.db_Memo);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "OutDate", "Date Drive Leaves", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "OutTime", "Time Drive Leaves", BoFieldTypes.db_Date, BoFldSubTypes.st_Time);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "InDate", "Date Drive Returns", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "InTime", "Time Drive Returns", BoFieldTypes.db_Date, BoFldSubTypes.st_Time);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "Active", "Active", BoFieldTypes.db_Alpha, EditSize: 1, DefaultValue: "Y");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "CNT_DOWN_ON", "Count down start time", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "CNT_DOWN_DATE", "Count down start date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "EmpID", "EmployeeID", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "EmpID2", "EmployeeID 2", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "VehicleID", "VehicleID", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "ManifestID", "ManifestID", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "Room", "Room", BoFieldTypes.db_Alpha, EditSize: 20);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "DestSeqNum",    "Destination Sequence Number",    BoFieldTypes.db_Numeric);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1,
                    new Dictionary<string, string>() { { "H", "Hold" }, { "R", "Ready" }, { "O", "Out" }, { "P", "Processed" } }, "H", true);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "SchdOutDate", "Scheduled Departure Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "SchdOutTime", "Scheduled Departure Time", BoFieldTypes.db_Date, BoFldSubTypes.st_Time);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteDetls, "LicenseType", "License Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1,
                    new Dictionary<string, string>() { { "M", "Medical" }, { "R", "Recreational" } }, "M", true);

                NSC_DI.UTIL.UDO.FieldRemove(NSC_DI.Globals.tRouteDetls, "U_DestSeqNum");  // moved to the route stops table

                //NSC_DI.UTIL.UDO.AddUDO(NSC_DI.Globals.tRouteDetls);
                oBar.Value++;

                //--------------- 
                //	Route Stops
                //--------------- 
                if (NSC_DI.UTIL.UDO.GetFieldSubType(NSC_DI.Globals.tRouteDetls, "OutTime") != "T") NSC_DI.UTIL.UDO.FieldRemove(NSC_DI.Globals.tRouteStops, "ArriveTime");
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tRouteStops, "Route Stops", BoUTBTableType.bott_MasterDataLines);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteStops, "DestSeqNum", "Destination Sequence Number", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 8, null, "0");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteStops, "DocType", "Doc Type", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 8);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteStops, "DocEntry", "Doc Entry", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 8);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteStops, "DocNum", "Doc Num", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 8);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteStops, "DocNum", "Doc Type", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 8);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteStops, "ArriveDate", "Arrival Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteStops, "ArriveTime", "Arrival Time", BoFieldTypes.db_Date, BoFldSubTypes.st_Time);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteStops, "RouteText", "Route Text", BoFieldTypes.db_Memo);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteStops, "QRCode", "QR Code", BoFieldTypes.db_Alpha, BoFldSubTypes.st_Image, 5000);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRouteStops, "Map", "Map", BoFieldTypes.db_Memo);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tRouteDetls, NSC_DI.Globals.tRouteStops);

                //--------------- 
                //	Treatment Log
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tTreatLog, "Treatment Log", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTreatLog, "PlantID", "Plant ID", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTreatLog, "ItemCode", "Item Code", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTreatLog, "DateCreated", "Date the Treatment was Applied", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTreatLog, "Amount", "Amount of Pesticide Applied", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTreatLog, "PSPPM", "PPM for Pesticide Applied", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTreatLog, "PSTDS", "PSTDS for Pesticide Applied", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTreatLog, "PSEC", "PSEC for Pesticide Applied", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldGUID(NSC_DI.Globals.tTreatLog);
                oBar.Value++;

                //---------------
                //	Co. Vehicle Details
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tVehicle, "Co. Vehicle Details", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tVehicle, "Make", "Make", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tVehicle, "Model", "Model", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tVehicle, "Color", "Color", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tVehicle, "Plate", "Plate", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tVehicle, "Vin", "Vin", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tVehicle, "Year", "Year", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tVehicle, "Miles", "Miles", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tVehicle, "Active", "Active", BoFieldTypes.db_Alpha, EditSize: 1);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tVehicle, "CNT", "Count", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tVehicle, "StateID", "State ID", BoFieldTypes.db_Numeric, BoFldSubTypes.st_None, 8);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tVehicle, "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, syncStatus, "R", true);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tVehicle);
                oBar.Value++;

                //--------------- 
                //	Water Log
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tWaterLog, "Water Log", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWaterLog, "PlantID", "Plant ID", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWaterLog, "DateCreated", "Date the Water was Applied", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWaterLog, "Amount", "Amount of Water Applied", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWaterLog, "Measure", "Measurement for Water Applied", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWaterLog, "PPM", "PPM for Water Applied", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWaterLog, "PHLEVEL", "ph Level for Water Applied", BoFieldTypes.db_Memo);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWaterLog, "DIONIZED", "Dionized Water Applied", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWaterLog, "Details", "Details About Waste", BoFieldTypes.db_Memo);
                NSC_DI.UTIL.UDO.AddFieldGUID(NSC_DI.Globals.tWaterLog);
                oBar.Value++;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oBar);
            }
        }

        private static void CreateCustomCannabis(ref ProgressBar oBar)
        {
            // CUSTOM USER OBJECTS

            try
            {
                var YN0 = new Dictionary<string, string>() { { "Y", "Yes" }, { "N", "No" } };
                var syncStatus = new Dictionary<string, string>() { { "R", "Ready" }, { "H", "Hold" }, { "P", "Processed" } };

                if (Globals.IsCannabis == false) return;

                //-----------------------------------------------------------------------------------------------------------------------------------------
                //  CANNABIS
                //-----------------------------------------------------------------------------------------------------------------------------------------

                //--------------- 
                //	COMPLIANCE - Inventory Transfer Types
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tInvTypXFer, "Inventory Type - Transfer", BoUTBTableType.bott_NoObject);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tInvTypXFer, "QuarState", "Quarantine State", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, new Dictionary<string, string>() { { "DEST", "Destruction" }, { "SICK", "Sick" } }, "DEST");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tInvTypXFer, "CompCode", "Compliance Code", BoFieldTypes.db_Alpha, EditSize: 10);
                oBar.Value++;

                //--------------- 
                //	COMPLIANCE - Waste Types
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tWasteTypes, "Waste Types", BoUTBTableType.bott_NoObject);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWasteTypes, "CompCode", "Compliance Code", BoFieldTypes.db_Alpha, EditSize: 30);

                ////--------------- 
                ////	COMPLIANCE - Goods Issue Types - OBSOLETE
                ////--------------- 
                //NSC_DI.UTIL.UDO.AddUserTable(   NSC_DI.Globals.tInvTypGdsIsu, "Inventory Type - Goods Issue", BoUTBTableType.bott_NoObject);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tInvTypGdsIsu, "CompCode",   "Compliance Code",              BoFieldTypes.db_Alpha, EditSize: 10);
                //oBar.Value++;

                //--------------- 
                //	COMPLIANCE - Compliance Types
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tCompTypes, "Compliance Types", BoUTBTableType.bott_NoObject);
                oBar.Value++;

                //--------------- 
                //	COMPLIANCE - Destruction Types
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tDestructTypes, "Destruction Type", BoUTBTableType.bott_NoObject);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tDestructTypes, "QuarState", "Quarantine State", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10, new Dictionary<string, string>() { { "DEST", "Destruction" }, { "SICK", "Sick" } }, "DEST");
                oBar.Value++;

                //--------------- 
                //	COMPLIANCE - Adjustment Types  
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tInvAdjustTypes, "Adjustment Type", BoUTBTableType.bott_NoObject);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tInvAdjustTypes, "CompCode", "Compliance Code", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20);

                //--------------- 
                // COMPLIANCE - Remediation table
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tRemediate, "Remediation Log", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRemediate, "BatchID", "Batch ID", BoFieldTypes.db_Alpha, EditSize: 36);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRemediate, "BatchItem", "Batch Item Code", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRemediate, "MethodName", "Method Name", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRemediate, "Steps", "Steps", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRemediate, "Date", "Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRemediate, "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, syncStatus, "R", true);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRemediate, "SampleBatch", "Sample Batch", BoFieldTypes.db_Alpha, EditSize: 36);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tRemediate, "SampleItem", "Sample Item", BoFieldTypes.db_Alpha, EditSize: 50);

                //--------------- 
                //	Disease
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tDisease, "Diseases", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tDisease, "Name", "Disease Name", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tDisease);
                oBar.Value++;

                //--------------- 
                //	Disease And Effect
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tDiseEffect, "Disease And Effect", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tDiseEffect, "DiseaseCode", "Disease Id", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tDiseEffect, "EffectCode", "Effect Id", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tDiseEffect);
                oBar.Value++;

                //--------------- 
                //	Effect
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tEffect, "Effects", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tEffect, "Name", "Effect Name", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tEffect);
                oBar.Value++;

                //--------------- 
                //	Compliance Log
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tCompliance, "Compliance API Calls", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "DateCreated", "Date Created", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "Status", "Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, new Dictionary<string, string>() { { "S", "Success" }, { "F", "Failed" } }, "F");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "Action", "API Action", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 6, new Dictionary<string, string>() { { "Add", "Add" }, { "Update", "Update" }, { "Delete", "Delete" }, { "Get", "Get" } });
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "Reason", "Reason", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "CompKey", "Compliance Key", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "KeyType", "Key Type (Plant, Package)", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "ItemCode", "Item Code", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "DocNum", "Doc Num", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "DocType", "Doc Type (BoObjectTypes)", BoFieldTypes.db_Alpha, EditSize: 30);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "APICall", "API data sent", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "Response", "Response from API call", BoFieldTypes.db_Memo, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "Resolved", "Reason API Call was Resolved", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompliance, "TransID", "Transaction ID", BoFieldTypes.db_Alpha, EditSize: 32);
                NSC_DI.UTIL.UDO.AddFieldGUID(NSC_DI.Globals.tCompliance);
                oBar.Value++;

                //--------------- 
                //	Compounds
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tCompounds, "Compounds", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompounds, "Name", "Compound Name", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompounds, "Type", "Compound Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 4, new Dictionary<string, string>() { { "TERP", "Terpenoids" }, { "CANA", "Cannabinoids" } });
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tCompounds);
                oBar.Value++;

                //--------------- 
                //	Compound And Effect
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tCompEffect, "Compound And Effect", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompEffect, "CompoundCode", "Compound Id", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompEffect, "EffectCode", "Effect Id", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tCompEffect);
                oBar.Value++;

                //--------------- 
                //	Plants
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tPlants, "Plants", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPlants, "DateCreated", "Date Created", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPlants, "StrainID", "Strain ID", BoFieldTypes.db_Numeric);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPlants, "Serial", "Plant Serial", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPlants, "PlantID", "Item Code & Sys Number", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPlants, "DateLastWater", "Date Last Watered", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tPlants);
                oBar.Value++;

                //--------------- 
                //	Plant Journal
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tPlantJournal, "Plant Journal", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPlantJournal, "PlantID", "PlantID", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPlantJournal, "DateCreated", "Date Created", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPlantJournal, "EntryType", "Type of Journal Entry", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 250,
                    new Dictionary<string, string>() { { "Water", "Water" }, { "Feed", "Feed" }, { "Treat", "Treat" }, { "Transfer", "Transfer" }, { "Waste", "Waste" }, { "Manicure", "Manicure" }, { "Remarks", "Remarks" } });
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPlantJournal, "Serial", "Plant Serial", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPlantJournal, "Quantity", "Quantity", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPlantJournal, "ProdOrderNum", "Production Order Num", BoFieldTypes.db_Numeric);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPlantJournal, "Note", "Note", BoFieldTypes.db_Memo);
                NSC_DI.UTIL.UDO.AddFieldGUID(NSC_DI.Globals.tPlantJournal);
                oBar.Value++;

              
                //--------------- 
                //	Strain Types
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tStrainType, "Strain Type", BoUTBTableType.bott_NoObject);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrainType, "Prefix", "ItemCode Prefix", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 8);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrainType, "NextStrain", "Next Strain Number", BoFieldTypes.db_Numeric);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrainType, "Note", "Note", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 254);

                //--------------- 
                //	Strains
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tStrains, "Strains", BoUTBTableType.bott_MasterData);
                // 1st Create temp table to save current values (code and UDF)
                // 2nd Copy vals to temp table
                // Ensure that the data is copied over before continuing
                // 3rd Delete tStrains
                // 4th Create tStrains as bott_NoObject
                // 5th Copy from temp into new tStrains. Need to copy U_StrainName to the SAP provided Name field because Name must be unique val
                // 6th Make deleting temp table manually (serves as backup)
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "StrainName", "Strain Name", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "Type", "Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "SelfFlowering", "Self Flowering", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "BestUsedFor", "Best Used For", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "Grade", "Strain Grade", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "BestClimate", "Best Climate", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "PrjctTHC", "Projected THC", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "PrjctCBD", "Projected CBD", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "PrjctCBN", "Projected CBN", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "PrjctYield", "Projected Yield", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "BuzzType", "Buzz Type", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "BuzzLength", "Buzz Length", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "NextSN", "Next Strain Serial Number Number", BoFieldTypes.db_Numeric);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "NextBatch", "Next Strain Batch Number", BoFieldTypes.db_Numeric);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "Notes", "Notes", BoFieldTypes.db_Memo);

                //-----------------------
                // Effects are based on a scale of 1 - 10 (10 being the highest)

                // General Effects
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_euphoria", "General Effects - Euphoria", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_focused", "General Effects - Focused", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_happy", "General Effects - Happy", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_relaxed", "General Effects - Relaxed", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_uplifting", "General Effects - Uplifting", BoFieldTypes.db_Alpha, EditSize: 250);

                // Medical Effects
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_migrains", "Medical Effects - Migrains", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_insomnia", "Medical Effects - Insomnia", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_spasms", "Medical Effects - Muscle Spams", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_pain", "Medical Effects - Pain", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_stress", "Medical Effects - Stress", BoFieldTypes.db_Alpha, EditSize: 250);

                // Negative Effects
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_diziness", "Negative Effects - Dizziness", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_drymouth", "Negative Effects - Dry Mouth", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_dryeyes", "Negative Effects - Dry Eyes", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_headaches", "Negative Effects - Headaches", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ef_paranoid", "Negative Effects - Paranoid", BoFieldTypes.db_Alpha, EditSize: 250);

                // Projected Times Fields.
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "PrjctSeed", "Projected Seed Propagation", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "PrjctClone", "Projected Clone Propagation", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "PrjctFlower", "Projected Flowering time", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "PrjctVeg", "Projected Veg Time", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "PrjctDrying", "Projected Drying Time", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "PrjctManicure", "Projected Manicuring Time", BoFieldTypes.db_Alpha, EditSize: 20);

                // Terpenes
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpPiene", "Terpene - Pinene", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpBetaCary", "Terpene - Beta Caryophyllene", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpBorneol", "Terpene - Borneol", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpCaryOx", "Terpene - Caryophllene Oxide", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpCineol", "Terpene - Cineol", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpCitron", "Terpene - Citronellol", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpHumlene", "Terpene - Humulene", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpLimonene", "Terpene - Limonene", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpLinalool", "Terpene - Linalool", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpMyrcene", "Terpene - Myrcene", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpNerolidol", "Terpene - Nerolidol", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpPhytol", "Terpene - Phytol", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "TrpTerpinolene", "Terpene - Terpinolene", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);

                //-----------------------
                // Prone To

                // Pests
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "pt_spidermites", "Prone To - Spider Mites", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "pt_tripes", "Prone To - Tripes", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "pt_caterpillar", "Prone To - Catapillars", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "pt_symphilds", "Prone To - Symphilids", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "pt_fungalgnats", "Prone To - Fungal Gnats", BoFieldTypes.db_Alpha, EditSize: 250);

                // Fungi
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "pt_mildew", "Prone To - Mildew", BoFieldTypes.db_Alpha, EditSize: 250);

                // Diseases
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "pt_rootrot", "Prone To - Root Rot", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "pt_budrot", "Prone To - Bud Rot", BoFieldTypes.db_Alpha, EditSize: 250);

                // Nutr Defficiencies
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ptd_N", "Nutr Defi - (N)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ptd_P", "Nutr Defi - (P)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ptd_K", "Nutr Defi - (K)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ptd_Mg", "Nutr Defi - (Mg)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ptd_Ca", "Nutr Defi - (Ca)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ptd_S", "Nutr Defi - (S)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ptd_B", "Nutr Defi - (B)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ptd_Fe", "Nutr Defi - (Fe)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ptd_Zn", "Nutr Defi - (Zn)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "ptd_Mo", "Nutr Defi - (Mo)", BoFieldTypes.db_Alpha, EditSize: 250);

                //-----------------------
                // Nutr Prof
                // Vegetative
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutveg_N", "Nutr Prof - Veg - (N)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutveg_P", "Nutr Prof - Veg - (P)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutveg_K", "Nutr Prof - Veg - (K)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutveg_Mg", "Nutr Prof - Veg - (Mg)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutveg_Ca", "Nutr Prof - Veg - (Ca)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutveg_S", "Nutr Prof - Veg - (S)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutveg_B", "Nutr Prof - Veg - (B)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutveg_Fe", "Nutr Prof - Veg - (Fe)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutveg_Zn", "Nutr Prof - Veg - (Zn)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutveg_Mo", "Nutr Prof - Veg - (Mo)", BoFieldTypes.db_Alpha, EditSize: 250);

                // Flowering
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutflw_N", "Nutr Prof - Flowr - (N)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutflw_P", "Nutr Prof - Flowr - (P)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutflw_K", "Nutr Prof - Flowr - (K)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutflw_Mg", "Nutr Prof - Flowr - (Mg)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutflw_Ca", "Nutr Prof - Flowr - (Ca)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutflw_S", "Nutr Prof - Flowr - (S)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutflw_B", "Nutr Prof - Flowr - (B)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutflw_Fe", "Nutr Prof - Flowr - (Fe)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutflw_Zn", "Nutr Prof - Flowr - (Zn)", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "nutflw_Mo", "Nutr Prof - Flowr - (Mo)", BoFieldTypes.db_Alpha, EditSize: 250);


                // Media
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "md_img_icon", "Media Main Icon Image", BoFieldTypes.db_Alpha, BoFldSubTypes.st_Image, 250, null, null, false);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "Looks", "Looks", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "Taste", "Taste", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrains, "CmpTransID", "Compliance Transaction ID", BoFieldTypes.db_Alpha, EditSize: 32);
                //NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tStrains);


                // ---------
                // Mix Materials 12903
                // ---------
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tMixMats, "Mix Materials", BoUTBTableType.bott_NoObject);

                // ---------
                // Waste Method 12903
                // ---------
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tWasteMeth, "Waste Method", BoUTBTableType.bott_NoObject);

                //--------------- 
                //	Product Category -- 12449
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tProdCategory, "Product Category", BoUTBTableType.bott_NoObject);

                oBar.Value++;

                //State location type for METRC

                if (NSC_DI.UTIL.Settings.Version.GetCompliance().ToUpper() == "METRC")
                {
                    NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tStateLocType, "State Location Type", BoUTBTableType.bott_NoObjectAutoIncrement);
                    NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStateLocType, "Code", "Metrc val", BoFieldTypes.db_Alpha, EditSize: 50);
                    NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStateLocType, "Name", "Name", BoFieldTypes.db_Alpha, EditSize: 50);
                }

                //---------------
                //	Tests
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tTests, "Tests", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTests, "Name", "Test Name", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTests, "Vendor", "Test Owner", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTests, "Inactive", "Inactive", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, IsMandatory: true, DefaultValue: "N");
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tTests);
                oBar.Value++;

                //---------------
                //	Test Sample 
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tTestSamp, "Test Sample", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestSamp, "TestCode", "Test Code", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestSamp, "SampleCode", "Sample Code", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestSamp, "IsComplete", "IsComplete", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, IsMandatory: true, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestSamp, "RecipientType", "Recipient Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestSamp, "RecipientCode", "Recipient Code", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 15);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestSamp, "StateSyncStatus", "State Sync Status", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1,
                    new Dictionary<string, string>() { { "H", "Hold" }, { "R", "Ready" }, { "P", "Processed" } }, "R", true);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestSamp, "SampleItem", "Sample Item Code", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestSamp, "SampledItem", "Sampled Item Code", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestSamp, "SampledBatch", "Sampled Batch ID", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 50);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tTestSamp);
                oBar.Value++;

                //---------------
                //	Test And Test Field
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tTestTestField, "Test And Test Field", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestTestField, "TestCode", "Test Id", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestTestField, "TestFieldCode", "Test Field Id", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestTestField, "Group", "Group", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tTestTestField);
                oBar.Value++;

                //---------------
                //	Test And Compound
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tTestComp, "Test And Compound", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestComp, "TestCode", "Test Id", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestComp, "CompoundCode", "Compound Id", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tTestComp);
                oBar.Value++;

                //---------------
                //	Test Compound Result
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tTCResult, "Test Compound Result", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTCResult, "TestSampleCode", "Test Sample Code", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTCResult, "CompoundCode", "Compound Code", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTCResult, "Value", "Value of the Compound", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tTCResult);
                oBar.Value++;

                //---------------
                //	Test Field Result
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tTFResult, "Test Field Result", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTFResult, "TestSampleCode", "Test Sample Code", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTFResult, "FieldCode", "Test Field Code", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTFResult, "Value", "Value of the Field", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tTFResult);
                oBar.Value++;

                //---------------
                //	Test Fields
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tTestField, "Test Fields", BoUTBTableType.bott_MasterData);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestField, "Name", "Test Field Name", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.UDO_Add(NSC_DI.Globals.tTestField);
                oBar.Value++;

                ////---------------
                ////	Test And Sample
                ////--------------- 
                //NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tTestSamp, "Test And Sample", BoUTBTableType.bott_MasterData);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestSamp, "TestCode", "Test Id", BoFieldTypes.db_Alpha, EditSize: 10);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestSamp, "SampleCode", "Sample Id", BoFieldTypes.db_Alpha, EditSize: 254);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestSamp, "IsComplete", "Test Completion Indicator", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues:YN0, DefaultValue:"N");
                //NSC_DI.UTIL.UDO.AddUDO(NSC_DI.Globals.tTestSamp);
                //oBar.Value++;

                //---------------
                //Test And Sample ID Change LOG
                //---------------
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tTestChangeLog, "Sample ID Change Log", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestChangeLog, "OldID", "Old Sample ID", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestChangeLog, "NewID", "New Sample ID", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestChangeLog, "SamBatchID", "Sample Batch ID", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestChangeLog, "SamItemCode", "Sample Item Code", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestChangeLog, "UserID", "User ID", BoFieldTypes.db_Numeric);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestChangeLog, "UserName", "User Name", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestChangeLog, "DateCreated", "Date Changed", BoFieldTypes.db_Date);

                //--------------- 
                //	Waste Log
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tWasteLog, "Waste Log", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWasteLog, "PlantID", "Plant ID", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWasteLog, "DateCreated", "Date the Waste was Removed", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWasteLog, "Amount", "Amount of Waste Removed", BoFieldTypes.db_Float, BoFldSubTypes.st_Measurement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWasteLog, "Measure", "Measurement for Waste Removed", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWasteLog, "Reason", "Reason for Waste", BoFieldTypes.db_Alpha, EditSize: 250);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tWasteLog, "Details", "Details About Waste", BoFieldTypes.db_Memo);
                NSC_DI.UTIL.UDO.AddFieldGUID(NSC_DI.Globals.tWasteLog);
                oBar.Value++;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oBar);
            }
        }

        private static void ComplianceCreate(ref ProgressBar oBar)
        {
            // SAP AND CUSTOM OBJECTS

            try
            {
                if (Globals.IsCannabis == false) return;
                var YN0 = new Dictionary<string, string>() { { "Y", "Yes" }, { "N", "No" } };
                var syncStatus = new Dictionary<string, string>() { { "R", "Ready" }, { "H", "Hold" }, { "P", "Processed" } };

                Compliance_FillSettingsTable();

                // remove the following fields - they are added via iConnect
                //NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "VPOSInv",              "POS Invoice",                      BoFieldTypes.db_Alpha, EditSize: 50);
                //NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "VPOSUser",             "POS User Name",                    BoFieldTypes.db_Alpha, EditSize: 50);
                //NSC_DI.UTIL.UDO.AddFieldToTable("OCRT", "VPOSID",               "POS ID",                           BoFieldTypes.db_Alpha, EditSize: 50);
                //NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "VPOSID",               "POS ID",                           BoFieldTypes.db_Alpha, EditSize: 50);
                //NSC_DI.UTIL.UDO.AddFieldToTable("OITM", "VPOSSync",             "Sync to POS",                      BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.FieldRemove("ORDR", "VPOSInv");
                NSC_DI.UTIL.UDO.FieldRemove("ORDR", "VPOSUser");
                NSC_DI.UTIL.UDO.FieldRemove("OCRT", "VPOSID");
                NSC_DI.UTIL.UDO.FieldRemove("OITM", "VPOSID");
                NSC_DI.UTIL.UDO.FieldRemove("OITM", "VPOSSync");

                // Customer/Transaction data fields for State integrations
                NSC_DI.UTIL.UDO.AddFieldToTable("ORDR", "TransDate", "Transaction Date Time", BoFieldTypes.db_Date, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OINV", "CompID", "Compliance ID", BoFieldTypes.db_Alpha, EditSize: 64);

                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "StateUBI", "State Unique ID", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "Birthday", "Birthdate", BoFieldTypes.db_Date, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "DL", "Driver's Lic", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "StateStart", "Effective Start Date", BoFieldTypes.db_Date, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "StateEnd", "Effective End Date", BoFieldTypes.db_Date, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "StateRecPlant", "Recommended Plants", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRD", "StateRecSmokQuant", "Recommended Smokable Quantity", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity, 64);

                NSC_DI.UTIL.UDO.AddFieldToTable("OCPR", "CareGiverID", "CareGiver's ID", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCPR", "CareGivStateDL", "CareGiver Driver's License ID", BoFieldTypes.db_Alpha, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCPR", "StateStart", "Effective Start Date", BoFieldTypes.db_Date, EditSize: 64);
                NSC_DI.UTIL.UDO.AddFieldToTable("OCPR", "StateEnd", "Effective End Date", BoFieldTypes.db_Date, EditSize: 64);

                //NSC_DI.UTIL.UDO.AddFieldToTable("OCRG", "ComplianceID",         "Compliance ID",                     BoFieldTypes.db_Alpha, EditSize: 8, DefaultValue: "0");
                NSC_DI.UTIL.UDO.FieldRemove("OCRG", "ComplianceID");
                NSC_DI.UTIL.UDO.AddFieldToTable("OCRG", "CompTypeID", "Compliance Type ID", BoFieldTypes.db_Alpha, EditSize: 8, DefaultValue: "0");

                //--------------- 
                // TEST RESULTS 
                if (NSC_DI.UTIL.UDO.TableExists(NSC_DI.Globals.tTestResults) &&
                    NSC_DI.UTIL.SQL.GetValue<int>($"SELECT COUNT(*) FROM [@{NSC_DI.Globals.tTestResults}]", 0) == 0)
                    NSC_DI.UTIL.UDO.UserTableRemove(NSC_DI.Globals.tTestResults);
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tTestResults, "Test Results", BoUTBTableType.bott_NoObjectAutoIncrement);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "BatchID",         "Batch ID",             BoFieldTypes.db_Alpha, EditSize: 50);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "ItemCode",        "ItemCode",             BoFieldTypes.db_Alpha, EditSize: 50);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "ItemName",        "Item Name",            BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "LabTestID", "Lab Test ID", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "StateItemGrp", "State Item Group", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "LabNo", "Lab Number", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "SourceStateID", "SourceState ID", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "SampleStateID", "SampleState ID", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "OverallPassed", "Overall Passed", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "CompoundName", "Compound Name", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "CompoundValue", "Compound Value", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "ValuePassed", "Value Passed", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "ValueComment", "ValueComment", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "TestDate", "Test Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "ResultReleased", "Result Released", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "ReleaseDate", "Release Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tTestResults, "Source", "Source", BoFieldTypes.db_Alpha, EditSize: 25);

                // create the indexes
                NSC_DI.UTIL.UDO.AddKey("TestID", NSC_DI.Globals.tTestResults, "LabTestID");
                NSC_DI.UTIL.UDO.AddKey("SrcStID", NSC_DI.Globals.tTestResults, "SourceStateID");
                NSC_DI.UTIL.UDO.AddKey("SmpStID", NSC_DI.Globals.tTestResults, "SampleStateID");
                NSC_DI.UTIL.UDO.AddKey("CmpdNam", NSC_DI.Globals.tTestResults, "CompoundName");
                //--------------- 

                //--------------- 
                // ACTIVE PACKAGES
                if (NSC_DI.UTIL.UDO.TableExists(NSC_DI.Globals.tCompPackages) &&
                    NSC_DI.UTIL.SQL.GetValue<int>($"SELECT COUNT(*) FROM [@{NSC_DI.Globals.tCompPackages}]", 0) == 0)
                    NSC_DI.UTIL.UDO.UserTableRemove(NSC_DI.Globals.tCompPackages);
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tCompPackages, "Compliance Active Packages", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "PackageID", "Package ID", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "PackageLabel", "Package Label", BoFieldTypes.db_Alpha, EditSize: 36);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "SourceHarvest", "Source Harvest", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "LocationID", "Location ID", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "LocationName", "Location Name", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "Quantity", "Quantity", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "UOM_Code", "Unit of Measure Code", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "UOM_Name", "Unit of Measure Name", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ProductId", "Product ID", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ProductName", "Product Name", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ProductCategory", "Product Catagory", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "LicenseNo", "Item From Facility License Number", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "LicenseName", "Item From Facility Name", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ItemStrain", "Item Strain Name", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "CBDPercent", "Item Unit CBD Percent", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "CBDContent", "Item Unit CBD Content", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "CBDUOM", "Item Unit CBD Content Unit Of Measure Name", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "THCPercent", "Item Unit THC Percent", BoFieldTypes.db_Float, BoFldSubTypes.st_Percentage);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "THCContent", "Item Unit THC Content", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "THCUOM", "Item Unit THC Content Unit Of Measure Name", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ItemVolume", "Item Unit Volume", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "VolumeUOM", "Item Unit Volume Unit Of Measure Name", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ItemWeight", "Item Unit Weight", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "WeightUOM", "Item Unit Weight Unit Of Measure Name", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ServingSize", "Item Serving Size", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "DurationDays", "Item Supply Duration Days", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "UnitQuantity", "Item Unit Quantity", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "UnitUOM", "Item Unit Quantity Unit Of Measure Name", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "Note", "Note", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "PackagedDate", "Packaged Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "InitialLabStatus", "Initial Lab Testing Status", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "LabStatus", "Lab Testing Status", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "LabStatusDate", "Lab Testing Status Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ProductionBatch", "Is Production Batch", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "Y");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ProdBatchNo", "Production Batch Number", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "TradSample", "Is Trade Sample", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "Donation", "Is Donation", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "SourcePkgDonation", "Source Package Is Donation", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "TestingSample", "Is Testing Sample", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ProcessValSample", "Is Process Validation Testing Sample", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "RequiresRemediation", "Product Requires Remediation", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ContainsRemediated", "Contains Remediated Product", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "Y");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "RemediationDate", "Remediation Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ReceivedDate", "Recieved Date Time", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ManifestNo", "Received From Manifest Number", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "OriginLicenseNo", "Received From Facility License Number", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "OriginLicenseName", "Received From Facility Name", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "OnHold", "Is On Hold", BoFieldTypes.db_Alpha, EditSize: 1, ValidValues: YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "ArchivedDate", "Archived Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "FinishedDate", "Finished Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages, "LastModified", "Last Modified", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompPackages,  "LicenseNumber", "Calling License Number", BoFieldTypes.db_Alpha, EditSize: 25);

                // create the indexes
                NSC_DI.UTIL.UDO.AddKey("PkgID", NSC_DI.Globals.tCompPackages, "PackageID");
                NSC_DI.UTIL.UDO.AddKey("PkgLbl", NSC_DI.Globals.tCompPackages, "PackageLabel");
                NSC_DI.UTIL.UDO.AddKey("LocName", NSC_DI.Globals.tCompPackages, "LocationName");
                NSC_DI.UTIL.UDO.AddKey("PrdID", NSC_DI.Globals.tCompPackages, "ProductId");
                NSC_DI.UTIL.UDO.AddKey("PrdName", NSC_DI.Globals.tCompPackages, "ProductName");
                NSC_DI.UTIL.UDO.AddKey("PrdCat", NSC_DI.Globals.tCompPackages, "ProductCategory");
                NSC_DI.UTIL.UDO.AddKey("LicNum", NSC_DI.Globals.tCompPackages, "LIcenseNo");
                NSC_DI.UTIL.UDO.AddKey("ItmStrn", NSC_DI.Globals.tCompPackages, "ItemStrain");

                //--------------- 
                // HARVEST
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tCompHarvest, "Compliance Active Harvest", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "HarvestID", "Harvest ID", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "HarvestName", "Harvest Name", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "DryingWhsNam", "Dry Whs Name", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "DryingWhsCod", "Dry Whs Code", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "TotalWaste", "Total Waste", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "PlantCount", "Plant Count", BoFieldTypes.db_Numeric);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "TotalWet", "Total Wet", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "CurrentWeight", "Current Weight", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "TotalRestWeight", "Total Restore Weight", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "TotPackWeight", "Total Package Weight", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "PackCount", "Package Count", BoFieldTypes.db_Numeric);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "UOM_Name", "Unit of Measure Name", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "HarvestDate", "Harvest Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "FinishedDate", "Finished Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "LabTestState", "Lab Test State", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "LabTestDate", "Lab Test Date", BoFieldTypes.db_Date);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "LastModified", "Last Modified", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "LicenseNumber", "Calling License Number", BoFieldTypes.db_Alpha, EditSize: 25);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tCompHarvest, "Finished", "Finished", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1,
                    YN0, "N", true); // 11710

                // create the indexes
                NSC_DI.UTIL.UDO.AddKey("HvstID", NSC_DI.Globals.tCompHarvest, "HarvestID");
                NSC_DI.UTIL.UDO.AddKey("HvstNam", NSC_DI.Globals.tCompHarvest, "HarvestName");
                NSC_DI.UTIL.UDO.AddKey("HvstDat", NSC_DI.Globals.tCompHarvest, "HarvestDate");
                
                //--------------- 
                // add the BP Group values
                Compliance_Fill_BP_Groups();

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oBar);
            }
        }

        private static void Compliance_FillSettingsTable()
        {
            try
            {
                if (Globals.IsCannabis == false) return;
                NSC_DI.UTIL.Settings.Value.Set("Compliance API Location ID", "", "X-Location-Id", false);
                NSC_DI.UTIL.Settings.Value.Set("Compliance API Key", "", "X-Api-Key", false);
                NSC_DI.UTIL.Settings.Value.Set("Compliance API URL", @"http://compliance-api.data.green/nbs-dev", "DataGreen API URL", false);    // want to make sure this entry gets added.

                // no longer used. it is now on the warehouse.
                //NSC_DI.UTIL.Settings.Value.Set("Compliance API Key", "", "X-Api-Key", false);            
                NSC_DI.UTIL.Settings.Delete("Compliance API Location ID");

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oBar);
            }
        }

        private static void Compliance_Fill_BP_Groups()
        {
            BusinessPartnerGroups oGrp = null;
            var successfulErrors = new int[] { 0, -2035 };

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("AddedBPGroups") == "Y") return;

                var dtValues = new System.Data.DataTable();

                dtValues.Columns.Add("GroupName", typeof(string));
                dtValues.Columns.Add("CompID", typeof(string));

                var compliance = NSC_DI.UTIL.Settings.Version.GetCompliance();

                switch (compliance)
                {
                    case "METRC":   // Maryland for now
                        dtValues.Rows.Add(new object[] { "Customers", "1" });
                        dtValues.Rows.Add(new object[] { "Patients", "2" });
                        dtValues.Rows.Add(new object[] { "Retailers", "0" });
                        dtValues.Rows.Add(new object[] { "Wholesale", "0" });
                        dtValues.Rows.Add(new object[] { "Caregiver", "3" });
                        dtValues.Rows.Add(new object[] { "External Patient", "4" });
                        break;

                    case "BIOTRACK":
                    case "NONE":
                    case "":

                    default:
                        dtValues.Rows.Add(new object[] { "Customers", "0" });
                        dtValues.Rows.Add(new object[] { "Patients", "0" });
                        dtValues.Rows.Add(new object[] { "Retailers", "0" });
                        dtValues.Rows.Add(new object[] { "Wholesale", "0" });
                        break;
                }

                foreach (System.Data.DataRow dr in dtValues.Rows)
                {
                    oGrp = Globals.oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartnerGroups);
                    oGrp.Name = dr["GroupName"].ToString();
                    if (successfulErrors.Contains(oGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                    // groups might already exist, so have to update the UDF
                    if (oGrp.GetByKey(NSC_DI.UTIL.SQL.GetValue<int>($"SELECT GroupCode FROM OCRG WHERE GroupName = '{oGrp.Name}'", 0)) == false)
                        throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                    oGrp.UserFields.Fields.Item("U_NSC_CompTypeID").Value = dr["CompID"].ToString();
                    if (oGrp.Update() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }

                NSC_DI.UTIL.Settings.Value.Set("AddedBPGroups", "Y", "Indicates that all Business Partner Groups have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGrp);
                GC.Collect();
            }
        }

        private static void CreateAuto(ref ProgressBar oBar)
        {
            //  AUTO TABLES

            try
            {
                var YN0 = new Dictionary<string, string>() { { "Y", "Yes" }, { "N", "No" } };

                //--------------- 
                //	Auto Item
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tAutoItem, "Auto Item", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "AddOrder", "Add Order", BoFieldTypes.db_Numeric);
                //NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "Active",			"Determins if this Item is to be created", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "ItemCode", "Item Code", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "ItemName", "Item Name", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "ItemGroupCode", "Item Group Code", BoFieldTypes.db_Numeric);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "ManageSN", "Manage Serial Numbers", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "ManageBatch", "Manage Batch Numbers", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "HasBOM", "Has Bill Of Materials", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "InvItem", "Inventory Item", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "PurItem", "Purchase Item", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "SalItem", "Sales Item", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "UnitPrice", "Unit Price", BoFieldTypes.db_Float, BoFldSubTypes.st_Price);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "DefaultWH", "Default Warehouse", BoFieldTypes.db_Alpha, EditSize: 8);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "Sampleable", "Sampleable", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "Batchable", "Batchable", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "Processable", "Processable", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "ProcessType", "Process Type", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "PropertyName", "Item Property Name", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "MV_Grow", "Is part of Micro Vertical GROW", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "MV_Processor", "Is part of Micro Vertical PROCESSOR", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "MV_Distributor", "Is part of Micro Vertical DISTRIBUTOR", BoFieldTypes.db_Alpha, EditSize: 50);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItem, "MV_Retail", "Is part of Micro Vertical RETAIL", BoFieldTypes.db_Alpha, EditSize: 50);
                oBar.Value++;

                //--------------- 
                //	Auto Item BOM
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tAutoItemBOM, "Auto BOM", BoUTBTableType.bott_NoObject);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItemBOM, "ParentItem", "Parent Item Code", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItemBOM, "ItemCode", "Item Code", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItemBOM, "Group", "Group", BoFieldTypes.db_Alpha, EditSize: 100);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItemBOM, "Quantity", "Quantity", BoFieldTypes.db_Float, BoFldSubTypes.st_Quantity);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItemBOM, "IssueMethod", "Issue Method", BoFieldTypes.db_Alpha, EditSize: 20);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoItemBOM, "SourceWH", "Source Warehouse Code", BoFieldTypes.db_Alpha, EditSize: 8);
                oBar.Value++;

                //--------------- 
                //	 Production Order Phases
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tPDOPhase, "Auto Production Order Phases", BoUTBTableType.bott_NoObject);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPDOPhase, "CurrentPhase", "Current Phase", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPDOPhase, "NextPhase", "Next Phase", BoFieldTypes.db_Alpha, EditSize: 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPDOPhase, "CanWater", "Can Water", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPDOPhase, "CanFeed", "Can Feed", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPDOPhase, "CanTreat", "Can Treat", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPDOPhase, "CanPrune", "Can Prune", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, DefaultValue: "N");
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tPDOPhase, "CanQuarantine", "Can Quarantine", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 1, YN0, DefaultValue: "N");
                oBar.Value++;

                //--------------- 
                //	 Strain, Item, SN, and Batch structre
                //--------------- 
                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tAutoStruct, "Auto Structres", BoUTBTableType.bott_NoObject);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoStruct, "Type", "Type", BoFieldTypes.db_Alpha, EditSize: 32);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoStruct, "Component", "Component", BoFieldTypes.db_Alpha, EditSize: 32);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoStruct, "Position", "Position", BoFieldTypes.db_Numeric);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoStruct, "Prefix", "Prefix", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tAutoStruct, "Suffix", "Suffix", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 10);
                oBar.Value++;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oBar);
            }
        }

        private static void CreateSettings()
        {
            // SETTINGS TABLE

            try
            {
                var YN0 = new Dictionary<string, string>() { { "Y", "Yes" }, { "N", "No" } };

                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tSettings, "NavSol Settings", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tSettings, "Value", "Setting Value", BoFieldTypes.db_Alpha, EditSize: 128);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tSettings, "Description", "Description", BoFieldTypes.db_Alpha, EditSize: 254, DefaultValue: "");

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oBar);
            }
        }

        private static void CreateUserOptions()
        {
            // USER OPTIONS TABLE

            try
            {
                var YN0 = new Dictionary<string, string>() { { "Y", "Yes" }, { "N", "No" } };

                NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tUserOptions, "NavSol User Options", BoUTBTableType.bott_NoObjectAutoIncrement);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tUserOptions, "Description", "Description", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tUserOptions, "Value1", "Value 1", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tUserOptions, "Value2", "Value 2", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tUserOptions, "Value3", "Value 3", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tUserOptions, "Value4", "Value 4", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tUserOptions, "Value5", "Value 5", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tUserOptions, "Value6", "Value 6", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tUserOptions, "Value7", "Value 7", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tUserOptions, "Value8", "Value 8", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tUserOptions, "Value9", "Value 9", BoFieldTypes.db_Alpha, EditSize: 254);
                NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tUserOptions, "Value10", "Value 10", BoFieldTypes.db_Alpha, EditSize: 254);

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oBar);
            }
        }

        private static void FillitemProperties()
        {
            // ITEM PRORERTY

            try
            {
                NSC_DI.SAP.Items.Property.Create(37, "Package Clone");
                NSC_DI.SAP.Items.Property.Create(36, "Metrc Tag");
                if (NSC_DI.UTIL.Settings.Value.Get("CreateItemProperties") == "Y") return;

                // get all of the properties from the settings table
                var dt = NSC_DI.UTIL.SQL.DataTable("SELECT Name, U_Value FROM [@NSC_SETTINGS] WHERE Name LIKE '" + NSC_DI.Globals.SettingsItemPropPrefix + "%'");
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    var desc = row["Name"].ToString().Substring(NSC_DI.Globals.SettingsItemPropPrefix.Length);
                    var num = row["U_Value"].ToString();
                    if (string.IsNullOrEmpty(desc) || string.IsNullOrEmpty(num)) continue;

                    NSC_DI.SAP.Items.Property.Create(Convert.ToInt32(num), desc);
                }

                NSC_DI.UTIL.Settings.Value.Set("CreateItemProperties", "Y", "Indicates that all Item Properties have been created.", true);

                //NSC_DI.SAP.Items.Property.Create(NSC_DI.Globals.ItemPropTemplate, "Template");
                //NSC_DI.SAP.Items.Property.Create(NSC_DI.Globals.ItemPropClone,	  "Clone");
                //NSC_DI.SAP.Items.Property.Create(NSC_DI.Globals.ItemPropPlant,	  "Plant");
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oBar);
            }
        }

        private static void FillTestSampleUDFs()
        {
            // fill new UDFs only

            try
            {
                var sql = $@"SELECT TOP 1 ISNULL(U_SampleItem, '') FROM [@NSC_TEST_SAMPLE] WHERE ISNULL(U_SampleItem, '') <> ''";
                if (NSC_DI.UTIL.SQL.GetValue<string>(sql, "") != "") return;

                sql = $@"EXEC('
IF OBJECT_ID(''tempdb..##TEMP'') IS NOT NULL DROP TABLE ##TEMP

SELECT DISTINCT
       OBTN.ItemCode, OITM.ItemName, OBTN.DistNumber, 
       OBTN.U_NSC_LabTestID AS TestID, 
	   OBTN_1.DistNumber AS UnderLot, OBTN_1.itemName AS UnderName, 
       OITM_1.ItemCode AS UnderItem, 
	   [@NSC_TEST_SAMPLE].U_SampleItem, [@NSC_TEST_SAMPLE].U_SampledItem, [@NSC_TEST_SAMPLE].U_SampledBatch
  INTO ##TEMP
  FROM [@NSC_TEST_SAMPLE]
  LEFT JOIN OBTN ON OBTN.DistNumber = [@NSC_TEST_SAMPLE].U_SampleCode 
 INNER JOIN OITM ON OBTN.ItemCode = OITM.ItemCode 
 INNER JOIN OBTQ ON OBTN.ItemCode = OBTQ.ItemCode AND OBTN.SysNumber = OBTQ.SysNumber 
 INNER JOIN OWHS ON OWHS.WhsCode = OBTQ.WhsCode 
  LEFT JOIN OBTN AS OBTN_1 ON OBTN.U_NSC_LabTestID = OBTN_1.U_NSC_LabTestID AND OBTN.AbsEntry <> OBTN_1.AbsEntry
  LEFT JOIN OBTQ AS OBTQ_1 ON OBTN_1.ItemCode = OBTQ_1.ItemCode AND OBTN_1.SysNumber = OBTQ_1.SysNumber 
  LEFT JOIN OITM AS OITM_1 ON OBTQ_1.ItemCode = OITM_1.ItemCode
 WHERE ISNULL(OITM_1.QryGroup53, '''') <> ''Y'' AND  ISNULL(OITM_1.QryGroup40, '''') <> ''Y'' 

 UPDATE [@NSC_TEST_SAMPLE] SET U_SampleItem =  
                 (SELECT TOP 1 ##TEMP.ItemCode
                   FROM ##TEMP
                   WHERE ##TEMP.DistNumber = [@NSC_TEST_SAMPLE].U_SampleCode )

 UPDATE [@NSC_TEST_SAMPLE] SET U_SampledItem =  
                 (SELECT TOP 1 ##TEMP.UnderItem
                   FROM ##TEMP
                   WHERE ##TEMP.DistNumber = [@NSC_TEST_SAMPLE].U_SampleCode)

 UPDATE [@NSC_TEST_SAMPLE] SET U_SampledBatch =  
                 (SELECT TOP 1 ##TEMP.UnderLot
                   FROM ##TEMP
                   WHERE ##TEMP.DistNumber = [@NSC_TEST_SAMPLE].U_SampleCode)')";

                NSC_DI.UTIL.SQL.RunSQLQuery(sql);

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oBar);
            }
        }

        private static void FillSettingsTable()
        {
            NSC_DI.UTIL.Settings.Value.Set("Use SQL Transactions - COMPLETE", "N", "Use SQL Transactions - COMPLETE", false);
            NSC_DI.UTIL.Settings.SetDesc("Use SQL Transactions - COMPLETE", "Use SQL Transactions - SO COMPLETE button");

            NSC_DI.UTIL.Settings.Value.Set("Next Number re-tries", "2000", "The number of times to attempt getting the Next Number.", false);
          
            if (NSC_DI.UTIL.Settings.Value.Get("Fill Settings Table") == "N") return;

            NSC_DI.UTIL.Settings.Value.Set("Inter-Company", "N", "Whether or not this company is part of a larger Enterprise. Values: Y or N ", false);
            NSC_DI.UTIL.Settings.Value.Set("Serialize Stage", "VEG", "Indicates the stage the plants are given Serialized IDs. There are three options Vegitative, Flower, and None  Values: VEG or FLO or Left Blank", false);           // want to make sure this entry gets added.

            NSC_DI.UTIL.Settings.Value.Set("GoogleMapsUri", @"https://maps.googleapis.com/maps/api/staticmap?size=550x380&format=jpg&maptype=roadmap", "", false);
            NSC_DI.UTIL.Settings.Value.Set("GoogleApiKey", "AIzaSyCInjLvnWoeItGohlFsPI2uMvWvokQxUYQ", "", false, true);
            NSC_DI.UTIL.Settings.Value.Set("StarinBOMerrorsSTOP", "N", "(Y or N) Stop creating Items and BOM and remove any partial entries if HasBOM is 'Y' but there is no BOM specifiecd.", false);
            NSC_DI.UTIL.Settings.Value.Set("IsCannabis", "Y", "Is Cannabis Tenant", false);
            NSC_DI.UTIL.Settings.Value.Set("MV_Grow", "Y", "Micro Vertical Grow", false);
            NSC_DI.UTIL.Settings.Value.Set("MV_Processor", "Y", "Micro Vertical Processor", false);
            NSC_DI.UTIL.Settings.Value.Set("MV_Distributor", "Y", "Micro Vertical Distributor", false);
            NSC_DI.UTIL.Settings.Value.Set("MV_Retail", "Y", "Micro Vertical Retail", false);
            NSC_DI.UTIL.Settings.Value.Set("WetCannabisProcess", "N", "Use Wet Cannabis Process", false);
            //NSC_DI.UTIL.Settings.Value.Set("Medical",					"Y", "", false);
            //NSC_DI.UTIL.Settings.Value.Set("Recreational",				"Y", "", false);
            NSC_DI.UTIL.Settings.Value.Set("Allow Deli Weight", "Y", "", false);
            NSC_DI.UTIL.Settings.Value.Set("Serialize Plants", "Y", "", false);

            NSC_DI.UTIL.Settings.Value.Set("State Plant ID Source", "SCAN", "The source of the ID:  'SCAN' for METRC. 'API' for others, or NONE for California.", false);
            NSC_DI.UTIL.Settings.Value.Set("Sub-Batch Required", "Y", "Indicates if the State requires Sub-Bathes", false);
            //NSC_DI.UTIL.Settings.Value.Set("State Compliance",          "METRC", "State Compliance System", false);
            NSC_DI.UTIL.Settings.Value.Set("Use Template Items", "Y", "", false);


            // Item Properties
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Template", "64", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Special", "38", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Cannabis Plant", "39", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Cannabis Waste", "40", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Clone", "41", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Concentrate Bulk", "42", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Concentrate Lot QA", "43", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Dry Cannabis", "44", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Dry Unbatched Cannabis", "45", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Dry Unbatched Trim", "46", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Edible Bulk", "47", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Flower Lot QA", "48", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Frozen Unbatched Flower", "49", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Frozen Unbatched Trim", "50", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Ground Cannabis", "51", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Pre Roll", "52", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Packaged Good", "53", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Plant Tissue", "54", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Sample", "55", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Seed", "56", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Trim Lot QA", "57", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Unbatched Cannabis", "58", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Unbatched Ground Cannabis", "59", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Unbatched Trim", "60", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Wet Cannabis", "61", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Wet Trim", "62", "Item Property Number", false);
            NSC_DI.UTIL.Settings.Value.Set(NSC_DI.Globals.SettingsItemPropPrefix + "Wet Unbatched Cannabis", "63", "Item Property Number", false);

            NSC_DI.UTIL.Settings.Value.Set("Fill Settings Table", "N", "Indicates that the Settings table needs to be loaded.", true);

            //NSC_DI.UTIL.Options.Value.SetSingle("Batch Plants", "To_Veg", 1, false, "Options are: 'All' - this will use batches all the way through the process, 'To_Veg'-Serialized Plants at Vegetative State, 'To_Flower'-Serialized Plants at the Flowering stage");
        }

        private static void FillUserOptionsTable()
        {
            NSC_DI.UTIL.Options.Value.SetSingle("RouteTimer", "24", 1, false, "Route timer relevant for Delivery Management. Value1 is the duration (in hours).");
            NSC_DI.UTIL.Options.Value.SetSingle("Max Lot Size - BUD", "5", 1, false, "Maximum Lot Size fro Bud.");
            NSC_DI.UTIL.Options.Value.SetSingle("Max Lot Size - TRIM", "5", 1, false, "Maximum Lot Size fro Trim.");
            NSC_DI.UTIL.Options.Value.SetSingle("QuarantineTimer", "72", 1, false, "Quantime timer relevant for Quarantine destruction. Value1 is the duration (in hours).");
            NSC_DI.UTIL.Options.Value.SetSingle("Harvest Wizard Single Select Only", "N", 1, false, "Allows you to select only one plant in the grid in the harvest wizard");
            NSC_DI.UTIL.Options.Value.SetSingle("Packaging PP - Issue all", "Y", 1, false, "Automatically Issue all Manual items.");
            NSC_DI.UTIL.Options.Value.SetSingle("Wet Processing", "N", 1, false, "Client does wet cannabis processing.");
            NSC_DI.UTIL.Options.Value.SetSingle("Crop Cycle Management", "N", 1, false, "Client manages Crop Cycles.");
            NSC_DI.UTIL.Options.Value.SetSingle("Clone Batches", "N", 1, false, "Include Batches in Clone Wizard.");
            NSC_DI.UTIL.Options.Value.SetSingle("User Can Set Delivery Date", "N", 1, false, "User set Delivery Date on Sales Order sets Delivery Date on Delivery document.");
            NSC_DI.UTIL.Options.Value.SetSingle("Use Test Results Table for Sample Bank", "N", 1, false, "Use Test Results Table to populate the completion tab on the Sample Bank");
        }

        private static void FillSettingsMenuTableNew()
        {
            UserTable oUDT = null;
            try
            {
                FillSettingsMenuTable_Update("Processing and Packaging Yields", "Packaging Yields");
                FillSettingsMenuTable("107030", "Processing Yields", "NSC_PROCESSING_PV", "Processing / Packaging", "", "N", "", "Y", "N", "Y", "Y", "Y", true);

                // additional menu items
                FillSettingsMenuTable("", "Batch Serial Tracking", Globals.SAP_PartnerCode + "_BATCHSERIALTRACKING", "Inventory", "Pick and Pack", "N");

                // new form for updating a closed Production Order
                FillSettingsMenuTable("107005", "Create Sub Batch", "NSC_SUB-BATCH", "Processing / Packaging", "", "N", "", "Y", "N", "Y", "Y", "Y");

                // COMPLIANCE
                FillSettingsMenuTable("109000", "Compliance", "NSC_MENU_CMP", "Main Menu", "Reports", "Y", "MainLogo-resized.bmp", "Y", "Y", "Y", "Y", "Y", true);
                FillSettingsMenuTable("109010", "State License and Credentials", "NSC_STATELIC", "Compliance", "", "N", "", "Y", "Y", "Y", "Y", "Y", true);
                FillSettingsMenuTable("109020", "Compliance Monitor", "NSC_COMPLIANCEMON", "Compliance", "", "N", "", "Y", "Y", "Y", "Y", "Y", true);
                FillSettingsMenuTable("109030", "Inventory Resolution", "NSC_INV_RES", "Compliance", "", "N", "", "Y", "Y", "Y", "Y", "Y", true);
                FillSettingsMenuTable("109040", "Dashboard", "NSC_DASHBOARD", "Compliance", "", "N", "", "Y", "Y", "Y", "Y", "Y", true);

                //--------------------------------
                // move to a different sub menu
                //FillSettingsMenuTable("103060", "Production Update", "NSC_PROD_ORDER_UPDATE", "Production / Garden", "Harvest Wizard", "N", "", "N");
                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tSettingsMenu);
                var WPYK = oUDT.GetByKey("108042");
                var WPYN = oUDT.Name == "Wet Processing Yeilds";
                if (oUDT.GetByKey("108042") && oUDT.Name == "Wet Processing Yeilds")
                {
                    oUDT.Name = "Wet Processing Yields";
                    oUDT.Update();
                }

                if (oUDT.GetByKey("103060") && oUDT.Name == "Production Update") oUDT.Remove();                
                FillSettingsMenuTable("109034", "Production Update", "NSC_PROD_ORDER_UPDATE", "Compliance", "Inventory Resolution", "N", "", "N", "N", "Y", "Y", "Y", true);

                NSC_DI.UTIL.Settings.Value.Set("SettingMenuLoaded", "Y", "Indicates that all Settings Menus have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        private static void FillSettingsMenuTableAll()
        {
            UserTable oUDT = null;
            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("SettingMenuLoaded") == "Y") return;
                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tSettingsMenu);

                var dtSettingsMenu = GetSettingsMenuTable();
                foreach (var dr in dtSettingsMenu.Select())
                {
                    // see if the menu item has already been added
                    var c = dr["Code"] as string;
                    var n = dr["Name"] as string;
                    if (NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Code FROM [@{NSC_DI.Globals.tSettingsMenu}] WHERE Code = '{c}'", "") != "") continue;
                    if (NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Name FROM [@{NSC_DI.Globals.tSettingsMenu}] WHERE Name = '{n}'", "") != "") continue;
                    oUDT.Code = dr["Code"] as string;
                    oUDT.Name = dr["Name"] as string;
                    oUDT.UserFields.Fields.Item("U_MenuCode").Value = dr["MenuCode"] as string;
                    oUDT.UserFields.Fields.Item("U_ParentName").Value = dr["ParentName"] as string;
                    oUDT.UserFields.Fields.Item("U_PredecessorName").Value = dr["PredecessorName"] as string;
                    oUDT.UserFields.Fields.Item("U_SubMenu").Value = dr["SubMenu"] as string;
                    oUDT.UserFields.Fields.Item("U_ImageIcon").Value = dr["ImageIcon"] as string;
                    oUDT.UserFields.Fields.Item("U_Cannabis").Value = dr["Cannabis"] as string;
                    oUDT.UserFields.Fields.Item("U_MV_Retail").Value = dr["Retail"] as string;
                    oUDT.UserFields.Fields.Item("U_MV_Distributor").Value = dr["Distributor"] as string;
                    oUDT.UserFields.Fields.Item("U_MV_Grow").Value = dr["Grow"] as string;
                    oUDT.UserFields.Fields.Item("U_MV_Processor").Value = dr["Processor"] as string;
                    if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }

                NSC_DI.UTIL.Settings.Value.Set("SettingMenuLoaded", "Y", "Indicates that all Settings Menus have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        public static void UpdateSettingsMenuTable(string pOldMenuCode, string pCode = null, string pName = null, string pMenuCode = null, string pParentName = null, string pPredecessorName = null, string pSubMenu = null, 
                                                    string pImageIcon = null, string pCannabis = null, string pRetail = null, string pDistributor = null, string pGrow = null, string pProcessor = null, bool pIgnoreError = true)
        {
            UserTable oUDT = null;
            try
            {
                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tSettingsMenu);

                // if can't find the old menu code or the code, then just return
                pCode = pCode ?? NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Code FROM [@{NSC_DI.Globals.tSettingsMenu}] WHERE U_MenuCode = '{pOldMenuCode}'", "");
                if (NSC_DI.UTIL.Strings.Empty(pCode)) return;
                if (oUDT.GetByKey(pCode) == false) return; 
                
                oUDT.Name = pName;
                oUDT.UserFields.Fields.Item("U_MenuCode").Value = pMenuCode;
                if (pParentName != null)        oUDT.UserFields.Fields.Item("U_ParentName").Value       = pParentName;
                if (pPredecessorName != null)   oUDT.UserFields.Fields.Item("U_PredecessorName").Value  = pPredecessorName;
                if (pSubMenu != null)           oUDT.UserFields.Fields.Item("U_SubMenu").Value          = pSubMenu;
                if (pImageIcon != null)         oUDT.UserFields.Fields.Item("U_ImageIcon").Value        = pImageIcon;
                if (pCannabis != null)          oUDT.UserFields.Fields.Item("U_Cannabis").Value         = pCannabis;
                if (pRetail != null)            oUDT.UserFields.Fields.Item("U_MV_Retail").Value        = pRetail;
                if (pDistributor != null)       oUDT.UserFields.Fields.Item("U_MV_Distributor").Value   = pDistributor;
                if (pGrow != null)              oUDT.UserFields.Fields.Item("U_MV_Grow").Value          = pGrow;
                if (pProcessor != null)         oUDT.UserFields.Fields.Item("U_MV_Processor").Value     = pProcessor;

                if (oUDT.Update() != 0 && pIgnoreError == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        public static void FillSettingsMenuTable(string pCode, string pName, string pMenuCode, string pParentName, string pPredecessorName = "", string pSubMenu = "", string pImageIcon = "",
                                                 string pCannabis = "Y", string pRetail = "Y", string pDistributor = "Y", string pGrow = "Y", string pProcessor = "Y", bool pIgnoreError = false)
        {
            UserTable oUDT = null;
            try
            {
                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tSettingsMenu);
                pCode = string.IsNullOrEmpty(pCode?.Trim()) ? NSC_DI.UTIL.SQL.GetNextAvailableCodeForItem(NSC_DI.Globals.tSettingsMenu, "Code", 10) : pCode; // Whitespace-Change
                // see if the menu item has already been added
                if (NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Code FROM [@{NSC_DI.Globals.tSettingsMenu}] WHERE Code = '{pCode}'", "") != "") return;
                if (NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Name FROM [@{NSC_DI.Globals.tSettingsMenu}] WHERE Name = '{pName}'", "") != "") return;
                oUDT.Code = pCode;
                oUDT.Name = pName;
                oUDT.UserFields.Fields.Item("U_MenuCode").Value         = pMenuCode;
                oUDT.UserFields.Fields.Item("U_ParentName").Value       = pParentName;
                oUDT.UserFields.Fields.Item("U_PredecessorName").Value  = pPredecessorName;
                oUDT.UserFields.Fields.Item("U_SubMenu").Value          = pSubMenu;
                oUDT.UserFields.Fields.Item("U_ImageIcon").Value        = pImageIcon;
                oUDT.UserFields.Fields.Item("U_Cannabis").Value         = pCannabis;
                oUDT.UserFields.Fields.Item("U_MV_Retail").Value        = pRetail;
                oUDT.UserFields.Fields.Item("U_MV_Distributor").Value   = pDistributor;
                oUDT.UserFields.Fields.Item("U_MV_Grow").Value          = pGrow;
                oUDT.UserFields.Fields.Item("U_MV_Processor").Value     = pProcessor;
                if (oUDT.Add() != 0 && pIgnoreError == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        private static void FillSettingsMenuTable_Update(string pNameOld, string pNameNew = null, string pUDFName = null, string pUDFValue = null)
        {
            UserTable oUDT = null;
            try
            {
                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tSettingsMenu);

                var code = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Code FROM [@{NSC_DI.Globals.tSettingsMenu}] WHERE Name = '{pNameOld}'", "");
                if (code == "") return;
                if (oUDT.GetByKey(code) == false) return;
                if (pNameNew != null) oUDT.Name = pNameNew;
                if (pUDFName != null) oUDT.UserFields.Fields.Item(pUDFName).Value = pUDFValue;
                if (oUDT.Update() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                NSC_DI.UTIL.Settings.Value.Set("SettingMenuLoaded", "Y", "Indicates that all Settings Menus have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        private static void FillItemGroups()
        {
            ItemGroups oItmGrp = null;
            var successfulErrors = new int[] { 0, -2035 };

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("AddItemGroups") == "Y") return;

                var dtItemGroups = GetItemGroupTable();

                var clause = $"{GetDataTableMvSelectClause()} OR ItemGroupName IN ({String.Join(",", GetAutoItemTable().Select(GetDataTableMvSelectClause() + " AND ItemGroupName <> ''").Select(n => "'" + n["ItemGroupName"] + "'").Distinct())})";

                foreach (var dr in dtItemGroups.Select(clause))
                {
                    oItmGrp = Globals.oCompany.GetBusinessObject(BoObjectTypes.oItemGroups);
                    oItmGrp.GroupName = dr["ItemGroupName"] as string;
                    if (dr["CanType"] as string != "-") oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = dr["CanType"] as string;
                    if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }
                NSC_DI.UTIL.Settings.Value.Set("AddItemGroups", "Y", "Indicates that all Item Groups have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItmGrp);
                GC.Collect();
            }
        }

        private static void FillUOMs()
        {
            UnitOfMeasurementsService oUOMService = null;
            UnitOfMeasurement oUOM = null;

            var successfulErrors = new int[] { 0, -2035 };

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("AddUOMs") == "Y") return;

                oUOMService = Globals.oCompService.GetBusinessService(ServiceTypes.UnitOfMeasurementsService);
                oUOM = oUOMService.GetDataInterface(UnitOfMeasurementsServiceDataInterfaces.uomsUnitOfMeasurement);
                oUOM.Code = "EA";
                oUOM.Name = "Each";
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                oUOM.Code = "BX";
                oUOM.Name = "Box";
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                oUOM.Code = "CS";
                oUOM.Name = "Case";
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                oUOM.Code = "LB";
                oUOM.Name = "Pounds";
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                oUOM.Code = "GR";
                oUOM.Name = "Grams";
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                oUOM.Code = "GAL";
                oUOM.Name = "Gallons";
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                oUOM.Code = "L";
                oUOM.Name = "Liters";
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                oUOM.Code = "CYd";
                oUOM.Name = "Cubic Yards";
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                oUOM.Code = "KG";
                oUOM.Name = "Kelogram";
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                NSC_DI.UTIL.Settings.Value.Set("AddUOMs", "Y", "Indicates that all UOMs have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUOM);
                NSC_DI.UTIL.Misc.KillObject(oUOMService);
                GC.Collect();
            }
        }

        private static void FillUOM_Groups()
        {
            UnitOfMeasurementGroupsService oUOMService = null;
            UnitOfMeasurementGroup oUOM = null;

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("AddUOM_Groups") == "Y") return;

                oUOMService = Globals.oCompService.GetBusinessService(ServiceTypes.UnitOfMeasurementGroupsService);
                oUOM = oUOMService.GetDataInterface(UnitOfMeasurementGroupsServiceDataInterfaces.uomgsUnitOfMeasurementGroup);

                oUOM.Code = "Each";
                oUOM.Name = "Each";
                oUOM.BaseUoM = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT UomEntry FROM OUOM WHERE UomName = '{oUOM.Code}'", -1);
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                oUOM.Code = "Grams";
                oUOM.Name = "Grams";
                oUOM.BaseUoM = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT UomEntry FROM OUOM WHERE UomName = '{oUOM.Code}'", -1);
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                oUOM.Code = "Liters";
                oUOM.Name = "Liters";
                oUOM.BaseUoM = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT UomEntry FROM OUOM WHERE UomName = '{oUOM.Code}'", -1);
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                oUOM.Code = "Dirt";
                oUOM.Name = "Dirt";
                oUOM.BaseUoM = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT UomEntry FROM OUOM WHERE UomName = 'Pounds'", -1);
                try { oUOMService.Add(oUOM); }
                catch (Exception ex)
                {
                    if (ex.HResult != -2035) throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                }

                NSC_DI.UTIL.Settings.Value.Set("AddUOM_Groups", "Y", "Indicates that all Item UOM Groups have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUOM);
                NSC_DI.UTIL.Misc.KillObject(oUOMService);
                GC.Collect();
            }
        }

        private static System.Data.DataTable GetItemGroupTable()
        {
            try
            {
              
                var dtItemGroup = new System.Data.DataTable();

                dtItemGroup.Columns.Add("ItemGroupName");
                dtItemGroup.Columns.Add("Grow");
                dtItemGroup.Columns.Add("Processor");
                dtItemGroup.Columns.Add("Distributor");
                dtItemGroup.Columns.Add("Retail");
                dtItemGroup.Columns.Add("CanType");

                //                                   ItemGroupName          Grow Processor Distributor Retail CanType
                dtItemGroup.Rows.Add(new object[] { "Additives", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Cannabis Plant", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Cannabis Waste", "Y", "Y", "Y", "Y", "-" });
                dtItemGroup.Rows.Add(new object[] { "Clone", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Concentrate Lot QA", "N", "Y", "Y", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Dry Cannabis", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Dry Unb Flower", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Dry Unb Trim", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Finished Good", "N", "Y", "Y", "Y", "PacC" });
                dtItemGroup.Rows.Add(new object[] { "Flower Lot QA", "Y", "Y", "Y", "N", "QCLot" });
                dtItemGroup.Rows.Add(new object[] { "Frzn Unb Flower", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Frzn Unb Trim", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Fungicides", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Growing Accessories", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Joint", "N", "Y", "Y", "Y", "PacC" });
                dtItemGroup.Rows.Add(new object[] { "Non-Cannabis Waste", "Y", "Y", "Y", "Y", "-" });
                dtItemGroup.Rows.Add(new object[] { "Nutrients", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Packaging Supplies", "N", "Y", "Y", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Pesticides", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Plant Tissue", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Sample", "Y", "Y", "Y", "Y", "PacC" });
                dtItemGroup.Rows.Add(new object[] { "Seed", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Trim Lot QA", "Y", "Y", "Y", "N", "QCLot" });
                dtItemGroup.Rows.Add(new object[] { "Unb Concentrate", "N", "Y", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Wet Cannabis", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Wet Unb Flower", "Y", "N", "N", "N", "-" });
                dtItemGroup.Rows.Add(new object[] { "Wet Unb Trim", "Y", "N", "N", "N", "-" });

                return dtItemGroup;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static System.Data.DataTable GetSettingsMenuTable()
        {
            try
            {
                var dtSettingsMenuTable = new System.Data.DataTable();

                dtSettingsMenuTable.Columns.Add("Code");
                dtSettingsMenuTable.Columns.Add("Name");
                dtSettingsMenuTable.Columns.Add("MENUCODE");
                dtSettingsMenuTable.Columns.Add("ParentName");
                dtSettingsMenuTable.Columns.Add("PredecessorName");
                dtSettingsMenuTable.Columns.Add("SubMenu");
                dtSettingsMenuTable.Columns.Add("ImageIcon");
                dtSettingsMenuTable.Columns.Add("Cannabis");
                dtSettingsMenuTable.Columns.Add("Retail");
                dtSettingsMenuTable.Columns.Add("Distributor");
                dtSettingsMenuTable.Columns.Add("Grow");
                dtSettingsMenuTable.Columns.Add("Processor");

                //                                           Code      Name                                MenuCode             ParentName                      PredecessorName     SubMenu ImageIcon               Cannabis Retail Distributor Grow Processor
                dtSettingsMenuTable.Rows.Add(new object[] { "100010", "Strain List", "NSC_STRAIN_LS", "Inventory", "Item Master Data", "N", "", "Y", "Y", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "101000", "Propagation / Nursery", "NSC_MENU_NURS", "Production", "Production Order", "Y", "MainLogo-resized.bmp", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "101010", "Mother Plant Management", "NSC_MOTHERPLANT", "Propagation / Nursery", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "101020", "Cloning Wizard", "NSC_CLONING_WIZ", "Propagation / Nursery", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "101030", "Clone Bank - Planner", "NSC_CLONEBANK", "Propagation / Nursery", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "101040", "Clone Viable Plant Yields", "NSC_PRODCLONE", "Propagation / Nursery", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "101050", "Seed Harvesting Wizard", "NSC_SEED_WIZ", "Propagation / Nursery", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "101060", "Seedling Planner", "NSC_SEEDPLAN", "Propagation / Nursery", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "101070", "Seedling Viable Plant Yields", "NSC_PRODSEED", "Propagation / Nursery", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "102000", "Production / Garden", "NSC_MENU_PROD", "Production", "Propagation / Nursery", "Y", "MainLogo-resized.bmp", "Y", "Y", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "102010", "Production View - Viable Plants", "NSC_PRODUCTION", "Production / Garden", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "102030", "Harvest Wizard", "NSC_HARVEST_WIZ", "Production / Garden", "", "N", "", "Y", "N", "N", "Y", "N" });
                //Wet Processing
                dtSettingsMenuTable.Rows.Add(new object[] { "108040", "Wet Processing", "NSC_MENU_WET", "Production", "Production / Garden", "Y", "MainLogo-resized.bmp", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "108041", "Wet Processing Planner", "NSC_WET_PROC_PP", "Wet Processing", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "108042", "Wet Processing Yields", "NSC_WET_PROC_VM", "Wet Processing", "Wet Processing Planner", "N", "", "Y", "N", "N", "Y", "N" });

                //Dry
                dtSettingsMenuTable.Rows.Add(new object[] { "103000", "Drying / Curing", "NSC_MENU_CUR", "Production", "Production / Garden", "Y", "MainLogo-resized.bmp", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "103010", "Dry - Planner", "NSC_PLANDRYBUD", "Drying / Curing", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "103020", "Dry - Yields", "NSC_PVDRY", "Drying / Curing", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "103030", "Curing + Grading - Planner", "NSC_PLANCUREDBUD", "Drying / Curing", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "103040", "Curing + Grading - Yields", "NSC_PVCURED", "Drying / Curing", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "104000", "Samples / Laboratory Testing", "NSC_MENU_SAMP", "Main Menu", "Production", "Y", "MainLogo-resized.bmp", "Y", "Y", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "104010", "QA Batch Creator", "NSC_BATCH_MNG", "Samples / Laboratory Testing", "", "N", "", "Y", "N", "N", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "104020", "Sample Item Intake Wizard", "NSC_SAMPLEINTAKE", "Samples / Laboratory Testing", "", "N", "", "Y", "N", "N", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "104030", "Test Sample Bank", "NSC_SAMPLEBANK", "Samples / Laboratory Testing", "", "N", "", "Y", "Y", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "104040", "Test List", "NSC_TEST_LS", "Samples / Laboratory Testing", "", "N", "", "Y", "Y", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "104050", "Test Compound List", "NSC_COMP_LS", "Samples / Laboratory Testing", "", "N", "", "Y", "Y", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "104060", "Test Fields List", "NSC_TESTFIELD_LS", "Samples / Laboratory Testing", "", "N", "", "Y", "Y", "Y", "Y", "Y" });
                // RH - comment out for now until fixed.
                //dtSettingsMenuTable.Rows.Add(new object [] { "104070", "Ailment List",                     "NSC_DISEA_LS",      "Samples / Laboratory Testing", "",                 "N",    "",                     "Y",     "Y",   "Y",        "Y", "Y" });
                //dtSettingsMenuTable.Rows.Add(new object [] 0                                                          { "104080", "Effect List",                      "NSC_EFFECT_LS",     "Samples / Laboratory Testing", "",                 "N",    "",                     "Y",     "Y",   "Y",        "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "105000", "Quarantine / Destruction", "NSC_MENU_QUAR", "Production", "Drying / Curing", "Y", "MainLogo-resized.bmp", "Y", "Y", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "105010", "Plant Quarantine View", "NSC_QUARANTINE", "Quarantine / Destruction", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "105020", "Batch Destruction Wizard", "NSC_QUARBATCH_WIZ", "Quarantine / Destruction", "", "N", "", "Y", "N", "N", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "105030", "Batch Quarantine View", "NSC_QUARBATCH", "Quarantine / Destruction", "", "N", "", "Y", "N", "N", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "106000", "Environmental Controls", "NSC_MENU_ENV", "Main Menu", "Reports", "Y", "MainLogo-resized.bmp", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "106010", "Air Quality", "NSC_AIR_QUALITY", "Environmental Controls", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "106020", "Filter Management", "NSC_FILTER_MNGT", "Environmental Controls", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "106030", "Light Life Management", "NSC_LIGHTLIFE", "Environmental Controls", "", "N", "", "Y", "N", "N", "Y", "N" });
                dtSettingsMenuTable.Rows.Add(new object[] { "107000", "Processing / Packaging", "NSC_MENU_PACK", "Production", "Quarantine / Destruction", "Y", "MainLogo-resized.bmp", "Y", "N", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "107010", "Processing and Packaging Planner", "NSC_PACKAGING_PP", "Processing / Packaging", "", "N", "", "Y", "N", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "107020", "Processing and Packaging Yields", "NSC_PACKAGING_PV", "Processing / Packaging", "", "N", "", "Y", "N", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "108000", "Cannabis Sales / Deliveries", "NSC_MENU_SALE", "Sales - A/R", "", "Y", "MainLogo-resized.bmp", "Y", "Y", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "108010", "Delivery and Route Management", "NSC_DELIVERY_MGNT", "Cannabis Sales / Deliveries", "", "N", "", "Y", "N", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "108020", "Vehicle Management", "NSC_VEHICLE", "Cannabis Sales / Deliveries", "", "N", "", "Y", "N", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "108030", "Inventory Intake Wizard", "NSC_CANABISINTAKE", "Inventory", "Strain List", "N", "", "Y", "Y", "Y", "Y", "Y" });

                //Compliance
                dtSettingsMenuTable.Rows.Add(new object[] { "109000", "Compliance", "NSC_MENU_CMP", "Main Menu", "Reports", "Y", "MainLogo-resized.bmp", "Y", "Y", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "109010", "State License and Credentials", "NSC_STATELIC", "Compliance", "", "N", "", "Y", "Y", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "109020", "Compliance Monitor", "NSC_COMPLIANCEMON", "Compliance", "", "N", "", "Y", "Y", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "109030", "Inventory Resolution", "NSC_INV_RES", "Compliance", "", "N", "", "Y", "Y", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "109034", "Production Update", "NSC_PROD_ORDER_UPDATE", "Compliance", "", "N", "", "N", "N", "Y", "Y", "Y" });
                dtSettingsMenuTable.Rows.Add(new object[] { "109040", "Dashboard", "NSC_DASHBOARD", "Compliance", "", "N", "", "Y", "Y", "Y", "Y", "Y" });

                //Crop Cycle Management
                dtSettingsMenuTable.Rows.Add(new object[] { "108050", "Crop Cycle Management", "NSC_CROP_MGT", "Project Management", "Project", "N", "", "Y", "N", "N", "Y", "N" });

                return dtSettingsMenuTable;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static System.Data.DataTable GetAutoItemTable()
        {
            // Items Generated 09/07/2017 15:32:53 -06:00

            try
            {
                var dtAutoItem = new System.Data.DataTable();
                dtAutoItem.Columns.Add("ItemCode");
                dtAutoItem.Columns.Add("ItemName");
                dtAutoItem.Columns.Add("Retail");
                dtAutoItem.Columns.Add("Processor");
                dtAutoItem.Columns.Add("Distributor");
                dtAutoItem.Columns.Add("Grow");
                dtAutoItem.Columns.Add("Properties");
                dtAutoItem.Columns.Add("ItemGroupName");
                dtAutoItem.Columns.Add("ManageSerialNumbers");
                dtAutoItem.Columns.Add("ManageBatchNumbers");
                dtAutoItem.Columns.Add("HasBillOfMaterials");
                dtAutoItem.Columns.Add("InventoryItem");
                dtAutoItem.Columns.Add("PurchaseItem");
                dtAutoItem.Columns.Add("SalesItem");
                dtAutoItem.Columns.Add("UnitPrice", typeof(double));
                dtAutoItem.Columns.Add("DefaultWarehouse");
                dtAutoItem.Columns.Add("Sampleable");
                dtAutoItem.Columns.Add("Batchable");
                dtAutoItem.Columns.Add("Processable");
                dtAutoItem.Columns.Add("ProcessType");
                dtAutoItem.Columns.Add("UOMGroup");

                //                                  ItemCode        ItemName                                   Retail Processor Distributor Grow Properties                 ItemGroupName          ManageSN ManageBatch HasBOM InvItem PurItem SalItem UnitPrice DefaultWH Sampleable Batchable Processable ProcessType UOM Group
                dtAutoItem.Rows.Add(new object[] { "~", "~Cannabis Plant", "N", "N", "N", "M", "Cannabis Plant", "Cannabis Plant", "Y", "N", "Y", "Y", "Y", "Y", 0, "3001", "N", "N", "Y", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~BDR-BK",      "~Budder - BULK",                          "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~BDR-QA",      "~Budder - QA",                            "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~BH-BK", "~Bubble Hash - BULK", "N", "M", "N", "N", "Concentrate Bulk", "Unb Concentrate", "N", "Y", "Y", "Y", "Y", "Y", 0, "8501", "Y", "N", "Y", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~BH-QA", "~Bubble Hash - QA", "N", "M", "B", "N", "Concentrate Lot QA", "Concentrate Lot QA", "N", "Y", "Y", "Y", "Y", "Y", 0, "8501", "N", "N", "N", "Standard", "Grams" });
                //dtAutoItem.Rows.Add(new object [] { "~BU-BK",       "~Infused Butter/Fat - BULK",              "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "Y",       "N",      "Y",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~BU-QA",       "~Infused Butter/Fat - QA",                "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "Y",       "N",      "Y",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~CL", "~Clone", "N", "N", "N", "M", "Clone", "Clone", "N", "Y", "Y", "Y", "Y", "Y", 0, "2001", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "~CO2-BK", "~CO2 Hash Oil - BULK", "N", "M", "N", "N", "Concentrate Bulk", "Unb Concentrate", "N", "Y", "Y", "Y", "Y", "Y", 0, "8501", "Y", "N", "Y", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~CO2-QA", "~CO2 Hash Oil - QA", "N", "M", "B", "N", "Concentrate Lot QA", "Concentrate Lot QA", "N", "Y", "Y", "Y", "Y", "Y", 0, "8501", "N", "N", "N", "Standard", "Grams" });
                //dtAutoItem.Rows.Add(new object [] { "~CO-BK",       "~Infused Cooking Oil - BULK",             "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "Y",       "N",      "Y",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~CRD-BK", "~Crude Oil - BULK", "N", "M", "B", "N", "Concentrate Bulk", "Unb Concentrate", "N", "Y", "Y", "Y", "Y", "Y", 0, "8501", "N", "N", "N", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~CRD-QA", "~Crude Oil - QA", "N", "M", "B", "N", "Concentrate Lot QA", "Concentrate Lot QA", "N", "Y", "Y", "Y", "Y", "Y", 0, "8501", "N", "N", "N", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~DC", "~Dry Cannabis Plant", "N", "N", "N", "M", "Dry Cannabis", "Dry Cannabis", "N", "Y", "Y", "Y", "Y", "Y", 0, "6001", "N", "N", "N", "Standard", "Grams" });
                //dtAutoItem.Rows.Add(new object [] { "~DIS-BK",      "~Distillate - BULK",                      "N",   "M",      "B",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~DIS-QA",      "~Distillate - QA",                        "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~DUFL", "~Dry Unbatched Flower", "N", "N", "N", "M", "Dry Unbatched Cannabis", "Dry Unb Flower", "N", "Y", "Y", "Y", "Y", "Y", 0, "7001", "Y", "N", "N", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~DUTR", "~Dry Unbatched Trim", "N", "N", "N", "M", "Dry Unbatched Trim", "Dry Unb Trim", "N", "Y", "Y", "Y", "Y", "Y", 0, "7001", "Y", "N", "N", "Standard", "Grams" });
                //dtAutoItem.Rows.Add(new object [] { "~ED",          "~Solid Edible - BULK",                    "N",   "M",      "B",        "N", "Edible Bulk",             "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~ED-BK",       "~Solid Edible - BULK",                    "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "Y",       "N",      "Y",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~ED-QA",       "~Solid Edible Lot - QA",                  "N",   "M",      "B",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "Y",       "N",      "Y",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~FL", "~Flower Lot - QA", "N", "B", "B", "M", "Flower lot QA", "Flower Lot QA", "N", "Y", "Y", "Y", "Y", "Y", 0, "8001", "Y", "Y", "Y", "Standard", "Grams" });
                //dtAutoItem.Rows.Add(new object [] { "~FO-BK",       "~Food Grade Solvent Extract - BULK",      "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "Y",       "N",      "Y",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~FO-QA",       "~Food Grade Solvent Extract - QA",        "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "Y",       "N",      "Y",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~FUFL",        "~Frozen Unbatched Flower",                "N",   "N",      "N",        "M", "Frozen Unbatched Flower", "Frzn Unb Flower",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "7001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~FUTR",        "~Frozen Unbatched Trim",                  "N",   "N",      "N",        "M", "Frozen Unbatched Trim",   "Frzn Unb Trim",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "7001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~GB", "~Finely Ground Bud", "B", "B", "B", "N", "Ground Cannabis", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~HA-BK", "~Hash - BULK", "N", "M", "N", "N", "Concentrate Bulk", "Unb Concentrate", "N", "Y", "Y", "Y", "Y", "Y", 0, "8501", "Y", "N", "Y", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~HA-QA", "~Hash - QA", "N", "M", "B", "N", "Concentrate Lot QA", "Concentrate Lot QA", "N", "Y", "Y", "Y", "Y", "Y", 0, "8501", "N", "N", "N", "Standard", "Grams" });
                //dtAutoItem.Rows.Add(new object [] { "~HCB-BK",      "~Honeycomb - BULK",                       "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~HCB-QA",      "~Honeycomb - QA",                         "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~HW-BK",       "~Hydrocarbon Wax - BULK",                 "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "Y",       "N",      "Y",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~HW-QA",       "~Hydrocarbon Wax - QA",                   "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~JO-.5G", "~Joint - 0.5G", "B", "M", "M", "N", "Joint", "Joint", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "Y", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~JO-1G",       "~Joint - 1G",                             "N",   "M",      "M",        "N", "Joint",                   "Joint",               "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "Y",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~KF-BK", "~Kief - BULK", "N", "M", "N", "N", "Concentrate Bulk", "Unb Concentrate", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "Y", "N", "N", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~KF-QA", "~Kief - QA", "N", "M", "B", "N", "Concentrate Lot QA", "Concentrate Lot QA", "N", "Y", "Y", "Y", "Y", "Y", 0, "8501", "N", "N", "N", "Standard", "Grams" });

                //if (NSC_DI.UTIL.Settings.Value.Get("WetCannabisProcess") == "Y")
                //{
                //dtAutoItem.Rows.Add(new object [] { "~LBDR-BK",     "~Live Budder - BULK",                     "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LBDR-QA",     "~Live Budder - QA",                       "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LBH-BK",      "~Live Bubble Hash - BULK",                "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LBH-QA",      "~Live Bubble Hash - QA",                  "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LBU-BK",      "~Live Infused Butter/Fat - BULK",         "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LCO2-BK",     "~Live CO2 Hash Oil - BULK",               "N",   "M",      "B",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LCO2-QA",     "~Live CO2 Hash Oil - QA",                 "N",   "M",      "N",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LCO-BK",      "~Live Infused Cooking Oil - BULK",        "N",   "M",      "B",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LCRD-BK",     "~Live Crude Oil - BULK",                  "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LCRD-QA",     "~Live Crude Oil - QA",                    "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LDIS-BK",     "~Live Distillate - BULK",                 "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LDIS-QA",     "~Live Distillate - QA",                   "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LE-BK",       "~Liquid Edible - BULK",                   "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "Y",       "N",      "Y",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LFO-BK",      "~Live Food Grade Solvent Extract - BULK", "N",   "M",      "B",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LHA-BK",      "~Live Hash - BULK",                       "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LHA-QA",      "~Live Hash - QA",                         "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LHCB-BK",     "~Live Honeycomb - BULK",                  "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LHCB-QA",     "~Live Honeycomb - QA",                    "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LHW-BK",      "~Live Hydrocarbon Wax - BULK",            "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LHW-QA",      "~Live Hydrocarbon Wax - QA",              "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LKF-BK",      "~Live Kief - BULK",                       "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LKF-QA",      "~Live Kief - QA",                         "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LRES-BK",     "~Live Resin - BULK",                      "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LRES-QA",     "~Live Resin - QA",                        "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LROS-BK",     "~Live Rosin - BULK",                      "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LROS-QA",     "~Live Rosin - QA",                        "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LSHA-BK",     "~Live Shatter - BULK",                    "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~LSHA-QA",     "~Live Shatter - QA",                      "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //}

                //dtAutoItem.Rows.Add(new object [] { "~PG-BDR-.5G",  "~Packaged Budder - 0.5G",                 "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-BDR-1G",   "~Packaged Budder - 1G",                   "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~PG-BH-.5G", "~Packaged Bubble Hash - 0.5G", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-BH-1G",    "~Packaged Bubble Hash - 1G",              "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~PG-CO2-.5G", "~Packaged CO2 Cartridge- 0.5G", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-CO2-1G",   "~Packaged CO2 Cartridge- 1G",             "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-CRD-.5G",  "~Packaged Crude Oil - 0.5G",              "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~PG-CRD-1G", "~Packaged Crude Oil - 1G", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-DIS-.5G",  "~Packaged Distillate - 0.5G",             "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-DIS-1G",   "~Packaged Distillate - 1G",               "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-ED-1P",    "~Packaged Solid Edible",                  "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-ED-3P",    "~Packaged Solid Edible - 3 Pack",         "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-ED-6P",    "~Packaged Solid Edible - 6 Pack",         "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-FL-113G",  "~Packaged Flower- 113G",                  "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-FL-14G",   "~Packaged Flower- 14G",                   "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~PG-FL-1G", "~Packaged Flower- 1G", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-FL-227G",  "~Packaged Flower - 227G",                 "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-FL-28G",   "~Packaged Flower- 28G",                   "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-FL-2G",    "~Packaged Flower- 2G",                    "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-FL-3.5G",  "~Packaged Flower- 3.5G",                  "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~PG-FL-454G", "~Packaged Flower - 454G", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-FL-4G",    "~Packaged Flower- 4G",                    "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-FL-7G",    "~Packaged Flower- 7G",                    "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~PG-DELI", "~Packaged DELI Product", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Grams" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-HA-.5G",   "~Packaged Hash - 0.5G",                   "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~PG-HA-1G", "~Packaged Hash - 1G", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-HCB-.5G",  "~Packaged Honeycomb - 0.5G",              "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-HCB-1G",   "~Packaged Honeycomb - 1G",                "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-HW-.5G",   "~Packaged Hydrocarbon Wax - 0.5G",        "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-HW-1G",    "~Packaged Hydrocarbon Wax - 1G",          "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~PG-JO-.5G", "~Packaged Joint - 0.5G", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-JO-1G",    "~Packaged Joint - 1G",                    "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~PG-JOPK-.5G", "~Packaged Joint 3pack - 0.5G", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-JOPK-1G",  "~Packaged Joint 3pack - 1G",              "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~PG-KF-.5G", "~Packaged Kief - 0.5G", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "~PG-KF-1G", "~Packaged Kief - 1G", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-LE",       "~Packaged Liquid Edible",                 "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~PG-PEN-1G", "~Packaged CO2 Vape Pen - 1G", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-RES-.5G",  "~Packaged Live Resin - 0.5G",             "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-RES-1G",   "~Packaged Live Resin - 1G",               "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-ROS-.5G",  "~Packaged Rosin - 0.5G",                  "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-ROS-1G",   "~Packaged Rosin - 1G",                    "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "Y",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-SHA-.5G",  "~Packaged Shatter - 0.5G",                "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-SHA-1G",   "~Packaged Shatter - 1G",                  "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~PG-TP",       "~Packaged Topical",                       "B",   "M",      "M",        "N", "Packaged Good",           "Finished Good",       "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~PT", "~Plant Tissue", "N", "N", "N", "M", "Plant Tissue", "Plant Tissue", "N", "Y", "Y", "Y", "Y", "Y", 0, "1001", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~RES-BK",      "~Live Resin - BK",                        "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~RES-QA",      "~Live Resin - QA",                        "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~ROS-BK",      "~Rosin - BULK",                           "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~ROS-QA",      "~Rosin - QA",                             "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-BDR",      "~Budder - Sample",                        "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~SA-BH", "~Bubble Hash - Sample", "B", "M", "B", "N", "Sample", "Sample", "N", "Y", "Y", "Y", "N", "N", 0, "100", "N", "N", "N", "Standard", "Grams" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-BU",       "~Infused Butter/Fat - Sample",            "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-CO",       "~Infused Cooking Oil - Sample",           "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~SA-CO2", "~CO2 Hash Oil - Sample", "B", "M", "B", "N", "Sample", "Sample", "N", "Y", "Y", "Y", "N", "N", 0, "100", "N", "N", "N", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~SA-CRD", "~Crude Oil - Sample", "B", "M", "B", "N", "Sample", "Sample", "N", "Y", "Y", "Y", "Y", "Y", 0, "100", "N", "N", "N", "Standard", "Grams" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-DIS",      "~Distillate - Sample",                    "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-ED",       "~Solid Edible - Sample",                  "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~SA-FL", "~Flower Lot - Sample", "B", "B", "B", "M", "Sample", "Sample", "N", "Y", "Y", "Y", "Y", "Y", 0, "100", "N", "N", "N", "Standard", "Grams" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-FO",       "~Food Grade - Sample",                    "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~SA-HA", "~Hash - Sample", "B", "M", "B", "N", "Sample", "Sample", "N", "Y", "Y", "Y", "Y", "Y", 0, "100", "N", "N", "N", "Standard", "Grams" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-HCB",      "~Honeycomb - Sample",                     "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-HW",       "~Hydrocarbon Wax - Sample",               "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "N",    "N",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~SA-KF", "~Kief - Sample", "B", "M", "B", "N", "Sample", "Sample", "N", "Y", "Y", "Y", "Y", "Y", 0, "100", "N", "N", "N", "Standard", "Grams" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-LE",       "~Liquid Edible - Sample",                 "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-RES",      "~Live Resin - Sample",                    "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-ROS",      "~Rosin - Sample",                         "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-SHA",      "~Shatter - Sample",                       "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~SA-SNIFF",    "~Sniffer Jar - Sample",                   "B",   "M",      "B",        "N", "Sample",                  "Sample",              "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "9001",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~SA-TR", "~Trim Lot - Sample", "B", "B", "B", "M", "Sample", "Sample", "N", "Y", "Y", "Y", "Y", "Y", 0, "100", "N", "N", "N", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~SE", "~Seeds", "N", "N", "N", "M", "Seed", "Seed", "N", "Y", "Y", "Y", "Y", "Y", 0, "1001", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "~SHA-BK",      "~Shatter - BULK",                         "N",   "M",      "N",        "N", "Concentrate Bulk",        "Unb Concentrate",     "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "~SHA-QA",      "~Shatter - QA",                           "N",   "M",      "B",        "N", "Concentrate Lot QA",      "Concentrate Lot QA",  "N",     "Y",        "Y",   "Y",    "Y",    "Y",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "~TR", "~Trim Lot - QA", "N", "B", "B", "M", "Trim Lot QA", "Trim Lot QA", "N", "Y", "Y", "Y", "Y", "Y", 0, "8001", "Y", "N", "Y", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~WA", "~Waste", "M", "M", "M", "M", "Cannabis Waste", "Cannabis Waste", "N", "Y", "N", "Y", "N", "N", 0, "9999", "N", "N", "N", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~WC", "~Wet Cannabis Plant", "N", "N", "N", "M", "Wet Cannabis", "Wet Cannabis", "N", "Y", "Y", "Y", "Y", "Y", 0, "6001", "N", "N", "N", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~WUFL", "~Wet Unbatched Flower", "N", "N", "N", "M", "Wet Unbatched Cannabis", "Wet Unb Flower", "N", "Y", "Y", "Y", "Y", "Y", 0, "7001", "N", "N", "N", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "~WUTR", "~Wet Unbatched Trim", "N", "N", "N", "M", "Wet Trim", "Wet Unb Trim", "N", "Y", "Y", "Y", "Y", "Y", 0, "7001", "N", "N", "N", "Standard", "Grams" });
                dtAutoItem.Rows.Add(new object[] { "BLEND", "Blended Product", "B", "M", "M", "N", "Packaged Good", "Finished Good", "N", "Y", "Y", "Y", "Y", "Y", 0, "9001", "N", "N", "N", "Standard", "Grams" });

                dtAutoItem.Rows.Add(new object[] { "BAG0001", "Planting Bag - 1 Gallon", "N", "N", "N", "B", "", "Growing Accessories", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "BAG0005", "Planting Bag - 5 Gallon", "N", "N", "N", "B", "", "Growing Accessories", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "BFS0001", "Bloom Feed Stock", "N", "N", "N", "B", "", "Nutrients", "N", "N", "Y", "Y", "N", "N", 0, "10", "N", "N", "N", "Standard", "Manual" });
                dtAutoItem.Rows.Add(new object[] { "BOX0001", "Box - 1", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "BOX0002", "Box - 2", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "CON0001", "Cone - Small", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "CON0002", "Cone - Large", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "CTG0005", "Cartridge - 0.5G", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "CTG0010",      "Cartridge - 1G",                          "N",   "B",      "B",        "N", "",                        "Packaging Supplies",  "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "10",     "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "CUB0001", "Planting Cube - Small", "N", "N", "N", "B", "", "Growing Accessories", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "CUB0002", "Planting Cube - Large", "N", "N", "N", "B", "", "Growing Accessories", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "DRT0001", "Planting Soil", "N", "N", "N", "B", "", "Growing Accessories", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Manual" });
                dtAutoItem.Rows.Add(new object[] { "FAB0001", "Fabric Bucket - 1 Gallon", "N", "N", "N", "B", "", "Growing Accessories", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "FAB0003",      "Fabric Bucket - 3 Gallon",                "N",   "N",      "N",        "B", "",                        "Growing Accessories", "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "10",     "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "FAB0005",      "Fabric Bucket - 5 Gallon",                "N",   "N",      "N",        "B", "",                        "Growing Accessories", "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "10",     "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "FUNG001", "Fungacide X", "N", "N", "N", "B", "", "Fungicides", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Manual" });
                dtAutoItem.Rows.Add(new object[] { "H2O", "Water", "N", "N", "N", "B", "", "Growing Accessories", "N", "N", "N", "Y", "Y", "N", 0, "9996", "N", "N", "N", "Standard", "Manual" });
                dtAutoItem.Rows.Add(new object[] { "JAR0001", "Dab Jar - 1G", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "JAR0002", "Dab Jar - 0.5G", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "JAR0003", "Jar - Small", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "JAR0004", "Jar - Medium", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "JAR0005", "Jar - Large", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "LBL0001", "Label - 1", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "LBL0002", "Label - 2", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "MED0001", "Planting Medium 1", "N", "N", "N", "B", "", "Growing Accessories", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Manual" });
                dtAutoItem.Rows.Add(new object[] { "MED0002", "Planting Medium 2", "N", "N", "N", "B", "", "Growing Accessories", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Manual" });
                dtAutoItem.Rows.Add(new object[] { "PEN0001", "Vape Pen", "N", "B", "Y", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "PEST001", "Pesticide X", "N", "N", "N", "B", "", "Pesticides", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Manual" });
                dtAutoItem.Rows.Add(new object[] { "PKG0005", "Bag - 0.5G", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "8501", "N", "N", "N", "Standard" });
                dtAutoItem.Rows.Add(new object[] { "PKG0010", "Bag - 1G", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "PKG0020",      "Bag - 2G",                                "N",   "B",      "B",        "N", "",                        "Packaging Supplies",  "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "PKG0035",      "Bag - 3.5G (1/8 oz)",                     "N",   "B",      "B",        "N", "",                        "Packaging Supplies",  "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "PKG0040",      "Bag - 4G",                                "N",   "B",      "B",        "N", "",                        "Packaging Supplies",  "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "PKG0070",      "Bag - 7G (1/4 oz)",                       "N",   "B",      "B",        "N", "",                        "Packaging Supplies",  "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "PKG0140",      "Bag - 14G (1/2 oz)",                      "N",   "B",      "B",        "N", "",                        "Packaging Supplies",  "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "PKG0280",      "Bag - 28G (1 oz)",                        "N",   "B",      "B",        "N", "",                        "Packaging Supplies",  "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "PKG1134",      "Bag - 113.4G (1/4 pound)",                "N",   "B",      "B",        "N", "",                        "Packaging Supplies",  "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "PKG2270",      "Bag - 227G (1/2 pound)",                  "N",   "B",      "B",        "N", "",                        "Packaging Supplies",  "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "8501",   "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "PKG4540", "Bag - 454G (1 pound)", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "POT0001", "Planting Bucket - 1 Gallon", "N", "N", "N", "B", "", "Growing Accessories", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "POT0005", "Planting Bucket - 5 Gallon", "N", "N", "N", "B", "", "Growing Accessories", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                //dtAutoItem.Rows.Add(new object [] { "POT0007",      "Planting Bucket - 7 Gallon",              "N",   "N",      "N",        "B", "",                        "Growing Accessories", "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "10",     "N",       "N",      "N",        "Standard" });
                //dtAutoItem.Rows.Add(new object [] { "POT0015",      "Planting Bucket - 15 Gallon",             "N",   "N",      "N",        "B", "",                        "Growing Accessories", "N",     "N",        "N",   "Y",    "Y",    "N",    0,        "10",     "N",       "N",      "N",        "Standard" });
                dtAutoItem.Rows.Add(new object[] { "PPR0001", "Paper - Small", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "PPR0002", "Paper - Large", "N", "B", "B", "N", "", "Packaging Supplies", "N", "N", "N", "Y", "Y", "N", 0, "10", "N", "N", "N", "Standard", "Each" });
                dtAutoItem.Rows.Add(new object[] { "VFS0001", "Veg Feed Stock", "N", "N", "N", "B", "", "Nutrients", "N", "N", "Y", "Y", "N", "N", 0, "10", "N", "N", "N", "Standard", "Manual" });

                return dtAutoItem;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static System.Data.DataTable GetAutoBOMTable()
        {
            // Items Generated 09/22/2017 17:11:00 -06:00

            try
            {
                var dtAutoItemBOM = new System.Data.DataTable();
                dtAutoItemBOM.Columns.Add("Code");
                dtAutoItemBOM.Columns.Add("Name");
                dtAutoItemBOM.Columns.Add("Group");
                dtAutoItemBOM.Columns.Add("ParentItemCode");
                dtAutoItemBOM.Columns.Add("ItemCode");
                dtAutoItemBOM.Columns.Add("Quantity", typeof(int));
                dtAutoItemBOM.Columns.Add("IssueMethod");
                dtAutoItemBOM.Columns.Add("SourceWarehouseCode");
                dtAutoItemBOM.Columns.Add("WarehouseCode");

                //                                     Code    Name    Group            ParentItem      ItemCode    Quantity IssueMethod  SourceWH  Warehouse
                dtAutoItemBOM.Rows.Add(new object[] { "1000", "1000", "Grow", "~", "~CL", 1, "Manual", "2001", "3001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1010", "1010", "Grow", "~", "~CL", -1, "Manual", "9999", "9999" });
                dtAutoItemBOM.Rows.Add(new object[] { "1020", "1020", "Grow", "~", "~PT", 1, "Manual", "2001", "3001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1030", "1030", "Grow", "~", "~PT", -1, "Manual", "9999", "9999" });
                dtAutoItemBOM.Rows.Add(new object[] { "1040", "1040", "Grow", "~", "~SE", 1, "Manual", "1001", "3001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1050", "1050", "Grow", "~", "~SE", -1, "Manual", "9999", "9999" });
                dtAutoItemBOM.Rows.Add(new object[] { "1060", "1060", "Grow", "~", "POT0005", 1, "Backflush", "10", "3001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1070", "1070", "Grow", "~", "DRT0001", 5, "Backflush", "10", "3001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1080", "1080", "Harvest Wet", "~WC", "~", 1, "Manual", "5001", "6001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1090", "1090", "Harvest Wet", "~WUFL", "~WC", 1, "Manual", "6001", "7001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1100", "1100", "Harvest Wet", "~WUFL", "~WUTR", -1, "Manual", "6001", "7001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1110", "1110", "Harvest Wet", "~WUFL", "~WA", -1, "Manual", "9999", "7001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1120", "1120", "Harvest Wet",   "~FUFL",        "~WUFL",    1,       "Manual",    "8501",   "8501" });    
                //dtAutoItemBOM.Rows.Add(new object [] { "1130", "1130", "Harvest Wet",   "~FUTR",        "~WUTR",    1,       "Manual",    "8501",   "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1150", "1150", "Harvest Dry", "~DC", "~WC", 1, "Manual", "6001", "8001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1160", "1160", "Harvest Dry", "~DUFL", "~DC", 1, "Manual", "8001", "8001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1170", "1170", "Harvest Dry", "~DUFL", "~DUTR", -1, "Manual", "8001", "8001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1180", "1180", "Harvest Dry", "~DUFL", "~WA", -1, "Manual", "9999", "8001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1190", "1190", "QA Flower", "~FL", "~DUFL", 1, "Manual", "8001", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1200", "1200", "QA Flower", "~TR", "~DUTR", 1, "Manual", "8001", "8001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1210", "1210", "Extraction", "~GB", "~DUFL", 1, "Manual", "8001", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1220", "1220", "Extraction", "~KF-BK", "~TR", 2, "Manual", "8501", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1221", "1221", "Extraction", "~KF-BK", "~FL", 3, "Manual", "8501", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1230", "1230", "Extraction", "~BH-BK", "~TR", 5, "Manual", "8501", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1231", "1231", "Extraction", "~BH-BK", "~FL", 5, "Manual", "8501", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1240", "1240", "Extraction", "~HA-BK", "~TR", 5, "Manual", "8501", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1241", "1241", "Extraction", "~HA-BK", "~FL", 5, "Manual", "8501", "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1250", "1250", "Extraction",    "~HW-BK",       "~TR",      7,       "Manual",    "6001",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1251", "1251", "Extraction",    "~HW-BK",       "~FL",      8,       "Manual",    "6001",   "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1260", "1260", "Extraction", "~CO2-BK", "~TR", 10, "Manual", "8501", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1261", "1261", "Extraction", "~CO2-BK", "~FL", 10, "Manual", "8501", "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1270", "1270", "Extraction",    "~ROS-BK",      "~TR",      5,       "Manual",    "6001",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1271", "1271", "Extraction",    "~ROS-BK",      "~FL",      5,       "Manual",    "6001",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1280", "1280", "Extraction",    "~RES-BK",      "~TR",      5,       "Manual",    "6001",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1281", "1281", "Extraction",    "~RES-BK",      "~FL",      5,       "Manual",    "6001",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1290", "1290", "Extraction",    "~DIS-BK",      "~TR",      5,       "Manual",    "6001",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1291", "1291", "Extraction",    "~DIS-BK",      "~FL",      5,       "Manual",    "6001",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1300", "1300", "Extraction",    "~SHA-BK",      "~TR",      5,       "Manual",    "6001",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1301", "1301", "Extraction",    "~SHA-BK",      "~FL",      5,       "Manual",    "6001",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1310", "1310", "Extraction",    "~HCB-BK",      "~TR",      5,       "Manual",    "6001",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1311", "1311", "Extraction",    "~HCB-BK",      "~FL",      5,       "Manual",    "6001",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1320", "1320", "Extraction",    "~BDR-BK",      "~TR",      5,       "Manual",    "6001",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1321", "1321", "Extraction",    "~BDR-BK",      "~FL",      5,       "Manual",    "6001",   "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1330", "1330", "Extraction", "~CRD-BK", "~TR", 5, "Manual", "8501", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1331", "1331", "Extraction", "~CRD-BK", "~FL", 5, "Manual", "8501", "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1340", "1340", "Extraction",    "~ED",          "~CO2-BK",  15,      "Manual",    "8501",   "9001" });    

                //if (NSC_DI.UTIL.Settings.Value.Get("WetCannabisProcess") == "Y")
                //{
                //dtAutoItemBOM.Rows.Add(new object [] { "1470", "1470", "Extraction",    "~LKF-BK",      "~FUFL",    5,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1480", "1480", "Extraction",    "~LBH-BK",      "~FUFL",    10,      "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1490", "1490", "Extraction",    "~LHA-BK",      "~FUFL",    10,      "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1500", "1500", "Extraction",    "~LHW-BK",      "~FUFL",    15,      "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1510", "1510", "Extraction",    "~LCO2-BK",     "~FUFL",    20,      "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1520", "1520", "Extraction",    "~LROS-BK",     "~FUFL",    1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1530", "1530", "Extraction",    "~LRES-BK",     "~FUFL",    1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1540", "1540", "Extraction",    "~LDIS-BK",     "~FUFL",    1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1550", "1550", "Extraction",    "~LSHA-BK",     "~FUFL",    1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1560", "1560", "Extraction",    "~LHCB-BK",     "~FUFL",    1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1570", "1570", "Extraction",    "~LBDR-BK",     "~FUFL",    1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1580", "1580", "Extraction",    "~LCRD-BK",     "~FUFL",    1,       "Manual",    "8501",   "8501" });
                //}

                dtAutoItemBOM.Rows.Add(new object[] { "1585", "1585", "QA Extraction", "~CRD-QA", "~CRD-BK", 1, "Manual", "8501", "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1590", "1590", "Extraction",    "~LFO-BK",      "~CRD-QA",  1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1600", "1600", "Extraction",    "~LBU-BK",      "~CRD-QA",  1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1610", "1610", "Extraction",    "~LCO-BK",      "~CRD-QA",  1,       "Manual",    "8501",   "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1620", "1620", "QA Ext", "~KF-QA", "~KF-BK", 1, "Manual", "8501", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1630", "1630", "QA Extraction", "~BH-QA", "~BH-BK", 1, "Manual", "8501", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1640", "1640", "QA Extraction", "~HA-QA", "~HA-BK", 1, "Manual", "8501", "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1650", "1650", "QA Extraction", "~HW-QA",       "~HW-BK",   1,       "Manual",    "8501",   "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1660", "1660", "QA Extraction", "~CO2-QA", "~CO2-BK", 1, "Manual", "8501", "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1670", "1670", "QA Extraction", "~ROS-QA",      "~ROS-BK",  1,       "Manual",    "8501",   "8501" });    
                //dtAutoItemBOM.Rows.Add(new object [] { "1680", "1680", "QA Extraction", "~RES-QA",      "~RES-BK",  1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1690", "1690", "QA Extraction", "~DIS-QA",      "~DIS-BK",  1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1700", "1700", "QA Extraction", "~SHA-QA",      "~SHA-BK",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1710", "1710", "QA Extraction", "~HCB-QA",      "~HCB-BK",  1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1720", "1720", "QA Extraction", "~BDR-QA",      "~BDR-BK",  1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1740", "1740", "QA Extraction", "~LKF-QA",      "~LKF-BK",  1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1750", "1750", "QA Extraction", "~LBH-QA",      "~LBH-BK",  1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1760", "1760", "QA Extraction", "~LHA-QA",      "~LHA-BK",  1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1770", "1770", "QA Extraction", "~LHW-QA",      "~LHW-BK",  1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1780", "1780", "QA Extraction", "~LCO2-QA",     "~LCO2-BK", 1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1790", "1790", "QA Extraction", "~LROS-QA",     "~LROS-BK", 1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1800", "1800", "QA Extraction", "~LRES-QA",     "~LRES-BK", 1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1810", "1810", "QA Extraction", "~LDIS-QA",     "~LDIS-BK", 1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1820", "1820", "QA Extraction", "~LSHA-QA",     "~LSHA-BK", 1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1830", "1830", "QA Extraction", "~LHCB-QA",     "~LHCB-BK", 1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1840", "1840", "QA Extraction", "~LBDR-QA",     "~LBDR-BK", 1,       "Manual",    "8501",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1850", "1850", "QA Extraction", "~LCRD-QA",     "~LCRD-BK", 1,       "Manual",    "8501",   "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1860", "1860", "Package", "~JO-.5G", "~FL", 0.5, "Manual", "8501", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1870", "1870", "Package", "~JO-.5G", "PPR0001", 1, "Backflush", "10", "8501" });
                dtAutoItemBOM.Rows.Add(new object[] { "1880", "1880", "Package", "~JO-.5G", "LBL0001", 1, "Backflush", "10", "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1890", "1890", "Package",       "~JO-1G",       "~FL",      1,       "Manual",    "6001",   "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1900", "1900", "Package",       "~JO-1G",       "PPR0001",  1,       "Backflush", "10",     "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1910", "1910", "Package",       "~JO-1G",       "LBL0001",  1,       "Backflush", "10",     "8501" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1920", "1920", "Package",       "~PG-JO-1G",    "~JO-1G",   1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1930", "1930", "Package",       "~PG-JO-1G",    "BOX0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1940", "1940", "Package",       "~PG-JO-1G",    "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1950", "1950", "Package",       "~PG-JOPK-1G",  "~JO-1G",   3,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1960", "1960", "Package",       "~PG-JOPK-1G",  "BOX0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "1970", "1970", "Package",       "~PG-JOPK-1G",  "LBL0001",  1,       "Backflush", "10",     "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1980", "1980", "Package", "~PG-JO-.5G", "~JO-.5G", 1, "Manual", "8501", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "1990", "1990", "Package", "~PG-JO-.5G", "BOX0001", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2000", "2000", "Package", "~PG-JO-.5G", "LBL0001", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2010", "2010", "Package", "~PG-JOPK-.5G", "~JO-.5G", 3, "Manual", "8501", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2020", "2020", "Package", "~PG-JOPK-.5G", "BOX0001", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2030", "2030", "Package", "~PG-JOPK-.5G", "LBL0001", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2040", "2040", "Package", "~PG-FL-1G", "~FL", 1, "Manual", "8501", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2050", "2050", "Package", "~PG-FL-1G", "PKG0010", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2060", "2060", "Package", "~PG-FL-1G", "LBL0001", 1, "Backflush", "10", "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2070", "2070", "Package",       "~PG-FL-2G",    "~FL",      2,       "Manual",    "8501",   "9001" });    
                //dtAutoItemBOM.Rows.Add(new object [] { "2080", "2080", "Package",       "~PG-FL-2G",    "PKG0020",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2090", "2090", "Package",       "~PG-FL-2G",    "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2100", "2100", "Package",       "~PG-FL-3.5G",  "~FL",      3,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2110", "2110", "Package",       "~PG-FL-3.5G",  "PKG0035",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2120", "2120", "Package",       "~PG-FL-3.5G",  "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2130", "2130", "Package",       "~PG-FL-4G",    "~FL",      4,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2140", "2140", "Package",       "~PG-FL-4G",    "PKG0040",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2150", "2150", "Package",       "~PG-FL-4G",    "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2160", "2160", "Package",       "~PG-FL-7G",    "~FL",      7,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2170", "2170", "Package",       "~PG-FL-7G",    "PKG0070",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2180", "2180", "Package",       "~PG-FL-7G",    "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2190", "2190", "Package",       "~PG-FL-28G",   "~FL",      28,      "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2200", "2200", "Package",       "~PG-FL-28G",   "PKG0280",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2210", "2210", "Package",       "~PG-FL-28G",   "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2220", "2220", "Package",       "~PG-FL-14G",   "~FL",      14,      "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2230", "2230", "Package",       "~PG-FL-14G",   "PKG0140",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2240", "2240", "Package",       "~PG-FL-14G",   "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2250", "2250", "Package",       "~PG-FL-227G",  "~FL",      227,     "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2260", "2260", "Package",       "~PG-FL-227G",  "PKG2270",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2270", "2270", "Package",       "~PG-FL-227G",  "LBL0001",  1,       "Backflush", "10",     "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2280", "2280", "Package", "~PG-FL-454G", "~FL", 454, "Manual", "8501", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2290", "2290", "Package", "~PG-FL-454G", "PKG4540", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2300", "2300", "Package", "~PG-FL-454G", "LBL0001", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2310", "2310", "Package", "~PG-KF-1G", "~KF-QA", 1, "Manual", "8501", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2320", "2320", "Package", "~PG-KF-1G", "PKG0010", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2330", "2330", "Package", "~PG-KF-1G", "LBL0001", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2340", "2340", "Package", "~PG-KF-.5G", "~KF-QA", 0.5, "Manual", "8501", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2350", "2350", "Package", "~PG-KF-.5G", "PKG0005", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2360", "2360", "Package", "~PG-KF-.5G", "LBL0001", 1, "Backflush", "10", "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2370", "2370", "Package",       "~PG-BH-1G",    "~BH-QA",   1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2380", "2380", "Package",       "~PG-BH-1G",    "PKG0010",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2390", "2390", "Package",       "~PG-BH-1G",    "LBL0001",  1,       "Backflush", "10",     "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2400", "2400", "Package", "~PG-BH-.5G", "~BH-QA", 0.5, "Manual", "8501", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2410", "2410", "Package", "~PG-BH-.5G", "PKG0005", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2420", "2420", "Package", "~PG-BH-.5G", "LBL0001", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2430", "2430", "Package", "~PG-HA-1G", "~HA-QA", 1, "Manual", "8501", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2440", "2440", "Package", "~PG-HA-1G", "PKG0010", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2450", "2450", "Package", "~PG-HA-1G", "LBL0001", 1, "Backflush", "10", "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2460", "2460", "Package",       "~PG-HA-.5G",   "~HA-QA",   0,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2470", "2470", "Package",       "~PG-HA-.5G",   "PKG0005",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2480", "2480", "Package",       "~PG-HA-.5G",   "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2490", "2490", "Package",       "~PG-HW-1G",    "~HW-QA",   1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2500", "2500", "Package",       "~PG-HW-1G",    "PKG0010",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2510", "2510", "Package",       "~PG-HW-1G",    "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2520", "2520", "Package",       "~PG-HW-.5G",   "~HW-QA",   0,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2530", "2530", "Package",       "~PG-HW-.5G",   "PKG0005",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2540", "2540", "Package",       "~PG-HW-.5G",   "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2550", "2550", "Package",       "~PG-CO2-1G",   "~CO2-QA",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2560", "2560", "Package",       "~PG-CO2-1G",   "CTG0010",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2570", "2570", "Package",       "~PG-CO2-1G",   "LBL0001",  1,       "Backflush", "10",     "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2580", "2580", "Package", "~PG-CO2-.5G", "~CO2-QA", 0.5, "Manual", "8501", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2590", "2590", "Package", "~PG-CO2-.5G", "CTG0005", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2600", "2600", "Package", "~PG-CO2-.5G", "LBL0001", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2610", "2610", "Package", "~PG-PEN-1G", "~CO2-QA", 1, "Manual", "8501", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2620", "2620", "Package", "~PG-PEN-1G", "PEN0001", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "2630", "2630", "Package", "~PG-PEN-1G", "LBL0001", 1, "Backflush", "10", "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2640", "2640", "Package",       "~PG-ROS-1G",   "~ROS-QA",  1,       "Manual",    "8501",   "9001" });    
                //dtAutoItemBOM.Rows.Add(new object [] { "2650", "2650", "Package",       "~PG-ROS-1G",   "PKG0010",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2660", "2660", "Package",       "~PG-ROS-1G",   "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2670", "2670", "Package",       "~PG-ROS-.5G",  "~ROS-QA",  0,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2680", "2680", "Package",       "~PG-ROS-.5G",  "PKG0005",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2690", "2690", "Package",       "~PG-ROS-.5G",  "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2700", "2700", "Package",       "~PG-RES-1G",   "~RES-QA",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2710", "2710", "Package",       "~PG-RES-1G",   "PKG0010",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2720", "2720", "Package",       "~PG-RES-1G",   "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2730", "2730", "Package",       "~PG-RES-.5G",  "~RES-QA",  0,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2740", "2740", "Package",       "~PG-RES-.5G",  "PKG0005",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2750", "2750", "Package",       "~PG-RES-.5G",  "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2760", "2760", "Package",       "~PG-DIS-1G",   "~DIS-QA",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2770", "2770", "Package",       "~PG-DIS-1G",   "PKG0010",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2780", "2780", "Package",       "~PG-DIS-1G",   "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2790", "2790", "Package",       "~PG-DIS-.5G",  "~DIS-QA",  0,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2800", "2800", "Package",       "~PG-DIS-.5G",  "PKG0005",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2810", "2810", "Package",       "~PG-DIS-.5G",  "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2820", "2820", "Package",       "~PG-SHA-1G",   "~SHA-QA",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2830", "2830", "Package",       "~PG-SHA-1G",   "PKG0010",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2840", "2840", "Package",       "~PG-SHA-1G",   "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2850", "2850", "Package",       "~PG-SHA-.5G",  "~SHA-QA",  0,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2860", "2860", "Package",       "~PG-SHA-.5G",  "PKG0005",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2870", "2870", "Package",       "~PG-SHA-.5G",  "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2880", "2880", "Package",       "~PG-HCB-1G",   "~HCB-QA",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2890", "2890", "Package",       "~PG-HCB-1G",   "PKG0010",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2900", "2900", "Package",       "~PG-HCB-1G",   "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2910", "2910", "Package",       "~PG-HCB-.5G",  "~HCB-QA",  0,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2920", "2920", "Package",       "~PG-HCB-.5G",  "PKG0005",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2930", "2930", "Package",       "~PG-HCB-.5G",  "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2940", "2940", "Package",       "~PG-BDR-1G",   "~BDR-QA",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2950", "2950", "Package",       "~PG-BDR-1G",   "PKG0010",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2960", "2960", "Package",       "~PG-BDR-1G",   "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2970", "2970", "Package",       "~PG-BDR-.5G",  "~BDR-QA",  0,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2980", "2980", "Package",       "~PG-BDR-.5G",  "PKG0005",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "2990", "2990", "Package",       "~PG-BDR-.5G",  "LBL0001",  1,       "Backflush", "10",     "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "3000", "3000", "Package", "~PG-CRD-1G", "~CRD-QA", 1, "Manual", "8501", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "3010", "3010", "Package", "~PG-CRD-1G", "PKG0010", 1, "Backflush", "10", "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "3020", "3020", "Package", "~PG-CRD-1G", "LBL0001", 1, "Backflush", "10", "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3030", "3030", "Package",       "~PG-CRD-.5G",  "~CRD-QA",  0,       "Manual",    "8501",   "9001" });    
                //dtAutoItemBOM.Rows.Add(new object [] { "3040", "3040", "Package",       "~PG-CRD-.5G",  "PKG0005",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3050", "3050", "Package",       "~PG-CRD-.5G",  "LBL0001",  1,       "Backflush", "10",     "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3060", "3060", "Package",       "~PG-ED-1P",    "~ED-QA",   1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3070", "3070", "Package",       "~PG-ED-3P",    "~ED-QA",   3,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3080", "3080", "Package",       "~PG-ED-6P",    "~ED-QA",   6,       "Manual",    "8501",   "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "3090", "3090", "Samples", "~SA-FL", "~FL", 1, "Manual", "8501", "100" });
                dtAutoItemBOM.Rows.Add(new object[] { "3100", "3100", "Samples", "~SA-TR", "~TR", 1, "Manual", "8501", "100" });
                dtAutoItemBOM.Rows.Add(new object[] { "3110", "3110", "Samples", "~SA-KF", "~KF-QA", 1, "Manual", "8501", "100" });
                dtAutoItemBOM.Rows.Add(new object[] { "3120", "3120", "Samples", "~SA-BH", "~BH-QA", 1, "Manual", "8501", "100" });
                dtAutoItemBOM.Rows.Add(new object[] { "3130", "3130", "Samples", "~SA-HA", "~HA-QA", 1, "Manual", "8501", "100" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3140", "3140", "Samples",       "~SA-HW",       "~HW-QA",   1,       "Manual",    "8501",   "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "3150", "3150", "Samples", "~SA-CO2", "~CO2-QA", 1, "Manual", "8501", "100" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3160", "3160", "Samples",       "~SA-FO",       "~FO-QA",   1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3170", "3170", "Samples",       "~SA-BU",       "~BU-QA",   1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3180", "3180", "Samples",       "~SA-CO",       "~CO2-QA",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3190", "3190", "Samples",       "~SA-ED",       "~ED-QA",   1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3200", "3200", "Samples",       "~SA-ROS",      "~ROS-QA",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3210", "3210", "Samples",       "~SA-RES",      "~RES-QA",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3220", "3220", "Samples",       "~SA-DIS",      "~DIS-QA",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3230", "3230", "Samples",       "~SA-SHA",      "~SHA-QA",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3240", "3240", "Samples",       "~SA-HCB",      "~HCB-QA",  1,       "Manual",    "8501",   "9001" });
                //dtAutoItemBOM.Rows.Add(new object [] { "3250", "3250", "Samples",       "~SA-BDR",      "~BDR-QA",  1,       "Manual",    "8501",   "9001" });
                dtAutoItemBOM.Rows.Add(new object[] { "3260", "3260", "Samples", "~SA-CRD", "~CRD-QA", 1, "Manual", "8501", "100" });

                return dtAutoItemBOM;

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static void DeleteAutoItems()
        {
            // OP #4577 - 25AUG2019
            // delete specific Items because the standard Items have changed

            var err = "";
            List<string> items = new List<string>();
            SAPbobsCOM.Items oITM = null;
            try
            {
                items.Add("~BDR-BK");
                items.Add("~BDR-QA");
                items.Add("~BU-BK");
                items.Add("~BU-QA");
                items.Add("~CO-BK");
                items.Add("~DIS-BK");
                items.Add("~DIS-QA");
                items.Add("~ED");
                items.Add("~ED-BK");
                items.Add("~ED-QA");
                items.Add("~FO-BK");
                items.Add("~FO-QA");
                items.Add("~FUFL");
                items.Add("~FUTR");
                items.Add("~HCB-BK");
                items.Add("~HCB-QA");
                items.Add("~HW-BK");
                items.Add("~HW-QA");
                items.Add("~JO-1G");
                items.Add("~LBDR-BK");
                items.Add("~LBDR-QA");
                items.Add("~LBH-BK");
                items.Add("~LBH-QA");
                items.Add("~LBU-BK");
                items.Add("~LCO2-BK");
                items.Add("~LCO2-QA");
                items.Add("~LCO-BK");
                items.Add("~LCRD-BK");
                items.Add("~LCRD-QA");
                items.Add("~LDIS-BK");
                items.Add("~LDIS-QA");
                items.Add("~LE-BK");
                items.Add("~LFO-BK");
                items.Add("~LHA-BK");
                items.Add("~LHA-QA");
                items.Add("~LHCB-BK");
                items.Add("~LHCB-QA");
                items.Add("~LHW-BK");
                items.Add("~LHW-QA");
                items.Add("~LKF-BK");
                items.Add("~LKF-QA");
                items.Add("~LRES-BK");
                items.Add("~LRES-QA");
                items.Add("~LROS-BK");
                items.Add("~LROS-QA");
                items.Add("~LSHA-BK");
                items.Add("~LSHA-QA");
                items.Add("~PG-BDR-.5G");
                items.Add("~PG-BDR-1G");
                items.Add("~PG-BH-1G");
                items.Add("~PG-CO2-1G");
                items.Add("~PG-CRD-.5G");
                items.Add("~PG-DIS-.5G");
                items.Add("~PG-DIS-1G");
                items.Add("~PG-ED-1P");
                items.Add("~PG-ED-3P");
                items.Add("~PG-ED-6P");
                items.Add("~PG-FL-113G");
                items.Add("~PG-FL-14G");
                items.Add("~PG-FL-227G");
                items.Add("~PG-FL-28G");
                items.Add("~PG-FL-2G");
                items.Add("~PG-FL-3.5G");
                items.Add("~PG-FL-4G");
                items.Add("~PG-FL-7G");
                items.Add("~PG-HA-.5G");
                items.Add("~PG-HCB-.5G");
                items.Add("~PG-HCB-1G");
                items.Add("~PG-HW-.5G");
                items.Add("~PG-HW-1G");
                items.Add("~PG-JO-1G");
                items.Add("~PG-JOPK-1G");
                items.Add("~PG-LE");
                items.Add("~PG-RES-.5G");
                items.Add("~PG-RES-1G");
                items.Add("~PG-ROS-.5G");
                items.Add("~PG-ROS-1G");
                items.Add("~PG-SHA-.5G");
                items.Add("~PG-SHA-1G");
                items.Add("~PG-TP");
                items.Add("~RES-BK");
                items.Add("~RES-QA");
                items.Add("~ROS-BK");
                items.Add("~ROS-QA");
                items.Add("~SA-BDR");
                items.Add("~SA-BU");
                items.Add("~SA-CO");
                items.Add("~SA-DIS");
                items.Add("~SA-ED");
                items.Add("~SA-FO");
                items.Add("~SA-HCB");
                items.Add("~SA-HW");
                items.Add("~SA-LE");
                items.Add("~SA-RES");
                items.Add("~SA-ROS");
                items.Add("~SA-SHA");
                items.Add("~SA-SNIFF");
                items.Add("~SHA-BK");
                items.Add("~SHA-QA");
                items.Add("CTG0010");
                items.Add("FAB0003");
                items.Add("FAB0005");
                items.Add("PKG0020");
                items.Add("PKG0035");
                items.Add("PKG0040");
                items.Add("PKG0070");
                items.Add("PKG0140");
                items.Add("PKG0280");
                items.Add("PKG1134");
                items.Add("PKG2270");
                items.Add("POT0007");
                items.Add("POT0015");

                foreach (var item in items)
                {
                    int ec = 0;
                    oITM = (SAPbobsCOM.Items)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                    if (oITM.GetByKey(item)) ec = oITM.Remove();
                    if (ec != 0) err += Environment.NewLine + item + "     " + Globals.oCompany.GetLastErrorDescription();
                }
                if (err != "") Globals.oApp.MessageBox("ERROR in DeleteAutoItems()" + Environment.NewLine + err);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oITM);
            }
        }

        private static void DeleteAutoBOMs()
        {
            // OP #4577 - 25AUG2019
            // delete specific BOMs because the standard BOMs have changed

            var err = "";
            List<string> items = new List<string>();

            SAPbobsCOM.ProductTrees oBOM = null;
            try
            {
                items.Add("~FUFL");
                items.Add("~FUTR");
                items.Add("~HW-BK");
                items.Add("~ROS-BK");
                items.Add("~RES-BK");
                items.Add("~DIS-BK");
                items.Add("~SHA-BK");
                items.Add("~HCB-BK");
                items.Add("~BDR-BK");
                items.Add("~ED");
                items.Add("~LKF-BK");
                items.Add("~LBH-BK");
                items.Add("~LHA-BK");
                items.Add("~LHW-BK");
                items.Add("~LCO2-BK");
                items.Add("~LROS-BK");
                items.Add("~LRES-BK");
                items.Add("~LDIS-BK");
                items.Add("~LSHA-BK");
                items.Add("~LHCB-BK");
                items.Add("~LBDR-BK");
                items.Add("~LCRD-BK");
                items.Add("~LFO-BK");
                items.Add("~LBU-BK");
                items.Add("~LCO-BK");
                items.Add("~HW-QA");
                items.Add("~ROS-QA");
                items.Add("~RES-QA");
                items.Add("~DIS-QA");
                items.Add("~SHA-QA");
                items.Add("~HCB-QA");
                items.Add("~BDR-QA");
                items.Add("~LKF-QA");
                items.Add("~LBH-QA");
                items.Add("~LHA-QA");
                items.Add("~LHW-QA");
                items.Add("~LCO2-QA");
                items.Add("~LROS-QA");
                items.Add("~LRES-QA");
                items.Add("~LDIS-QA");
                items.Add("~LSHA-QA");
                items.Add("~LHCB-QA");
                items.Add("~LBDR-QA");
                items.Add("~LCRD-QA");
                items.Add("~JO-1G");
                items.Add("~PG-JO-1G");
                items.Add("~PG-JOPK-1G");
                items.Add("~PG-FL-2G");
                items.Add("~PG-FL-3.5G");
                items.Add("~PG-FL-4G");
                items.Add("~PG-FL-7G");
                items.Add("~PG-FL-28G");
                items.Add("~PG-FL-14G");
                items.Add("~PG-FL-227G");
                items.Add("~PG-BH-1G");
                items.Add("~PG-HA-.5G");
                items.Add("~PG-HW-1G");
                items.Add("~PG-HW-.5G");
                items.Add("~PG-CO2-1G");
                items.Add("~PG-ROS-1G");
                items.Add("~PG-ROS-.5G");
                items.Add("~PG-RES-1G");
                items.Add("~PG-RES-.5G");
                items.Add("~PG-DIS-1G");
                items.Add("~PG-DIS-.5G");
                items.Add("~PG-SHA-1G");
                items.Add("~PG-SHA-.5G");
                items.Add("~PG-HCB-1G");
                items.Add("~PG-HCB-.5G");
                items.Add("~PG-BDR-1G");
                items.Add("~PG-BDR-.5G");
                items.Add("~PG-CRD-.5G");
                items.Add("~PG-ED-1P");
                items.Add("~SA-HW");
                items.Add("~SA-FO");
                items.Add("~SA-BU");
                items.Add("~SA-CO");
                items.Add("~SA-ED");
                items.Add("~SA-ROS");
                items.Add("~SA-RES");
                items.Add("~SA-DIS");
                items.Add("~SA-SHA");
                items.Add("~SA-HCB");
                items.Add("~SA-BDR");

                foreach (var item in items)
                {
                    int ec = 0;
                    oBOM = (SAPbobsCOM.ProductTrees)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductTrees);
                    if (oBOM.GetByKey(item)) ec = oBOM.Remove();
                    if (ec != 0) err += Environment.NewLine + item + "     " + Globals.oCompany.GetLastErrorDescription();
                }
                if (err != "") Globals.oApp.MessageBox("ERROR in DeleteAutoBOMs()" + Environment.NewLine + err);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oBOM);
            }
        }

        private static System.Data.DataTable GetWarehouseTable()
        {
            // Items Generated 09/08/2017 09:40:19 -06:00

            try
            {
                var dtWarehouse = new System.Data.DataTable();

                dtWarehouse.Columns.Add("WarehouseCode");
                dtWarehouse.Columns.Add("WarehouseName");
                dtWarehouse.Columns.Add("Nettable");
                dtWarehouse.Columns.Add("WarehouseType");
                dtWarehouse.Columns.Add("State");
                dtWarehouse.Columns.Add("Grow");
                dtWarehouse.Columns.Add("Processor");
                dtWarehouse.Columns.Add("Distributor");
                dtWarehouse.Columns.Add("Retail");
                dtWarehouse.Columns.Add("Facility");

                //                                   WarehouseCode WarehouseName        Nettable WarehouseType State  Grow Processor Distributor Retail Facility
                dtWarehouse.Rows.Add(new object[] { "10", "General Warehouse", "Y", "", "", "Y", "Y", "Y", "Y", "" });
                dtWarehouse.Rows.Add(new object[] { "100", "Samples", "N", "", "", "Y", "Y", "Y", "N", "" });
                dtWarehouse.Rows.Add(new object[] { "1001", "Seed Bank", "Y", "GER", "INV", "Y", "N", "N", "N", "Producer" });
                dtWarehouse.Rows.Add(new object[] { "2001", "Clone Bank 1", "Y", "CLO", "INV", "Y", "N", "N", "N", "Producer" });
                dtWarehouse.Rows.Add(new object[] { "3001", "Work Room 1", "Y", "WOR", "CUL", "Y", "N", "N", "N", "Producer" });
                dtWarehouse.Rows.Add(new object[] { "4001", "Veg 1", "Y", "VEG", "CUL", "Y", "N", "N", "N", "Producer" });
                dtWarehouse.Rows.Add(new object[] { "5001", "Flower 1", "Y", "FLO", "CUL", "Y", "N", "N", "N", "Producer" });
                dtWarehouse.Rows.Add(new object[] { "6001", "Harvest Room", "Y", "WET", "CUL", "Y", "N", "N", "N", "Producer" });
                dtWarehouse.Rows.Add(new object[] { "7001", "Drying 1", "Y", "DRY", "CUL", "Y", "N", "N", "N", "Producer" });
                dtWarehouse.Rows.Add(new object[] { "8001", "Curing 1", "Y", "CUR", "INV", "Y", "N", "N", "N", "Producer" });
                dtWarehouse.Rows.Add(new object[] { "8501", "Processing 1", "Y", "PRO", "INV", "Y", "Y", "N", "Y", "Processor" });
                dtWarehouse.Rows.Add(new object[] { "9001", "Finished Goods 1", "Y", "FIN", "INV", "Y", "Y", "Y", "Y", "Wholesaler" });
                dtWarehouse.Rows.Add(new object[] { "9996", "Water Tank", "Y", "WTR", "INV", "Y", "N", "N", "N", "" });
                dtWarehouse.Rows.Add(new object[] { "9997", "Sick Quarantine", "Y", "QNX", "INV", "Y", "Y", "N", "N", "Producer" });
                dtWarehouse.Rows.Add(new object[] { "9998", "Sales Quarantine", "Y", "QNS", "INV", "Y", "Y", "Y", "Y", "Wholesaler" });
                dtWarehouse.Rows.Add(new object[] { "9999", "Destruction", "Y", "QND", "INV", "Y", "Y", "Y", "Y", "" });

                return dtWarehouse;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static System.Data.DataTable GetFieldLabelTable()
        {
            var dtFieldLabel = new System.Data.DataTable();

            dtFieldLabel.Columns.Add("FormID");
            dtFieldLabel.Columns.Add("ItemID");
            dtFieldLabel.Columns.Add("ColumnID");
            dtFieldLabel.Columns.Add("Language");
            dtFieldLabel.Columns.Add("ItemString");
            dtFieldLabel.Columns.Add("IsBold");
            dtFieldLabel.Columns.Add("IsItalics");

            dtFieldLabel.Rows.Add(new object[] { "NSC_PLANDRYBUD", "LBL_WHSE", "-1", "English", "Send to Location:", "N", "N" });
            dtFieldLabel.Rows.Add(new object[] { "NSC_PLANDRYBUD", "LBL_WHSE", "-1", "Spanish (Latin America)", "Mandar localidad:", "N", "N" });
            dtFieldLabel.Rows.Add(new object[] { "136", "1470002221", "-1", "English", "Strain Facility ID", "N", "N" });   //  Company Details
            dtFieldLabel.Rows.Add(new object[] { "136", "1470002221", "-1", "Spanish (Latin America)", "ID de instalación de esfuerzo", "N", "N" });    //  Company Details

            return dtFieldLabel;
        }

        private static string GetDataTableMvSelectClause() => $"((Grow <> 'N' AND '{NSC_DI.UTIL.Settings.Value.Get("MV_Grow")}' = 'Y') OR (Processor <> 'N' AND '{NSC_DI.UTIL.Settings.Value.Get("MV_Processor")}' = 'Y') OR (Distributor <> 'N' AND '{NSC_DI.UTIL.Settings.Value.Get("MV_Distributor")}' = 'Y') OR (Retail <> 'N' AND '{NSC_DI.UTIL.Settings.Value.Get("MV_Retail")}' = 'Y'))";
        private static string GetDataTableBomMvSelectClause() => $"((Grow = 'M' AND '{NSC_DI.UTIL.Settings.Value.Get("MV_Grow")}' = 'Y') OR (Processor = 'M' AND '{NSC_DI.UTIL.Settings.Value.Get("MV_Processor")}' = 'Y') OR (Distributor = 'M' AND '{NSC_DI.UTIL.Settings.Value.Get("MV_Distributor")}' = 'Y') OR (Retail = 'M' AND '{NSC_DI.UTIL.Settings.Value.Get("MV_Retail")}' = 'Y'))";

        private static void FillAutoItem_NEW()
        {

            //--------------- 
            //	Auto Item
            //--------------- 
            SAPbobsCOM.Items oItem = null;
            SAPbobsCOM.UserTable oUserTable = null;
            try
            {
                //CheckAutoItemForInconsistencies();
                if (NSC_DI.UTIL.Settings.Value.Get("AutoItemsLoaded") == "Y") return;
                if (NSC_DI.UTIL.Settings.Value.Get("AddTemplates") == "N") return;

                Globals.oCompany.StartTransaction();

                var dtAutoItems = GetAutoItemTable();

                var whCode = GetWarehouseTable().Select(GetDataTableMvSelectClause()).Select(n => n["WarehouseCode"] as string);

                Func<string, SAPbobsCOM.BoYesNoEnum> YN = NSC_DI.UTIL.Misc.ToSAPYesNo;

                foreach (var dr in dtAutoItems.Select(GetDataTableMvSelectClause()))
                {
                    try
                    {
                        oItem = Globals.oCompany.GetBusinessObject(BoObjectTypes.oItems);

                        oItem.ItemCode = dr["ItemCode"] as string;

                        oItem.ItemName = dr["ItemName"] as string;

                        oItem.ItemsGroupCode = NSC_DI.UTIL.SQL.GetValue<int>($"SELECT ItmsGrpCod FROM OITB WHERE ItmsGrpNam = '{dr["ItemGroupName"]}'");

                        // add warehouses if they aren't listed for the item

                        var dtBOM = GetAutoBOMTable();

                        var parentWarehouses = dtBOM.Select($"ParentItemCode = '{oItem.ItemCode}'").Select(n => n["WarehouseCode"] as string);

                        var childWarehouses = dtBOM.Select($"ItemCode = '{oItem.ItemCode}'").Select(n => n["SourceWarehouseCode"] as string);

                        var warehouses = parentWarehouses.ToList();

                        warehouses.AddRange(childWarehouses);

                        warehouses.Add(dr["DefaultWarehouse"] as string);

                        if (oItem.ItemCode == "~")
                        {
                            warehouses.Add("4001"); // Veg 1
                            warehouses.Add("9997"); // sick quarantine
                        }
                        else if (oItem.ItemCode == "~DC")
                        {
                            warehouses.Add("7001"); // Drying 1
                        }
                        else if (oItem.ItemCode.StartsWith("~DU"))
                        {
                            warehouses.Add("8001"); // Curing 1
                        }

                        if ((dr["SalesItem"] as string) == "Y") warehouses.Add("9998"); // sales quarantine

                        warehouses.Add("9999"); // destruction


                        var warehousesToAdd = new SortedSet<string>(warehouses.Where(n => whCode.Contains(n)).Distinct());
                        if (!warehousesToAdd.Any(n => !n.StartsWith("999"))) warehousesToAdd.Add("10");

                        //for (var i = 0; i < oItem.WhsInfo.Count; i++)
                        //{
                        //    oItem.WhsInfo.SetCurrentLine(i);

                        //    if (warehousesToAdd.Contains(oItem.WhsInfo.WarehouseCode)) warehousesToAdd.Remove(oItem.WhsInfo.WarehouseCode);
                        //}

                        foreach (var wh in warehousesToAdd)
                        {
                            if (warehousesToAdd.First() != wh) oItem.WhsInfo.Add();

                            oItem.WhsInfo.WarehouseCode = wh;
                        }

                        if (whCode.Contains(dr["DefaultWarehouse"]))
                        {
                            oItem.DefaultWarehouse = dr["DefaultWarehouse"] as string;
                        }
                        else
                        {
                            oItem.DefaultWarehouse = "10";
                        }

                        oItem.ManageStockByWarehouse = SAPbobsCOM.BoYesNoEnum.tYES;

                        oItem.MaterialType = SAPbobsCOM.BoMaterialTypes.mt_FinishedGoods;

                        if (!string.IsNullOrEmpty(dr["Properties"] as string)) NSC_DI.SAP.Item.SetItemProperty(oItem, dr["Properties"] as string);

                        if (dr["UnitPrice"] != null) oItem.AvgStdPrice = (dr["UnitPrice"] as double?).Value;

                        oItem.UserFields.Fields.Item($"U_{Globals.SAP_PartnerCode}_StrainID").Value = string.Empty;
                        oItem.UserFields.Fields.Item($"U_{Globals.SAP_PartnerCode}_Batchable").Value = dr["Batchable"] as string;
                        oItem.UserFields.Fields.Item($"U_{Globals.SAP_PartnerCode}_Sampleable").Value = dr["Sampleable"] as string;
                        oItem.UserFields.Fields.Item($"U_{Globals.SAP_PartnerCode}_Processable").Value = dr["Processable"] as string;


                        if (oItem.ItemCode.StartsWith("~")) // an auto item
                        {
                            var isPurchaseItem = (dr["Grow"] as string == "B" && NSC_DI.UTIL.Settings.Value.Get("MV_Grow") == "Y")
                               || (dr["Processor"] as string == "B" && NSC_DI.UTIL.Settings.Value.Get("MV_Processor") == "Y")
                               || (dr["Distributor"] as string == "B" && NSC_DI.UTIL.Settings.Value.Get("MV_Distributor") == "Y")
                               || (dr["Retail"] as string == "B" && NSC_DI.UTIL.Settings.Value.Get("MV_Retail") == "Y");

                            var isByProduct = GetAutoBOMTable().Select().Any(n => n["ItemCode"] as string == oItem.ItemCode && n["Quantity"] as int? < 0);

                            oItem.InventoryItem = isByProduct ? BoYesNoEnum.tYES : BoYesNoEnum.tNO; // by products must be a inventory item for a template bom
                            oItem.PurchaseItem = BoYesNoEnum.tNO;
                            oItem.SalesItem = BoYesNoEnum.tNO;

                            // Set item to Inactive
                            oItem.Valid = BoYesNoEnum.tNO;
                            oItem.Frozen = BoYesNoEnum.tYES;

                            oUserTable = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tAutoItem);

                            oUserTable.Name = string.Empty;
                            oUserTable.UserFields.Fields.Item("U_ItemCode").Value = dr["ItemCode"];
                            oUserTable.UserFields.Fields.Item("U_InvItem").Value = dr["InventoryItem"];
                            oUserTable.UserFields.Fields.Item("U_PurItem").Value = dr["PurchaseItem"];
                            oUserTable.UserFields.Fields.Item("U_SalItem").Value = dr["SalesItem"];
                            oUserTable.UserFields.Fields.Item("U_ManageBatch").Value = dr["ManageBatchNumbers"];
                            oUserTable.UserFields.Fields.Item("U_ManageSN").Value = dr["ManageSerialNumbers"];

                            NSC_DI.SAP.Item.SetItemProperty(oItem, "Template");

                            if (oUserTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                        }
                        else // not an auto item
                        {
                            oItem.InventoryItem = YN(dr["InventoryItem"] as string);
                            oItem.PurchaseItem = YN(dr["PurchaseItem"] as string);
                            oItem.SalesItem = YN(dr["SalesItem"] as string);
                            oItem.ManageBatchNumbers = YN(dr["ManageBatchNumbers"] as string);
                            oItem.ManageSerialNumbers = YN(dr["ManageSerialNumbers"] as string);
                        }

                        if (oItem.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Item: {oItem.ItemCode}{Environment.NewLine}{NSC_DI.UTIL.Message.Format(ex)}");
                    }
                    finally
                    {
                        NSC_DI.UTIL.Misc.KillObject(oItem);
                        NSC_DI.UTIL.Misc.KillObject(oUserTable);
                        GC.Collect();
                    }
                }

                NSC_DI.UTIL.Settings.Value.Set("AutoItemsLoaded", "Y", "Indicates that all Auto Items have been created.", true);

                Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
            }
        }

        private static void CheckAutoItemForInconsistencies()
        {
            // Displays an Message box when there are Auto Items or BoMs activated for the microverticals that don't
            // have the right default warehouse

            var sb = new System.Text.StringBuilder();

            var warehouses = GetWarehouseTable().Select("NOT " + GetDataTableMvSelectClause());

            // Check Auto Items

            var autoItems = GetAutoItemTable().Select(GetDataTableMvSelectClause() + " AND DefaultWarehouse <> ''");

            var autoItemWhs = autoItems.Join(warehouses, n => n["DefaultWarehouse"] as string, m => m["WarehouseCode"] as string, (n, m) => n);

            var errorItemCodes = autoItemWhs.Select(n => n["ItemCode"] as string);

            if (errorItemCodes.Any())
            {
                sb.AppendLine("Items with Inconsistent Default Warehouses:");
                sb.AppendLine(string.Join(", ", errorItemCodes));
                sb.AppendLine();
            }

            // Check Auto Boms

            var autoItemForBom = GetAutoItemTable().Select(GetDataTableBomMvSelectClause());
            var ai = autoItems.ToLookup(n => n["ItemCode"] as string);
            var errorBoms = autoItemForBom.Where(n => !ai.Contains(n["ItemCode"] as string));

            if (errorBoms.Any())
            {
                sb.AppendLine("BOMs have missing child items in item master:");
                sb.AppendLine(string.Join(", ", errorBoms));
                sb.AppendLine();
            }

            var autoBom = GetAutoBOMTable().Select($"ParentItemCode IN ({String.Join(",", autoItemForBom.Select(n => "'" + n["ItemCode"] + "'").Distinct())}) AND SourceWarehouseCode <> ''");

            var autoBomSourceWhs = warehouses.Join(autoBom, n => n["WarehouseCode"] as string, m => m["SourceWarehouseCode"] as string, (n, m) => m);

            var errorBomSourceParents = autoBomSourceWhs.Select(n => n["ParentItemCode"]);

            if (errorBomSourceParents.Any())
            {
                sb.AppendLine("BOMs with Inconsistent Source Warehouse Codes:");
                sb.AppendLine(string.Join(", ", errorItemCodes));
            }

            var errors = sb.ToString();
            if (!string.IsNullOrEmpty(errors))
            {
                Globals.oApp.MessageBox(errors);
            }
        }

        private static void FillAutoBOM_NEW()
        {
            if (Globals.IsCannabis == false) return;
            if (NSC_DI.UTIL.Settings.Value.Get("AddTemplates") == "N") return;

            var dtAutoItem = GetAutoItemTable().Select(GetDataTableBomMvSelectClause());
            var dtAutoBOM = GetAutoBOMTable().Select($"ParentItemCode IN ('{string.Join("','", dtAutoItem.Select(n => n["ItemCode"] as string))}')");

            var whCode = GetWarehouseTable().Select(GetDataTableMvSelectClause()).Select(n => n["WarehouseCode"] as string);

            try
            {

                if (NSC_DI.UTIL.Settings.Value.Get("AutoBOMsLoaded") == "Y") return;

                //Globals.oCompany.StartTransaction();

                var parentItems = dtAutoBOM.Select(dr => dr["ParentItemCode"] as string).Distinct().ToList();

                var queue = new Queue<string>(dtAutoBOM.Select(dr => dr["ParentItemCode"] as string).Distinct().ToList());

                var addedBoms = new List<string>();

                while (queue.Any())
                {
                    var parentItem = queue.Dequeue();

                    var rows = dtAutoBOM.Where(n => n["ParentItemCode"] as string == parentItem);

                    if (rows.Any(n => parentItems.Contains(n["ItemCode"] as string) && !addedBoms.Contains(n["ItemCode"] as string)))
                    {
                        queue.Enqueue(parentItem);
                        continue;
                    }

                    var bomItems = rows.Select(dr => new NSC_DI.SAP.Item.BillOfMaterialItem()
                    {
                        IssueMethod = BoIssueMethod.im_Backflush,
                        ItemCode = dr["ItemCode"] as string,
                        Quantity = (double)(dr["Quantity"] as int?).Value,
                        SourceWarehouseCode = whCode.Contains(dr["SourceWarehouseCode"] as string) ? dr["SourceWarehouseCode"] as string : null
                    }).ToList();

                    var warehouseCode = rows.First()["WarehouseCode"] as string;

                    warehouseCode = whCode.Contains(warehouseCode) ? warehouseCode : null;


                    NSC_DI.SAP.Item.AddBOM(parentItem, bomItems, true, warehouseCode);

                    addedBoms.Add(parentItem);
                }

                // Add Purchase Item attribute to items without BOMs

                //var itemCodes = dtAutoItem.Where(n => !addedBoms.Contains(n["ItemCode"] as string) && n["PurchaseItem"] as string != "Y").Select(n => n["ItemCode"] as string);

                //NSC_DI.UTIL.SQL.RunSQLQuery($"UPDATE [@{NSC_DI.Globals.tAutoItem}] SET U_PurItem = 'Y' WHERE U_ItemCode IN ('{string.Join("','", itemCodes)}')");

                NSC_DI.UTIL.Settings.Value.Set("AutoBOMsLoaded", "Y", "Indicates that all Auto BOMs have been created.", true);

                //Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                //NSC_DI.UTIL.Misc.KillObject(sboTable);
            }
        }

        private static void FillAutoItem_OLD()
        {
            // Items Generated 08/03/2017 13:44:05 -06:00

            // fill tables if there only if table is empty

            //--------------- 
            //	Auto Item
            //--------------- 

            UserTable sboTable = null;
            SAPbobsCOM.Items sboItem = null;
            var sql = "SELECT ItmsGrpCod FROM OITB WHERE ItmsGrpNam = '{0}'";

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("AutoItemsLoaded") == "Y") return;
                if (NSC_DI.UTIL.SQL.GetValue<int>("SELECT COUNT(Code) FROM [@" + NSC_DI.Globals.tAutoItem + "]", 0) != 0) return;

                sboTable = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tAutoItem);
                var dtAutoItem = GetAutoItemTable();
                foreach (var dr in dtAutoItem.Select(GetDataTableMvSelectClause()))
                {
                    if (dr["ItemCode"].ToString().StartsWith("~"))
                    {
                        sboTable.Name = String.Empty;
                        sboTable.UserFields.Fields.Item("U_ItemCode").Value = dr["ItemCode"];
                        sboTable.UserFields.Fields.Item("U_ItemName").Value = dr["ItemName"];
                        sboTable.UserFields.Fields.Item("U_ItemGroupCode").Value = NSC_DI.UTIL.SQL.GetValue<string>(String.Format(sql, dr["ItemGroupName"]));
                        sboTable.UserFields.Fields.Item("U_ManageSN").Value = dr["ManageSerialNumbers"];
                        sboTable.UserFields.Fields.Item("U_ManageBatch").Value = dr["ManageBatchNumbers"];
                        sboTable.UserFields.Fields.Item("U_HasBOM").Value = dr["HasBillOfMaterials"];
                        sboTable.UserFields.Fields.Item("U_DefaultWH").Value = dr["DefaultWarehouse"];
                        sboTable.UserFields.Fields.Item("U_InvItem").Value = dr["InventoryItem"];
                        sboTable.UserFields.Fields.Item("U_PurItem").Value = dr["PurchaseItem"];
                        sboTable.UserFields.Fields.Item("U_SalItem").Value = dr["SalesItem"];
                        sboTable.UserFields.Fields.Item("U_UnitPrice").Value = dr["UnitPrice"];
                        sboTable.UserFields.Fields.Item("U_MV_Retail").Value = dr["Retail"] as string == "N" ? "N" : "Y";
                        sboTable.UserFields.Fields.Item("U_MV_Distributor").Value = dr["Distributor"] as string == "N" ? "N" : "Y";
                        sboTable.UserFields.Fields.Item("U_MV_Grow").Value = dr["Grow"] as string == "N" ? "N" : "Y";
                        sboTable.UserFields.Fields.Item("U_MV_Processor").Value = dr["Processor"] as string == "N" ? "N" : "Y";
                        sboTable.UserFields.Fields.Item("U_PropertyName").Value = dr["Properties"];
                        sboTable.UserFields.Fields.Item("U_Sampleable").Value = dr["Sampleable"];
                        sboTable.UserFields.Fields.Item("U_Batchable").Value = dr["Batchable"];
                        sboTable.UserFields.Fields.Item("U_Processable").Value = dr["Processable"];
                        sboTable.UserFields.Fields.Item("U_ProcessType").Value = dr["ProcessType"];

                        if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                    }
                    else
                    {
                        sboItem = Globals.oCompany.GetBusinessObject(BoObjectTypes.oItems);
                        if (sboItem.GetByKey(dr["ItemCode"].ToString()) != true)
                        {
                            sboItem.ItemCode = dr["ItemCode"] as string;
                            sboItem.ItemName = dr["ItemName"] as string;

                            sboItem.ItemsGroupCode = NSC_DI.UTIL.SQL.GetValue<int>(string.Format(sql, dr["ItemGroupName"]));

                            Func<string, SAPbobsCOM.BoYesNoEnum> YN = NSC_DI.UTIL.Misc.ToSAPYesNo;

                            sboItem.ManageSerialNumbers = YN(dr["ManageSerialNumbers"] as string);
                            sboItem.ManageBatchNumbers = YN(dr["ManageBatchNumbers"] as string);
                            sboItem.InventoryItem = YN(dr["InventoryItem"] as string);
                            sboItem.PurchaseItem = YN(dr["PurchaseItem"] as string);
                            sboItem.SalesItem = YN(dr["SalesItem"] as string);

                            if (!string.IsNullOrEmpty(dr["DefaultWarehouse"] as string)) sboItem.DefaultWarehouse = dr["DefaultWarehouse"] as string;

                            var propertyToSet = dr["Properties"] as string;
                            if (!string.IsNullOrEmpty(propertyToSet)) NSC_DI.SAP.Item.SetItemProperty(sboItem, propertyToSet);

                            if (sboItem.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                        }
                    }
                }

                NSC_DI.UTIL.Settings.Value.Set("AutoItemsLoaded", "Y", "Indicates that all Auto Items have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(sboTable);
            }
        }

        private static void FillAutoBOM_OLD()
        {
            // Items Generated 08/03/2017 13:44:05 -06:00
            if (Globals.IsCannabis == false) return;

            // fill tables if there only if table is empty

            UserTable sboTable = null;

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("AutoBOMsLoaded") == "Y") return;
                if (NSC_DI.UTIL.SQL.GetValue<int>("SELECT COUNT(Code) FROM [@" + NSC_DI.Globals.tAutoItemBOM + "]", 0) != 0) return;

                sboTable = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tAutoItemBOM);
                var dtAutoBOM = GetAutoBOMTable();
                foreach (System.Data.DataRow dr in dtAutoBOM.Rows)
                {
                    sboTable.Code = dr["Code"] as string;
                    sboTable.Name = dr["Name"] as string;
                    sboTable.UserFields.Fields.Item("U_Group").Value = dr["Group"];
                    sboTable.UserFields.Fields.Item("U_ParentItem").Value = dr["ParentItemCode"];
                    sboTable.UserFields.Fields.Item("U_ItemCode").Value = dr["ItemCode"];
                    sboTable.UserFields.Fields.Item("U_Quantity").Value = dr["Quantity"];
                    sboTable.UserFields.Fields.Item("U_IssueMethod").Value = dr["IssueMethod"];
                    if (!string.IsNullOrEmpty(dr["SourceWarehouseCode"] as string)) sboTable.UserFields.Fields.Item("U_SourceWH").Value = dr["SourceWarehouseCode"];

                    if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }


                NSC_DI.UTIL.Settings.Value.Set("AutoBOMsLoaded", "Y", "Indicates that all Auto BOMs have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(sboTable);
            }
        }

        private static void FillAutoStruct()
        {
            // Items Generated 07/12/2017 09:24:02 -06:00
            if (Globals.IsCannabis == false) return;

            // fill tables if there only if table is empty

            UserTable sboTable = null;

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("AddedAutoStruct") == "Y") return;

                sboTable = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tAutoStruct);

                // Strain ID

                sboTable.Code = "10010";
                sboTable.Name = "10010";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Strain ID";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Strain Code";
                sboTable.UserFields.Fields.Item("U_Position").Value = "1";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "10020";
                sboTable.Name = "10020";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Strain ID";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Number";
                sboTable.UserFields.Fields.Item("U_Position").Value = "2";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "-";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                // Strain Name

                sboTable.Code = "10110";
                sboTable.Name = "10110";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Strain Name";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Strain Name";
                sboTable.UserFields.Fields.Item("U_Position").Value = "1";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "10120";
                sboTable.Name = "10120";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Strain Name";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Item Group Name";
                sboTable.UserFields.Fields.Item("U_Position").Value = "2";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "-";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                // Item Code

                sboTable.Code = "10210";
                sboTable.Name = "10210";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Item Code";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Item Code";
                sboTable.UserFields.Fields.Item("U_Position").Value = "1";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "10220";
                sboTable.Name = "10220";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Item Code";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Strain ID";
                sboTable.UserFields.Fields.Item("U_Position").Value = "2";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "-";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                // Item Name

                sboTable.Code = "10230";
                sboTable.Name = "10230";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Item Name";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Strain Name";
                sboTable.UserFields.Fields.Item("U_Position").Value = "1";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "10240";
                sboTable.Name = "10240";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Item Name";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Item Name";
                sboTable.UserFields.Fields.Item("U_Position").Value = "2";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = " - ";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                // Batch

                sboTable.Code = "10310";
                sboTable.Name = "10310";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Batch";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Strain ID";
                sboTable.UserFields.Fields.Item("U_Position").Value = "1";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "10320";
                sboTable.Name = "10320";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Batch";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#BatchCode";
                sboTable.UserFields.Fields.Item("U_Position").Value = "2";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "-";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "10330";
                sboTable.Name = "10330";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Batch";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Number";
                sboTable.UserFields.Fields.Item("U_Position").Value = "3";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "-";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                // Sub Batch

                sboTable.Code = "10410";
                sboTable.Name = "10410";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Sub Batch";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#BatchID";
                sboTable.UserFields.Fields.Item("U_Position").Value = "1";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "10420";
                sboTable.Name = "10420";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Sub Batch";
                sboTable.UserFields.Fields.Item("U_Component").Value = "SB";
                sboTable.UserFields.Fields.Item("U_Position").Value = "2";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "-";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "10430";
                sboTable.Name = "10430";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Sub Batch";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Number";
                sboTable.UserFields.Fields.Item("U_Position").Value = "3";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "-";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                // Serial Plant

                sboTable.Code = "10510";
                sboTable.Name = "10510";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Serial Plant";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Strain ID";
                sboTable.UserFields.Fields.Item("U_Position").Value = "1";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "10520";
                sboTable.Name = "10520";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Serial Plant";
                sboTable.UserFields.Fields.Item("U_Component").Value = "P";
                sboTable.UserFields.Fields.Item("U_Position").Value = "2";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "-";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "10530";
                sboTable.Name = "10530";
                sboTable.UserFields.Fields.Item("U_Type").Value = "Serial Plant";
                sboTable.UserFields.Fields.Item("U_Component").Value = "#Number";
                sboTable.UserFields.Fields.Item("U_Position").Value = "3";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "-";
                sboTable.UserFields.Fields.Item("U_Suffix").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                NSC_DI.UTIL.Settings.Value.Set("AddedAutoStruct", "Y", "Indicates that all Auto Struct have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(sboTable);
            }
        }

        private static void FillPdoPhases()
        {
            if (Globals.IsCannabis == false) return;

            // fill tables if there only if table is empty

            UserTable sboTable = null;

            try
            {
                if (NSC_DI.UTIL.SQL.GetValue<int>("SELECT COUNT(Code) FROM [@" + NSC_DI.Globals.tPDOPhase + "]", 0) != 0) return;
                if (NSC_DI.UTIL.Settings.Value.Get("AddedPdoPhases") == "Y") return;

                sboTable = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tPDOPhase);

                sboTable.Code = "10";
                sboTable.Name = "Work Room";
                sboTable.UserFields.Fields.Item("U_CurrentPhase").Value = "WOR";
                sboTable.UserFields.Fields.Item("U_NextPhase").Value = "VEG";
                sboTable.UserFields.Fields.Item("U_CanWater").Value = "Y";
                sboTable.UserFields.Fields.Item("U_CanFeed").Value = "Y";
                sboTable.UserFields.Fields.Item("U_CanTreat").Value = "Y";
                sboTable.UserFields.Fields.Item("U_CanPrune").Value = "Y";
                sboTable.UserFields.Fields.Item("U_CanQuarantine").Value = "Y";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "20";
                sboTable.Name = "Vegetating";
                sboTable.UserFields.Fields.Item("U_CurrentPhase").Value = "VEG";
                sboTable.UserFields.Fields.Item("U_NextPhase").Value = "FLO";
                sboTable.UserFields.Fields.Item("U_CanWater").Value = "Y";
                sboTable.UserFields.Fields.Item("U_CanFeed").Value = "Y";
                sboTable.UserFields.Fields.Item("U_CanTreat").Value = "Y";
                sboTable.UserFields.Fields.Item("U_CanPrune").Value = "Y";
                sboTable.UserFields.Fields.Item("U_CanQuarantine").Value = "Y";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "30";
                sboTable.Name = "Flowering";
                sboTable.UserFields.Fields.Item("U_CurrentPhase").Value = "FLO";
                sboTable.UserFields.Fields.Item("U_NextPhase").Value = "WET";
                sboTable.UserFields.Fields.Item("U_CanWater").Value = "Y";
                sboTable.UserFields.Fields.Item("U_CanFeed").Value = "Y";
                sboTable.UserFields.Fields.Item("U_CanTreat").Value = "Y";
                sboTable.UserFields.Fields.Item("U_CanPrune").Value = "Y";
                sboTable.UserFields.Fields.Item("U_CanQuarantine").Value = "Y";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                NSC_DI.UTIL.Settings.Value.Set("AddedPdoPhases", "Y", "Indicates that all Production Order Phases have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(sboTable);
            }
        }

        private static void FillWarehouses()
        {
            SAPbobsCOM.Warehouses oWarehouse = null;
            AdminInfo adminInfo = null;
            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("AddedWarehouses") == "Y") return;

                var dtWarehouse = GetWarehouseTable();

                oWarehouse = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oWarehouses);
                var successfulErrors = new int[] { 0, -2035 };

                if (NSC_DI.UTIL.Settings.Value.Get("AddTemplates") == "N")   // if no template items are being added, then add WHs based on MV
                {
                    // Checks to see if the Warehouse has been set to always enter based on MV or it is used by an active AutoItem that it set as a Default Warehouse
                    var autoItems = GetAutoItemTable().Select(GetDataTableMvSelectClause() + " AND DefaultWarehouse <> ''");
                    var autoItemWcs = autoItems.Select(n => "'" + n["DefaultWarehouse"] + "'");
                    var autoBomWcs = GetAutoBOMTable().Select($"ParentItemCode IN ({String.Join(",", autoItems.Select(n => "'" + n["ItemCode"] + "'").Distinct())}) AND SourceWarehouseCode <> ''").Select(n => "'" + n["SourceWarehouseCode"] + "'");
                    var wcs = autoItemWcs.Concat(autoBomWcs).Distinct();
                }

                // var clause =  $"{GetDataTableMvSelectClause()} OR WarehouseCode IN ({String.Join(",", wcs)})";
                var clause = GetDataTableMvSelectClause();

                foreach (var dr in dtWarehouse.Select(clause))
                {
                    // see if it already exists
                    if (NSC_DI.UTIL.SQL.GetValue<string>($"SELECT WhsCode FROM OWHS WHERE WhsCode = '{dr["WarehouseCode"].ToString()}'", "") != "") continue;
                    oWarehouse.WarehouseCode = dr["WarehouseCode"] as string;
                    oWarehouse.WarehouseName = dr["WarehouseName"] as string;
                    oWarehouse.Nettable = NSC_DI.UTIL.Misc.ToSAPYesNo(dr["Nettable"] as string);
                    oWarehouse.UserFields.Fields.Item("U_NSC_WhrsType").Value = dr["WarehouseType"];
                    oWarehouse.UserFields.Fields.Item("U_NSC_State").Value = dr["State"];
                    oWarehouse.UserFields.Fields.Item("U_NSC_StateID").Value = dr["Facility"];

                    if (successfulErrors.Contains(oWarehouse.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }


                try
                {
                    // commented this code out because it never completed because setting SetItemsWarehouses failed.
                    //adminInfo                    = Globals.oCompService.GetAdminInfo();
                    //adminInfo.DefaultWarehouse   = "10";
                    //adminInfo.SetItemsWarehouses = BoYesNoEnum.tYES;
                    //Globals.oCompService.UpdateAdminInfo(adminInfo);

                    //oWarehouse.GetByKey("01"); // sap defined warehouse
                    //oWarehouse.Remove();
                }
                catch (Exception)
                {
                    // ignore exceptions
                }

                NSC_DI.UTIL.Settings.Value.Set("AddedWarehouses", "Y", "Indicates that all Warehouses have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oWarehouse);
                NSC_DI.UTIL.Misc.KillObject(adminInfo);
                GC.Collect();
            }
        }

        private static void FillStrainTypes()
        {
            if (Globals.IsCannabis == false) return;

            // fill tables if there only if table is empty

            UserTable sboTable = null;

            try
            {
                string sativaSQLVal = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT Name FROM [@NSC_STRAIN_TYPE] WHERE U_Prefix = 'S'");
                
                if (!string.IsNullOrEmpty(sativaSQLVal?.Trim()))// Whitespace-Change
                {
                    if (sativaSQLVal.ToUpper().Contains("BODY"))
                    {
                        NSC_DI.UTIL.Settings.Value.Set("AddedStrainTypes", "N", "Indicates that all Strain Types have been created.", true);
                        NSC_DI.UTIL.UDO.UserTableRemove(NSC_DI.Globals.tStrainType);
                        NSC_DI.UTIL.UDO.AddUserTable(NSC_DI.Globals.tStrainType, "Strain Type", BoUTBTableType.bott_NoObject);
                        NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrainType, "Prefix", "ItemCode Prefix", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 8);
                        NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrainType, "NextStrain", "Next Strain Number", BoFieldTypes.db_Numeric);
                        NSC_DI.UTIL.UDO.AddFieldToTable(NSC_DI.Globals.tStrainType, "Note", "Note", BoFieldTypes.db_Alpha, BoFldSubTypes.st_None, 254);
                    }
                }

                if (NSC_DI.UTIL.Settings.Value.Get("AddedStrainTypes") == "Y") return;

                sboTable = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tStrainType);

                sboTable.Code = "Sativa";
                sboTable.Name = "For the Mind";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "S";
                sboTable.UserFields.Fields.Item("U_NextStrain").Value = "1";
                sboTable.UserFields.Fields.Item("U_Note").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "Indica";
                sboTable.Name = "For the Body";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "I";
                sboTable.UserFields.Fields.Item("U_NextStrain").Value = "1";
                sboTable.UserFields.Fields.Item("U_Note").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "Hybrid";
                sboTable.Name = "A mix of both";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "H";
                sboTable.UserFields.Fields.Item("U_NextStrain").Value = "1";
                sboTable.UserFields.Fields.Item("U_Note").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "HybridSativa";
                sboTable.Name = "Sativa Dominant Hybrid";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "HS";
                sboTable.UserFields.Fields.Item("U_NextStrain").Value = "1";
                sboTable.UserFields.Fields.Item("U_Note").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "HybridIndica";
                sboTable.Name = "Indica Dominant Hybrid";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "HI";
                sboTable.UserFields.Fields.Item("U_NextStrain").Value = "1";
                sboTable.UserFields.Fields.Item("U_Note").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                sboTable.Code = "High CBD";
                sboTable.Name = "High CBD Strains";
                sboTable.UserFields.Fields.Item("U_Prefix").Value = "CBD";
                sboTable.UserFields.Fields.Item("U_NextStrain").Value = "1";
                sboTable.UserFields.Fields.Item("U_Note").Value = "";
                if (sboTable.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                NSC_DI.UTIL.Settings.Value.Set("AddedStrainTypes", "Y", "Indicates that all Strain Types have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(sboTable);
            }
        }

        private static void FillFieldLabels()
        {
            SAPbobsCOM.DynamicSystemStrings oLabels = null;

            try
            {
                var success = new int[] { 0, -2035 };
                var dt = GetFieldLabelTable();

                foreach (var dr in dt.Select())
                {
                    var language = BoSuppLangs.ln_English;
                    if (string.Equals(dr["Language"] as string, "Spanish (Latin America)", StringComparison.InvariantCultureIgnoreCase))
                    {
                        language = BoSuppLangs.ln_Spanish_La;
                    }

                    if (Globals.oCompany.language != language) continue;

                    try
                    {
                        oLabels = Globals.oCompany.GetBusinessObject(BoObjectTypes.oDynamicSystemStrings);

                        oLabels.FormID = dr["FormID"] as string;
                        oLabels.ItemID = dr["ItemID"] as string;
                        oLabels.ColumnID = dr["ColumnID"] as string;
                        oLabels.ItemString = dr["ItemString"] as string;
                        oLabels.IsBold = NSC_DI.UTIL.Misc.ToSAPYesNo(dr["IsBold"] as string);
                        oLabels.IsItalics = NSC_DI.UTIL.Misc.ToSAPYesNo(dr["IsItalics"] as string);

                        var retCode = oLabels.Add();

                        if (success.Contains(retCode) == false)
                        {
                            throw new Exception(NSC_DI.UTIL.Message.Format(retCode + " : " + Globals.oCompany.GetLastErrorDescription()));
                        }
                    }
                    finally
                    {
                        NSC_DI.UTIL.Misc.KillObject(oLabels);
                        GC.Collect();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private static void FillItemGroups_OLD()
        {
            // This is the old list of Item Groups. Will delete after a while

            ItemGroups oItmGrp = null;

            var successfulErrors = new int[] { 0, -2035 };

            try
            {
                //if (NSC_DI.UTIL.SQL.GetValue<int>("SELECT COUNT(ItmsGrpCod) FROM OITB", 0) > 1 && pForceLoad == false) return;
                if (NSC_DI.UTIL.Settings.Value.Get("AddItemGroups") == "Y") return;


                oItmGrp = Globals.oCompany.GetBusinessObject(BoObjectTypes.oItemGroups);

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Additives";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Clone";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Fungicides";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Growing Accessories";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Labor";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Lights";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Mature Plant";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Office Equipment";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Office Supplies";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Pesticides";
                oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = "PacX";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Seed";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Waste";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Warehouse Equipment";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Warehouse Supplies";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                if (Globals.IsCannabis == false) return;

                //-----------------------------------------------------  CANNABIS ----------------------------------------------------

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Bubble Hash";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Bubble Hash Lot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Bubble Hash Sample";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Capsules";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "CO2 Hash Oil";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "CO2 Hash Oil Lot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "CO2 Hash Oil Sample";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Consolidated waste";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Dried Cannabis";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Dried Trim";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Farm Equip";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Finished Good";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Flower";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Flower Lot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Food Grade SolventEx";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Hash";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Hydrocarbon Wax";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Hydrocarbon Wax Lot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Hydrocarbon Wax Samp";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Infused Butter/Fat";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Infused Cooking Oil";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Janitorial";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Kief";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Keif Lot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Keif Sample";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Distillate";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Distillate Lot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Distillate Sample";
                oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = "QCLot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Edible Solid";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Edible Solid Lot";
                oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = "QCLot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Edible Solid Samp";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Extract Inhale";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Ext Inhale Lot";
                oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = "QCLot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Ext Inhale Sample";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Infused Topicals";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Inf Topicals Lot";
                oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = "QCLot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Inf Top Sample";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Liquid Edible";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Liquid Edible Lot";
                oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = "QCLot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "MJ Liq Edible Sample";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Nutrients";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Other Material Lot";
                oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = "QCLot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Other Material Sampl";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Other Plant Material";
                oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = "PacC";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Plant Tissue";
                oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = "PacM";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Sample Jar";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Trim Lot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Trim Sample";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Usable MJ";
                oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = "FgFl";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Wet Flower";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Wet Cannabis";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Wet Cannabis Lot";
                oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = "QCLot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Wet Cannabis Sample";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Wet Trim";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Wet Trim Lot";
                oItmGrp.UserFields.Fields.Item("U_NSC_CanType").Value = "QCLot";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                // BEGIN GENERATED ITEM
                oItmGrp.GroupName = "Wet Trim Sample";
                if (successfulErrors.Contains(oItmGrp.Add()) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                // END GENERATED ITEM

                NSC_DI.UTIL.Settings.Value.Set("AddItemGroups", "Y", "Indicates that all Item Groups have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItmGrp);
                GC.Collect();
            }
        }

        private static void FillInvTypeXFer()
        {
            if (Globals.IsCannabis == false) return;

            // fill tables if there only if table is empty


            UserTable oUDT = null;

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("AddedInvTypeXFer") == "Y") return;

                // delete any existing records
                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tInvTypXFer);

                var dt = NSC_DI.UTIL.SQL.DataTable($"SELECT Code FROM [@{NSC_DI.Globals.tInvTypXFer}]");
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    var code = row[0].ToString();

                    if (oUDT.GetByKey(code) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                    //if (oUDT.Remove() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                    oUDT.Remove();
                }

                // the "Remove" is not working
                var sql = $"EXEC('TRUNCATE TABLE [@{NSC_DI.Globals.tInvTypXFer}]')";
                NSC_DI.UTIL.SQL.RunSQLQuery(sql);

                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tInvTypXFer);

                // changed to apply for all Compliance systems
                // add the values based on the Compliance system
                //switch (NSC_DI.UTIL.Settings.Version.GetCompliance())
                //{
                //    case "BIOTRACK":
                oUDT.Code = "0";
                oUDT.Name = "Other";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "1";
                oUDT.Name = "Waste";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                //if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                oUDT.Add();

                oUDT.Code = "2";
                oUDT.Name = "Unhealthy or Died";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                //if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                oUDT.Add();

                oUDT.Code = "3";
                oUDT.Name = "Infestation";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                //if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                oUDT.Add();

                oUDT.Code = "4";
                oUDT.Name = "Product Return";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                //if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                oUDT.Add();

                oUDT.Code = "5";
                oUDT.Name = "Mistake";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                //if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                oUDT.Add();

                oUDT.Code = "6";
                oUDT.Name = "Spoilage";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                //if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                oUDT.Add();

                oUDT.Code = "7";
                oUDT.Name = "Quality Control";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                //if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                oUDT.Add();

                oUDT.Code = "8";
                oUDT.Name = "Sick";
                //oUDT.UserFields.Fields.Item("U_QuarState").Value = "SICK";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                oUDT.Add();
                //        break;

                //    default:

                //        oUDT.Code = "Dest";
                //        oUDT.Name = "Destroy";
                //        oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                //        if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                //        oUDT.Code = "Sick";
                //        oUDT.Name = "Sick";
                //        oUDT.UserFields.Fields.Item("U_QuarState").Value = "SICK";
                //        if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                //        break;
                //}

                NSC_DI.UTIL.Settings.Value.Set("AddedInvTypeXFer", "Y", "Indicates that all AddedInvTypeXFer have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
            }
        }

        private static void FillDestructTypes()
        {
            if (Globals.IsCannabis == false) return;

            // fill tables if there only if table is empty

            UserTable oUDT = null;

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("AddedDestructTypes") == "Y") return;

                // delete any existing records
                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tDestructTypes);

                var dt = NSC_DI.UTIL.SQL.DataTable($"SELECT Code FROM [@{NSC_DI.Globals.tDestructTypes}]");
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    var code = row[0].ToString();

                    if (oUDT.GetByKey(code))
                    {
                        oUDT.Remove();
                    }
                }

                // ************** MAKE THESE THE STANDARD OPTIONS FOR ALL ***************
                NSC_DI.UTIL.SQL.RunSQLQuery($"EXEC('DELETE FROM [@{NSC_DI.Globals.tDestructTypes}]')");
                // add the values based on the Compliance system
                //switch (NSC_DI.UTIL.Settings.Version.GetCompliance())
                //{
                //    case "BIOTRACK":
                oUDT.Code = "0";
                oUDT.Name = "Other";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "1";
                oUDT.Name = "Waste";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "2";
                oUDT.Name = "Unhealthy or Died";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "3";
                oUDT.Name = "Infestation";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "4";
                oUDT.Name = "Product Return";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "5";
                oUDT.Name = "Mistake";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "6";
                oUDT.Name = "Spoilage";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "7";
                oUDT.Name = "Quality Control";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                //        break;

                //    default:
                //        oUDT.Code = "Dest";
                //        oUDT.Name = "Destroy";
                //        oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                //        if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                //        break;
                //}

                NSC_DI.UTIL.Settings.Value.Set("AddedDestructTypes", "Y", "Indicates that all AddedDestructTypes have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
            }
        }

        private static void FillAdjustmentType()
        {
            if (Globals.IsCannabis == false) return;

            // fill tables if there only if table is empty

            UserTable oUDT = null;

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("FillAdjustmentType") == "Y") return;

                // Get the Compliance Method
                var compMeth = NSC_DI.UTIL.Settings.Value.Get("State Compliance").ToUpper();
                if (compMeth == "NONE") return;

                // delete any existing records
                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tInvAdjustTypes);

                var dt = NSC_DI.UTIL.SQL.DataTable($"SELECT Code FROM [@{NSC_DI.Globals.tInvAdjustTypes}]");
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    var code = row[0].ToString();
                    if (oUDT.GetByKey(code)) oUDT.Remove();
                }

                // AdjustmentType

                // the above code does not work on this table. So.......
                NSC_DI.UTIL.SQL.RunSQLQuery($"EXEC('DELETE FROM [@{NSC_DI.Globals.tInvAdjustTypes}]')", null);

                if (compMeth == "BIOTRACK")
                {
                    oUDT.Code = "A";
                    oUDT.Name = "General Inventory Audit";
                    oUDT.UserFields.Fields.Item("U_CompCode").Value = "1";
                    if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                    oUDT.Code = "T";
                    oUDT.Name = "Theft";
                    oUDT.UserFields.Fields.Item("U_CompCode").Value = "2";
                    if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                    oUDT.Code = "S";
                    oUDT.Name = "Seizure by Federal, State, Local or Tribal Law Enforcement";
                    oUDT.UserFields.Fields.Item("U_CompCode").Value = "3";
                    if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                    oUDT.Code = "M";
                    oUDT.Name = "Correcting a Mistake";
                    oUDT.UserFields.Fields.Item("U_CompCode").Value = "4";
                    if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                    oUDT.Code = "ML";
                    oUDT.Name = "Moisture loss, e.g. wet other plant material";
                    oUDT.UserFields.Fields.Item("U_CompCode").Value = "5";
                    if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                    oUDT.Code = "D";
                    oUDT.Name = "Depletion";
                    oUDT.UserFields.Fields.Item("U_CompCode").Value = "6";
                    if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }

                if (compMeth == "METRC")
                {
                }

                NSC_DI.UTIL.Settings.Value.Set("FillAdjustmentType", "Y", "Indicates that all AdjustmentTypes have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
            }
        }

        private static void FillComplianceType()
        {
            if (Globals.IsCannabis == false) return;

            // fill tables if there only if table is empty

            UserTable oUDT = null;

            try
            {
                // Get the Compliance Method
                //var compMeth = NSC_DI.UTIL.Settings.Value.Get("State Compliance").ToUpper();
                //if (compMeth == "NONE") return;

                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tCompTypes);

                if (NSC_DI.UTIL.Settings.Value.Get("AddedComplianceType") == "Y")
                {
                    // add any additional values here
                    oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tCompTypes);
                    oUDT.Code = "plantbatch_package";
                    oUDT.Name = "plantbatch_package";
                    var err = oUDT.Add();
                    if (err == 0 || err == -2035) return;
                    throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }

                // delete any existing records
                var dt = NSC_DI.UTIL.SQL.DataTable($"SELECT Code FROM [@{NSC_DI.Globals.tCompTypes}]");
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    var code = row[0].ToString();
                    if (oUDT.GetByKey(code)) oUDT.Remove();
                }

                // the above code does not work on this table (if there are references to the values?). So.......
                NSC_DI.UTIL.SQL.RunSQLQuery($"EXEC('DELETE FROM [@{NSC_DI.Globals.tCompTypes}]')");

                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tCompTypes);
                oUDT.Code = "Inventory_split";
                oUDT.Name = "Inventory_split";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tCompTypes);
                oUDT.Code = "plant_waste_weigh";
                oUDT.Name = "plant_waste_weigh";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tCompTypes);
                oUDT.Code = "inventory_transfer_inbound";
                oUDT.Name = "inventory_transfer_inbound";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tCompTypes);
                oUDT.Code = "plantbatch_package";
                oUDT.Name = "plantbatch_package";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                NSC_DI.UTIL.Settings.Value.Set("AddedComplianceType", "Y", "Indicates that all Compliance Types have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
            }
        }

        private static void FillQuarantineType()
        {
            if (Globals.IsCannabis == false) return;

            // fill tables if there only if table is empty

            UserTable oUDT = null;

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("AddedQuarantineType") == "Y") return;

                // delete any existing records
                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tQuarantineType);

                var dt = NSC_DI.UTIL.SQL.DataTable($"SELECT Code FROM [@{NSC_DI.Globals.tQuarantineType}]");
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    var code = row[0].ToString();

                    if (oUDT.GetByKey(code))
                    {
                        oUDT.Remove();
                    }
                }

                // ************** MAKE THESE THE STANDARD OPTIONS FOR ALL ***************
                // add the values based on the Compliance system
                //switch (NSC_DI.UTIL.Settings.Version.GetCompliance())
                //{
                //    case "BIOTRACK":
                oUDT.Code = "0";
                oUDT.Name = "Other";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "1";
                oUDT.Name = "Waste";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "2";
                oUDT.Name = "Unhealthy or Died";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "3";
                oUDT.Name = "Infestation";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "4";
                oUDT.Name = "Product Return";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "5";
                oUDT.Name = "Mistake";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "6";
                oUDT.Name = "Spoilage";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "7";
                oUDT.Name = "Quality Control";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                oUDT.Code = "8";
                oUDT.Name = "Sick";
                oUDT.UserFields.Fields.Item("U_QuarState").Value = "SICK";
                if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                //break;

                //    default:

                //        oUDT.Code = "Dest";
                //        oUDT.Name = "Destroy";
                //        oUDT.UserFields.Fields.Item("U_QuarState").Value = "DEST";
                //        if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));

                //        oUDT.Code = "Sick";
                //        oUDT.Name = "Sick";
                //        oUDT.UserFields.Fields.Item("U_QuarState").Value = "SICK";
                //        if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                //        break;
                //}

                NSC_DI.UTIL.Settings.Value.Set("AddedQuarantineType", "Y", "Indicates that all AddedInvTypeXFer have been created.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
            }
        }

        private static void FillTestList()
        {
            if (Globals.IsCannabis == false) return;

            GeneralService oGeneralService = null;
            GeneralData oGeneralData = null;
            // UserTable oUDT = null;

            var NextCode = NSC_DI.UTIL.SQL.GetNextAvailableCodeForItem(NSC_DI.Globals.tTests, "Code");
            var oCompService = Globals.oCompany.GetCompanyService();

            oGeneralService = oCompService.GetGeneralService(NSC_DI.Globals.tTests + "_C");
            oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

            try
            {
                if (NSC_DI.UTIL.Settings.Value.Get("AddedTestList") == "Y") return;

                //oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tTests);

                // add the values based on the Compliance system
                switch (NSC_DI.UTIL.Settings.Version.GetCompliance())
                {
                    case "BIOTRACK":
                        oGeneralData.SetProperty("Code", NextCode);
                        oGeneralData.SetProperty("U_Name", "Biotrack Analysis");
                        oGeneralService.Add(oGeneralData);
                        //if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));                       
                        break;
                    case "METRC":
                        oGeneralData.SetProperty("Code", NextCode);
                        oGeneralData.SetProperty("U_Name", "Metrc Analysis");
                        oGeneralService.Add(oGeneralData);
                        //if (oUDT.Add() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                        break;
                }
                NSC_DI.UTIL.Settings.Value.Set("AddedTestList", "Y", "Indicates that Compliance Test was added to Test List.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oGeneralData);
                NSC_DI.UTIL.Misc.KillObject(oGeneralService);
            }
        }

        private static void FillProdCat()
        {
            if (Globals.IsCannabis == false) return;

            // fill tables if there only if table is empty

            UserTable oUDT = null;

            try
            {              
                if (NSC_DI.UTIL.Settings.Value.Get("FillProdCat") == "Y") return; // delete the the second part of this if statement afte the version 46.05 goes up

                // delete any existing records
                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tProdCategory);

                var dt = NSC_DI.UTIL.SQL.DataTable($"SELECT Code FROM [@{NSC_DI.Globals.tProdCategory}]");
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    var code = row[0].ToString();

                    if (oUDT.GetByKey(code) == false) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                    //if (oUDT.Remove() != 0) throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                    oUDT.Remove();
                }

                // the "Remove" is not working
                var sql = $"EXEC('TRUNCATE TABLE [@{NSC_DI.Globals.tProdCategory}]')";
                NSC_DI.UTIL.SQL.RunSQLQuery(sql);

                oUDT = Globals.oCompany.UserTables.Item(NSC_DI.Globals.tProdCategory);            

                oUDT.Code = "01";
                oUDT.Name = "Accessory";
                oUDT.Add();
                 
                oUDT.Code = "02";
                oUDT.Name = "CBD";           
                oUDT.Add();

                oUDT.Code = "03";
                oUDT.Name = "Drink";
                oUDT.Add();

                oUDT.Code = "04";
                oUDT.Name = "Edible";              
                oUDT.Add();

                oUDT.Code = "05";
                oUDT.Name = "Extract";
                oUDT.Add();

                oUDT.Code = "06";
                oUDT.Name = "Flower";
                oUDT.Add();

                oUDT.Code = "07";
                oUDT.Name = "Grow";
                oUDT.Add();

                oUDT.Code = "08";
                oUDT.Name = "Merchandise";
                oUDT.Add();

                oUDT.Code = "09";
                oUDT.Name = "Pre-roll";
                oUDT.Add();

                oUDT.Code = "10";
                oUDT.Name = "Tincture";
                oUDT.Add();

                oUDT.Code = "11";
                oUDT.Name = "Topical";
                oUDT.Add();

                oUDT.Code = "12";
                oUDT.Name = "Vape";
                oUDT.Add();


                NSC_DI.UTIL.Settings.Value.Set("FillProdCat", "Y", "Indicates that Product Category valid values have been added.", true);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oUDT);
            }
        }

        internal static void WaitForSettingsToComplete()
        {
            // Waits for about 5 minutes then keeps going if the user didn't create the settings table
            int ticks = (5 * 60 * 2);

            while ((NSC_DI.UTIL.UDO.IsFieldInTable("@" + NSC_DI.Globals.tSettings, "Description") == false) && ticks > 0)
            {
                System.Threading.Thread.Sleep(500);
                ticks--;
            }
        }

        private static void RebuildStrainTable()
        {
            // 9988 Takes the MasterData NSC_STRAIN DB, moves its data to temp table of NoObj type and then removes NSC_STRAIN DB and rebuilds it as a NoObj type db and places the data back
            try
            {
                if (Globals.IsCannabis == false || NSC_DI.UTIL.Settings.Value.Get("StrainsDBTransfered") == "Y")
                    return; // sees if this function has been completed before and only runs if it has not.
                //Globals.oCompany.StartTransaction(); //THIS IS CAUSING A LOCK CONFLICT TO OCCUR AND WILL NOT COMPLETE

                CreateAndAddFieldsToTable("NSC_Temp_Table", NSC_DI.Globals.tStrains, "Temp Table", BoUTBTableType.bott_NoObject);
                InsertIntoTable(NSC_DI.Globals.tStrains, "NSC_Temp_Table", "U_StrainName", "CreateDate");
                NSC_DI.UTIL.UDO.UDO_Remove("NSC_Strain");
                NSC_DI.UTIL.UDO.UserTableRemove("NSC_Strain");
                CreateAndAddFieldsToTable(NSC_DI.Globals.tStrains, "NSC_Temp_Table", "Strains", BoUTBTableType.bott_NoObject);
                InsertIntoTable("NSC_Temp_Table", NSC_DI.Globals.tStrains, "U_StrainName", "CreateDate");

                NSC_DI.UTIL.Settings.Value.Set("StrainsDBTransfered", "Y", "Indicates that Strain DB has been converted to NoObject.", true);
                //Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);                              
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction)
                    Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
            }
        }

        private static void InsertIntoTable(string pFromTableName, string pToTableName, string pNameValField = "", string pCreateDateValField = "")
        {
            // name and createDeate params are used to help assign values to the new table that wont get set correctly otherwise
            // 9988 Takes in a from, to table and potentially a nameVal
            UserTable sboTable = null;
            try
            {
                if (pCreateDateValField != "") // checks to see if the table needs to have a createdate udf
                    NSC_DI.UTIL.UDO.AddFieldToTable(pToTableName, "CreateDate", "Create Date", BoFieldTypes.db_Date); // creates createdate udf
                // 9988 creates a table, datatable, and finds out what data type the totable is
                sboTable = Globals.oCompany.UserTables.Item(pToTableName); 
                string SQL = $"SELECT * FROM [@{pFromTableName}]";
                var fromTblType = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT OUTB.ObjectType FROM OUTB WHERE TableName = '{pToTableName}'", 0);
                var dt = NSC_DI.UTIL.SQL.DataTable(SQL);
                string checkForError;    // here for debugging, probably can be removed later            

                // 9988 go through each row get the vals and inserts into table
                foreach (System.Data.DataRow row in dt.Rows)
                {
                    checkForError = "Unassigned"; // here for debugging, probably can be removed later
                    if (fromTblType != "5") // if the datatype is not an auto incremeant it sets the val
                        sboTable.Code = row["Code"].ToString(); 
                    if (pNameValField != "") // if the nameValField is passed in it sets the name to that param
                        sboTable.Name = row[pNameValField].ToString();
                    if (pToTableName == "NSC_Temp_Table" && pCreateDateValField != "")
                        sboTable.UserFields.Fields.Item("U_CreateDate").Value = row[pCreateDateValField].ToString(); // if totbl is temp && insertCreateDate, then it needs to be explicitly assigned otherwise it will just happen in the columns foreach
                    foreach (System.Data.DataColumn col in dt.Columns)
                    {
                        var colName = col.ColumnName;
                        if (colName.Substring(0, 2) != "U_") // skips non userdefined fields
                            continue;
                        // if there is a null val it simply does nothing and a null is placed in the new table. This was done because it keeps consistancy between the original table and the newly created table and it also solved some issues we had with how the cloud version was handling null vals
                        try
                        {
                            string rowVal = row[colName].ToString().Trim();
                            if (!String.IsNullOrEmpty(rowVal))
                            {
                                checkForError = "Val Assigned"; // here for debugging, probably can be removed later
                                sboTable.UserFields.Fields.Item(colName).Value = rowVal;
                            }                                                       
                        }
                        catch(Exception ex)
                        {
                            // gives a more specific error and more detail. done because the cloud and local version were having different results
                            throw new Exception(NSC_DI.UTIL.Message.Format($@"
Here are the vals going into AddFieldToTable: 
Sbo Item: {colName} 
Row Value: {row[colName].ToString()}
Assigned Null: {checkForError}
{ex.Message}"));
                        }
                        
                    }
                    if (sboTable.Add() != 0)
                        throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
                }                
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(sboTable);
            }
        }

        private static void CreateAndAddFieldsToTable(string pToTableName, string pFromTableName, string pNewTableDescr, BoUTBTableType pTableDateType)
        {
            // 9988 creates new table and then creates fields based on another table's user defined fields
            Recordset oRS = null;
            try
            {
                if(pFromTableName != "NSC_Temp_Table" && NSC_DI.UTIL.UDO.TableExists("NSC_Temp_Table")) // if the from table is not the temp table and the temp table exists, it gets deleted
                {
                    NSC_DI.UTIL.UDO.UDO_Remove("NSC_Temp_Table");
                    NSC_DI.UTIL.UDO.UserTableRemove("NSC_Temp_Table");
                }
                NSC_DI.UTIL.UDO.AddUserTable(pToTableName, pNewTableDescr, pTableDateType); // creates the totable
                string CUFDSQL = $@"SELECT FieldID, AliasID, Descr, TypeID, EditType, SizeID, EditSize, Dflt
                                FROM [CUFD]
                                WHERE [CUFD].TableID = '@{pFromTableName}'"; 
                var dt = NSC_DI.UTIL.SQL.DataTable(CUFDSQL); // gets user defined fields from the fromtable
                foreach (System.Data.DataRow row in dt.Rows) // gets field type
                {
                    BoFieldTypes fieldsType;
                    switch (row["TypeID"].ToString().Trim().ToUpper())
                    {
                        case "A":
                            fieldsType = BoFieldTypes.db_Alpha;
                            break;
                        case "M":
                            fieldsType = BoFieldTypes.db_Memo;
                            break;
                        case "N":
                            fieldsType = BoFieldTypes.db_Numeric;
                            break;
                        case "D":
                            fieldsType = BoFieldTypes.db_Date;
                            break;
                        case "B":
                            fieldsType = BoFieldTypes.db_Float;
                            break;
                        default:
                            throw new Exception(NSC_DI.UTIL.Message.Format($"Could not handle BoFieldTypes: {row["TypeID"].ToString().Trim().ToUpper()}"));
                    }
                    BoFldSubTypes fldSubTypes;
                    switch (row["EditType"].ToString().Trim().ToUpper()) // gets field sub type
                    {
                        case "":
                        case null:
                            fldSubTypes = BoFldSubTypes.st_None;
                            break;
                        case "%":
                            fldSubTypes = BoFldSubTypes.st_Percentage;
                            break;
                        case "#":
                            fldSubTypes = BoFldSubTypes.st_Phone;
                            break;
                        case "?":
                            fldSubTypes = BoFldSubTypes.st_Address;
                            break;
                        case "B":
                            fldSubTypes = BoFldSubTypes.st_Link;
                            break;
                        case "I":
                            fldSubTypes = BoFldSubTypes.st_Image;
                            break;
                        case "M":
                            fldSubTypes = BoFldSubTypes.st_Measurement;
                            break;
                        case "P":
                            fldSubTypes = BoFldSubTypes.st_Price;
                            break;
                        case "Q":
                            fldSubTypes = BoFldSubTypes.st_Quantity;
                            break;
                        case "R":
                            fldSubTypes = BoFldSubTypes.st_Rate;
                            break;
                        case "S":
                            fldSubTypes = BoFldSubTypes.st_Sum;
                            break;
                        case "T":
                            fldSubTypes = BoFldSubTypes.st_Time;
                            break;
                        default:
                            throw new Exception(NSC_DI.UTIL.Message.Format($"Could not handle BoFldSubTypes: {row["EditType"].ToString().Trim().ToUpper()}"));
                    }
                    string FieldIDSQl = $@"SELECT UFD1.FldValue, UFD1.Descr
                                        FROM UFD1 
                                        WHERE UFD1.TableID = '@NSC_STRAIN' AND UFD1.FieldID = {row["FieldID"].ToString()}";

                    oRS = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                    oRS.DoQuery(FieldIDSQl); // gets record set of valid vals, creates a dictionary and either assigns valid vals or sets it to null
                    var tempDictionary = new Dictionary<string, string>() { };
                    if (oRS.RecordCount > 0)
                    {
                        for (int i = 0; i < oRS.RecordCount; i++)
                        {
                            if (i > 0)
                                oRS.MoveNext();                        
                            tempDictionary.Add(oRS.Fields.Item("FldValue").Value, oRS.Fields.Item("Descr").Value);
                        }
                    }
                    else
                        tempDictionary = null;
                    NSC_DI.UTIL.Misc.KillObject(oRS); // needs to be killed for because the meta data is causing issues
                    try
                    {
                        NSC_DI.UTIL.UDO.AddFieldToTable(pToTableName, row["AliasID"].ToString(), row["Descr"].ToString(), fieldsType, fldSubTypes, int.Parse(row["SizeID"].ToString()), tempDictionary, DefaultValue: row["Dflt"].ToString());
                    }
                    catch (Exception ex)
                    {
                        // gives a more specific error and more detail. done because the cloud and local version were having different results
                        throw new Exception(NSC_DI.UTIL.Message.Format($@"
Here are the vals going into AddFieldToTable: 
To Table Name: {pToTableName} 
Alias: {row["AliasID"].ToString()} 
Descr: {row["Descr"].ToString()} 
Type: {fieldsType}
Subtype: {fldSubTypes}
Size ID: {row["SizeID"].ToString()}
Default: {row["Dflt"].ToString()}
{ex.Message}"));
                    }
                   
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oRS);
                GC.Collect();
            }
        }

        private static void ReplaceJointWithPreRoll()
        {
            SAPbobsCOM.ItemProperties oITG = null;
            ItemGroups oItmGrp = null;
            try
            {
                int DBVersion = NavSol.Globals.DBVersion;
                
                if (DBVersion > 90 && DBVersion < 94)
                {
                    // Fixes Joint to Pre Roll in the Item Group for the Item Master Data form
                    oItmGrp = Globals.oCompany.GetBusinessObject(BoObjectTypes.oItemGroups);
                    var dt = NSC_DI.UTIL.SQL.DataTable("SELECT ItmsGrpCod FROM OITB WHERE ItmsGrpNam LIKE '%Joint%'");
                    foreach (System.Data.DataRow row in dt.Rows)
                    {
                        oItmGrp.GetByKey(int.Parse(row["ItmsGrpCod"].ToString()));
                        oItmGrp.GroupName = "Pre-Roll";
                        oItmGrp.Update();
                    }

                    // Fixes Joint Pre Roll in the Item Master Data Properties tab    
                    oITG = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItemProperties);
                    oITG.GetByKey(52);
                    if (oITG.PropertyName != "Pre-Roll")
                    {
                        oITG.PropertyName = "Pre-Roll";
                        oITG.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorDescription()));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItmGrp);
                NSC_DI.UTIL.Misc.KillObject(oITG);
                GC.Collect();
            }           
        }
    }
}