﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using NSC_DI.SAP;
using NSC_DI.UTIL;
using SAPbobsCOM;
using SAPbouiCOM;
using Fields = SAPbobsCOM.Fields;
using Message = NSC_DI.UTIL.Message;

namespace NavSol.CommonUI
{
	public static class LabControls
	{
		public static void LoadCBO(Form pForm, string pField, string pTable)
		{
			Recordset oRecordSet = null;
			SAPbouiCOM.ComboBox cbo = null;

            try
            {
                // Prepare to run a SQL statement.
                oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                // get the effects.
                oRecordSet.DoQuery("SELECT Code, U_Name FROM [@" + pTable + "]");

                cbo = pForm.Items.Item(pField).Specific;

                // If items already exist in the drop down
                if (cbo.ValidValues.Count > 0)
                {
                    // Remove all currently existing values from warehouse drop down
                    for (var i = cbo.ValidValues.Count; i-- > 0;)
                    {
                        cbo.ValidValues.Remove(i, BoSearchKey.psk_Index);
                    }
                }

                // If more than 1 warehouses exists
                if (oRecordSet.RecordCount > 1)
                {
                    // Create the first item as an empty item
                    cbo.ValidValues.Add("", "");

                }

                // Add allowed warehouses to the drop down
                for (var i = 0; i < oRecordSet.RecordCount; i++)
                {
                    try
                    {
                        cbo.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
                    }
                    catch (Exception ex)
                    {
                    }
                    cbo.Item.Enabled = true;
                    oRecordSet.MoveNext();
                }

                // If we only have exactly one warehouse that contains our item simply select it and disable the field.
                if (cbo.ValidValues.Count >= 1)
                {
                    cbo.Select(0, BoSearchKey.psk_Index);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(oRecordSet);
                Misc.KillObject(cbo);
                GC.Collect();
            }
		}

		public static void ADD_EFFECT(Form pForm)
		{
			SAPbouiCOM.ComboBox cmbEffect = null;
			Recordset oRecordSet = null;

            try
            {

                cmbEffect = pForm.Items.Item("CMB_EFFCT").Specific;

                if (String.IsNullOrEmpty(cmbEffect.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please select an Effect to add.", BoMessageTime.bmt_Short);
                    cmbEffect.Active = true;
                    return;
                }

                // Get the "description" from the ComboBox, where we are really storing the value
                var effectCode = cmbEffect.Selected.Description;

                oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                string sqlQuery = string.Format("SELECT * FROM [@" + NSC_DI.Globals.tCompEffect + "] WHERE U_EffectCode = '{0}' AND U_CompoundCode = '{1}'",
                    effectCode, pForm.Items.Item("TXT_ID").Specific.Value);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);

                if (oRecordSet.RecordCount > 0)
                {
                    oRecordSet = null;
                    Globals.oApp.StatusBar.SetText("Effect is already associated!", BoMessageTime.bmt_Short);
                    throw new WarningException();
                }

                oRecordSet = null;

                if (String.IsNullOrEmpty(pForm.Items.Item("TXT_ID").Specific.Value))
                {
                    Globals.oApp.StatusBar.SetText("Compound must exist before adding effect.", BoMessageTime.bmt_Short);

                    cmbEffect.Active = true;
                    throw new WarningException();
                }

                // Prepare to run a SQL statement.
                oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tCompEffect + "] ");

                // Set the textbox for the ID to the pre-determined number.
                string CompoundEffectCode = oRecordSet.Fields.Item(0).Value.ToString();

                oRecordSet = null;

                // Automatically generate items from the newly created strain.
                Compound.CreateCompoundEffectRelation(CompoundEffectCode, pForm.Items.Item("TXT_ID").Specific.Value, effectCode);

                Globals.oApp.StatusBar.SetText("Success!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

            }
            catch (WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.StatusBar.SetText("Failed to add effect to compound!", BoMessageTime.bmt_Short);
                Globals.oApp.MessageBox(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(cmbEffect);
                Misc.KillObject(oRecordSet);
                GC.Collect();
            }
		}

		public static void REMOVE_EFFECT(Form pForm)
		{
			SAPbouiCOM.Matrix MTX_EFFCT = null;
            try
            {
                // Grab the matrix from the form UI
                MTX_EFFCT = pForm.Items.Item("MTX_EFFCT").Specific;

                var effectId = String.Empty;

                // For each row already selected
                for (var i = 1; i < (MTX_EFFCT.RowCount + 1); i++)
                {
                    if (MTX_EFFCT.IsRowSelected(i))
                    {
                        // Grab the selected row's Plant ID column
                        effectId = ((EditText)MTX_EFFCT.Columns.Item(0).Cells.Item(i).Specific).Value;
                    }
                }

                if (String.IsNullOrEmpty(effectId))
                {
                    Globals.oApp.StatusBar.SetText("Failed to remove Effect!", BoMessageTime.bmt_Short);
                    throw new WarningException();
                }

                // Automatically generate items from the newly created strain.
                Compound.RemoveCompoundEffectRelation(effectId, pForm.Items.Item("TXT_ID").Specific.Value);
                Globals.oApp.StatusBar.SetText("Removed Effect from Compound!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

                Load_Compound_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
                Load_Effect_Matrix(pForm, pForm.Items.Item("TXT_ID").Specific);
            }
            catch (WarningException) { }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(MTX_EFFCT);
                GC.Collect();
            }
		}

		public static void ADD_COMPOUND_EFFECT(Form pForm)
		{
			SAPbouiCOM.ComboBox cmbCompound = null;
			Recordset oRecordSet = null;

            try
            {

                cmbCompound = pForm.Items.Item("CMB_COMP").Specific;

                if (String.IsNullOrEmpty(cmbCompound.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please select a Compound to add.", BoMessageTime.bmt_Short);
                    cmbCompound.Active = true;
                    throw new WarningException();
                }

                // Get the "description" from the ComboBox, where we are really storing the value
                var CompoundtCode = cmbCompound.Selected.Description;

                oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                string sqlQuery = string.Format($"SELECT * FROM [@{NSC_DI.Globals.tCompEffect}] WHERE U_EffectCode = '{Forms.GetField<string>(pForm, "TXT_ID")}' AND U_CompoundCode = '{CompoundtCode}'");

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(sqlQuery);

                if (oRecordSet.RecordCount > 0)
                {
                    oRecordSet = null;
                    Globals.oApp.StatusBar.SetText("Compound is already associated!", BoMessageTime.bmt_Short);
                    throw new WarningException();
                }

                oRecordSet = null;

                if (String.IsNullOrEmpty(pForm.Items.Item("TXT_ID").Specific.Value))
                {
                    Globals.oApp.StatusBar.SetText("Effect must exist before adding Compound.", BoMessageTime.bmt_Short);

                    cmbCompound.Active = true;
                    throw new WarningException();
                }

                // Prepare to run a SQL statement.
                oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tCompEffect + "] ");

                // Set the textbox for the ID to the pre-determined number.
                string CompoundEffectCode = oRecordSet.Fields.Item(0).Value.ToString();

                oRecordSet = null;

                // Automatically generate items from the newly created strain.
                Compound.CreateCompoundEffectRelation(CompoundEffectCode, CompoundtCode, pForm.Items.Item("TXT_ID").Specific.Value);

                Globals.oApp.StatusBar.SetText("Success!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

            }
            catch (WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.StatusBar.SetText("Failed to add effect to compound!", BoMessageTime.bmt_Short);
                Globals.oApp.MessageBox(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(cmbCompound);
                Misc.KillObject(oRecordSet);
                GC.Collect();
            }
		}

		public static void REMOVE_COMPOUND(Form pForm)
		{
			SAPbouiCOM.Matrix MTX_EFFCT = null;
			try
			{
				// Grab the matrix from the form UI
				MTX_EFFCT = pForm.Items.Item("MTX_COMP").Specific;

				var compoundId = String.Empty;

				// For each row already selected
				for (var i = 1; i < (MTX_EFFCT.RowCount + 1); i++)
				{
					if (MTX_EFFCT.IsRowSelected(i))
					{
						// Grab the selected row's Plant ID column
						compoundId = ((EditText)MTX_EFFCT.Columns.Item(0).Cells.Item(i).Specific).Value;
					}
				}

				if (String.IsNullOrEmpty(compoundId))
				{
					Globals.oApp.StatusBar.SetText("Failed to remove Compound!", BoMessageTime.bmt_Short);
					throw new WarningException();
				}

				// Automatically generate items from the newly created strain.
				Compound.RemoveCompoundEffectRelation(pForm.Items.Item("TXT_ID").Specific.Value, compoundId);
				Globals.oApp.StatusBar.SetText("Removed Compound!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
			}
			catch (Exception ex)
			{
				throw new Exception(Message.Format(ex));
			}
			finally
			{
				Misc.KillObject(MTX_EFFCT);
				GC.Collect();
			}
		}

        public static void ADD_COMPOUND_TEST(Form pForm)
        {
            SAPbouiCOM.ComboBox cmbCompound = null;
            EditText testFieldCode = null;
            Recordset oRecordSet = null;

            try
            {
                // Compound
                cmbCompound = pForm.Items.Item("CMB_COMP").Specific;

                if (String.IsNullOrEmpty(cmbCompound.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please select a Compound to add.", BoMessageTime.bmt_Short);
                    cmbCompound.Active = true;
                    throw new WarningException();
                }
                var CompoundtCode = cmbCompound.Selected.Description;

                // Test ID
                testFieldCode = pForm.Items.Item("TXT_ID").Specific;
                if (String.IsNullOrEmpty(testFieldCode.Value))
                {
                    Globals.oApp.StatusBar.SetText("Test must by saved before adding Compound relation.", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                    cmbCompound.Active = true;
                    throw new WarningException();
                }

                // Check if associated
                oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                string sqlQuery = string.Format("SELECT [U_TestCode] FROM [@" + NSC_DI.Globals.tTestComp + "] WHERE [U_CompoundCode] = '{0}' AND [U_TestCode] = '{1}'",
                                        testFieldCode, pForm.Items.Item("TXT_NAME").Specific.Value);
                oRecordSet.DoQuery(sqlQuery);

                if (oRecordSet.RecordCount > 0)
                {
                    Globals.oApp.StatusBar.SetText("Compound is already associated!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                    throw new WarningException();
                }

                // Next Test And Test Field Code
                oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tTestComp + "] ");
                string NewTestAndTestFieldCode = oRecordSet.Fields.Item(0).Value.ToString();

                // Add Relation
                Test.CreateCompoundRelation(NewTestAndTestFieldCode, pForm.Items.Item("TXT_ID").Specific.Value, testFieldCode.Value);

                Globals.oApp.StatusBar.SetText("Success!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);

            }
            catch (Exception ex)
            {
                Globals.oApp.StatusBar.SetText("Failed to add effect to compound!", BoMessageTime.bmt_Short);
                Globals.oApp.MessageBox(Message.Format(ex));
            }
            finally
            {
                Misc.KillObject(testFieldCode);
                Misc.KillObject(cmbCompound);
                Misc.KillObject(oRecordSet);
                GC.Collect();
            }
        }

		public static void ADD_DISEASE(Form pForm)
		{
			SAPbouiCOM.ComboBox cmbDisease = null;
			Recordset oRecordSet = null;

			try
			{

				cmbDisease = pForm.Items.Item("CMB_DISEA").Specific;

				if (String.IsNullOrEmpty(cmbDisease.Value))
				{
					Globals.oApp.StatusBar.SetText("Please select a Disease to add.", BoMessageTime.bmt_Short);
					cmbDisease.Active = true;
					throw new WarningException();
				}

				// Get the "description" from the ComboBox, where we are really storing the value
				var diseaseCode = cmbDisease.Selected.Description;

				oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

				string sqlQuery = string.Format("SELECT * FROM [@" + NSC_DI.Globals.tCompEffect + "] WHERE U_EffectCode] = {0} AND U_DiseaseCode] = {1}",
					pForm.Items.Item("TXT_ID").Specific.Value, diseaseCode);

				// Count how many current records exist within the database.
				oRecordSet.DoQuery(sqlQuery);

				if (oRecordSet.RecordCount > 0)
				{
					oRecordSet = null;
					Globals.oApp.StatusBar.SetText("Disease is already associated!", BoMessageTime.bmt_Short);
					throw new WarningException();
				}

				oRecordSet = null;

				if (String.IsNullOrEmpty(pForm.Items.Item("TXT_ID").Specific.Value))
				{
					Globals.oApp.StatusBar.SetText("Effect must be saved before adding Disease!", BoMessageTime.bmt_Short);

					cmbDisease.Active = true;
					throw new WarningException();
				}

				// Prepare to run a SQL statement.
				oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

				// Count how many current records exist within the database.
				oRecordSet.DoQuery("SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM [@" + NSC_DI.Globals.tDiseEffect + "] ");

				// Set the textbox for the ID to the pre-determined number.
				string DiseaseAndEffectCode = oRecordSet.Fields.Item(0).Value.ToString();

				oRecordSet = null;

				// Automatically generate items from the newly created strain.
				Compound.CreateCompoundEffectRelation(DiseaseAndEffectCode, diseaseCode, pForm.Items.Item("TXT_ID").Specific.Value);

				Globals.oApp.StatusBar.SetText("Success!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
			}
			catch (Exception ex)
			{
				Globals.oApp.StatusBar.SetText("Failed to add effect to compound!", BoMessageTime.bmt_Short);
				Globals.oApp.MessageBox(Message.Format(ex));
			}
			finally
			{
				Misc.KillObject(cmbDisease);
				Misc.KillObject(oRecordSet);
				GC.Collect();
			}
		}

		public static void REMOVE_DISEASE(Form pForm)
		{
			SAPbouiCOM.Matrix MTX_DISEA = null;
			try
			{
				// Grab the matrix from the form UI
				MTX_DISEA = pForm.Items.Item("MTX_DISEA").Specific;

				var diseaseId = String.Empty;

				// For each row already selected
				for (var i = 1; i < (MTX_DISEA.RowCount + 1); i++)
				{
					if (MTX_DISEA.IsRowSelected(i))
					{
						// Grab the selected row's Plant ID column
						diseaseId = ((EditText)MTX_DISEA.Columns.Item(0).Cells.Item(i).Specific).Value;
					}
				}

				if (String.IsNullOrEmpty(diseaseId))
				{
					Globals.oApp.StatusBar.SetText("Failed to remove Compound!", BoMessageTime.bmt_Short);
					throw new WarningException();
				}

				// Automatically generate items from the newly created strain.
				Compound.RemoveCompoundEffectRelation(diseaseId, pForm.Items.Item("TXT_ID").Specific.Value);
				Globals.oApp.StatusBar.SetText("Removed Disease!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
			}
			catch (Exception ex)
			{
				throw new Exception(Message.Format(ex));
			}
			finally
			{
				Misc.KillObject(MTX_DISEA);
				GC.Collect();
			}
		}

        public static void Load_TestFields_Matrix(Form pForm, EditText txtID)
        {
            try
            {
                Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tTestTestField, "MTX_TFIELD", new List<Matrix.MatrixColumn>()
			    {
				    new Matrix.MatrixColumn() {Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
				    new Matrix.MatrixColumn() {Caption = "Name", ColumnName = "U_Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT},
                    new Matrix.MatrixColumn() { Caption = "Group", ColumnName = "U_Group", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
			    },
                String.Format(@"
SELECT DISTINCT [@" + NSC_DI.Globals.tTestField + @"].CODE, [@" + NSC_DI.Globals.tTestField + @"].[U_Name], [@" + NSC_DI.Globals.tTestTestField + @"].[U_Group]
FROM [@" + NSC_DI.Globals.tTestTestField + @"]
INNER JOIN [@" + NSC_DI.Globals.tTestField + @"] 
ON [@" + NSC_DI.Globals.tTestField + @"].Code = [@" + NSC_DI.Globals.tTestTestField + @"].[U_TestFieldCode]
WHERE [@" + NSC_DI.Globals.tTestTestField + @"].[U_TestCode] = '{0}'",
                    txtID.Value.ToString()));
            }
            catch (Exception ex)
            {
                throw new Exception(Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

		public static void Load_Effect_Matrix(Form pForm, EditText txtID)
		{
			try
			{
                if (Forms.HasField(pForm, "MTX_EFFCT") == false) return;

				Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tCompounds, "MTX_EFFCT", new List<Matrix.MatrixColumn>()
			    {
				    new Matrix.MatrixColumn() {Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
				    new Matrix.MatrixColumn() {Caption = "Name", ColumnName = "U_Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}
			    },
				String.Format(@"
SELECT DISTINCT [@" + NSC_DI.Globals.tEffect + @"].CODE, [@" + NSC_DI.Globals.tEffect + @"].[U_Name]
FROM [@" + NSC_DI.Globals.tCompEffect + @"]
INNER JOIN [@" + NSC_DI.Globals.tEffect + @"] 
ON [@" + NSC_DI.Globals.tEffect + @"].Code = [@" + NSC_DI.Globals.tCompEffect + @"].[U_EffectCode]
WHERE [@" + NSC_DI.Globals.tCompEffect + @"].U_CompoundCode = '{0}'",
					txtID.Value.ToString()));
			}
			catch (Exception ex)
			{
				throw new Exception(Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		public static void Load_Disease_Matrix(Form pForm, EditText txtID)
		{
            if (Forms.HasField(pForm, "MTX_DISEA") == false) return;

            var sqlQueryToRun = String.Format(@"
SELECT DISTINCT [@" + NSC_DI.Globals.tDisease + @"].Code, [@" + NSC_DI.Globals.tDisease + @"].[U_Name]
  FROM [@" + NSC_DI.Globals.tCompEffect + @"]

-- Join the Effect Table to the CompoundAndEffect Table
INNER JOIN [@" + NSC_DI.Globals.tEffect + @"] 
ON [@" + NSC_DI.Globals.tEffect + @"].Code = [@" + NSC_DI.Globals.tCompEffect + @"].[U_EffectCode]

-- Join the DiseaseAndEffect table
INNER JOIN [@" + NSC_DI.Globals.tDiseEffect + @"] 
ON [@" + NSC_DI.Globals.tDiseEffect + @"].[U_EffectCode] = [@" + NSC_DI.Globals.tEffect + @"].Code

-- Join The Disease table
INNER JOIN [@" + NSC_DI.Globals.tDisease + @"] 
ON [@" + NSC_DI.Globals.tDisease + @"].Code = [@" + NSC_DI.Globals.tDiseEffect + @"].[U_DiseaseCode]
WHERE [@" + NSC_DI.Globals.tCompEffect + @"].[U_CompoundCode] = '{0}'", txtID.Value.ToString());

			// Load the matrix.
			Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tCompounds, "MTX_DISEA", new List<Matrix.MatrixColumn>() { 
                        new Matrix.MatrixColumn() { Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON } 
                        , new Matrix.MatrixColumn() { Caption = "Name", ColumnName = "U_Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT }
                    }, sqlQueryToRun);

		}

		public static void Load_Compound_Matrix(Form pForm, EditText txtID)
		{
            if (Forms.HasField(pForm, "MTX_COMP") == false) return;

            try
			{
				var sqlQueryToRun = String.Format(@"
SELECT DISTINCT [@" + NSC_DI.Globals.tCompounds + @"].Code, [@" + NSC_DI.Globals.tCompounds + @"].[U_Name]
FROM [@" + NSC_DI.Globals.tCompounds + @"]

-- Join the Effect Table to the CompoundAndEffect Table
INNER JOIN [@" + NSC_DI.Globals.tCompEffect + @"] 
ON [@" + NSC_DI.Globals.tCompEffect + @"].[U_CompoundCode] = [@" + NSC_DI.Globals.tCompounds + @"].Code

-- Join the Effect Table to the CompoundAndEffect Table
INNER JOIN [@" + NSC_DI.Globals.tEffect + @"] 
ON [@" + NSC_DI.Globals.tEffect + @"].Code = [@" + NSC_DI.Globals.tCompEffect + @"].[U_EffectCode]

-- Join the DiseaseAndEffect table
INNER JOIN [@" + NSC_DI.Globals.tDiseEffect + @"] 
ON [@" + NSC_DI.Globals.tDiseEffect + @"].[U_EffectCode] = [@" + NSC_DI.Globals.tEffect + @"].Code

WHERE [@" + NSC_DI.Globals.tDiseEffect + @"].[U_DiseaseCode] = '{0}'", txtID.Value.ToString());

				// Load the matrix.
				Matrix.LoadDatabaseDataIntoMatrix(pForm, "@" + NSC_DI.Globals.tCompounds, "MTX_COMP", new List<Matrix.MatrixColumn>()
				    {new Matrix.MatrixColumn() {Caption = "ID", ColumnName = "Code", ColumnWidth = 80, ItemType = BoFormItemTypes.it_LINKED_BUTTON},
					 new Matrix.MatrixColumn() {Caption = "Name", ColumnName = "U_Name", ColumnWidth = 80, ItemType = BoFormItemTypes.it_EDIT}},
					 sqlQueryToRun);
			}
			catch (Exception ex)
			{
				throw new Exception(Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		public static bool TestValidate(Form pForm)
		{
			EditText TextBox_Name = null;

			try
			{
				// Check for existing Compound
				TextBox_Name = pForm.Items.Item("TXT_NAME").Specific;

				string SQL_Query_FindCompoundByName = "SELECT [Code] FROM [@" + NSC_DI.Globals.tTests + "] WHERE [U_Name] = '" + TextBox_Name.Value + "'";

				SAPbobsCOM.Fields ResultsFromSQL = NSC_DI.UTIL.SQL.GetFieldsFromSQLQuery(SQL_Query_FindCompoundByName);

				if (ResultsFromSQL == null || ResultsFromSQL.Count <= 0) return true;

				Globals.oApp.StatusBar.SetText("There is already a Test named '" + TextBox_Name.Value + "'");
				return false;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
				NSC_DI.UTIL.Misc.KillObject(TextBox_Name);
				GC.Collect();
			}
		}
	}
}
