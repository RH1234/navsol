﻿using System;
using SAPbouiCOM;

namespace NavSol.CommonUI
{
	public class ProgressBar
	{
		private SAPbouiCOM.ProgressBar _progressBar = null;
		private int _maxProgressCount;
		private int _currentProgress = 0;
		private string _initalMessage = "Loading..";

		/// <summary>
		/// Constructor for the Progress Bar.
		/// </summary>
		/// <param name="SBO_Application"></param>
		public ProgressBar(Application SBO_Application)
		{
			Globals.oApp = SBO_Application;
		}

		/// <summary>
		/// Create a new Progress Bar.
		/// </summary>
		/// <param name="initialMessage"></param>
		/// <param name="maxCount"></param>
		/// <param name="isStoppable"></param>
		public void Create(string initialMessage, int maxCount, bool isStoppable = false)
		{
			// Validate that we aren't currently using this reference already.. If so dump it.
			if (_progressBar != null)
				Stop();

			_currentProgress = 0;
			_maxProgressCount = maxCount;
			_progressBar = Globals.oApp.StatusBar.CreateProgressBar(initialMessage, maxCount, isStoppable);
		}

		/// <summary>
		/// Set the message of the current stage of the Progress.
		/// </summary>
		/// <param name="message">Message to display in the progress bar area.</param>
		public void SetText(string message)
		{
			if (_progressBar == null)
			{
				_progressBar = Globals.oApp.StatusBar.CreateProgressBar(_initalMessage, _maxProgressCount, false);
			}

			try
			{
				_progressBar.Text = message;
			}
			catch (Exception e)
			{
				// Try to kill the progress bar..
				NSC_DI.UTIL.Misc.KillObject(_progressBar);
				_progressBar = null;
			}
		}

		/// <summary>
		/// Increment the Progess slider by a specified amount.
		/// The ratio is determined by the currentProgressCount/maxProgressCount
		/// </summary>
		/// <param name="incrementProgressBy"></param>
		public void Increment(int incrementProgressBy = 1)
		{
			if (_progressBar == null)
			{
				_progressBar = Globals.oApp.StatusBar.CreateProgressBar(_initalMessage, _maxProgressCount, false);
			}

			try
			{
				int nextCount = _currentProgress + incrementProgressBy;

				// Do not allow greater than the set max or less then 0.
				if (nextCount > _maxProgressCount && nextCount < 0)
					return;
				_currentProgress += incrementProgressBy;
				_progressBar.Value = _currentProgress;
			}
			catch (Exception e)
			{
				// Try to kill the progress bar..
				NSC_DI.UTIL.Misc.KillObject(_progressBar);
				_progressBar = null;
			}
		}

		/// <summary>
		/// Stop the progress bar. This will kill all the references.
		/// </summary>
		public void Stop()
		{
			if (_progressBar == null)
			{
				try
				{
					_progressBar = Globals.oApp.StatusBar.CreateProgressBar(_initalMessage, _maxProgressCount, false);
				}
				catch (Exception e)
				{
				}
			}

			try
			{
				_progressBar.Stop();
				NSC_DI.UTIL.Misc.KillObject(_progressBar);
				GC.Collect();
				_progressBar = null;
			}
			catch (Exception e)
			{
				// Try to kill the progress bar..
				NSC_DI.UTIL.Misc.KillObject(_progressBar);
				GC.Collect();
				_progressBar = null;
			}
		}
	}
}
