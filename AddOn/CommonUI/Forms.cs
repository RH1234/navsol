﻿using System;
using System.Xml;
using B1WizardBase;
using SAPbouiCOM;


namespace NavSol.CommonUI
{
	public static partial class Forms
    {
		//private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Loads a form into the SAP Business One UI given an XML file formatted for SAP Business One.
        /// </summary>
        /// <param name="XMLFileLocation">The location of the XML file to load.</param>
		public static void ShowFormFromXMLFile(string XMLFileLocation)
        {
            XmlDocument oXmlDoc = new XmlDocument();

            // Attempt to load file as XML
            try
            {
                oXmlDoc.Load(XMLFileLocation);
            }
            catch (Exception ex)
            {
				//log.Warn("Error loading XML file '" + XMLFileLocation + "'", er);
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

            // Try loading the form into SAP Business One
            try
            {
                Globals.oApp.LoadBatchActions(oXmlDoc.InnerXml);
            }
            catch (Exception ex)
            {
				//log.Warn("Failed loading _SBO_Form from xml", er);
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}

            // Operation was successful
        }

		public static Form Load(string pFormID, bool pSingleInstance = false)
        {
            var oFormXml = GetFormXml(pFormID);
            return LoadXML(oFormXml, pSingleInstance);
        }

        private static string GetFormXml(string pFormID)
        {
            try
            {
                var oXml = mFormFiles[pFormID];
                oXml = oXml.Replace("(NSC_FORM)", pFormID);
                return oXml;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
        }

        public static Form LoadXML(string pXML, bool pSingleInstance = false)
		{
			// open the form from the XML string
			// if pSingleInstance is true then just return the current form if opene

			Form oForm = null;

			try
			{
				if (pSingleInstance)
				{
					var formType = GetFormType(pXML);
					try
					{
						oForm = Globals.oApp.Forms.GetForm(formType, 1);
						oForm.Select();
						oForm.VisibleEx = true;
						return oForm;
					}
					catch
					{
					}
				}

				if (pXML.Length < 1) return null;

				FormCreationParams oCreationParams = (FormCreationParams)B1Connections.theAppl.CreateObject(BoCreatableObjectType.cot_FormCreationParams);
				oCreationParams.XmlData = pXML;
				oForm = B1Connections.theAppl.Forms.AddEx(oCreationParams);

				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				//NSC_DI.UTIL.Misc.KillObject(oForm);
				GC.Collect();
			}
		}

		public static Form LoadXML_Modal(string pXML)
		{
			// open the form from the XML string as a modal form

			// can use modality='1' and use the regular LoadXML

			Form oForm = null;

			try
			{

				if (pXML.Length < 1) return null;

				FormCreationParams oCreationParams = (FormCreationParams)B1Connections.theAppl.CreateObject(BoCreatableObjectType.cot_FormCreationParams);
				oCreationParams.XmlData = pXML;
				oCreationParams.Modality = BoFormModality.fm_Modal;
				oForm = B1Connections.theAppl.Forms.AddEx(oCreationParams);

				return oForm;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				
			}
		}
		
		public static bool IsFormOpen(string FormType)
		{
			// see if the form type is already open

			try
			{
				Form LastForm = Globals.oApp.Forms.GetForm(FormType, 1);
				return true;
			}
			catch 
			{
				return false;
			}
		}

        public static Form GetForm(string FormType, int FormCount)
        {
            // To get the number of forms open of the same type, use Form.TypeCount
            
            try
            {
                Form LastForm = Globals.oApp.Forms.GetForm(FormType, FormCount);
                return (LastForm);

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static Form GetActiveForm()
        {
            try
            {
                Form CurrentFormLoaded = Globals.oApp.Forms.ActiveForm;
                return (CurrentFormLoaded);
            }
            catch (Exception)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorCode(), Globals.oCompany.GetLastErrorDescription()));
                
            }
        }

        public static string GetFormType(string pXML)
        {
            if (string.IsNullOrEmpty(pXML?.Trim())) throw new ArgumentException();//(NSC_DI.UTIL.Strings.Empty(pXML)) Whitespace-Change

            try
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml(pXML);
                XmlNode node = xDoc.SelectSingleNode("/Application/forms/action/form/@FormType");
                return node.Value;
            }
            catch (Exception)
            {
                throw new Exception("Could not find FormType attribute");
            }
        }

        public static int FormCount(SAPbouiCOM.Application oApp, string pFormType)
        {
            // return the number of time the form type is open
            try
            {
                if (pFormType.Trim().Length < 1) return 0;

                int _count = 0;
                try
                {
                    while (true)
                    {
                        SAPbouiCOM.Form _form = oApp.Forms.GetForm(pFormType, _count++);
                        //_count = _form.TypeCount;
                    }
                }
                catch { }

                return _count;
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public enum FormControlTypes { PictureBox, Folder, EditText, Matrix, StaticText, Button, ComboBox, ChooseFromList, CheckBox, ButtonCombo }

        /// <summary>
        /// Returns an SAP Form object given the Forms UID
        /// </summary>
        /// <param name="SAPBusinessOne_Application"></param>
        /// <param name="FormUID"></param>
        /// <returns></returns>
		public static Form GetFormFromUID(string FormUID)
        {
            try
            {
				var v = Globals.oApp.Forms.Item(FormUID);
                if (v != null)
                {
                    return v;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Returns an SAP UI object based off of a provided type, and item unique identifier from the active form.
        /// </summary>
        /// <param name="ItemType"></param>
        /// <param name="ItemUID"></param>
        /// <returns></returns>
		public static dynamic GetControlFromForm(FormControlTypes ItemType, string ItemUID, Form FormToSearchIn)
        {
			// TODO: replace all calls to this sub
            try
            {
                object returnObject = null;

                switch (ItemType)
                {
                    case FormControlTypes.PictureBox:
                        returnObject = (PictureBox)FormToSearchIn.Items.Item(ItemUID).Specific;
                        break;

                    case FormControlTypes.Folder:
                        returnObject = (Folder)FormToSearchIn.Items.Item(ItemUID).Specific;
                        break;

                    case FormControlTypes.EditText:
                        returnObject = (EditText)FormToSearchIn.Items.Item(ItemUID).Specific;
                        break;

                    case FormControlTypes.Matrix:
                        returnObject = (SAPbouiCOM.Matrix)FormToSearchIn.Items.Item(ItemUID).Specific;
                        break;

                    case FormControlTypes.StaticText:
                        returnObject = (StaticText)FormToSearchIn.Items.Item(ItemUID).Specific;
                        break;

                    case FormControlTypes.Button:
                        returnObject = (Button)FormToSearchIn.Items.Item(ItemUID).Specific;
                        break;

                    case FormControlTypes.ComboBox:
                        returnObject = (SAPbouiCOM.ComboBox)FormToSearchIn.Items.Item(ItemUID).Specific;
                        break;

                    case FormControlTypes.ChooseFromList:
                        returnObject = (ChooseFromList)FormToSearchIn.Items.Item(ItemUID).Specific;
                        break;

                    case FormControlTypes.CheckBox:
                        returnObject = (SAPbouiCOM.CheckBox)FormToSearchIn.Items.Item(ItemUID).Specific;
                        break;

                    case FormControlTypes.ButtonCombo:
                        returnObject = (ButtonCombo)FormToSearchIn.Items.Item(ItemUID).Specific;
                        break;
                }

                return returnObject;
            }
            catch (Exception ex)
            {
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static UserDataSource FindUserDataSource(Form oForm, string UniqueID)
        {
             try
            {
                if (oForm == null) throw new ArgumentNullException("oForm");
                return oForm.DataSources.UserDataSources.Item(UniqueID);
            }
            catch (Exception ex)
            {
                //throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                return null;
            }
        }

		public static void CreateUserDataSource(Form pForm, string pField, string UniqueID, SAPbouiCOM.BoDataType oDataType, int Length)
		{
			// create and assign a user data source
			try
			{
				CreateUserDataSource(pForm, UniqueID, oDataType, Length);
				pForm.Items.Item(pField).Specific.DataBind.SetBound(true, "", UniqueID);
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorCode(), Globals.oCompany.GetLastErrorDescription()));
			}
		}

		public static SAPbouiCOM.UserDataSource CreateUserDataSource(Form oForm, string UniqueID, SAPbouiCOM.BoDataType oDataType, int Length)
        {
            SAPbouiCOM.UserDataSource oUSDatasource = null;

            try
            {
                if (oForm == null) throw new ArgumentNullException("oForm");

                oUSDatasource = FindUserDataSource(oForm, UniqueID);

                if (oUSDatasource == null)
                {
                    oUSDatasource = oForm.DataSources.UserDataSources.Add(UniqueID, oDataType, Length);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(Globals.oCompany.GetLastErrorCode(), Globals.oCompany.GetLastErrorDescription()));
            }
            return (oUSDatasource);
        }

        public static SAPbouiCOM.DBDataSource FindDBDataSource2(Form oForm, string pDBTable)
        {
            SAPbouiCOM.DBDataSource oDBDatasource = null;

            try
            {
                if (oForm == null) throw new ArgumentNullException("oForm");

                for (int i = 0; i < oForm.DataSources.DBDataSources.Count; i++)
                {
                    oDBDatasource = oForm.DataSources.DBDataSources.Item(i);

                }

                return oDBDatasource;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //if (oDBDatasource != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oDBDatasource);
                //oDBDatasource = null;
                GC.Collect();
            }
        }

        public static SAPbouiCOM.DBDataSource FindDBDataSource(SAPbouiCOM.Form oForm, string pDBTable)
        {

            SAPbouiCOM.DBDataSource oDBDatasource = null;

            try
            {
                if (oForm == null) throw new ArgumentNullException("oForm");

                try
                {
                    oDBDatasource = oForm.DataSources.DBDataSources.Item(pDBTable.Replace("[", "").Replace("]", ""));
                }
                catch
                {
                }

                return oDBDatasource;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //if (oDBDatasource != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oDBDatasource);
                //oDBDatasource = null;
                GC.Collect();
            }
        }

        public static SAPbouiCOM.DBDataSource CreateDBDataSource(ref SAPbouiCOM.Form oForm, string pItem)
        {
            SAPbouiCOM.DBDataSource oDBDatasource = null;

            try
            {
                if (oForm == null) throw new ArgumentNullException("oForm");

                oDBDatasource = FindDBDataSource(oForm, pItem);
                if (oDBDatasource == null)
                {
                    oDBDatasource = oForm.DataSources.DBDataSources.Add(pItem);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            return (oDBDatasource);
        }

        public static void addDataSrc(string pFormID, string pSrcType, string pSrceName, string pType, int pLength)
        {
            DBDataSource oDBS;
            BoDataType oDataType = BoDataType.dt_SHORT_TEXT;

            try
            {
                Form _oForm = GetFormFromUID(pFormID);

                if (pSrcType == "DBDataSource")
                {
                    oDBS = _oForm.DataSources.DBDataSources.Add(pSrceName);
                }
                else if (pSrcType == "UserDataSource")
                {
                    if (pType == "DATE")
                    {
                        oDataType = BoDataType.dt_DATE;
                    }
                    else if (pType == "LONG_NUMBER")
                    {
                        oDataType = BoDataType.dt_LONG_NUMBER;
                    }
                    else if (pType == "LONG_TEXT")
                    {
                        oDataType = BoDataType.dt_LONG_TEXT;
                    }
                    else if (pType == "MEASURE")
                    {
                        oDataType = BoDataType.dt_MEASURE;
                    }
                    else if (pType == "PERCENT")
                    {
                        oDataType = BoDataType.dt_PERCENT;
                    }
                    else if (pType == "PRICE")
                    {
                        oDataType = BoDataType.dt_PRICE;
                    }
                    else if (pType == "QUANTITY")
                    {
                        oDataType = BoDataType.dt_QUANTITY;
                    }
                    else if (pType == "RATE")
                    {
                        oDataType = BoDataType.dt_RATE;
                    }
                    else if (pType == "SHORT_NUMBER")
                    {
                        oDataType = BoDataType.dt_SHORT_NUMBER;
                    }
                    else if (pType == "SHORT_TEXT")
                    {
                        oDataType = BoDataType.dt_SHORT_TEXT;
                    }
                    else if (pType == "SUM")
                    {
                        oDataType = BoDataType.dt_SUM;
                    }

                    _oForm.DataSources.UserDataSources.Add(pSrceName, oDataType, pLength);
                }

            }
            catch (Exception ex)
            {
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
        }

        public static bool HasField(Form pForm, string sField)
        {
            Item oItem = null;
            try
            {
                oItem = pForm.Items.Item(sField); // should fail at this point as of 9.2 if field does not exist
                return oItem.Specific != null;
            }
            catch
            {
                return false;
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItem);
                GC.Collect();
            }
        }

        public static T GetField<T>(Form oForm, string sField, bool pUseComboDesc = false)
        { 
            return GetField<T>(oForm, sField, -1, "", pUseComboDesc);
        }

		public static T GetField<T>(Form oForm, string sField, int iRow, dynamic pCol, bool pUseComboDesc = false)
		{
            // see if the field exists
            if (GUI_ItemExists(oForm, sField) == false) throw new ArgumentException("Field '" + sField + "' does not exist", "sField");

			string sRtn = "";
			//
			// combo boxes do not issue a "Validate" event only a "lost focus".
			// a "lost focus" event ONLY occures in an "after event".
			//

			// sCmbBoxValOrDsc   - V or D - if the field/ column is a combo box, this indicates to return the Value or the Description.

			PictureBox oPic = null;
			SAPbouiCOM.ComboBox oCmb = null;
            SAPbouiCOM.CheckBox oCkb = null;
			EditText oEdt = null;
			Column oCol = null;
			SAPbouiCOM.Matrix oMat = null;
			SAPbouiCOM.Grid oGrd = null;// Grid oGrd = null;  Grid was not instantiated expliciytly as a sapbouicom obj. was causing build errors so I set it this way

            //Dim oCels As SAPbouiCOM.Cells = Nothing
            //Dim oCel As SAPbouiCOM.Cell = Nothing

            try
			{
				switch (oForm.Items.Item(sField).Type)
				{
					case BoFormItemTypes.it_EDIT:
					case BoFormItemTypes.it_EXTEDIT:
					case BoFormItemTypes.it_LINKED_BUTTON:
						oEdt = (EditText)oForm.Items.Item(sField).Specific;
						sRtn = oEdt.String;
						break;

					case BoFormItemTypes.it_PICTURE:
						oPic = (PictureBox)oForm.Items.Item(sField).Specific;
						sRtn = oPic.Picture;
						break;

					case BoFormItemTypes.it_COMBO_BOX:
						oCmb = (SAPbouiCOM.ComboBox)oForm.Items.Item(sField).Specific;
                        if (string.IsNullOrEmpty(oCmb.Value) || oCmb.ValidValues.Count == 0) break;
                        if (pUseComboDesc)
                            sRtn = oCmb.Selected.Description;
                        else
                            sRtn = oCmb.Selected.Value;
						break;

					case BoFormItemTypes.it_CHECK_BOX:
						oCkb = (SAPbouiCOM.CheckBox)oForm.Items.Item(sField).Specific;
                        sRtn = CheckBox.GetValue(oCkb);
                        break;

                    case BoFormItemTypes.it_STATIC:
                        sRtn = (oForm.Items.Item(sField).Specific as SAPbouiCOM.StaticText).Caption;
                        break;

                    case BoFormItemTypes.it_MATRIX:
                        oMat = (SAPbouiCOM.Matrix)oForm.Items.Item(sField).Specific;
						oCol = oMat.Columns.Item(pCol);
						switch (oCol.Type)
						{
							case BoFormItemTypes.it_EDIT:
							case BoFormItemTypes.it_EXTEDIT:
							case BoFormItemTypes.it_LINKED_BUTTON:
								//oEdt = oMat.Columns.Item(sCol).Cells.Item(iRow).Specific
								oEdt = (EditText)oMat.GetCellSpecific(pCol, iRow);
								sRtn = oEdt.Value;

								//if (pCol == "37")
								//{
								//	//sTmp = oEdt.Value.GetTypeCode;
								//	decimal dtmp = Convert.ToDecimal(oEdt.Value);
								//}
								break;

							case BoFormItemTypes.it_PICTURE:
								//oPic = oMat.Columns.Item(sCol).Cells.Item(iRow).Specific
								oPic = (PictureBox)oMat.GetCellSpecific(pCol, iRow);
								sRtn = oPic.Picture;
								break;

							case BoFormItemTypes.it_COMBO_BOX:
								//oCmb = oMat.Columns.Item(sCol).Cells.Item(iRow).Specific
								oCmb = (SAPbouiCOM.ComboBox)oMat.GetCellSpecific(pCol, iRow);
                                if (pUseComboDesc)
                                    sRtn = oCmb.Selected.Description;
                                else
								    sRtn = oCmb.Selected.Value;
								break;

							case BoFormItemTypes.it_CHECK_BOX:
								//oCkb = oMat.Columns.Item(sCol).Cells.Item(iRow).Specific
								oCkb = (SAPbouiCOM.CheckBox)oMat.GetCellSpecific(pCol, iRow);
                                sRtn =  CheckBox.GetValue(oCkb);
								break;
						}
						break;

					case BoFormItemTypes.it_GRID:
						oGrd = oForm.Items.Item(sField).Specific; //(Grid)oForm.Items.Item(sField).Specific was initially written like this and the Grid was not instantiated expliciytly as a sapbouicom obj. was causing build errors so I set it this way
                        sRtn = Convert.ToString(oGrd.DataTable.GetValue(pCol, iRow));// Convert.ToString() can handle runtime null binding, .ToString() cannot
						break;
				}
                object o = null;
                try
                {
                    o = Convert.ChangeType(sRtn, typeof(T));
                }
                catch (Exception) { }
                if (o != null) return (T)o;
                else return default(T);
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oPic);
				NSC_DI.UTIL.Misc.KillObject(oCmb);
				NSC_DI.UTIL.Misc.KillObject(oCkb);
				NSC_DI.UTIL.Misc.KillObject(oEdt);
				NSC_DI.UTIL.Misc.KillObject(oCol);
				NSC_DI.UTIL.Misc.KillObject(oMat);
				NSC_DI.UTIL.Misc.KillObject(oGrd);
				GC.Collect();
			}
		}

        public static void SetField(Form oForm, string sField, object value)
        {
            SetField(oForm, sField, -1, null, value);
        }

        public static void SetField(Form oForm, string sField, int iRow, dynamic pCol, object value)
        {
            SetField(oForm, -1, sField, iRow, pCol, value);
        }

        public static void SetField(Form oForm, int paneNumber, object pField, int iRow, dynamic pCol, object value)
        {
            SAPbouiCOM.PictureBox pictureBoxItem = null;
            //SAPbouiCOM.ComboBoxColumn comboBoxCol = null;
            SAPbouiCOM.ComboBox comboBoxItem = null;
            SAPbouiCOM.CheckBox checkBoxItem = null;
            SAPbouiCOM.EditText editTextItem = null;
            SAPbouiCOM.Column columnItem = null;
            SAPbouiCOM.Matrix matrixItem = null;
            SAPbouiCOM.Grid gridItem = null;
            StaticText staticText = null;

            int currentPane = -1;

            if (GUI_ItemExists(oForm, pField) == false) throw new ArgumentException("Field '" + pField.ToString() + "' does not exist", "sField");


            try
            {
                currentPane = oForm.PaneLevel;

                // if the pane has a valid value and is different from the current pane, set it
                if (paneNumber >= 0 && paneNumber != currentPane)
                {
                    oForm.Freeze(true);
                    oForm.PaneLevel = paneNumber;
                }

                switch (oForm.Items.Item(pField).Type)
                {

                    case SAPbouiCOM.BoFormItemTypes.it_EDIT:
                    case SAPbouiCOM.BoFormItemTypes.it_EXTEDIT:
                    case SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON:
                        editTextItem = (SAPbouiCOM.EditText)oForm.Items.Item(pField).Specific;
                        editTextItem.Value = value.ToString();
                        break;
                    case BoFormItemTypes.it_STATIC:
                        staticText = oForm.Items.Item(pField).Specific as StaticText;
                        staticText.Caption = value as string;
                        break;
                    case SAPbouiCOM.BoFormItemTypes.it_PICTURE:
                        pictureBoxItem = (SAPbouiCOM.PictureBox)oForm.Items.Item(pField).Specific;
                        pictureBoxItem.Picture = value.ToString();
                        break;

                    case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
                        comboBoxItem = (SAPbouiCOM.ComboBox)oForm.Items.Item(pField).Specific;
                        comboBoxItem.Select(value.ToString(), SAPbouiCOM.BoSearchKey.psk_ByValue);
                        break;

                    case SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX:
                        checkBoxItem = (SAPbouiCOM.CheckBox)oForm.Items.Item(pField).Specific;
                        checkBoxItem.Checked = Convert.ToBoolean(value);
                        break;

                    case SAPbouiCOM.BoFormItemTypes.it_MATRIX:
                        matrixItem = (SAPbouiCOM.Matrix)oForm.Items.Item(pField).Specific;
                        columnItem = matrixItem.Columns.Item(pCol);

                        // if the column is a UDF, the use the generic set method
                        if (pCol is string && ((string)pCol).Length > 2)
                        {
                            if (((string)pCol).Substring(0, 2) == "U_")
                            {
                                matrixItem.SetCellWithoutValidation(iRow, pCol, value.ToString());
                                return;
                            }
                        }

                        switch (columnItem.Type)
                        {
                            case SAPbouiCOM.BoFormItemTypes.it_EDIT:
                            case SAPbouiCOM.BoFormItemTypes.it_EXTEDIT:
                            case SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON:
                                editTextItem = (SAPbouiCOM.EditText)matrixItem.Columns.Item(pCol).Cells.Item(iRow).Specific;
                                editTextItem.String = value.ToString();
                                break;

                            case SAPbouiCOM.BoFormItemTypes.it_PICTURE:
                                pictureBoxItem = (SAPbouiCOM.PictureBox)matrixItem.Columns.Item(pCol).Cells.Item(iRow).Specific;
                                pictureBoxItem.Picture = value.ToString();
                                break;

                            case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
                                //if (value.GetType() == string)
                                comboBoxItem = (SAPbouiCOM.ComboBox)matrixItem.Columns.Item(pCol).Cells.Item(iRow).Specific;
                                comboBoxItem.Select(value.ToString(), SAPbouiCOM.BoSearchKey.psk_ByValue);

                                //    comboBoxItem = (SAPbouiCOM.ComboBox)matrixItem.Columns.Item(columName).Cells.Item(rowNumber).Specific;
                                //    comboBoxItem.Select(1, SAPbouiCOM.BoSearchKey.psk_Index);

                                //case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
                                //    comboBoxCol = (SAPbouiCOM.ComboBoxColumn)matrixItem.Columns.Item(columName).Cells.Item(rowNumber).Specific;
                                //    comboBoxCol.SetSelectedValue(rowNumber, );
                                break;

                            case SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX:
                                checkBoxItem = (SAPbouiCOM.CheckBox)matrixItem.Columns.Item(pCol).Cells.Item(iRow).Specific;
                                checkBoxItem.Checked = Convert.ToBoolean(value);
                                break;
                        }
                        break;

                    case SAPbouiCOM.BoFormItemTypes.it_GRID:
                        gridItem = (SAPbouiCOM.Grid)oForm.Items.Item(pField).Specific;
                        gridItem.DataTable.SetValue(pCol, iRow, value);
                        break;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (currentPane >= 0 && paneNumber >= 0 && paneNumber != currentPane)
                {
                    oForm.Freeze(false);
                    oForm.PaneLevel = currentPane;
                }
            }
        }

        public static string Status(Form pForm, string pField = "81")
		{
			// get the value of the Status field
			// open 1   open-printed 2    closed 3    calcelled 4    Unapproved  5    draft 6

			try
			{
				var status = CommonUI.Forms.GetField<int>(pForm, pField);
				switch (status)
				{
					case 1:
						return "Open";

					case 2:
						return "Open-Printed";

					case 3:
						return "Closed";

					case 4:
						return "Cancelled";

					case 5:
						return "Unapproved";

					case 6:
						return "Draft";

					default:
						return "";
				}
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		public static Boolean GUI_ColumnExists(Form pForm, string pMat, string pCol)
		{
			// see if a GUI column exists

			SAPbouiCOM.Matrix oMat = null;
			Column oCol = null;

			try
			{
				oMat = (SAPbouiCOM.Matrix)pForm.Items.Item(pMat).Specific;
				oCol = oMat.Columns.Item(pCol);
				return true;
			}
			catch
			{
				return false;
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oMat);
				NSC_DI.UTIL.Misc.KillObject(oCol);
				GC.Collect();
			}
		}

		public static Boolean GUI_ItemExists(Form pForm, object pItem)
		{
			// see if a GUI item exists

			Item oItm = null;

			try
			{
				oItm = pForm.Items.Item(pItem);
				return true;
			}
			catch (Exception ex)
			{
				return false;
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oItm);
				oItm = null;
				GC.Collect();
			}
		}

		public static void SetAutoManaged(Form oForm, string UniqueID, bool AllMode)
		{
			SAPbouiCOM.Item oItem = null;
			try
			{
				oItem = oForm.Items.Item(UniqueID);

				if (oItem != null)
				{
					if (AllMode)
					{
						oItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_All), SAPbouiCOM.BoModeVisualBehavior.mvb_True);
					}
					else
					{
						oItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_All), SAPbouiCOM.BoModeVisualBehavior.mvb_False);
					}
				}
			}
			catch (Exception ex)
			{
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
                NSC_DI.UTIL.Misc.KillObject(oItem);
				GC.Collect();
			}
		}

		public static void SetAutoManaged(Form oForm, string UniqueID, bool OKMode, bool AddMode, bool FindMode, bool ViewMode)
		{
			SAPbouiCOM.Item oItem = null;
			try
			{
				oItem = oForm.Items.Item(UniqueID);

				if (oItem != null)
				{
					if (OKMode)
					{
						oItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_Ok), SAPbouiCOM.BoModeVisualBehavior.mvb_True);
					}
					else
					{
						oItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_Ok), SAPbouiCOM.BoModeVisualBehavior.mvb_False);
					}
					if (AddMode)
					{
						oItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_Add), SAPbouiCOM.BoModeVisualBehavior.mvb_True);
					}
					else
					{
						oItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_Add), SAPbouiCOM.BoModeVisualBehavior.mvb_False);
					}
					if (FindMode)
					{
						oItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_Find), SAPbouiCOM.BoModeVisualBehavior.mvb_True);
					}
					else
					{
						oItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_Find), SAPbouiCOM.BoModeVisualBehavior.mvb_False);
					}
					if (ViewMode)
					{
						oItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_View), SAPbouiCOM.BoModeVisualBehavior.mvb_True);
					}
					else
					{
						oItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, Convert.ToInt32(SAPbouiCOM.BoAutoFormMode.afm_View), SAPbouiCOM.BoModeVisualBehavior.mvb_False);
					}
				}
			}
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItem);
                GC.Collect();
            }
        }


		public static class Create
		{
			public static SAPbouiCOM.Item Item(SAPbouiCOM.Form oForm, string sID, SAPbouiCOM.BoFormItemTypes ObjType,
                                              int iTop, int iLeft, int iHeight, int iWidth, int iFromPane = 0, int iToPane = 0,
                                              string sTitle = "", string sLinkTo = "", string sBindTable = "", string sBindAlias = "",
                                              bool bActive = true, bool bVisible = true, bool bAffectsFormMode = true, bool bSetAutoManaged = true)
			{
				SAPbouiCOM.Item oRtn = default(SAPbouiCOM.Item);

				// routine to create a UI object - ACTUAL FORM ID

				// if the height or width is -1, then use the system default

				//Dim oItm As SAPbouiCOM.Item = Nothing
				SAPbouiCOM.Button		oBtn = null;
				SAPbouiCOM.StaticText	oSta = null;
				SAPbouiCOM.EditText		oEdt = null;
				SAPbouiCOM.EditText		oExt = null;
				SAPbouiCOM.Grid			oGrd = null;
				SAPbouiCOM.Matrix		oMat = null;
				SAPbouiCOM.ComboBox		oCmb = null;
				SAPbouiCOM.CheckBox		oChk = null;
				SAPbouiCOM.OptionBtn	oOpt = null;
				SAPbouiCOM.Folder		oFdr = null;


				try
				{
					oRtn = oForm.Items.Add(sID, ObjType);
					oRtn.Top = iTop;
					oRtn.Left = iLeft;
					if (iHeight < 0)
					{
						oRtn.Height = B1WizardBase.B1Connections.theAppl.GetFormItemDefaultHeight((SAPbouiCOM.BoFormSizeableItemTypes)ObjType);
					}
					else
					{
						oRtn.Height = iHeight;
					}
					if (iWidth < 0)
					{
						oRtn.Width = B1WizardBase.B1Connections.theAppl.GetFormItemDefaultWidth((SAPbouiCOM.BoFormSizeableItemTypes)ObjType);
					}
					else
					{
						oRtn.Width = iWidth;
					}
					oRtn.Visible		 = bVisible;
					oRtn.Enabled		 = bActive;
					oRtn.FromPane		 = iFromPane;
					oRtn.ToPane			 = iToPane;
					oRtn.AffectsFormMode = bAffectsFormMode;

					if (!string.IsNullOrEmpty(sLinkTo)) oRtn.LinkTo = sLinkTo;

					//if (LIBDI.Strings.IsEmpty(sBindAlias) == false) oForm.DataSources.UserDataSources.Add(sBindAlias, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254);

					switch (ObjType)
					{
						case SAPbouiCOM.BoFormItemTypes.it_BUTTON:
							oBtn = (SAPbouiCOM.Button)oRtn.Specific;
							if (!string.IsNullOrEmpty(sTitle)) oBtn.Caption = sTitle;
							// mainly for "OK" and "Cancle" buttons
							break;

						case SAPbouiCOM.BoFormItemTypes.it_EDIT:
							oEdt = (SAPbouiCOM.EditText)oRtn.Specific;
							if (string.IsNullOrEmpty(sBindAlias) == false) oEdt.DataBind.SetBound(true, sBindTable, sBindAlias);
							break;

						case (SAPbouiCOM.BoFormItemTypes.it_EXTEDIT):
							oExt = (SAPbouiCOM.EditText)oRtn.Specific;
							if (string.IsNullOrEmpty(sBindAlias) == false) oExt.DataBind.SetBound(true, sBindTable, sBindAlias);
							break;

						case SAPbouiCOM.BoFormItemTypes.it_GRID:
							oGrd = (SAPbouiCOM.Grid)oRtn.Specific;
							break;

						case SAPbouiCOM.BoFormItemTypes.it_MATRIX:
							oMat = (SAPbouiCOM.Matrix)oRtn.Specific;
							break;

						case SAPbouiCOM.BoFormItemTypes.it_STATIC:
							oSta = (SAPbouiCOM.StaticText)oRtn.Specific;
							oSta.Caption = sTitle;
							break;

						case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
							oRtn.DisplayDesc = true;
							oCmb = (SAPbouiCOM.ComboBox)oRtn.Specific;
							if (string.IsNullOrEmpty(sBindAlias) == false) oCmb.DataBind.SetBound(true, sBindTable, sBindAlias);
							break;

						case SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX:
							oRtn.DisplayDesc = true;
							oChk = (SAPbouiCOM.CheckBox)oRtn.Specific;
							if (string.IsNullOrEmpty(sBindAlias) == false) oChk.DataBind.SetBound(true, sBindTable, sBindAlias);
							break;

						case SAPbouiCOM.BoFormItemTypes.it_OPTION_BUTTON:
							oRtn.DisplayDesc = true;
							oOpt = (SAPbouiCOM.OptionBtn)oRtn.Specific;
							if (string.IsNullOrEmpty(sBindAlias) == false) oOpt.DataBind.SetBound(true, sBindTable, sBindAlias);
							if (!string.IsNullOrEmpty(sTitle)) oOpt.Caption = sTitle;
							break;

						case SAPbouiCOM.BoFormItemTypes.it_FOLDER:
							oFdr = (SAPbouiCOM.Folder)oRtn.Specific;
							oFdr.Caption = sTitle;
							break;
					}

					// for some reason, setting to false causes problems with the Quick Add
					if (bSetAutoManaged == true) Forms.SetAutoManaged(oForm, sID, bSetAutoManaged);    // this is for ALL modes.

					return oRtn;
				}
				//catch (Exception ex)
				//{
				//    // Item already exists
				//    oRtn = oForm.Items.Item(sID);
				//}
				catch (Exception ex)
				{
					oForm.Visible = true;
					throw new Exception(NSC_DI.UTIL.Message.Format(ex));
				}
				finally
				{
					// exit try ends up here, but the objects must be instantiated otherwise there will be an error.
					//If Not oForm Is Nothing Then System.Runtime.InteropServices.Marshal.ReleaseComObject(oForm)
					NSC_DI.UTIL.Misc.KillObject(oBtn);
					NSC_DI.UTIL.Misc.KillObject(oSta);
					NSC_DI.UTIL.Misc.KillObject(oEdt);
					NSC_DI.UTIL.Misc.KillObject(oExt);
					NSC_DI.UTIL.Misc.KillObject(oGrd);
					NSC_DI.UTIL.Misc.KillObject(oMat);
					NSC_DI.UTIL.Misc.KillObject(oFdr);
					NSC_DI.UTIL.Misc.KillObject(oOpt);
					GC.Collect();
				}
			}
		}

		public static class SetFieldvalue
		{
			public static void Logo(Form pForm, string pImgField = "IMG_Logo", string pImageFile = "ViridianLogo.bmp")
			{
				Picture(pForm, pImgField, pImageFile);
			}

			public static void Icon(Form pForm, string pImgField, string pImageFile)
			{
				Picture(pForm, pImgField, System.IO.Path.Combine(@"Icons\", pImageFile));
			}

			public static void Loader(Form pForm, string pImgField, string pImageFile)
			{
				Picture(pForm, pImgField, System.IO.Path.Combine(@"Loader\", pImageFile));
			}

			public static void Weather(Form pForm, string pImgField, string pImageFile)
			{
				Picture(pForm, pImgField, System.IO.Path.Combine(@"Weather\", pImageFile));
			}

			public static void Picture(Form pForm, string pImgField, string pImageFile)
			{
                Button      imageBtn = null;
                PictureBox  imageBox = null;
				var s = System.Windows.Forms.Application.StartupPath;
				try
				{
					if (GUI_ItemExists(pForm, pImgField) == false) return;
                    if(pForm.Items.Item(pImgField).Type == BoFormItemTypes.it_BUTTON)
                    {
                        imageBtn        = pForm.Items.Item(pImgField).Specific;
                        imageBtn.Image  = System.IO.Path.Combine(Globals.pathToImg, pImageFile);
                    }
                    if (pForm.Items.Item(pImgField).Type == BoFormItemTypes.it_PICTURE)
                    {
                        imageBox         = pForm.Items.Item(pImgField).Specific;
                        imageBox.Picture = System.IO.Path.Combine(Globals.pathToImg, pImageFile);
                    }
				}
				catch (Exception ex)
				{
					throw new Exception(NSC_DI.UTIL.Message.Format(ex));
				}
				finally
				{
                    NSC_DI.UTIL.Misc.KillObject(imageBtn);
                    NSC_DI.UTIL.Misc.KillObject(imageBox);
					GC.Collect();
				}
			}
		}
    }
}
