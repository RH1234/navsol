﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavSol.CommonUI
{
    class Grid
    {
        private static string sTmp = "";
        private static string SQL = "";

        public static void DeleteSelectedRowsGUI(SAPbouiCOM.Grid oGrid, ref int iErr)
        {
            //  delete rows from screen

            if (iErr != 0) return;

            try
            {
                int cnt = oGrid.Rows.SelectedRows.Count;
                if (cnt < 1)
                {
                    //StatusBar.ErrorBeep("There are no rows selected to remove.");
                    return;
                }

                // first, need to save all of the selected rows because as soon as a row is deleted, the "selections" are cleared.
                int[] selectedRows = new int[cnt];
                for (int i = 0; i < cnt; i++)
                {
                    selectedRows[i] = oGrid.Rows.SelectedRows.Item(i, SAPbouiCOM.BoOrderType.ot_SelectionOrder);
                }

                // now delete the select rows
                for (int i = cnt - 1; i >= 0; i--)
                {
                    oGrid.DataTable.Rows.Remove(oGrid.GetDataTableRowIndex(selectedRows[i]));
                }
            }
            catch (Exception ex)
            {
                iErr = 1;
                NSC_DI.UTIL.Message.Format(ex);
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void DeleteSelectedRowsDB(SAPbobsCOM.Company oComp, SAPbouiCOM.Grid oGrid, string pTable, string pKeyField, string pKeyValue, string pLineField, ref int iErr)
        {
            //  delete rows from screen and the database - NOT COMPLETED YET

            if (iErr != 0) return;

            try
            {
                //int cnt = oGrid.Rows.SelectedRows.Count;
                //if (cnt < 1)
                //{
                //    StatusBar.ErrorBeep("There are no rows selected to remove.");
                //    return;
                //}

                //DeleteSelectedRowsGUI(oGrid, ref iErr);
                //if (iErr != 0) return;

                //// all rows from the database
                //SQL = "DELETE FROM " + pTable + " WHERE " + pKeyField + " = '" + pKeyValue + "'";
                //if (NBSLIBDI.SQLDirect.SQLCommand(oComp, SQL) < 1) { iErr = 2; return; }

                //// add the rows in from the Grid
                //cnt = oGrid.Rows.Count;
                //for (int i = 0; i < cnt; i++)
                //{
                //    //SQL="INSERT INTO "+pTable+"
                //}
            }
            catch (Exception ex)
            {
                iErr = 1;
                NSC_DI.UTIL.Message.Format(ex);
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void SetColumnDescription(ref SAPbouiCOM.Form oForm, SAPbouiCOM.Grid oGrid, string ColumnUID, string Desc, ref int iErr)
        {

            // the description contains the DB field name and is used for updating

            if (iErr != 0) return;

            try
            {
                oGrid.Columns.Item(ColumnUID).Type = SAPbouiCOM.BoGridColumnType.gct_EditText;
                SAPbouiCOM.EditTextColumn oEditTextColumn = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item(ColumnUID);
                oEditTextColumn.Description = Desc;
            }
            catch (Exception ex)
            {
                iErr = 1;
                NSC_DI.UTIL.Message.Format(ex);
            }
        }

        public static void SetColumnLinkedObject(ref SAPbouiCOM.Form oForm, SAPbouiCOM.Grid oGrid, string ColumnUID, SAPbobsCOM.BoObjectTypes LinkedObjectType, ref int iErr)
        {

            if (iErr != 0) return;

            try
            {
                oGrid.Columns.Item(ColumnUID).Type = SAPbouiCOM.BoGridColumnType.gct_EditText;
                SAPbouiCOM.EditTextColumn oEditTextColumn = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item(ColumnUID);
                oEditTextColumn.LinkedObjectType = ((int)LinkedObjectType).ToString();
            }
            catch (Exception ex)
            {
                iErr = 1;
                NSC_DI.UTIL.Message.Format(ex);
            }
        }

        public static void SetColumnType(ref SAPbouiCOM.Grid oGrid, object ColumnID, SAPbouiCOM.BoGridColumnType ColumnType, ref int iErr)
        {

            if (iErr != 0) return;

            if (oGrid == null) return;

            try
            {
                oGrid.Columns.Item(ColumnID).Type = ColumnType;
            }
            catch (Exception ex)
            {
                iErr = 1;
                NSC_DI.UTIL.Message.Format(ex);
            }
        }

        public static void SetColumnLinkedObject(ref SAPbouiCOM.Form oForm, SAPbouiCOM.Grid oGrid, int ColumnUID, SAPbobsCOM.BoObjectTypes LinkedObjectType, ref int iErr)
        {

            if (iErr != 0) return;

            try
            {
                oGrid.Columns.Item(ColumnUID).Type = SAPbouiCOM.BoGridColumnType.gct_EditText;
                SAPbouiCOM.EditTextColumn oEditTextColumn = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item(ColumnUID);
                oEditTextColumn.LinkedObjectType = ((int)LinkedObjectType).ToString();
            }
            catch (Exception ex)
            {
                iErr = 1;
                NSC_DI.UTIL.Message.Format(ex);
            }
        }

        public static void SetColumnChooseFromList(ref SAPbouiCOM.Form oForm, ref SAPbouiCOM.Grid oGrid, object ColumnUID, string ChooseFromListUID, string ChooseFromListAliasUID, ref int iErr)
        {

            if (iErr != 0) return;

            try
            {
                SetColumnType(ref oGrid, ColumnUID, SAPbouiCOM.BoGridColumnType.gct_EditText, ref iErr);
                oGrid.Columns.Item(ColumnUID).Type = SAPbouiCOM.BoGridColumnType.gct_EditText;
                SAPbouiCOM.EditTextColumn oEditTextColumn = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item(ColumnUID);

                oEditTextColumn.ChooseFromListUID = ChooseFromListUID;
                if (ChooseFromListAliasUID != "") oEditTextColumn.ChooseFromListAlias = ChooseFromListAliasUID;
            }
            catch (Exception ex)
            {
                iErr = 1;
                NSC_DI.UTIL.Message.Format(ex);
            }
        }

        public static void ColumnComboBoxAddVals(SAPbobsCOM.Company oComp, ref SAPbouiCOM.Grid oGrid, string sColumn, string sSQL, ref int iErr)
        {
            // routine to add values to a combo box based on a query.

            // IF THERE IS FATAL ERROR ON THIS STATEMENT         : oGrid.Columns.Item("Territory").Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            // THE SOULUTION IS TO CAST THE RESULT AS CHARACTER : CAST([@ABY_COMMGROUPS].U_territry AS VARCHAR (20))

            if (iErr != 0) return;

            SAPbouiCOM.ComboBoxColumn oColCmbox = null;

            try
            {
                oGrid.Columns.Item(sColumn).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox;
                oColCmbox = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item(sColumn);
                oColCmbox.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description;
                ColumnComboBoxAddVals(oComp, oColCmbox, sSQL, ref iErr);

            }
            catch (Exception ex)
            {
                iErr = 1;
                NSC_DI.UTIL.Message.Format(ex);
            }
            finally
            {
                // exit try ends up here, but the objects must be instantiated otherwise there will be and error.
                if (oColCmbox != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oColCmbox);
                GC.Collect();
            }
        }

        public static void ColumnComboBoxAddVals(SAPbobsCOM.Company oComp, SAPbouiCOM.ComboBoxColumn oComboBox, string sSQL, ref int iErr)
        {
            // routine to add values to a combo box based on a query.

            // IF THERE IS FATAL ERROR ON THIS STATEMENT         : oGrid.Columns.Item("Territory").Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            // THE SOULUTION IS TO CAST THE RESULT AS CHARACTER : CAST([@ABY_COMMGROUPS].U_territry AS VARCHAR (20))

            if (iErr != 0)
                return;

            int i1 = 0;
            int iCnt = 0;

            SAPbobsCOM.Recordset oRS = null;

            try
            {
                oRS = (SAPbobsCOM.Recordset)oComp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (sSQL.Length == 0)
                {
                    // just remove the valid values
                    iCnt = oComboBox.ValidValues.Count;
                    if (iCnt < 1)
                        return;

                    for (i1 = iCnt - 1; i1 >= 0; i1 += -1)
                    {
                        oComboBox.ValidValues.Remove(i1, SAPbouiCOM.BoSearchKey.psk_Index);
                    }
                    return;
                }

                oRS.DoQuery(sSQL);
                if (oRS.RecordCount < 1)
                    return;

                for (i1 = 1; i1 <= oRS.RecordCount; i1++)
                {
                    try
                    {
                        oComboBox.ValidValues.Add(oRS.Fields.Item(0).Value.ToString(), oRS.Fields.Item(1).Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        sTmp = ex.Message;  // only to remove compile warning
                        sTmp = "Entry already exists: " + oRS.Fields.Item(0).Value.ToString() + " - " + oRS.Fields.Item(1).Value.ToString() + Environment.NewLine;
                        sTmp = sTmp + sSQL;
                        //B1WizardBase.B1Connections.theAppl.MessageBox(sTmp + Environment.NewLine + "(ComboBoxAddVals)", 1, "Ok", "", "");
                    }
                    oRS.MoveNext();
                }
            }
            catch (Exception ex)
            {
                iErr = 1;
                NSC_DI.UTIL.Message.Format(ex);
            }
            finally
            {
                // exit try ends up here, but the objects must be instantiated otherwise there will be and error.
                if (oRS != null)
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRS);
                GC.Collect();
            }

        }

        public static int iGridRowLevel(SAPbouiCOM.Grid oGrid, ref int iErr)
        {
            // function to return the tree level of the selected row
            // 1 is the top level

            int i1 = 0;

            int iRow = 0;

            if (iErr != 0) return 0;


            try
            {
                iRow = oGrid.Rows.SelectedRows.Item(0, SAPbouiCOM.BoOrderType.ot_RowOrder);
                while (iRow >= 0)
                {
                    i1 = i1 + 1;
                    iRow = oGrid.Rows.GetParent(iRow);
                }

                return i1;

            }
            catch (Exception ex)
            {
                iErr = 1;
                NSC_DI.UTIL.Message.Format(ex);
                return 0;
            }
            finally
            {
                GC.Collect();
            }
        }

        public static void RowAdd(SAPbouiCOM.Form oForm, string sGrid, ref int iErr)
        {

            // add a row to the grid

            if (iErr != 0) return;

            SAPbouiCOM.Grid oGrd = null;

            try
            {
                oGrd = (SAPbouiCOM.Grid)oForm.Items.Item(sGrid).Specific;
                oGrd.DataTable.Rows.Add();
            }
            catch (Exception ex)
            {
                iErr = 1;
                NSC_DI.UTIL.Message.Format(ex);
            }
            finally
            {
                if (oGrd != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oGrd);
                GC.Collect();
            }

        }

        public static string GetCellValue(ref SAPbouiCOM.Form pForm, ref SAPbouiCOM.Grid oGrid, ref SAPbouiCOM.GridColumn oGridColumn, string ColumnID, int Row, ref int iErr)
        {
            string sReturn = "";
            if (oGridColumn == null)
                return (sReturn);
            try
            {
                if (oGridColumn.Type == SAPbouiCOM.BoGridColumnType.gct_CheckBox)
                {
                    sReturn = "N";
                    SAPbouiCOM.CheckBoxColumn oCheckBoxColumn = (SAPbouiCOM.CheckBoxColumn)oGrid.Columns.Item(ColumnID);
                    if (oCheckBoxColumn.IsChecked(Row))
                        sReturn = "Y";
                }
                else if (oGridColumn.Type == SAPbouiCOM.BoGridColumnType.gct_ComboBox)
                {
                    SAPbouiCOM.ComboBoxColumn oComboBoxColumn = (SAPbouiCOM.ComboBoxColumn)oGrid.Columns.Item(ColumnID);
                    sReturn = oComboBoxColumn.GetSelectedValue(Row).Value.ToString();
                }
                else if (oGridColumn.Type == SAPbouiCOM.BoGridColumnType.gct_EditText)
                {
                    SAPbouiCOM.EditTextColumn oEditTextColumn = (SAPbouiCOM.EditTextColumn)oGrid.Columns.Item(ColumnID);
                    sReturn = oEditTextColumn.GetText(Row);
                }
                else if (oGridColumn.Type == SAPbouiCOM.BoGridColumnType.gct_Picture)
                {
                    SAPbouiCOM.PictureColumn oPictureColumn = (SAPbouiCOM.PictureColumn)oGrid.Columns.Item(ColumnID);
                    sReturn = oPictureColumn.GetPath(Row);
                }
            }
            catch (Exception ex)
            {
                iErr = 1;
                NSC_DI.UTIL.Message.Format(ex);
            }
            finally
            {
                //if (oGrd != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oGrd);
                GC.Collect();
            }
            return (sReturn);
        }
        public static void GridDate()
        {
            //This will set a datepicker in the grid to null
            //            SELECT[OWOR].[DocEntry] as 'Production Order', [OWOR].[PlannedQty] as 'Total Quantity', [OITM].InvntryUom as 'UoM', OBTN.MnfSerial as StateID,
            //(SELECT OITL.DocDate FROM OITL WHERE DocDate <= '12/31/1950') AS ExpDate -- This line is so that we can get a null date in the Grid column
            //FROM[OWOR]
        }
    }    
}
