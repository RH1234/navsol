﻿using System;
using System.Collections.Generic;
using SAPbouiCOM;

namespace NavSol.CommonUI
{

    public static class MatrixExtensionMethods
    {
        // CRreate extension method 
        public static List<string> GetStringsOfSelectedRow(this SAPbouiCOM.Matrix matrix, int columnNumber)
        {
            List<string> list = new List<string>();
            for (int i = 1; i < (matrix.RowCount + 1); i++)
            {
                if (matrix.IsRowSelected(i))
                {
                    // Grab the selected row's Plant ID column
                    try
                    {
                        list.Add(matrix.Columns.Item(columnNumber).Cells.Item(i).Specific.Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        //log.Warn("GetStringsOfSelectedRow() error:", ex);
						throw new Exception(NSC_DI.UTIL.Message.Format(ex));
					}
                }
            }
            return list;
        }

        public static List<string> GetStringsOfSelectedRow(this SAPbouiCOM.Matrix matrix, string columnTitle)
        {
            List<string> list = new List<string>();
            for (int i = 1; i < (matrix.RowCount + 1); i++)
            {
                if (matrix.IsRowSelected(i))
                {
                    // Grab the selected row's Plant ID column
                    try
                    {
                        for (int ii = 0; ii < matrix.Columns.Count; ii++)
                        {
                            string title = matrix.Columns.Item(ii).Title;
                            if (columnTitle == title)
                            {
                                list.Add(matrix.Columns.Item(ii).Cells.Item(i).Specific.Value.ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //log.Warn("GetStringsOfSelectedRow() error:", ex);
						throw new Exception(NSC_DI.UTIL.Message.Format(ex));
					}
                }
            }
            return list;
        }

        public static string GetStringOfSelectedRow(this SAPbouiCOM.Matrix matrix, string columnTitle)
        {
            for (int i = 1; i < (matrix.RowCount + 1); i++)
            {
                if (matrix.IsRowSelected(i))
                {
                    // Grab the selected row's Plant ID column
                    try
                    {
                        for (int ii = 0; ii < matrix.Columns.Count; ii++)
                        {
                            string title = matrix.Columns.Item(ii).Title;
                            if (title == columnTitle)
                            {
                                return matrix.Columns.Item(ii).Cells.Item(i).Specific.Value.ToString();
                            }
                        }
                    }
                    catch
                    {

                    }
                }
            }
            return null;
        }

        public static int GetColumnIndex(this SAPbouiCOM.Matrix matrix, string columnTitle)
        {
            try
            {
                for (int i = 0; i < matrix.Columns.Count; i++)
                {
                    string title = matrix.Columns.Item(i).Title;
                    if (title == columnTitle)
                    {
                        return i;
                    }
                }
            }
            catch(Exception e){}

            return -1;
        }

        public static int GetColumnName(this SAPbouiCOM.Matrix oMat, int columnNum)
        {
            try
            {
                string name = oMat.Columns.Item(columnNum).UniqueID;
                //for (int i = 0; i < oMat.Columns.Count; i++)
                //{
                //    string title = oMat.Columns.Item(i).Title;
                //    if (title == columnTitle)
                //    {
                //        return i;
                //    }
                //}
            }
            catch (Exception e) { }

            return -1;
        }

        // extend ViewModel to take string for combobox id and return selected
    }

    public static class Matrix
    {
        //private static string DatabaseTableName { get; set; }

        //private static string MaxrixUID { get; set; }

        //private static Application SBO_Application;
        //private static Form oForm;
        //private static SAPbouiCOM.Matrix oMatrix;
        //private static Columns oCols;
        //private static Column oCol;
        //private static EditText oEdit;

        public static void LoadMatrixDB(Form pForm, string DatabaseTableName, string MatrixUID, List<MatrixColumn> ListOfColumns, string SQLQuery = null)
        {
            // THIS IS THE NEW METHOD OF LOADING WHERE THE COLUMN IDS ARE THE ACTUAL NAMES AND NOT "col_#"

            try
            {
                // Freeze the form UI
                pForm.Freeze(true);

                SAPbouiCOM.Matrix oMatrix = null;
                Columns oColumns = null;
                Column oColumn = null;
                DBDataSource oDBDataSource = null;
                DataTable oDataTable = null;

                // Finding the Matrix item
                oMatrix = ((SAPbouiCOM.Matrix)(pForm.Items.Item(MatrixUID).Specific));

                // Clear any existing columns within the matrix
                int totalColumnCount = oMatrix.Columns.Count;
                if (totalColumnCount != 0)
                {
                    oMatrix.Clear();

                    for (int i = 0; i < totalColumnCount; i++)
                    {
                        oMatrix.Columns.Remove(0);
                    }
                }

                oColumns = oMatrix.Columns;


                // Add DB data sources for the DB bound columns in the matrix
                if (SQLQuery != null)
                {
                    try
                    {
                        bool AlredyExists = false;
                        int iDataTableCount = pForm.DataSources.DataTables.Count;
                        if (iDataTableCount == 0)
                        {
                            //FirstData Table
                            oDataTable = pForm.DataSources.DataTables.Add(DatabaseTableName);
                            pForm.DataSources.DataTables.Item(DatabaseTableName).ExecuteQuery(SQLQuery);
                        }
                        else
                        {
                            //Check if the datatable already exists????
                            for (int i = 0; i < iDataTableCount; i++)
                            {
                                string strHolder = pForm.DataSources.DataTables.Item(i).UniqueID.ToString();
                                if (strHolder == DatabaseTableName)
                                {
                                    AlredyExists = true;
                                }
                            }
                            if (AlredyExists == true)
                            {
                                //Do not add new but get it, set it, and forget it
                                oDataTable = pForm.DataSources.DataTables.Item(DatabaseTableName);
                                //Clear DataTable to hold new data
                                oDataTable.Clear();
                                oDataTable.ExecuteQuery(SQLQuery);
                            }
                            else
                            {
                                // add new Data Table
                                oDataTable = pForm.DataSources.DataTables.Add(DatabaseTableName);
                                pForm.DataSources.DataTables.Item(DatabaseTableName).ExecuteQuery(SQLQuery);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                    }
                    //pForm.DataSources.DataTables.Item(DatabaseTableName).ExecuteQuery(SQLQuery);
                }
                else
                {
                    oDBDataSource = pForm.DataSources.DBDataSources.Add(DatabaseTableName);
                }

                //***********************************
                // Adding column items to the matrix
                //***********************************
                int colID = 0;
                foreach (MatrixColumn oMatrixColumn in ListOfColumns)
                {
                    string columnName = oMatrixColumn.ColumnName;
                    oColumn = oColumns.Add(columnName, oMatrixColumn.ItemType); // column name can only be 10 chars or less
                    oColumn.TitleObject.Caption = oMatrixColumn.Caption;
                    oColumn.Width = oMatrixColumn.ColumnWidth;
                    oColumn.Editable = oMatrixColumn.IsEditable;
                    oColumn.Visible = oMatrixColumn.IsVisable;


                    oColumn.TitleObject.Sortable = true;

                    if (oMatrixColumn.ColumnName != null)
                    {
                        if (SQLQuery != null)
                        {
                            oColumn.DataBind.Bind(DatabaseTableName, oMatrixColumn.ColumnName);
                        }
                        else
                        {
                            oColumn.DataBind.SetBound(true, DatabaseTableName, oMatrixColumn.ColumnName);
                        }
                    }
                    else
                    {
                        // Try to create a new user data source for the column
                        try
                        {
                            pForm.DataSources.UserDataSources.Add("UDS_" + colID.ToString(), oMatrixColumn.DataType);
                        }
                        catch
                        {
                        }
                        oColumn.DataBind.SetBound(true, "", "UDS_" + colID.ToString());
                    }
                    colID++;
                }

                // Get the DBdatasource we base the matrix on
                if (SQLQuery == null)
                {
                    oDBDataSource = pForm.DataSources.DBDataSources.Item(DatabaseTableName);
                    oDBDataSource.Query();
                }

                // Ready Matrix to populate data
                oMatrix.Clear();

                // Load data back to 
                oMatrix.LoadFromDataSource();

                oMatrix.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                pForm.Freeze(false);
                //NSC_DI.UTIL.Misc.KillObject(oBar);
                GC.Collect();
            }
        }
        public static void LoadDatabaseDataIntoMatrix(Form pForm, string DatabaseTableName, string MatrixUID, List<MatrixColumn> ListOfColumns,	string SQLQuery = null, string pUserDataSource = null)
		{
            // THIS IS THE OLD METHOD OF LOADING WHERE THE COLUMN IDS ARE "col_#"
			try
			{
				// Freeze the form UI
				pForm.Freeze(true);

				SAPbouiCOM.Matrix oMatrix = null;
				Columns oColumns = null;
				Column oColumn = null;
				DBDataSource oDBDataSource = null;
				DataTable oDataTable = null;

				// Finding the Matrix item
				oMatrix = ((SAPbouiCOM.Matrix) (pForm.Items.Item(MatrixUID).Specific));

				// Clear any existing columns within the matrix
				int totalColumnCount = oMatrix.Columns.Count;
				if (totalColumnCount != 0)
				{
					oMatrix.Clear();

					for (int i = 0; i < totalColumnCount; i++)
					{
						oMatrix.Columns.Remove(0);
					}
				}

				oColumns = oMatrix.Columns;


				// Add DB data sources for the DB bound columns in the matrix
				if (SQLQuery != null)
				{
					try
					{
                        bool AlredyExists = false;
                        int iDataTableCount = pForm.DataSources.DataTables.Count;
                        if (iDataTableCount == 0)
                        {
                            //FirstData Table
                            oDataTable = pForm.DataSources.DataTables.Add(DatabaseTableName);
                            pForm.DataSources.DataTables.Item(DatabaseTableName).ExecuteQuery(SQLQuery);
                        }
                        else
                        {
                            //Check if the datatable already exists????
                            for(int i=0; i< iDataTableCount; i++)
                            {
                                string strHolder = pForm.DataSources.DataTables.Item(i).UniqueID.ToString();
                                if(strHolder == DatabaseTableName)
                                {
                                    AlredyExists = true;
                                }
                            }
                            if(AlredyExists==true)
                            {
                                //Do not add new but get it, set it, and forget it
                                oDataTable = pForm.DataSources.DataTables.Item(DatabaseTableName);
                                //Clear DataTable to hold new data
                                oDataTable.Clear();
                                oDataTable.ExecuteQuery(SQLQuery);
                            }
                            else
                            {
                                // add new Data Table
                                oDataTable = pForm.DataSources.DataTables.Add(DatabaseTableName);
                                pForm.DataSources.DataTables.Item(DatabaseTableName).ExecuteQuery(SQLQuery);
                            }

                        }
					}
					catch (Exception ex)
                    {
                        throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                    }
					//pForm.DataSources.DataTables.Item(DatabaseTableName).ExecuteQuery(SQLQuery);
				}
				else
				{
					oDBDataSource = pForm.DataSources.DBDataSources.Add(DatabaseTableName);
				}
                
                //***********************************
                // Adding column items to the matrix
                //***********************************
                int colID = 0;
				foreach (MatrixColumn oMatrixColumn in ListOfColumns)
				{
					string columnName = "col_" + colID.ToString();
					oColumn = oColumns.Add(columnName, oMatrixColumn.ItemType);
					oColumn.TitleObject.Caption = oMatrixColumn.Caption;
					oColumn.Width = oMatrixColumn.ColumnWidth;
					oColumn.Editable = oMatrixColumn.IsEditable;
                    oColumn.Visible = oMatrixColumn.IsVisable;


                    oColumn.TitleObject.Sortable = true;

					if (oMatrixColumn.ColumnName != null)
					{
						if (SQLQuery != null)
						{
							oColumn.DataBind.Bind(DatabaseTableName, oMatrixColumn.ColumnName);
						}
						else
						{
							oColumn.DataBind.SetBound(true, DatabaseTableName, oMatrixColumn.ColumnName);
						}
					}
					else
					{
						// Try to create a new user data source for the column
						try
						{
							pForm.DataSources.UserDataSources.Add("UDS_" + colID.ToString(), oMatrixColumn.DataType);
						}
						catch
						{
						}
						oColumn.DataBind.SetBound(true, "", "UDS_" + colID.ToString());
					}
                    if(oMatrixColumn.DataType == BoDataType.dt_DATE && !string.IsNullOrEmpty(pUserDataSource?.Trim())) 
                    {
                        //12582 if the datatype is date and pUDS has a val, instantiates a newuserdata source then the current col is the bound to the UserDataSource created in the calling form.  
                        var test = pForm.DataSources.UserDataSources.Count;
                        if (pForm.DataSources.UserDataSources.Count == 1)
                            pForm.DataSources.UserDataSources.Add(pUserDataSource, SAPbouiCOM.BoDataType.dt_DATE, 254);
                     
                        oColumn = oMatrix.Columns.Item(columnName);
                        oColumn.DataBind.SetBound(true, "", pUserDataSource);
                    }
					colID++;
				}

				// Get the DBdatasource we base the matrix on
				if (SQLQuery == null)
				{
                    oDBDataSource = pForm.DataSources.DBDataSources.Item(DatabaseTableName);
					oDBDataSource.Query();
				}

				// Ready Matrix to populate data
				oMatrix.Clear();

				// Load data back to 
				oMatrix.LoadFromDataSource();

				oMatrix.AutoResizeColumns();
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
				Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				pForm.Freeze(false);
				//NSC_DI.UTIL.Misc.KillObject(oBar);
				GC.Collect();
			}
		}

		public static void SetLink(SAPbouiCOM.Matrix pMat, int pCol, SAPbouiCOM.BoLinkedObject pType)
		{
			try
			{
				var col = pMat.Columns.Item(pCol).UniqueID;
				SetLink(pMat, col, pType);
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

		public static void SetLink(SAPbouiCOM.Matrix pMat, string pCol, SAPbouiCOM.BoLinkedObject pType)
		{
			SAPbouiCOM.LinkedButton oLink	= null;
			SAPbouiCOM.Column oCol			= null;

			try
			{
				// link the Drill Down Column to the correct form.
				//oLink = ((SAPbouiCOM.LinkedButton)(SAPbouiCOM.Column) pMat.Columns.Item(pCol).ExtendedObject);
				oCol = pMat.Columns.Item(pCol);
				oLink = ((SAPbouiCOM.LinkedButton)(oCol.ExtendedObject));
				oLink.LinkedObject = pType;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oLink);
				NSC_DI.UTIL.Misc.KillObject(oCol);
				GC.Collect();
			}
		}

        public static void SelectRow(Form pForm, ItemEvent pVal, BoMatrixSelect? pMode = null)
        {
            // select the row even if the column is not the 1st column
            SAPbouiCOM.Matrix oMat = null;

            try
            {
                if(pForm.Items.Item(pVal.ItemUID).Type != BoFormItemTypes.it_MATRIX) return;
                oMat = pForm.Items.Item(pVal.ItemUID).Specific;
                if (pVal.Row <= 0) return;
                if (pVal.Row > oMat.RowCount) return;

                // check the column number
                if (oMat.Columns.Item(0).UniqueID == pVal.ColUID) return;   // 1st column. don't need to select it.

                var sMode = pMode ?? oMat.SelectionMode;
                if (sMode == BoMatrixSelect.ms_None && pMode == null) return;
                if (oMat.SelectionMode == BoMatrixSelect.ms_NotSupported) return;

                var state = oMat.IsRowSelected(pVal.Row);

                if (sMode == BoMatrixSelect.ms_Single) oMat.SelectionMode = BoMatrixSelect.ms_None;

                oMat.SelectionMode = sMode;
                oMat.SelectRow(pVal.Row, state == false, sMode == BoMatrixSelect.ms_Auto);

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }

        public static string[] GetColumnList(SAPbouiCOM.DataTable pDT)
        {
            try
            {
                string[] cols = new string[pDT.Columns.Count];

                // Loop through all the columns of the DataTable and load the column names into an array.
                for (int t = 0; t < pDT.Columns.Count; t++)
                {
                    cols[t] = pDT.Columns.Item(t).Name;
                }
                return cols;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        public static int ColumnSelected(SAPbouiCOM.Matrix pMat, out List<ValueTuple<(int Number, string Name, bool Selected)>> ColList)
        {
            List<ValueTuple<(int Number, string Name, bool Selected)>> list = new List<ValueTuple<(int Number, string Name, bool Selected)>>();
            var cnt = 0;

            // Loop through all the columns of the Matrix.
            for (int t = 0; t < pMat.Columns.Count; t++)
            {
                //list.Item1.Number = t;
                //list.Item1.Name = pMat.Columns.Item(t).Title;
                //list.Item1.Selected = pMat.Columns.Item(t).Selected;

                //ValueTuple<(int Number, string Name, bool Selected)> rec =  (t, pMat.Columns.Item(t).Title, pMat.Columns.Item(t).Selected);

                //list.Add((Number: t, Name: pMat.Columns.Item(t).Title, Selected: pMat.Columns.Item(t).Selected));

                ValueTuple<(int Number, string Name, bool Selected)> rec;
                rec.Item1.Number = t;
                rec.Item1.Name = pMat.Columns.Item(t).Title;
                rec.Item1.Selected = pMat.Columns.Item(t).Selected;
                if (rec.Item1.Selected) cnt++;
                list.Add(rec);
            }

            ColList = list;
            return cnt;
        }

        public static int ColumnSelected(SAPbouiCOM.Matrix pMat, out bool[] Selected)
        {
            bool[] list = new bool[pMat.Columns.Count];
            var cnt = 0;
            Selected = null;
            try
            {

                // Loop through all the columns of the Matrix .
                for (int t = 0; t < pMat.Columns.Count; t++)
                {
                    list[t] = pMat.Columns.Item(t).Selected;
                    if (list[t]) cnt++;
                }

                Selected = list;
                return cnt;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return cnt;
            }
            finally
            {
                GC.Collect();
            }
        }

        public static int RowSelected(SAPbouiCOM.Matrix pMat, out bool[] Selected)
        {
            var cnt = 0;
            Selected = null;
            try
            {
                // Matrix rows start at 1. DataTables start at 0
                // set up for DataTable use.
                bool[] list = new bool[pMat.RowCount];    

                // change to use:
                var ndx = 0;
                do
                {
                    ndx = pMat.GetNextSelectedRow(ndx, BoOrderType.ot_RowOrder);
                    if (ndx < 1) break;
                    list[ndx-1] = true;
                    cnt++;
                } while (true);

                Selected = list;
                return cnt;

                //// Loop through all the columns of the Matrix .
                //for (int i = 1; i <= pMat.RowCount; i++)
                //{
                //    list[i] = pMat.IsRowSelected(i);
                //    if (list[i]) cnt++;
                //}

                //Selected = list;
                //return cnt;

            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                return cnt;
            }
            finally
            {
                GC.Collect();
            }
        }

        public class MatrixColumn
        {
			public string ColumnName { get; set; }

			public string Caption { get; set; }

			public int ColumnWidth { get; set; }

			public BoFormItemTypes ItemType { get; set; }

			public bool IsEditable { get; set; }

            public BoDataType DataType { get; set; } = BoDataType.dt_QUANTITY;

            public bool IsVisable { get; set; } = true;
        }

        //public static void Bindmatrix(string pFormID, string pMxID, string pTblName, string pColumnID, string pColAlias, string pSourceType, string pSourceName)
        //{
        //	//oForm = SBO_Application.Forms.Item(pFormID);
        //	//oMatrix = oForm.Items.Item(pMxID).Specific as SAPbouiCOM.Matrix;
        //	//oCols = oMatrix.Columns;
        //	//oCol = oCols.Item(pColumnID);

        //	try
        //	{
        //		if (pSourceType == "DBDataSource")
        //		{
        //			oCol.DataBind.SetBound(true, pTblName, pColAlias);
        //		}
        //		else if (pSourceType == "UserDataSource")
        //		{
        //			oCol.DataBind.SetBound(true, "", pSourceName);
        //		}
        //	}
        //	catch (Exception e)
        //	{
        //		SBO_Application.MessageBox("Could not Bind Matrix");
        //		SBO_Application.MessageBox(e.ToString());
        //	}
        //}

        public static void loadMatrix(Form pForm, string pMxID, string pSrc)
		{
			SAPbouiCOM.Matrix oMatrix = null;
			try
			{
				oMatrix = pForm.Items.Item(pMxID).Specific as SAPbouiCOM.Matrix;
				DBDataSource oDBS = pForm.DataSources.DBDataSources.Item(pSrc);
				oDBS.Query();
				oMatrix.Clear();
				oMatrix.LoadFromDataSource();
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oMatrix);
				GC.Collect();
			}
		}

		//public string GetCellTxt(SAPbouiCOM.Item Item, string ColUID, int RowIndex, SAPbouiCOM.BoSearchKey ComboBoxSearchKeyType = SAPbouiCOM.BoSearchKey.psk_ByValue)
        //{
        //    string retVal = string.Empty;
        //    if (Item.Specific is SAPbouiCOM.IMatrix)
        //    {

        //        retVal = GetB1ObjectTxt(((SAPbouiCOM.Matrix)Item.Specific).GetCellSpecific(ColUID, RowIndex), ComboBoxSearchKeyType);
        //    }
        //    else if (Item.Specific is SAPbouiCOM.IGrid)
        //    {
        //        retVal = ((SAPbouiCOM.Grid)Item.Specific).DataTable.Columns.Item(ColUID).Cells.Item(RowIndex).Value;
        //    }
        //    return retVal;
        //}
		
		public static List<int> getSelected_MXRows_DocEntry(ref SAPbouiCOM.Matrix pMx)
		{
			EditText oEdit = null;

            List<int> list = new List<int>();

            int intHolder;
            string strHolder = null;
            for (int i = 1; i <= pMx.RowCount; i++)
            {
                if (pMx.IsRowSelected(i))
                {
                    oEdit = pMx.GetCellSpecific("DocEntry", i) as EditText;
                    strHolder = oEdit.Value;
                    intHolder = Convert.ToInt32(strHolder);
                    list.Add(intHolder);

                }
            }

            return list;
        }

		public static Dictionary<int, int> getSelected_MXRows_DocEntry_n_RowNo(ref SAPbouiCOM.Matrix pMx)
		{
			EditText oEdit = null;

			try
			{
				Dictionary<int, int> Dict = new Dictionary<int, int>();

				int intHolder;
				string strHolder = null;
				for (int i = 1; i <= pMx.RowCount; i++)
				{
					if (pMx.IsRowSelected(i))
					{
						oEdit = pMx.GetCellSpecific("DocEntry", i) as EditText;
						strHolder = oEdit.Value;
						intHolder = Convert.ToInt32(strHolder);
						Dict.Add(intHolder, i);

					}
				}

				return Dict;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				NSC_DI.UTIL.Misc.KillObject(oEdit);
				GC.Collect();
			}
		}

		public static int getSelected_MXRow(ref SAPbouiCOM.Matrix pMx)
        {
            int Val = 1;
            try
            {
                while (!(pMx.IsRowSelected(Val)))
                {
                    Val++;
                }
            }
            catch
            {
                Val = -1;
            }


            return Val;
        }
        
		//public static void SetCellTxt(this SAPbouiCOM.Item Item, string ColID, int RowIndex, string Val, SAPbouiCOM.BoSearchKey cmbSearchKeyType = SAPbouiCOM.BoSearchKey.psk_ByValue, bool SkipNULLs = true)
        //{
        //    if (Item.Specific is SAPbouiCOM.IMatrix)
        //    {
        //        SetB1ObjectTxt((((SAPbouiCOM.Matrix)Item.Specific).GetCellSpecific(ColID, RowIndex)), Val, cmbSearchKeyType, SkipNULLs);
        //    }
        //    else if (Item.Specific is SAPbouiCOM.IGrid)
        //    {
        //        ((SAPbouiCOM.Grid)Item.Specific).DataTable.Columns.Item(ColID).Cells.Item(RowIndex).Value = Val;
        //    }
        //}

		public static string GetB1ObjectTxt(dynamic B1Obj, BoSearchKey cmbSearchKeyType = BoSearchKey.psk_ByValue)
        {
            object rVal = null;
            try
            {
                if (B1Obj is ICheckBox)
                {
                    rVal = CheckBox.GetValue((SAPbouiCOM.CheckBox)B1Obj);
                }
                if (B1Obj is IEditText)
                {
                    if (cmbSearchKeyType == BoSearchKey.psk_ByValue)
                        rVal = ((EditText)B1Obj).Value;
                    else if (cmbSearchKeyType == BoSearchKey.psk_ByDescription)
                        rVal = ((EditText)B1Obj).String;
                }
                else if (B1Obj is IStaticText)
                {
                    rVal = ((StaticText)B1Obj).Caption;
                }
                else if (B1Obj is IComboBox)
                {
                    if (cmbSearchKeyType == BoSearchKey.psk_ByValue)
                        rVal = ((SAPbouiCOM.ComboBox)B1Obj).Selected.Value;
                    else
                        rVal = ((SAPbouiCOM.ComboBox)B1Obj).Selected.Description;
                }
                else if (B1Obj is IButton)
                {
                    rVal = ((Button)B1Obj).Caption;
                }
                else if (B1Obj is IStatusBar)
                {
                    rVal = ((StatusBar)B1Obj).ToString();
                }
                else if (B1Obj is IFolder)
                {
                    rVal = ((Folder)B1Obj).Caption;
                }
                else if (B1Obj is IForm)
                {
                    rVal = ((Form)B1Obj).Title;
                }
                else if (B1Obj is IMenuItem)
                {
                    rVal = ((MenuItem)B1Obj).String;
                }
            }
            catch
            {

            }
            if (rVal == DBNull.Value || rVal == null) return null;
            return (string)rVal;
        }
    }


    /// <summary>
    /// Class that assists in Sorting a Matrix.
    /// </summary>
	public static class MatrixSortHelper
    {
		private static List<ColumnSortOption> _listOfColumnSorts = new List<ColumnSortOption>();

        /// <summary>
        /// Add a sort option to the Matrix Sort options.
        /// </summary>
        /// <param name="SortOption">Object that contains data to appy sort options to a Matrix.</param>
		public static void AddColumnSortOption(ColumnSortOption SortOption)
        {
            _listOfColumnSorts.Add(SortOption);
        }

        /// <summary>
        /// Apply Sort Options to the Matrix.
        /// </summary>
        /// <param name="MatrixToSort">Matrix to apply sort options.</param>
		public static void ApplySort(SAPbouiCOM.Matrix MatrixToSort)
        {
            // Loop through all the columns of the Matrix and find our specified columns.
            for (int t = 0; t < MatrixToSort.Columns.Count; t++)
            {
                string title = MatrixToSort.Columns.Item(t).Title;
                foreach (ColumnSortOption colSort in _listOfColumnSorts)
                {
                    // If the title(ColumnCaption) matches our Sort object ColumnCaption. We apply our sort.
                    if (title == colSort.ColumnCaption)
                    {
                        Column oColumn = MatrixToSort.Columns.Item(t);
                        oColumn.TitleObject.Sortable = colSort.EnableSort;
                        oColumn.TitleObject.Sort(colSort.SortType);
                    }
                }
            }
        }

        public class ColumnSortOption
        {
            public string ColumnCaption;
            public bool EnableSort;
            public BoGridSortType SortType;

            /// <summary>
            /// Create a new Column Sort Option. 
            /// These are used in MatrixSortHelper to apply various sort features to columns.
            /// </summary>
            /// <param name="ColumnCaption">ColumnCaption of the field to Sort by.</param>
            /// <param name="EnableSort">Enable sorting for user.</param>
            /// <param name="SortType">Enumerable specifying the order of the desired sort.</param>
            public ColumnSortOption(string ColumnCaption, bool EnableSort, BoGridSortType SortType)
            {
                this.ColumnCaption = ColumnCaption;
                this.EnableSort = EnableSort;
                this.SortType = SortType;
            }
        }
    }

    /// <summary>
    /// Class to Assist in retrieval of columns. 
    /// Columns can be converted to other SAPboui types such as EditText.
    /// </summary>
    public class MatrixColumnCollector
    {
        private Dictionary<string, ColumnCollectorHelper> _listOfColumns = new Dictionary<string, ColumnCollectorHelper>();
        private SAPbouiCOM.Matrix _matrixToSearch;

        /// <summary>
        /// Create a List of Columns to collect.
        /// </summary>
        /// <param name="CaptionName">Caption Name of the Column.</param>
        /// <param name="MatrixToSearch">Matrix to collect columns from.</param>
        public MatrixColumnCollector(List<string> ColumnCaptionNames, SAPbouiCOM.Matrix MatrixToSearch)
        {
            _matrixToSearch = MatrixToSearch;
            CollectColumnList(ColumnCaptionNames);
        }

        /// <summary>
        /// Create a Single Item Column Collector. 
        /// You can use the AddColumnToCollect to provide more columns.
        /// </summary>
        /// <param name="CaptionName">Caption Name of the Column.</param>
        /// <param name="MatrixToSearch">Matrix to collect columns from.</param>
        public MatrixColumnCollector(string CaptionName, SAPbouiCOM.Matrix MatrixToSearch)
        {
            _matrixToSearch = MatrixToSearch;
            CollectColumnList(new List<string>() { CaptionName });
        }

        /// <summary>
        /// Add in a single CaptionName to the Column Collection helper.
        /// </summary>
        /// <param name="CaptionName">Caption Name of the Column.</param>
        public void AddColumnToCollect(string CaptionName)
        {
            if (_matrixToSearch == null)
            {
                return;
            }

            CollectColumnList(new List<string>() { CaptionName });
        }

        /// <summary>
        /// Perform the operation to get the column specified by the Caption Name.
        /// </summary>
        /// <param name="ColumnCaptionNames">List of CaptionNames to collect. This can be a single item.</param>
        private void CollectColumnList(List<string> ColumnCaptionNames)
        {
            // Loop through all the columns of the Matrix and find our specified columns.
            for (int t = 0; t < _matrixToSearch.Columns.Count; t++)
            {
                string title = _matrixToSearch.Columns.Item(t).Title;
                foreach (string colCaption in ColumnCaptionNames)
                {
                    if (title == colCaption)
                    {
                        ColumnCollectorHelper colHelper = new ColumnCollectorHelper();

                        colHelper.Column = _matrixToSearch.Columns.Item(t);
                        colHelper.IndexOfColumn = t;
                        _listOfColumns.Add(title, colHelper);
                    }
                }
            }
        }

        /// <summary>
        /// Get's the Column in the Matrix by CaptionName
        /// </summary>
        /// <param name="CaptionName">Caption Name of the column.</param>
        /// <returns>SAPbouiCOM.Column which can be cast into other types.</returns>
        public Column GetColumn(string CaptionName)
        {
            if (_listOfColumns.Count > 0)
            {
                if (_listOfColumns.ContainsKey(CaptionName))
                {
                    return _listOfColumns[CaptionName].Column;
                }
            }

            return null;
        }

        /// <summary>
        /// Get the Index location of the Column.
        /// </summary>
        /// <param name="CaptionName">CaptionName of the column.</param>
        /// <returns>Index of the column. If it doens't exist will return -1.</returns>
        public int GetIndexOfColumn(string CaptionName)
        {
            if (_listOfColumns.Count > 0)
            {
                if (_listOfColumns.ContainsKey(CaptionName))
                {
                    return _listOfColumns[CaptionName].IndexOfColumn;
                }
            }

            return -1;
        }

        public class ColumnCollectorHelper
        {
            public Column Column;
            public int IndexOfColumn = -1;
        }
    }
}
