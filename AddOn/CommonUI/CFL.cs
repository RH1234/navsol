﻿using System;
using System.Linq;

namespace NavSol.CommonUI
{
    public static class CFL
    {
        public static void Create(SAPbouiCOM.Form oForm, string sID, string sType)
        {

            try
            {
                Create(oForm, sID, sType, false);
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

        }

        public static void Create(SAPbouiCOM.Form oForm, string sID, SAPbouiCOM.BoLinkedObject pType)
        {

            try
            {
                string sType = Convert.ToInt32(pType).ToString();

                Create(oForm, sID, sType, false);
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }

        }

        public static void Create(SAPbouiCOM.Form oForm, string sID, string sType, bool pMultiSel)
        {
            // routine to create a CFL

            //NOTES:

            // see CFL_Notes() for actual use

            //CFL form is a SBO form with UID = 10001. When you will check
            //pVal.FormType = 10001 and pval.beforeaction = true for formload event type
            //and when you dont need to open this form, set bubbleevent to false.
            //
            //Type form for CLF is 10000 not 10001
            //
            //b0th form types are Choose Form List forms... 
            //The difference between the two is that one of them have the "New" button next to the Cancel button 
            //(all depending on the Object Type in the Choose From List)
            //
            //When the user selects an item from the list, the pval.SelectedObjects object is created and that object is read to get get what the user selected. 
            //When the user cancels the CFL form, this object is not created; therefore it doesn't exist. 
            //So the code in the CFL event handler should read the value only if the pval.SelectedObjects object exists.
            //
            //If Not pval.SelectedObjects Is Nothing Then
            //write your code in this if condition
            //end if
            //

            SAPbouiCOM.ChooseFromListCollection oCFLs = null;
            SAPbouiCOM.ChooseFromList oCFL = null;
            SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;

            try
            {
                oCFLs = oForm.ChooseFromLists;

				// see if already exists
				try
				{
					oCFLs.Item(sID);
					return;
				}
				catch { }

                oCFLCreationParams = (SAPbouiCOM.ChooseFromListCreationParams)Globals.oApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams);

                oCFLCreationParams.MultiSelection = pMultiSel;
                oCFLCreationParams.ObjectType = sType;  // sen CFL_Notes() below
                oCFLCreationParams.UniqueID = sID;
                oCFL = oCFLs.Add(oCFLCreationParams);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oCFL);
                NSC_DI.UTIL.Misc.KillObject(oCFLCreationParams);
                NSC_DI.UTIL.Misc.KillObject(oCFLs);
                GC.Collect();
            }
        }

        public static void CreateWH(SAPbouiCOM.Form pForm, string pID, string pCBField, string pCFLField = "txtWH_CFL", string pDataSource = "dsWHCFL", string pReturnField = "WhsCode")
        {
            // create a WareHouse CFL as a replacement for a combobox list
            SAPbouiCOM.Item oCBField = null;

            try
            {
                if (CommonUI.Forms.HasField(pForm, pCFLField)) return; // field already exists.

                oCBField = pForm.Items.Item(pCBField);
                
                // create the CFL field
                CommonUI.Forms.Create.Item(pForm, pCFLField, SAPbouiCOM.BoFormItemTypes.it_EDIT, oCBField.Top, oCBField.Left, oCBField.Height, oCBField.Width, oCBField.FromPane, oCBField.ToPane);
                CommonUI.Forms.CreateUserDataSource(pForm, pCFLField, pDataSource, SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 50);

                // create the CFL
                string sType = Convert.ToInt32(SAPbouiCOM.BoLinkedObject.lf_Warehouses).ToString();
                Create(pForm, pID, sType, false);
                pForm.Items.Item(pCFLField).Specific.ChooseFromListUID   = pID;
                pForm.Items.Item(pCFLField).Specific.ChooseFromListAlias = pReturnField;

                // hide the old field
                //oCBField.Visible = false;   // this causes an error if the field is in focus
                oCBField.Top = -20;

                // set the tab orders
                pForm.Items.Item(pCFLField).Specific.TabOrder = oCBField.Specific.TabOrder;
                //oCBField.Specific.TabOrder = 999;     // doesn't appear to tab correctly
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oCBField);
                GC.Collect();
            }

        }

        public static void AddCon(SAPbouiCOM.Form oForm, string sID, SAPbouiCOM.BoConditionRelationship Rel, string sDBField, SAPbouiCOM.BoConditionOperation Op, string sValue, 
            int pBracketOpenNum = 0, int pBracketCloseNum = 0)
        {
            // routine to add conditions to a CFL
            // this is the same as a where clause


            SAPbouiCOM.ChooseFromListCollection oCFLs = null;
            SAPbouiCOM.Conditions               oCons = null;
            SAPbouiCOM.Condition                oCon  = null;
            SAPbouiCOM.ChooseFromList           oCFL  = null;

            try
            {
                oCFLs = oForm.ChooseFromLists;
                oCFL = oCFLs.Item(sID);

                // Reset the conditions
                if (Rel == SAPbouiCOM.BoConditionRelationship.cr_NONE)
                {
                    oCFL.SetConditions(oCons);
                }

                oCons = oCFL.GetConditions();

                // If there are already user conditions.
                if (oCons.Count > 0)
                {
                    oCons.Item(oCons.Count - 1).Relationship = Rel;
                }

                // Add Conditions
                // if there is no DB field then do not set any conditions
                if (!string.IsNullOrEmpty(sDBField))
                {
                    oCon = oCons.Add();

                    oCon.Alias = sDBField;
                    oCon.Operation = Op;            //SAPbouiCOM.BoConditionOperation.co_EQUAL
                    oCon.CondVal = sValue;
                    //oCon.CompareFields = true;  // used to compare to another filed in the database. use ComparedAlias
                    if (pBracketOpenNum > 0)  oCon.BracketOpenNum  = pBracketOpenNum;
                    if (pBracketCloseNum > 0) oCon.BracketCloseNum = pBracketCloseNum;
                    oCFL.SetConditions(oCons);
                }


                // reset conditions
                //if (oForm.Items.Item("4").Specific.Value == "") Globals.CFL.CFL_AddCon(ref oForm, "cflSalesQt", BoConditionRelationship.cr_NONE, null, BoConditionOperation.co_NONE, null, ref iErr);


                // this might work to filter the name
   //             Sub SetConditionToCFL()
   //Try
   //         Dim oCFL As SAPbouiCOM.ChooseFromList
   //         Dim oCons As SAPbouiCOM.Conditions
   //         Dim oCon As SAPbouiCOM.Condition
   //         Dim emptyCon As New SAPbouiCOM.Conditions
   //         Dim rs As SAPbobsCOM.Recordset = com.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
   //         oCFL = Conv_Form.ChooseFromLists.Item("TabCFL")
   //         rs.DoQuery("Give ur Query")
   //         oCFL.SetConditions(emptyCon)
   //         oCons = oCFL.GetConditions
   //         For i As Integer = 1 To rs.RecordCount
   //             Debug.Print(rs.Fields.Item(0).Value)
   //             oCon = oCons.Add()
   //             oCon.Alias = "U_SONo"
   //             oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
   //             oCon.CondVal = Trim(rs.Fields.Item("U_SONo").Value)
   //             If i <> (rs.RecordCount) Then
   //                 oCon.Relationship = SAPbouiCOM.BoConditionRelationship.cr_OR
   //             End If
   //             rs.MoveNext()
   //         Next
   //         If rs.RecordCount = 0 Then
   //             oCon = oCons.Add()
   //             oCon.Alias = "U_SONo"
   //             oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
   //             oCon.CondVal = "-1"
   //             oCFL.SetConditions(oCons)
   //         End If
   //         oCFL.SetConditions(oCons)
   //     Catch ex As Exception
   //         app.StatusBar.SetText(ex.Message)
   //     End Try
   // End Sub
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oCFL);
                NSC_DI.UTIL.Misc.KillObject(oCFLs);
                NSC_DI.UTIL.Misc.KillObject(oCons);
                NSC_DI.UTIL.Misc.KillObject(oCon);
                GC.Collect();
            }
        }

        public static void AddCon_Branches(SAPbouiCOM.Form pForm, string sID, SAPbouiCOM.BoConditionRelationship pFirstRel, string[] pBranches = null)
        {
            // routine to add conditions to a CFL for branches
            // this is the same as a where clause

            // if branches are not being used, then the condition is not added.

            try
            {
                if(pBranches == null) pBranches = NSC_DI.SAP.Branch.GetAll_User_Ary(Globals.BranchDflt);
                for (var i = 0; i < pBranches.Length; i++)
                {
                    var bracketOpenNum = (i == 0) ? 1 : 0;
                    var bracketCloseNum = (i == pBranches.Length - 1) ? 1 : 0;
                    var relationship = (i == 0) ? pFirstRel : SAPbouiCOM.BoConditionRelationship.cr_OR;
                    NavSol.CommonUI.CFL.AddCon(pForm, sID, relationship, "BPLId", SAPbouiCOM.BoConditionOperation.co_EQUAL, pBranches[i].ToString(), bracketOpenNum, bracketCloseNum);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
               GC.Collect();
            }
        }

        public static void UpdateCon(SAPbouiCOM.Form oForm, string sID, SAPbouiCOM.BoConditionRelationship Rel, string sDBField, SAPbouiCOM.BoConditionOperation Op, string sValue)
        {
            // ************************************************* INCOMPLETE ************************************
            // routine to update conditions to a CFL
            // this is the same as a where clause


            SAPbouiCOM.Conditions oCons = null;
            SAPbouiCOM.Condition oCon = null;
            SAPbouiCOM.ChooseFromList oCFL = null;

            try
            {
                oCFL = oForm.ChooseFromLists.Item(sID);

                // Reset the conditions
                if (Rel == SAPbouiCOM.BoConditionRelationship.cr_NONE)
                {
                    oCFL.SetConditions(oCons);
                }

                oCons = oCFL.GetConditions();

                // If there are already user conditions.
                if (oCons.Count > 0)
                {
                    oCons.Item(oCons.Count - 1).Relationship = Rel;
                }

                // Add Conditions
                // if there is no DB field then do not set any conditions
                if (!string.IsNullOrEmpty(sDBField))
                {
                    oCon = oCons.Add();

                    oCon.Alias = sDBField;
                    oCon.Operation = Op;            //SAPbouiCOM.BoConditionOperation.co_EQUAL
                    oCon.CondVal = sValue;
                    //oCon.CompareFields = true;  // used to compare to another filed in the database. use ComparedAlias
                    oCFL.SetConditions(oCons);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oCFL);
                NSC_DI.UTIL.Misc.KillObject(oCons);
                NSC_DI.UTIL.Misc.KillObject(oCon);
                GC.Collect();
            }
        }

        public static void SetVal(SAPbouiCOM.Form pForm, SAPbouiCOM.ChooseFromListEvent pVal, dynamic pReturnField = null)
        {
            // routine to set the chosen value from a CFL

            SAPbouiCOM.EditText oEdit = null;

            try
            {
                // get the value
                var sVal = GetVal(pForm, pVal, pReturnField);

                // set the value
                if (pForm.Items.Item(pVal.ItemUID).Type == SAPbouiCOM.BoFormItemTypes.it_EXTEDIT | pForm.Items.Item(pVal.ItemUID).Type == SAPbouiCOM.BoFormItemTypes.it_EDIT)
                {
                    oEdit = pForm.Items.Item(pVal.ItemUID).Specific;
                    pForm.DataSources.UserDataSources.Item(oEdit.DataBind.Alias).ValueEx = sVal;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oEdit);
                GC.Collect();
            }
        }

        public static string GetVal(SAPbouiCOM.Form oForm, SAPbouiCOM.ChooseFromListEvent pVal, dynamic pReturnField = null)
        {
            // routine to get the chosen value from a CFL.
            // if a specific CFL field is specifiec, use it, otherwise get the first value.

            SAPbouiCOM.ChooseFromListEvent  oCFLEvento  = null;
            SAPbouiCOM.DataTable            oDataTable  = null;
            SAPbouiCOM.ChooseFromList       oCFL        = null;

            string sVal = null;

            try
            {
                oCFLEvento  = ((SAPbouiCOM.ChooseFromListEvent)(pVal));
                oCFL        = oForm.ChooseFromLists.Item(oCFLEvento.ChooseFromListUID);
                oDataTable  = oCFLEvento.SelectedObjects;

                if (oCFLEvento.SelectedObjects == null) return "";    // this means that the user clicked "Cancel"
                sVal = System.Convert.ToString(oDataTable.GetValue(pReturnField ?? 0, 0));
                //try
                //{
                    

                //}
                //catch { }
                
                return sVal;

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oCFLEvento = null;
                NSC_DI.UTIL.Misc.KillObject(oCFL);
                NSC_DI.UTIL.Misc.KillObject(oDataTable);
                GC.Collect();
            }
        }

        public static string[] GetValS(SAPbouiCOM.Form oForm, SAPbouiCOM.ItemEvent pVal)
        {
            // routine to get All chosen values from a CFL

            SAPbouiCOM.IChooseFromListEvent oCFLEvento = null;
            SAPbouiCOM.DataTable oDataTable = null;
            SAPbouiCOM.ChooseFromList oCFL = null;

            try
            {
                oCFLEvento = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
                oCFL = oForm.ChooseFromLists.Item(oCFLEvento.ChooseFromListUID);
                oDataTable = oCFLEvento.SelectedObjects;

                if (oCFLEvento.SelectedObjects == null) return null;    // this means that the user clicked "Cancel"

                int cnt = oDataTable.Rows.Count;
                string[] sVal = new string[cnt];
                for (int i = 0; i < cnt; i++)
                {
                    sVal[i] = System.Convert.ToString(oDataTable.GetValue(0, i));
                }
                return sVal;

            }
            catch (Exception ex)
            {
                NSC_DI.UTIL.Message.Format(ex);
                return null;
            }
            finally
            {
                oCFLEvento = null;
                //System.Runtime.InteropServices.Marshal.ReleaseComObject(oCFLEvento) ' this causes pVal to be NULL, even if pVal is ByVal 
                //System.Runtime.InteropServices.Marshal.ReleaseComObject(oDataTable);
                NSC_DI.UTIL.Misc.KillObject(oCFL);
                GC.Collect();
            }
        }

		public static void SetBPFieldsDBDS(SAPbouiCOM.Form oForm, SAPbouiCOM.ItemEvent pVal, string pDBTable, string pCoode, string pName)
		{
			// get the chosen value Card Code from a CFL and populate the Code and Description fields using the database datasource

			SAPbouiCOM.IChooseFromListEvent oCFLEvento = null;
			SAPbouiCOM.DataTable oDataTable = null;
			SAPbouiCOM.ChooseFromList oCFL = null;

			SAPbouiCOM.DBDataSource oDBDatasource = null;

			string sVal = null;

			try
			{
				oCFLEvento = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
				oCFL = oForm.ChooseFromLists.Item(oCFLEvento.ChooseFromListUID);
				oDataTable = oCFLEvento.SelectedObjects;

				if (oCFLEvento.SelectedObjects == null) return;    // this means that the user clicked "Cancel"

				sVal = System.Convert.ToString(oDataTable.GetValue(0, 0));

				oDBDatasource = Forms.FindDBDataSource(oForm, pDBTable);

				if (string.IsNullOrEmpty(pCoode) == false) oDBDatasource.SetValue(pCoode, 0, sVal);

				if (string.IsNullOrEmpty(pName) == false)
				{
					sVal = NSC_DI.SAP.BusinessPartners.GetField<string>(sVal, "CardName");
					oDBDatasource.SetValue(pName, 0, sVal);
				}

			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				oCFLEvento = null;

				NSC_DI.UTIL.Misc.KillObject(oCFL);
				NSC_DI.UTIL.Misc.KillObject(oDBDatasource);

				GC.Collect();
			}
		}

		public static void SetItemFieldsDBDS(SAPbouiCOM.Form oForm, SAPbouiCOM.ItemEvent pVal, string pDBTable, string pCoode, string pName)
		{
			// get the chosen value ITEM CODE from a CFL and populate the Code and Description fields using the database datasource

			SAPbouiCOM.IChooseFromListEvent oCFLEvento = null;
			SAPbouiCOM.DataTable oDataTable = null;
			SAPbouiCOM.ChooseFromList oCFL = null;

			SAPbouiCOM.DBDataSource oDBDatasource = null;

			string sVal = null;

			try
			{
				oCFLEvento = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
				oCFL = oForm.ChooseFromLists.Item(oCFLEvento.ChooseFromListUID);
				oDataTable = oCFLEvento.SelectedObjects;

				if (oCFLEvento.SelectedObjects == null) return;    // this means that the user clicked "Cancel"

				sVal = System.Convert.ToString(oDataTable.GetValue(0, 0));

				oDBDatasource = Forms.FindDBDataSource(oForm, pDBTable);

				if (string.IsNullOrEmpty(pCoode) == false) oDBDatasource.SetValue(pCoode, 0, sVal);

				if (string.IsNullOrEmpty(pName) == false)
				{
					sVal = NSC_DI.SAP.Items.GetField<string>(sVal, "ItemName");
					oDBDatasource.SetValue(pName, 0, sVal);
				}

			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				oCFLEvento = null;

				NSC_DI.UTIL.Misc.KillObject(oCFL);
				NSC_DI.UTIL.Misc.KillObject(oDBDatasource);

				GC.Collect();
			}
		}

        public static void SetItemFieldsUDS(SAPbouiCOM.Form pForm, SAPbouiCOM.ChooseFromListEvent pVal, string pCoode, string pName)
        {
            // get the chosen value ITEM CODE from a CFL and populate the Code and Description fields using the user datasource

            SAPbouiCOM.ChooseFromList       oCFL        = null;
            SAPbouiCOM.EditText             oEdit       = null;
            SAPbouiCOM.IChooseFromListEvent oCFLEvento  = null;
            SAPbouiCOM.DataTable            oDataTable  = null;

            string sVal = null;

            try
            {
                oCFLEvento  = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
                oCFL        = pForm.ChooseFromLists.Item(oCFLEvento.ChooseFromListUID);
                oDataTable  = oCFLEvento.SelectedObjects;

                if (oCFLEvento.SelectedObjects == null) return;    // this means that the user clicked "Cancel"


                if (string.IsNullOrEmpty(pCoode) == false)
                {
                    sVal = System.Convert.ToString(oDataTable.GetValue(0, 0));
                    oEdit = pForm.Items.Item(pCoode).Specific;
                    pForm.DataSources.UserDataSources.Item(oEdit.DataBind.Alias).ValueEx = sVal;
                }

                if (string.IsNullOrEmpty(pName) == false)
                {
                    sVal = System.Convert.ToString(oDataTable.GetValue(1, 0));
                    oEdit = pForm.Items.Item(pName).Specific;
                    pForm.DataSources.UserDataSources.Item(oEdit.DataBind.Alias).ValueEx = sVal;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oCFLEvento = null;

                NSC_DI.UTIL.Misc.KillObject(oCFL);
                NSC_DI.UTIL.Misc.KillObject(oEdit);
                NSC_DI.UTIL.Misc.KillObject(oCFLEvento);
                NSC_DI.UTIL.Misc.KillObject(oDataTable);

                GC.Collect();
            }
        }

        public static void SetItemFieldsGUI(SAPbouiCOM.Form oForm, SAPbouiCOM.ChooseFromListEvent pVal, string pCoode, string pName)
		{
			// get the chosen value ITEM CODE from a CFL and populate the Code and Description fields 

			SAPbouiCOM.IChooseFromListEvent oCFLEvento = null;
			SAPbouiCOM.DataTable oDataTable = null;
			SAPbouiCOM.ChooseFromList oCFL = null;

			string sVal = null;

			try
			{
				oCFLEvento = ((SAPbouiCOM.IChooseFromListEvent)(pVal));
				oCFL = oForm.ChooseFromLists.Item(oCFLEvento.ChooseFromListUID);
				oDataTable = oCFLEvento.SelectedObjects;

				if (oCFLEvento.SelectedObjects == null) return;    // this means that the user clicked "Cancel"

				sVal = System.Convert.ToString(oDataTable.GetValue(0, 0));

				if (string.IsNullOrEmpty(pCoode) == false) Forms.SetField(oForm, pCoode, sVal);

				if (string.IsNullOrEmpty(pName) == false)
				{
					sVal = NSC_DI.SAP.Items.GetField<string>(sVal, pName);
					Forms.SetField(oForm, pName, sVal);
				}

			}
			catch (Exception ex)
			{
                NSC_DI.UTIL.Misc.KillObject(oCFL);
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
			finally
			{
				GC.Collect();
			}
		}

        public static void SetFieldsWH(SAPbouiCOM.Form pForm, SAPbouiCOM.ChooseFromListEvent pVal, string pCodeFld =  null, string pNameFld = null)
        {
            // get the chosen value WH CODE from a CFL and populate the Code and Description fields 

            if (pVal.SelectedObjects == null) return;   // this means that the user clicked "Cancel"

            SAPbouiCOM.Item     oItem = null;
            SAPbouiCOM.EditText oEdit = null;

            var test = pVal;
            try
            {
                if (!string.IsNullOrEmpty(pCodeFld?.Trim()))// == false)// Whitespace-Change
                   
                {
                    oItem = pForm.Items.Item(pCodeFld);
                    switch (oItem.Type)
                    {
                        case SAPbouiCOM.BoFormItemTypes.it_EDIT:
                        case SAPbouiCOM.BoFormItemTypes.it_EXTEDIT:
                            oEdit = oItem.Specific;
                            pForm.DataSources.UserDataSources.Item(oEdit.DataBind.Alias).ValueEx = GetVal(pForm, pVal, "WhsCode");
                            break;

                        case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
                            CommonUI.ComboBox.DeleteVals(pForm, pCodeFld);
                            CommonUI.ComboBox.AddVal(pForm, pCodeFld, GetVal(pForm, pVal, 0), GetVal(pForm, pVal, "WhsCode"));
                            break;

                        case SAPbouiCOM.BoFormItemTypes.it_GRID:
                            break;

                        case SAPbouiCOM.BoFormItemTypes.it_MATRIX:
                            break;

                        default:
                            return;
                    }
                }

                if (!NSC_DI.UTIL.Strings.Empty(pNameFld)) //(NSC_DI.UTIL.Strings.Empty(pNameFld) == false)// Whitespace-Change
                {
                    oItem = pForm.Items.Item(pNameFld);
                    switch (oItem.Type)
                    {
                        case SAPbouiCOM.BoFormItemTypes.it_EDIT:
                        case SAPbouiCOM.BoFormItemTypes.it_EXTEDIT:
                            oEdit = oItem.Specific;
                            pForm.DataSources.UserDataSources.Item(oEdit.DataBind.Alias).ValueEx = GetVal(pForm, pVal, "WhsName");
                            break;

                        case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
                            CommonUI.ComboBox.AddVal(pForm, pNameFld, GetVal(pForm, pVal, 0), GetVal(pForm, pVal, "WhsName"));
                            break;

                        case SAPbouiCOM.BoFormItemTypes.it_GRID:
                            break;

                        case SAPbouiCOM.BoFormItemTypes.it_MATRIX:
                            break;

                        default:
                            return;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oItem);
                NSC_DI.UTIL.Misc.KillObject(oEdit);
                GC.Collect();
            }
        }

        [Obsolete("Don't use", true)]
        private static void CFL_Notes()
        {
            //        SAPbouiCOM.BoLinkedObject   <----- use this one. 

            //SDK help says that this is the master list for both linked objects and Choose From lists

            //        SAPbobsCOM.BoObjectTypes()


            // USE-----------------------
            // ----------------------
            //' Create CFL
            //' ----------------------
            //' Code Field
            //oItm = oLIBUI.oAddItem(ref oForm, "SAItemCodF", "", "", "", SAPbouiCOM.BoFormItemTypes.it_EDIT, iTop, iLeft, -1, 80, 1, 1, True, True, BubbleEvent)
            //' Code Label
            //oItm = oLIBUI.oAddItem(ref oForm, "SAItemCodL", "Item No.", "SAItemCodF", "", SAPbouiCOM.BoFormItemTypes.it_STATIC, iTop, iLeft, -1, 50, 1, 1, True, True, BubbleEvent)
            //' Group Field
            //oForm.DataSources.UserDataSources.Add("SADSCFLIG", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 254)
            //oItm = oLIBUI.oAddItem(ref oForm, "SAItemGrpF", "", "", "SADSCFLIG", SAPbouiCOM.BoFormItemTypes.it_EDIT, iTop, iLeft, -1, iLabelLen, 1, 1, True, True, BubbleEvent)
            //Call oLIBUI.CFL_Create(ref oForm, "SAItemGrpCFL", SAPbouiCOM.BoLinkedObject.lf_ItemGroups, BubbleEvent)
            //oEtx = oItm.Specific
            //oEtx.ChooseFromListUID = "SAItemGrpCFL"
            //oExt.ChooseFromListAlias = "ItemCode";     <<<<<<<<<<<<<<<<<  this allows for the partial entry  >>>>>>>>>>>>>>>>>>>>>>>>>   **************************   !!!!!!!!!!!!!!!!
            //Call oLIBUI.CFL_AddCon(ref oForm, "SAItemGrpCFL", SAPbouiCOM.BoConditionRelationship.cr_NONE, "", SAPbouiCOM.BoConditionOperation.co_EQUAL, "", BubbleEvent)
            //' Group Label
            //oItm = oLIBUI.oAddItem(ref oForm, "SAItemGrpL", "Item Group", "SAItemGrpF", "", SAPbouiCOM.BoFormItemTypes.it_STATIC, iTop, iLeft, -1, 60, 1, 1, True, True, BubbleEvent)



            // use for a grid------------

            //pForm.DataSources.DataTables.Add("ds" + cGrid1);

            //SQL = "SELECT U_ItemCode AS [Item], U_ItemName AS [Description], U_Quantity AS [Quantity]"
            //     + "  FROM " + Global.scSampleTable
            //     + " WHERE DocEntry = '" + pForm.Items.Item("5").Specific.Value + "'"
            //     + " ORDER BY VisOrder";

            //SAPbouiCOM.Grid oGrd = pForm.Items.Item(cGrid1).Specific;
            //oGrd.DataTable.ExecuteQuery(SQL);

            //LIBUI.Grid.SetColumnLinkedObject(ref pForm, oGrd, 0, "4", ref iErr);                   V --- this ("ItemCode") had to be here ???? no. either "ItemCode" or ""
            //LIBUI.Grid.SetColumnChooseFromList(ref pForm, ref oGrd, 0, "cfl" + cGrid1 + "Item", "ItemCode", ref iErr);



            //lf_None				-1	No target object.
            //lf_UserDefinedObject		0	User-defined object.
            //lf_GLAccounts			    1	G/L account object.
            //lf_BusinessPartner		2	Business Partner object.
            //lf_Items			        4	Item object.
            //lf_SalesEmployee		    53	Sales employee object.
            //lf_TransactionTemplates	55	Transaction template.
            //lf_JournalPosting		    30	Journal Posting object.
            //lf_LoadingFactors		    62	Loading Factor object.
            //lf_RecurringTransactions	34	Recurring Transaction object.
            //lf_ProductTree			66	Product Tree object.
            //lf_CheckForPayment		57	Check for Payment object.
            //lf_PaymentTermsTypes		40	Payment Terms object. 
            //lf_Deposit			    25	Deposit object.
            //lf_PredatedDeposit		76	Predated Deposit object.
            //lf_Warehouses			    64	Warehouse object.
            //lf_ImportFile			    69	Import File object.
            //lf_BudgetSystem			78	Budget System object.
            //lf_SalesTaxAuthorities    126	Sales Tax Authorities object.
            //lf_SalesTaxCodes		    128	Sales Tax odes object.
            //lf_RunExternalsApplications	86	Run External Application object.
            //lf_DueDates			    71	Due Date objects.
            //lf_UserDefaults			93	User Defaults object.
            //lf_FinancePeriod		    111	Financial Period object. 
            //lf_SalesOpportunity		97	Sales Opportunity object.
            //lf_ConfirmationLevel		120	Confirmation Level object. 
            //lf_ConfirmationTemplates	121	Confirmation Template object.
            //lf_ConfirmationDocumnets	122	Confirmation Document object.
            //lf_Drafts			        112	Draft object.
            //lf_GoodsIssue			    60	Goods Issue object.
            //lf_GoodsReceipt			59	Goods Receipt object.
            //lf_ProjectCodes			63	Project Code object.
            //lf_ContactWithCustAndVend	33	Contact object.
            //lf_JournalVoucher		    28	Journal Voucher object.
            //lf_ProfitCenter			61	ffProfit Center object.
            //lf_VendorPayment		    46	Vendor Checks Forment object.
            //lf_Receipt			        24	Receipt object.
            //lf_Quotation			    23	Quotation object.
            //lf_Order			        17	Order object.
            //lf_DeliveryNotes		    15	Delivery Note object.
            //lf_DeliveryNotesReturns	16	Delivery Note Return object.
            //lf_Invoice			        13	Invoice object.
            //lf_InvoiceCreditMemo		14	Invoice Credit Memo object.
            //lf_PurchaseOrder		    22	Purchase Order object.
            //lf_GoodsReceiptPO		    20	Goods Receipt PO object.
            //lf_GoodsReturns			21	Goods Return object.
            //lf_PurchaseInvoice		    18	Purchase Invoice object.
            //lf_PurchaseInvoiceCreditMemo	19	Purchase Invoice Credit Memo object.
            //lf_CorrectionInvoice		132	Correction Invoice object.
            //lf_StockTransfers		    67	Stock Transfer object.
            //lf_WorkInstructions		68	Work Instructions object.
            //lf_AlertsTemplate		    80	Alerts Template object.
            //lf_SpecialPricesForGroups	85	Special Prices object.
            //lf_CustomerVendorCatalogNumber	73	Customer/Vendor Catalog Number
            //lf_SpecialPrices		      7	Special Prices object.
            //lf_SerialNumbersForItems	94	Serial Numbers for Items object.
            //lf_ItemBatchNumbers	   106	Item Batch Numbers object.
            //lf_UserValidValues		   110	User Valid Values object.
            //lf_UserDisplayCategories	114	User Display Categories object.
            //lf_AddressFormats		    113	Address Format object.
            //lf_Indicator			    138	Indicator object.
            //lf_CashDiscount			133	Cash Discount object.
            //lf_DeliveryTypes		    49	Delivery Type object.
            //lf_VatGroup			    5	VAT Group object.
            //lf_VatIndicator			135	VAT Indicator object.
            //lf_GoodsShipment		    139	Goods Shipment object.
            //lf_ExpensesDefinition		125	Expense Definition object. 
            //lf_CreditCards			    36	Credit Card object.
            //lf_CentralBankIndicator	161	Business Partner Central Bank Indicator object.
            //lf_BPBankAccount		    187	Business Partner Bank Account object.
            //lf_DiscountCodes		    3	Discount Code object.
            //lf_PaymentBlock			159	Block Payment object for vendors and customers.
            //lf_AgentPerson			177	Agent Person object.
            //lf_PeriodIndicator		184	Period Indicator object for document numbering.
            //lf_HolidaysTable		186	Holidays Table object.
            //lf_Employee			171	Employee object.
            //lf_PredefinedText		215	Pre-defined Text object for sales and marketing documents.
            //lf_Territory			200	Territory (geographic location, brand, or item) object.
            //lf_User				12	SAP Business One User object.
            //lf_ProductionOrder		202	Production Order object.
            //lf_BillOfExchange		181	Bill of Exchange object.
            //lf_BillOfExchangeTransaction	182	Bill of Exchange Transaction object.
            //lf_AddressFormat		131	Address Pattern object.
            //lf_AccountSegmentationCode	143	Account Segmentation Code object.
            //lf_FileFormat			183	File Format object.
            //lf_StockRevaluation		162	Stock Revaluation object.
            //lf_PickList			156	Inventory Pick List object.
            //lf_DunningTerms			196	Dunning Term object.
            //lf_ServiceContract		190	Service Contract object.
            //lf_ContractTemplete		170	Contract Template object.
            //lf_InstallBase			176	Install Base object.
            //lf_ServiceCall			191	Service Call object.
            //lf_ServiceCallSolution		189	Service Call Solution object.
            //lf_ItemGroups			52	Item Groups object.
            //lf_PackageType			206	Package Type object.
            //lf_SalesForecast		198	Sales Forecast object.
            //lf_PaymentMethod		147	Payment Method object.
            //lf_WithHoldingTax		178	Withholding Tax object.

            //				203	A/R Down Payment
            //				204	A/P Down Payment
        }
    }
}
