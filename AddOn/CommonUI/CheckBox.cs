﻿using System;

namespace NavSol.CommonUI
{
    public static class CheckBox
    {
        public static string GetValue(SAPbouiCOM.CheckBox pChk)
        {
            try
            {
                return pChk.Checked ? pChk.ValOn : pChk.ValOff;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static string GetValue(SAPbouiCOM.Form pForm, string pChk)
        {
            SAPbouiCOM.CheckBox oChk = null;
            try
            {
                oChk = pForm.Items.Item(pChk).Specific;
                return GetValue(oChk);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oChk);
                GC.Collect();
            }
        }
        public static void SetValue(SAPbouiCOM.CheckBox pChk, string pVal)
        {
            try
            {
                if (string.Equals(pChk.ValOn, pVal, StringComparison.InvariantCultureIgnoreCase))
                {
                    pChk.Checked = true;
                }
                else if (string.Equals(pChk.ValOff, pVal, StringComparison.InvariantCultureIgnoreCase))
                {
                    pChk.Checked = false;
                }
                else
                {
                    throw new Exception("Checked value, " + pVal + ", does not exist as a checked option in checkbox " + pChk.Item.UniqueID);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }
        public static void SetValue(SAPbouiCOM.Form pForm, string pChk, string pVal)
        {
            SAPbouiCOM.CheckBox oChk = null;
            try
            {
                oChk = pForm.Items.Item(pChk).Specific;
                SetValue(oChk, pVal);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oChk);
                GC.Collect();
            }
        }


    }
}
