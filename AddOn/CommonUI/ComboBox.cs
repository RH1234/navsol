﻿using System;
using System.Collections.Generic;
using SAPbobsCOM;
using SAPbouiCOM;
using System.Linq;

namespace NavSol.CommonUI
{
    public static class ComboBox
    {
        public static void LoadComboBoxSQL(Form pForm, string pComboBox, string pSQL, string pSQLColumnForValue = "", string pSQLColumnForDescription = "")
        {
            SAPbouiCOM.ComboBox oComboBox = null;

            try
            {
                oComboBox = pForm.Items.Item(pComboBox).Specific;
                LoadComboBoxSQL(oComboBox, pSQL, pSQLColumnForValue, pSQLColumnForDescription);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oComboBox);
                GC.Collect();
            }
        }

        /// <summary>
        /// Will load a ComboBox with items from a SQL query.
        /// </summary>
        /// <param name="pComboBox">The ComboBox to fill with items.</param>
        /// <param name="pSQL">The SQL query to run.</param>
        /// <param name="pSQLColumnForValue">The SQL column we'll use for the value of the ComboBox items.</param>
        /// <param name="pSQLColumnForDescription">The SQL column we'll use for the description of the ComboBox items.</param>
		public static void LoadComboBoxSQL(SAPbouiCOM.ComboBox pComboBox, string pSQL, string pSQLColumnForValue = "", string pSQLColumnForDescription = "")
        {
            // Prepare an SAP RecordSet
            SAPbobsCOM.Recordset oRecordSet = null;

            try
            {
                if (pComboBox.ValidValues.Count <= 0)
                {
                    // Add an empty item as the first item in the ComboBox
                    pComboBox.ValidValues.Add("", "");
                }
                // Prepare an SAP Business One RecordSet
                oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset) as Recordset;

                // Grab data from SQL
                oRecordSet.DoQuery(pSQL);

                // Move to the first record in the SQL record set
                oRecordSet.MoveFirst();

                // While we are still navigating through records
                while (!oRecordSet.EoF)
                {
                    // Attempt to add the item to the ComboBox

                    dynamic val, dsc;

                    try
                    {
                        if (pSQLColumnForValue == "") val = 0; else val = pSQLColumnForValue;
                        if (pSQLColumnForDescription == "") dsc = 1; else dsc = pSQLColumnForDescription;
                        pComboBox.ValidValues.Add(oRecordSet.Fields.Item(val).Value, oRecordSet.Fields.Item(dsc).Value);
                    }
                    // Item already exists within the ComboBox
                    catch
                    {

                    }

                    // Move to the next database record
                    oRecordSet.MoveNext();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                // Run GarbageCollection
                NSC_DI.UTIL.Misc.KillObject(oRecordSet);
                GC.Collect();
            }
        }

        /// <see cref="CommonUI.Forms.GetField{T}(Form, string, bool)"/>
        [Obsolete("Use CommonUI.Forms.GetField")]
		public static string GetComboBoxValueOrDescription(ref Form pForm, string pComboBoxUID, string pComboBoxItemDescription = null, string pComboBoxItemValue = null)
        {
            // Grab the ComboBox from the SAP Business One form.
            SAPbouiCOM.ComboBox oComboBox = pForm.Items.Item(pComboBoxUID).Specific as SAPbouiCOM.ComboBox;

            // For each item within the ComboBox
            for (int i = 0; i <= oComboBox.ValidValues.Count - 1; i++)
            {
                // Grab the ComboBox item's description
                string oItemDescription = oComboBox.ValidValues.Item(i).Description.ToString();

                // Grab the ComboBox item's value
                string oItemValue = oComboBox.ValidValues.Item(i).Value.ToString();

                // If the ComboBox's values match
                if (pComboBoxItemValue != null && oItemValue == pComboBoxItemValue)
                {
                    return oItemDescription;
                }
                // If the ComboBox's descriptions match
                else if (pComboBoxItemDescription != null && oItemDescription == pComboBoxItemDescription)
                {
                    return oItemValue;
                }
            }

            // No matches were found          
            return null;
        }

        public static void AddValidValues(ref SAPbobsCOM.Company oCompany, ref SAPbouiCOM.Form pForm, string pUIField, string pDITable, string pDIField)
        {
            // sub to load the Valid values for a combobox from the values created in SAP.

            SAPbobsCOM.Recordset oRS = default(SAPbobsCOM.Recordset);
            string SQL = null;

            try
            {
                oRS = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                SQL = "SELECT FieldID FROM CUFD WHERE TableID = '@" + pDITable + "' AND AliasID = '" + pDIField + "'";
                oRS.DoQuery(SQL);
                if (oRS.RecordCount < 1)
                    return;

                SQL = "SELECT FldValue, Descr FROM UFD1 WHERE TableID = '@" + pDITable + "' AND FieldID = " + oRS.Fields.Item(0).Value.ToString();
                oRS.DoQuery(SQL);
                if (oRS.RecordCount < 1)
                    return;               

                for (int i = 1; i <= oRS.RecordCount; i++)
                {
                    if (i > 1)
                        oRS.MoveNext();
                    //pForm.Items.Item(pUIField).Specific.ValidValues.Add(oRS.Fields.Item(0).Value, oRS.Fields.Item(1).Value);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void ComboBoxAddVals(SAPbouiCOM.Form pForm, string sComboBox, string sSQL, string pEmptyVal)
        {
            try
            {

                // routine to add values to a combo box based on a query.

                // pEmptyVal - if there is a value, then use it and " " as the description.


                ComboBoxAddVals(pForm, sComboBox, "", sSQL, pEmptyVal);
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void ComboBoxAdd_OSLP(SAPbouiCOM.Form pForm, string sComboBox)
        {

            // routine to add values to a combo box from OSLP - Sales Person

            // pEmptyVal - if there is a value, then use it and " " as the description.

            try
            {
                string SQL = "SELECT SlpCode, SlpName FROM OSLP";

                ComboBoxAddVals(pForm, sComboBox, "", SQL, "");
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void ComboBoxAdd_OCTG(SAPbouiCOM.Form pForm, string sComboBox)
        {

            // routine to add values to a combo box from OCTG. - Payment Terms

            // pEmptyVal - if there is a value, then use it and " " as the description.

            try
            {
                string SQL = "SELECT GroupNum, PymntGroup FROM OCTG";

                ComboBoxAddVals(pForm, sComboBox, "", SQL, "");
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void ComboBoxAdd_OCRN(SAPbouiCOM.Form pForm, string sComboBox)
        {
            // routine to add values to a combo box from OCTG. - Payment Terms

            // pEmptyVal - if there is a value, then use it and " " as the description.

            try
            {
                string SQL = "SELECT CurrCode, CurrName FROM OCRN";

                ComboBoxAddVals(pForm, sComboBox, "", SQL, "");
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void ComboBoxAdd_OCRY(SAPbouiCOM.Form pForm, string sComboBox)
        {
            // routine to add values to a combo box from OCRY. - Countries

            // pEmptyVal - if there is a value, then use it and " " as the description.



            try
            {
                string SQL = "SELECT Code, Name FROM OCRY";

                ComboBoxAddVals(pForm, sComboBox, "", SQL, "");
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void ComboBoxAdd_UFD1(SAPbouiCOM.Form pForm, string pObject, string pTable, string pSQLField, dynamic pColName = null, string pEmptyVal = "")// add another arg string pCol = "", string pDfltVal = "", 
        {
            // routine to add values to a combo box from UFD1. - Used Defined Values
            // pEmptyVal - if there is a value, then use it and " " as the description.
            try
            {
                string SQL = "SELECT UFD1.FldValue, UFD1.Descr FROM CUFD INNER JOIN UFD1 ON CUFD.TableID = UFD1.TableID AND CUFD.FieldID = UFD1.FieldID "
                               + " WHERE CUFD.TableID = N'" + NSC_DI.UTIL.Strings.RemoveBrackets(pTable) + "' AND CUFD.AliasID = N'" + pSQLField + "'";

                ComboBoxAddVals(pForm, pObject, pColName, SQL, pEmptyVal);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void ComboBoxAdd_UDT_CN(SAPbouiCOM.Form pForm, string sComboBox, string pUDT, string pFilter = "")
        {
            // routine to add values to a combo box from a User Defined Table - Code and Name

            // pEmptyVal - if there is a value, then use it and " " as the description.
            try
            {
                if (pUDT.Length < 3) return;
                if (pUDT.Substring(1, 1) != "@") pUDT = "@" + pUDT;
                if (pUDT.Substring(0, 1) != "[") pUDT = "[" + pUDT + "]";

                string SQL = "SELECT Code, Name FROM " + pUDT;
                if (pFilter != "") SQL += " WHERE " + pFilter;

                ComboBoxAddVals(pForm, sComboBox, "", SQL, "");
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void ComboBoxAdd_UDT_CN(SAPbouiCOM.Form pForm, string pMatrix, dynamic pColumn, string pUDT)
        {
            // routine to add values to a combo box from a User Defined Table - Code and Name to a matrix column
            SAPbobsCOM.Recordset oRS  = null;
            SAPbouiCOM.Matrix    oMat = null;

            // pEmptyVal - if there is a value, then use it and " " as the description.
            try
            {
                if (pUDT.Length < 3) return;
                if (pUDT.Substring(1, 1) != "@") pUDT = "@" + pUDT;
                if (pUDT.Substring(0, 1) != "[") pUDT = "[" + pUDT + "]";

                string SQL = "SELECT Code, Name FROM " + pUDT;
                oRS = Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRS.DoQuery(SQL);
                if (oRS.RecordCount < 1) return;

                oMat = pForm.Items.Item(pMatrix).Specific;

                for (int i = 1; i <= oRS.RecordCount; i++)
                {
                    if (i > 1) oRS.MoveNext();
                    oMat.Columns.Item(pColumn).ValidValues.Add(oRS.Fields.Item(0).Value, oRS.Fields.Item(1).Value);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oRS);
                NSC_DI.UTIL.Misc.KillObject(oMat);
                GC.Collect();
            }
        }

        public static void ComboBoxAdd_UDT_NO(SAPbouiCOM.Form pForm, string sComboBox, string sUDT)
        {
            // routine to add values to a combo box from a User Defined Table - Name Only

            // pEmptyVal - if there is a value, then use it and " " as the description.

            string SQL = "SELECT Name, Name FROM " + sUDT;

            ComboBoxAddVals(pForm, sComboBox, "", SQL, "");
        }

        public static void ComboBoxAdd_DocType(SAPbouiCOM.Form pForm, string pField, string pCol, string pEmptyVal)
        {
            // routine to add values to a combo box - Document Types

            //  bodt_Invoice                13  Sales Invoice 
            //  bodt_CreditNote             14  Sales Credit Memo  
            //  bodt_DeliveryNote           15  Sales Delivery Note  
            //  bodt_Return                 16  Sales Returns  
            //  bodt_Order                  17  Sales Order  
            //  bodt_PurchaseInvoice        18  Purchase Invoice  
            //  bodt_PurchaseCreditNote     19  Purchase Credit Memo  
            //  bodt_PurchaseDeliveryNote   20  Goods Receipt PO  
            //  bodt_PurchaseReturn         21  Purchase Goods Returns  
            //  bodt_PurchaseOrder          22  Purchase Order  
            //  bodt_Quotation              23  Sales Quotation  
            //  bodt_CorrectionAPInvoice    163 A/P correction Invoice 
            //  bodt_CorrectionARInvoice    165 A/R correction Invoice 

            try
            {
                string SQL = "SELECT 13, 'Sales Invoice'"
                    + "UNION 14, 'Sales Credit Memo'"
                    + "UNION 15, 'Sales Delivery Note'"
                    + "UNION 16, 'Sales Returns'"
                    + "UNION 17, 'Sales Order'"
                    + "UNION 18, 'Purchase Invoice'"
                    + "UNION 19, 'Purchase Credit Memo'"
                    + "UNION 20, 'Goods Receipt PO'"
                    + "UNION 21, 'Purchase Goods Returns'"
                    + "UNION 22, 'Purchase Order'"
                    + "UNION 23, 'Sales Quotation'"
                    + "UNION 163, 'A/P Correction Invoice'"
                    + "UNION 165, 'A/R Correction Invoice'";

                ComboBoxAddVals(pForm, pField, pCol, SQL, pEmptyVal);
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        public static void ComboBoxAddVals(SAPbouiCOM.Form oForm, string pField, dynamic pCol, string sSQL, string pEmptyVal)
        {
            // routine to add values to a combo box based on a query.

            // pEmptyVal - if there is a value, then use it and " " as the description.


            // IF THERE IS FATAL ERROR ON THIS STATEMENT         : oGrid.Columns.Item("Territory").Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            // THE SOULUTION IS TO CAST THE RESULT AS CHARACTER  : CAST([@ABY_COMMGROUPS].U_territry AS VARCHAR (20))

            int i1 = 0;
            int iCnt = 0;

            SAPbobsCOM.Recordset oRS = null;
            SAPbouiCOM.Item oItem = null;
            SAPbouiCOM.Matrix oMat = null;
            SAPbouiCOM.Grid oGrd = null;
            SAPbouiCOM.Column oCol = null;
            SAPbouiCOM.ComboBox oCmbBox = default(SAPbouiCOM.ComboBox);
            SAPbouiCOM.ComboBoxColumn oCmbBoxC = default(SAPbouiCOM.ComboBoxColumn);

            try
            {
                oRS = (SAPbobsCOM.Recordset)Globals.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (sSQL.Length > 0)
                {
                    oRS.DoQuery(sSQL);
                    if (oRS.RecordCount < 1) return;
                }

                oItem = oForm.Items.Item(pField);
                oItem.DisplayDesc = true;

                switch (oItem.Type)
                {
                    case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
                        oCmbBox = (SAPbouiCOM.ComboBox)oItem.Specific;

                        if (sSQL.Length == 0)
                        {
                            // just remove the valid values
                            iCnt = oCmbBox.ValidValues.Count;

                            if (iCnt < 1) return;

                            for (i1 = iCnt - 1; i1 >= 0; i1 += -1)
                            {
                                oCmbBox.ValidValues.Remove(i1, SAPbouiCOM.BoSearchKey.psk_Index);
                            }
                            return;
                        }

                        if (string.IsNullOrEmpty(pEmptyVal) == false) oCmbBox.ValidValues.Add(pEmptyVal, " ");

                        for (i1 = 1; i1 <= oRS.RecordCount; i1++)
                        {
                            try
                            {
                                oCmbBox.ValidValues.Add(oRS.Fields.Item(0).Value.ToString(), oRS.Fields.Item(1).Value.ToString());
                            }
                            catch (Exception ex)
                            {
                                //sTmp = ex.Message;  // only to remove compile warning
                                //sTmp = "Entry already exists: " + oRS.Fields.Item(0).Value.ToString() + " - " + oRS.Fields.Item(1).Value.ToString() + Environment.NewLine;
                                //sTmp = sTmp + sSQL + Environment.NewLine + "(ComboBoxAddVals)";
                                //B1WizardBase.B1Connections.theAppl.MessageBox(sTmp, 1, "Ok", "", "");
                            }
                            oRS.MoveNext();
                        }
                        break;

                    case SAPbouiCOM.BoFormItemTypes.it_GRID:
                        oGrd = (SAPbouiCOM.Grid)oForm.Items.Item(pField).Specific;
                        oCmbBoxC = (SAPbouiCOM.ComboBoxColumn)oGrd.Columns.Item(pCol);

                        if (sSQL.Length == 0)
                        {
                            // just remove the valid values
                            iCnt = oCmbBoxC.ValidValues.Count;
                            if (iCnt < 1) return;

                            for (i1 = iCnt - 1; i1 >= 0; i1 += -1)
                            {
                                oCmbBoxC.ValidValues.Remove(i1, SAPbouiCOM.BoSearchKey.psk_Index);
                            }
                            return;
                        }

                        if (string.IsNullOrEmpty(pEmptyVal) == false) oCmbBoxC.ValidValues.Add(pEmptyVal, " ");
                        for (i1 = 1; i1 <= oRS.RecordCount; i1++)
                        {
                            try
                            {
                                oCmbBoxC.ValidValues.Add(oRS.Fields.Item(0).Value.ToString(), oRS.Fields.Item(1).Value.ToString());
                            }
                            catch (Exception ex)
                            {
                                //sTmp = ex.Message;  // only to remove compile warning
                                //sTmp = "Entry already exists: " + oRS.Fields.Item(0).Value.ToString() + " - " + oRS.Fields.Item(1).Value.ToString() + Environment.NewLine;
                                //sTmp = sTmp + sSQL;
                                //B1WizardBase.B1Connections.theAppl.MessageBox(sTmp + Environment.NewLine + "(ComboBoxAddVals)", 1, "Ok", "", "");
                            }
                            oRS.MoveNext();
                        }
                        break;

                    case SAPbouiCOM.BoFormItemTypes.it_MATRIX:
                        oMat = (SAPbouiCOM.Matrix)oForm.Items.Item(pField).Specific;
                        //try
                        //{
                            var testTwo = oMat.Columns;
                            oCol = oMat.Columns.Item(pCol);
                        //}
                        //catch
                        //{                            
                        //    int colIndex;
                        //    int.TryParse(pCol, out colIndex);
                        //    oCol = oMat.Columns.Item(colIndex);
                        //}
                       

                        switch (oCol.Type)
                        {
                            case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
                                //oCmbBox = (SAPbouiCOM.ComboBox)(SAPbouiCOM.Column)oMat.Columns.Item(sCol);

                                while (oCol.ValidValues.Count > 0)
                                {
                                    oCol.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                                }
                                //iCnt = oCol.ValidValues.Count;

                                //if (iCnt < 1) return;

                                //for (i1 = iCnt - 1; i1 >= 0; i1 += -1)
                                //{
                                //    oCol.ValidValues.Remove(i1, SAPbouiCOM.BoSearchKey.psk_Index);
                                //}


                                if (sSQL.Length == 0) return; // just remove the valid values

                                break;

                            default:
                                return;
                        }

                        for (i1 = 1; i1 <= oRS.RecordCount; i1++)
                        {
                            try
                            {
                                oCol.ValidValues.Add(oRS.Fields.Item(0).Value.ToString(), oRS.Fields.Item(1).Value.ToString());
                            }
                            catch (Exception ex)
                            {
                                //sTmp = ex.Message;  // only to remove compile warning
                                //sTmp = "Entry already exists: " + oRS.Fields.Item(0).Value.ToString() + " - " + oRS.Fields.Item(1).Value.ToString() + Environment.NewLine;
                                //sTmp = sTmp + sSQL + Environment.NewLine + "(ComboBoxAddVals)";
                                //B1WizardBase.B1Connections.theAppl.MessageBox(sTmp, 1, "Ok", "", "");
                            }
                            oRS.MoveNext();
                        }
                        break;

                    default:
                        return;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
               
                NSC_DI.UTIL.Misc.KillObject(oRS);
                GC.Collect();
            }
        }

        public static void ComboBoxAddVals(SAPbobsCOM.Company oComp, SAPbouiCOM.ComboBox oComboBox, string sSQL, string pEmptyVal)
        {
            // routine to add values to a combo box based on a query.

            // pEmptyVal - if there is a value, then use it and " " as the description.

            // IF THERE IS FATAL ERROR ON THIS STATEMENT         : oGrid.Columns.Item("Territory").Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            // THE SOULUTION IS TO CAST THE RESULT AS CHARACTER : CAST([@ABY_COMMGROUPS].U_territry AS VARCHAR (20))


            int i1 = 0;
            int iCnt = 0;

            SAPbobsCOM.Recordset oRS = null;

            try
            {
                oRS = (SAPbobsCOM.Recordset)oComp.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (sSQL.Length == 0)
                {
                    // just remove the valid values
                    iCnt = oComboBox.ValidValues.Count;
                    if (iCnt < 1) return;

                    if (string.IsNullOrEmpty(pEmptyVal) == false) oComboBox.ValidValues.Add(pEmptyVal, " ");

                    for (i1 = iCnt - 1; i1 >= 0; i1 += -1)
                    {
                        try
                        {
                            oComboBox.ValidValues.Add(oRS.Fields.Item(0).Value.ToString(), oRS.Fields.Item(1).Value.ToString());
                        }
                        catch (Exception ex)
                        {
                            //sTmp = ex.Message;  // only to remove compile warning
                            //sTmp = "Entry already exists: " + oRS.Fields.Item(0).Value.ToString() + " - " + oRS.Fields.Item(1).Value.ToString() + Environment.NewLine;
                            //sTmp = sTmp + sSQL;
                            //B1WizardBase.B1Connections.theAppl.MessageBox(sTmp + Environment.NewLine + "(ComboBoxAddVals)", 1, "Ok", "", "");
                        }
                    }
                    return;
                }

                oRS.DoQuery(sSQL);
                if (oRS.RecordCount < 1)
                    return;

                for (i1 = 1; i1 <= oRS.RecordCount; i1++)
                {
                    oComboBox.ValidValues.Add(oRS.Fields.Item(0).Value.ToString(), oRS.Fields.Item(1).Value.ToString());
                    oRS.MoveNext();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oRS);
                GC.Collect();
            }

        }

        public static void ComboBoxAddValsS(SAPbouiCOM.Form pForm, string pComboBox, List<Tuple<string, string>> pCodesAndDescs, string pEmptyVal)
        {
            // routine to add values to a combo box 

            // pEmptyVal - if there is a value, then use it and " " as the description.

            //  sample of adding recs to tuple            
            //List<Tuple<string, string>> list = new List<Tuple<string, string>>();
            //list.Add(new Tuple<string, string>("0", "Other"));
            //list.Add(new Tuple<string, string>("DEST", "Destroy"));
            //list.Add(new Tuple<string, string>("SALE", "Sale"));
            //list.Add(new Tuple<string, string>("SICK", "Sick"));


            SAPbouiCOM.ComboBox oCmbBox = default(SAPbouiCOM.ComboBox);

            try
            {
                var fmtCD = pCodesAndDescs.Select(n => $"'{n.Item1.Trim()}','{n.Item2.Trim()}'");

                var SQL = "SELECT " + string.Join(" UNION SELECT ", fmtCD);

                ComboBoxAddVals(pForm, pComboBox, SQL, pEmptyVal);

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (oCmbBox != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oCmbBox);
                GC.Collect();
            }

        }

        public static void ComboBoxAddValsS(SAPbouiCOM.Form pForm, string pComboBox, IEnumerable<Tuple<string, string>> pCodesAndDescs, string pEmptyVal)
        {
            // routine to add values to a combo box from strings of values seperated by ;

            // pEmptyVal - if there is a value, then use it and " " as the description.

            SAPbouiCOM.ComboBox oCmbBox = default(SAPbouiCOM.ComboBox);

            try
            {
                var fmtCD = pCodesAndDescs.Select(n => $"'{n.Item1.Trim()}','{n.Item2.Trim()}'");

                var SQL = "SELECT " + string.Join(" UNION SELECT ", fmtCD);

                ComboBoxAddVals(pForm, pComboBox, SQL, pEmptyVal);

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (oCmbBox != null) System.Runtime.InteropServices.Marshal.ReleaseComObject(oCmbBox);
                GC.Collect();
            }

        }

        public static void ComboBoxAddValsS(SAPbouiCOM.Form pForm, string pField, string pCol, IEnumerable<Tuple<string, string>> pCodesAndDescs, string pEmptyVal)
        {
            // routine to add values to a combo box from strings of values seperated by ;

            // pEmptyVal - if there is a value, then use it and " " as the description.

            try
            {
                var fmtCD = pCodesAndDescs.Select(n => $"'{n.Item1.Trim()}','{n.Item2.Trim()}'");

                var SQL = "SELECT " + string.Join(" UNION SELECT ", fmtCD);

                ComboBoxAddVals(pForm, pField, pCol, SQL, pEmptyVal);

            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }

        }

        public static void AddVal(SAPbouiCOM.Form pForm, string pField, string pCode, string pDesc, int pRow = -1, dynamic pCol = null)
        {
            // routine to add a single value set to a combo box.

            SAPbouiCOM.Item             oItem    = null;
            SAPbouiCOM.Matrix           oMat     = null;
            SAPbouiCOM.Column           oCol     = null;
            SAPbouiCOM.ComboBox         oCmbBox  = default(SAPbouiCOM.ComboBox);
            SAPbouiCOM.ButtonCombo      oCmbBtn  = default(SAPbouiCOM.ButtonCombo);
            SAPbouiCOM.ComboBoxColumn   oCmbBoxC = default(SAPbouiCOM.ComboBoxColumn);

            try
            {
                oItem = pForm.Items.Item(pField);
                oItem.DisplayDesc = true;

                //DeleteVals(pForm, pField, pRow, pCol);

                switch (oItem.Type)
                {
                    case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
                        oCmbBox = (SAPbouiCOM.ComboBox)oItem.Specific;

                        try
                        {
                            oCmbBox.ValidValues.Add(pCode, pDesc);
                            //oCmbBox.Select(0, BoSearchKey.psk_Index);
                            // CHECK TO SEE ITS BOUND, IF SO USE CURRENT ALIAS TO SET VAL, 
                            // IF NOT CREATE A UDS AND ASSOCIATE IT WITH THE FIELD, SET VAL, DELET UDS
                            if(oCmbBox.DataBind.DataBound)                           
                                pForm.DataSources.UserDataSources.Item(oCmbBox.DataBind.Alias).ValueEx = pCode;                            
                            else
                            {
                                CommonUI.Forms.CreateUserDataSource(pForm, pField, $"{pField}asdfdsafsdsfs".Substring(0,10), BoDataType.dt_SHORT_TEXT, 20);
                                pForm.DataSources.UserDataSources.Item(oCmbBox.DataBind.Alias).ValueEx = pCode;
                                //oCmbBox.DataBind.SetBound(false);
                                //pForm.DataSources.UserDataSources.Item(pField.Substring(0, 10))
                            }
                           
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                        }
                        break;

                    case SAPbouiCOM.BoFormItemTypes.it_BUTTON_COMBO:
                        oCmbBtn = (SAPbouiCOM.ButtonCombo)oItem.Specific;

                        try
                        {
                            oCmbBox.ValidValues.Add(pCode, pDesc);
                            oCmbBox.Select(0, BoSearchKey.psk_Index);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                        }
                        break;

                    case SAPbouiCOM.BoFormItemTypes.it_GRID:
                        oCmbBoxC = (SAPbouiCOM.ComboBoxColumn)oItem.Specific;
                        try
                        {
                            oCmbBoxC.ValidValues.Add(pCode, pDesc);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                        }
                        break;

                    case SAPbouiCOM.BoFormItemTypes.it_MATRIX:
                        oMat = (SAPbouiCOM.Matrix)pForm.Items.Item(pField).Specific;
                        oCol = oMat.Columns.Item(pCol);
                        //switch (oCol.Type)
                        //{
                        //    case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
                        //        //oCmbBox = (SAPbouiCOM.ComboBox)(SAPbouiCOM.Column)oMat.Columns.Item(pCol);

                        //        break;

                        //    default:
                        //        return;
                        //}

                        try
                        {
                            oCol.ValidValues.Add(pCode, pDesc);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(NSC_DI.UTIL.Message.Format(ex));
                            //sTmp = ex.Message;  // only to remove compile warning
                            //sTmp = "Entry already exists: " + oRS.Fields.Item(0).Value.ToString() + " - " + oRS.Fields.Item(1).Value.ToString() + Environment.NewLine;
                            //sTmp = sTmp + sSQL + Environment.NewLine + "(ComboBoxAddVals)";
                            //B1WizardBase.B1Connections.theAppl.MessageBox(sTmp, 1, "Ok", "", "");
                        }
                        break;

                    default:
                        return;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMat);
                NSC_DI.UTIL.Misc.KillObject(oCol);
                NSC_DI.UTIL.Misc.KillObject(oItem);
                NSC_DI.UTIL.Misc.KillObject(oCmbBox);
                NSC_DI.UTIL.Misc.KillObject(oCmbBtn);
                NSC_DI.UTIL.Misc.KillObject(oCmbBoxC);
                GC.Collect();
            }
        }

        public static void ComboBoxSetIndex(SAPbouiCOM.Form pForm, string sComboBox, int pValue)
        {
            // set the comboBox by Index.

            

            SAPbouiCOM.ComboBox oCbx = null;

            try
            {
                if (pForm.Items.Item(sComboBox).Type != SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX) return;

                oCbx = (SAPbouiCOM.ComboBox)pForm.Items.Item(sComboBox).Specific;
                oCbx.Select(pValue, SAPbouiCOM.BoSearchKey.psk_Index);
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oCbx);
                GC.Collect();
            }
        }

        public static void DeleteVals(SAPbouiCOM.Form pForm, string pField, int pRow = -1, dynamic pCol = null)
        {
            // routine to DELETE values from a combo box 

            int i1 = 0;
            int iCnt = 0;

            SAPbouiCOM.Item             oItem    = null;
            SAPbouiCOM.ComboBox         oCmbBox  = default(SAPbouiCOM.ComboBox);
            SAPbouiCOM.ComboBoxColumn   oCmbBoxC = default(SAPbouiCOM.ComboBoxColumn);
            SAPbouiCOM.Matrix           oMat     = null;
            SAPbouiCOM.Column           oCol     = null;
            //SAPbouiCOM.Grid oGrd = null;

            try
            {
                oItem = pForm.Items.Item(pField);

                switch (oItem.Type)
                {
                    case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
                        oCmbBox = (SAPbouiCOM.ComboBox)oItem.Specific;

                        // just remove the valid values
                        iCnt = oCmbBox.ValidValues.Count;
                        if (iCnt < 1)
                            return;

                        for (i1 = iCnt - 1; i1 >= 0; i1 -= 1)
                        {
                            oCmbBox.ValidValues.Remove(i1, SAPbouiCOM.BoSearchKey.psk_Index);
                        }
                        return;

                    case SAPbouiCOM.BoFormItemTypes.it_GRID:
                        oCmbBoxC = (SAPbouiCOM.ComboBoxColumn)oItem.Specific;

                        for (i1 = iCnt - 1; i1 >= 0; i1 -= 1)
                        {
                            oCmbBoxC.ValidValues.Remove(i1, SAPbouiCOM.BoSearchKey.psk_Index);
                        }
                        return;

                    case SAPbouiCOM.BoFormItemTypes.it_MATRIX:
                        oMat = (SAPbouiCOM.Matrix)pForm.Items.Item(pField).Specific;
                        oCol = oMat.Columns.Item(pCol);
                        switch (oCol.Type)
                        {
                            case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
                                //oCmb = oMat.Columns.Item(sCol).Cells.Item(iRow).Specific
                                oCmbBox = (SAPbouiCOM.ComboBox)oMat.GetCellSpecific(pCol, pRow);

                                for (i1 = iCnt - 1; i1 >= 0; i1 -= 1)
                                {
                                    oCmbBox.ValidValues.Remove(i1, SAPbouiCOM.BoSearchKey.psk_Index);
                                }
                                return;

                            default:
                                return;
                        }

                    default:
                        return;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oMat);
                NSC_DI.UTIL.Misc.KillObject(oCol);
                NSC_DI.UTIL.Misc.KillObject(oItem);
                NSC_DI.UTIL.Misc.KillObject(oCmbBox);
                NSC_DI.UTIL.Misc.KillObject(oCmbBoxC);
                GC.Collect();
            }
        }
       
        public static void SetVal(SAPbouiCOM.Form pForm, string pField, dynamic pValue, SAPbouiCOM.BoSearchKey pSearchKey, dynamic pCol = null, int pRow = 0)
        {
            // set combo box value.

            SAPbouiCOM.ComboBox oCB = null;            
            SAPbouiCOM.Item oItem = null;
            SAPbouiCOM.Matrix oMat = null;
            SAPbouiCOM.Grid oGrd = null;
            SAPbouiCOM.Column oCol = null;
            //SAPbouiCOM.ComboBoxColumn oColCB = null;
            
            try
            {
                oItem = pForm.Items.Item(pField);
                oItem.DisplayDesc = true;

                switch (oItem.Type) // 12753 added a switch to handle different types of comboboxes
                {
                    case SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX:
                        // 12753 the old code for this forum.
                        oCB = pForm.Items.Item(pField).Specific;
                        oCB.Select(pValue, pSearchKey);                        
                        break;
                    case SAPbouiCOM.BoFormItemTypes.it_GRID:
                        // 12753 new code for grids                        
                        oGrd = (SAPbouiCOM.Grid)pForm.Items.Item(pField).Specific;
                        oGrd.DataTable.SetValue(pCol, pRow, pValue);
                        break;
                    case SAPbouiCOM.BoFormItemTypes.it_MATRIX:
                        // 12753 new code for matrix --- NOT TESTED YET
                        oMat = (SAPbouiCOM.Matrix)pForm.Items.Item(pField).Specific;
                        oCol = oMat.Columns.Item(pCol);                        
                        
                        for (int i = 1; i <= oMat.RowCount; i++)
                        {
                           oCB= (SAPbouiCOM.ComboBox)oMat.Columns.Item(pCol).Cells.Item(i).Specific;
                           oCB.Select(pValue.ToString(), pSearchKey);
                        }
                        
                        //oCB.Select(pValue, pSearchKey);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oCB);
                NSC_DI.UTIL.Misc.KillObject(oItem);
                NSC_DI.UTIL.Misc.KillObject(oMat);
                NSC_DI.UTIL.Misc.KillObject(oGrd);
                GC.Collect();
            }
        }
    }
}
