#define Win32
//using Microsoft.VisualBasic;
using System.Reflection;
using System.Runtime.InteropServices;

//using Microsoft.VisualBasic.Compatibility;


// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly


// TODO: Review the values of the assembly attributes


[assembly: AssemblyTitle("Viridian Sciences")]
[assembly: AssemblyDescription("Viridian Sciences for AgriBusiness")]
[assembly: AssemblyCompany("Akerna")]
[assembly: AssemblyProduct("Viridian")]
[assembly: AssemblyCopyright("Viridian Sciences 2021")]
[assembly:AssemblyTrademark("")]
[assembly:AssemblyCulture("")]

// Version information for an assembly consists of the following four values:

//	Major version
//	Minor Version
//	Revision
//	Build Number

// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below



[assembly: AssemblyVersion("420.04.55.10")]


[assembly: AssemblyFileVersion("420.04.55.10")]


[assembly: ComVisible(false)]
