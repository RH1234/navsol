﻿using System;

namespace NavSol.VERSCI
{
	public class Forms
	{
		public enum FormControlTypes { PictureBox, Folder, EditText, Matrix, StaticText, Button, ComboBox, ChooseFromList, CheckBox, ButtonCombo }

		/// <summary>
		/// Returns an SAP UI object based off of a provided type, and item unique identifier from the active form.
		/// </summary>
		/// <param name="ItemType"></param>
		/// <param name="ItemUID"></param>
		/// <returns></returns>
		public dynamic GetControlFromForm(FormControlTypes ItemType, string ItemUID, SAPbouiCOM.Form FormToSearchIn)
		{
			try
			{
				object returnObject = null;

				switch (ItemType)
				{
					case FormControlTypes.PictureBox:
						returnObject = (SAPbouiCOM.PictureBox)FormToSearchIn.Items.Item(ItemUID).Specific;
						break;

					case FormControlTypes.Folder:
						returnObject = (SAPbouiCOM.Folder)FormToSearchIn.Items.Item(ItemUID).Specific;
						break;

					case FormControlTypes.EditText:
						returnObject = (SAPbouiCOM.EditText)FormToSearchIn.Items.Item(ItemUID).Specific;
						break;

					case FormControlTypes.Matrix:
						returnObject = (SAPbouiCOM.Matrix)FormToSearchIn.Items.Item(ItemUID).Specific;
						break;

					case FormControlTypes.StaticText:
						returnObject = (SAPbouiCOM.StaticText)FormToSearchIn.Items.Item(ItemUID).Specific;
						break;

					case FormControlTypes.Button:
						returnObject = (SAPbouiCOM.Button)FormToSearchIn.Items.Item(ItemUID).Specific;
						break;

					case FormControlTypes.ComboBox:
						returnObject = (SAPbouiCOM.ComboBox)FormToSearchIn.Items.Item(ItemUID).Specific;
						break;

					case FormControlTypes.ChooseFromList:
						returnObject = (SAPbouiCOM.ChooseFromList)FormToSearchIn.Items.Item(ItemUID).Specific;
						break;

					case FormControlTypes.CheckBox:
						returnObject = (SAPbouiCOM.CheckBox)FormToSearchIn.Items.Item(ItemUID).Specific;
						break;

					case FormControlTypes.ButtonCombo:
						returnObject = (SAPbouiCOM.ButtonCombo)FormToSearchIn.Items.Item(ItemUID).Specific;
						break;
				}

				return returnObject;
			}
			catch (Exception ex)
			{
				throw new Exception(NSC_DI.UTIL.Message.Format(ex));
			}
		}
	}
}
