﻿using System;
using System.Collections.Generic;

namespace NavSol.VERSCI
{

    public static class MatrixExtensionMethods
    {

        // CRreate extension method 
        public static List<string> GetStringsOfSelectedRow(this SAPbouiCOM.Matrix matrix, int columnNumber)
        {
            List<string> list = new List<string>();
            for (int i = 1; i < (matrix.RowCount + 1); i++)
            {
                if (matrix.IsRowSelected(i))
                {
                    // Grab the selected row's Plant ID column
                    try
                    {
                        list.Add(matrix.Columns.Item(columnNumber).Cells.Item(i).Specific.Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        //log.Warn("GetStringsOfSelectedRow() error:", ex);
                    }
                }
            }
            return list;
        }

        public static List<string> GetStringsOfSelectedRow(this SAPbouiCOM.Matrix matrix, string columnTitle)
        {
            List<string> list = new List<string>();
            for (int i = 1; i < (matrix.RowCount + 1); i++)
            {
                if (matrix.IsRowSelected(i))
                {
                    // Grab the selected row's Plant ID column
                    try
                    {
                        for (int ii = 0; ii < matrix.Columns.Count; ii++)
                        {
                            string title = matrix.Columns.Item(ii).Title;
                            if (columnTitle == title)
                            {
                                list.Add(matrix.Columns.Item(ii).Cells.Item(i).Specific.Value.ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //log.Warn("GetStringsOfSelectedRow() error:", ex);
                    }
                }
            }
            return list;
        }


        public static string GetStringOfSelectedRow(this SAPbouiCOM.Matrix matrix, string columnTitle)
        {
            for (int i = 1; i < (matrix.RowCount + 1); i++)
            {
                if (matrix.IsRowSelected(i))
                {
                    // Grab the selected row's Plant ID column
                    try
                    {
                        for (int ii = 0; ii < matrix.Columns.Count; ii++)
                        {
                            string title = matrix.Columns.Item(ii).Title;
                            if (title == columnTitle)
                            {
                                return matrix.Columns.Item(ii).Cells.Item(i).Specific.Value.ToString();
                            }
                        }
                    }
                    catch
                    {

                    }
                }
            }
            return null;
        }

        public static int GetColumnIndex(this SAPbouiCOM.Matrix matrix, string columnTitle)
        {
            try
            {
                for (int i = 0; i < matrix.Columns.Count; i++)
                {
                    string title = matrix.Columns.Item(i).Title;
                    if (title == columnTitle)
                    {
                        return i;
                    }
                }
            }
            catch(Exception e){}

            return -1;
        }

        // extend ViewModel to take string for combobox id and return selected
    }

    public class Matrix
    {
        private string DatabaseTableName { get; set; }

        private string MaxrixUID { get; set; }

        private SAPbouiCOM.Application SBO_Application;
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Matrix oMatrix;
        private SAPbouiCOM.Columns oCols;
        private SAPbouiCOM.Column oCol;
        private SAPbouiCOM.EditText oEdit;

        public Matrix(SAPbouiCOM.Application SAPBusinessOne_Application, SAPbouiCOM.Form FormToUse)
        {
            SBO_Application = SAPBusinessOne_Application;
            oForm = FormToUse;
        }

        public void LoadDatabaseDataIntoMatrix(string DatabaseTableName, string MatrixUID, List<MatrixColumn> ListOfColumns, string SQLQuery = null)
        {
            // Freeze the form UI
            try
            {
                oForm.Freeze(true);

                SAPbouiCOM.Matrix oMatrix = null;
                SAPbouiCOM.Columns oColumns = null;
                SAPbouiCOM.Column oColumn = null;
                SAPbouiCOM.DBDataSource oDBDataSource = null;
                SAPbouiCOM.DataTable oDataTable = null;

                // Finding the Matrix item
                oMatrix = ((SAPbouiCOM.Matrix)(oForm.Items.Item(MatrixUID).Specific));

                // Clear any existing columns within the matrix
                int totalColumnCount = oMatrix.Columns.Count;
                if (totalColumnCount != 0)
                {
                    oMatrix.Clear();

                    for (int i = 0; i < totalColumnCount; i++)
                    {
                        oMatrix.Columns.Remove(0);
                    }
                }

                oColumns = oMatrix.Columns;


                // Add DB data sources for the DB bound columns in the matrix
                if (SQLQuery != null)
                {
                    try
                    {
                        oDataTable = oForm.DataSources.DataTables.Add(DatabaseTableName);
                    }
                    catch { }
                    oForm.DataSources.DataTables.Item(DatabaseTableName).ExecuteQuery(SQLQuery);
                }
                else
                {
                    oDBDataSource = oForm.DataSources.DBDataSources.Add(DatabaseTableName);
                }

                //***********************************
                // Adding column items to the matrix
                //***********************************
                int colID = 0;
                foreach (MatrixColumn oMatrixColumn in ListOfColumns)
                {
                    string columnName = "col_" + colID.ToString();
                    oColumn = oColumns.Add(columnName, oMatrixColumn.ItemType);
                    oColumn.Width = oMatrixColumn.ColumnWidth;
                    oColumn.TitleObject.Caption = oMatrixColumn.Caption;
                    oColumn.Editable = oMatrixColumn.IsEditable;

                    if (oMatrixColumn.HasPhoto)
                    {
                        oColumn.ColumnSetting.DisplayType = SAPbouiCOM.BoColumnDisplayType.cdt_Picture;
                    }

                    oColumn.TitleObject.Sortable = true;

                    if(oMatrixColumn.BindTable != null && oMatrixColumn.BindField != null)
                    {
                        oColumn.DataBind.SetBound(true, oMatrixColumn.BindTable, oMatrixColumn.BindField);
                    }

                    if (oMatrixColumn.ColumnName != null)
                    {
                        if (SQLQuery != null)
                        {
                            oColumn.DataBind.Bind(DatabaseTableName, oMatrixColumn.ColumnName);
                        }
                        else
                        {
                            oColumn.DataBind.SetBound(true, DatabaseTableName, oMatrixColumn.ColumnName);
                        }
                    }
                    else
                    {
                        // Try to create a new user data source for the column
                        try
                        {
                            oForm.DataSources.UserDataSources.Add("UDS_" + colID.ToString(), SAPbouiCOM.BoDataType.dt_QUANTITY);
                        }
                        catch { }
                        oColumn.DataBind.SetBound(true, "", "UDS_" + colID.ToString());
                    }
                    colID++;
                }

                // Get the DBdatasource we base the matrix on
                if (SQLQuery == null)
                {
                    oDBDataSource = oForm.DataSources.DBDataSources.Item(DatabaseTableName);
                    oDBDataSource.Query(null);
                }

                // Ready Matrix to populate data
                oMatrix.Clear();

                // Load data back to 
                oMatrix.LoadFromDataSource();

                oMatrix.AutoResizeColumns();
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oForm.Freeze(false);
            }
        }

        public class MatrixColumn
        {
            public SAPbouiCOM.BoFormItemTypes ItemType { get; set; }

            public string ColumnName { get; set; }

            public string Caption { get; set; }

            public int ColumnWidth { get; set; }

            public bool IsEditable { get; set; }

            public bool HasPhoto { get; set; }

            public string BindTable { get; set; }

            public string BindField { get; set; }
        }

        public void Bindmatrix(string pFormID, string pMxID, string pTblName, string pColumnID, string pColAlias, string pSourceType, string pSourceName)
        {
            oForm = SBO_Application.Forms.Item(pFormID);
            oMatrix = oForm.Items.Item(pMxID).Specific as SAPbouiCOM.Matrix;
            oCols = oMatrix.Columns;
            oCol = oCols.Item(pColumnID);

            try
            {
                if (pSourceType == "DBDataSource")
                {
                    oCol.DataBind.SetBound(true, pTblName, pColAlias);
                }
                else if (pSourceType == "UserDataSource")
                {
                    oCol.DataBind.SetBound(true, "", pSourceName);
                }
            }
            catch (Exception e)
            {
                SBO_Application.MessageBox("Could not Bind Matrix");
                SBO_Application.MessageBox(e.ToString());
            }
        }
        public void loadMatrix(string pFormID, string pMxID, string pSrc)
        {
            oForm = SBO_Application.Forms.Item(pFormID);

            oMatrix = oForm.Items.Item(pMxID).Specific as SAPbouiCOM.Matrix;
            SAPbouiCOM.DBDataSource oDBS = oForm.DataSources.DBDataSources.Item(pSrc);
            oDBS.Query();
            oMatrix.Clear();
            oMatrix.LoadFromDataSource();
        }
        //public string GetCellTxt(SAPbouiCOM.Item Item, string ColUID, int RowIndex, SAPbouiCOM.BoSearchKey ComboBoxSearchKeyType = SAPbouiCOM.BoSearchKey.psk_ByValue)
        //{
        //    string retVal = string.Empty;
        //    if (Item.Specific is SAPbouiCOM.IMatrix)
        //    {

        //        retVal = GetB1ObjectTxt(((SAPbouiCOM.Matrix)Item.Specific).GetCellSpecific(ColUID, RowIndex), ComboBoxSearchKeyType);
        //    }
        //    else if (Item.Specific is SAPbouiCOM.IGrid)
        //    {
        //        retVal = ((SAPbouiCOM.Grid)Item.Specific).DataTable.Columns.Item(ColUID).Cells.Item(RowIndex).Value;
        //    }
        //    return retVal;
        //}
        public List<int> getSelected_MXRows_DocEntry(ref SAPbouiCOM.Matrix pMx)
        {
            List<int> list = new List<int>();

            int intHolder;
            string strHolder = null;
            for (int i = 1; i <= pMx.RowCount; i++)
            {
                if (pMx.IsRowSelected(i))
                {
                    oEdit = pMx.GetCellSpecific("DocEntry", i) as SAPbouiCOM.EditText;
                    strHolder = oEdit.Value;
                    intHolder = Convert.ToInt32(strHolder);
                    list.Add(intHolder);

                }
            }

            return list;
        }
        public Dictionary<int, int> getSelected_MXRows_DocEntry_n_RowNo(ref SAPbouiCOM.Matrix pMx)
        {
            Dictionary<int, int> Dict = new Dictionary<int, int>();

            int intHolder;
            string strHolder = null;
            for (int i = 1; i <= pMx.RowCount; i++)
            {
                if (pMx.IsRowSelected(i))
                {
                    oEdit = pMx.GetCellSpecific("DocEntry", i) as SAPbouiCOM.EditText;
                    strHolder = oEdit.Value;
                    intHolder = Convert.ToInt32(strHolder);
                    Dict.Add(intHolder, i);

                }
            }

            return Dict;
        }
        public int getSelected_MXRow(ref SAPbouiCOM.Matrix pMx)
        {
            int Val = 1;
            try
            {
                while (!(pMx.IsRowSelected(Val)))
                {
                    Val++;
                }
            }
            catch
            {
                Val = -1;
            }


            return Val;
        }
        //public static void SetCellTxt(this SAPbouiCOM.Item Item, string ColID, int RowIndex, string Val, SAPbouiCOM.BoSearchKey cmbSearchKeyType = SAPbouiCOM.BoSearchKey.psk_ByValue, bool SkipNULLs = true)
        //{
        //    if (Item.Specific is SAPbouiCOM.IMatrix)
        //    {
        //        SetB1ObjectTxt((((SAPbouiCOM.Matrix)Item.Specific).GetCellSpecific(ColID, RowIndex)), Val, cmbSearchKeyType, SkipNULLs);
        //    }
        //    else if (Item.Specific is SAPbouiCOM.IGrid)
        //    {
        //        ((SAPbouiCOM.Grid)Item.Specific).DataTable.Columns.Item(ColID).Cells.Item(RowIndex).Value = Val;
        //    }
        //}



        public string GetB1ObjectTxt(dynamic B1Obj, SAPbouiCOM.BoSearchKey cmbSearchKeyType = SAPbouiCOM.BoSearchKey.psk_ByValue)
        {
            object rVal = null;
            try
            {
                if (B1Obj is SAPbouiCOM.ICheckBox)
                {
                    bool chkbState = ((SAPbouiCOM.CheckBox)B1Obj).Checked;
                    if (chkbState)
                        rVal = "Y";
                    else
                        rVal = "N";
                }
                if (B1Obj is SAPbouiCOM.IEditText)
                {
                    if (cmbSearchKeyType == SAPbouiCOM.BoSearchKey.psk_ByValue)
                        rVal = ((SAPbouiCOM.EditText)B1Obj).Value;
                    else if (cmbSearchKeyType == SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        rVal = ((SAPbouiCOM.EditText)B1Obj).String;
                }
                else if (B1Obj is SAPbouiCOM.IStaticText)
                {
                    rVal = ((SAPbouiCOM.StaticText)B1Obj).Caption;
                }
                else if (B1Obj is SAPbouiCOM.IComboBox)
                {
                    if (cmbSearchKeyType == SAPbouiCOM.BoSearchKey.psk_ByValue)
                        rVal = ((SAPbouiCOM.ComboBox)B1Obj).Selected.Value;
                    else
                        rVal = ((SAPbouiCOM.ComboBox)B1Obj).Selected.Description;
                }
                else if (B1Obj is SAPbouiCOM.IButton)
                {
                    rVal = ((SAPbouiCOM.Button)B1Obj).Caption;
                }
                else if (B1Obj is SAPbouiCOM.IStatusBar)
                {
                    rVal = ((SAPbouiCOM.StatusBar)B1Obj).ToString();
                }
                else if (B1Obj is SAPbouiCOM.IFolder)
                {
                    rVal = ((SAPbouiCOM.Folder)B1Obj).Caption;
                }
                else if (B1Obj is SAPbouiCOM.IForm)
                {
                    rVal = ((SAPbouiCOM.Form)B1Obj).Title;
                }
                else if (B1Obj is SAPbouiCOM.IMenuItem)
                {
                    rVal = ((SAPbouiCOM.MenuItem)B1Obj).String;
                }
            }
            catch
            {

            }
            if (rVal == DBNull.Value || rVal == null)
                return null;
            return (string)rVal;
        }

    }


    /// <summary>
    /// Class that assists in Sorting a Matrix.
    /// </summary>
    public class MatrixSortHelper
    {
        private List<ColumnSortOption> _listOfColumnSorts = new List<ColumnSortOption>();

        /// <summary>
        /// Add a sort option to the Matrix Sort options.
        /// </summary>
        /// <param name="SortOption">Object that contains data to appy sort options to a Matrix.</param>
        public void AddColumnSortOption(ColumnSortOption SortOption)
        {
            _listOfColumnSorts.Add(SortOption);
        }

        /// <summary>
        /// Apply Sort Options to the Matrix.
        /// </summary>
        /// <param name="MatrixToSort">Matrix to apply sort options.</param>
        public void ApplySort(SAPbouiCOM.Matrix MatrixToSort)
        {
            // Loop through all the columns of the Matrix and find our specified columns.
            for (int t = 0; t < MatrixToSort.Columns.Count; t++)
            {
                string title = MatrixToSort.Columns.Item(t).Title;
                foreach (ColumnSortOption colSort in _listOfColumnSorts)
                {
                    // If the title(ColumnCaption) matches our Sort object ColumnCaption. We apply our sort.
                    if (title == colSort.ColumnCaption)
                    {
                        SAPbouiCOM.Column oColumn = MatrixToSort.Columns.Item(t);
                        oColumn.TitleObject.Sortable = colSort.EnableSort;
                        oColumn.TitleObject.Sort(sortType: colSort.SortType);
                    }
                }
            }
        }

        public class ColumnSortOption
        {
            public string ColumnCaption;
            public bool EnableSort;
            public SAPbouiCOM.BoGridSortType SortType;

            /// <summary>
            /// Create a new Column Sort Option. 
            /// These are used in MatrixSortHelper to apply various sort features to columns.
            /// </summary>
            /// <param name="ColumnCaption">ColumnCaption of the field to Sort by.</param>
            /// <param name="EnableSort">Enable sorting for user.</param>
            /// <param name="SortType">Enumerable specifying the order of the desired sort.</param>
            public ColumnSortOption(string ColumnCaption, bool EnableSort, SAPbouiCOM.BoGridSortType SortType)
            {
                this.ColumnCaption = ColumnCaption;
                this.EnableSort = EnableSort;
                this.SortType = SortType;
            }
        }
    }

    /// <summary>
    /// Class to Assist in retrieval of columns. 
    /// Columns can be converted to other SAPboui types such as EditText.
    /// </summary>
    public class MatrixColumnCollector
    {
        private Dictionary<string, ColumnCollectorHelper> _listOfColumns = new Dictionary<string, ColumnCollectorHelper>();
        private SAPbouiCOM.Matrix _matrixToSearch;

        /// <summary>
        /// Create a List of Columns to collect.
        /// </summary>
        /// <param name="CaptionName">Caption Name of the Column.</param>
        /// <param name="MatrixToSearch">Matrix to collect columns from.</param>
        public MatrixColumnCollector(List<string> ColumnCaptionNames, SAPbouiCOM.Matrix MatrixToSearch)
        {
            _matrixToSearch = MatrixToSearch;
            CollectColumnList(ColumnCaptionNames: ColumnCaptionNames);
        }

        /// <summary>
        /// Create a Single Item Column Collector. 
        /// You can use the AddColumnToCollect to provide more columns.
        /// </summary>
        /// <param name="CaptionName">Caption Name of the Column.</param>
        /// <param name="MatrixToSearch">Matrix to collect columns from.</param>
        public MatrixColumnCollector(string CaptionName, SAPbouiCOM.Matrix MatrixToSearch)
        {
            _matrixToSearch = MatrixToSearch;
            CollectColumnList(ColumnCaptionNames: new List<string>() { CaptionName });
        }

        /// <summary>
        /// Add in a single CaptionName to the Column Collection helper.
        /// </summary>
        /// <param name="CaptionName">Caption Name of the Column.</param>
        public void AddColumnToCollect(string CaptionName)
        {
            if (_matrixToSearch == null)
            {
                return;
            }

            CollectColumnList(ColumnCaptionNames: new List<string>() { CaptionName });
        }

        /// <summary>
        /// Perform the operation to get the column specified by the Caption Name.
        /// </summary>
        /// <param name="ColumnCaptionNames">List of CaptionNames to collect. This can be a single item.</param>
        private void CollectColumnList(List<string> ColumnCaptionNames)
        {
            // Loop through all the columns of the Matrix and find our specified columns.
            for (int t = 0; t < _matrixToSearch.Columns.Count; t++)
            {
                string title = _matrixToSearch.Columns.Item(t).Title;
                foreach (string colCaption in ColumnCaptionNames)
                {
                    if (title == colCaption)
                    {
                        ColumnCollectorHelper colHelper = new ColumnCollectorHelper();

                        colHelper.Column = _matrixToSearch.Columns.Item(t);
                        colHelper.IndexOfColumn = t;
                        _listOfColumns.Add(title, colHelper);
                    }
                }
            }
        }

        /// <summary>
        /// Get's the Column in the Matrix by CaptionName
        /// </summary>
        /// <param name="CaptionName">Caption Name of the column.</param>
        /// <returns>SAPbouiCOM.Column which can be cast into other types.</returns>
        public SAPbouiCOM.Column GetColumn(string CaptionName)
        {
            if (_listOfColumns.Count > 0)
            {
                if (_listOfColumns.ContainsKey(CaptionName))
                {
                    return _listOfColumns[CaptionName].Column;
                }
            }

            return null;
        }

        /// <summary>
        /// Get the Index location of the Column.
        /// </summary>
        /// <param name="CaptionName">CaptionName of the column.</param>
        /// <returns>Index of the column. If it doens't exist will return -1.</returns>
        public int GetIndexOfColumn(string CaptionName)
        {
            if (_listOfColumns.Count > 0)
            {
                if (_listOfColumns.ContainsKey(CaptionName))
                {
                    return _listOfColumns[CaptionName].IndexOfColumn;
                }
            }

            return -1;
        }

        public class ColumnCollectorHelper
        {
            public SAPbouiCOM.Column Column;
            public int IndexOfColumn = -1;
        }
    }
}
