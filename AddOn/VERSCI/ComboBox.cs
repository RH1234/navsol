﻿using System;
using System.Collections.Generic;

namespace NavSol.VERSCI
{
    public class ComboBox
    {
        #region Private - Fields
        private SAPbouiCOM.Application _SBO_Application;
        private SAPbobsCOM.Company _SBO_Company;
        private SAPbouiCOM.Form _SBO_Form;

        #endregion

        public ComboBox(SAPbouiCOM.Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company, SAPbouiCOM.Form SAPBusinessOne_Form)
        {
            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
            _SBO_Form = SAPBusinessOne_Form;
        }

        /// <summary>
        /// Will load a ComboBox with items from a SQL query.
        /// </summary>
        /// <param name="pComboBox">The ComboBox to fill with items.</param>
        /// <param name="pSQL">The SQL query to run.</param>
        /// <param name="pSQLColumnForValue">The SQL column we'll use for the value of the ComboBox items.</param>
        /// <param name="pSQLColumnForDescription">The SQL column we'll use for the description of the ComboBox items.</param>
        public void LoadComboBoxSQL(SAPbouiCOM.ComboBox pComboBox, string pSQL, string pSQLColumnForValue, string pSQLColumnForDescription)
        {
            // Prepare an SAP RecordSet
            SAPbobsCOM.Recordset oRecordSet = null;

            try
            {
                if(pComboBox.ValidValues.Count<=0)
                { 
                    // Add an empty item as the first item in the ComboBox
                    pComboBox.ValidValues.Add("", "");
                }
                // Prepare an SAP Business One RecordSet
                oRecordSet = _SBO_Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;

                // Grab data from SQL
                oRecordSet.DoQuery(pSQL);

                // Move to the first record in the SQL record set
                oRecordSet.MoveFirst();

                // While we are still navigating through records
                while (!oRecordSet.EoF)
                {
                    // Attempt to add the item to the ComboBox
                    try
                    {
                        pComboBox.ValidValues.Add(oRecordSet.Fields.Item(pSQLColumnForValue).Value.ToString(), oRecordSet.Fields.Item(pSQLColumnForDescription).Value.ToString());
                    }
                    // Item already exists within the ComboBox
                    catch
                    {
                        
                    }

                    // Move to the next database record
                    oRecordSet.MoveNext();
                }
            }
            catch
            {

            }
            finally
            {
                // Run GarbageCollection
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordSet);
                GC.Collect();
            }
        }

        /// <summary>
        /// Attempts to remove all items within a ComboBox.
        /// </summary>
        /// <param name="pComboBox">The ComboBox to remove all items from.</param>
        public void ClearAllItemsInAComboBox(SAPbouiCOM.ComboBox pComboBox)
        {
            try
            {
                // In reverse for loop, recursively navigate through ComboBox items and remove them.
                for (int i = pComboBox.ValidValues.Count - 1; i >= 0; i--)
                {
                    pComboBox.ValidValues.Remove(i, SAPbouiCOM.BoSearchKey.psk_Index);
                }

            }
            // If no items are present within the ComboBox
            catch
            {

            }
        }

        /// <summary>
        /// Will fill a ComboBox with items passed to it.
        /// </summary>
        /// <param name="pFormUID">The SAP Business One form where the ComboBox resides.</param>
        /// <param name="pComboBoxUID">The SAP Business One UID for the ComboBox control.</param>
        /// <param name="pComboBoxItems">A Dictionary of items we will be adding to the ComboBox.  First string is the value, second string is the description.</param>
        public void FillComboBoxWithData(string pFormUID, string pComboBoxUID, Dictionary<string, string> pComboBoxItems)
        {
            // Find the ComboBox on the SAP Business One form.
            SAPbouiCOM.ComboBox oComboBox = _SBO_Form.Items.Item(pComboBoxUID).Specific as SAPbouiCOM.ComboBox;

            // Empty all existing items within the ComboBox
            this.ClearAllItemsInAComboBox(oComboBox);

            // Add an empty item as the first item in the ComboBox
            oComboBox.ValidValues.Add("", "");

            // For each item we are adding into the ComboBox
            foreach (string key in pComboBoxItems.Keys)
            {
                // Add the item to the ComboBox
                oComboBox.ValidValues.Add(Value: key, Description: pComboBoxItems[key].ToString());
            }
        }

        /// <summary>
        /// Will return the description of a ComboBox item if passed the value.  Alternativley, you can pass a value and it will return the description.
        /// </summary>
        /// <param name="pFormUID">The SAP Business One form where the ComboBox resides.</param>
        /// <param name="pComboBoxUID">The SAP Business One UID for the ComboBox control.</param>
        /// <param name="pComboBoxItemDescription">The description you have.  Will return the partnered "value" of the ComboBox item.</param>
        /// <param name="pComboBoxItemValue">The value you have.  Will return the partnered "description" of the ComboBox item.</param>
        /// <returns>Returns the partnered value if a description is passed, or a description if a value was passed.</returns>
        public string GetComboBoxValueOrDescription(string pFormUID, string pComboBoxUID, string pComboBoxItemDescription = null, string pComboBoxItemValue = null)
        {
            // Grab the ComboBox from the SAP Business One form.
            SAPbouiCOM.ComboBox oComboBox = _SBO_Form.Items.Item(pComboBoxUID).Specific as SAPbouiCOM.ComboBox;

            // For each item within the ComboBox
            for (int i = 0; i <= oComboBox.ValidValues.Count - 1; i++)
            {
                // Grab the ComboBox item's description
                string oItemDescription = oComboBox.ValidValues.Item(i).Description.ToString();

                // Grab the ComboBox item's value
                string oItemValue = oComboBox.ValidValues.Item(i).Value.ToString();

                // If the ComboBox's values match
                if (pComboBoxItemValue != null && oItemValue == pComboBoxItemValue)
                {
                    return oItemDescription;
                }
                // If the ComboBox's descriptions match
                else if (pComboBoxItemDescription != null && oItemDescription == pComboBoxItemDescription)
                {
                    return oItemValue;
                }
            }

            // No matches were found          
            return null;
        }
    }
}
