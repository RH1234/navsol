using System;
using System.Diagnostics;
using System.Windows.Forms;
using B1WizardBase;
using SAPbouiCOM;

//0030002C0030002C00530041005000420044005F00440061007400650076002C0050004C006F006D0056004900490056

// CHANGE LOG
// #### DATE    WHO      VERSION         DESCRIPTION
// ---- ------- -------  -------         ----------------------------------------------------------------
// 0000 24feb17 RHH      920.xx.1.1      Initial Development.

namespace NavSol
{
	class AddOnMain
    {
        [STAThread]
        public static void Main()
        {
            try
            {
                string connStr = "";
                B1Connections.ConnectionType cnxType = B1Connections.ConnectionType.MultipleAddOns;

                string addOnIdentifierStr = null;
                if ((Environment.GetCommandLineArgs().Length == 1))
                {
                    connStr = B1Connections.connStr;
                }
                else
                {
                    connStr = Environment.GetCommandLineArgs().GetValue(1).ToString();
                }

	            if (B1Connections.Init(connStr, addOnIdentifierStr, B1Connections.ConnectionType.SSO) != 0)
	            {
		            // CONNECTION FAILED
		            MessageBox.Show("ERROR - Connection failed: " + B1Connections.diCompany.GetLastErrorCode().ToString() + Environment.NewLine + B1Connections.diCompany.GetLastErrorDescription());
		            return;
	            }

	            // CREATE ADD-ON
				AddOn addOn = new AddOn(out bool err);
	            
				if (!err) System.Windows.Forms.Application.Run();
            }
            catch (Exception ex)
            {
                // HANDLE ANY COMException HERE
                MessageBox.Show(ex.Message);
            }
        }
    }

    public partial class AddOn : B1AddOn
    {
	    public AddOn(out bool pErr)
		{
			pErr = false;
            SAPbouiCOM.Form oForm = null;
            try
            {
	            Globals.oApp			= B1Connections.theAppl;
				Globals.oCompany		= Globals.oApp.Company.GetDICompany();
	            Globals.oCompService	= Globals.oCompany.GetCompanyService();
				Globals.SAPVersion		= Globals.oCompany.Version.ToString();
				Globals.UserName		= Globals.oApp.Company.UserName;

                Globals.oSTAThreadDispatcher = System.Windows.Threading.Dispatcher.CurrentDispatcher;

                try
                {
                    ////******************************************************************************************************************************************
                    ////Justin - 5/2/19 - Try to strip out the SAP Logos and Menues. This is for a demo and will be commented out later.  
                    //Globals.oApp.Forms.GetFormByTypeAndCount(0, 0).Items.Item("10000112").Click();
                    //Globals.oApp.Forms.GetFormByTypeAndCount(0, 0).Items.Item("10000112").Visible = false;

                    //Globals.oApp.Menus.Item("257").String = "Help";
                    ////Globals.oApp.Menus.Item("257").Enabled = false;

                    //Globals.oApp.Menus.Item("293").String = "Links";
                    ////Links Sub-menues
                    //Globals.oApp.Menus.Item("294").String = "Roadmap";
                    //Globals.oApp.Menus.Item("295").String = "Help Portal";
                    //Globals.oApp.Menus.Item("296").String = "Support";
                    //Globals.oApp.Menus.Item("297").String = "Customer Influence";
                    //Globals.oApp.Menus.Item("298").String = "Top Resolutions";
                    //Globals.oApp.Menus.Item("299").String = "Training";
                    //Globals.oApp.Menus.Item("300").String = "Tips";
                    ////******************************************************************************************************************************************

                }
                catch (Exception Ex)
                {

                }

				// get the CANNABIS parameter form the install
	            var parmVal = Globals.oApp.Company.GetExtensionProperty(Environment.GetCommandLineArgs().GetValue(1).ToString(), SAPbouiCOM.BoExtensionLCMStageType.lcm_assignment, "Cannabis");
                Globals.IsCannabis = !string.Equals(parmVal, "N", StringComparison.InvariantCultureIgnoreCase);

				var parmVal2 = Globals.oApp.Company.GetExtensionProperty(Environment.GetCommandLineArgs().GetValue(1).ToString(), SAPbouiCOM.BoExtensionLCMStageType.lcm_assignment, "TestParm");
                //MessageBox.Show("Cannabis = " + parmVal + Environment.NewLine + "Parm 2 = " + parmVal2);

                // set the image path if running from Visual Studio
                if (Debugger.IsAttached) Globals.pathToImg = Globals.pathToImg.Substring(0, Globals.pathToImg.IndexOf(@"\AddOn")) + @"\ImageFiles\";
                
				NSC_DI.NSCDI.Init(Globals.oCompany, Globals.ProdVersion);

                Globals.AddonInitialized = (NSC_DI.UTIL.UDO.TableExists(NSC_DI.Globals.tSettings) && NSC_DI.UTIL.SQL.GetValue<int>($"SELECT COUNT(*) FROM [@{NSC_DI.Globals.tSettings}]", 0) > 0);

                if (Globals.AddonInitialized == false) Forms.F_Setup.FormCreate();
                else InitializeComponents();

            }
            catch (Exception ex)
            {
	            pErr = true;
	            MessageBox.Show("Error in loading. Closing Add-On." + Environment.NewLine + NSC_DI.UTIL.Message.Format(ex.Message));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(oForm);
            }
        }

        internal static void InitializeComponents()
        {
            SAPbouiCOM.Form oFrmSplash = null;
            try
            {
                try
                { // we don't want the splash screen to crash the whole addon
                    oFrmSplash = Forms.F_Splash.FormCreate();
                }
                catch { }

                // make sure a Compliance is aelected
                if (NSC_DI.UTIL.Settings.Value.Get("State Compliance") == "")
                {
                    NSC_DI.UTIL.Settings.Value.Set("State Compliance", "METRC", "State Compliance System: METRC, Biotrack, or NONE", false);
                    //B1Connections.theAppl.MessageBox("Select a Compliance value before proceeding.");
                    //throw new Exception("Compliance not selected.");
                }

                //-------------------------
                // crete any new DB fields
                if (Init() == false) throw new Exception("Error in creating database.");
                //-------------------------


                // set the Inter Company Code
                NSC_DI.Globals.InterCoCode = NSC_DI.SAP.Warehouse.Get_InterCoCode();
                //if (NSC_DI.UTIL.Settings.Value.Get("Inter-Company") != "Y") NSC_DI.Globals.InterCoCode = "";

                // set Branches
                Globals.BranchDflt = NavSol.Forms.SAP.F_169.GetDefaultBranch();
                Globals.BranchList = NSC_DI.SAP.Branch.GetAll_User_Str(Globals.BranchDflt);

                Globals.IsCannabis = (NSC_DI.UTIL.Settings.Value.Get("IsCannabis") == "Y");


                // update the product version if it has changed - this setting is informational onl;y
                NSC_DI.UTIL.Settings.Version.ProductChanged(Globals.ProdVersion);

                MenuAdd.AddFromSettings();
                               
                string can = (Globals.IsCannabis) ? "-C" : "";
                B1Connections.theAppl.SetStatusBarMessage("Add-on " + Globals.ProdName + ". Version: " + Globals.ProdVersion + can + " connected to company: " + Globals.oCompany.CompanyName, BoMessageTime.bmt_Medium, false);
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {// we don't want the splash screen to crash the whole addon
                    oFrmSplash.Close();
                }
                catch { }
                
            }
        }
        //------------------------------------------------------------
        public override void OnShutDown()
        {
        }

        public override void OnCompanyChanged()
        {
        }

		public override void OnLanguageChanged(BoLanguages language)
		{
		}

        public override void OnStatusBarErrorMessage(string txt)
        {
        }

        public override void OnStatusBarSuccessMessage(string txt)
        {
        }

        public override void OnStatusBarWarningMessage(string txt)
        {
        }

        public override void OnStatusBarNoTypedMessage(string txt)
        {
        }

        public override bool OnBeforeProgressBarCreated()
        {
            return true;
        }

        public override bool OnAfterProgressBarCreated()
        {
            return true;
        }

        public override bool OnBeforeProgressBarStopped(bool success)
        {
            return true;
        }

        public override bool OnAfterProgressBarStopped(bool success)
        {
            return true;
        }

        public override bool OnProgressBarReleased()
        {
            return true;
        }
    }
}
