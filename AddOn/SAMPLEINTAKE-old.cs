using System;
using System.Collections.Generic;
using System.Xml;
using SAPbobsCOM;
using SAPbouiCOM;
using B1WizardBase;
using System.Linq;

namespace NavSol.Forms.Sample_Management
{
    class F_SampleIntakeWizard : B1Event
    {
        #region ---------------------------------- VARS, CLASSES --------------------------------------------------------

        private const string cFormID = "NSC_SAMPLEINTAKE";
        private const string cItemType = "Additives";

        // FOR OLD CODE
        public static VERSCI.Forms _VirSci_Helper_Form;
        public static SAPbouiCOM.Application _SBO_Application;
        public static SAPbobsCOM.Company _SBO_Company;
        public static SAPbouiCOM.Form _SBO_Form;
        private SAPbouiCOM.ProgressBar oProgBar;
        private const string PREFIX_OF_SAMPLE_BATCHNUMBER = "SA";

        private CommonUI.ProgressBar _progressBar;
        private NSC_DI.Globals.ItemGroupTypes itemGroupType = NSC_DI.Globals.ItemGroupTypes.Sample;

        internal class SampleComplaincyInfo
        {
            public string TypeOfSample { get; set; }
            public string StateId { get; set; }
            public decimal Quantity { get; set; }
            public string SampleRecipient { get; set; }

            public string TestSampleID { get; set; }
        }

        internal class TransactionData
        {
            public string itemCode;
            public double quantity;
            public string sourceWarehouseId;

            public string destinationWarehouse;
            public string destinationWarehouseId;

            public DateTime dueDate;
            public string batchNo;

            public int productionOrderKey;
        }
        #endregion VARS

        #region ---------------------------------- BEFORE EVENT --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_VALIDATE
        [B1Listener(BoEventTypes.et_VALIDATE, true, new string[] { cFormID })]
        public virtual bool OnBeforeValidate(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            if (pVal.ItemUID == "TXT_BCFL" && pVal.InnerEvent == false) SetSampleType(oForm);

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, true, new string[] { cFormID })]
        public virtual bool OnBeforeItemPressed(ItemEvent pVal)
        {
            bool BubbleEvent = true;
            Form oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "1":
                    if(pVal.FormMode == (int)BoFormMode.fm_UPDATE_MODE) BTN_FINISH_Click_OLD(oForm);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();

            return BubbleEvent;
        }
        #endregion BEFORE EVENT

        #region ---------------------------------- AFTER EVENT  --------------------------------------------------------

        //--------------------------------------------------------------------------------------- et_RIGHT_CLICK
        [B1Listener(BoEventTypes.et_RIGHT_CLICK, false, new string[] { cFormID })]
        public virtual void OnAfterRightClick(ContextMenuInfo pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            _SBO_Form = oForm;

            switch (pVal.ItemUID)
            {
                case "MTX_OITEM":
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_KEY_DOWN
        [B1Listener(BoEventTypes.et_KEY_DOWN, false, new string[] { cFormID })]
        public virtual void OnAfterKeyDown(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            _SBO_Form = oForm;

            if (pVal.ItemUID == "TXT_RNAME") TXT_RNAME_HANDLER(pVal);

            if (pVal.ItemUID == "TXT_ITMCFL" && pVal.CharPressed == 9) CFL_Item_Branches(oForm, pVal);  // 10823-3
            if (pVal.ItemUID == "TXT_BCFL" && pVal.CharPressed == 9) CFL_Batch_Branches(oForm, pVal);  // 10823-3

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_ITEM_PRESSED
        [B1Listener(BoEventTypes.et_ITEM_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterItemPressed(ItemEvent pVal)
        {
            // if (pVal.InnerEvent == true) return;

            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;


            switch (pVal.ItemUID)
            {
                case "BTN_ADDLS":
                    BTN_ADDLS_Click();
                    break;

                case "BTN_REMOVE":
                    BTN_REMOVE_Click();
                    break;

                case "BTN_REMT":
                    BTN_REMOVE_TEST_Click();
                    break;

                case "btnItemCFL":   // 10823-3
                    CFL_Item_Branches(oForm, pVal);
                    break;

                case "btnBatCFL":   // 10823-3
                    CFL_Batch_Branches(oForm, pVal);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_COMBO_SELECT
        [B1Listener(BoEventTypes.et_COMBO_SELECT, false, new string[] { cFormID })]
        public virtual void OnAfterComboSelect(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            switch (pVal.ItemUID)
            {
                case "CMB_STYPE":
                    SampleTypeModified(oForm);
                    break;

                case "CMB_TEST":
                    AddTest(oForm);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_CHOOSE_FROM_LIST
        [B1Listener(BoEventTypes.et_CHOOSE_FROM_LIST, false, new string[] { cFormID })]
        public virtual void OnAfterChooseFromList(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            _SBO_Form = oForm;

            switch (pVal.ItemUID)
            {
                // Item ChooseFromList
                case "TXT_ITMCFL":
                    TXT_ITMCFL_ChooseFromList_Selected((IChooseFromListEvent)pVal);
                    break;
                // Warehouse ChooseFromList
                case "TXT_WRHS":
                    TXT_WRHS_ChooseFromList_Selected((IChooseFromListEvent)pVal);
                    break;
                // Batch ChooseFromList
                case "TXT_BCFL":
                    TXT_BATCH_ChooseFromList_Selected((IChooseFromListEvent)pVal);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }

        //--------------------------------------------------------------------------------------- et_MATRIX_LINK_PRESSED
        [B1Listener(BoEventTypes.et_MATRIX_LINK_PRESSED, false, new string[] { cFormID })]
        public virtual void OnAfterMatrixLinkPressed(ItemEvent pVal)
        {
            if (pVal.ActionSuccess == false) return;

            var oForm = B1Connections.theAppl.Forms.Item(pVal.FormUID);

            switch (pVal.ItemUID)
            {
                case "MTX_TEST":
                    //MTX_TEST_MATRIX_LINK_PRESSED(SAP_UI_ItemEvent: pVal);
                    break;
            }

            NSC_DI.UTIL.Misc.KillObject(oForm);
            GC.Collect();
        }
        #endregion AFTER EVENT

        public F_SampleIntakeWizard() : this(Globals.oApp, Globals.oCompany) { }
        public F_SampleIntakeWizard(Application SAPBusinessOne_Application, SAPbobsCOM.Company SAPBusinessOne_Company)
        {
            _SBO_Application = SAPBusinessOne_Application;
            _SBO_Company = SAPBusinessOne_Company;
        }

        public void Form_Load()
        {
            bool showStateID = false;
            SAPbouiCOM.Form oForm = null;

            try
            {
                oForm = CommonUI.Forms.Load(cFormID, true);

                // Freeze the Form UI
                oForm.Freeze(true);

                // HIDE SOME FIELDS IF NOT IN DEBUG MODE
                if(System.Diagnostics.Debugger.IsAttached == false)
                {
                    oForm.Items.Item("TXT_ITEMC").Visible = false;
                    oForm.Items.Item("TXT_ITMB").Visible  = false;
                    oForm.Items.Item("TXT_ITMS").Visible  = false;
                    oForm.Items.Item("txtRID").Visible    = false;
                    oForm.Items.Item("TXT_WRHSC").Visible = false;
                    oForm.Items.Item("TXT_RCODE").Visible = false;
                    oForm.Items.Item("TXT_BCODE").Visible = false;
                }

                showStateID = !(NSC_DI.UTIL.Settings.Version.GetCompliance() == "BIOTRACK");
                oForm.Items.Item("txtStateID").Visible = showStateID;
                oForm.Items.Item("staStateID").Visible = showStateID;

                CommonUI.Forms.SetFieldvalue.Logo(oForm, "IMG_HEADER", "WizardHeader.bmp");

                //                var sql = @"
                //SELECT * FROM (SELECT '99999999' AS [LineItem],'99999999ABCDEFGHIJKLMNOPQRSTUVWXYZ' AS [Item],'99999999ABCDEFGHIJKLMNOPQRSTUVWXYZ' AS [BatchNumber], 'ABC99999' AS [Warehouse]
                //, '99999999' AS [Quantity], 'ABC999991234567890' AS [UoM], 'ABC99999' AS [SourceWhs], '99999999ABCDEFGHIJKLMNOPQRSTUVWXYZ' AS [BatchItemCode], '99999999ABCDEFGHIJKLMNOPQRSTUVWXYZ' AS [StateID])
                //AS [ItemTable]";

                var sql = @"
SELECT '9999' AS [LineItem], SPACE(100) AS [Item], SPACE(40) AS [FromBatch], SPACE(8) AS [Warehouse]
, '99999' AS [Quantity], SPACE(10)AS [UoM], SPACE(8) AS [SourceWhs], SPACE(40) AS [BaItemCode], SPACE(64) AS [StateID],
SPACE(20) AS TestIDs, SPACE(100) AS RcpntName, SPACE(10) AS RcpntCode, SPACE(20) AS SampleType";

                // Find the Matrix on the form UI
                Matrix MTX_OITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_OITEM", oForm);
                MTX_OITEM.SelectionMode = BoMatrixSelect.ms_Single;

                if (MTX_OITEM.RowCount == 0)
                {
                    CommonUI.Matrix.LoadMatrixDB(oForm, "ItemTable", "MTX_OITEM", new List<CommonUI.Matrix.MatrixColumn>() {
                            new CommonUI.Matrix.MatrixColumn() { Caption="ID", ColumnName="LineItem", ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn() { Caption="Sample To Create", ColumnName="Item", ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn() { Caption="From Batch", ColumnName="FromBatch", ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn() { Caption="Destination WH", ColumnName="Warehouse", ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT}
                            ,new CommonUI.Matrix.MatrixColumn() { Caption="Quantity", ColumnName="Quantity", ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT }
                            ,new CommonUI.Matrix.MatrixColumn() { Caption="UoM", ColumnName="UoM", ColumnWidth=40, ItemType= BoFormItemTypes.it_EDIT }
                            ,new CommonUI.Matrix.MatrixColumn() { Caption="Source WH", ColumnName="SourceWhs", ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT }
                            ,new CommonUI.Matrix.MatrixColumn() { Caption="Batch Item Code", ColumnName="BaItemCode", ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT }
                            ,new CommonUI.Matrix.MatrixColumn() { Caption="State ID", ColumnName="StateID", ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT, IsVisable = showStateID }
                            ,new CommonUI.Matrix.MatrixColumn() { Caption="Test ID", ColumnName="TestIDs", ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT }
                            ,new CommonUI.Matrix.MatrixColumn() { Caption="Recipient", ColumnName="RcpntName", ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT }
                            ,new CommonUI.Matrix.MatrixColumn() { Caption="RecipientCode", ColumnName="RcpntCode", ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT, IsVisable = false }
                            ,new CommonUI.Matrix.MatrixColumn() { Caption="Sample Type", ColumnName="SampleType", ColumnWidth=80, ItemType= BoFormItemTypes.it_EDIT, IsVisable = false }
                        }, sql);

                    MTX_OITEM.AutoResizeColumns();

                    // Remove the first item in the Matrix
                    DataTable DataTable = oForm.DataSources.DataTables.Item("ItemTable");
                    DataTable.Rows.Remove(0);
                }

                MTX_OITEM.LoadFromDataSource();

                if (Globals.BranchDflt >= 0)
                {

                }
                else
                {
                    oForm.Items.Item("btnItemCFL").Visible = false;
                    oForm.Items.Item("btnBatCFL").Visible = false;

                    oForm.Items.Item("TXT_ITMCFL").Specific.ChooseFromListUID = "CFL_OITEM";
                    oForm.Items.Item("TXT_BCFL").Specific.ChooseFromListUID = "CFL_ITMB";
                    FilterItemChooseFromList(oForm);
                }

                // Warehouse CFL filters
                NavSol.CommonUI.CFL.AddCon(oForm, "CFL_WRHS", BoConditionRelationship.cr_NONE, "Inactive", BoConditionOperation.co_EQUAL, "N");   //10823
                NavSol.CommonUI.CFL.AddCon_Branches(oForm, "CFL_WRHS", BoConditionRelationship.cr_AND);   //10823


                // Initialize our Test Dropdown and Matrix; 
                //LoadTestMatrix();

                LoadTestDropDown(oForm);

                sql = "  SELECT 'Employee', 'Employee Sample' UNION SELECT 'Customer', 'Customer Sample' UNION SELECT 'Lab', 'Vendor Lab'";
                CommonUI.ComboBox.ComboBoxAddVals(oForm, "CMB_STYPE", sql, null);

                // Select the first tab by default
                //((Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_1", oForm)).Select();

                oForm.ActiveItem = "TXT_ITMCFL";

                ShowLabTestOptions(oForm, false);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                oForm.Freeze(false);
                oForm.VisibleEx = true;
            }
        }

        private void CFL_Item_Branches(Form pForm, ItemEvent pVal)  // 10823-3
        {
            try
            {
                if (Globals.BranchDflt < 0) return;

                // if they tab off of the field and it contains a value, then return
                if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pForm.Items.Item("TXT_WRHS").Specific.Value != "") return;

                var wh = pForm.Items.Item("TXT_WRHSC").Specific.Value;
                if (pForm.Items.Item("TXT_WRHS").Specific.Value == "")
                {
                    Globals.oApp.MessageBox("You must select a Warehouse 1st.");
                    return;
                }
                var branch = NSC_DI.SAP.Warehouse.GetBranch(wh);
                var sql = $@"
SELECT OITM.ItemName, OITM.ItemCode, OWHS.WhsCode, OWHS.WhsName, OITW.OnHand
  FROM OITM
 INNER JOIN OITW ON OITM.ItemCode = OITW.ItemCode
 INNER JOIN OWHS ON OITW.WhsCode = OWHS.WhsCode
 WHERE OITM.QryGroup64 = 'N' AND OITM.validFor = 'Y' AND OITM.U_NSC_Batchable = 'Y' AND OWHS.BPLid = '{branch}'
 ORDER BY OITM.ItemName";

                F_CFL_GRID.CflCB = delegate (string pCallingFormUid)
                    {
                        _SBO_Form = CommonUI.Forms.GetFormFromUID(pCallingFormUid);
                        Post_Item_ChooseFromList_Selected();
                    };
                F_CFL_GRID.FormCreate("Batch Choose From List", sql, pForm.UniqueID, "TXT_ITMCFL,TXT_ITEMC", "ItemName,ItemCode");
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        private void Post_Item_ChooseFromList_Selected()
        {
            try
            {
                string itemCode = _SBO_Form.Items.Item("TXT_ITEMC").Specific.Value;

                var batchItemCode = NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(itemCode).FirstOrDefault().ItemCode;

                _SBO_Form.Items.Item("TXT_BCODE").Specific.Value = batchItemCode;
                if (Globals.BranchDflt < 0)
                {
                    LoadBatchCFL(batchItemCode);
                    BatchSelectionVisible(false);
                }

                //// Save the business partners code
                //EditText TXT_ITEMC = (EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEMC", _SBO_Form);

                //TXT_ITEMC.Value = itemCode;

                //Determine if Purchase UoM exists is not set to inventory and then default to Grams
                string strUoM = null;
                strUoM = NSC_DI.UTIL.SQL.GetValue<string>($"Select T0.InvntryUom from OITM T0 where T0.ItemCode = '" + itemCode + "'");
                if (strUoM.Length > 0)
                {
                    //Check for Inventory UoM
                    SAPbouiCOM.StaticText lblUoM = _SBO_Form.Items.Item("LBL_UoM").Specific;
                    lblUoM.Caption = strUoM;
                }
                else
                {
                    //Default to grams if nothing is set
                    SAPbouiCOM.StaticText lblUoM = _SBO_Form.Items.Item("LBL_UoM").Specific;
                    lblUoM.Caption = "g";
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        private void CFL_Batch_Branches(Form pForm, ItemEvent pVal)  // 10823-3
        {
            try
            {
                if (Globals.BranchDflt < 0) return;

                // if they tab off of the field and it contains a value, then return
                if (pVal.EventType == BoEventTypes.et_KEY_DOWN && pForm.Items.Item("txtBatch").Specific.Value != "") return;

                var itemCode = pForm.Items.Item("TXT_BCODE").Specific.Value;
                var wh = pForm.Items.Item("TXT_WRHSC").Specific.Value;
                if (pForm.Items.Item("TXT_WRHS").Specific.Value == "")
                {
                    Globals.oApp.MessageBox("You must select a Warehouse 1st.");
                    return;
                }
                var branch = NSC_DI.SAP.Warehouse.GetBranch(wh);
                var sql = $@"
SELECT OBTN.ItemCode, OBTN.DistNumber, OBTQ.Quantity, OWHS.WhsCode, OWHS.WhsName
  FROM OBTN
 INNER JOIN OBTQ ON OBTN.AbsEntry = OBTQ.MdAbsEntry
 INNER JOIN OWHS ON OBTQ.WhsCode  = OWHS.WhsCode
 WHERE OBTN.ItemCode = '{itemCode}' AND OWHS.BPLid = '{branch}'";

                F_CFL_GRID.FormCreate("Batch Choose From List", sql, pForm.UniqueID, "TXT_BCFL,TXT_ITMS", "DistNumber,ItemCode");
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                GC.Collect();
            }
        }

        /// <summary>
        /// Will filter the choose from list of items to only display items that are considered "Sample" items.
        /// </summary>
        private void FilterItemChooseFromList(SAPbouiCOM.Form pForm)
        {
            try
            {
                CommonUI.CFL.AddCon(pForm, "CFL_OITEM", BoConditionRelationship.cr_NONE, NSC_DI.SAP.Items.Property.GetFieldName("Sample"), BoConditionOperation.co_EQUAL, "Y");
                CommonUI.CFL.AddCon(pForm, "CFL_OITEM", BoConditionRelationship.cr_AND, NSC_DI.SAP.Items.Property.GetFieldName("Template"), BoConditionOperation.co_EQUAL, "N");
                return;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void TXT_RNAME_HANDLER(ItemEvent pVal)
        {
            EditText TXT_RNAME = null;
            try
            {
                if (pVal.CharPressed == 9)
                {
                    TXT_RNAME = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_RNAME", _SBO_Form);

                    // Only handle the tab event if the text field is empty.
                    if (string.IsNullOrEmpty(TXT_RNAME.Value))
                    {
                        TXT_RNAME.Active = true;
                        HandleChooseFromListSelectionCFL();
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(TXT_RNAME);
                GC.Collect();
            }
        }

        private void LoadTestDropDown(SAPbouiCOM.Form pForm)
        {
            try
            {
                ComboBox cmbTest = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_TEST", pForm) as ComboBox;

                // Prepare to run a SQL statement.
                SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                // Count how many current records exist within the database.
                oRecordSet.DoQuery(string.Format("SELECT Code, [U_Name] FROM [@" + NSC_DI.Globals.tTests + "]"));


                // If items already exist in the drop down
                if (cmbTest.ValidValues.Count > 0)
                {
                    // Remove all currently existing values from warehouse drop down
                    for (int i = cmbTest.ValidValues.Count; i-- > 0;)
                    {
                        cmbTest.ValidValues.Remove(i, BoSearchKey.psk_Index);
                    }
                }

                // If more than 1 warehouses exists
                if (oRecordSet.RecordCount > 1)
                {
                    // Create the first item as an empty item
                    cmbTest.ValidValues.Add("", "");

                    // Select the empty item (forcing the user to make a decision)
                    cmbTest.Select(0, BoSearchKey.psk_Index);
                }

                // Add allowed warehouses to the drop down
                for (int i = 0; i < oRecordSet.RecordCount; i++)
                {
                    try
                    {
                        cmbTest.ValidValues.Add(oRecordSet.Fields.Item(1).Value.ToString(), oRecordSet.Fields.Item(0).Value.ToString());
                    }
                    catch { }
                    cmbTest.Item.Enabled = true;
                    oRecordSet.MoveNext();
                }

                // If we only have exactly one warehouse that contains our item simply select it and disable the field.
                if (cmbTest.ValidValues.Count == 1)
                {
                    cmbTest.Select(0, BoSearchKey.psk_Index);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void BTN_REMOVE_TEST_Click()
        {
            try
            {
                // Getting a DataTable of the DataSource.
                DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("TestTable");

                // Grab the matrix from the form UI
                Matrix MTX_TEST = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_TEST", _SBO_Form) as Matrix;

                // For each row already selected
                for (int i = 1; i < (MTX_TEST.RowCount + 1); i++)
                {
                    if (MTX_TEST.IsRowSelected(i))
                    {
                        try
                        {
                            DataTable.Rows.Remove(i - 1);
                        }
                        catch { }
                    }
                }
                MTX_TEST.LoadFromDataSource();
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void AddAllTestsToItem(string BatchNumber, string pRecipientType, string pRecipientCode, string pTestCodes)
        {
            try
            {
                if (pTestCodes.Length < 1) return;
                string[] testCodeAry = pTestCodes.Split(',').ToArray();

                // Validate that we aren't adding the same Test Twice.
                for (int i = 0; i < testCodeAry.Length; i++)
                {
                    string testCode = testCodeAry[i];

                    var nextCode = NSC_DI.UTIL.SQL.GetValue<string>($"SELECT ISNULL((MAX(CONVERT(int,[Code])) + 1),1) FROM[@{NSC_DI.Globals.tTestSamp}]", "1");

                    NSC_DI.SAP.Test.CreateSampleRelation(nextCode, testCode, BatchNumber, pRecipientType, pRecipientCode);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void TXT_ITMCFL_ChooseFromList_Selected(IChooseFromListEvent pChooseFromListEvent)
        {
            try
            {
                SAPbouiCOM.ChooseFromList oChooseFromList = _SBO_Form.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

                if (pChooseFromListEvent.BeforeAction == false && pChooseFromListEvent.SelectedObjects != null)
                {
                    DataTable oDataTable = pChooseFromListEvent.SelectedObjects;
                    string oItemCode = oDataTable.GetValue(0, 0).ToString();
                    string oItemName = oDataTable.GetValue(1, 0).ToString();

                    // Set the userdatasource for the name textbox
                    _SBO_Form.DataSources.UserDataSources.Item("UDS_OITEM").ValueEx = Convert.ToString(oItemName);
                    _SBO_Form.Items.Item("TXT_ITEMC").Specific.Value = oItemCode;

                    Post_Item_ChooseFromList_Selected();
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(oUDT);
                GC.Collect();
            }
        }

        private void TXT_WRHS_ChooseFromList_Selected(IChooseFromListEvent pChooseFromListEvent)
        {
            SAPbouiCOM.ChooseFromList oChooseFromList = _SBO_Form.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

            if (pChooseFromListEvent.BeforeAction == false && pChooseFromListEvent.SelectedObjects != null)
            {
                DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

                try
                {
                    string oWarehouseCode = oDataTable.GetValue(0, 0).ToString();
                    string oWarehouseName = oDataTable.GetValue(1, 0).ToString();

                    // Set the userdatasource for the name textbox
                    _SBO_Form.DataSources.UserDataSources.Item("UDS_WRHS").ValueEx = Convert.ToString(oWarehouseName);

                    ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHSC", _SBO_Form)).Value = oWarehouseCode;

                    // Prepare to run a SQL statement.
                    SAPbobsCOM.Recordset oRecordSet = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                    // Count how many current records exist within the database.
                    oRecordSet.DoQuery(string.Format("SELECT DISTINCT TOP 1 [OWHS].[U_{0}_WhrsType] FROM [OWHS] WHERE [OWHS].WhsCode = '{1}'", Globals.SAP_PartnerCode, oWarehouseCode));

                    string queryResultType = "";

                    if (oRecordSet.RecordCount <= 0)
                    {
                        // The warehouse doesn't have a warehouse type set.
                        queryResultType = "None";
                    }
                    else
                    {
                        // Move to the first record.
                        oRecordSet.MoveFirst();

                        // We should only ever get one row from this.
                        queryResultType = oRecordSet.Fields.Item(0).Value.ToString();
                    }

                    // RHH - commented out. the field does not exist on the form.
                    //((EditText)CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_STATE", _SBO_Form)).Value = queryResultType;
                }
                catch (Exception ex)
                {
                    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                }
            }
        }

        private void BatchSelectionVisible(bool isVisible)
        {
            try
            {

                StaticText LBL_ITMB = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.StaticText, "LBL_ITMB", _SBO_Form);

                EditText TXT_BCFL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_BCFL", _SBO_Form);

                TXT_BCFL.Item.Visible = isVisible;
                LBL_ITMB.Item.Visible = isVisible;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
        }

        private void LoadBatchCFL(string itemCode)
        {
            try
            {
                EditText TXT_BCFL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_BCFL", _SBO_Form);

                TXT_BCFL.Value = "";

                BatchSelectionVisible(true);

                SAPbouiCOM.ChooseFromList CFL_ITMBS = _SBO_Form.ChooseFromLists.Item("CFL_ITMB");
                CFL_ITMBS.SetConditions(null);

                Conditions objConditions = CFL_ITMBS.GetConditions();

                Condition objCondition;
                objCondition = objConditions.Add();

                //Item is Batched Display batched only.
                objCondition.Alias = "ItemCode";
                objCondition.Operation = BoConditionOperation.co_EQUAL;
                objCondition.CondVal = itemCode;

                //objCondition.Relationship = SAPbouiCOM.BoConditionRelationship.cr_AND;

                //// We only care about Batches that contain Quantities greater than 0
                //objCondition = objConditions.Add();
                //objCondition.Alias = "Quantity";
                //objCondition.Operation = SAPbouiCOM.BoConditionOperation.co_GRATER_THAN;
                //objCondition.CondVal = "0";

                CFL_ITMBS.SetConditions(objConditions);

            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex), ex);
            }
        }

        private void TXT_BATCH_ChooseFromList_Selected(IChooseFromListEvent pChooseFromListEvent)
        {
            if (pChooseFromListEvent.BeforeAction) return;

            try
            {

                SAPbouiCOM.ChooseFromList oChooseFromList = _SBO_Form.ChooseFromLists.Item(pChooseFromListEvent.ChooseFromListUID);

                DataTable oDataTable = pChooseFromListEvent.SelectedObjects;

                try
                {
                    string itemCode      = oDataTable.GetValue(0, 0).ToString();
                    string batchNumber   = oDataTable.GetValue(1, 0).ToString();
                    string warehouseCode = oDataTable.GetValue(2, 0).ToString();

                    // Set the userdatasource for the name textbox
                    _SBO_Form.DataSources.UserDataSources.Item("UDS_ITMB").ValueEx = Convert.ToString(batchNumber);

                    // Save the Batch Item Code
                    ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_BCODE", _SBO_Form))
                    .Value = itemCode;

                    // Save the Batch Number
                    ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITMB", _SBO_Form))
                    .Value = batchNumber;

                    // Save the Warehouse Code of this Batch.
                    ((EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHBC", _SBO_Form))
                    .Value = warehouseCode;

                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void BTN_FINISH_Click_OLD(Form pForm)
        {
            try
            {
                int newPDOKwy = 0;
                //// Selected Type Code to send with the API.. txtRID
                //EditText TXT_RNAME = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_RNAME", pForm);


                //if (TXT_RNAME == null || string.IsNullOrEmpty(TXT_RNAME.Value))
                //{
                //    Globals.oApp.StatusBar.SetText("Please select a recipient!",
                //        Type: BoStatusBarMessageType.smt_Error,
                //        Seconds: BoMessageTime.bmt_Short);

                //    return;
                //}

                //var txtRID = pForm.Items.Item("txtRID").Specific.Value;

                //ComboBox CMB_STYPE = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.ComboBox, "CMB_STYPE", pForm) as ComboBox;

                //string sampleTypeValue = CMB_STYPE.Value.ToString();


                // We need to query the database for what kind of items we are taking in..
                Recordset oRS = Globals.oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                // DataTable of all the items
                DataTable oDT = pForm.DataSources.DataTables.Item("ItemTable");

                // Setting up a Progress Bar.
                // _progressBar = new SAP_BusinessOne.Helpers.ProgressBar(SBO_Application: Globals.oApp);
                // _progressBar.Create("Creating Goods Receipt Purchase Order.", DataTable.Rows.Count, false);

                List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines> ListOfItemsToReceipt = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines>();
                List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines> ListOfItemsToIssue = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();
                /* // COMPLIANCY
                            BioTrack.API btAPI = TraceCon.new_API_obj();
                */
                //List<SampleComplaincyInfo> sampleComplaincyToCreate = new List<SampleComplaincyInfo>();
                try
                {
                    for (int i = 0; i < oDT.Rows.Count; i++)
                    {
                        if (NSC_DI.UTIL.Settings.Value.Get("Use SQL Transactions") == "Y") Globals.oCompany.StartTransaction();

                        string creationItemCode = oDT.GetValue("Item", i).ToString();
                        var quantity = Convert.ToDouble(oDT.GetValue("Quantity", i).ToString());
                        string warehouseCode = oDT.GetValue("Warehouse", i).ToString();
                        string batchNumber = oDT.GetValue("FromBatch", i).ToString();
                        string batchItemCode = oDT.GetValue("BaItemCode", i).ToString();
                        string matStateID = oDT.GetValue("StateID", i).ToString();
                        var sampleTypeValue = oDT.GetValue("SampleType", i).ToString();
                        var TXT_RNAME = oDT.GetValue("RcpntName", i).ToString();
                        var txtRID = !string.IsNullOrWhiteSpace(TXT_RNAME) ? NSC_DI.SAP.BusinessPartners.GetPartnerCode(TXT_RNAME) : "";

                        // We only care if the item is batched or not.
                        string sqlQuery = $@"
SELECT OITM.ManBtchNum, OBTN.U_NSC_StateID AS [StateID]
  FROM OITM
  JOIN OIBT ON OIBT.ItemCode = OITM.ItemCode
  JOIN OBTN ON OBTN.DistNumber = OIBT.BatchNum
 WHERE OITM.ItemCode = '{batchItemCode}' AND OBTN.DistNumber = '{batchNumber}'";

                        oRS.DoQuery(sqlQuery);
                        oRS.MoveFirst();

                        // Be sure to get the State id!
                        string stateId = oRS.Fields.Item("StateID").Value.ToString();
                        if (stateId == "") stateId = matStateID;

                        // Figure out if we are dealing with a Batched Item, Serialized Item, or neither.
                        bool isBatch = oRS.Fields.Item("ManBtchNum").Value.ToString() == "Y" ? true : false;

                        if (string.IsNullOrEmpty(creationItemCode))
                        {
                            Globals.oApp.StatusBar.SetText("Failed to pull the Bill of Materials!");

                            return;
                        }

                       
                        if (isBatch)
                        {
                            // Pull the source warehouse from the item we are grabbing.
                            string sourceWarehouse = oDT.GetValue("SourceWhs", i).ToString();
                            string destinationWarehouse = oDT.GetValue("Warehouse", i).ToString();
                            ListOfItemsToIssue = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines>();
                            // Create a Goods Issued for the amount we are removing from the Batched item.
                            ListOfItemsToIssue.Add(new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines()
                            {
                                ItemCode = batchItemCode,
                                Quantity = quantity,
                                WarehouseCode = sourceWarehouse,
                                BatchLineItems = new List<NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches>()
                            {
                                new NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines_Batches()
                                {
                                    BatchNumber = batchNumber,
                                    Quantity = quantity
                                }
                            }
                            });

                            // Prepare a list of line items for the production order
                            List<NSC_DI.SAP.ProductionOrder_OLD.StandardProductionOrderComponentLineItem> components = new List<NSC_DI.SAP.ProductionOrder_OLD.StandardProductionOrderComponentLineItem>();

                            foreach (var item in NSC_DI.SAP.BillOfMaterials.ListOfComponentsFromAnItemCode(creationItemCode))
                            {
                                components.Add
                                (
                                    new NSC_DI.SAP.ProductionOrder_OLD.StandardProductionOrderComponentLineItem()
                                    {
                                        ItemCodeForItemBeingDestroyed = item.ItemCode,
                                        SourceWarehouseCode = sourceWarehouse,
                                        BaseQuantity = item.Quantity,
                                        PlannedQuantity = quantity
                                    }
                                );
                            }

                            //Remarks
                            EditText TXT_REM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_REM", pForm) as EditText;
                            string Remarks = TXT_REM.Value;



                            // Take the Items out of our Inventory.
                            foreach (NSC_DI.SAP.GoodsIssued.GoodsIssued_Lines line in ListOfItemsToIssue)
                            {
                                try
                                {
                                    newPDOKwy = NSC_DI.SAP.ProductionOrder_OLD.CreateStandardProductionOrder(creationItemCode
                                        , line.Quantity
                                        , destinationWarehouse
                                        , DateTime.Now
                                        , components
                                        , pRemarks: Remarks
                                        , pProcess: (sampleTypeValue == "Lab")? "QA Sample" : "Sample");
                                    //Update status of Production order to released.
                                    NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(newPDOKwy, BoProductionOrderStatusEnum.boposReleased);

                                    NSC_DI.SAP.IssueFromProduction.Create(newPDOKwy,DateTime.Now, ListOfItemsToIssue);
                                    
                                    Globals.oApp.StatusBar.SetText("Goods were issued!", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Success);
                                }
                                catch (Exception ex)
                                {
                                    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
                                }
                            }

                        }
                        // COMPLIANCY
                        // Might need to create Complaincy here..
                        SampleComplaincyInfo sampleCompliancy = new SampleComplaincyInfo();

                        sampleCompliancy.SampleRecipient = TXT_RNAME;
                        sampleCompliancy.TypeOfSample = sampleTypeValue;
                        sampleCompliancy.StateId = stateId;
                        sampleCompliancy.Quantity = Convert.ToDecimal(quantity);

                        //CreateSampleCompliancy(ref sampleCompliancy, btAPI);
                        

                        Dictionary<string, string> userDefinedFields = new Dictionary<string, string>()
                        {
                            { "U_"+Globals.SAP_PartnerCode+"_MotherID", batchNumber}
                        };

                        if (!string.IsNullOrEmpty(sampleCompliancy.TestSampleID))
                        {
                            userDefinedFields = new Dictionary<string, string>()
                            {
                               { "U_"+Globals.SAP_PartnerCode+"_MotherID", batchNumber},
                                { "U_"+Globals.SAP_PartnerCode+"_LabTestID", sampleCompliancy.TestSampleID}
                            };

                            // ToDO: Update the current batch to have the LabTestID.
                            NSC_DI.SAP.BatchItems.UpdateUDF(
                                pBatchID: batchNumber,
                                udfName: "LabTestID",
                                udfValue: sampleCompliancy.TestSampleID);

                        }
                        else
                        {
                            if (sampleTypeValue == "Lab")
                            {
                                //Create Random Unique Test ID and set it on the batch
                                sampleCompliancy.TestSampleID = Guid.NewGuid().ToString();

                                userDefinedFields = new Dictionary<string, string>()
                            {
                               { "U_"+Globals.SAP_PartnerCode+"_MotherID", batchNumber},
                                { "U_"+Globals.SAP_PartnerCode+"_LabTestID", sampleCompliancy.TestSampleID}
                            };

                                // ToDO: Update the current batch to have the LabTestID.
                                NSC_DI.SAP.BatchItems.UpdateUDF(
                                    pBatchID: batchNumber,
                                    udfName: "LabTestID",
                                    udfValue: sampleCompliancy.TestSampleID);
                            }
                        }

                        //Inter-Company Check 
                        string InterCoSubCode = NSC_DI.UTIL.SQL.GetValue<string>($@"select ISNULL(T0.U_NSC_SubsidiaryID,'') from OWHS T0 where T0.WhsCode = '{warehouseCode}'");
                        string InterCo = NSC_DI.UTIL.SQL.GetValue<string>("select T0.U_Value from dbo.[@NSC_SETTINGS] T0 where T0.Name = 'Inter-Company'");


                        //string newBatchNumber = controllerBatch.GetNextAvailableBatchID(ItemCode: creationItemCode, Prefix: PREFIX_OF_SAMPLE_BATCHNUMBER);
                        var newBatchNumber = NSC_DI.UTIL.AutoStrain.NextBatch(creationItemCode);
                        newBatchNumber = NSC_DI.UTIL.Settings.concatBatchNum(InterCoSubCode, newBatchNumber);
                        //try
                        //{
                        //    //Inter-Company Check 
                        //    if (InterCo == "Y" && InterCoSubCode.Trim().Length > 0)
                        //    {
                        //        newBatchNumber = InterCoSubCode.Trim() + "-" + newBatchNumber;
                        //    }

                        //}
                        //catch
                        //{
                        //    throw new Exception(NSC_DI.UTIL.Message.Format("Inter-Company Error: Please check your settings (Y or N) and make sure you have a subsidiary code on your warehouse."));
                        //}

                        //Added this because the list was adding additional lines because of its positioning in the 4-loop
                        ListOfItemsToReceipt.Clear();

                        ListOfItemsToReceipt.Add(new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines()
                        {
                            ItemCode = creationItemCode, 
                            Quantity = quantity,
                            WarehouseCode = warehouseCode,
                            ListOfBatchLineItems = new List<NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem>()
                            {
                                new NSC_DI.SAP.GoodsReceipt.GoodsReceipt_Lines.BatchItem()
                                {
                                    BatchNumber = newBatchNumber
                                    ,Quantity = quantity
                                    ,UserDefinedFields = userDefinedFields
                                    ,ManufacturerSerialNumber = stateId
                                }
                            } 
                        });


                        // _progressBar.SetText("Created a Batch Line Item : " + batchItemCode);

                        // _progressBar.Increment();
                        if (sampleTypeValue == "Lab")
                        {
                            // Add Association of each Test we have.
                            AddAllTestsToItem(newBatchNumber, sampleTypeValue, txtRID, oDT.GetValue("TestIDs", i).ToString());
                        }



                        //_progressBar.Stop();
                        NSC_DI.SAP.ReceiptFromProduction.Create(newPDOKwy, DateTime.Now, ListOfItemsToReceipt);


                       NSC_DI.SAP.BatchItems.CopyUDFs(newBatchNumber, batchNumber, new string[1] { "U_NSC_StateID" });


                        //    controllerGoodsReceiptPO.Create(
                        //productionOrderKey: controllerProductionOrder.NewlyCreatedKey,
                        //documentDate: DateTime.Now,
                        //listOfGoodsReceiptLines: ListOfItemsToReceipt,
                        //CallingForm: "Sample Intake Wizard");

                        // Get DocEntry from ItemCode/SysNumber
                        /*
                        string SQL_Query_GetDocEntryFromItemCodeAndSysNum = string.Format(@"
            SELECT
            [AbsEntry]
            FROM [OBTN]
            WHERE [OBTN].DistNumber = '{0}'", batchNumber);

                        // Prepare an SQL Helper
                        SAP_BusinessOne.Helpers.SQL VirSci_Helpers_SQL = new SAP_BusinessOne.Helpers.SQL(SAPBusinessOne_Company: Globals.oCompany);

                        int DocEntry = 0;

                        // Attempt to get the DocEntry number from our SQL results
                        if (Int32.TryParse(VirSci_Helpers_SQL.GetFieldsFromSQLQuery(SQL_Query_GetDocEntryFromItemCodeAndSysNum).Item(0).Value.ToString(), out DocEntry))
                        {
                            // Manually update the batch - because the above did not work
                            string sql = @"
                                                UPDATE
                                                [OBTN]
                                                SET
                                                [U_VSC_MotherID] = '" + batchNumber + @"',
                                                [U_VSC_LabTestID] = '" + sampleCompliancy.TestSampleID + @"' 
                                                WHERE [DistNumber]='" + newBatchNumber +"'";
                            //Remove last ","

                            SAP_BusinessOne.Helpers.SQL dataAccessLayer = new SAP_BusinessOne.Helpers.SQL(SAPBusinessOne_Company: Globals.oCompany);

                            dataAccessLayer.RunSQLQuery(SQLQuery: SQL);

                        }
                        */
                        // Construct a "Goods Receipt" document
                        //controllerGoodsReceipt.Create(
                        //    documentDate: DateTime.Now,
                        //    ListOfGoodsReceiptLines: ListOfItemsToReceipt
                        //);
                        Globals.oApp.StatusBar.SetText("Goods received!", BoMessageTime.bmt_Medium, BoStatusBarMessageType.smt_Success);

                        //close procuction order
                        NSC_DI.SAP.ProductionOrder_OLD.UpdateStatus(newPDOKwy, BoProductionOrderStatusEnum.boposClosed);

                        if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                    } //End of For Loop of Items




                    //pForm.Close();
                }
                catch (Exception e)
                {
                    Globals.oApp.StatusBar.SetText(e.Message);
                    Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(e));
                    throw new System.ComponentModel.WarningException();
                }
            }
            catch (System.ComponentModel.WarningException) { }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                if (Globals.oCompany.InTransaction) Globals.oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
            }

        }

        private void BTN_NEXT_Click()
        {
            try
            {
                switch (CurrentlySelectedTab())
                {
                    case 1:
                        ((Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_2", _SBO_Form))
                        .Select();
                        break;

                    case 2:
                        ((Folder)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_3", _SBO_Form))
                        .Select();
                        break;
                }
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private int CurrentlySelectedTab()
        {
            try
            {
                for (int i = 1; i <= 3; i++)
                {
                    Folder tab = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Folder, "TAB_" + i.ToString(), _SBO_Form);

                    if (tab.Selected)
                    {
                        return i;
                    }
                }

                return 0;
            }
            catch (Exception ex)
            {

                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void BTN_ADDLS_Click()
        {
            try
            {
                #region Validate Fields
                // Validate that we have an Item selected.
                EditText TXT_ITEMC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITEMC", _SBO_Form);

                EditText TXT_ITMCFL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_ITMCFL", _SBO_Form);

                if (string.IsNullOrEmpty(TXT_ITEMC.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please select an item!", BoMessageTime.bmt_Short);
                    TXT_ITMCFL.Active = true;
                    return;
                }

                // Validate that a warehouse is selected.
                EditText TXT_WRHSC = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHSC", _SBO_Form);

                EditText TXT_WRHS = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHS", _SBO_Form);

                if (string.IsNullOrEmpty(TXT_WRHSC.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please select a warehouse!", BoMessageTime.bmt_Short);
                    TXT_WRHS.Active = true;
                    return;
                }

                // Validate that a quantity was entered.
                EditText TXT_QUANT = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_QUANT", _SBO_Form);

                if (string.IsNullOrEmpty(TXT_QUANT.Value))
                {
                    Globals.oApp.StatusBar.SetText("Please enter a quantity!", BoMessageTime.bmt_Short);
                    TXT_QUANT.Active = true;
                    return;
                }

                string batchNumber = "";

                EditText TXT_BCFL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_BCFL", _SBO_Form);

                EditText TXT_SCFL = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_SCFL", _SBO_Form);

                // Getting either a Serial or Batch.
                // It doesn't matter which because the Finish button will check if the Item is Serial or Batched.
                if (!string.IsNullOrEmpty(TXT_BCFL.Value))
                {
                    batchNumber = TXT_BCFL.Value;
                }

                if (!string.IsNullOrEmpty(TXT_SCFL.Value))
                {
                    batchNumber = TXT_SCFL.Value;
                }

                if (string.IsNullOrEmpty(batchNumber))
                {
                    Globals.oApp.StatusBar.SetText("Please select a batch or serial number of the item!", BoMessageTime.bmt_Short);
                    TXT_ITMCFL.Active = true;
                    return;
                }

                //Validate that the UoM was set
                string UoM = CommonUI.Forms.GetField<string>(_SBO_Form, "LBL_UoM");
                if (string.IsNullOrEmpty(UoM))
                {
                    Globals.oApp.StatusBar.SetText("Please enter a unit of Measure!", BoMessageTime.bmt_Short);
                    return;
                }
#endregion
                EditText TXT_SourceWarehouse = (EditText)CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_WRHBC", _SBO_Form);


                EditText TXT_BatchItemCode = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.EditText, "TXT_BCODE", _SBO_Form) as EditText;

                // Find the matrix in the form UI
                Matrix MTX_OITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_OITEM", _SBO_Form);

                // Determine the next row number
                int oNewRowNumber = MTX_OITEM.RowCount;

                DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("ItemTable");

                _SBO_Form.Freeze(true);

                DataTable.Rows.Add();
                DataTable.SetValue("LineItem", oNewRowNumber, oNewRowNumber + 1);
                DataTable.SetValue("Item", oNewRowNumber, TXT_ITEMC.Value.ToString());
                DataTable.SetValue("FromBatch", oNewRowNumber, batchNumber);
                DataTable.SetValue("Warehouse", oNewRowNumber, TXT_WRHSC.Value.ToString());
                DataTable.SetValue("Quantity", oNewRowNumber, TXT_QUANT.Value.ToString());
                DataTable.SetValue("UoM", oNewRowNumber, UoM);
                // This column will only exist for Batched Items.
                DataTable.SetValue("SourceWhs", oNewRowNumber, TXT_SourceWarehouse.Value.ToString());
                DataTable.SetValue("BaItemCode", oNewRowNumber, TXT_BatchItemCode.Value);
                DataTable.SetValue("StateID", oNewRowNumber, _SBO_Form.Items.Item("txtStateID").Specific.Value);
                DataTable.SetValue("TestIDs", oNewRowNumber, _SBO_Form.Items.Item("txtDefTest").Specific.Value);
                DataTable.SetValue("RcpntName", oNewRowNumber, _SBO_Form.Items.Item("TXT_RNAME").Specific.Value);
                DataTable.SetValue("RcpntCode", oNewRowNumber, _SBO_Form.Items.Item("txtRID").Specific.Value);
                DataTable.SetValue("SampleType", oNewRowNumber, _SBO_Form.Items.Item("CMB_STYPE").Specific.Value);

                _SBO_Form.Items.Item("txtStateID").Specific.Value = "";

                // Add new row to matrix
                MTX_OITEM.LoadFromDataSource();
                MTX_OITEM.AutoResizeColumns();

                //// Clear item code
                //TXT_ITEMC.Value = "";

                //// Clear warehouse code
                //TXT_WRHSC.Value = "";

                //// Clear quantity
                //TXT_QUANT.Value = "";

                //_SBO_Form.Items.Item("txtDefTest").Specific.Value = "";
                //_SBO_Form.Items.Item("TXT_RNAME").Specific.Value = "";
                //_SBO_Form.Items.Item("txtRID").Specific.Value = "";

                //// Clear our invisible Source warehouse. 
                //TXT_SourceWarehouse.Value = "";

                //// Clear Choose from Lists.
                //TXT_WRHS.Value = "";
                //TXT_BCFL.Value = "";
                //TXT_SCFL.Value = "";
                //TXT_ITMCFL.Value = "";

                for (var i = 0; i < _SBO_Form.Items.Count; i++)
                {
                    var itemType = _SBO_Form.Items.Item(i).Type;
                    if (itemType == BoFormItemTypes.it_EDIT || itemType == BoFormItemTypes.it_EXTEDIT) _SBO_Form.Items.Item(i).Specific.Value = "";
                    //if (itemType == BoFormItemTypes.it_COMBO_BOX) _SBO_Form.Items.Item(i).Specific.Select(0, BoSearchKey.psk_Index);  // does not work
                }

                // the 1st Select causes the form object to "disconnect".
                var fid = _SBO_Form.UniqueID;
                _SBO_Form.Items.Item("CMB_STYPE").Specific.Select(0, BoSearchKey.psk_Index);
                _SBO_Form = B1Connections.theAppl.Forms.Item(fid);
                _SBO_Form.Items.Item("CMB_TEST").Specific.Select(0, BoSearchKey.psk_Index);

                _SBO_Form.Mode = BoFormMode.fm_UPDATE_MODE;
                _SBO_Form.Items.Item("1").Specific.Caption = "Add";
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                _SBO_Form.Freeze(false);
                GC.Collect();
            }
        }

        /// <summary>
        /// Handle a Matrix Click event. In this case we will be getting setting a selected row?
        /// </summary>
        private void BTN_REMOVE_Click()
        {
            try
            {
                // Getting a DataTable of the DataSource.
                DataTable DataTable = _SBO_Form.DataSources.DataTables.Item("ItemTable");

                // Grab the matrix from the form UI
                Matrix MTX_OITEM = CommonUI.Forms.GetControlFromForm(CommonUI.Forms.FormControlTypes.Matrix, "MTX_OITEM", _SBO_Form) as Matrix;

                // For each row already selected
                for (int i = 1; i < (MTX_OITEM.RowCount + 1); i++)
                {
                    if (MTX_OITEM.IsRowSelected(i))
                    {
                        try
                        {
                            DataTable.Rows.Remove(i - 1);
                        }
                        catch { }
                    }
                }
                MTX_OITEM.LoadFromDataSource();
            }
            catch (Exception ex)
            {

                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
        }

        private void ShowLabTestOptions(Form pForm, bool isVisible)
        {
            try
            {
                pForm.Items.Item("lblToAdd").Visible = isVisible;
                pForm.Items.Item("staDefTest").Visible = isVisible;
                pForm.Items.Item("txtDefTest").Visible = isVisible;
                pForm.Items.Item("CMB_TEST").Visible = isVisible;
            }
            catch (Exception ex)
            {
                throw new Exception(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(CMB_STYPE);
                GC.Collect();
            }
        }

        private void AddTest(Form pForm)
        {
            try
            {
                var newTest = CommonUI.Forms.GetField<int>(pForm, "CMB_TEST", true);
                string testStr = pForm.Items.Item("txtDefTest").Specific.Value;
                if (testStr.Length > 0)
                {
                    int[] testAry = testStr.Split(',').Select(str => int.Parse(str)).ToArray();
                    if (testAry.Contains<int>(newTest)) return;
                }

                if(testStr.Length > 0) testStr += ",";
                testStr += newTest.ToString();
                pForm.Items.Item("txtDefTest").Specific.Value = testStr;
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(CMB_STYPE);
                GC.Collect();
            }
        }

        private void SetSampleType(Form pForm)
        {
            // sample type to Lab 

            try
            {
                CommonUI.ComboBox.SetVal(pForm, "CMB_STYPE", "Lab", SAPbouiCOM.BoSearchKey.psk_ByValue);
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                //NSC_DI.UTIL.Misc.KillObject(CMB_STYPE);
                GC.Collect();
            }
        }

        private void SampleTypeModified(Form pForm)
        {
            ComboBox CMB_STYPE = null;

            try
            {

                CMB_STYPE = pForm.Items.Item("CMB_STYPE").Specific;

                string sampleTypeValue = CMB_STYPE.Value.ToString();

                switch (sampleTypeValue)
                {
                    case "Lab":
                        ShowLabTestOptions(pForm, true);

                        // get the default test ids
                        var itemCode = _SBO_Form.Items.Item("TXT_BCODE").Specific.Value.ToString();
                        var sql = $@"SELECT OITB.U_NSC_CmplTestID FROM OITB INNER JOIN OITM ON OITB.ItmsGrpCod = OITM.ItmsGrpCod WHERE OITM.ItemCode = '{itemCode}'";
                        pForm.Items.Item("txtDefTest").Specific.Value = NSC_DI.UTIL.SQL.GetValue<string>(sql, "");

                        break;
                    case "Employee":
                    case "Vendor":
                    default:
                        ShowLabTestOptions(pForm, false);
                        pForm.Items.Item("txtDefTest").Specific.Value = "";
                        break;
                }

                // Select our current form.
                pForm.Select();

                pForm.Items.Item("TXT_RCODE").Specific.Value = "";
                pForm.Items.Item("TXT_RNAME").Specific.Value = "";
            }
            catch (Exception ex)
            {
                Globals.oApp.MessageBox(NSC_DI.UTIL.Message.Format(ex));
            }
            finally
            {
                NSC_DI.UTIL.Misc.KillObject(CMB_STYPE);
                GC.Collect();
            }
        }

        private void HandleChooseFromListSelectionCFL()
        {
            var sampleTypeValue = _SBO_Form.Items.Item("CMB_STYPE").Specific.Value as string;

            if (string.IsNullOrEmpty(sampleTypeValue))
            {
                Globals.oApp.StatusBar.SetText("Please select a Sample Type before setting recipient",
                    Type: BoStatusBarMessageType.smt_Warning,
                    Seconds: BoMessageTime.bmt_Short);

                return;
            }
            
            switch (sampleTypeValue)
            {
                case "Lab":
                    CreateLabChooseFromList();
                    break;
                case "Employee":
                    CreateEmployeeChooseFromList();
                    break;
                case "Customer":
                    CreateCustomerChooseFromList();
                    break;
            }
        }

        private void CreateCustomerChooseFromList()
        {
            var title = "Customer Choose From List";
            // Specifying the columns we want the Superior CFL to have.
            var matrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
            {
                 new CommonUI.Matrix.MatrixColumn(){ Caption="Code", ColumnWidth=80, ColumnName="Id", ItemType = BoFormItemTypes.it_EDIT}               
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=40, ColumnName="Name", ItemType= BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="State Id", ColumnWidth=80, ColumnName="StateLocationId", ItemType = BoFormItemTypes.it_EDIT}
            };

            //int cflWidth = Math.Max(450, matrixColumns.Count * 150);
            //int cflHeight = 420;

            // Max Width is 80% of the users screen space.
            //This does not currently work correctly in the Web Browser version so we are hard coding the dimensions of the form.
            var width = 700;    //Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
            var height = 400;   //Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);

            var top = 40;       //(int)((Globals.oApp.Desktop.Height - height) / 0.7);
            var left = 380;     //(Globals.oApp.Desktop.Width - width) / 2;

            var sql = @"
SELECT 
	[OCRD].[CardCode] AS [Id]
	, [OCRD].[CardName] AS [Name]
	, [OCRD].[U_NSC_LocUBI] AS [StateLocationId]
FROM [OCRD]
WHERE [OCRD].[U_NSC_LocUBI] IS NOT NULL
	AND [OCRD].[CardType] = 'C' -- Customer only
";
            F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pResults)
            {
                _SBO_Form = B1Connections.theAppl.Forms.Item(pCallingFormUid);

                VendorChooseFromListCallBack(pResults);
            };
            Form oForm_Cfl = F_CFL.FormCreate(title, _SBO_Form.UniqueID, sql, matrixColumns, false, null, top, left, height, width);
        }

        private void CreateLabChooseFromList()
        {
            var title = "Lab Choose From List";

            var matrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
            {
                 new CommonUI.Matrix.MatrixColumn(){ Caption="Id", ColumnWidth=80, ColumnName="Id", ItemType = BoFormItemTypes.it_EDIT}               
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Name", ColumnWidth=40, ColumnName="Name", ItemType= BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="State Id", ColumnWidth=80, ColumnName="StateLocationId", ItemType = BoFormItemTypes.it_EDIT}
            };

            //int cflWidth = Math.Max(450, matrixColumns.Count * 150);
            //int cflHeight = 420;

            // Max Width is 80% of the users screen space.
            //This does not currently work correctly in the Web Browser version so we are hard coding the dimensions of the form.
            var width = 700;    //Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
            var height = 400;   //Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);

            var top = 40;       //(int)((Globals.oApp.Desktop.Height - height) / 0.7);
            var left = 380;     //(Globals.oApp.Desktop.Width - width) / 2;

            // And lastly the Query to populate the Matrix.
            var sql = @"
SELECT [OCRD].[CardCode] AS [Id], [OCRD].[CardName] AS [Name], [OCRD].[U_NSC_LocUBI] AS [StateLocationId]
  FROM [OCRD]
 INNER JOIN OCRG ON OCRD.GroupCode = OCRG.GroupCode
 WHERE [OCRD].[CardType]  = 'S' -- Vendor only
   AND [OCRG].[GroupName] LIKE '%Lab%'";
            F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pResults)
            {
                _SBO_Form = B1Connections.theAppl.Forms.Item(pCallingFormUid);
                LabChooseFromListCallBack(pResults);
            };
            Form oForm_Cfl = F_CFL.FormCreate(title, _SBO_Form.UniqueID, sql, matrixColumns, false, null, top, left, height, width);

        }

        private void CreateEmployeeChooseFromList()
        {
            var title = "Employee Choose From List";
            
            var matrixColumns = new List<CommonUI.Matrix.MatrixColumn>()
            {
                 new CommonUI.Matrix.MatrixColumn(){ Caption="Employee Id", ColumnWidth=80, ColumnName="EmployeeId", ItemType = BoFormItemTypes.it_EDIT}               
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="First", ColumnWidth=40, ColumnName="FirstName", ItemType= BoFormItemTypes.it_EDIT}
                ,new CommonUI.Matrix.MatrixColumn(){ Caption="Last", ColumnWidth=80, ColumnName="LastName", ItemType = BoFormItemTypes.it_EDIT}
            };


            //int cflWidth = Math.Max(450, matrixColumns.Count * 150);
            //int cflHeight = 420;

            // Max Width is 80% of the users screen space.
            //This does not currently work correctly in the Web Browser version so we are hard coding the dimensions of the form.
            var width = 700;    //Math.Min((int)(Globals.oApp.Desktop.Width * 0.8), cflWidth);
            var height = 400;   //Math.Min((int)(Globals.oApp.Desktop.Height * 0.8), cflHeight);

            var top = 40;       //(int)((Globals.oApp.Desktop.Height - height) / 0.7);
            var left = 380;     //(Globals.oApp.Desktop.Width - width) / 2;

            var sql = @"
SELECT 
	[OHEM].[empID] AS [EmployeeId]
	, [OHEM].[firstName] AS [FirstName]
	, [OHEM].[lastName] AS [LastName]
	, [OHEM].[middleName] AS [MiddleName]
	, [OHEM].[jobTitle] AS [JobTitle]
FROM [OHEM]
";
            F_CFL.CflCB = delegate (string pCallingFormUid, System.Data.DataTable pResults)
            {
                _SBO_Form = B1Connections.theAppl.Forms.Item(pCallingFormUid);
                EmployeeChooseFromListCallBack(pResults);
            };
            Form oForm_Cfl = F_CFL.FormCreate(title, _SBO_Form.UniqueID, sql, matrixColumns, false, null, top, left, height, width);
        }

        public void VendorChooseFromListCallBack(System.Data.DataTable results)
        {
            try
            {
                // Selected Type Code to send with the API..
                EditText TXT_RCODE = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_RCODE", _SBO_Form);

                EditText TXT_RNAME = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_RNAME", _SBO_Form);

                // Select our current form.
                _SBO_Form.Select();

                TXT_RCODE.Value = results.Rows[0]["StateLocationId"] as string;
                TXT_RNAME.Value = results.Rows[0]["Name"] as string;
                _SBO_Form.Items.Item("txtRID").Specific.Value = results.Rows[0]["Id"] as string;
            }
            catch (Exception ex)
            {
                // What are you complaining about?
            }
        }

        public void LabChooseFromListCallBack(System.Data.DataTable results)
        {
            try
            {
                // Selected Type Code to send with the API..
                EditText TXT_RCODE = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_RCODE", _SBO_Form);

                EditText TXT_RNAME = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_RNAME", _SBO_Form);

                // Select our current form.
                _SBO_Form.Select();

                TXT_RCODE.Value = results.Rows[0]["StateLocationId"] as string;
                TXT_RNAME.Value = results.Rows[0]["Name"] as string;
                _SBO_Form.Items.Item("txtRID").Specific.Value = results.Rows[0]["Id"] as string;
            }
            catch (Exception ex)
            {
                // What are you complaining about?
            }
        }

        public void EmployeeChooseFromListCallBack(System.Data.DataTable results)
        {
            try
            {
                // Selected Type Code to send with the API..
                EditText TXT_RCODE = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_RCODE", _SBO_Form);

                EditText TXT_RNAME = CommonUI.Forms.GetControlFromForm( CommonUI.Forms.FormControlTypes.EditText, "TXT_RNAME", _SBO_Form);

                // Select our current form.
                _SBO_Form.Select();

                TXT_RCODE.Value = results.Rows[0]["EmployeeId"] as string;
                TXT_RNAME.Value = results.Rows[0]["FirstName"] + " " + results.Rows[0]["LastName"];
                _SBO_Form.Items.Item("txtRID").Specific.Value = results.Rows[0]["EmployeeId"] as string;
            }
            catch (Exception ex)
            {
                // What are you complaining about?
            }
        }
    }
}	