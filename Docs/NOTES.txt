Simulates a user click on a menu item.
Syntax
Visual Basic 
Public Sub ActivateMenuItem( _
   ByVal MenuUID As String _
) 
 

Parameters
MenuUID 
The unique ID of the item to activate 
Remarks
We recommend using this method instead of MenuItem.Activate because:

The MenuUID parameter does not change and does not require you to follow the item path of the Activate method, which is subject to changes. 

This method is more efficient. 

----------------------------------------------------------------------------------------------

menu 5923  ---> Production order, rt click, Issue Components